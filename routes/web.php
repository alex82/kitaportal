<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/login');
});

Auth::routes();

Route::get('imprint', 'ImprintController@index');

Route::get('logout', [ 'uses' => 'Auth\LoginController@logout', 'as' => 'logout' ]);
Route::get('/users/{id}/impersonate', 'Auth\LoginController@impersonate')->name('impersonate');
Route::get('/users/stop', 'Auth\LoginController@stopImpersonate');

Route::get('register/successful', 'Auth\RegisterController@showSuccessfulMessage');
Route::get('user/activation/{token}', 'Auth\RegisterController@activateUser')->name('user.activate');

Route::get('shiftlist_public/{day?}', 'PublicShiftlistController@index')->name('public_shiftlist');

Route::group(['namespace' => 'Admin', 'middleware' => ['auth', 'impersonate']], function () {
    Route::get('download/{file}', 'GlobalController@downloadFile')->name('download_file');

    /**
     * News
     */
    Route::get('news', 'News\PostController@index')->name('news_index');

    Route::group(['middleware' => ['role:admin|manager']], function () {
        Route::get('news/{post}/delete', 'News\PostController@delete');
        Route::get('news/comment/{comment}/delete', 'News\CommentController@delete')->name('delete_comment');

        Route::get('news/create', 'News\PostController@create')->name('news_create_post');
        Route::post('news/store', 'News\PostController@store')->name('news_store_post');

        Route::get('news/{post}/edit', 'News\PostController@edit')->name('news_edit_post');
        Route::post('news/{post}/edit', 'News\PostController@update')->name('news_update_post');
        Route::get('news/{post}/edit/file/{file}/delete', 'News\PostController@deleteFile')->name('news_delete_file');
    });

    Route::get('news/{post}', 'News\PostController@details')->name('news_show_comments');
    Route::post('news/{post}', 'News\CommentController@store')->name('news_create_comment');

    Route::group(['middleware' => ['role:admin|manager']], function () {
        Route::get('news/comment/approval', 'News\CommentController@showApprovals')->name('show_approvals');
        Route::get('news/comment/{comment}/approve', 'News\CommentController@approve')->name('approve');
    });

    /**
     * Shiftlist
     */
    Route::get('shiftlist/view/{year?}/{month?}', 'Shiftlist\IndexController@view')->name('shiftlist_view');
    Route::get('shiftlist/{year?}/{month?}', 'Shiftlist\IndexController@index')->name('shiftlist_index');

    /**
     * Settings
     */
    Route::resource('children', 'Settings\ChildrenController');
    Route::resource('closuretimes', 'Settings\ClosuretimesController');
    Route::get('settings', 'Settings\SettingsController@index')->name('settings_index');
    Route::post('settings', 'Settings\SettingsController@save')->name('settings_save');

    /**
     * UserManager
     */
    Route::resource('user', 'UserManager\UserController');
    Route::resource('parent', 'UserManager\ParentController');
    Route::resource('permission', 'UserManager\PermissionController');
});
