<?php

use Illuminate\Database\Seeder;

class TestingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->createDummyNews();
        $this->createParentForParentManagerTest();
    }

    protected function createParentForParentManagerTest()
    {
        $user = factory(\App\Models\User::class)->create([
            'email' => 'ParentForParentManagerTest@example.com',
            'confirmed' => true,
            'activated' => false
        ]);
        $role = \App\Models\Role::where('name', 'parent')->first();
        $user->attachRole($role);

        factory(\App\Models\Child::class, 4)->create([
            'user_id' => $user->id,
            'nursery_group_id' => \App\Models\NurseryGroup::take(1)->get()->first()->id
        ]);
    }

    protected function createDummyNews()
    {
        factory(\App\Models\Post::class, 1)->create(['title' => 'My Test Post'])->each(function ($post) {
            for($i = 0; $i < 5; $i++) {
                factory(\App\Models\Comment::class)->create([
                    'post_id' => $post->id
                ]);
            }
        });
    }
}
