<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{

    /**
     * @var \App\Models\Role
     */
    protected $admin_role;

    /**
     * @var \App\Models\Role
     */
    protected $manager_role;

    /**
     * @var \App\Models\Role
     */
    protected $nursery_role;

    /**
     * @var \App\Models\Role
     */
    protected $parent_role;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->createAdminRoleAndUser();
        $this->createManagerRoleAndUser();
        $this->createNurseryRoleAndUser();
        $this->createParentRoleAndUser();

        $this->createPermissions();
    }

    private function createPermissions()
    {
        $permission = \App\Models\Permission::create([
            'name' => 'usermanager-access'
        ]);
        $this->admin_role->attachPermission($permission);

        ######################################################

        $permission = \App\Models\Permission::create([
            'name' => 'parentmanager-access'
        ]);
        $this->admin_role->attachPermission($permission);
        $this->manager_role->attachPermission($permission);

        ######################################################

        $permission = \App\Models\Permission::create([
            'name' => 'parentsettings-access'
        ]);
        $this->parent_role->attachPermission($permission);

        ######################################################

        $permission = \App\Models\Permission::create([
            'name' => 'shiftlist-edit'
        ]);
        $this->parent_role->attachPermission($permission);

        ######################################################

        $permission = \App\Models\Permission::create([
            'name' => 'closuretimes-access'
        ]);
        $this->admin_role->attachPermission($permission);
        $this->manager_role->attachPermission($permission);

        ######################################################

        $permission = \App\Models\Permission::create([
            'name' => 'shiftlist-view'
        ]);
        $this->admin_role->attachPermission($permission);
        $this->manager_role->attachPermission($permission);
        $this->nursery_role->attachPermission($permission);

        ######################################################

        $permission = \App\Models\Permission::create([
            'name' => 'news-add'
        ]);
        $this->admin_role->attachPermission($permission);
        $this->manager_role->attachPermission($permission);
        $this->nursery_role->attachPermission($permission);

        ######################################################

        $permission = \App\Models\Permission::create([
            'name' => 'news-delete'
        ]);
        $this->admin_role->attachPermission($permission);
        $this->manager_role->attachPermission($permission);
        $this->nursery_role->attachPermission($permission);

        ######################################################

        $permission = \App\Models\Permission::create([
            'name' => 'news-comment-delete'
        ]);
        $this->admin_role->attachPermission($permission);
        $this->manager_role->attachPermission($permission);
    }

    private function createParentRoleAndUser()
    {
        $user = factory(\App\Models\User::class)->create([
            'name' => 'parent',
            'email' => 'parent@example.com',
            'password' => bcrypt('secret'),
            'remember_token' => str_random(10),
            'confirmed' => true,
            'activated' => true
        ]);

        factory(\App\Models\Child::class, 4)->create([
            'user_id' => $user->id,
            'nursery_group_id' => \App\Models\NurseryGroup::take(1)->get()->first()->id
        ]);

        $this->parent_role = \App\Models\Role::create([
            'name' => 'parent',
            'display_name' => 'Eltern',
            'description' => ''
        ]);

        $user->attachRole($this->parent_role);
    }

    private function createNurseryRoleAndUser()
    {
        $user = factory(\App\Models\User::class)->create([
            'name' => 'nursery',
            'email' => 'nursery@example.com',
            'password' => bcrypt('secret'),
            'remember_token' => str_random(10),
            'confirmed' => true,
            'activated' => true
        ]);

        $this->nursery_role = \App\Models\Role::create([
            'name' => 'nursery',
            'display_name' => 'Erzieher',
            'description' => ''
        ]);

        $user->attachRole($this->nursery_role);
    }

    private function createManagerRoleAndUser()
    {
        $user = factory(\App\Models\User::class)->create([
            'name' => 'manager',
            'email' => 'manager@example.com',
            'password' => bcrypt('secret'),
            'remember_token' => str_random(10),
            'confirmed' => true,
            'activated' => true
        ]);

        $this->manager_role = \App\Models\Role::create([
            'name' => 'manager',
            'display_name' => 'Kita-Leitung',
            'description' => ''
        ]);

        $user->attachRole($this->manager_role);
    }

    private function createAdminRoleAndUser()
    {
        $user = factory(\App\Models\User::class)->create([
            'name' => 'admin',
            'email' => 'admin@example.com',
            'password' => bcrypt('secret'),
            'remember_token' => str_random(10),
            'confirmed' => true,
            'activated' => true
        ]);

        $this->admin_role = \App\Models\Role::create([
            'name' => 'admin',
            'display_name' => 'Admin',
            'description' => ''
        ]);

        $user->attachRole($this->admin_role);
    }
}
