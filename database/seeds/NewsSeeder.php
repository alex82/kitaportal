<?php

use Illuminate\Database\Seeder;

class NewsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\App\Models\Post::class, 3)->create()->each(function ($post) {
            for($i = 0; $i < (rand(0,5)); $i++) {
                factory(\App\Models\Comment::class)->create([
                    'post_id' => $post->id
                ]);
            }
        });
    }
}
