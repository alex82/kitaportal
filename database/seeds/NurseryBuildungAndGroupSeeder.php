<?php

use Illuminate\Database\Seeder;

class NurseryBuildungAndGroupSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\App\Models\NurseryBuilding::class, 3)->create()->each(function ($building) {
            for($i = 0; $i < 3; $i++) {
                factory(\App\Models\NurseryGroup::class)->create([
                    'nursery_building_id' => $building->id
                ]);
            }
        });
    }
}
