<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'name' => $faker->firstName.' '.$faker->lastName,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
        'activated' => false,
        'confirmed' => false
    ];
});

$factory->define(App\Models\NurseryBuilding::class, function (Faker\Generator $faker) {
    return [
        'description' => $faker->word
    ];
});

$factory->define(App\Models\NurseryGroup::class, function (Faker\Generator $faker) {
    return [
        'description' => $faker->word,
        'nursery_building_id' => function () {
            return factory(\App\Models\NurseryBuilding::class)->create()->id;
        }
    ];
});

$factory->define(App\Models\Child::class, function (Faker\Generator $faker) {
    return [
        'firstname' => $faker->firstName,
        'lastname' => $faker->lastName,
        'nursery_group_id' => function () {
            return factory(\App\Models\NurseryGroup::class)->create()->id;
        },
        'user_id' => factory(\App\Models\User::class)->create()->id
    ];
});

$factory->define(App\Models\Comment::class, function (Faker\Generator $faker) {
    return [
        'text' => $faker->paragraphs(rand(1,5), true),
        'post_id' => function () {
            return factory(\App\Models\Post::class)->create()->id;
        },
        'user_id' => function () {
            return factory(\App\Models\User::class)->create()->id;
        }
    ];
});

$factory->define(App\Models\Post::class, function (Faker\Generator $faker) {
    return [
        'title' => $faker->sentence,
        'text' => $faker->paragraphs(rand(1,5), true),
        'user_id' => function () {
            return factory(\App\Models\User::class)->create()->id;
        }
    ];
});

$factory
    ->define(App\Models\ClosureTime::class, function (Faker\Generator $faker) {
        $date = \Carbon\Carbon::parse($faker->date());
        return [
            'from' => $date->format('Y-m-d'),
            'to' => $date->addDays(rand(1,20))->format('Y-m-d'),
            'type' => App\Models\ClosureTime::TYPE_FULL_DAY_CLOSED
        ];
    })
    ->state(App\Models\ClosureTime::class, App\Models\ClosureTime::TYPE_FULL_DAY_CLOSED, function ($faker) {
        return [
            'type' => App\Models\ClosureTime::TYPE_FULL_DAY_CLOSED,
        ];
    })
    ->state(App\Models\ClosureTime::class, App\Models\ClosureTime::TYPE_MORNING_CLOSED, function ($faker) {
        return [
            'to' => null,
            'type' => App\Models\ClosureTime::TYPE_MORNING_CLOSED,
        ];
    });