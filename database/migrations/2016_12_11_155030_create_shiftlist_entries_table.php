<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShiftlistEntriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shiftlist_entries', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('child_id')->unsigned()->nullable();
            $table->foreign('child_id')->references('id')->on('children')->onDelete('cascade');
            $table->date('day');
            $table->string('start_time')->nullable();
            $table->string('end_time')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shiftlist_entries');
    }
}
