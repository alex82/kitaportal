<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNurseryGroupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('nursery_groups', function (Blueprint $table) {
            $table->increments('id');

            $table->string('description');

            $table->integer('nursery_building_id')->unsigned()->nullable();
            $table->foreign('nursery_building_id')->references('id')->on('nursery_buildings')->onDelete('SET NULL');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('nursery_groups');
    }
}
