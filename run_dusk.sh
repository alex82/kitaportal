SERVER=10.0.2.2
PORT=9515
</dev/tcp/$SERVER/$PORT
if [ "$?" -ne 0 ]; then
  echo "Please start chrome drive with command: vendor\laravel\dusk\bin\chromedriver-win.exe"
  exit 1
else
  echo "Chrome driver running ..."
fi

echo "refreshing database"
php artisan migrate:refresh --seed --env=testing_local

echo "seeding test data"
php artisan db:seed --class=TestingSeeder --env=testing_local

echo "running dusk"
php artisan dusk