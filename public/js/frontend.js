/*!
 * jQuery JavaScript Library v2.1.4
 * http://jquery.com/
 *
 * Includes Sizzle.js
 * http://sizzlejs.com/
 *
 * Copyright 2005, 2014 jQuery Foundation, Inc. and other contributors
 * Released under the MIT license
 * http://jquery.org/license
 *
 * Date: 2015-04-28T16:01Z
 */

(function( global, factory ) {

	if ( typeof module === "object" && typeof module.exports === "object" ) {
		// For CommonJS and CommonJS-like environments where a proper `window`
		// is present, execute the factory and get jQuery.
		// For environments that do not have a `window` with a `document`
		// (such as Node.js), expose a factory as module.exports.
		// This accentuates the need for the creation of a real `window`.
		// e.g. var jQuery = require("jquery")(window);
		// See ticket #14549 for more info.
		module.exports = global.document ?
			factory( global, true ) :
			function( w ) {
				if ( !w.document ) {
					throw new Error( "jQuery requires a window with a document" );
				}
				return factory( w );
			};
	} else {
		factory( global );
	}

// Pass this if window is not defined yet
}(typeof window !== "undefined" ? window : this, function( window, noGlobal ) {

// Support: Firefox 18+
// Can't be in strict mode, several libs including ASP.NET trace
// the stack via arguments.caller.callee and Firefox dies if
// you try to trace through "use strict" call chains. (#13335)
//

var arr = [];

var slice = arr.slice;

var concat = arr.concat;

var push = arr.push;

var indexOf = arr.indexOf;

var class2type = {};

var toString = class2type.toString;

var hasOwn = class2type.hasOwnProperty;

var support = {};



var
	// Use the correct document accordingly with window argument (sandbox)
	document = window.document,

	version = "2.1.4",

	// Define a local copy of jQuery
	jQuery = function( selector, context ) {
		// The jQuery object is actually just the init constructor 'enhanced'
		// Need init if jQuery is called (just allow error to be thrown if not included)
		return new jQuery.fn.init( selector, context );
	},

	// Support: Android<4.1
	// Make sure we trim BOM and NBSP
	rtrim = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g,

	// Matches dashed string for camelizing
	rmsPrefix = /^-ms-/,
	rdashAlpha = /-([\da-z])/gi,

	// Used by jQuery.camelCase as callback to replace()
	fcamelCase = function( all, letter ) {
		return letter.toUpperCase();
	};

jQuery.fn = jQuery.prototype = {
	// The current version of jQuery being used
	jquery: version,

	constructor: jQuery,

	// Start with an empty selector
	selector: "",

	// The default length of a jQuery object is 0
	length: 0,

	toArray: function() {
		return slice.call( this );
	},

	// Get the Nth element in the matched element set OR
	// Get the whole matched element set as a clean array
	get: function( num ) {
		return num != null ?

			// Return just the one element from the set
			( num < 0 ? this[ num + this.length ] : this[ num ] ) :

			// Return all the elements in a clean array
			slice.call( this );
	},

	// Take an array of elements and push it onto the stack
	// (returning the new matched element set)
	pushStack: function( elems ) {

		// Build a new jQuery matched element set
		var ret = jQuery.merge( this.constructor(), elems );

		// Add the old object onto the stack (as a reference)
		ret.prevObject = this;
		ret.context = this.context;

		// Return the newly-formed element set
		return ret;
	},

	// Execute a callback for every element in the matched set.
	// (You can seed the arguments with an array of args, but this is
	// only used internally.)
	each: function( callback, args ) {
		return jQuery.each( this, callback, args );
	},

	map: function( callback ) {
		return this.pushStack( jQuery.map(this, function( elem, i ) {
			return callback.call( elem, i, elem );
		}));
	},

	slice: function() {
		return this.pushStack( slice.apply( this, arguments ) );
	},

	first: function() {
		return this.eq( 0 );
	},

	last: function() {
		return this.eq( -1 );
	},

	eq: function( i ) {
		var len = this.length,
			j = +i + ( i < 0 ? len : 0 );
		return this.pushStack( j >= 0 && j < len ? [ this[j] ] : [] );
	},

	end: function() {
		return this.prevObject || this.constructor(null);
	},

	// For internal use only.
	// Behaves like an Array's method, not like a jQuery method.
	push: push,
	sort: arr.sort,
	splice: arr.splice
};

jQuery.extend = jQuery.fn.extend = function() {
	var options, name, src, copy, copyIsArray, clone,
		target = arguments[0] || {},
		i = 1,
		length = arguments.length,
		deep = false;

	// Handle a deep copy situation
	if ( typeof target === "boolean" ) {
		deep = target;

		// Skip the boolean and the target
		target = arguments[ i ] || {};
		i++;
	}

	// Handle case when target is a string or something (possible in deep copy)
	if ( typeof target !== "object" && !jQuery.isFunction(target) ) {
		target = {};
	}

	// Extend jQuery itself if only one argument is passed
	if ( i === length ) {
		target = this;
		i--;
	}

	for ( ; i < length; i++ ) {
		// Only deal with non-null/undefined values
		if ( (options = arguments[ i ]) != null ) {
			// Extend the base object
			for ( name in options ) {
				src = target[ name ];
				copy = options[ name ];

				// Prevent never-ending loop
				if ( target === copy ) {
					continue;
				}

				// Recurse if we're merging plain objects or arrays
				if ( deep && copy && ( jQuery.isPlainObject(copy) || (copyIsArray = jQuery.isArray(copy)) ) ) {
					if ( copyIsArray ) {
						copyIsArray = false;
						clone = src && jQuery.isArray(src) ? src : [];

					} else {
						clone = src && jQuery.isPlainObject(src) ? src : {};
					}

					// Never move original objects, clone them
					target[ name ] = jQuery.extend( deep, clone, copy );

				// Don't bring in undefined values
				} else if ( copy !== undefined ) {
					target[ name ] = copy;
				}
			}
		}
	}

	// Return the modified object
	return target;
};

jQuery.extend({
	// Unique for each copy of jQuery on the page
	expando: "jQuery" + ( version + Math.random() ).replace( /\D/g, "" ),

	// Assume jQuery is ready without the ready module
	isReady: true,

	error: function( msg ) {
		throw new Error( msg );
	},

	noop: function() {},

	isFunction: function( obj ) {
		return jQuery.type(obj) === "function";
	},

	isArray: Array.isArray,

	isWindow: function( obj ) {
		return obj != null && obj === obj.window;
	},

	isNumeric: function( obj ) {
		// parseFloat NaNs numeric-cast false positives (null|true|false|"")
		// ...but misinterprets leading-number strings, particularly hex literals ("0x...")
		// subtraction forces infinities to NaN
		// adding 1 corrects loss of precision from parseFloat (#15100)
		return !jQuery.isArray( obj ) && (obj - parseFloat( obj ) + 1) >= 0;
	},

	isPlainObject: function( obj ) {
		// Not plain objects:
		// - Any object or value whose internal [[Class]] property is not "[object Object]"
		// - DOM nodes
		// - window
		if ( jQuery.type( obj ) !== "object" || obj.nodeType || jQuery.isWindow( obj ) ) {
			return false;
		}

		if ( obj.constructor &&
				!hasOwn.call( obj.constructor.prototype, "isPrototypeOf" ) ) {
			return false;
		}

		// If the function hasn't returned already, we're confident that
		// |obj| is a plain object, created by {} or constructed with new Object
		return true;
	},

	isEmptyObject: function( obj ) {
		var name;
		for ( name in obj ) {
			return false;
		}
		return true;
	},

	type: function( obj ) {
		if ( obj == null ) {
			return obj + "";
		}
		// Support: Android<4.0, iOS<6 (functionish RegExp)
		return typeof obj === "object" || typeof obj === "function" ?
			class2type[ toString.call(obj) ] || "object" :
			typeof obj;
	},

	// Evaluates a script in a global context
	globalEval: function( code ) {
		var script,
			indirect = eval;

		code = jQuery.trim( code );

		if ( code ) {
			// If the code includes a valid, prologue position
			// strict mode pragma, execute code by injecting a
			// script tag into the document.
			if ( code.indexOf("use strict") === 1 ) {
				script = document.createElement("script");
				script.text = code;
				document.head.appendChild( script ).parentNode.removeChild( script );
			} else {
			// Otherwise, avoid the DOM node creation, insertion
			// and removal by using an indirect global eval
				indirect( code );
			}
		}
	},

	// Convert dashed to camelCase; used by the css and data modules
	// Support: IE9-11+
	// Microsoft forgot to hump their vendor prefix (#9572)
	camelCase: function( string ) {
		return string.replace( rmsPrefix, "ms-" ).replace( rdashAlpha, fcamelCase );
	},

	nodeName: function( elem, name ) {
		return elem.nodeName && elem.nodeName.toLowerCase() === name.toLowerCase();
	},

	// args is for internal usage only
	each: function( obj, callback, args ) {
		var value,
			i = 0,
			length = obj.length,
			isArray = isArraylike( obj );

		if ( args ) {
			if ( isArray ) {
				for ( ; i < length; i++ ) {
					value = callback.apply( obj[ i ], args );

					if ( value === false ) {
						break;
					}
				}
			} else {
				for ( i in obj ) {
					value = callback.apply( obj[ i ], args );

					if ( value === false ) {
						break;
					}
				}
			}

		// A special, fast, case for the most common use of each
		} else {
			if ( isArray ) {
				for ( ; i < length; i++ ) {
					value = callback.call( obj[ i ], i, obj[ i ] );

					if ( value === false ) {
						break;
					}
				}
			} else {
				for ( i in obj ) {
					value = callback.call( obj[ i ], i, obj[ i ] );

					if ( value === false ) {
						break;
					}
				}
			}
		}

		return obj;
	},

	// Support: Android<4.1
	trim: function( text ) {
		return text == null ?
			"" :
			( text + "" ).replace( rtrim, "" );
	},

	// results is for internal usage only
	makeArray: function( arr, results ) {
		var ret = results || [];

		if ( arr != null ) {
			if ( isArraylike( Object(arr) ) ) {
				jQuery.merge( ret,
					typeof arr === "string" ?
					[ arr ] : arr
				);
			} else {
				push.call( ret, arr );
			}
		}

		return ret;
	},

	inArray: function( elem, arr, i ) {
		return arr == null ? -1 : indexOf.call( arr, elem, i );
	},

	merge: function( first, second ) {
		var len = +second.length,
			j = 0,
			i = first.length;

		for ( ; j < len; j++ ) {
			first[ i++ ] = second[ j ];
		}

		first.length = i;

		return first;
	},

	grep: function( elems, callback, invert ) {
		var callbackInverse,
			matches = [],
			i = 0,
			length = elems.length,
			callbackExpect = !invert;

		// Go through the array, only saving the items
		// that pass the validator function
		for ( ; i < length; i++ ) {
			callbackInverse = !callback( elems[ i ], i );
			if ( callbackInverse !== callbackExpect ) {
				matches.push( elems[ i ] );
			}
		}

		return matches;
	},

	// arg is for internal usage only
	map: function( elems, callback, arg ) {
		var value,
			i = 0,
			length = elems.length,
			isArray = isArraylike( elems ),
			ret = [];

		// Go through the array, translating each of the items to their new values
		if ( isArray ) {
			for ( ; i < length; i++ ) {
				value = callback( elems[ i ], i, arg );

				if ( value != null ) {
					ret.push( value );
				}
			}

		// Go through every key on the object,
		} else {
			for ( i in elems ) {
				value = callback( elems[ i ], i, arg );

				if ( value != null ) {
					ret.push( value );
				}
			}
		}

		// Flatten any nested arrays
		return concat.apply( [], ret );
	},

	// A global GUID counter for objects
	guid: 1,

	// Bind a function to a context, optionally partially applying any
	// arguments.
	proxy: function( fn, context ) {
		var tmp, args, proxy;

		if ( typeof context === "string" ) {
			tmp = fn[ context ];
			context = fn;
			fn = tmp;
		}

		// Quick check to determine if target is callable, in the spec
		// this throws a TypeError, but we will just return undefined.
		if ( !jQuery.isFunction( fn ) ) {
			return undefined;
		}

		// Simulated bind
		args = slice.call( arguments, 2 );
		proxy = function() {
			return fn.apply( context || this, args.concat( slice.call( arguments ) ) );
		};

		// Set the guid of unique handler to the same of original handler, so it can be removed
		proxy.guid = fn.guid = fn.guid || jQuery.guid++;

		return proxy;
	},

	now: Date.now,

	// jQuery.support is not used in Core but other projects attach their
	// properties to it so it needs to exist.
	support: support
});

// Populate the class2type map
jQuery.each("Boolean Number String Function Array Date RegExp Object Error".split(" "), function(i, name) {
	class2type[ "[object " + name + "]" ] = name.toLowerCase();
});

function isArraylike( obj ) {

	// Support: iOS 8.2 (not reproducible in simulator)
	// `in` check used to prevent JIT error (gh-2145)
	// hasOwn isn't used here due to false negatives
	// regarding Nodelist length in IE
	var length = "length" in obj && obj.length,
		type = jQuery.type( obj );

	if ( type === "function" || jQuery.isWindow( obj ) ) {
		return false;
	}

	if ( obj.nodeType === 1 && length ) {
		return true;
	}

	return type === "array" || length === 0 ||
		typeof length === "number" && length > 0 && ( length - 1 ) in obj;
}
var Sizzle =
/*!
 * Sizzle CSS Selector Engine v2.2.0-pre
 * http://sizzlejs.com/
 *
 * Copyright 2008, 2014 jQuery Foundation, Inc. and other contributors
 * Released under the MIT license
 * http://jquery.org/license
 *
 * Date: 2014-12-16
 */
(function( window ) {

var i,
	support,
	Expr,
	getText,
	isXML,
	tokenize,
	compile,
	select,
	outermostContext,
	sortInput,
	hasDuplicate,

	// Local document vars
	setDocument,
	document,
	docElem,
	documentIsHTML,
	rbuggyQSA,
	rbuggyMatches,
	matches,
	contains,

	// Instance-specific data
	expando = "sizzle" + 1 * new Date(),
	preferredDoc = window.document,
	dirruns = 0,
	done = 0,
	classCache = createCache(),
	tokenCache = createCache(),
	compilerCache = createCache(),
	sortOrder = function( a, b ) {
		if ( a === b ) {
			hasDuplicate = true;
		}
		return 0;
	},

	// General-purpose constants
	MAX_NEGATIVE = 1 << 31,

	// Instance methods
	hasOwn = ({}).hasOwnProperty,
	arr = [],
	pop = arr.pop,
	push_native = arr.push,
	push = arr.push,
	slice = arr.slice,
	// Use a stripped-down indexOf as it's faster than native
	// http://jsperf.com/thor-indexof-vs-for/5
	indexOf = function( list, elem ) {
		var i = 0,
			len = list.length;
		for ( ; i < len; i++ ) {
			if ( list[i] === elem ) {
				return i;
			}
		}
		return -1;
	},

	booleans = "checked|selected|async|autofocus|autoplay|controls|defer|disabled|hidden|ismap|loop|multiple|open|readonly|required|scoped",

	// Regular expressions

	// Whitespace characters http://www.w3.org/TR/css3-selectors/#whitespace
	whitespace = "[\\x20\\t\\r\\n\\f]",
	// http://www.w3.org/TR/css3-syntax/#characters
	characterEncoding = "(?:\\\\.|[\\w-]|[^\\x00-\\xa0])+",

	// Loosely modeled on CSS identifier characters
	// An unquoted value should be a CSS identifier http://www.w3.org/TR/css3-selectors/#attribute-selectors
	// Proper syntax: http://www.w3.org/TR/CSS21/syndata.html#value-def-identifier
	identifier = characterEncoding.replace( "w", "w#" ),

	// Attribute selectors: http://www.w3.org/TR/selectors/#attribute-selectors
	attributes = "\\[" + whitespace + "*(" + characterEncoding + ")(?:" + whitespace +
		// Operator (capture 2)
		"*([*^$|!~]?=)" + whitespace +
		// "Attribute values must be CSS identifiers [capture 5] or strings [capture 3 or capture 4]"
		"*(?:'((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\"|(" + identifier + "))|)" + whitespace +
		"*\\]",

	pseudos = ":(" + characterEncoding + ")(?:\\((" +
		// To reduce the number of selectors needing tokenize in the preFilter, prefer arguments:
		// 1. quoted (capture 3; capture 4 or capture 5)
		"('((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\")|" +
		// 2. simple (capture 6)
		"((?:\\\\.|[^\\\\()[\\]]|" + attributes + ")*)|" +
		// 3. anything else (capture 2)
		".*" +
		")\\)|)",

	// Leading and non-escaped trailing whitespace, capturing some non-whitespace characters preceding the latter
	rwhitespace = new RegExp( whitespace + "+", "g" ),
	rtrim = new RegExp( "^" + whitespace + "+|((?:^|[^\\\\])(?:\\\\.)*)" + whitespace + "+$", "g" ),

	rcomma = new RegExp( "^" + whitespace + "*," + whitespace + "*" ),
	rcombinators = new RegExp( "^" + whitespace + "*([>+~]|" + whitespace + ")" + whitespace + "*" ),

	rattributeQuotes = new RegExp( "=" + whitespace + "*([^\\]'\"]*?)" + whitespace + "*\\]", "g" ),

	rpseudo = new RegExp( pseudos ),
	ridentifier = new RegExp( "^" + identifier + "$" ),

	matchExpr = {
		"ID": new RegExp( "^#(" + characterEncoding + ")" ),
		"CLASS": new RegExp( "^\\.(" + characterEncoding + ")" ),
		"TAG": new RegExp( "^(" + characterEncoding.replace( "w", "w*" ) + ")" ),
		"ATTR": new RegExp( "^" + attributes ),
		"PSEUDO": new RegExp( "^" + pseudos ),
		"CHILD": new RegExp( "^:(only|first|last|nth|nth-last)-(child|of-type)(?:\\(" + whitespace +
			"*(even|odd|(([+-]|)(\\d*)n|)" + whitespace + "*(?:([+-]|)" + whitespace +
			"*(\\d+)|))" + whitespace + "*\\)|)", "i" ),
		"bool": new RegExp( "^(?:" + booleans + ")$", "i" ),
		// For use in libraries implementing .is()
		// We use this for POS matching in `select`
		"needsContext": new RegExp( "^" + whitespace + "*[>+~]|:(even|odd|eq|gt|lt|nth|first|last)(?:\\(" +
			whitespace + "*((?:-\\d)?\\d*)" + whitespace + "*\\)|)(?=[^-]|$)", "i" )
	},

	rinputs = /^(?:input|select|textarea|button)$/i,
	rheader = /^h\d$/i,

	rnative = /^[^{]+\{\s*\[native \w/,

	// Easily-parseable/retrievable ID or TAG or CLASS selectors
	rquickExpr = /^(?:#([\w-]+)|(\w+)|\.([\w-]+))$/,

	rsibling = /[+~]/,
	rescape = /'|\\/g,

	// CSS escapes http://www.w3.org/TR/CSS21/syndata.html#escaped-characters
	runescape = new RegExp( "\\\\([\\da-f]{1,6}" + whitespace + "?|(" + whitespace + ")|.)", "ig" ),
	funescape = function( _, escaped, escapedWhitespace ) {
		var high = "0x" + escaped - 0x10000;
		// NaN means non-codepoint
		// Support: Firefox<24
		// Workaround erroneous numeric interpretation of +"0x"
		return high !== high || escapedWhitespace ?
			escaped :
			high < 0 ?
				// BMP codepoint
				String.fromCharCode( high + 0x10000 ) :
				// Supplemental Plane codepoint (surrogate pair)
				String.fromCharCode( high >> 10 | 0xD800, high & 0x3FF | 0xDC00 );
	},

	// Used for iframes
	// See setDocument()
	// Removing the function wrapper causes a "Permission Denied"
	// error in IE
	unloadHandler = function() {
		setDocument();
	};

// Optimize for push.apply( _, NodeList )
try {
	push.apply(
		(arr = slice.call( preferredDoc.childNodes )),
		preferredDoc.childNodes
	);
	// Support: Android<4.0
	// Detect silently failing push.apply
	arr[ preferredDoc.childNodes.length ].nodeType;
} catch ( e ) {
	push = { apply: arr.length ?

		// Leverage slice if possible
		function( target, els ) {
			push_native.apply( target, slice.call(els) );
		} :

		// Support: IE<9
		// Otherwise append directly
		function( target, els ) {
			var j = target.length,
				i = 0;
			// Can't trust NodeList.length
			while ( (target[j++] = els[i++]) ) {}
			target.length = j - 1;
		}
	};
}

function Sizzle( selector, context, results, seed ) {
	var match, elem, m, nodeType,
		// QSA vars
		i, groups, old, nid, newContext, newSelector;

	if ( ( context ? context.ownerDocument || context : preferredDoc ) !== document ) {
		setDocument( context );
	}

	context = context || document;
	results = results || [];
	nodeType = context.nodeType;

	if ( typeof selector !== "string" || !selector ||
		nodeType !== 1 && nodeType !== 9 && nodeType !== 11 ) {

		return results;
	}

	if ( !seed && documentIsHTML ) {

		// Try to shortcut find operations when possible (e.g., not under DocumentFragment)
		if ( nodeType !== 11 && (match = rquickExpr.exec( selector )) ) {
			// Speed-up: Sizzle("#ID")
			if ( (m = match[1]) ) {
				if ( nodeType === 9 ) {
					elem = context.getElementById( m );
					// Check parentNode to catch when Blackberry 4.6 returns
					// nodes that are no longer in the document (jQuery #6963)
					if ( elem && elem.parentNode ) {
						// Handle the case where IE, Opera, and Webkit return items
						// by name instead of ID
						if ( elem.id === m ) {
							results.push( elem );
							return results;
						}
					} else {
						return results;
					}
				} else {
					// Context is not a document
					if ( context.ownerDocument && (elem = context.ownerDocument.getElementById( m )) &&
						contains( context, elem ) && elem.id === m ) {
						results.push( elem );
						return results;
					}
				}

			// Speed-up: Sizzle("TAG")
			} else if ( match[2] ) {
				push.apply( results, context.getElementsByTagName( selector ) );
				return results;

			// Speed-up: Sizzle(".CLASS")
			} else if ( (m = match[3]) && support.getElementsByClassName ) {
				push.apply( results, context.getElementsByClassName( m ) );
				return results;
			}
		}

		// QSA path
		if ( support.qsa && (!rbuggyQSA || !rbuggyQSA.test( selector )) ) {
			nid = old = expando;
			newContext = context;
			newSelector = nodeType !== 1 && selector;

			// qSA works strangely on Element-rooted queries
			// We can work around this by specifying an extra ID on the root
			// and working up from there (Thanks to Andrew Dupont for the technique)
			// IE 8 doesn't work on object elements
			if ( nodeType === 1 && context.nodeName.toLowerCase() !== "object" ) {
				groups = tokenize( selector );

				if ( (old = context.getAttribute("id")) ) {
					nid = old.replace( rescape, "\\$&" );
				} else {
					context.setAttribute( "id", nid );
				}
				nid = "[id='" + nid + "'] ";

				i = groups.length;
				while ( i-- ) {
					groups[i] = nid + toSelector( groups[i] );
				}
				newContext = rsibling.test( selector ) && testContext( context.parentNode ) || context;
				newSelector = groups.join(",");
			}

			if ( newSelector ) {
				try {
					push.apply( results,
						newContext.querySelectorAll( newSelector )
					);
					return results;
				} catch(qsaError) {
				} finally {
					if ( !old ) {
						context.removeAttribute("id");
					}
				}
			}
		}
	}

	// All others
	return select( selector.replace( rtrim, "$1" ), context, results, seed );
}

/**
 * Create key-value caches of limited size
 * @returns {Function(string, Object)} Returns the Object data after storing it on itself with
 *	property name the (space-suffixed) string and (if the cache is larger than Expr.cacheLength)
 *	deleting the oldest entry
 */
function createCache() {
	var keys = [];

	function cache( key, value ) {
		// Use (key + " ") to avoid collision with native prototype properties (see Issue #157)
		if ( keys.push( key + " " ) > Expr.cacheLength ) {
			// Only keep the most recent entries
			delete cache[ keys.shift() ];
		}
		return (cache[ key + " " ] = value);
	}
	return cache;
}

/**
 * Mark a function for special use by Sizzle
 * @param {Function} fn The function to mark
 */
function markFunction( fn ) {
	fn[ expando ] = true;
	return fn;
}

/**
 * Support testing using an element
 * @param {Function} fn Passed the created div and expects a boolean result
 */
function assert( fn ) {
	var div = document.createElement("div");

	try {
		return !!fn( div );
	} catch (e) {
		return false;
	} finally {
		// Remove from its parent by default
		if ( div.parentNode ) {
			div.parentNode.removeChild( div );
		}
		// release memory in IE
		div = null;
	}
}

/**
 * Adds the same handler for all of the specified attrs
 * @param {String} attrs Pipe-separated list of attributes
 * @param {Function} handler The method that will be applied
 */
function addHandle( attrs, handler ) {
	var arr = attrs.split("|"),
		i = attrs.length;

	while ( i-- ) {
		Expr.attrHandle[ arr[i] ] = handler;
	}
}

/**
 * Checks document order of two siblings
 * @param {Element} a
 * @param {Element} b
 * @returns {Number} Returns less than 0 if a precedes b, greater than 0 if a follows b
 */
function siblingCheck( a, b ) {
	var cur = b && a,
		diff = cur && a.nodeType === 1 && b.nodeType === 1 &&
			( ~b.sourceIndex || MAX_NEGATIVE ) -
			( ~a.sourceIndex || MAX_NEGATIVE );

	// Use IE sourceIndex if available on both nodes
	if ( diff ) {
		return diff;
	}

	// Check if b follows a
	if ( cur ) {
		while ( (cur = cur.nextSibling) ) {
			if ( cur === b ) {
				return -1;
			}
		}
	}

	return a ? 1 : -1;
}

/**
 * Returns a function to use in pseudos for input types
 * @param {String} type
 */
function createInputPseudo( type ) {
	return function( elem ) {
		var name = elem.nodeName.toLowerCase();
		return name === "input" && elem.type === type;
	};
}

/**
 * Returns a function to use in pseudos for buttons
 * @param {String} type
 */
function createButtonPseudo( type ) {
	return function( elem ) {
		var name = elem.nodeName.toLowerCase();
		return (name === "input" || name === "button") && elem.type === type;
	};
}

/**
 * Returns a function to use in pseudos for positionals
 * @param {Function} fn
 */
function createPositionalPseudo( fn ) {
	return markFunction(function( argument ) {
		argument = +argument;
		return markFunction(function( seed, matches ) {
			var j,
				matchIndexes = fn( [], seed.length, argument ),
				i = matchIndexes.length;

			// Match elements found at the specified indexes
			while ( i-- ) {
				if ( seed[ (j = matchIndexes[i]) ] ) {
					seed[j] = !(matches[j] = seed[j]);
				}
			}
		});
	});
}

/**
 * Checks a node for validity as a Sizzle context
 * @param {Element|Object=} context
 * @returns {Element|Object|Boolean} The input node if acceptable, otherwise a falsy value
 */
function testContext( context ) {
	return context && typeof context.getElementsByTagName !== "undefined" && context;
}

// Expose support vars for convenience
support = Sizzle.support = {};

/**
 * Detects XML nodes
 * @param {Element|Object} elem An element or a document
 * @returns {Boolean} True iff elem is a non-HTML XML node
 */
isXML = Sizzle.isXML = function( elem ) {
	// documentElement is verified for cases where it doesn't yet exist
	// (such as loading iframes in IE - #4833)
	var documentElement = elem && (elem.ownerDocument || elem).documentElement;
	return documentElement ? documentElement.nodeName !== "HTML" : false;
};

/**
 * Sets document-related variables once based on the current document
 * @param {Element|Object} [doc] An element or document object to use to set the document
 * @returns {Object} Returns the current document
 */
setDocument = Sizzle.setDocument = function( node ) {
	var hasCompare, parent,
		doc = node ? node.ownerDocument || node : preferredDoc;

	// If no document and documentElement is available, return
	if ( doc === document || doc.nodeType !== 9 || !doc.documentElement ) {
		return document;
	}

	// Set our document
	document = doc;
	docElem = doc.documentElement;
	parent = doc.defaultView;

	// Support: IE>8
	// If iframe document is assigned to "document" variable and if iframe has been reloaded,
	// IE will throw "permission denied" error when accessing "document" variable, see jQuery #13936
	// IE6-8 do not support the defaultView property so parent will be undefined
	if ( parent && parent !== parent.top ) {
		// IE11 does not have attachEvent, so all must suffer
		if ( parent.addEventListener ) {
			parent.addEventListener( "unload", unloadHandler, false );
		} else if ( parent.attachEvent ) {
			parent.attachEvent( "onunload", unloadHandler );
		}
	}

	/* Support tests
	---------------------------------------------------------------------- */
	documentIsHTML = !isXML( doc );

	/* Attributes
	---------------------------------------------------------------------- */

	// Support: IE<8
	// Verify that getAttribute really returns attributes and not properties
	// (excepting IE8 booleans)
	support.attributes = assert(function( div ) {
		div.className = "i";
		return !div.getAttribute("className");
	});

	/* getElement(s)By*
	---------------------------------------------------------------------- */

	// Check if getElementsByTagName("*") returns only elements
	support.getElementsByTagName = assert(function( div ) {
		div.appendChild( doc.createComment("") );
		return !div.getElementsByTagName("*").length;
	});

	// Support: IE<9
	support.getElementsByClassName = rnative.test( doc.getElementsByClassName );

	// Support: IE<10
	// Check if getElementById returns elements by name
	// The broken getElementById methods don't pick up programatically-set names,
	// so use a roundabout getElementsByName test
	support.getById = assert(function( div ) {
		docElem.appendChild( div ).id = expando;
		return !doc.getElementsByName || !doc.getElementsByName( expando ).length;
	});

	// ID find and filter
	if ( support.getById ) {
		Expr.find["ID"] = function( id, context ) {
			if ( typeof context.getElementById !== "undefined" && documentIsHTML ) {
				var m = context.getElementById( id );
				// Check parentNode to catch when Blackberry 4.6 returns
				// nodes that are no longer in the document #6963
				return m && m.parentNode ? [ m ] : [];
			}
		};
		Expr.filter["ID"] = function( id ) {
			var attrId = id.replace( runescape, funescape );
			return function( elem ) {
				return elem.getAttribute("id") === attrId;
			};
		};
	} else {
		// Support: IE6/7
		// getElementById is not reliable as a find shortcut
		delete Expr.find["ID"];

		Expr.filter["ID"] =  function( id ) {
			var attrId = id.replace( runescape, funescape );
			return function( elem ) {
				var node = typeof elem.getAttributeNode !== "undefined" && elem.getAttributeNode("id");
				return node && node.value === attrId;
			};
		};
	}

	// Tag
	Expr.find["TAG"] = support.getElementsByTagName ?
		function( tag, context ) {
			if ( typeof context.getElementsByTagName !== "undefined" ) {
				return context.getElementsByTagName( tag );

			// DocumentFragment nodes don't have gEBTN
			} else if ( support.qsa ) {
				return context.querySelectorAll( tag );
			}
		} :

		function( tag, context ) {
			var elem,
				tmp = [],
				i = 0,
				// By happy coincidence, a (broken) gEBTN appears on DocumentFragment nodes too
				results = context.getElementsByTagName( tag );

			// Filter out possible comments
			if ( tag === "*" ) {
				while ( (elem = results[i++]) ) {
					if ( elem.nodeType === 1 ) {
						tmp.push( elem );
					}
				}

				return tmp;
			}
			return results;
		};

	// Class
	Expr.find["CLASS"] = support.getElementsByClassName && function( className, context ) {
		if ( documentIsHTML ) {
			return context.getElementsByClassName( className );
		}
	};

	/* QSA/matchesSelector
	---------------------------------------------------------------------- */

	// QSA and matchesSelector support

	// matchesSelector(:active) reports false when true (IE9/Opera 11.5)
	rbuggyMatches = [];

	// qSa(:focus) reports false when true (Chrome 21)
	// We allow this because of a bug in IE8/9 that throws an error
	// whenever `document.activeElement` is accessed on an iframe
	// So, we allow :focus to pass through QSA all the time to avoid the IE error
	// See http://bugs.jquery.com/ticket/13378
	rbuggyQSA = [];

	if ( (support.qsa = rnative.test( doc.querySelectorAll )) ) {
		// Build QSA regex
		// Regex strategy adopted from Diego Perini
		assert(function( div ) {
			// Select is set to empty string on purpose
			// This is to test IE's treatment of not explicitly
			// setting a boolean content attribute,
			// since its presence should be enough
			// http://bugs.jquery.com/ticket/12359
			docElem.appendChild( div ).innerHTML = "<a id='" + expando + "'></a>" +
				"<select id='" + expando + "-\f]' msallowcapture=''>" +
				"<option selected=''></option></select>";

			// Support: IE8, Opera 11-12.16
			// Nothing should be selected when empty strings follow ^= or $= or *=
			// The test attribute must be unknown in Opera but "safe" for WinRT
			// http://msdn.microsoft.com/en-us/library/ie/hh465388.aspx#attribute_section
			if ( div.querySelectorAll("[msallowcapture^='']").length ) {
				rbuggyQSA.push( "[*^$]=" + whitespace + "*(?:''|\"\")" );
			}

			// Support: IE8
			// Boolean attributes and "value" are not treated correctly
			if ( !div.querySelectorAll("[selected]").length ) {
				rbuggyQSA.push( "\\[" + whitespace + "*(?:value|" + booleans + ")" );
			}

			// Support: Chrome<29, Android<4.2+, Safari<7.0+, iOS<7.0+, PhantomJS<1.9.7+
			if ( !div.querySelectorAll( "[id~=" + expando + "-]" ).length ) {
				rbuggyQSA.push("~=");
			}

			// Webkit/Opera - :checked should return selected option elements
			// http://www.w3.org/TR/2011/REC-css3-selectors-20110929/#checked
			// IE8 throws error here and will not see later tests
			if ( !div.querySelectorAll(":checked").length ) {
				rbuggyQSA.push(":checked");
			}

			// Support: Safari 8+, iOS 8+
			// https://bugs.webkit.org/show_bug.cgi?id=136851
			// In-page `selector#id sibing-combinator selector` fails
			if ( !div.querySelectorAll( "a#" + expando + "+*" ).length ) {
				rbuggyQSA.push(".#.+[+~]");
			}
		});

		assert(function( div ) {
			// Support: Windows 8 Native Apps
			// The type and name attributes are restricted during .innerHTML assignment
			var input = doc.createElement("input");
			input.setAttribute( "type", "hidden" );
			div.appendChild( input ).setAttribute( "name", "D" );

			// Support: IE8
			// Enforce case-sensitivity of name attribute
			if ( div.querySelectorAll("[name=d]").length ) {
				rbuggyQSA.push( "name" + whitespace + "*[*^$|!~]?=" );
			}

			// FF 3.5 - :enabled/:disabled and hidden elements (hidden elements are still enabled)
			// IE8 throws error here and will not see later tests
			if ( !div.querySelectorAll(":enabled").length ) {
				rbuggyQSA.push( ":enabled", ":disabled" );
			}

			// Opera 10-11 does not throw on post-comma invalid pseudos
			div.querySelectorAll("*,:x");
			rbuggyQSA.push(",.*:");
		});
	}

	if ( (support.matchesSelector = rnative.test( (matches = docElem.matches ||
		docElem.webkitMatchesSelector ||
		docElem.mozMatchesSelector ||
		docElem.oMatchesSelector ||
		docElem.msMatchesSelector) )) ) {

		assert(function( div ) {
			// Check to see if it's possible to do matchesSelector
			// on a disconnected node (IE 9)
			support.disconnectedMatch = matches.call( div, "div" );

			// This should fail with an exception
			// Gecko does not error, returns false instead
			matches.call( div, "[s!='']:x" );
			rbuggyMatches.push( "!=", pseudos );
		});
	}

	rbuggyQSA = rbuggyQSA.length && new RegExp( rbuggyQSA.join("|") );
	rbuggyMatches = rbuggyMatches.length && new RegExp( rbuggyMatches.join("|") );

	/* Contains
	---------------------------------------------------------------------- */
	hasCompare = rnative.test( docElem.compareDocumentPosition );

	// Element contains another
	// Purposefully does not implement inclusive descendent
	// As in, an element does not contain itself
	contains = hasCompare || rnative.test( docElem.contains ) ?
		function( a, b ) {
			var adown = a.nodeType === 9 ? a.documentElement : a,
				bup = b && b.parentNode;
			return a === bup || !!( bup && bup.nodeType === 1 && (
				adown.contains ?
					adown.contains( bup ) :
					a.compareDocumentPosition && a.compareDocumentPosition( bup ) & 16
			));
		} :
		function( a, b ) {
			if ( b ) {
				while ( (b = b.parentNode) ) {
					if ( b === a ) {
						return true;
					}
				}
			}
			return false;
		};

	/* Sorting
	---------------------------------------------------------------------- */

	// Document order sorting
	sortOrder = hasCompare ?
	function( a, b ) {

		// Flag for duplicate removal
		if ( a === b ) {
			hasDuplicate = true;
			return 0;
		}

		// Sort on method existence if only one input has compareDocumentPosition
		var compare = !a.compareDocumentPosition - !b.compareDocumentPosition;
		if ( compare ) {
			return compare;
		}

		// Calculate position if both inputs belong to the same document
		compare = ( a.ownerDocument || a ) === ( b.ownerDocument || b ) ?
			a.compareDocumentPosition( b ) :

			// Otherwise we know they are disconnected
			1;

		// Disconnected nodes
		if ( compare & 1 ||
			(!support.sortDetached && b.compareDocumentPosition( a ) === compare) ) {

			// Choose the first element that is related to our preferred document
			if ( a === doc || a.ownerDocument === preferredDoc && contains(preferredDoc, a) ) {
				return -1;
			}
			if ( b === doc || b.ownerDocument === preferredDoc && contains(preferredDoc, b) ) {
				return 1;
			}

			// Maintain original order
			return sortInput ?
				( indexOf( sortInput, a ) - indexOf( sortInput, b ) ) :
				0;
		}

		return compare & 4 ? -1 : 1;
	} :
	function( a, b ) {
		// Exit early if the nodes are identical
		if ( a === b ) {
			hasDuplicate = true;
			return 0;
		}

		var cur,
			i = 0,
			aup = a.parentNode,
			bup = b.parentNode,
			ap = [ a ],
			bp = [ b ];

		// Parentless nodes are either documents or disconnected
		if ( !aup || !bup ) {
			return a === doc ? -1 :
				b === doc ? 1 :
				aup ? -1 :
				bup ? 1 :
				sortInput ?
				( indexOf( sortInput, a ) - indexOf( sortInput, b ) ) :
				0;

		// If the nodes are siblings, we can do a quick check
		} else if ( aup === bup ) {
			return siblingCheck( a, b );
		}

		// Otherwise we need full lists of their ancestors for comparison
		cur = a;
		while ( (cur = cur.parentNode) ) {
			ap.unshift( cur );
		}
		cur = b;
		while ( (cur = cur.parentNode) ) {
			bp.unshift( cur );
		}

		// Walk down the tree looking for a discrepancy
		while ( ap[i] === bp[i] ) {
			i++;
		}

		return i ?
			// Do a sibling check if the nodes have a common ancestor
			siblingCheck( ap[i], bp[i] ) :

			// Otherwise nodes in our document sort first
			ap[i] === preferredDoc ? -1 :
			bp[i] === preferredDoc ? 1 :
			0;
	};

	return doc;
};

Sizzle.matches = function( expr, elements ) {
	return Sizzle( expr, null, null, elements );
};

Sizzle.matchesSelector = function( elem, expr ) {
	// Set document vars if needed
	if ( ( elem.ownerDocument || elem ) !== document ) {
		setDocument( elem );
	}

	// Make sure that attribute selectors are quoted
	expr = expr.replace( rattributeQuotes, "='$1']" );

	if ( support.matchesSelector && documentIsHTML &&
		( !rbuggyMatches || !rbuggyMatches.test( expr ) ) &&
		( !rbuggyQSA     || !rbuggyQSA.test( expr ) ) ) {

		try {
			var ret = matches.call( elem, expr );

			// IE 9's matchesSelector returns false on disconnected nodes
			if ( ret || support.disconnectedMatch ||
					// As well, disconnected nodes are said to be in a document
					// fragment in IE 9
					elem.document && elem.document.nodeType !== 11 ) {
				return ret;
			}
		} catch (e) {}
	}

	return Sizzle( expr, document, null, [ elem ] ).length > 0;
};

Sizzle.contains = function( context, elem ) {
	// Set document vars if needed
	if ( ( context.ownerDocument || context ) !== document ) {
		setDocument( context );
	}
	return contains( context, elem );
};

Sizzle.attr = function( elem, name ) {
	// Set document vars if needed
	if ( ( elem.ownerDocument || elem ) !== document ) {
		setDocument( elem );
	}

	var fn = Expr.attrHandle[ name.toLowerCase() ],
		// Don't get fooled by Object.prototype properties (jQuery #13807)
		val = fn && hasOwn.call( Expr.attrHandle, name.toLowerCase() ) ?
			fn( elem, name, !documentIsHTML ) :
			undefined;

	return val !== undefined ?
		val :
		support.attributes || !documentIsHTML ?
			elem.getAttribute( name ) :
			(val = elem.getAttributeNode(name)) && val.specified ?
				val.value :
				null;
};

Sizzle.error = function( msg ) {
	throw new Error( "Syntax error, unrecognized expression: " + msg );
};

/**
 * Document sorting and removing duplicates
 * @param {ArrayLike} results
 */
Sizzle.uniqueSort = function( results ) {
	var elem,
		duplicates = [],
		j = 0,
		i = 0;

	// Unless we *know* we can detect duplicates, assume their presence
	hasDuplicate = !support.detectDuplicates;
	sortInput = !support.sortStable && results.slice( 0 );
	results.sort( sortOrder );

	if ( hasDuplicate ) {
		while ( (elem = results[i++]) ) {
			if ( elem === results[ i ] ) {
				j = duplicates.push( i );
			}
		}
		while ( j-- ) {
			results.splice( duplicates[ j ], 1 );
		}
	}

	// Clear input after sorting to release objects
	// See https://github.com/jquery/sizzle/pull/225
	sortInput = null;

	return results;
};

/**
 * Utility function for retrieving the text value of an array of DOM nodes
 * @param {Array|Element} elem
 */
getText = Sizzle.getText = function( elem ) {
	var node,
		ret = "",
		i = 0,
		nodeType = elem.nodeType;

	if ( !nodeType ) {
		// If no nodeType, this is expected to be an array
		while ( (node = elem[i++]) ) {
			// Do not traverse comment nodes
			ret += getText( node );
		}
	} else if ( nodeType === 1 || nodeType === 9 || nodeType === 11 ) {
		// Use textContent for elements
		// innerText usage removed for consistency of new lines (jQuery #11153)
		if ( typeof elem.textContent === "string" ) {
			return elem.textContent;
		} else {
			// Traverse its children
			for ( elem = elem.firstChild; elem; elem = elem.nextSibling ) {
				ret += getText( elem );
			}
		}
	} else if ( nodeType === 3 || nodeType === 4 ) {
		return elem.nodeValue;
	}
	// Do not include comment or processing instruction nodes

	return ret;
};

Expr = Sizzle.selectors = {

	// Can be adjusted by the user
	cacheLength: 50,

	createPseudo: markFunction,

	match: matchExpr,

	attrHandle: {},

	find: {},

	relative: {
		">": { dir: "parentNode", first: true },
		" ": { dir: "parentNode" },
		"+": { dir: "previousSibling", first: true },
		"~": { dir: "previousSibling" }
	},

	preFilter: {
		"ATTR": function( match ) {
			match[1] = match[1].replace( runescape, funescape );

			// Move the given value to match[3] whether quoted or unquoted
			match[3] = ( match[3] || match[4] || match[5] || "" ).replace( runescape, funescape );

			if ( match[2] === "~=" ) {
				match[3] = " " + match[3] + " ";
			}

			return match.slice( 0, 4 );
		},

		"CHILD": function( match ) {
			/* matches from matchExpr["CHILD"]
				1 type (only|nth|...)
				2 what (child|of-type)
				3 argument (even|odd|\d*|\d*n([+-]\d+)?|...)
				4 xn-component of xn+y argument ([+-]?\d*n|)
				5 sign of xn-component
				6 x of xn-component
				7 sign of y-component
				8 y of y-component
			*/
			match[1] = match[1].toLowerCase();

			if ( match[1].slice( 0, 3 ) === "nth" ) {
				// nth-* requires argument
				if ( !match[3] ) {
					Sizzle.error( match[0] );
				}

				// numeric x and y parameters for Expr.filter.CHILD
				// remember that false/true cast respectively to 0/1
				match[4] = +( match[4] ? match[5] + (match[6] || 1) : 2 * ( match[3] === "even" || match[3] === "odd" ) );
				match[5] = +( ( match[7] + match[8] ) || match[3] === "odd" );

			// other types prohibit arguments
			} else if ( match[3] ) {
				Sizzle.error( match[0] );
			}

			return match;
		},

		"PSEUDO": function( match ) {
			var excess,
				unquoted = !match[6] && match[2];

			if ( matchExpr["CHILD"].test( match[0] ) ) {
				return null;
			}

			// Accept quoted arguments as-is
			if ( match[3] ) {
				match[2] = match[4] || match[5] || "";

			// Strip excess characters from unquoted arguments
			} else if ( unquoted && rpseudo.test( unquoted ) &&
				// Get excess from tokenize (recursively)
				(excess = tokenize( unquoted, true )) &&
				// advance to the next closing parenthesis
				(excess = unquoted.indexOf( ")", unquoted.length - excess ) - unquoted.length) ) {

				// excess is a negative index
				match[0] = match[0].slice( 0, excess );
				match[2] = unquoted.slice( 0, excess );
			}

			// Return only captures needed by the pseudo filter method (type and argument)
			return match.slice( 0, 3 );
		}
	},

	filter: {

		"TAG": function( nodeNameSelector ) {
			var nodeName = nodeNameSelector.replace( runescape, funescape ).toLowerCase();
			return nodeNameSelector === "*" ?
				function() { return true; } :
				function( elem ) {
					return elem.nodeName && elem.nodeName.toLowerCase() === nodeName;
				};
		},

		"CLASS": function( className ) {
			var pattern = classCache[ className + " " ];

			return pattern ||
				(pattern = new RegExp( "(^|" + whitespace + ")" + className + "(" + whitespace + "|$)" )) &&
				classCache( className, function( elem ) {
					return pattern.test( typeof elem.className === "string" && elem.className || typeof elem.getAttribute !== "undefined" && elem.getAttribute("class") || "" );
				});
		},

		"ATTR": function( name, operator, check ) {
			return function( elem ) {
				var result = Sizzle.attr( elem, name );

				if ( result == null ) {
					return operator === "!=";
				}
				if ( !operator ) {
					return true;
				}

				result += "";

				return operator === "=" ? result === check :
					operator === "!=" ? result !== check :
					operator === "^=" ? check && result.indexOf( check ) === 0 :
					operator === "*=" ? check && result.indexOf( check ) > -1 :
					operator === "$=" ? check && result.slice( -check.length ) === check :
					operator === "~=" ? ( " " + result.replace( rwhitespace, " " ) + " " ).indexOf( check ) > -1 :
					operator === "|=" ? result === check || result.slice( 0, check.length + 1 ) === check + "-" :
					false;
			};
		},

		"CHILD": function( type, what, argument, first, last ) {
			var simple = type.slice( 0, 3 ) !== "nth",
				forward = type.slice( -4 ) !== "last",
				ofType = what === "of-type";

			return first === 1 && last === 0 ?

				// Shortcut for :nth-*(n)
				function( elem ) {
					return !!elem.parentNode;
				} :

				function( elem, context, xml ) {
					var cache, outerCache, node, diff, nodeIndex, start,
						dir = simple !== forward ? "nextSibling" : "previousSibling",
						parent = elem.parentNode,
						name = ofType && elem.nodeName.toLowerCase(),
						useCache = !xml && !ofType;

					if ( parent ) {

						// :(first|last|only)-(child|of-type)
						if ( simple ) {
							while ( dir ) {
								node = elem;
								while ( (node = node[ dir ]) ) {
									if ( ofType ? node.nodeName.toLowerCase() === name : node.nodeType === 1 ) {
										return false;
									}
								}
								// Reverse direction for :only-* (if we haven't yet done so)
								start = dir = type === "only" && !start && "nextSibling";
							}
							return true;
						}

						start = [ forward ? parent.firstChild : parent.lastChild ];

						// non-xml :nth-child(...) stores cache data on `parent`
						if ( forward && useCache ) {
							// Seek `elem` from a previously-cached index
							outerCache = parent[ expando ] || (parent[ expando ] = {});
							cache = outerCache[ type ] || [];
							nodeIndex = cache[0] === dirruns && cache[1];
							diff = cache[0] === dirruns && cache[2];
							node = nodeIndex && parent.childNodes[ nodeIndex ];

							while ( (node = ++nodeIndex && node && node[ dir ] ||

								// Fallback to seeking `elem` from the start
								(diff = nodeIndex = 0) || start.pop()) ) {

								// When found, cache indexes on `parent` and break
								if ( node.nodeType === 1 && ++diff && node === elem ) {
									outerCache[ type ] = [ dirruns, nodeIndex, diff ];
									break;
								}
							}

						// Use previously-cached element index if available
						} else if ( useCache && (cache = (elem[ expando ] || (elem[ expando ] = {}))[ type ]) && cache[0] === dirruns ) {
							diff = cache[1];

						// xml :nth-child(...) or :nth-last-child(...) or :nth(-last)?-of-type(...)
						} else {
							// Use the same loop as above to seek `elem` from the start
							while ( (node = ++nodeIndex && node && node[ dir ] ||
								(diff = nodeIndex = 0) || start.pop()) ) {

								if ( ( ofType ? node.nodeName.toLowerCase() === name : node.nodeType === 1 ) && ++diff ) {
									// Cache the index of each encountered element
									if ( useCache ) {
										(node[ expando ] || (node[ expando ] = {}))[ type ] = [ dirruns, diff ];
									}

									if ( node === elem ) {
										break;
									}
								}
							}
						}

						// Incorporate the offset, then check against cycle size
						diff -= last;
						return diff === first || ( diff % first === 0 && diff / first >= 0 );
					}
				};
		},

		"PSEUDO": function( pseudo, argument ) {
			// pseudo-class names are case-insensitive
			// http://www.w3.org/TR/selectors/#pseudo-classes
			// Prioritize by case sensitivity in case custom pseudos are added with uppercase letters
			// Remember that setFilters inherits from pseudos
			var args,
				fn = Expr.pseudos[ pseudo ] || Expr.setFilters[ pseudo.toLowerCase() ] ||
					Sizzle.error( "unsupported pseudo: " + pseudo );

			// The user may use createPseudo to indicate that
			// arguments are needed to create the filter function
			// just as Sizzle does
			if ( fn[ expando ] ) {
				return fn( argument );
			}

			// But maintain support for old signatures
			if ( fn.length > 1 ) {
				args = [ pseudo, pseudo, "", argument ];
				return Expr.setFilters.hasOwnProperty( pseudo.toLowerCase() ) ?
					markFunction(function( seed, matches ) {
						var idx,
							matched = fn( seed, argument ),
							i = matched.length;
						while ( i-- ) {
							idx = indexOf( seed, matched[i] );
							seed[ idx ] = !( matches[ idx ] = matched[i] );
						}
					}) :
					function( elem ) {
						return fn( elem, 0, args );
					};
			}

			return fn;
		}
	},

	pseudos: {
		// Potentially complex pseudos
		"not": markFunction(function( selector ) {
			// Trim the selector passed to compile
			// to avoid treating leading and trailing
			// spaces as combinators
			var input = [],
				results = [],
				matcher = compile( selector.replace( rtrim, "$1" ) );

			return matcher[ expando ] ?
				markFunction(function( seed, matches, context, xml ) {
					var elem,
						unmatched = matcher( seed, null, xml, [] ),
						i = seed.length;

					// Match elements unmatched by `matcher`
					while ( i-- ) {
						if ( (elem = unmatched[i]) ) {
							seed[i] = !(matches[i] = elem);
						}
					}
				}) :
				function( elem, context, xml ) {
					input[0] = elem;
					matcher( input, null, xml, results );
					// Don't keep the element (issue #299)
					input[0] = null;
					return !results.pop();
				};
		}),

		"has": markFunction(function( selector ) {
			return function( elem ) {
				return Sizzle( selector, elem ).length > 0;
			};
		}),

		"contains": markFunction(function( text ) {
			text = text.replace( runescape, funescape );
			return function( elem ) {
				return ( elem.textContent || elem.innerText || getText( elem ) ).indexOf( text ) > -1;
			};
		}),

		// "Whether an element is represented by a :lang() selector
		// is based solely on the element's language value
		// being equal to the identifier C,
		// or beginning with the identifier C immediately followed by "-".
		// The matching of C against the element's language value is performed case-insensitively.
		// The identifier C does not have to be a valid language name."
		// http://www.w3.org/TR/selectors/#lang-pseudo
		"lang": markFunction( function( lang ) {
			// lang value must be a valid identifier
			if ( !ridentifier.test(lang || "") ) {
				Sizzle.error( "unsupported lang: " + lang );
			}
			lang = lang.replace( runescape, funescape ).toLowerCase();
			return function( elem ) {
				var elemLang;
				do {
					if ( (elemLang = documentIsHTML ?
						elem.lang :
						elem.getAttribute("xml:lang") || elem.getAttribute("lang")) ) {

						elemLang = elemLang.toLowerCase();
						return elemLang === lang || elemLang.indexOf( lang + "-" ) === 0;
					}
				} while ( (elem = elem.parentNode) && elem.nodeType === 1 );
				return false;
			};
		}),

		// Miscellaneous
		"target": function( elem ) {
			var hash = window.location && window.location.hash;
			return hash && hash.slice( 1 ) === elem.id;
		},

		"root": function( elem ) {
			return elem === docElem;
		},

		"focus": function( elem ) {
			return elem === document.activeElement && (!document.hasFocus || document.hasFocus()) && !!(elem.type || elem.href || ~elem.tabIndex);
		},

		// Boolean properties
		"enabled": function( elem ) {
			return elem.disabled === false;
		},

		"disabled": function( elem ) {
			return elem.disabled === true;
		},

		"checked": function( elem ) {
			// In CSS3, :checked should return both checked and selected elements
			// http://www.w3.org/TR/2011/REC-css3-selectors-20110929/#checked
			var nodeName = elem.nodeName.toLowerCase();
			return (nodeName === "input" && !!elem.checked) || (nodeName === "option" && !!elem.selected);
		},

		"selected": function( elem ) {
			// Accessing this property makes selected-by-default
			// options in Safari work properly
			if ( elem.parentNode ) {
				elem.parentNode.selectedIndex;
			}

			return elem.selected === true;
		},

		// Contents
		"empty": function( elem ) {
			// http://www.w3.org/TR/selectors/#empty-pseudo
			// :empty is negated by element (1) or content nodes (text: 3; cdata: 4; entity ref: 5),
			//   but not by others (comment: 8; processing instruction: 7; etc.)
			// nodeType < 6 works because attributes (2) do not appear as children
			for ( elem = elem.firstChild; elem; elem = elem.nextSibling ) {
				if ( elem.nodeType < 6 ) {
					return false;
				}
			}
			return true;
		},

		"parent": function( elem ) {
			return !Expr.pseudos["empty"]( elem );
		},

		// Element/input types
		"header": function( elem ) {
			return rheader.test( elem.nodeName );
		},

		"input": function( elem ) {
			return rinputs.test( elem.nodeName );
		},

		"button": function( elem ) {
			var name = elem.nodeName.toLowerCase();
			return name === "input" && elem.type === "button" || name === "button";
		},

		"text": function( elem ) {
			var attr;
			return elem.nodeName.toLowerCase() === "input" &&
				elem.type === "text" &&

				// Support: IE<8
				// New HTML5 attribute values (e.g., "search") appear with elem.type === "text"
				( (attr = elem.getAttribute("type")) == null || attr.toLowerCase() === "text" );
		},

		// Position-in-collection
		"first": createPositionalPseudo(function() {
			return [ 0 ];
		}),

		"last": createPositionalPseudo(function( matchIndexes, length ) {
			return [ length - 1 ];
		}),

		"eq": createPositionalPseudo(function( matchIndexes, length, argument ) {
			return [ argument < 0 ? argument + length : argument ];
		}),

		"even": createPositionalPseudo(function( matchIndexes, length ) {
			var i = 0;
			for ( ; i < length; i += 2 ) {
				matchIndexes.push( i );
			}
			return matchIndexes;
		}),

		"odd": createPositionalPseudo(function( matchIndexes, length ) {
			var i = 1;
			for ( ; i < length; i += 2 ) {
				matchIndexes.push( i );
			}
			return matchIndexes;
		}),

		"lt": createPositionalPseudo(function( matchIndexes, length, argument ) {
			var i = argument < 0 ? argument + length : argument;
			for ( ; --i >= 0; ) {
				matchIndexes.push( i );
			}
			return matchIndexes;
		}),

		"gt": createPositionalPseudo(function( matchIndexes, length, argument ) {
			var i = argument < 0 ? argument + length : argument;
			for ( ; ++i < length; ) {
				matchIndexes.push( i );
			}
			return matchIndexes;
		})
	}
};

Expr.pseudos["nth"] = Expr.pseudos["eq"];

// Add button/input type pseudos
for ( i in { radio: true, checkbox: true, file: true, password: true, image: true } ) {
	Expr.pseudos[ i ] = createInputPseudo( i );
}
for ( i in { submit: true, reset: true } ) {
	Expr.pseudos[ i ] = createButtonPseudo( i );
}

// Easy API for creating new setFilters
function setFilters() {}
setFilters.prototype = Expr.filters = Expr.pseudos;
Expr.setFilters = new setFilters();

tokenize = Sizzle.tokenize = function( selector, parseOnly ) {
	var matched, match, tokens, type,
		soFar, groups, preFilters,
		cached = tokenCache[ selector + " " ];

	if ( cached ) {
		return parseOnly ? 0 : cached.slice( 0 );
	}

	soFar = selector;
	groups = [];
	preFilters = Expr.preFilter;

	while ( soFar ) {

		// Comma and first run
		if ( !matched || (match = rcomma.exec( soFar )) ) {
			if ( match ) {
				// Don't consume trailing commas as valid
				soFar = soFar.slice( match[0].length ) || soFar;
			}
			groups.push( (tokens = []) );
		}

		matched = false;

		// Combinators
		if ( (match = rcombinators.exec( soFar )) ) {
			matched = match.shift();
			tokens.push({
				value: matched,
				// Cast descendant combinators to space
				type: match[0].replace( rtrim, " " )
			});
			soFar = soFar.slice( matched.length );
		}

		// Filters
		for ( type in Expr.filter ) {
			if ( (match = matchExpr[ type ].exec( soFar )) && (!preFilters[ type ] ||
				(match = preFilters[ type ]( match ))) ) {
				matched = match.shift();
				tokens.push({
					value: matched,
					type: type,
					matches: match
				});
				soFar = soFar.slice( matched.length );
			}
		}

		if ( !matched ) {
			break;
		}
	}

	// Return the length of the invalid excess
	// if we're just parsing
	// Otherwise, throw an error or return tokens
	return parseOnly ?
		soFar.length :
		soFar ?
			Sizzle.error( selector ) :
			// Cache the tokens
			tokenCache( selector, groups ).slice( 0 );
};

function toSelector( tokens ) {
	var i = 0,
		len = tokens.length,
		selector = "";
	for ( ; i < len; i++ ) {
		selector += tokens[i].value;
	}
	return selector;
}

function addCombinator( matcher, combinator, base ) {
	var dir = combinator.dir,
		checkNonElements = base && dir === "parentNode",
		doneName = done++;

	return combinator.first ?
		// Check against closest ancestor/preceding element
		function( elem, context, xml ) {
			while ( (elem = elem[ dir ]) ) {
				if ( elem.nodeType === 1 || checkNonElements ) {
					return matcher( elem, context, xml );
				}
			}
		} :

		// Check against all ancestor/preceding elements
		function( elem, context, xml ) {
			var oldCache, outerCache,
				newCache = [ dirruns, doneName ];

			// We can't set arbitrary data on XML nodes, so they don't benefit from dir caching
			if ( xml ) {
				while ( (elem = elem[ dir ]) ) {
					if ( elem.nodeType === 1 || checkNonElements ) {
						if ( matcher( elem, context, xml ) ) {
							return true;
						}
					}
				}
			} else {
				while ( (elem = elem[ dir ]) ) {
					if ( elem.nodeType === 1 || checkNonElements ) {
						outerCache = elem[ expando ] || (elem[ expando ] = {});
						if ( (oldCache = outerCache[ dir ]) &&
							oldCache[ 0 ] === dirruns && oldCache[ 1 ] === doneName ) {

							// Assign to newCache so results back-propagate to previous elements
							return (newCache[ 2 ] = oldCache[ 2 ]);
						} else {
							// Reuse newcache so results back-propagate to previous elements
							outerCache[ dir ] = newCache;

							// A match means we're done; a fail means we have to keep checking
							if ( (newCache[ 2 ] = matcher( elem, context, xml )) ) {
								return true;
							}
						}
					}
				}
			}
		};
}

function elementMatcher( matchers ) {
	return matchers.length > 1 ?
		function( elem, context, xml ) {
			var i = matchers.length;
			while ( i-- ) {
				if ( !matchers[i]( elem, context, xml ) ) {
					return false;
				}
			}
			return true;
		} :
		matchers[0];
}

function multipleContexts( selector, contexts, results ) {
	var i = 0,
		len = contexts.length;
	for ( ; i < len; i++ ) {
		Sizzle( selector, contexts[i], results );
	}
	return results;
}

function condense( unmatched, map, filter, context, xml ) {
	var elem,
		newUnmatched = [],
		i = 0,
		len = unmatched.length,
		mapped = map != null;

	for ( ; i < len; i++ ) {
		if ( (elem = unmatched[i]) ) {
			if ( !filter || filter( elem, context, xml ) ) {
				newUnmatched.push( elem );
				if ( mapped ) {
					map.push( i );
				}
			}
		}
	}

	return newUnmatched;
}

function setMatcher( preFilter, selector, matcher, postFilter, postFinder, postSelector ) {
	if ( postFilter && !postFilter[ expando ] ) {
		postFilter = setMatcher( postFilter );
	}
	if ( postFinder && !postFinder[ expando ] ) {
		postFinder = setMatcher( postFinder, postSelector );
	}
	return markFunction(function( seed, results, context, xml ) {
		var temp, i, elem,
			preMap = [],
			postMap = [],
			preexisting = results.length,

			// Get initial elements from seed or context
			elems = seed || multipleContexts( selector || "*", context.nodeType ? [ context ] : context, [] ),

			// Prefilter to get matcher input, preserving a map for seed-results synchronization
			matcherIn = preFilter && ( seed || !selector ) ?
				condense( elems, preMap, preFilter, context, xml ) :
				elems,

			matcherOut = matcher ?
				// If we have a postFinder, or filtered seed, or non-seed postFilter or preexisting results,
				postFinder || ( seed ? preFilter : preexisting || postFilter ) ?

					// ...intermediate processing is necessary
					[] :

					// ...otherwise use results directly
					results :
				matcherIn;

		// Find primary matches
		if ( matcher ) {
			matcher( matcherIn, matcherOut, context, xml );
		}

		// Apply postFilter
		if ( postFilter ) {
			temp = condense( matcherOut, postMap );
			postFilter( temp, [], context, xml );

			// Un-match failing elements by moving them back to matcherIn
			i = temp.length;
			while ( i-- ) {
				if ( (elem = temp[i]) ) {
					matcherOut[ postMap[i] ] = !(matcherIn[ postMap[i] ] = elem);
				}
			}
		}

		if ( seed ) {
			if ( postFinder || preFilter ) {
				if ( postFinder ) {
					// Get the final matcherOut by condensing this intermediate into postFinder contexts
					temp = [];
					i = matcherOut.length;
					while ( i-- ) {
						if ( (elem = matcherOut[i]) ) {
							// Restore matcherIn since elem is not yet a final match
							temp.push( (matcherIn[i] = elem) );
						}
					}
					postFinder( null, (matcherOut = []), temp, xml );
				}

				// Move matched elements from seed to results to keep them synchronized
				i = matcherOut.length;
				while ( i-- ) {
					if ( (elem = matcherOut[i]) &&
						(temp = postFinder ? indexOf( seed, elem ) : preMap[i]) > -1 ) {

						seed[temp] = !(results[temp] = elem);
					}
				}
			}

		// Add elements to results, through postFinder if defined
		} else {
			matcherOut = condense(
				matcherOut === results ?
					matcherOut.splice( preexisting, matcherOut.length ) :
					matcherOut
			);
			if ( postFinder ) {
				postFinder( null, results, matcherOut, xml );
			} else {
				push.apply( results, matcherOut );
			}
		}
	});
}

function matcherFromTokens( tokens ) {
	var checkContext, matcher, j,
		len = tokens.length,
		leadingRelative = Expr.relative[ tokens[0].type ],
		implicitRelative = leadingRelative || Expr.relative[" "],
		i = leadingRelative ? 1 : 0,

		// The foundational matcher ensures that elements are reachable from top-level context(s)
		matchContext = addCombinator( function( elem ) {
			return elem === checkContext;
		}, implicitRelative, true ),
		matchAnyContext = addCombinator( function( elem ) {
			return indexOf( checkContext, elem ) > -1;
		}, implicitRelative, true ),
		matchers = [ function( elem, context, xml ) {
			var ret = ( !leadingRelative && ( xml || context !== outermostContext ) ) || (
				(checkContext = context).nodeType ?
					matchContext( elem, context, xml ) :
					matchAnyContext( elem, context, xml ) );
			// Avoid hanging onto element (issue #299)
			checkContext = null;
			return ret;
		} ];

	for ( ; i < len; i++ ) {
		if ( (matcher = Expr.relative[ tokens[i].type ]) ) {
			matchers = [ addCombinator(elementMatcher( matchers ), matcher) ];
		} else {
			matcher = Expr.filter[ tokens[i].type ].apply( null, tokens[i].matches );

			// Return special upon seeing a positional matcher
			if ( matcher[ expando ] ) {
				// Find the next relative operator (if any) for proper handling
				j = ++i;
				for ( ; j < len; j++ ) {
					if ( Expr.relative[ tokens[j].type ] ) {
						break;
					}
				}
				return setMatcher(
					i > 1 && elementMatcher( matchers ),
					i > 1 && toSelector(
						// If the preceding token was a descendant combinator, insert an implicit any-element `*`
						tokens.slice( 0, i - 1 ).concat({ value: tokens[ i - 2 ].type === " " ? "*" : "" })
					).replace( rtrim, "$1" ),
					matcher,
					i < j && matcherFromTokens( tokens.slice( i, j ) ),
					j < len && matcherFromTokens( (tokens = tokens.slice( j )) ),
					j < len && toSelector( tokens )
				);
			}
			matchers.push( matcher );
		}
	}

	return elementMatcher( matchers );
}

function matcherFromGroupMatchers( elementMatchers, setMatchers ) {
	var bySet = setMatchers.length > 0,
		byElement = elementMatchers.length > 0,
		superMatcher = function( seed, context, xml, results, outermost ) {
			var elem, j, matcher,
				matchedCount = 0,
				i = "0",
				unmatched = seed && [],
				setMatched = [],
				contextBackup = outermostContext,
				// We must always have either seed elements or outermost context
				elems = seed || byElement && Expr.find["TAG"]( "*", outermost ),
				// Use integer dirruns iff this is the outermost matcher
				dirrunsUnique = (dirruns += contextBackup == null ? 1 : Math.random() || 0.1),
				len = elems.length;

			if ( outermost ) {
				outermostContext = context !== document && context;
			}

			// Add elements passing elementMatchers directly to results
			// Keep `i` a string if there are no elements so `matchedCount` will be "00" below
			// Support: IE<9, Safari
			// Tolerate NodeList properties (IE: "length"; Safari: <number>) matching elements by id
			for ( ; i !== len && (elem = elems[i]) != null; i++ ) {
				if ( byElement && elem ) {
					j = 0;
					while ( (matcher = elementMatchers[j++]) ) {
						if ( matcher( elem, context, xml ) ) {
							results.push( elem );
							break;
						}
					}
					if ( outermost ) {
						dirruns = dirrunsUnique;
					}
				}

				// Track unmatched elements for set filters
				if ( bySet ) {
					// They will have gone through all possible matchers
					if ( (elem = !matcher && elem) ) {
						matchedCount--;
					}

					// Lengthen the array for every element, matched or not
					if ( seed ) {
						unmatched.push( elem );
					}
				}
			}

			// Apply set filters to unmatched elements
			matchedCount += i;
			if ( bySet && i !== matchedCount ) {
				j = 0;
				while ( (matcher = setMatchers[j++]) ) {
					matcher( unmatched, setMatched, context, xml );
				}

				if ( seed ) {
					// Reintegrate element matches to eliminate the need for sorting
					if ( matchedCount > 0 ) {
						while ( i-- ) {
							if ( !(unmatched[i] || setMatched[i]) ) {
								setMatched[i] = pop.call( results );
							}
						}
					}

					// Discard index placeholder values to get only actual matches
					setMatched = condense( setMatched );
				}

				// Add matches to results
				push.apply( results, setMatched );

				// Seedless set matches succeeding multiple successful matchers stipulate sorting
				if ( outermost && !seed && setMatched.length > 0 &&
					( matchedCount + setMatchers.length ) > 1 ) {

					Sizzle.uniqueSort( results );
				}
			}

			// Override manipulation of globals by nested matchers
			if ( outermost ) {
				dirruns = dirrunsUnique;
				outermostContext = contextBackup;
			}

			return unmatched;
		};

	return bySet ?
		markFunction( superMatcher ) :
		superMatcher;
}

compile = Sizzle.compile = function( selector, match /* Internal Use Only */ ) {
	var i,
		setMatchers = [],
		elementMatchers = [],
		cached = compilerCache[ selector + " " ];

	if ( !cached ) {
		// Generate a function of recursive functions that can be used to check each element
		if ( !match ) {
			match = tokenize( selector );
		}
		i = match.length;
		while ( i-- ) {
			cached = matcherFromTokens( match[i] );
			if ( cached[ expando ] ) {
				setMatchers.push( cached );
			} else {
				elementMatchers.push( cached );
			}
		}

		// Cache the compiled function
		cached = compilerCache( selector, matcherFromGroupMatchers( elementMatchers, setMatchers ) );

		// Save selector and tokenization
		cached.selector = selector;
	}
	return cached;
};

/**
 * A low-level selection function that works with Sizzle's compiled
 *  selector functions
 * @param {String|Function} selector A selector or a pre-compiled
 *  selector function built with Sizzle.compile
 * @param {Element} context
 * @param {Array} [results]
 * @param {Array} [seed] A set of elements to match against
 */
select = Sizzle.select = function( selector, context, results, seed ) {
	var i, tokens, token, type, find,
		compiled = typeof selector === "function" && selector,
		match = !seed && tokenize( (selector = compiled.selector || selector) );

	results = results || [];

	// Try to minimize operations if there is no seed and only one group
	if ( match.length === 1 ) {

		// Take a shortcut and set the context if the root selector is an ID
		tokens = match[0] = match[0].slice( 0 );
		if ( tokens.length > 2 && (token = tokens[0]).type === "ID" &&
				support.getById && context.nodeType === 9 && documentIsHTML &&
				Expr.relative[ tokens[1].type ] ) {

			context = ( Expr.find["ID"]( token.matches[0].replace(runescape, funescape), context ) || [] )[0];
			if ( !context ) {
				return results;

			// Precompiled matchers will still verify ancestry, so step up a level
			} else if ( compiled ) {
				context = context.parentNode;
			}

			selector = selector.slice( tokens.shift().value.length );
		}

		// Fetch a seed set for right-to-left matching
		i = matchExpr["needsContext"].test( selector ) ? 0 : tokens.length;
		while ( i-- ) {
			token = tokens[i];

			// Abort if we hit a combinator
			if ( Expr.relative[ (type = token.type) ] ) {
				break;
			}
			if ( (find = Expr.find[ type ]) ) {
				// Search, expanding context for leading sibling combinators
				if ( (seed = find(
					token.matches[0].replace( runescape, funescape ),
					rsibling.test( tokens[0].type ) && testContext( context.parentNode ) || context
				)) ) {

					// If seed is empty or no tokens remain, we can return early
					tokens.splice( i, 1 );
					selector = seed.length && toSelector( tokens );
					if ( !selector ) {
						push.apply( results, seed );
						return results;
					}

					break;
				}
			}
		}
	}

	// Compile and execute a filtering function if one is not provided
	// Provide `match` to avoid retokenization if we modified the selector above
	( compiled || compile( selector, match ) )(
		seed,
		context,
		!documentIsHTML,
		results,
		rsibling.test( selector ) && testContext( context.parentNode ) || context
	);
	return results;
};

// One-time assignments

// Sort stability
support.sortStable = expando.split("").sort( sortOrder ).join("") === expando;

// Support: Chrome 14-35+
// Always assume duplicates if they aren't passed to the comparison function
support.detectDuplicates = !!hasDuplicate;

// Initialize against the default document
setDocument();

// Support: Webkit<537.32 - Safari 6.0.3/Chrome 25 (fixed in Chrome 27)
// Detached nodes confoundingly follow *each other*
support.sortDetached = assert(function( div1 ) {
	// Should return 1, but returns 4 (following)
	return div1.compareDocumentPosition( document.createElement("div") ) & 1;
});

// Support: IE<8
// Prevent attribute/property "interpolation"
// http://msdn.microsoft.com/en-us/library/ms536429%28VS.85%29.aspx
if ( !assert(function( div ) {
	div.innerHTML = "<a href='#'></a>";
	return div.firstChild.getAttribute("href") === "#" ;
}) ) {
	addHandle( "type|href|height|width", function( elem, name, isXML ) {
		if ( !isXML ) {
			return elem.getAttribute( name, name.toLowerCase() === "type" ? 1 : 2 );
		}
	});
}

// Support: IE<9
// Use defaultValue in place of getAttribute("value")
if ( !support.attributes || !assert(function( div ) {
	div.innerHTML = "<input/>";
	div.firstChild.setAttribute( "value", "" );
	return div.firstChild.getAttribute( "value" ) === "";
}) ) {
	addHandle( "value", function( elem, name, isXML ) {
		if ( !isXML && elem.nodeName.toLowerCase() === "input" ) {
			return elem.defaultValue;
		}
	});
}

// Support: IE<9
// Use getAttributeNode to fetch booleans when getAttribute lies
if ( !assert(function( div ) {
	return div.getAttribute("disabled") == null;
}) ) {
	addHandle( booleans, function( elem, name, isXML ) {
		var val;
		if ( !isXML ) {
			return elem[ name ] === true ? name.toLowerCase() :
					(val = elem.getAttributeNode( name )) && val.specified ?
					val.value :
				null;
		}
	});
}

return Sizzle;

})( window );



jQuery.find = Sizzle;
jQuery.expr = Sizzle.selectors;
jQuery.expr[":"] = jQuery.expr.pseudos;
jQuery.unique = Sizzle.uniqueSort;
jQuery.text = Sizzle.getText;
jQuery.isXMLDoc = Sizzle.isXML;
jQuery.contains = Sizzle.contains;



var rneedsContext = jQuery.expr.match.needsContext;

var rsingleTag = (/^<(\w+)\s*\/?>(?:<\/\1>|)$/);



var risSimple = /^.[^:#\[\.,]*$/;

// Implement the identical functionality for filter and not
function winnow( elements, qualifier, not ) {
	if ( jQuery.isFunction( qualifier ) ) {
		return jQuery.grep( elements, function( elem, i ) {
			/* jshint -W018 */
			return !!qualifier.call( elem, i, elem ) !== not;
		});

	}

	if ( qualifier.nodeType ) {
		return jQuery.grep( elements, function( elem ) {
			return ( elem === qualifier ) !== not;
		});

	}

	if ( typeof qualifier === "string" ) {
		if ( risSimple.test( qualifier ) ) {
			return jQuery.filter( qualifier, elements, not );
		}

		qualifier = jQuery.filter( qualifier, elements );
	}

	return jQuery.grep( elements, function( elem ) {
		return ( indexOf.call( qualifier, elem ) >= 0 ) !== not;
	});
}

jQuery.filter = function( expr, elems, not ) {
	var elem = elems[ 0 ];

	if ( not ) {
		expr = ":not(" + expr + ")";
	}

	return elems.length === 1 && elem.nodeType === 1 ?
		jQuery.find.matchesSelector( elem, expr ) ? [ elem ] : [] :
		jQuery.find.matches( expr, jQuery.grep( elems, function( elem ) {
			return elem.nodeType === 1;
		}));
};

jQuery.fn.extend({
	find: function( selector ) {
		var i,
			len = this.length,
			ret = [],
			self = this;

		if ( typeof selector !== "string" ) {
			return this.pushStack( jQuery( selector ).filter(function() {
				for ( i = 0; i < len; i++ ) {
					if ( jQuery.contains( self[ i ], this ) ) {
						return true;
					}
				}
			}) );
		}

		for ( i = 0; i < len; i++ ) {
			jQuery.find( selector, self[ i ], ret );
		}

		// Needed because $( selector, context ) becomes $( context ).find( selector )
		ret = this.pushStack( len > 1 ? jQuery.unique( ret ) : ret );
		ret.selector = this.selector ? this.selector + " " + selector : selector;
		return ret;
	},
	filter: function( selector ) {
		return this.pushStack( winnow(this, selector || [], false) );
	},
	not: function( selector ) {
		return this.pushStack( winnow(this, selector || [], true) );
	},
	is: function( selector ) {
		return !!winnow(
			this,

			// If this is a positional/relative selector, check membership in the returned set
			// so $("p:first").is("p:last") won't return true for a doc with two "p".
			typeof selector === "string" && rneedsContext.test( selector ) ?
				jQuery( selector ) :
				selector || [],
			false
		).length;
	}
});


// Initialize a jQuery object


// A central reference to the root jQuery(document)
var rootjQuery,

	// A simple way to check for HTML strings
	// Prioritize #id over <tag> to avoid XSS via location.hash (#9521)
	// Strict HTML recognition (#11290: must start with <)
	rquickExpr = /^(?:\s*(<[\w\W]+>)[^>]*|#([\w-]*))$/,

	init = jQuery.fn.init = function( selector, context ) {
		var match, elem;

		// HANDLE: $(""), $(null), $(undefined), $(false)
		if ( !selector ) {
			return this;
		}

		// Handle HTML strings
		if ( typeof selector === "string" ) {
			if ( selector[0] === "<" && selector[ selector.length - 1 ] === ">" && selector.length >= 3 ) {
				// Assume that strings that start and end with <> are HTML and skip the regex check
				match = [ null, selector, null ];

			} else {
				match = rquickExpr.exec( selector );
			}

			// Match html or make sure no context is specified for #id
			if ( match && (match[1] || !context) ) {

				// HANDLE: $(html) -> $(array)
				if ( match[1] ) {
					context = context instanceof jQuery ? context[0] : context;

					// Option to run scripts is true for back-compat
					// Intentionally let the error be thrown if parseHTML is not present
					jQuery.merge( this, jQuery.parseHTML(
						match[1],
						context && context.nodeType ? context.ownerDocument || context : document,
						true
					) );

					// HANDLE: $(html, props)
					if ( rsingleTag.test( match[1] ) && jQuery.isPlainObject( context ) ) {
						for ( match in context ) {
							// Properties of context are called as methods if possible
							if ( jQuery.isFunction( this[ match ] ) ) {
								this[ match ]( context[ match ] );

							// ...and otherwise set as attributes
							} else {
								this.attr( match, context[ match ] );
							}
						}
					}

					return this;

				// HANDLE: $(#id)
				} else {
					elem = document.getElementById( match[2] );

					// Support: Blackberry 4.6
					// gEBID returns nodes no longer in the document (#6963)
					if ( elem && elem.parentNode ) {
						// Inject the element directly into the jQuery object
						this.length = 1;
						this[0] = elem;
					}

					this.context = document;
					this.selector = selector;
					return this;
				}

			// HANDLE: $(expr, $(...))
			} else if ( !context || context.jquery ) {
				return ( context || rootjQuery ).find( selector );

			// HANDLE: $(expr, context)
			// (which is just equivalent to: $(context).find(expr)
			} else {
				return this.constructor( context ).find( selector );
			}

		// HANDLE: $(DOMElement)
		} else if ( selector.nodeType ) {
			this.context = this[0] = selector;
			this.length = 1;
			return this;

		// HANDLE: $(function)
		// Shortcut for document ready
		} else if ( jQuery.isFunction( selector ) ) {
			return typeof rootjQuery.ready !== "undefined" ?
				rootjQuery.ready( selector ) :
				// Execute immediately if ready is not present
				selector( jQuery );
		}

		if ( selector.selector !== undefined ) {
			this.selector = selector.selector;
			this.context = selector.context;
		}

		return jQuery.makeArray( selector, this );
	};

// Give the init function the jQuery prototype for later instantiation
init.prototype = jQuery.fn;

// Initialize central reference
rootjQuery = jQuery( document );


var rparentsprev = /^(?:parents|prev(?:Until|All))/,
	// Methods guaranteed to produce a unique set when starting from a unique set
	guaranteedUnique = {
		children: true,
		contents: true,
		next: true,
		prev: true
	};

jQuery.extend({
	dir: function( elem, dir, until ) {
		var matched = [],
			truncate = until !== undefined;

		while ( (elem = elem[ dir ]) && elem.nodeType !== 9 ) {
			if ( elem.nodeType === 1 ) {
				if ( truncate && jQuery( elem ).is( until ) ) {
					break;
				}
				matched.push( elem );
			}
		}
		return matched;
	},

	sibling: function( n, elem ) {
		var matched = [];

		for ( ; n; n = n.nextSibling ) {
			if ( n.nodeType === 1 && n !== elem ) {
				matched.push( n );
			}
		}

		return matched;
	}
});

jQuery.fn.extend({
	has: function( target ) {
		var targets = jQuery( target, this ),
			l = targets.length;

		return this.filter(function() {
			var i = 0;
			for ( ; i < l; i++ ) {
				if ( jQuery.contains( this, targets[i] ) ) {
					return true;
				}
			}
		});
	},

	closest: function( selectors, context ) {
		var cur,
			i = 0,
			l = this.length,
			matched = [],
			pos = rneedsContext.test( selectors ) || typeof selectors !== "string" ?
				jQuery( selectors, context || this.context ) :
				0;

		for ( ; i < l; i++ ) {
			for ( cur = this[i]; cur && cur !== context; cur = cur.parentNode ) {
				// Always skip document fragments
				if ( cur.nodeType < 11 && (pos ?
					pos.index(cur) > -1 :

					// Don't pass non-elements to Sizzle
					cur.nodeType === 1 &&
						jQuery.find.matchesSelector(cur, selectors)) ) {

					matched.push( cur );
					break;
				}
			}
		}

		return this.pushStack( matched.length > 1 ? jQuery.unique( matched ) : matched );
	},

	// Determine the position of an element within the set
	index: function( elem ) {

		// No argument, return index in parent
		if ( !elem ) {
			return ( this[ 0 ] && this[ 0 ].parentNode ) ? this.first().prevAll().length : -1;
		}

		// Index in selector
		if ( typeof elem === "string" ) {
			return indexOf.call( jQuery( elem ), this[ 0 ] );
		}

		// Locate the position of the desired element
		return indexOf.call( this,

			// If it receives a jQuery object, the first element is used
			elem.jquery ? elem[ 0 ] : elem
		);
	},

	add: function( selector, context ) {
		return this.pushStack(
			jQuery.unique(
				jQuery.merge( this.get(), jQuery( selector, context ) )
			)
		);
	},

	addBack: function( selector ) {
		return this.add( selector == null ?
			this.prevObject : this.prevObject.filter(selector)
		);
	}
});

function sibling( cur, dir ) {
	while ( (cur = cur[dir]) && cur.nodeType !== 1 ) {}
	return cur;
}

jQuery.each({
	parent: function( elem ) {
		var parent = elem.parentNode;
		return parent && parent.nodeType !== 11 ? parent : null;
	},
	parents: function( elem ) {
		return jQuery.dir( elem, "parentNode" );
	},
	parentsUntil: function( elem, i, until ) {
		return jQuery.dir( elem, "parentNode", until );
	},
	next: function( elem ) {
		return sibling( elem, "nextSibling" );
	},
	prev: function( elem ) {
		return sibling( elem, "previousSibling" );
	},
	nextAll: function( elem ) {
		return jQuery.dir( elem, "nextSibling" );
	},
	prevAll: function( elem ) {
		return jQuery.dir( elem, "previousSibling" );
	},
	nextUntil: function( elem, i, until ) {
		return jQuery.dir( elem, "nextSibling", until );
	},
	prevUntil: function( elem, i, until ) {
		return jQuery.dir( elem, "previousSibling", until );
	},
	siblings: function( elem ) {
		return jQuery.sibling( ( elem.parentNode || {} ).firstChild, elem );
	},
	children: function( elem ) {
		return jQuery.sibling( elem.firstChild );
	},
	contents: function( elem ) {
		return elem.contentDocument || jQuery.merge( [], elem.childNodes );
	}
}, function( name, fn ) {
	jQuery.fn[ name ] = function( until, selector ) {
		var matched = jQuery.map( this, fn, until );

		if ( name.slice( -5 ) !== "Until" ) {
			selector = until;
		}

		if ( selector && typeof selector === "string" ) {
			matched = jQuery.filter( selector, matched );
		}

		if ( this.length > 1 ) {
			// Remove duplicates
			if ( !guaranteedUnique[ name ] ) {
				jQuery.unique( matched );
			}

			// Reverse order for parents* and prev-derivatives
			if ( rparentsprev.test( name ) ) {
				matched.reverse();
			}
		}

		return this.pushStack( matched );
	};
});
var rnotwhite = (/\S+/g);



// String to Object options format cache
var optionsCache = {};

// Convert String-formatted options into Object-formatted ones and store in cache
function createOptions( options ) {
	var object = optionsCache[ options ] = {};
	jQuery.each( options.match( rnotwhite ) || [], function( _, flag ) {
		object[ flag ] = true;
	});
	return object;
}

/*
 * Create a callback list using the following parameters:
 *
 *	options: an optional list of space-separated options that will change how
 *			the callback list behaves or a more traditional option object
 *
 * By default a callback list will act like an event callback list and can be
 * "fired" multiple times.
 *
 * Possible options:
 *
 *	once:			will ensure the callback list can only be fired once (like a Deferred)
 *
 *	memory:			will keep track of previous values and will call any callback added
 *					after the list has been fired right away with the latest "memorized"
 *					values (like a Deferred)
 *
 *	unique:			will ensure a callback can only be added once (no duplicate in the list)
 *
 *	stopOnFalse:	interrupt callings when a callback returns false
 *
 */
jQuery.Callbacks = function( options ) {

	// Convert options from String-formatted to Object-formatted if needed
	// (we check in cache first)
	options = typeof options === "string" ?
		( optionsCache[ options ] || createOptions( options ) ) :
		jQuery.extend( {}, options );

	var // Last fire value (for non-forgettable lists)
		memory,
		// Flag to know if list was already fired
		fired,
		// Flag to know if list is currently firing
		firing,
		// First callback to fire (used internally by add and fireWith)
		firingStart,
		// End of the loop when firing
		firingLength,
		// Index of currently firing callback (modified by remove if needed)
		firingIndex,
		// Actual callback list
		list = [],
		// Stack of fire calls for repeatable lists
		stack = !options.once && [],
		// Fire callbacks
		fire = function( data ) {
			memory = options.memory && data;
			fired = true;
			firingIndex = firingStart || 0;
			firingStart = 0;
			firingLength = list.length;
			firing = true;
			for ( ; list && firingIndex < firingLength; firingIndex++ ) {
				if ( list[ firingIndex ].apply( data[ 0 ], data[ 1 ] ) === false && options.stopOnFalse ) {
					memory = false; // To prevent further calls using add
					break;
				}
			}
			firing = false;
			if ( list ) {
				if ( stack ) {
					if ( stack.length ) {
						fire( stack.shift() );
					}
				} else if ( memory ) {
					list = [];
				} else {
					self.disable();
				}
			}
		},
		// Actual Callbacks object
		self = {
			// Add a callback or a collection of callbacks to the list
			add: function() {
				if ( list ) {
					// First, we save the current length
					var start = list.length;
					(function add( args ) {
						jQuery.each( args, function( _, arg ) {
							var type = jQuery.type( arg );
							if ( type === "function" ) {
								if ( !options.unique || !self.has( arg ) ) {
									list.push( arg );
								}
							} else if ( arg && arg.length && type !== "string" ) {
								// Inspect recursively
								add( arg );
							}
						});
					})( arguments );
					// Do we need to add the callbacks to the
					// current firing batch?
					if ( firing ) {
						firingLength = list.length;
					// With memory, if we're not firing then
					// we should call right away
					} else if ( memory ) {
						firingStart = start;
						fire( memory );
					}
				}
				return this;
			},
			// Remove a callback from the list
			remove: function() {
				if ( list ) {
					jQuery.each( arguments, function( _, arg ) {
						var index;
						while ( ( index = jQuery.inArray( arg, list, index ) ) > -1 ) {
							list.splice( index, 1 );
							// Handle firing indexes
							if ( firing ) {
								if ( index <= firingLength ) {
									firingLength--;
								}
								if ( index <= firingIndex ) {
									firingIndex--;
								}
							}
						}
					});
				}
				return this;
			},
			// Check if a given callback is in the list.
			// If no argument is given, return whether or not list has callbacks attached.
			has: function( fn ) {
				return fn ? jQuery.inArray( fn, list ) > -1 : !!( list && list.length );
			},
			// Remove all callbacks from the list
			empty: function() {
				list = [];
				firingLength = 0;
				return this;
			},
			// Have the list do nothing anymore
			disable: function() {
				list = stack = memory = undefined;
				return this;
			},
			// Is it disabled?
			disabled: function() {
				return !list;
			},
			// Lock the list in its current state
			lock: function() {
				stack = undefined;
				if ( !memory ) {
					self.disable();
				}
				return this;
			},
			// Is it locked?
			locked: function() {
				return !stack;
			},
			// Call all callbacks with the given context and arguments
			fireWith: function( context, args ) {
				if ( list && ( !fired || stack ) ) {
					args = args || [];
					args = [ context, args.slice ? args.slice() : args ];
					if ( firing ) {
						stack.push( args );
					} else {
						fire( args );
					}
				}
				return this;
			},
			// Call all the callbacks with the given arguments
			fire: function() {
				self.fireWith( this, arguments );
				return this;
			},
			// To know if the callbacks have already been called at least once
			fired: function() {
				return !!fired;
			}
		};

	return self;
};


jQuery.extend({

	Deferred: function( func ) {
		var tuples = [
				// action, add listener, listener list, final state
				[ "resolve", "done", jQuery.Callbacks("once memory"), "resolved" ],
				[ "reject", "fail", jQuery.Callbacks("once memory"), "rejected" ],
				[ "notify", "progress", jQuery.Callbacks("memory") ]
			],
			state = "pending",
			promise = {
				state: function() {
					return state;
				},
				always: function() {
					deferred.done( arguments ).fail( arguments );
					return this;
				},
				then: function( /* fnDone, fnFail, fnProgress */ ) {
					var fns = arguments;
					return jQuery.Deferred(function( newDefer ) {
						jQuery.each( tuples, function( i, tuple ) {
							var fn = jQuery.isFunction( fns[ i ] ) && fns[ i ];
							// deferred[ done | fail | progress ] for forwarding actions to newDefer
							deferred[ tuple[1] ](function() {
								var returned = fn && fn.apply( this, arguments );
								if ( returned && jQuery.isFunction( returned.promise ) ) {
									returned.promise()
										.done( newDefer.resolve )
										.fail( newDefer.reject )
										.progress( newDefer.notify );
								} else {
									newDefer[ tuple[ 0 ] + "With" ]( this === promise ? newDefer.promise() : this, fn ? [ returned ] : arguments );
								}
							});
						});
						fns = null;
					}).promise();
				},
				// Get a promise for this deferred
				// If obj is provided, the promise aspect is added to the object
				promise: function( obj ) {
					return obj != null ? jQuery.extend( obj, promise ) : promise;
				}
			},
			deferred = {};

		// Keep pipe for back-compat
		promise.pipe = promise.then;

		// Add list-specific methods
		jQuery.each( tuples, function( i, tuple ) {
			var list = tuple[ 2 ],
				stateString = tuple[ 3 ];

			// promise[ done | fail | progress ] = list.add
			promise[ tuple[1] ] = list.add;

			// Handle state
			if ( stateString ) {
				list.add(function() {
					// state = [ resolved | rejected ]
					state = stateString;

				// [ reject_list | resolve_list ].disable; progress_list.lock
				}, tuples[ i ^ 1 ][ 2 ].disable, tuples[ 2 ][ 2 ].lock );
			}

			// deferred[ resolve | reject | notify ]
			deferred[ tuple[0] ] = function() {
				deferred[ tuple[0] + "With" ]( this === deferred ? promise : this, arguments );
				return this;
			};
			deferred[ tuple[0] + "With" ] = list.fireWith;
		});

		// Make the deferred a promise
		promise.promise( deferred );

		// Call given func if any
		if ( func ) {
			func.call( deferred, deferred );
		}

		// All done!
		return deferred;
	},

	// Deferred helper
	when: function( subordinate /* , ..., subordinateN */ ) {
		var i = 0,
			resolveValues = slice.call( arguments ),
			length = resolveValues.length,

			// the count of uncompleted subordinates
			remaining = length !== 1 || ( subordinate && jQuery.isFunction( subordinate.promise ) ) ? length : 0,

			// the master Deferred. If resolveValues consist of only a single Deferred, just use that.
			deferred = remaining === 1 ? subordinate : jQuery.Deferred(),

			// Update function for both resolve and progress values
			updateFunc = function( i, contexts, values ) {
				return function( value ) {
					contexts[ i ] = this;
					values[ i ] = arguments.length > 1 ? slice.call( arguments ) : value;
					if ( values === progressValues ) {
						deferred.notifyWith( contexts, values );
					} else if ( !( --remaining ) ) {
						deferred.resolveWith( contexts, values );
					}
				};
			},

			progressValues, progressContexts, resolveContexts;

		// Add listeners to Deferred subordinates; treat others as resolved
		if ( length > 1 ) {
			progressValues = new Array( length );
			progressContexts = new Array( length );
			resolveContexts = new Array( length );
			for ( ; i < length; i++ ) {
				if ( resolveValues[ i ] && jQuery.isFunction( resolveValues[ i ].promise ) ) {
					resolveValues[ i ].promise()
						.done( updateFunc( i, resolveContexts, resolveValues ) )
						.fail( deferred.reject )
						.progress( updateFunc( i, progressContexts, progressValues ) );
				} else {
					--remaining;
				}
			}
		}

		// If we're not waiting on anything, resolve the master
		if ( !remaining ) {
			deferred.resolveWith( resolveContexts, resolveValues );
		}

		return deferred.promise();
	}
});


// The deferred used on DOM ready
var readyList;

jQuery.fn.ready = function( fn ) {
	// Add the callback
	jQuery.ready.promise().done( fn );

	return this;
};

jQuery.extend({
	// Is the DOM ready to be used? Set to true once it occurs.
	isReady: false,

	// A counter to track how many items to wait for before
	// the ready event fires. See #6781
	readyWait: 1,

	// Hold (or release) the ready event
	holdReady: function( hold ) {
		if ( hold ) {
			jQuery.readyWait++;
		} else {
			jQuery.ready( true );
		}
	},

	// Handle when the DOM is ready
	ready: function( wait ) {

		// Abort if there are pending holds or we're already ready
		if ( wait === true ? --jQuery.readyWait : jQuery.isReady ) {
			return;
		}

		// Remember that the DOM is ready
		jQuery.isReady = true;

		// If a normal DOM Ready event fired, decrement, and wait if need be
		if ( wait !== true && --jQuery.readyWait > 0 ) {
			return;
		}

		// If there are functions bound, to execute
		readyList.resolveWith( document, [ jQuery ] );

		// Trigger any bound ready events
		if ( jQuery.fn.triggerHandler ) {
			jQuery( document ).triggerHandler( "ready" );
			jQuery( document ).off( "ready" );
		}
	}
});

/**
 * The ready event handler and self cleanup method
 */
function completed() {
	document.removeEventListener( "DOMContentLoaded", completed, false );
	window.removeEventListener( "load", completed, false );
	jQuery.ready();
}

jQuery.ready.promise = function( obj ) {
	if ( !readyList ) {

		readyList = jQuery.Deferred();

		// Catch cases where $(document).ready() is called after the browser event has already occurred.
		// We once tried to use readyState "interactive" here, but it caused issues like the one
		// discovered by ChrisS here: http://bugs.jquery.com/ticket/12282#comment:15
		if ( document.readyState === "complete" ) {
			// Handle it asynchronously to allow scripts the opportunity to delay ready
			setTimeout( jQuery.ready );

		} else {

			// Use the handy event callback
			document.addEventListener( "DOMContentLoaded", completed, false );

			// A fallback to window.onload, that will always work
			window.addEventListener( "load", completed, false );
		}
	}
	return readyList.promise( obj );
};

// Kick off the DOM ready check even if the user does not
jQuery.ready.promise();




// Multifunctional method to get and set values of a collection
// The value/s can optionally be executed if it's a function
var access = jQuery.access = function( elems, fn, key, value, chainable, emptyGet, raw ) {
	var i = 0,
		len = elems.length,
		bulk = key == null;

	// Sets many values
	if ( jQuery.type( key ) === "object" ) {
		chainable = true;
		for ( i in key ) {
			jQuery.access( elems, fn, i, key[i], true, emptyGet, raw );
		}

	// Sets one value
	} else if ( value !== undefined ) {
		chainable = true;

		if ( !jQuery.isFunction( value ) ) {
			raw = true;
		}

		if ( bulk ) {
			// Bulk operations run against the entire set
			if ( raw ) {
				fn.call( elems, value );
				fn = null;

			// ...except when executing function values
			} else {
				bulk = fn;
				fn = function( elem, key, value ) {
					return bulk.call( jQuery( elem ), value );
				};
			}
		}

		if ( fn ) {
			for ( ; i < len; i++ ) {
				fn( elems[i], key, raw ? value : value.call( elems[i], i, fn( elems[i], key ) ) );
			}
		}
	}

	return chainable ?
		elems :

		// Gets
		bulk ?
			fn.call( elems ) :
			len ? fn( elems[0], key ) : emptyGet;
};


/**
 * Determines whether an object can have data
 */
jQuery.acceptData = function( owner ) {
	// Accepts only:
	//  - Node
	//    - Node.ELEMENT_NODE
	//    - Node.DOCUMENT_NODE
	//  - Object
	//    - Any
	/* jshint -W018 */
	return owner.nodeType === 1 || owner.nodeType === 9 || !( +owner.nodeType );
};


function Data() {
	// Support: Android<4,
	// Old WebKit does not have Object.preventExtensions/freeze method,
	// return new empty object instead with no [[set]] accessor
	Object.defineProperty( this.cache = {}, 0, {
		get: function() {
			return {};
		}
	});

	this.expando = jQuery.expando + Data.uid++;
}

Data.uid = 1;
Data.accepts = jQuery.acceptData;

Data.prototype = {
	key: function( owner ) {
		// We can accept data for non-element nodes in modern browsers,
		// but we should not, see #8335.
		// Always return the key for a frozen object.
		if ( !Data.accepts( owner ) ) {
			return 0;
		}

		var descriptor = {},
			// Check if the owner object already has a cache key
			unlock = owner[ this.expando ];

		// If not, create one
		if ( !unlock ) {
			unlock = Data.uid++;

			// Secure it in a non-enumerable, non-writable property
			try {
				descriptor[ this.expando ] = { value: unlock };
				Object.defineProperties( owner, descriptor );

			// Support: Android<4
			// Fallback to a less secure definition
			} catch ( e ) {
				descriptor[ this.expando ] = unlock;
				jQuery.extend( owner, descriptor );
			}
		}

		// Ensure the cache object
		if ( !this.cache[ unlock ] ) {
			this.cache[ unlock ] = {};
		}

		return unlock;
	},
	set: function( owner, data, value ) {
		var prop,
			// There may be an unlock assigned to this node,
			// if there is no entry for this "owner", create one inline
			// and set the unlock as though an owner entry had always existed
			unlock = this.key( owner ),
			cache = this.cache[ unlock ];

		// Handle: [ owner, key, value ] args
		if ( typeof data === "string" ) {
			cache[ data ] = value;

		// Handle: [ owner, { properties } ] args
		} else {
			// Fresh assignments by object are shallow copied
			if ( jQuery.isEmptyObject( cache ) ) {
				jQuery.extend( this.cache[ unlock ], data );
			// Otherwise, copy the properties one-by-one to the cache object
			} else {
				for ( prop in data ) {
					cache[ prop ] = data[ prop ];
				}
			}
		}
		return cache;
	},
	get: function( owner, key ) {
		// Either a valid cache is found, or will be created.
		// New caches will be created and the unlock returned,
		// allowing direct access to the newly created
		// empty data object. A valid owner object must be provided.
		var cache = this.cache[ this.key( owner ) ];

		return key === undefined ?
			cache : cache[ key ];
	},
	access: function( owner, key, value ) {
		var stored;
		// In cases where either:
		//
		//   1. No key was specified
		//   2. A string key was specified, but no value provided
		//
		// Take the "read" path and allow the get method to determine
		// which value to return, respectively either:
		//
		//   1. The entire cache object
		//   2. The data stored at the key
		//
		if ( key === undefined ||
				((key && typeof key === "string") && value === undefined) ) {

			stored = this.get( owner, key );

			return stored !== undefined ?
				stored : this.get( owner, jQuery.camelCase(key) );
		}

		// [*]When the key is not a string, or both a key and value
		// are specified, set or extend (existing objects) with either:
		//
		//   1. An object of properties
		//   2. A key and value
		//
		this.set( owner, key, value );

		// Since the "set" path can have two possible entry points
		// return the expected data based on which path was taken[*]
		return value !== undefined ? value : key;
	},
	remove: function( owner, key ) {
		var i, name, camel,
			unlock = this.key( owner ),
			cache = this.cache[ unlock ];

		if ( key === undefined ) {
			this.cache[ unlock ] = {};

		} else {
			// Support array or space separated string of keys
			if ( jQuery.isArray( key ) ) {
				// If "name" is an array of keys...
				// When data is initially created, via ("key", "val") signature,
				// keys will be converted to camelCase.
				// Since there is no way to tell _how_ a key was added, remove
				// both plain key and camelCase key. #12786
				// This will only penalize the array argument path.
				name = key.concat( key.map( jQuery.camelCase ) );
			} else {
				camel = jQuery.camelCase( key );
				// Try the string as a key before any manipulation
				if ( key in cache ) {
					name = [ key, camel ];
				} else {
					// If a key with the spaces exists, use it.
					// Otherwise, create an array by matching non-whitespace
					name = camel;
					name = name in cache ?
						[ name ] : ( name.match( rnotwhite ) || [] );
				}
			}

			i = name.length;
			while ( i-- ) {
				delete cache[ name[ i ] ];
			}
		}
	},
	hasData: function( owner ) {
		return !jQuery.isEmptyObject(
			this.cache[ owner[ this.expando ] ] || {}
		);
	},
	discard: function( owner ) {
		if ( owner[ this.expando ] ) {
			delete this.cache[ owner[ this.expando ] ];
		}
	}
};
var data_priv = new Data();

var data_user = new Data();



//	Implementation Summary
//
//	1. Enforce API surface and semantic compatibility with 1.9.x branch
//	2. Improve the module's maintainability by reducing the storage
//		paths to a single mechanism.
//	3. Use the same single mechanism to support "private" and "user" data.
//	4. _Never_ expose "private" data to user code (TODO: Drop _data, _removeData)
//	5. Avoid exposing implementation details on user objects (eg. expando properties)
//	6. Provide a clear path for implementation upgrade to WeakMap in 2014

var rbrace = /^(?:\{[\w\W]*\}|\[[\w\W]*\])$/,
	rmultiDash = /([A-Z])/g;

function dataAttr( elem, key, data ) {
	var name;

	// If nothing was found internally, try to fetch any
	// data from the HTML5 data-* attribute
	if ( data === undefined && elem.nodeType === 1 ) {
		name = "data-" + key.replace( rmultiDash, "-$1" ).toLowerCase();
		data = elem.getAttribute( name );

		if ( typeof data === "string" ) {
			try {
				data = data === "true" ? true :
					data === "false" ? false :
					data === "null" ? null :
					// Only convert to a number if it doesn't change the string
					+data + "" === data ? +data :
					rbrace.test( data ) ? jQuery.parseJSON( data ) :
					data;
			} catch( e ) {}

			// Make sure we set the data so it isn't changed later
			data_user.set( elem, key, data );
		} else {
			data = undefined;
		}
	}
	return data;
}

jQuery.extend({
	hasData: function( elem ) {
		return data_user.hasData( elem ) || data_priv.hasData( elem );
	},

	data: function( elem, name, data ) {
		return data_user.access( elem, name, data );
	},

	removeData: function( elem, name ) {
		data_user.remove( elem, name );
	},

	// TODO: Now that all calls to _data and _removeData have been replaced
	// with direct calls to data_priv methods, these can be deprecated.
	_data: function( elem, name, data ) {
		return data_priv.access( elem, name, data );
	},

	_removeData: function( elem, name ) {
		data_priv.remove( elem, name );
	}
});

jQuery.fn.extend({
	data: function( key, value ) {
		var i, name, data,
			elem = this[ 0 ],
			attrs = elem && elem.attributes;

		// Gets all values
		if ( key === undefined ) {
			if ( this.length ) {
				data = data_user.get( elem );

				if ( elem.nodeType === 1 && !data_priv.get( elem, "hasDataAttrs" ) ) {
					i = attrs.length;
					while ( i-- ) {

						// Support: IE11+
						// The attrs elements can be null (#14894)
						if ( attrs[ i ] ) {
							name = attrs[ i ].name;
							if ( name.indexOf( "data-" ) === 0 ) {
								name = jQuery.camelCase( name.slice(5) );
								dataAttr( elem, name, data[ name ] );
							}
						}
					}
					data_priv.set( elem, "hasDataAttrs", true );
				}
			}

			return data;
		}

		// Sets multiple values
		if ( typeof key === "object" ) {
			return this.each(function() {
				data_user.set( this, key );
			});
		}

		return access( this, function( value ) {
			var data,
				camelKey = jQuery.camelCase( key );

			// The calling jQuery object (element matches) is not empty
			// (and therefore has an element appears at this[ 0 ]) and the
			// `value` parameter was not undefined. An empty jQuery object
			// will result in `undefined` for elem = this[ 0 ] which will
			// throw an exception if an attempt to read a data cache is made.
			if ( elem && value === undefined ) {
				// Attempt to get data from the cache
				// with the key as-is
				data = data_user.get( elem, key );
				if ( data !== undefined ) {
					return data;
				}

				// Attempt to get data from the cache
				// with the key camelized
				data = data_user.get( elem, camelKey );
				if ( data !== undefined ) {
					return data;
				}

				// Attempt to "discover" the data in
				// HTML5 custom data-* attrs
				data = dataAttr( elem, camelKey, undefined );
				if ( data !== undefined ) {
					return data;
				}

				// We tried really hard, but the data doesn't exist.
				return;
			}

			// Set the data...
			this.each(function() {
				// First, attempt to store a copy or reference of any
				// data that might've been store with a camelCased key.
				var data = data_user.get( this, camelKey );

				// For HTML5 data-* attribute interop, we have to
				// store property names with dashes in a camelCase form.
				// This might not apply to all properties...*
				data_user.set( this, camelKey, value );

				// *... In the case of properties that might _actually_
				// have dashes, we need to also store a copy of that
				// unchanged property.
				if ( key.indexOf("-") !== -1 && data !== undefined ) {
					data_user.set( this, key, value );
				}
			});
		}, null, value, arguments.length > 1, null, true );
	},

	removeData: function( key ) {
		return this.each(function() {
			data_user.remove( this, key );
		});
	}
});


jQuery.extend({
	queue: function( elem, type, data ) {
		var queue;

		if ( elem ) {
			type = ( type || "fx" ) + "queue";
			queue = data_priv.get( elem, type );

			// Speed up dequeue by getting out quickly if this is just a lookup
			if ( data ) {
				if ( !queue || jQuery.isArray( data ) ) {
					queue = data_priv.access( elem, type, jQuery.makeArray(data) );
				} else {
					queue.push( data );
				}
			}
			return queue || [];
		}
	},

	dequeue: function( elem, type ) {
		type = type || "fx";

		var queue = jQuery.queue( elem, type ),
			startLength = queue.length,
			fn = queue.shift(),
			hooks = jQuery._queueHooks( elem, type ),
			next = function() {
				jQuery.dequeue( elem, type );
			};

		// If the fx queue is dequeued, always remove the progress sentinel
		if ( fn === "inprogress" ) {
			fn = queue.shift();
			startLength--;
		}

		if ( fn ) {

			// Add a progress sentinel to prevent the fx queue from being
			// automatically dequeued
			if ( type === "fx" ) {
				queue.unshift( "inprogress" );
			}

			// Clear up the last queue stop function
			delete hooks.stop;
			fn.call( elem, next, hooks );
		}

		if ( !startLength && hooks ) {
			hooks.empty.fire();
		}
	},

	// Not public - generate a queueHooks object, or return the current one
	_queueHooks: function( elem, type ) {
		var key = type + "queueHooks";
		return data_priv.get( elem, key ) || data_priv.access( elem, key, {
			empty: jQuery.Callbacks("once memory").add(function() {
				data_priv.remove( elem, [ type + "queue", key ] );
			})
		});
	}
});

jQuery.fn.extend({
	queue: function( type, data ) {
		var setter = 2;

		if ( typeof type !== "string" ) {
			data = type;
			type = "fx";
			setter--;
		}

		if ( arguments.length < setter ) {
			return jQuery.queue( this[0], type );
		}

		return data === undefined ?
			this :
			this.each(function() {
				var queue = jQuery.queue( this, type, data );

				// Ensure a hooks for this queue
				jQuery._queueHooks( this, type );

				if ( type === "fx" && queue[0] !== "inprogress" ) {
					jQuery.dequeue( this, type );
				}
			});
	},
	dequeue: function( type ) {
		return this.each(function() {
			jQuery.dequeue( this, type );
		});
	},
	clearQueue: function( type ) {
		return this.queue( type || "fx", [] );
	},
	// Get a promise resolved when queues of a certain type
	// are emptied (fx is the type by default)
	promise: function( type, obj ) {
		var tmp,
			count = 1,
			defer = jQuery.Deferred(),
			elements = this,
			i = this.length,
			resolve = function() {
				if ( !( --count ) ) {
					defer.resolveWith( elements, [ elements ] );
				}
			};

		if ( typeof type !== "string" ) {
			obj = type;
			type = undefined;
		}
		type = type || "fx";

		while ( i-- ) {
			tmp = data_priv.get( elements[ i ], type + "queueHooks" );
			if ( tmp && tmp.empty ) {
				count++;
				tmp.empty.add( resolve );
			}
		}
		resolve();
		return defer.promise( obj );
	}
});
var pnum = (/[+-]?(?:\d*\.|)\d+(?:[eE][+-]?\d+|)/).source;

var cssExpand = [ "Top", "Right", "Bottom", "Left" ];

var isHidden = function( elem, el ) {
		// isHidden might be called from jQuery#filter function;
		// in that case, element will be second argument
		elem = el || elem;
		return jQuery.css( elem, "display" ) === "none" || !jQuery.contains( elem.ownerDocument, elem );
	};

var rcheckableType = (/^(?:checkbox|radio)$/i);



(function() {
	var fragment = document.createDocumentFragment(),
		div = fragment.appendChild( document.createElement( "div" ) ),
		input = document.createElement( "input" );

	// Support: Safari<=5.1
	// Check state lost if the name is set (#11217)
	// Support: Windows Web Apps (WWA)
	// `name` and `type` must use .setAttribute for WWA (#14901)
	input.setAttribute( "type", "radio" );
	input.setAttribute( "checked", "checked" );
	input.setAttribute( "name", "t" );

	div.appendChild( input );

	// Support: Safari<=5.1, Android<4.2
	// Older WebKit doesn't clone checked state correctly in fragments
	support.checkClone = div.cloneNode( true ).cloneNode( true ).lastChild.checked;

	// Support: IE<=11+
	// Make sure textarea (and checkbox) defaultValue is properly cloned
	div.innerHTML = "<textarea>x</textarea>";
	support.noCloneChecked = !!div.cloneNode( true ).lastChild.defaultValue;
})();
var strundefined = typeof undefined;



support.focusinBubbles = "onfocusin" in window;


var
	rkeyEvent = /^key/,
	rmouseEvent = /^(?:mouse|pointer|contextmenu)|click/,
	rfocusMorph = /^(?:focusinfocus|focusoutblur)$/,
	rtypenamespace = /^([^.]*)(?:\.(.+)|)$/;

function returnTrue() {
	return true;
}

function returnFalse() {
	return false;
}

function safeActiveElement() {
	try {
		return document.activeElement;
	} catch ( err ) { }
}

/*
 * Helper functions for managing events -- not part of the public interface.
 * Props to Dean Edwards' addEvent library for many of the ideas.
 */
jQuery.event = {

	global: {},

	add: function( elem, types, handler, data, selector ) {

		var handleObjIn, eventHandle, tmp,
			events, t, handleObj,
			special, handlers, type, namespaces, origType,
			elemData = data_priv.get( elem );

		// Don't attach events to noData or text/comment nodes (but allow plain objects)
		if ( !elemData ) {
			return;
		}

		// Caller can pass in an object of custom data in lieu of the handler
		if ( handler.handler ) {
			handleObjIn = handler;
			handler = handleObjIn.handler;
			selector = handleObjIn.selector;
		}

		// Make sure that the handler has a unique ID, used to find/remove it later
		if ( !handler.guid ) {
			handler.guid = jQuery.guid++;
		}

		// Init the element's event structure and main handler, if this is the first
		if ( !(events = elemData.events) ) {
			events = elemData.events = {};
		}
		if ( !(eventHandle = elemData.handle) ) {
			eventHandle = elemData.handle = function( e ) {
				// Discard the second event of a jQuery.event.trigger() and
				// when an event is called after a page has unloaded
				return typeof jQuery !== strundefined && jQuery.event.triggered !== e.type ?
					jQuery.event.dispatch.apply( elem, arguments ) : undefined;
			};
		}

		// Handle multiple events separated by a space
		types = ( types || "" ).match( rnotwhite ) || [ "" ];
		t = types.length;
		while ( t-- ) {
			tmp = rtypenamespace.exec( types[t] ) || [];
			type = origType = tmp[1];
			namespaces = ( tmp[2] || "" ).split( "." ).sort();

			// There *must* be a type, no attaching namespace-only handlers
			if ( !type ) {
				continue;
			}

			// If event changes its type, use the special event handlers for the changed type
			special = jQuery.event.special[ type ] || {};

			// If selector defined, determine special event api type, otherwise given type
			type = ( selector ? special.delegateType : special.bindType ) || type;

			// Update special based on newly reset type
			special = jQuery.event.special[ type ] || {};

			// handleObj is passed to all event handlers
			handleObj = jQuery.extend({
				type: type,
				origType: origType,
				data: data,
				handler: handler,
				guid: handler.guid,
				selector: selector,
				needsContext: selector && jQuery.expr.match.needsContext.test( selector ),
				namespace: namespaces.join(".")
			}, handleObjIn );

			// Init the event handler queue if we're the first
			if ( !(handlers = events[ type ]) ) {
				handlers = events[ type ] = [];
				handlers.delegateCount = 0;

				// Only use addEventListener if the special events handler returns false
				if ( !special.setup || special.setup.call( elem, data, namespaces, eventHandle ) === false ) {
					if ( elem.addEventListener ) {
						elem.addEventListener( type, eventHandle, false );
					}
				}
			}

			if ( special.add ) {
				special.add.call( elem, handleObj );

				if ( !handleObj.handler.guid ) {
					handleObj.handler.guid = handler.guid;
				}
			}

			// Add to the element's handler list, delegates in front
			if ( selector ) {
				handlers.splice( handlers.delegateCount++, 0, handleObj );
			} else {
				handlers.push( handleObj );
			}

			// Keep track of which events have ever been used, for event optimization
			jQuery.event.global[ type ] = true;
		}

	},

	// Detach an event or set of events from an element
	remove: function( elem, types, handler, selector, mappedTypes ) {

		var j, origCount, tmp,
			events, t, handleObj,
			special, handlers, type, namespaces, origType,
			elemData = data_priv.hasData( elem ) && data_priv.get( elem );

		if ( !elemData || !(events = elemData.events) ) {
			return;
		}

		// Once for each type.namespace in types; type may be omitted
		types = ( types || "" ).match( rnotwhite ) || [ "" ];
		t = types.length;
		while ( t-- ) {
			tmp = rtypenamespace.exec( types[t] ) || [];
			type = origType = tmp[1];
			namespaces = ( tmp[2] || "" ).split( "." ).sort();

			// Unbind all events (on this namespace, if provided) for the element
			if ( !type ) {
				for ( type in events ) {
					jQuery.event.remove( elem, type + types[ t ], handler, selector, true );
				}
				continue;
			}

			special = jQuery.event.special[ type ] || {};
			type = ( selector ? special.delegateType : special.bindType ) || type;
			handlers = events[ type ] || [];
			tmp = tmp[2] && new RegExp( "(^|\\.)" + namespaces.join("\\.(?:.*\\.|)") + "(\\.|$)" );

			// Remove matching events
			origCount = j = handlers.length;
			while ( j-- ) {
				handleObj = handlers[ j ];

				if ( ( mappedTypes || origType === handleObj.origType ) &&
					( !handler || handler.guid === handleObj.guid ) &&
					( !tmp || tmp.test( handleObj.namespace ) ) &&
					( !selector || selector === handleObj.selector || selector === "**" && handleObj.selector ) ) {
					handlers.splice( j, 1 );

					if ( handleObj.selector ) {
						handlers.delegateCount--;
					}
					if ( special.remove ) {
						special.remove.call( elem, handleObj );
					}
				}
			}

			// Remove generic event handler if we removed something and no more handlers exist
			// (avoids potential for endless recursion during removal of special event handlers)
			if ( origCount && !handlers.length ) {
				if ( !special.teardown || special.teardown.call( elem, namespaces, elemData.handle ) === false ) {
					jQuery.removeEvent( elem, type, elemData.handle );
				}

				delete events[ type ];
			}
		}

		// Remove the expando if it's no longer used
		if ( jQuery.isEmptyObject( events ) ) {
			delete elemData.handle;
			data_priv.remove( elem, "events" );
		}
	},

	trigger: function( event, data, elem, onlyHandlers ) {

		var i, cur, tmp, bubbleType, ontype, handle, special,
			eventPath = [ elem || document ],
			type = hasOwn.call( event, "type" ) ? event.type : event,
			namespaces = hasOwn.call( event, "namespace" ) ? event.namespace.split(".") : [];

		cur = tmp = elem = elem || document;

		// Don't do events on text and comment nodes
		if ( elem.nodeType === 3 || elem.nodeType === 8 ) {
			return;
		}

		// focus/blur morphs to focusin/out; ensure we're not firing them right now
		if ( rfocusMorph.test( type + jQuery.event.triggered ) ) {
			return;
		}

		if ( type.indexOf(".") >= 0 ) {
			// Namespaced trigger; create a regexp to match event type in handle()
			namespaces = type.split(".");
			type = namespaces.shift();
			namespaces.sort();
		}
		ontype = type.indexOf(":") < 0 && "on" + type;

		// Caller can pass in a jQuery.Event object, Object, or just an event type string
		event = event[ jQuery.expando ] ?
			event :
			new jQuery.Event( type, typeof event === "object" && event );

		// Trigger bitmask: & 1 for native handlers; & 2 for jQuery (always true)
		event.isTrigger = onlyHandlers ? 2 : 3;
		event.namespace = namespaces.join(".");
		event.namespace_re = event.namespace ?
			new RegExp( "(^|\\.)" + namespaces.join("\\.(?:.*\\.|)") + "(\\.|$)" ) :
			null;

		// Clean up the event in case it is being reused
		event.result = undefined;
		if ( !event.target ) {
			event.target = elem;
		}

		// Clone any incoming data and prepend the event, creating the handler arg list
		data = data == null ?
			[ event ] :
			jQuery.makeArray( data, [ event ] );

		// Allow special events to draw outside the lines
		special = jQuery.event.special[ type ] || {};
		if ( !onlyHandlers && special.trigger && special.trigger.apply( elem, data ) === false ) {
			return;
		}

		// Determine event propagation path in advance, per W3C events spec (#9951)
		// Bubble up to document, then to window; watch for a global ownerDocument var (#9724)
		if ( !onlyHandlers && !special.noBubble && !jQuery.isWindow( elem ) ) {

			bubbleType = special.delegateType || type;
			if ( !rfocusMorph.test( bubbleType + type ) ) {
				cur = cur.parentNode;
			}
			for ( ; cur; cur = cur.parentNode ) {
				eventPath.push( cur );
				tmp = cur;
			}

			// Only add window if we got to document (e.g., not plain obj or detached DOM)
			if ( tmp === (elem.ownerDocument || document) ) {
				eventPath.push( tmp.defaultView || tmp.parentWindow || window );
			}
		}

		// Fire handlers on the event path
		i = 0;
		while ( (cur = eventPath[i++]) && !event.isPropagationStopped() ) {

			event.type = i > 1 ?
				bubbleType :
				special.bindType || type;

			// jQuery handler
			handle = ( data_priv.get( cur, "events" ) || {} )[ event.type ] && data_priv.get( cur, "handle" );
			if ( handle ) {
				handle.apply( cur, data );
			}

			// Native handler
			handle = ontype && cur[ ontype ];
			if ( handle && handle.apply && jQuery.acceptData( cur ) ) {
				event.result = handle.apply( cur, data );
				if ( event.result === false ) {
					event.preventDefault();
				}
			}
		}
		event.type = type;

		// If nobody prevented the default action, do it now
		if ( !onlyHandlers && !event.isDefaultPrevented() ) {

			if ( (!special._default || special._default.apply( eventPath.pop(), data ) === false) &&
				jQuery.acceptData( elem ) ) {

				// Call a native DOM method on the target with the same name name as the event.
				// Don't do default actions on window, that's where global variables be (#6170)
				if ( ontype && jQuery.isFunction( elem[ type ] ) && !jQuery.isWindow( elem ) ) {

					// Don't re-trigger an onFOO event when we call its FOO() method
					tmp = elem[ ontype ];

					if ( tmp ) {
						elem[ ontype ] = null;
					}

					// Prevent re-triggering of the same event, since we already bubbled it above
					jQuery.event.triggered = type;
					elem[ type ]();
					jQuery.event.triggered = undefined;

					if ( tmp ) {
						elem[ ontype ] = tmp;
					}
				}
			}
		}

		return event.result;
	},

	dispatch: function( event ) {

		// Make a writable jQuery.Event from the native event object
		event = jQuery.event.fix( event );

		var i, j, ret, matched, handleObj,
			handlerQueue = [],
			args = slice.call( arguments ),
			handlers = ( data_priv.get( this, "events" ) || {} )[ event.type ] || [],
			special = jQuery.event.special[ event.type ] || {};

		// Use the fix-ed jQuery.Event rather than the (read-only) native event
		args[0] = event;
		event.delegateTarget = this;

		// Call the preDispatch hook for the mapped type, and let it bail if desired
		if ( special.preDispatch && special.preDispatch.call( this, event ) === false ) {
			return;
		}

		// Determine handlers
		handlerQueue = jQuery.event.handlers.call( this, event, handlers );

		// Run delegates first; they may want to stop propagation beneath us
		i = 0;
		while ( (matched = handlerQueue[ i++ ]) && !event.isPropagationStopped() ) {
			event.currentTarget = matched.elem;

			j = 0;
			while ( (handleObj = matched.handlers[ j++ ]) && !event.isImmediatePropagationStopped() ) {

				// Triggered event must either 1) have no namespace, or 2) have namespace(s)
				// a subset or equal to those in the bound event (both can have no namespace).
				if ( !event.namespace_re || event.namespace_re.test( handleObj.namespace ) ) {

					event.handleObj = handleObj;
					event.data = handleObj.data;

					ret = ( (jQuery.event.special[ handleObj.origType ] || {}).handle || handleObj.handler )
							.apply( matched.elem, args );

					if ( ret !== undefined ) {
						if ( (event.result = ret) === false ) {
							event.preventDefault();
							event.stopPropagation();
						}
					}
				}
			}
		}

		// Call the postDispatch hook for the mapped type
		if ( special.postDispatch ) {
			special.postDispatch.call( this, event );
		}

		return event.result;
	},

	handlers: function( event, handlers ) {
		var i, matches, sel, handleObj,
			handlerQueue = [],
			delegateCount = handlers.delegateCount,
			cur = event.target;

		// Find delegate handlers
		// Black-hole SVG <use> instance trees (#13180)
		// Avoid non-left-click bubbling in Firefox (#3861)
		if ( delegateCount && cur.nodeType && (!event.button || event.type !== "click") ) {

			for ( ; cur !== this; cur = cur.parentNode || this ) {

				// Don't process clicks on disabled elements (#6911, #8165, #11382, #11764)
				if ( cur.disabled !== true || event.type !== "click" ) {
					matches = [];
					for ( i = 0; i < delegateCount; i++ ) {
						handleObj = handlers[ i ];

						// Don't conflict with Object.prototype properties (#13203)
						sel = handleObj.selector + " ";

						if ( matches[ sel ] === undefined ) {
							matches[ sel ] = handleObj.needsContext ?
								jQuery( sel, this ).index( cur ) >= 0 :
								jQuery.find( sel, this, null, [ cur ] ).length;
						}
						if ( matches[ sel ] ) {
							matches.push( handleObj );
						}
					}
					if ( matches.length ) {
						handlerQueue.push({ elem: cur, handlers: matches });
					}
				}
			}
		}

		// Add the remaining (directly-bound) handlers
		if ( delegateCount < handlers.length ) {
			handlerQueue.push({ elem: this, handlers: handlers.slice( delegateCount ) });
		}

		return handlerQueue;
	},

	// Includes some event props shared by KeyEvent and MouseEvent
	props: "altKey bubbles cancelable ctrlKey currentTarget eventPhase metaKey relatedTarget shiftKey target timeStamp view which".split(" "),

	fixHooks: {},

	keyHooks: {
		props: "char charCode key keyCode".split(" "),
		filter: function( event, original ) {

			// Add which for key events
			if ( event.which == null ) {
				event.which = original.charCode != null ? original.charCode : original.keyCode;
			}

			return event;
		}
	},

	mouseHooks: {
		props: "button buttons clientX clientY offsetX offsetY pageX pageY screenX screenY toElement".split(" "),
		filter: function( event, original ) {
			var eventDoc, doc, body,
				button = original.button;

			// Calculate pageX/Y if missing and clientX/Y available
			if ( event.pageX == null && original.clientX != null ) {
				eventDoc = event.target.ownerDocument || document;
				doc = eventDoc.documentElement;
				body = eventDoc.body;

				event.pageX = original.clientX + ( doc && doc.scrollLeft || body && body.scrollLeft || 0 ) - ( doc && doc.clientLeft || body && body.clientLeft || 0 );
				event.pageY = original.clientY + ( doc && doc.scrollTop  || body && body.scrollTop  || 0 ) - ( doc && doc.clientTop  || body && body.clientTop  || 0 );
			}

			// Add which for click: 1 === left; 2 === middle; 3 === right
			// Note: button is not normalized, so don't use it
			if ( !event.which && button !== undefined ) {
				event.which = ( button & 1 ? 1 : ( button & 2 ? 3 : ( button & 4 ? 2 : 0 ) ) );
			}

			return event;
		}
	},

	fix: function( event ) {
		if ( event[ jQuery.expando ] ) {
			return event;
		}

		// Create a writable copy of the event object and normalize some properties
		var i, prop, copy,
			type = event.type,
			originalEvent = event,
			fixHook = this.fixHooks[ type ];

		if ( !fixHook ) {
			this.fixHooks[ type ] = fixHook =
				rmouseEvent.test( type ) ? this.mouseHooks :
				rkeyEvent.test( type ) ? this.keyHooks :
				{};
		}
		copy = fixHook.props ? this.props.concat( fixHook.props ) : this.props;

		event = new jQuery.Event( originalEvent );

		i = copy.length;
		while ( i-- ) {
			prop = copy[ i ];
			event[ prop ] = originalEvent[ prop ];
		}

		// Support: Cordova 2.5 (WebKit) (#13255)
		// All events should have a target; Cordova deviceready doesn't
		if ( !event.target ) {
			event.target = document;
		}

		// Support: Safari 6.0+, Chrome<28
		// Target should not be a text node (#504, #13143)
		if ( event.target.nodeType === 3 ) {
			event.target = event.target.parentNode;
		}

		return fixHook.filter ? fixHook.filter( event, originalEvent ) : event;
	},

	special: {
		load: {
			// Prevent triggered image.load events from bubbling to window.load
			noBubble: true
		},
		focus: {
			// Fire native event if possible so blur/focus sequence is correct
			trigger: function() {
				if ( this !== safeActiveElement() && this.focus ) {
					this.focus();
					return false;
				}
			},
			delegateType: "focusin"
		},
		blur: {
			trigger: function() {
				if ( this === safeActiveElement() && this.blur ) {
					this.blur();
					return false;
				}
			},
			delegateType: "focusout"
		},
		click: {
			// For checkbox, fire native event so checked state will be right
			trigger: function() {
				if ( this.type === "checkbox" && this.click && jQuery.nodeName( this, "input" ) ) {
					this.click();
					return false;
				}
			},

			// For cross-browser consistency, don't fire native .click() on links
			_default: function( event ) {
				return jQuery.nodeName( event.target, "a" );
			}
		},

		beforeunload: {
			postDispatch: function( event ) {

				// Support: Firefox 20+
				// Firefox doesn't alert if the returnValue field is not set.
				if ( event.result !== undefined && event.originalEvent ) {
					event.originalEvent.returnValue = event.result;
				}
			}
		}
	},

	simulate: function( type, elem, event, bubble ) {
		// Piggyback on a donor event to simulate a different one.
		// Fake originalEvent to avoid donor's stopPropagation, but if the
		// simulated event prevents default then we do the same on the donor.
		var e = jQuery.extend(
			new jQuery.Event(),
			event,
			{
				type: type,
				isSimulated: true,
				originalEvent: {}
			}
		);
		if ( bubble ) {
			jQuery.event.trigger( e, null, elem );
		} else {
			jQuery.event.dispatch.call( elem, e );
		}
		if ( e.isDefaultPrevented() ) {
			event.preventDefault();
		}
	}
};

jQuery.removeEvent = function( elem, type, handle ) {
	if ( elem.removeEventListener ) {
		elem.removeEventListener( type, handle, false );
	}
};

jQuery.Event = function( src, props ) {
	// Allow instantiation without the 'new' keyword
	if ( !(this instanceof jQuery.Event) ) {
		return new jQuery.Event( src, props );
	}

	// Event object
	if ( src && src.type ) {
		this.originalEvent = src;
		this.type = src.type;

		// Events bubbling up the document may have been marked as prevented
		// by a handler lower down the tree; reflect the correct value.
		this.isDefaultPrevented = src.defaultPrevented ||
				src.defaultPrevented === undefined &&
				// Support: Android<4.0
				src.returnValue === false ?
			returnTrue :
			returnFalse;

	// Event type
	} else {
		this.type = src;
	}

	// Put explicitly provided properties onto the event object
	if ( props ) {
		jQuery.extend( this, props );
	}

	// Create a timestamp if incoming event doesn't have one
	this.timeStamp = src && src.timeStamp || jQuery.now();

	// Mark it as fixed
	this[ jQuery.expando ] = true;
};

// jQuery.Event is based on DOM3 Events as specified by the ECMAScript Language Binding
// http://www.w3.org/TR/2003/WD-DOM-Level-3-Events-20030331/ecma-script-binding.html
jQuery.Event.prototype = {
	isDefaultPrevented: returnFalse,
	isPropagationStopped: returnFalse,
	isImmediatePropagationStopped: returnFalse,

	preventDefault: function() {
		var e = this.originalEvent;

		this.isDefaultPrevented = returnTrue;

		if ( e && e.preventDefault ) {
			e.preventDefault();
		}
	},
	stopPropagation: function() {
		var e = this.originalEvent;

		this.isPropagationStopped = returnTrue;

		if ( e && e.stopPropagation ) {
			e.stopPropagation();
		}
	},
	stopImmediatePropagation: function() {
		var e = this.originalEvent;

		this.isImmediatePropagationStopped = returnTrue;

		if ( e && e.stopImmediatePropagation ) {
			e.stopImmediatePropagation();
		}

		this.stopPropagation();
	}
};

// Create mouseenter/leave events using mouseover/out and event-time checks
// Support: Chrome 15+
jQuery.each({
	mouseenter: "mouseover",
	mouseleave: "mouseout",
	pointerenter: "pointerover",
	pointerleave: "pointerout"
}, function( orig, fix ) {
	jQuery.event.special[ orig ] = {
		delegateType: fix,
		bindType: fix,

		handle: function( event ) {
			var ret,
				target = this,
				related = event.relatedTarget,
				handleObj = event.handleObj;

			// For mousenter/leave call the handler if related is outside the target.
			// NB: No relatedTarget if the mouse left/entered the browser window
			if ( !related || (related !== target && !jQuery.contains( target, related )) ) {
				event.type = handleObj.origType;
				ret = handleObj.handler.apply( this, arguments );
				event.type = fix;
			}
			return ret;
		}
	};
});

// Support: Firefox, Chrome, Safari
// Create "bubbling" focus and blur events
if ( !support.focusinBubbles ) {
	jQuery.each({ focus: "focusin", blur: "focusout" }, function( orig, fix ) {

		// Attach a single capturing handler on the document while someone wants focusin/focusout
		var handler = function( event ) {
				jQuery.event.simulate( fix, event.target, jQuery.event.fix( event ), true );
			};

		jQuery.event.special[ fix ] = {
			setup: function() {
				var doc = this.ownerDocument || this,
					attaches = data_priv.access( doc, fix );

				if ( !attaches ) {
					doc.addEventListener( orig, handler, true );
				}
				data_priv.access( doc, fix, ( attaches || 0 ) + 1 );
			},
			teardown: function() {
				var doc = this.ownerDocument || this,
					attaches = data_priv.access( doc, fix ) - 1;

				if ( !attaches ) {
					doc.removeEventListener( orig, handler, true );
					data_priv.remove( doc, fix );

				} else {
					data_priv.access( doc, fix, attaches );
				}
			}
		};
	});
}

jQuery.fn.extend({

	on: function( types, selector, data, fn, /*INTERNAL*/ one ) {
		var origFn, type;

		// Types can be a map of types/handlers
		if ( typeof types === "object" ) {
			// ( types-Object, selector, data )
			if ( typeof selector !== "string" ) {
				// ( types-Object, data )
				data = data || selector;
				selector = undefined;
			}
			for ( type in types ) {
				this.on( type, selector, data, types[ type ], one );
			}
			return this;
		}

		if ( data == null && fn == null ) {
			// ( types, fn )
			fn = selector;
			data = selector = undefined;
		} else if ( fn == null ) {
			if ( typeof selector === "string" ) {
				// ( types, selector, fn )
				fn = data;
				data = undefined;
			} else {
				// ( types, data, fn )
				fn = data;
				data = selector;
				selector = undefined;
			}
		}
		if ( fn === false ) {
			fn = returnFalse;
		} else if ( !fn ) {
			return this;
		}

		if ( one === 1 ) {
			origFn = fn;
			fn = function( event ) {
				// Can use an empty set, since event contains the info
				jQuery().off( event );
				return origFn.apply( this, arguments );
			};
			// Use same guid so caller can remove using origFn
			fn.guid = origFn.guid || ( origFn.guid = jQuery.guid++ );
		}
		return this.each( function() {
			jQuery.event.add( this, types, fn, data, selector );
		});
	},
	one: function( types, selector, data, fn ) {
		return this.on( types, selector, data, fn, 1 );
	},
	off: function( types, selector, fn ) {
		var handleObj, type;
		if ( types && types.preventDefault && types.handleObj ) {
			// ( event )  dispatched jQuery.Event
			handleObj = types.handleObj;
			jQuery( types.delegateTarget ).off(
				handleObj.namespace ? handleObj.origType + "." + handleObj.namespace : handleObj.origType,
				handleObj.selector,
				handleObj.handler
			);
			return this;
		}
		if ( typeof types === "object" ) {
			// ( types-object [, selector] )
			for ( type in types ) {
				this.off( type, selector, types[ type ] );
			}
			return this;
		}
		if ( selector === false || typeof selector === "function" ) {
			// ( types [, fn] )
			fn = selector;
			selector = undefined;
		}
		if ( fn === false ) {
			fn = returnFalse;
		}
		return this.each(function() {
			jQuery.event.remove( this, types, fn, selector );
		});
	},

	trigger: function( type, data ) {
		return this.each(function() {
			jQuery.event.trigger( type, data, this );
		});
	},
	triggerHandler: function( type, data ) {
		var elem = this[0];
		if ( elem ) {
			return jQuery.event.trigger( type, data, elem, true );
		}
	}
});


var
	rxhtmlTag = /<(?!area|br|col|embed|hr|img|input|link|meta|param)(([\w:]+)[^>]*)\/>/gi,
	rtagName = /<([\w:]+)/,
	rhtml = /<|&#?\w+;/,
	rnoInnerhtml = /<(?:script|style|link)/i,
	// checked="checked" or checked
	rchecked = /checked\s*(?:[^=]|=\s*.checked.)/i,
	rscriptType = /^$|\/(?:java|ecma)script/i,
	rscriptTypeMasked = /^true\/(.*)/,
	rcleanScript = /^\s*<!(?:\[CDATA\[|--)|(?:\]\]|--)>\s*$/g,

	// We have to close these tags to support XHTML (#13200)
	wrapMap = {

		// Support: IE9
		option: [ 1, "<select multiple='multiple'>", "</select>" ],

		thead: [ 1, "<table>", "</table>" ],
		col: [ 2, "<table><colgroup>", "</colgroup></table>" ],
		tr: [ 2, "<table><tbody>", "</tbody></table>" ],
		td: [ 3, "<table><tbody><tr>", "</tr></tbody></table>" ],

		_default: [ 0, "", "" ]
	};

// Support: IE9
wrapMap.optgroup = wrapMap.option;

wrapMap.tbody = wrapMap.tfoot = wrapMap.colgroup = wrapMap.caption = wrapMap.thead;
wrapMap.th = wrapMap.td;

// Support: 1.x compatibility
// Manipulating tables requires a tbody
function manipulationTarget( elem, content ) {
	return jQuery.nodeName( elem, "table" ) &&
		jQuery.nodeName( content.nodeType !== 11 ? content : content.firstChild, "tr" ) ?

		elem.getElementsByTagName("tbody")[0] ||
			elem.appendChild( elem.ownerDocument.createElement("tbody") ) :
		elem;
}

// Replace/restore the type attribute of script elements for safe DOM manipulation
function disableScript( elem ) {
	elem.type = (elem.getAttribute("type") !== null) + "/" + elem.type;
	return elem;
}
function restoreScript( elem ) {
	var match = rscriptTypeMasked.exec( elem.type );

	if ( match ) {
		elem.type = match[ 1 ];
	} else {
		elem.removeAttribute("type");
	}

	return elem;
}

// Mark scripts as having already been evaluated
function setGlobalEval( elems, refElements ) {
	var i = 0,
		l = elems.length;

	for ( ; i < l; i++ ) {
		data_priv.set(
			elems[ i ], "globalEval", !refElements || data_priv.get( refElements[ i ], "globalEval" )
		);
	}
}

function cloneCopyEvent( src, dest ) {
	var i, l, type, pdataOld, pdataCur, udataOld, udataCur, events;

	if ( dest.nodeType !== 1 ) {
		return;
	}

	// 1. Copy private data: events, handlers, etc.
	if ( data_priv.hasData( src ) ) {
		pdataOld = data_priv.access( src );
		pdataCur = data_priv.set( dest, pdataOld );
		events = pdataOld.events;

		if ( events ) {
			delete pdataCur.handle;
			pdataCur.events = {};

			for ( type in events ) {
				for ( i = 0, l = events[ type ].length; i < l; i++ ) {
					jQuery.event.add( dest, type, events[ type ][ i ] );
				}
			}
		}
	}

	// 2. Copy user data
	if ( data_user.hasData( src ) ) {
		udataOld = data_user.access( src );
		udataCur = jQuery.extend( {}, udataOld );

		data_user.set( dest, udataCur );
	}
}

function getAll( context, tag ) {
	var ret = context.getElementsByTagName ? context.getElementsByTagName( tag || "*" ) :
			context.querySelectorAll ? context.querySelectorAll( tag || "*" ) :
			[];

	return tag === undefined || tag && jQuery.nodeName( context, tag ) ?
		jQuery.merge( [ context ], ret ) :
		ret;
}

// Fix IE bugs, see support tests
function fixInput( src, dest ) {
	var nodeName = dest.nodeName.toLowerCase();

	// Fails to persist the checked state of a cloned checkbox or radio button.
	if ( nodeName === "input" && rcheckableType.test( src.type ) ) {
		dest.checked = src.checked;

	// Fails to return the selected option to the default selected state when cloning options
	} else if ( nodeName === "input" || nodeName === "textarea" ) {
		dest.defaultValue = src.defaultValue;
	}
}

jQuery.extend({
	clone: function( elem, dataAndEvents, deepDataAndEvents ) {
		var i, l, srcElements, destElements,
			clone = elem.cloneNode( true ),
			inPage = jQuery.contains( elem.ownerDocument, elem );

		// Fix IE cloning issues
		if ( !support.noCloneChecked && ( elem.nodeType === 1 || elem.nodeType === 11 ) &&
				!jQuery.isXMLDoc( elem ) ) {

			// We eschew Sizzle here for performance reasons: http://jsperf.com/getall-vs-sizzle/2
			destElements = getAll( clone );
			srcElements = getAll( elem );

			for ( i = 0, l = srcElements.length; i < l; i++ ) {
				fixInput( srcElements[ i ], destElements[ i ] );
			}
		}

		// Copy the events from the original to the clone
		if ( dataAndEvents ) {
			if ( deepDataAndEvents ) {
				srcElements = srcElements || getAll( elem );
				destElements = destElements || getAll( clone );

				for ( i = 0, l = srcElements.length; i < l; i++ ) {
					cloneCopyEvent( srcElements[ i ], destElements[ i ] );
				}
			} else {
				cloneCopyEvent( elem, clone );
			}
		}

		// Preserve script evaluation history
		destElements = getAll( clone, "script" );
		if ( destElements.length > 0 ) {
			setGlobalEval( destElements, !inPage && getAll( elem, "script" ) );
		}

		// Return the cloned set
		return clone;
	},

	buildFragment: function( elems, context, scripts, selection ) {
		var elem, tmp, tag, wrap, contains, j,
			fragment = context.createDocumentFragment(),
			nodes = [],
			i = 0,
			l = elems.length;

		for ( ; i < l; i++ ) {
			elem = elems[ i ];

			if ( elem || elem === 0 ) {

				// Add nodes directly
				if ( jQuery.type( elem ) === "object" ) {
					// Support: QtWebKit, PhantomJS
					// push.apply(_, arraylike) throws on ancient WebKit
					jQuery.merge( nodes, elem.nodeType ? [ elem ] : elem );

				// Convert non-html into a text node
				} else if ( !rhtml.test( elem ) ) {
					nodes.push( context.createTextNode( elem ) );

				// Convert html into DOM nodes
				} else {
					tmp = tmp || fragment.appendChild( context.createElement("div") );

					// Deserialize a standard representation
					tag = ( rtagName.exec( elem ) || [ "", "" ] )[ 1 ].toLowerCase();
					wrap = wrapMap[ tag ] || wrapMap._default;
					tmp.innerHTML = wrap[ 1 ] + elem.replace( rxhtmlTag, "<$1></$2>" ) + wrap[ 2 ];

					// Descend through wrappers to the right content
					j = wrap[ 0 ];
					while ( j-- ) {
						tmp = tmp.lastChild;
					}

					// Support: QtWebKit, PhantomJS
					// push.apply(_, arraylike) throws on ancient WebKit
					jQuery.merge( nodes, tmp.childNodes );

					// Remember the top-level container
					tmp = fragment.firstChild;

					// Ensure the created nodes are orphaned (#12392)
					tmp.textContent = "";
				}
			}
		}

		// Remove wrapper from fragment
		fragment.textContent = "";

		i = 0;
		while ( (elem = nodes[ i++ ]) ) {

			// #4087 - If origin and destination elements are the same, and this is
			// that element, do not do anything
			if ( selection && jQuery.inArray( elem, selection ) !== -1 ) {
				continue;
			}

			contains = jQuery.contains( elem.ownerDocument, elem );

			// Append to fragment
			tmp = getAll( fragment.appendChild( elem ), "script" );

			// Preserve script evaluation history
			if ( contains ) {
				setGlobalEval( tmp );
			}

			// Capture executables
			if ( scripts ) {
				j = 0;
				while ( (elem = tmp[ j++ ]) ) {
					if ( rscriptType.test( elem.type || "" ) ) {
						scripts.push( elem );
					}
				}
			}
		}

		return fragment;
	},

	cleanData: function( elems ) {
		var data, elem, type, key,
			special = jQuery.event.special,
			i = 0;

		for ( ; (elem = elems[ i ]) !== undefined; i++ ) {
			if ( jQuery.acceptData( elem ) ) {
				key = elem[ data_priv.expando ];

				if ( key && (data = data_priv.cache[ key ]) ) {
					if ( data.events ) {
						for ( type in data.events ) {
							if ( special[ type ] ) {
								jQuery.event.remove( elem, type );

							// This is a shortcut to avoid jQuery.event.remove's overhead
							} else {
								jQuery.removeEvent( elem, type, data.handle );
							}
						}
					}
					if ( data_priv.cache[ key ] ) {
						// Discard any remaining `private` data
						delete data_priv.cache[ key ];
					}
				}
			}
			// Discard any remaining `user` data
			delete data_user.cache[ elem[ data_user.expando ] ];
		}
	}
});

jQuery.fn.extend({
	text: function( value ) {
		return access( this, function( value ) {
			return value === undefined ?
				jQuery.text( this ) :
				this.empty().each(function() {
					if ( this.nodeType === 1 || this.nodeType === 11 || this.nodeType === 9 ) {
						this.textContent = value;
					}
				});
		}, null, value, arguments.length );
	},

	append: function() {
		return this.domManip( arguments, function( elem ) {
			if ( this.nodeType === 1 || this.nodeType === 11 || this.nodeType === 9 ) {
				var target = manipulationTarget( this, elem );
				target.appendChild( elem );
			}
		});
	},

	prepend: function() {
		return this.domManip( arguments, function( elem ) {
			if ( this.nodeType === 1 || this.nodeType === 11 || this.nodeType === 9 ) {
				var target = manipulationTarget( this, elem );
				target.insertBefore( elem, target.firstChild );
			}
		});
	},

	before: function() {
		return this.domManip( arguments, function( elem ) {
			if ( this.parentNode ) {
				this.parentNode.insertBefore( elem, this );
			}
		});
	},

	after: function() {
		return this.domManip( arguments, function( elem ) {
			if ( this.parentNode ) {
				this.parentNode.insertBefore( elem, this.nextSibling );
			}
		});
	},

	remove: function( selector, keepData /* Internal Use Only */ ) {
		var elem,
			elems = selector ? jQuery.filter( selector, this ) : this,
			i = 0;

		for ( ; (elem = elems[i]) != null; i++ ) {
			if ( !keepData && elem.nodeType === 1 ) {
				jQuery.cleanData( getAll( elem ) );
			}

			if ( elem.parentNode ) {
				if ( keepData && jQuery.contains( elem.ownerDocument, elem ) ) {
					setGlobalEval( getAll( elem, "script" ) );
				}
				elem.parentNode.removeChild( elem );
			}
		}

		return this;
	},

	empty: function() {
		var elem,
			i = 0;

		for ( ; (elem = this[i]) != null; i++ ) {
			if ( elem.nodeType === 1 ) {

				// Prevent memory leaks
				jQuery.cleanData( getAll( elem, false ) );

				// Remove any remaining nodes
				elem.textContent = "";
			}
		}

		return this;
	},

	clone: function( dataAndEvents, deepDataAndEvents ) {
		dataAndEvents = dataAndEvents == null ? false : dataAndEvents;
		deepDataAndEvents = deepDataAndEvents == null ? dataAndEvents : deepDataAndEvents;

		return this.map(function() {
			return jQuery.clone( this, dataAndEvents, deepDataAndEvents );
		});
	},

	html: function( value ) {
		return access( this, function( value ) {
			var elem = this[ 0 ] || {},
				i = 0,
				l = this.length;

			if ( value === undefined && elem.nodeType === 1 ) {
				return elem.innerHTML;
			}

			// See if we can take a shortcut and just use innerHTML
			if ( typeof value === "string" && !rnoInnerhtml.test( value ) &&
				!wrapMap[ ( rtagName.exec( value ) || [ "", "" ] )[ 1 ].toLowerCase() ] ) {

				value = value.replace( rxhtmlTag, "<$1></$2>" );

				try {
					for ( ; i < l; i++ ) {
						elem = this[ i ] || {};

						// Remove element nodes and prevent memory leaks
						if ( elem.nodeType === 1 ) {
							jQuery.cleanData( getAll( elem, false ) );
							elem.innerHTML = value;
						}
					}

					elem = 0;

				// If using innerHTML throws an exception, use the fallback method
				} catch( e ) {}
			}

			if ( elem ) {
				this.empty().append( value );
			}
		}, null, value, arguments.length );
	},

	replaceWith: function() {
		var arg = arguments[ 0 ];

		// Make the changes, replacing each context element with the new content
		this.domManip( arguments, function( elem ) {
			arg = this.parentNode;

			jQuery.cleanData( getAll( this ) );

			if ( arg ) {
				arg.replaceChild( elem, this );
			}
		});

		// Force removal if there was no new content (e.g., from empty arguments)
		return arg && (arg.length || arg.nodeType) ? this : this.remove();
	},

	detach: function( selector ) {
		return this.remove( selector, true );
	},

	domManip: function( args, callback ) {

		// Flatten any nested arrays
		args = concat.apply( [], args );

		var fragment, first, scripts, hasScripts, node, doc,
			i = 0,
			l = this.length,
			set = this,
			iNoClone = l - 1,
			value = args[ 0 ],
			isFunction = jQuery.isFunction( value );

		// We can't cloneNode fragments that contain checked, in WebKit
		if ( isFunction ||
				( l > 1 && typeof value === "string" &&
					!support.checkClone && rchecked.test( value ) ) ) {
			return this.each(function( index ) {
				var self = set.eq( index );
				if ( isFunction ) {
					args[ 0 ] = value.call( this, index, self.html() );
				}
				self.domManip( args, callback );
			});
		}

		if ( l ) {
			fragment = jQuery.buildFragment( args, this[ 0 ].ownerDocument, false, this );
			first = fragment.firstChild;

			if ( fragment.childNodes.length === 1 ) {
				fragment = first;
			}

			if ( first ) {
				scripts = jQuery.map( getAll( fragment, "script" ), disableScript );
				hasScripts = scripts.length;

				// Use the original fragment for the last item instead of the first because it can end up
				// being emptied incorrectly in certain situations (#8070).
				for ( ; i < l; i++ ) {
					node = fragment;

					if ( i !== iNoClone ) {
						node = jQuery.clone( node, true, true );

						// Keep references to cloned scripts for later restoration
						if ( hasScripts ) {
							// Support: QtWebKit
							// jQuery.merge because push.apply(_, arraylike) throws
							jQuery.merge( scripts, getAll( node, "script" ) );
						}
					}

					callback.call( this[ i ], node, i );
				}

				if ( hasScripts ) {
					doc = scripts[ scripts.length - 1 ].ownerDocument;

					// Reenable scripts
					jQuery.map( scripts, restoreScript );

					// Evaluate executable scripts on first document insertion
					for ( i = 0; i < hasScripts; i++ ) {
						node = scripts[ i ];
						if ( rscriptType.test( node.type || "" ) &&
							!data_priv.access( node, "globalEval" ) && jQuery.contains( doc, node ) ) {

							if ( node.src ) {
								// Optional AJAX dependency, but won't run scripts if not present
								if ( jQuery._evalUrl ) {
									jQuery._evalUrl( node.src );
								}
							} else {
								jQuery.globalEval( node.textContent.replace( rcleanScript, "" ) );
							}
						}
					}
				}
			}
		}

		return this;
	}
});

jQuery.each({
	appendTo: "append",
	prependTo: "prepend",
	insertBefore: "before",
	insertAfter: "after",
	replaceAll: "replaceWith"
}, function( name, original ) {
	jQuery.fn[ name ] = function( selector ) {
		var elems,
			ret = [],
			insert = jQuery( selector ),
			last = insert.length - 1,
			i = 0;

		for ( ; i <= last; i++ ) {
			elems = i === last ? this : this.clone( true );
			jQuery( insert[ i ] )[ original ]( elems );

			// Support: QtWebKit
			// .get() because push.apply(_, arraylike) throws
			push.apply( ret, elems.get() );
		}

		return this.pushStack( ret );
	};
});


var iframe,
	elemdisplay = {};

/**
 * Retrieve the actual display of a element
 * @param {String} name nodeName of the element
 * @param {Object} doc Document object
 */
// Called only from within defaultDisplay
function actualDisplay( name, doc ) {
	var style,
		elem = jQuery( doc.createElement( name ) ).appendTo( doc.body ),

		// getDefaultComputedStyle might be reliably used only on attached element
		display = window.getDefaultComputedStyle && ( style = window.getDefaultComputedStyle( elem[ 0 ] ) ) ?

			// Use of this method is a temporary fix (more like optimization) until something better comes along,
			// since it was removed from specification and supported only in FF
			style.display : jQuery.css( elem[ 0 ], "display" );

	// We don't have any data stored on the element,
	// so use "detach" method as fast way to get rid of the element
	elem.detach();

	return display;
}

/**
 * Try to determine the default display value of an element
 * @param {String} nodeName
 */
function defaultDisplay( nodeName ) {
	var doc = document,
		display = elemdisplay[ nodeName ];

	if ( !display ) {
		display = actualDisplay( nodeName, doc );

		// If the simple way fails, read from inside an iframe
		if ( display === "none" || !display ) {

			// Use the already-created iframe if possible
			iframe = (iframe || jQuery( "<iframe frameborder='0' width='0' height='0'/>" )).appendTo( doc.documentElement );

			// Always write a new HTML skeleton so Webkit and Firefox don't choke on reuse
			doc = iframe[ 0 ].contentDocument;

			// Support: IE
			doc.write();
			doc.close();

			display = actualDisplay( nodeName, doc );
			iframe.detach();
		}

		// Store the correct default display
		elemdisplay[ nodeName ] = display;
	}

	return display;
}
var rmargin = (/^margin/);

var rnumnonpx = new RegExp( "^(" + pnum + ")(?!px)[a-z%]+$", "i" );

var getStyles = function( elem ) {
		// Support: IE<=11+, Firefox<=30+ (#15098, #14150)
		// IE throws on elements created in popups
		// FF meanwhile throws on frame elements through "defaultView.getComputedStyle"
		if ( elem.ownerDocument.defaultView.opener ) {
			return elem.ownerDocument.defaultView.getComputedStyle( elem, null );
		}

		return window.getComputedStyle( elem, null );
	};



function curCSS( elem, name, computed ) {
	var width, minWidth, maxWidth, ret,
		style = elem.style;

	computed = computed || getStyles( elem );

	// Support: IE9
	// getPropertyValue is only needed for .css('filter') (#12537)
	if ( computed ) {
		ret = computed.getPropertyValue( name ) || computed[ name ];
	}

	if ( computed ) {

		if ( ret === "" && !jQuery.contains( elem.ownerDocument, elem ) ) {
			ret = jQuery.style( elem, name );
		}

		// Support: iOS < 6
		// A tribute to the "awesome hack by Dean Edwards"
		// iOS < 6 (at least) returns percentage for a larger set of values, but width seems to be reliably pixels
		// this is against the CSSOM draft spec: http://dev.w3.org/csswg/cssom/#resolved-values
		if ( rnumnonpx.test( ret ) && rmargin.test( name ) ) {

			// Remember the original values
			width = style.width;
			minWidth = style.minWidth;
			maxWidth = style.maxWidth;

			// Put in the new values to get a computed value out
			style.minWidth = style.maxWidth = style.width = ret;
			ret = computed.width;

			// Revert the changed values
			style.width = width;
			style.minWidth = minWidth;
			style.maxWidth = maxWidth;
		}
	}

	return ret !== undefined ?
		// Support: IE
		// IE returns zIndex value as an integer.
		ret + "" :
		ret;
}


function addGetHookIf( conditionFn, hookFn ) {
	// Define the hook, we'll check on the first run if it's really needed.
	return {
		get: function() {
			if ( conditionFn() ) {
				// Hook not needed (or it's not possible to use it due
				// to missing dependency), remove it.
				delete this.get;
				return;
			}

			// Hook needed; redefine it so that the support test is not executed again.
			return (this.get = hookFn).apply( this, arguments );
		}
	};
}


(function() {
	var pixelPositionVal, boxSizingReliableVal,
		docElem = document.documentElement,
		container = document.createElement( "div" ),
		div = document.createElement( "div" );

	if ( !div.style ) {
		return;
	}

	// Support: IE9-11+
	// Style of cloned element affects source element cloned (#8908)
	div.style.backgroundClip = "content-box";
	div.cloneNode( true ).style.backgroundClip = "";
	support.clearCloneStyle = div.style.backgroundClip === "content-box";

	container.style.cssText = "border:0;width:0;height:0;top:0;left:-9999px;margin-top:1px;" +
		"position:absolute";
	container.appendChild( div );

	// Executing both pixelPosition & boxSizingReliable tests require only one layout
	// so they're executed at the same time to save the second computation.
	function computePixelPositionAndBoxSizingReliable() {
		div.style.cssText =
			// Support: Firefox<29, Android 2.3
			// Vendor-prefix box-sizing
			"-webkit-box-sizing:border-box;-moz-box-sizing:border-box;" +
			"box-sizing:border-box;display:block;margin-top:1%;top:1%;" +
			"border:1px;padding:1px;width:4px;position:absolute";
		div.innerHTML = "";
		docElem.appendChild( container );

		var divStyle = window.getComputedStyle( div, null );
		pixelPositionVal = divStyle.top !== "1%";
		boxSizingReliableVal = divStyle.width === "4px";

		docElem.removeChild( container );
	}

	// Support: node.js jsdom
	// Don't assume that getComputedStyle is a property of the global object
	if ( window.getComputedStyle ) {
		jQuery.extend( support, {
			pixelPosition: function() {

				// This test is executed only once but we still do memoizing
				// since we can use the boxSizingReliable pre-computing.
				// No need to check if the test was already performed, though.
				computePixelPositionAndBoxSizingReliable();
				return pixelPositionVal;
			},
			boxSizingReliable: function() {
				if ( boxSizingReliableVal == null ) {
					computePixelPositionAndBoxSizingReliable();
				}
				return boxSizingReliableVal;
			},
			reliableMarginRight: function() {

				// Support: Android 2.3
				// Check if div with explicit width and no margin-right incorrectly
				// gets computed margin-right based on width of container. (#3333)
				// WebKit Bug 13343 - getComputedStyle returns wrong value for margin-right
				// This support function is only executed once so no memoizing is needed.
				var ret,
					marginDiv = div.appendChild( document.createElement( "div" ) );

				// Reset CSS: box-sizing; display; margin; border; padding
				marginDiv.style.cssText = div.style.cssText =
					// Support: Firefox<29, Android 2.3
					// Vendor-prefix box-sizing
					"-webkit-box-sizing:content-box;-moz-box-sizing:content-box;" +
					"box-sizing:content-box;display:block;margin:0;border:0;padding:0";
				marginDiv.style.marginRight = marginDiv.style.width = "0";
				div.style.width = "1px";
				docElem.appendChild( container );

				ret = !parseFloat( window.getComputedStyle( marginDiv, null ).marginRight );

				docElem.removeChild( container );
				div.removeChild( marginDiv );

				return ret;
			}
		});
	}
})();


// A method for quickly swapping in/out CSS properties to get correct calculations.
jQuery.swap = function( elem, options, callback, args ) {
	var ret, name,
		old = {};

	// Remember the old values, and insert the new ones
	for ( name in options ) {
		old[ name ] = elem.style[ name ];
		elem.style[ name ] = options[ name ];
	}

	ret = callback.apply( elem, args || [] );

	// Revert the old values
	for ( name in options ) {
		elem.style[ name ] = old[ name ];
	}

	return ret;
};


var
	// Swappable if display is none or starts with table except "table", "table-cell", or "table-caption"
	// See here for display values: https://developer.mozilla.org/en-US/docs/CSS/display
	rdisplayswap = /^(none|table(?!-c[ea]).+)/,
	rnumsplit = new RegExp( "^(" + pnum + ")(.*)$", "i" ),
	rrelNum = new RegExp( "^([+-])=(" + pnum + ")", "i" ),

	cssShow = { position: "absolute", visibility: "hidden", display: "block" },
	cssNormalTransform = {
		letterSpacing: "0",
		fontWeight: "400"
	},

	cssPrefixes = [ "Webkit", "O", "Moz", "ms" ];

// Return a css property mapped to a potentially vendor prefixed property
function vendorPropName( style, name ) {

	// Shortcut for names that are not vendor prefixed
	if ( name in style ) {
		return name;
	}

	// Check for vendor prefixed names
	var capName = name[0].toUpperCase() + name.slice(1),
		origName = name,
		i = cssPrefixes.length;

	while ( i-- ) {
		name = cssPrefixes[ i ] + capName;
		if ( name in style ) {
			return name;
		}
	}

	return origName;
}

function setPositiveNumber( elem, value, subtract ) {
	var matches = rnumsplit.exec( value );
	return matches ?
		// Guard against undefined "subtract", e.g., when used as in cssHooks
		Math.max( 0, matches[ 1 ] - ( subtract || 0 ) ) + ( matches[ 2 ] || "px" ) :
		value;
}

function augmentWidthOrHeight( elem, name, extra, isBorderBox, styles ) {
	var i = extra === ( isBorderBox ? "border" : "content" ) ?
		// If we already have the right measurement, avoid augmentation
		4 :
		// Otherwise initialize for horizontal or vertical properties
		name === "width" ? 1 : 0,

		val = 0;

	for ( ; i < 4; i += 2 ) {
		// Both box models exclude margin, so add it if we want it
		if ( extra === "margin" ) {
			val += jQuery.css( elem, extra + cssExpand[ i ], true, styles );
		}

		if ( isBorderBox ) {
			// border-box includes padding, so remove it if we want content
			if ( extra === "content" ) {
				val -= jQuery.css( elem, "padding" + cssExpand[ i ], true, styles );
			}

			// At this point, extra isn't border nor margin, so remove border
			if ( extra !== "margin" ) {
				val -= jQuery.css( elem, "border" + cssExpand[ i ] + "Width", true, styles );
			}
		} else {
			// At this point, extra isn't content, so add padding
			val += jQuery.css( elem, "padding" + cssExpand[ i ], true, styles );

			// At this point, extra isn't content nor padding, so add border
			if ( extra !== "padding" ) {
				val += jQuery.css( elem, "border" + cssExpand[ i ] + "Width", true, styles );
			}
		}
	}

	return val;
}

function getWidthOrHeight( elem, name, extra ) {

	// Start with offset property, which is equivalent to the border-box value
	var valueIsBorderBox = true,
		val = name === "width" ? elem.offsetWidth : elem.offsetHeight,
		styles = getStyles( elem ),
		isBorderBox = jQuery.css( elem, "boxSizing", false, styles ) === "border-box";

	// Some non-html elements return undefined for offsetWidth, so check for null/undefined
	// svg - https://bugzilla.mozilla.org/show_bug.cgi?id=649285
	// MathML - https://bugzilla.mozilla.org/show_bug.cgi?id=491668
	if ( val <= 0 || val == null ) {
		// Fall back to computed then uncomputed css if necessary
		val = curCSS( elem, name, styles );
		if ( val < 0 || val == null ) {
			val = elem.style[ name ];
		}

		// Computed unit is not pixels. Stop here and return.
		if ( rnumnonpx.test(val) ) {
			return val;
		}

		// Check for style in case a browser which returns unreliable values
		// for getComputedStyle silently falls back to the reliable elem.style
		valueIsBorderBox = isBorderBox &&
			( support.boxSizingReliable() || val === elem.style[ name ] );

		// Normalize "", auto, and prepare for extra
		val = parseFloat( val ) || 0;
	}

	// Use the active box-sizing model to add/subtract irrelevant styles
	return ( val +
		augmentWidthOrHeight(
			elem,
			name,
			extra || ( isBorderBox ? "border" : "content" ),
			valueIsBorderBox,
			styles
		)
	) + "px";
}

function showHide( elements, show ) {
	var display, elem, hidden,
		values = [],
		index = 0,
		length = elements.length;

	for ( ; index < length; index++ ) {
		elem = elements[ index ];
		if ( !elem.style ) {
			continue;
		}

		values[ index ] = data_priv.get( elem, "olddisplay" );
		display = elem.style.display;
		if ( show ) {
			// Reset the inline display of this element to learn if it is
			// being hidden by cascaded rules or not
			if ( !values[ index ] && display === "none" ) {
				elem.style.display = "";
			}

			// Set elements which have been overridden with display: none
			// in a stylesheet to whatever the default browser style is
			// for such an element
			if ( elem.style.display === "" && isHidden( elem ) ) {
				values[ index ] = data_priv.access( elem, "olddisplay", defaultDisplay(elem.nodeName) );
			}
		} else {
			hidden = isHidden( elem );

			if ( display !== "none" || !hidden ) {
				data_priv.set( elem, "olddisplay", hidden ? display : jQuery.css( elem, "display" ) );
			}
		}
	}

	// Set the display of most of the elements in a second loop
	// to avoid the constant reflow
	for ( index = 0; index < length; index++ ) {
		elem = elements[ index ];
		if ( !elem.style ) {
			continue;
		}
		if ( !show || elem.style.display === "none" || elem.style.display === "" ) {
			elem.style.display = show ? values[ index ] || "" : "none";
		}
	}

	return elements;
}

jQuery.extend({

	// Add in style property hooks for overriding the default
	// behavior of getting and setting a style property
	cssHooks: {
		opacity: {
			get: function( elem, computed ) {
				if ( computed ) {

					// We should always get a number back from opacity
					var ret = curCSS( elem, "opacity" );
					return ret === "" ? "1" : ret;
				}
			}
		}
	},

	// Don't automatically add "px" to these possibly-unitless properties
	cssNumber: {
		"columnCount": true,
		"fillOpacity": true,
		"flexGrow": true,
		"flexShrink": true,
		"fontWeight": true,
		"lineHeight": true,
		"opacity": true,
		"order": true,
		"orphans": true,
		"widows": true,
		"zIndex": true,
		"zoom": true
	},

	// Add in properties whose names you wish to fix before
	// setting or getting the value
	cssProps: {
		"float": "cssFloat"
	},

	// Get and set the style property on a DOM Node
	style: function( elem, name, value, extra ) {

		// Don't set styles on text and comment nodes
		if ( !elem || elem.nodeType === 3 || elem.nodeType === 8 || !elem.style ) {
			return;
		}

		// Make sure that we're working with the right name
		var ret, type, hooks,
			origName = jQuery.camelCase( name ),
			style = elem.style;

		name = jQuery.cssProps[ origName ] || ( jQuery.cssProps[ origName ] = vendorPropName( style, origName ) );

		// Gets hook for the prefixed version, then unprefixed version
		hooks = jQuery.cssHooks[ name ] || jQuery.cssHooks[ origName ];

		// Check if we're setting a value
		if ( value !== undefined ) {
			type = typeof value;

			// Convert "+=" or "-=" to relative numbers (#7345)
			if ( type === "string" && (ret = rrelNum.exec( value )) ) {
				value = ( ret[1] + 1 ) * ret[2] + parseFloat( jQuery.css( elem, name ) );
				// Fixes bug #9237
				type = "number";
			}

			// Make sure that null and NaN values aren't set (#7116)
			if ( value == null || value !== value ) {
				return;
			}

			// If a number, add 'px' to the (except for certain CSS properties)
			if ( type === "number" && !jQuery.cssNumber[ origName ] ) {
				value += "px";
			}

			// Support: IE9-11+
			// background-* props affect original clone's values
			if ( !support.clearCloneStyle && value === "" && name.indexOf( "background" ) === 0 ) {
				style[ name ] = "inherit";
			}

			// If a hook was provided, use that value, otherwise just set the specified value
			if ( !hooks || !("set" in hooks) || (value = hooks.set( elem, value, extra )) !== undefined ) {
				style[ name ] = value;
			}

		} else {
			// If a hook was provided get the non-computed value from there
			if ( hooks && "get" in hooks && (ret = hooks.get( elem, false, extra )) !== undefined ) {
				return ret;
			}

			// Otherwise just get the value from the style object
			return style[ name ];
		}
	},

	css: function( elem, name, extra, styles ) {
		var val, num, hooks,
			origName = jQuery.camelCase( name );

		// Make sure that we're working with the right name
		name = jQuery.cssProps[ origName ] || ( jQuery.cssProps[ origName ] = vendorPropName( elem.style, origName ) );

		// Try prefixed name followed by the unprefixed name
		hooks = jQuery.cssHooks[ name ] || jQuery.cssHooks[ origName ];

		// If a hook was provided get the computed value from there
		if ( hooks && "get" in hooks ) {
			val = hooks.get( elem, true, extra );
		}

		// Otherwise, if a way to get the computed value exists, use that
		if ( val === undefined ) {
			val = curCSS( elem, name, styles );
		}

		// Convert "normal" to computed value
		if ( val === "normal" && name in cssNormalTransform ) {
			val = cssNormalTransform[ name ];
		}

		// Make numeric if forced or a qualifier was provided and val looks numeric
		if ( extra === "" || extra ) {
			num = parseFloat( val );
			return extra === true || jQuery.isNumeric( num ) ? num || 0 : val;
		}
		return val;
	}
});

jQuery.each([ "height", "width" ], function( i, name ) {
	jQuery.cssHooks[ name ] = {
		get: function( elem, computed, extra ) {
			if ( computed ) {

				// Certain elements can have dimension info if we invisibly show them
				// but it must have a current display style that would benefit
				return rdisplayswap.test( jQuery.css( elem, "display" ) ) && elem.offsetWidth === 0 ?
					jQuery.swap( elem, cssShow, function() {
						return getWidthOrHeight( elem, name, extra );
					}) :
					getWidthOrHeight( elem, name, extra );
			}
		},

		set: function( elem, value, extra ) {
			var styles = extra && getStyles( elem );
			return setPositiveNumber( elem, value, extra ?
				augmentWidthOrHeight(
					elem,
					name,
					extra,
					jQuery.css( elem, "boxSizing", false, styles ) === "border-box",
					styles
				) : 0
			);
		}
	};
});

// Support: Android 2.3
jQuery.cssHooks.marginRight = addGetHookIf( support.reliableMarginRight,
	function( elem, computed ) {
		if ( computed ) {
			return jQuery.swap( elem, { "display": "inline-block" },
				curCSS, [ elem, "marginRight" ] );
		}
	}
);

// These hooks are used by animate to expand properties
jQuery.each({
	margin: "",
	padding: "",
	border: "Width"
}, function( prefix, suffix ) {
	jQuery.cssHooks[ prefix + suffix ] = {
		expand: function( value ) {
			var i = 0,
				expanded = {},

				// Assumes a single number if not a string
				parts = typeof value === "string" ? value.split(" ") : [ value ];

			for ( ; i < 4; i++ ) {
				expanded[ prefix + cssExpand[ i ] + suffix ] =
					parts[ i ] || parts[ i - 2 ] || parts[ 0 ];
			}

			return expanded;
		}
	};

	if ( !rmargin.test( prefix ) ) {
		jQuery.cssHooks[ prefix + suffix ].set = setPositiveNumber;
	}
});

jQuery.fn.extend({
	css: function( name, value ) {
		return access( this, function( elem, name, value ) {
			var styles, len,
				map = {},
				i = 0;

			if ( jQuery.isArray( name ) ) {
				styles = getStyles( elem );
				len = name.length;

				for ( ; i < len; i++ ) {
					map[ name[ i ] ] = jQuery.css( elem, name[ i ], false, styles );
				}

				return map;
			}

			return value !== undefined ?
				jQuery.style( elem, name, value ) :
				jQuery.css( elem, name );
		}, name, value, arguments.length > 1 );
	},
	show: function() {
		return showHide( this, true );
	},
	hide: function() {
		return showHide( this );
	},
	toggle: function( state ) {
		if ( typeof state === "boolean" ) {
			return state ? this.show() : this.hide();
		}

		return this.each(function() {
			if ( isHidden( this ) ) {
				jQuery( this ).show();
			} else {
				jQuery( this ).hide();
			}
		});
	}
});


function Tween( elem, options, prop, end, easing ) {
	return new Tween.prototype.init( elem, options, prop, end, easing );
}
jQuery.Tween = Tween;

Tween.prototype = {
	constructor: Tween,
	init: function( elem, options, prop, end, easing, unit ) {
		this.elem = elem;
		this.prop = prop;
		this.easing = easing || "swing";
		this.options = options;
		this.start = this.now = this.cur();
		this.end = end;
		this.unit = unit || ( jQuery.cssNumber[ prop ] ? "" : "px" );
	},
	cur: function() {
		var hooks = Tween.propHooks[ this.prop ];

		return hooks && hooks.get ?
			hooks.get( this ) :
			Tween.propHooks._default.get( this );
	},
	run: function( percent ) {
		var eased,
			hooks = Tween.propHooks[ this.prop ];

		if ( this.options.duration ) {
			this.pos = eased = jQuery.easing[ this.easing ](
				percent, this.options.duration * percent, 0, 1, this.options.duration
			);
		} else {
			this.pos = eased = percent;
		}
		this.now = ( this.end - this.start ) * eased + this.start;

		if ( this.options.step ) {
			this.options.step.call( this.elem, this.now, this );
		}

		if ( hooks && hooks.set ) {
			hooks.set( this );
		} else {
			Tween.propHooks._default.set( this );
		}
		return this;
	}
};

Tween.prototype.init.prototype = Tween.prototype;

Tween.propHooks = {
	_default: {
		get: function( tween ) {
			var result;

			if ( tween.elem[ tween.prop ] != null &&
				(!tween.elem.style || tween.elem.style[ tween.prop ] == null) ) {
				return tween.elem[ tween.prop ];
			}

			// Passing an empty string as a 3rd parameter to .css will automatically
			// attempt a parseFloat and fallback to a string if the parse fails.
			// Simple values such as "10px" are parsed to Float;
			// complex values such as "rotate(1rad)" are returned as-is.
			result = jQuery.css( tween.elem, tween.prop, "" );
			// Empty strings, null, undefined and "auto" are converted to 0.
			return !result || result === "auto" ? 0 : result;
		},
		set: function( tween ) {
			// Use step hook for back compat.
			// Use cssHook if its there.
			// Use .style if available and use plain properties where available.
			if ( jQuery.fx.step[ tween.prop ] ) {
				jQuery.fx.step[ tween.prop ]( tween );
			} else if ( tween.elem.style && ( tween.elem.style[ jQuery.cssProps[ tween.prop ] ] != null || jQuery.cssHooks[ tween.prop ] ) ) {
				jQuery.style( tween.elem, tween.prop, tween.now + tween.unit );
			} else {
				tween.elem[ tween.prop ] = tween.now;
			}
		}
	}
};

// Support: IE9
// Panic based approach to setting things on disconnected nodes
Tween.propHooks.scrollTop = Tween.propHooks.scrollLeft = {
	set: function( tween ) {
		if ( tween.elem.nodeType && tween.elem.parentNode ) {
			tween.elem[ tween.prop ] = tween.now;
		}
	}
};

jQuery.easing = {
	linear: function( p ) {
		return p;
	},
	swing: function( p ) {
		return 0.5 - Math.cos( p * Math.PI ) / 2;
	}
};

jQuery.fx = Tween.prototype.init;

// Back Compat <1.8 extension point
jQuery.fx.step = {};




var
	fxNow, timerId,
	rfxtypes = /^(?:toggle|show|hide)$/,
	rfxnum = new RegExp( "^(?:([+-])=|)(" + pnum + ")([a-z%]*)$", "i" ),
	rrun = /queueHooks$/,
	animationPrefilters = [ defaultPrefilter ],
	tweeners = {
		"*": [ function( prop, value ) {
			var tween = this.createTween( prop, value ),
				target = tween.cur(),
				parts = rfxnum.exec( value ),
				unit = parts && parts[ 3 ] || ( jQuery.cssNumber[ prop ] ? "" : "px" ),

				// Starting value computation is required for potential unit mismatches
				start = ( jQuery.cssNumber[ prop ] || unit !== "px" && +target ) &&
					rfxnum.exec( jQuery.css( tween.elem, prop ) ),
				scale = 1,
				maxIterations = 20;

			if ( start && start[ 3 ] !== unit ) {
				// Trust units reported by jQuery.css
				unit = unit || start[ 3 ];

				// Make sure we update the tween properties later on
				parts = parts || [];

				// Iteratively approximate from a nonzero starting point
				start = +target || 1;

				do {
					// If previous iteration zeroed out, double until we get *something*.
					// Use string for doubling so we don't accidentally see scale as unchanged below
					scale = scale || ".5";

					// Adjust and apply
					start = start / scale;
					jQuery.style( tween.elem, prop, start + unit );

				// Update scale, tolerating zero or NaN from tween.cur(),
				// break the loop if scale is unchanged or perfect, or if we've just had enough
				} while ( scale !== (scale = tween.cur() / target) && scale !== 1 && --maxIterations );
			}

			// Update tween properties
			if ( parts ) {
				start = tween.start = +start || +target || 0;
				tween.unit = unit;
				// If a +=/-= token was provided, we're doing a relative animation
				tween.end = parts[ 1 ] ?
					start + ( parts[ 1 ] + 1 ) * parts[ 2 ] :
					+parts[ 2 ];
			}

			return tween;
		} ]
	};

// Animations created synchronously will run synchronously
function createFxNow() {
	setTimeout(function() {
		fxNow = undefined;
	});
	return ( fxNow = jQuery.now() );
}

// Generate parameters to create a standard animation
function genFx( type, includeWidth ) {
	var which,
		i = 0,
		attrs = { height: type };

	// If we include width, step value is 1 to do all cssExpand values,
	// otherwise step value is 2 to skip over Left and Right
	includeWidth = includeWidth ? 1 : 0;
	for ( ; i < 4 ; i += 2 - includeWidth ) {
		which = cssExpand[ i ];
		attrs[ "margin" + which ] = attrs[ "padding" + which ] = type;
	}

	if ( includeWidth ) {
		attrs.opacity = attrs.width = type;
	}

	return attrs;
}

function createTween( value, prop, animation ) {
	var tween,
		collection = ( tweeners[ prop ] || [] ).concat( tweeners[ "*" ] ),
		index = 0,
		length = collection.length;
	for ( ; index < length; index++ ) {
		if ( (tween = collection[ index ].call( animation, prop, value )) ) {

			// We're done with this property
			return tween;
		}
	}
}

function defaultPrefilter( elem, props, opts ) {
	/* jshint validthis: true */
	var prop, value, toggle, tween, hooks, oldfire, display, checkDisplay,
		anim = this,
		orig = {},
		style = elem.style,
		hidden = elem.nodeType && isHidden( elem ),
		dataShow = data_priv.get( elem, "fxshow" );

	// Handle queue: false promises
	if ( !opts.queue ) {
		hooks = jQuery._queueHooks( elem, "fx" );
		if ( hooks.unqueued == null ) {
			hooks.unqueued = 0;
			oldfire = hooks.empty.fire;
			hooks.empty.fire = function() {
				if ( !hooks.unqueued ) {
					oldfire();
				}
			};
		}
		hooks.unqueued++;

		anim.always(function() {
			// Ensure the complete handler is called before this completes
			anim.always(function() {
				hooks.unqueued--;
				if ( !jQuery.queue( elem, "fx" ).length ) {
					hooks.empty.fire();
				}
			});
		});
	}

	// Height/width overflow pass
	if ( elem.nodeType === 1 && ( "height" in props || "width" in props ) ) {
		// Make sure that nothing sneaks out
		// Record all 3 overflow attributes because IE9-10 do not
		// change the overflow attribute when overflowX and
		// overflowY are set to the same value
		opts.overflow = [ style.overflow, style.overflowX, style.overflowY ];

		// Set display property to inline-block for height/width
		// animations on inline elements that are having width/height animated
		display = jQuery.css( elem, "display" );

		// Test default display if display is currently "none"
		checkDisplay = display === "none" ?
			data_priv.get( elem, "olddisplay" ) || defaultDisplay( elem.nodeName ) : display;

		if ( checkDisplay === "inline" && jQuery.css( elem, "float" ) === "none" ) {
			style.display = "inline-block";
		}
	}

	if ( opts.overflow ) {
		style.overflow = "hidden";
		anim.always(function() {
			style.overflow = opts.overflow[ 0 ];
			style.overflowX = opts.overflow[ 1 ];
			style.overflowY = opts.overflow[ 2 ];
		});
	}

	// show/hide pass
	for ( prop in props ) {
		value = props[ prop ];
		if ( rfxtypes.exec( value ) ) {
			delete props[ prop ];
			toggle = toggle || value === "toggle";
			if ( value === ( hidden ? "hide" : "show" ) ) {

				// If there is dataShow left over from a stopped hide or show and we are going to proceed with show, we should pretend to be hidden
				if ( value === "show" && dataShow && dataShow[ prop ] !== undefined ) {
					hidden = true;
				} else {
					continue;
				}
			}
			orig[ prop ] = dataShow && dataShow[ prop ] || jQuery.style( elem, prop );

		// Any non-fx value stops us from restoring the original display value
		} else {
			display = undefined;
		}
	}

	if ( !jQuery.isEmptyObject( orig ) ) {
		if ( dataShow ) {
			if ( "hidden" in dataShow ) {
				hidden = dataShow.hidden;
			}
		} else {
			dataShow = data_priv.access( elem, "fxshow", {} );
		}

		// Store state if its toggle - enables .stop().toggle() to "reverse"
		if ( toggle ) {
			dataShow.hidden = !hidden;
		}
		if ( hidden ) {
			jQuery( elem ).show();
		} else {
			anim.done(function() {
				jQuery( elem ).hide();
			});
		}
		anim.done(function() {
			var prop;

			data_priv.remove( elem, "fxshow" );
			for ( prop in orig ) {
				jQuery.style( elem, prop, orig[ prop ] );
			}
		});
		for ( prop in orig ) {
			tween = createTween( hidden ? dataShow[ prop ] : 0, prop, anim );

			if ( !( prop in dataShow ) ) {
				dataShow[ prop ] = tween.start;
				if ( hidden ) {
					tween.end = tween.start;
					tween.start = prop === "width" || prop === "height" ? 1 : 0;
				}
			}
		}

	// If this is a noop like .hide().hide(), restore an overwritten display value
	} else if ( (display === "none" ? defaultDisplay( elem.nodeName ) : display) === "inline" ) {
		style.display = display;
	}
}

function propFilter( props, specialEasing ) {
	var index, name, easing, value, hooks;

	// camelCase, specialEasing and expand cssHook pass
	for ( index in props ) {
		name = jQuery.camelCase( index );
		easing = specialEasing[ name ];
		value = props[ index ];
		if ( jQuery.isArray( value ) ) {
			easing = value[ 1 ];
			value = props[ index ] = value[ 0 ];
		}

		if ( index !== name ) {
			props[ name ] = value;
			delete props[ index ];
		}

		hooks = jQuery.cssHooks[ name ];
		if ( hooks && "expand" in hooks ) {
			value = hooks.expand( value );
			delete props[ name ];

			// Not quite $.extend, this won't overwrite existing keys.
			// Reusing 'index' because we have the correct "name"
			for ( index in value ) {
				if ( !( index in props ) ) {
					props[ index ] = value[ index ];
					specialEasing[ index ] = easing;
				}
			}
		} else {
			specialEasing[ name ] = easing;
		}
	}
}

function Animation( elem, properties, options ) {
	var result,
		stopped,
		index = 0,
		length = animationPrefilters.length,
		deferred = jQuery.Deferred().always( function() {
			// Don't match elem in the :animated selector
			delete tick.elem;
		}),
		tick = function() {
			if ( stopped ) {
				return false;
			}
			var currentTime = fxNow || createFxNow(),
				remaining = Math.max( 0, animation.startTime + animation.duration - currentTime ),
				// Support: Android 2.3
				// Archaic crash bug won't allow us to use `1 - ( 0.5 || 0 )` (#12497)
				temp = remaining / animation.duration || 0,
				percent = 1 - temp,
				index = 0,
				length = animation.tweens.length;

			for ( ; index < length ; index++ ) {
				animation.tweens[ index ].run( percent );
			}

			deferred.notifyWith( elem, [ animation, percent, remaining ]);

			if ( percent < 1 && length ) {
				return remaining;
			} else {
				deferred.resolveWith( elem, [ animation ] );
				return false;
			}
		},
		animation = deferred.promise({
			elem: elem,
			props: jQuery.extend( {}, properties ),
			opts: jQuery.extend( true, { specialEasing: {} }, options ),
			originalProperties: properties,
			originalOptions: options,
			startTime: fxNow || createFxNow(),
			duration: options.duration,
			tweens: [],
			createTween: function( prop, end ) {
				var tween = jQuery.Tween( elem, animation.opts, prop, end,
						animation.opts.specialEasing[ prop ] || animation.opts.easing );
				animation.tweens.push( tween );
				return tween;
			},
			stop: function( gotoEnd ) {
				var index = 0,
					// If we are going to the end, we want to run all the tweens
					// otherwise we skip this part
					length = gotoEnd ? animation.tweens.length : 0;
				if ( stopped ) {
					return this;
				}
				stopped = true;
				for ( ; index < length ; index++ ) {
					animation.tweens[ index ].run( 1 );
				}

				// Resolve when we played the last frame; otherwise, reject
				if ( gotoEnd ) {
					deferred.resolveWith( elem, [ animation, gotoEnd ] );
				} else {
					deferred.rejectWith( elem, [ animation, gotoEnd ] );
				}
				return this;
			}
		}),
		props = animation.props;

	propFilter( props, animation.opts.specialEasing );

	for ( ; index < length ; index++ ) {
		result = animationPrefilters[ index ].call( animation, elem, props, animation.opts );
		if ( result ) {
			return result;
		}
	}

	jQuery.map( props, createTween, animation );

	if ( jQuery.isFunction( animation.opts.start ) ) {
		animation.opts.start.call( elem, animation );
	}

	jQuery.fx.timer(
		jQuery.extend( tick, {
			elem: elem,
			anim: animation,
			queue: animation.opts.queue
		})
	);

	// attach callbacks from options
	return animation.progress( animation.opts.progress )
		.done( animation.opts.done, animation.opts.complete )
		.fail( animation.opts.fail )
		.always( animation.opts.always );
}

jQuery.Animation = jQuery.extend( Animation, {

	tweener: function( props, callback ) {
		if ( jQuery.isFunction( props ) ) {
			callback = props;
			props = [ "*" ];
		} else {
			props = props.split(" ");
		}

		var prop,
			index = 0,
			length = props.length;

		for ( ; index < length ; index++ ) {
			prop = props[ index ];
			tweeners[ prop ] = tweeners[ prop ] || [];
			tweeners[ prop ].unshift( callback );
		}
	},

	prefilter: function( callback, prepend ) {
		if ( prepend ) {
			animationPrefilters.unshift( callback );
		} else {
			animationPrefilters.push( callback );
		}
	}
});

jQuery.speed = function( speed, easing, fn ) {
	var opt = speed && typeof speed === "object" ? jQuery.extend( {}, speed ) : {
		complete: fn || !fn && easing ||
			jQuery.isFunction( speed ) && speed,
		duration: speed,
		easing: fn && easing || easing && !jQuery.isFunction( easing ) && easing
	};

	opt.duration = jQuery.fx.off ? 0 : typeof opt.duration === "number" ? opt.duration :
		opt.duration in jQuery.fx.speeds ? jQuery.fx.speeds[ opt.duration ] : jQuery.fx.speeds._default;

	// Normalize opt.queue - true/undefined/null -> "fx"
	if ( opt.queue == null || opt.queue === true ) {
		opt.queue = "fx";
	}

	// Queueing
	opt.old = opt.complete;

	opt.complete = function() {
		if ( jQuery.isFunction( opt.old ) ) {
			opt.old.call( this );
		}

		if ( opt.queue ) {
			jQuery.dequeue( this, opt.queue );
		}
	};

	return opt;
};

jQuery.fn.extend({
	fadeTo: function( speed, to, easing, callback ) {

		// Show any hidden elements after setting opacity to 0
		return this.filter( isHidden ).css( "opacity", 0 ).show()

			// Animate to the value specified
			.end().animate({ opacity: to }, speed, easing, callback );
	},
	animate: function( prop, speed, easing, callback ) {
		var empty = jQuery.isEmptyObject( prop ),
			optall = jQuery.speed( speed, easing, callback ),
			doAnimation = function() {
				// Operate on a copy of prop so per-property easing won't be lost
				var anim = Animation( this, jQuery.extend( {}, prop ), optall );

				// Empty animations, or finishing resolves immediately
				if ( empty || data_priv.get( this, "finish" ) ) {
					anim.stop( true );
				}
			};
			doAnimation.finish = doAnimation;

		return empty || optall.queue === false ?
			this.each( doAnimation ) :
			this.queue( optall.queue, doAnimation );
	},
	stop: function( type, clearQueue, gotoEnd ) {
		var stopQueue = function( hooks ) {
			var stop = hooks.stop;
			delete hooks.stop;
			stop( gotoEnd );
		};

		if ( typeof type !== "string" ) {
			gotoEnd = clearQueue;
			clearQueue = type;
			type = undefined;
		}
		if ( clearQueue && type !== false ) {
			this.queue( type || "fx", [] );
		}

		return this.each(function() {
			var dequeue = true,
				index = type != null && type + "queueHooks",
				timers = jQuery.timers,
				data = data_priv.get( this );

			if ( index ) {
				if ( data[ index ] && data[ index ].stop ) {
					stopQueue( data[ index ] );
				}
			} else {
				for ( index in data ) {
					if ( data[ index ] && data[ index ].stop && rrun.test( index ) ) {
						stopQueue( data[ index ] );
					}
				}
			}

			for ( index = timers.length; index--; ) {
				if ( timers[ index ].elem === this && (type == null || timers[ index ].queue === type) ) {
					timers[ index ].anim.stop( gotoEnd );
					dequeue = false;
					timers.splice( index, 1 );
				}
			}

			// Start the next in the queue if the last step wasn't forced.
			// Timers currently will call their complete callbacks, which
			// will dequeue but only if they were gotoEnd.
			if ( dequeue || !gotoEnd ) {
				jQuery.dequeue( this, type );
			}
		});
	},
	finish: function( type ) {
		if ( type !== false ) {
			type = type || "fx";
		}
		return this.each(function() {
			var index,
				data = data_priv.get( this ),
				queue = data[ type + "queue" ],
				hooks = data[ type + "queueHooks" ],
				timers = jQuery.timers,
				length = queue ? queue.length : 0;

			// Enable finishing flag on private data
			data.finish = true;

			// Empty the queue first
			jQuery.queue( this, type, [] );

			if ( hooks && hooks.stop ) {
				hooks.stop.call( this, true );
			}

			// Look for any active animations, and finish them
			for ( index = timers.length; index--; ) {
				if ( timers[ index ].elem === this && timers[ index ].queue === type ) {
					timers[ index ].anim.stop( true );
					timers.splice( index, 1 );
				}
			}

			// Look for any animations in the old queue and finish them
			for ( index = 0; index < length; index++ ) {
				if ( queue[ index ] && queue[ index ].finish ) {
					queue[ index ].finish.call( this );
				}
			}

			// Turn off finishing flag
			delete data.finish;
		});
	}
});

jQuery.each([ "toggle", "show", "hide" ], function( i, name ) {
	var cssFn = jQuery.fn[ name ];
	jQuery.fn[ name ] = function( speed, easing, callback ) {
		return speed == null || typeof speed === "boolean" ?
			cssFn.apply( this, arguments ) :
			this.animate( genFx( name, true ), speed, easing, callback );
	};
});

// Generate shortcuts for custom animations
jQuery.each({
	slideDown: genFx("show"),
	slideUp: genFx("hide"),
	slideToggle: genFx("toggle"),
	fadeIn: { opacity: "show" },
	fadeOut: { opacity: "hide" },
	fadeToggle: { opacity: "toggle" }
}, function( name, props ) {
	jQuery.fn[ name ] = function( speed, easing, callback ) {
		return this.animate( props, speed, easing, callback );
	};
});

jQuery.timers = [];
jQuery.fx.tick = function() {
	var timer,
		i = 0,
		timers = jQuery.timers;

	fxNow = jQuery.now();

	for ( ; i < timers.length; i++ ) {
		timer = timers[ i ];
		// Checks the timer has not already been removed
		if ( !timer() && timers[ i ] === timer ) {
			timers.splice( i--, 1 );
		}
	}

	if ( !timers.length ) {
		jQuery.fx.stop();
	}
	fxNow = undefined;
};

jQuery.fx.timer = function( timer ) {
	jQuery.timers.push( timer );
	if ( timer() ) {
		jQuery.fx.start();
	} else {
		jQuery.timers.pop();
	}
};

jQuery.fx.interval = 13;

jQuery.fx.start = function() {
	if ( !timerId ) {
		timerId = setInterval( jQuery.fx.tick, jQuery.fx.interval );
	}
};

jQuery.fx.stop = function() {
	clearInterval( timerId );
	timerId = null;
};

jQuery.fx.speeds = {
	slow: 600,
	fast: 200,
	// Default speed
	_default: 400
};


// Based off of the plugin by Clint Helfers, with permission.
// http://blindsignals.com/index.php/2009/07/jquery-delay/
jQuery.fn.delay = function( time, type ) {
	time = jQuery.fx ? jQuery.fx.speeds[ time ] || time : time;
	type = type || "fx";

	return this.queue( type, function( next, hooks ) {
		var timeout = setTimeout( next, time );
		hooks.stop = function() {
			clearTimeout( timeout );
		};
	});
};


(function() {
	var input = document.createElement( "input" ),
		select = document.createElement( "select" ),
		opt = select.appendChild( document.createElement( "option" ) );

	input.type = "checkbox";

	// Support: iOS<=5.1, Android<=4.2+
	// Default value for a checkbox should be "on"
	support.checkOn = input.value !== "";

	// Support: IE<=11+
	// Must access selectedIndex to make default options select
	support.optSelected = opt.selected;

	// Support: Android<=2.3
	// Options inside disabled selects are incorrectly marked as disabled
	select.disabled = true;
	support.optDisabled = !opt.disabled;

	// Support: IE<=11+
	// An input loses its value after becoming a radio
	input = document.createElement( "input" );
	input.value = "t";
	input.type = "radio";
	support.radioValue = input.value === "t";
})();


var nodeHook, boolHook,
	attrHandle = jQuery.expr.attrHandle;

jQuery.fn.extend({
	attr: function( name, value ) {
		return access( this, jQuery.attr, name, value, arguments.length > 1 );
	},

	removeAttr: function( name ) {
		return this.each(function() {
			jQuery.removeAttr( this, name );
		});
	}
});

jQuery.extend({
	attr: function( elem, name, value ) {
		var hooks, ret,
			nType = elem.nodeType;

		// don't get/set attributes on text, comment and attribute nodes
		if ( !elem || nType === 3 || nType === 8 || nType === 2 ) {
			return;
		}

		// Fallback to prop when attributes are not supported
		if ( typeof elem.getAttribute === strundefined ) {
			return jQuery.prop( elem, name, value );
		}

		// All attributes are lowercase
		// Grab necessary hook if one is defined
		if ( nType !== 1 || !jQuery.isXMLDoc( elem ) ) {
			name = name.toLowerCase();
			hooks = jQuery.attrHooks[ name ] ||
				( jQuery.expr.match.bool.test( name ) ? boolHook : nodeHook );
		}

		if ( value !== undefined ) {

			if ( value === null ) {
				jQuery.removeAttr( elem, name );

			} else if ( hooks && "set" in hooks && (ret = hooks.set( elem, value, name )) !== undefined ) {
				return ret;

			} else {
				elem.setAttribute( name, value + "" );
				return value;
			}

		} else if ( hooks && "get" in hooks && (ret = hooks.get( elem, name )) !== null ) {
			return ret;

		} else {
			ret = jQuery.find.attr( elem, name );

			// Non-existent attributes return null, we normalize to undefined
			return ret == null ?
				undefined :
				ret;
		}
	},

	removeAttr: function( elem, value ) {
		var name, propName,
			i = 0,
			attrNames = value && value.match( rnotwhite );

		if ( attrNames && elem.nodeType === 1 ) {
			while ( (name = attrNames[i++]) ) {
				propName = jQuery.propFix[ name ] || name;

				// Boolean attributes get special treatment (#10870)
				if ( jQuery.expr.match.bool.test( name ) ) {
					// Set corresponding property to false
					elem[ propName ] = false;
				}

				elem.removeAttribute( name );
			}
		}
	},

	attrHooks: {
		type: {
			set: function( elem, value ) {
				if ( !support.radioValue && value === "radio" &&
					jQuery.nodeName( elem, "input" ) ) {
					var val = elem.value;
					elem.setAttribute( "type", value );
					if ( val ) {
						elem.value = val;
					}
					return value;
				}
			}
		}
	}
});

// Hooks for boolean attributes
boolHook = {
	set: function( elem, value, name ) {
		if ( value === false ) {
			// Remove boolean attributes when set to false
			jQuery.removeAttr( elem, name );
		} else {
			elem.setAttribute( name, name );
		}
		return name;
	}
};
jQuery.each( jQuery.expr.match.bool.source.match( /\w+/g ), function( i, name ) {
	var getter = attrHandle[ name ] || jQuery.find.attr;

	attrHandle[ name ] = function( elem, name, isXML ) {
		var ret, handle;
		if ( !isXML ) {
			// Avoid an infinite loop by temporarily removing this function from the getter
			handle = attrHandle[ name ];
			attrHandle[ name ] = ret;
			ret = getter( elem, name, isXML ) != null ?
				name.toLowerCase() :
				null;
			attrHandle[ name ] = handle;
		}
		return ret;
	};
});




var rfocusable = /^(?:input|select|textarea|button)$/i;

jQuery.fn.extend({
	prop: function( name, value ) {
		return access( this, jQuery.prop, name, value, arguments.length > 1 );
	},

	removeProp: function( name ) {
		return this.each(function() {
			delete this[ jQuery.propFix[ name ] || name ];
		});
	}
});

jQuery.extend({
	propFix: {
		"for": "htmlFor",
		"class": "className"
	},

	prop: function( elem, name, value ) {
		var ret, hooks, notxml,
			nType = elem.nodeType;

		// Don't get/set properties on text, comment and attribute nodes
		if ( !elem || nType === 3 || nType === 8 || nType === 2 ) {
			return;
		}

		notxml = nType !== 1 || !jQuery.isXMLDoc( elem );

		if ( notxml ) {
			// Fix name and attach hooks
			name = jQuery.propFix[ name ] || name;
			hooks = jQuery.propHooks[ name ];
		}

		if ( value !== undefined ) {
			return hooks && "set" in hooks && (ret = hooks.set( elem, value, name )) !== undefined ?
				ret :
				( elem[ name ] = value );

		} else {
			return hooks && "get" in hooks && (ret = hooks.get( elem, name )) !== null ?
				ret :
				elem[ name ];
		}
	},

	propHooks: {
		tabIndex: {
			get: function( elem ) {
				return elem.hasAttribute( "tabindex" ) || rfocusable.test( elem.nodeName ) || elem.href ?
					elem.tabIndex :
					-1;
			}
		}
	}
});

if ( !support.optSelected ) {
	jQuery.propHooks.selected = {
		get: function( elem ) {
			var parent = elem.parentNode;
			if ( parent && parent.parentNode ) {
				parent.parentNode.selectedIndex;
			}
			return null;
		}
	};
}

jQuery.each([
	"tabIndex",
	"readOnly",
	"maxLength",
	"cellSpacing",
	"cellPadding",
	"rowSpan",
	"colSpan",
	"useMap",
	"frameBorder",
	"contentEditable"
], function() {
	jQuery.propFix[ this.toLowerCase() ] = this;
});




var rclass = /[\t\r\n\f]/g;

jQuery.fn.extend({
	addClass: function( value ) {
		var classes, elem, cur, clazz, j, finalValue,
			proceed = typeof value === "string" && value,
			i = 0,
			len = this.length;

		if ( jQuery.isFunction( value ) ) {
			return this.each(function( j ) {
				jQuery( this ).addClass( value.call( this, j, this.className ) );
			});
		}

		if ( proceed ) {
			// The disjunction here is for better compressibility (see removeClass)
			classes = ( value || "" ).match( rnotwhite ) || [];

			for ( ; i < len; i++ ) {
				elem = this[ i ];
				cur = elem.nodeType === 1 && ( elem.className ?
					( " " + elem.className + " " ).replace( rclass, " " ) :
					" "
				);

				if ( cur ) {
					j = 0;
					while ( (clazz = classes[j++]) ) {
						if ( cur.indexOf( " " + clazz + " " ) < 0 ) {
							cur += clazz + " ";
						}
					}

					// only assign if different to avoid unneeded rendering.
					finalValue = jQuery.trim( cur );
					if ( elem.className !== finalValue ) {
						elem.className = finalValue;
					}
				}
			}
		}

		return this;
	},

	removeClass: function( value ) {
		var classes, elem, cur, clazz, j, finalValue,
			proceed = arguments.length === 0 || typeof value === "string" && value,
			i = 0,
			len = this.length;

		if ( jQuery.isFunction( value ) ) {
			return this.each(function( j ) {
				jQuery( this ).removeClass( value.call( this, j, this.className ) );
			});
		}
		if ( proceed ) {
			classes = ( value || "" ).match( rnotwhite ) || [];

			for ( ; i < len; i++ ) {
				elem = this[ i ];
				// This expression is here for better compressibility (see addClass)
				cur = elem.nodeType === 1 && ( elem.className ?
					( " " + elem.className + " " ).replace( rclass, " " ) :
					""
				);

				if ( cur ) {
					j = 0;
					while ( (clazz = classes[j++]) ) {
						// Remove *all* instances
						while ( cur.indexOf( " " + clazz + " " ) >= 0 ) {
							cur = cur.replace( " " + clazz + " ", " " );
						}
					}

					// Only assign if different to avoid unneeded rendering.
					finalValue = value ? jQuery.trim( cur ) : "";
					if ( elem.className !== finalValue ) {
						elem.className = finalValue;
					}
				}
			}
		}

		return this;
	},

	toggleClass: function( value, stateVal ) {
		var type = typeof value;

		if ( typeof stateVal === "boolean" && type === "string" ) {
			return stateVal ? this.addClass( value ) : this.removeClass( value );
		}

		if ( jQuery.isFunction( value ) ) {
			return this.each(function( i ) {
				jQuery( this ).toggleClass( value.call(this, i, this.className, stateVal), stateVal );
			});
		}

		return this.each(function() {
			if ( type === "string" ) {
				// Toggle individual class names
				var className,
					i = 0,
					self = jQuery( this ),
					classNames = value.match( rnotwhite ) || [];

				while ( (className = classNames[ i++ ]) ) {
					// Check each className given, space separated list
					if ( self.hasClass( className ) ) {
						self.removeClass( className );
					} else {
						self.addClass( className );
					}
				}

			// Toggle whole class name
			} else if ( type === strundefined || type === "boolean" ) {
				if ( this.className ) {
					// store className if set
					data_priv.set( this, "__className__", this.className );
				}

				// If the element has a class name or if we're passed `false`,
				// then remove the whole classname (if there was one, the above saved it).
				// Otherwise bring back whatever was previously saved (if anything),
				// falling back to the empty string if nothing was stored.
				this.className = this.className || value === false ? "" : data_priv.get( this, "__className__" ) || "";
			}
		});
	},

	hasClass: function( selector ) {
		var className = " " + selector + " ",
			i = 0,
			l = this.length;
		for ( ; i < l; i++ ) {
			if ( this[i].nodeType === 1 && (" " + this[i].className + " ").replace(rclass, " ").indexOf( className ) >= 0 ) {
				return true;
			}
		}

		return false;
	}
});




var rreturn = /\r/g;

jQuery.fn.extend({
	val: function( value ) {
		var hooks, ret, isFunction,
			elem = this[0];

		if ( !arguments.length ) {
			if ( elem ) {
				hooks = jQuery.valHooks[ elem.type ] || jQuery.valHooks[ elem.nodeName.toLowerCase() ];

				if ( hooks && "get" in hooks && (ret = hooks.get( elem, "value" )) !== undefined ) {
					return ret;
				}

				ret = elem.value;

				return typeof ret === "string" ?
					// Handle most common string cases
					ret.replace(rreturn, "") :
					// Handle cases where value is null/undef or number
					ret == null ? "" : ret;
			}

			return;
		}

		isFunction = jQuery.isFunction( value );

		return this.each(function( i ) {
			var val;

			if ( this.nodeType !== 1 ) {
				return;
			}

			if ( isFunction ) {
				val = value.call( this, i, jQuery( this ).val() );
			} else {
				val = value;
			}

			// Treat null/undefined as ""; convert numbers to string
			if ( val == null ) {
				val = "";

			} else if ( typeof val === "number" ) {
				val += "";

			} else if ( jQuery.isArray( val ) ) {
				val = jQuery.map( val, function( value ) {
					return value == null ? "" : value + "";
				});
			}

			hooks = jQuery.valHooks[ this.type ] || jQuery.valHooks[ this.nodeName.toLowerCase() ];

			// If set returns undefined, fall back to normal setting
			if ( !hooks || !("set" in hooks) || hooks.set( this, val, "value" ) === undefined ) {
				this.value = val;
			}
		});
	}
});

jQuery.extend({
	valHooks: {
		option: {
			get: function( elem ) {
				var val = jQuery.find.attr( elem, "value" );
				return val != null ?
					val :
					// Support: IE10-11+
					// option.text throws exceptions (#14686, #14858)
					jQuery.trim( jQuery.text( elem ) );
			}
		},
		select: {
			get: function( elem ) {
				var value, option,
					options = elem.options,
					index = elem.selectedIndex,
					one = elem.type === "select-one" || index < 0,
					values = one ? null : [],
					max = one ? index + 1 : options.length,
					i = index < 0 ?
						max :
						one ? index : 0;

				// Loop through all the selected options
				for ( ; i < max; i++ ) {
					option = options[ i ];

					// IE6-9 doesn't update selected after form reset (#2551)
					if ( ( option.selected || i === index ) &&
							// Don't return options that are disabled or in a disabled optgroup
							( support.optDisabled ? !option.disabled : option.getAttribute( "disabled" ) === null ) &&
							( !option.parentNode.disabled || !jQuery.nodeName( option.parentNode, "optgroup" ) ) ) {

						// Get the specific value for the option
						value = jQuery( option ).val();

						// We don't need an array for one selects
						if ( one ) {
							return value;
						}

						// Multi-Selects return an array
						values.push( value );
					}
				}

				return values;
			},

			set: function( elem, value ) {
				var optionSet, option,
					options = elem.options,
					values = jQuery.makeArray( value ),
					i = options.length;

				while ( i-- ) {
					option = options[ i ];
					if ( (option.selected = jQuery.inArray( option.value, values ) >= 0) ) {
						optionSet = true;
					}
				}

				// Force browsers to behave consistently when non-matching value is set
				if ( !optionSet ) {
					elem.selectedIndex = -1;
				}
				return values;
			}
		}
	}
});

// Radios and checkboxes getter/setter
jQuery.each([ "radio", "checkbox" ], function() {
	jQuery.valHooks[ this ] = {
		set: function( elem, value ) {
			if ( jQuery.isArray( value ) ) {
				return ( elem.checked = jQuery.inArray( jQuery(elem).val(), value ) >= 0 );
			}
		}
	};
	if ( !support.checkOn ) {
		jQuery.valHooks[ this ].get = function( elem ) {
			return elem.getAttribute("value") === null ? "on" : elem.value;
		};
	}
});




// Return jQuery for attributes-only inclusion


jQuery.each( ("blur focus focusin focusout load resize scroll unload click dblclick " +
	"mousedown mouseup mousemove mouseover mouseout mouseenter mouseleave " +
	"change select submit keydown keypress keyup error contextmenu").split(" "), function( i, name ) {

	// Handle event binding
	jQuery.fn[ name ] = function( data, fn ) {
		return arguments.length > 0 ?
			this.on( name, null, data, fn ) :
			this.trigger( name );
	};
});

jQuery.fn.extend({
	hover: function( fnOver, fnOut ) {
		return this.mouseenter( fnOver ).mouseleave( fnOut || fnOver );
	},

	bind: function( types, data, fn ) {
		return this.on( types, null, data, fn );
	},
	unbind: function( types, fn ) {
		return this.off( types, null, fn );
	},

	delegate: function( selector, types, data, fn ) {
		return this.on( types, selector, data, fn );
	},
	undelegate: function( selector, types, fn ) {
		// ( namespace ) or ( selector, types [, fn] )
		return arguments.length === 1 ? this.off( selector, "**" ) : this.off( types, selector || "**", fn );
	}
});


var nonce = jQuery.now();

var rquery = (/\?/);



// Support: Android 2.3
// Workaround failure to string-cast null input
jQuery.parseJSON = function( data ) {
	return JSON.parse( data + "" );
};


// Cross-browser xml parsing
jQuery.parseXML = function( data ) {
	var xml, tmp;
	if ( !data || typeof data !== "string" ) {
		return null;
	}

	// Support: IE9
	try {
		tmp = new DOMParser();
		xml = tmp.parseFromString( data, "text/xml" );
	} catch ( e ) {
		xml = undefined;
	}

	if ( !xml || xml.getElementsByTagName( "parsererror" ).length ) {
		jQuery.error( "Invalid XML: " + data );
	}
	return xml;
};


var
	rhash = /#.*$/,
	rts = /([?&])_=[^&]*/,
	rheaders = /^(.*?):[ \t]*([^\r\n]*)$/mg,
	// #7653, #8125, #8152: local protocol detection
	rlocalProtocol = /^(?:about|app|app-storage|.+-extension|file|res|widget):$/,
	rnoContent = /^(?:GET|HEAD)$/,
	rprotocol = /^\/\//,
	rurl = /^([\w.+-]+:)(?:\/\/(?:[^\/?#]*@|)([^\/?#:]*)(?::(\d+)|)|)/,

	/* Prefilters
	 * 1) They are useful to introduce custom dataTypes (see ajax/jsonp.js for an example)
	 * 2) These are called:
	 *    - BEFORE asking for a transport
	 *    - AFTER param serialization (s.data is a string if s.processData is true)
	 * 3) key is the dataType
	 * 4) the catchall symbol "*" can be used
	 * 5) execution will start with transport dataType and THEN continue down to "*" if needed
	 */
	prefilters = {},

	/* Transports bindings
	 * 1) key is the dataType
	 * 2) the catchall symbol "*" can be used
	 * 3) selection will start with transport dataType and THEN go to "*" if needed
	 */
	transports = {},

	// Avoid comment-prolog char sequence (#10098); must appease lint and evade compression
	allTypes = "*/".concat( "*" ),

	// Document location
	ajaxLocation = window.location.href,

	// Segment location into parts
	ajaxLocParts = rurl.exec( ajaxLocation.toLowerCase() ) || [];

// Base "constructor" for jQuery.ajaxPrefilter and jQuery.ajaxTransport
function addToPrefiltersOrTransports( structure ) {

	// dataTypeExpression is optional and defaults to "*"
	return function( dataTypeExpression, func ) {

		if ( typeof dataTypeExpression !== "string" ) {
			func = dataTypeExpression;
			dataTypeExpression = "*";
		}

		var dataType,
			i = 0,
			dataTypes = dataTypeExpression.toLowerCase().match( rnotwhite ) || [];

		if ( jQuery.isFunction( func ) ) {
			// For each dataType in the dataTypeExpression
			while ( (dataType = dataTypes[i++]) ) {
				// Prepend if requested
				if ( dataType[0] === "+" ) {
					dataType = dataType.slice( 1 ) || "*";
					(structure[ dataType ] = structure[ dataType ] || []).unshift( func );

				// Otherwise append
				} else {
					(structure[ dataType ] = structure[ dataType ] || []).push( func );
				}
			}
		}
	};
}

// Base inspection function for prefilters and transports
function inspectPrefiltersOrTransports( structure, options, originalOptions, jqXHR ) {

	var inspected = {},
		seekingTransport = ( structure === transports );

	function inspect( dataType ) {
		var selected;
		inspected[ dataType ] = true;
		jQuery.each( structure[ dataType ] || [], function( _, prefilterOrFactory ) {
			var dataTypeOrTransport = prefilterOrFactory( options, originalOptions, jqXHR );
			if ( typeof dataTypeOrTransport === "string" && !seekingTransport && !inspected[ dataTypeOrTransport ] ) {
				options.dataTypes.unshift( dataTypeOrTransport );
				inspect( dataTypeOrTransport );
				return false;
			} else if ( seekingTransport ) {
				return !( selected = dataTypeOrTransport );
			}
		});
		return selected;
	}

	return inspect( options.dataTypes[ 0 ] ) || !inspected[ "*" ] && inspect( "*" );
}

// A special extend for ajax options
// that takes "flat" options (not to be deep extended)
// Fixes #9887
function ajaxExtend( target, src ) {
	var key, deep,
		flatOptions = jQuery.ajaxSettings.flatOptions || {};

	for ( key in src ) {
		if ( src[ key ] !== undefined ) {
			( flatOptions[ key ] ? target : ( deep || (deep = {}) ) )[ key ] = src[ key ];
		}
	}
	if ( deep ) {
		jQuery.extend( true, target, deep );
	}

	return target;
}

/* Handles responses to an ajax request:
 * - finds the right dataType (mediates between content-type and expected dataType)
 * - returns the corresponding response
 */
function ajaxHandleResponses( s, jqXHR, responses ) {

	var ct, type, finalDataType, firstDataType,
		contents = s.contents,
		dataTypes = s.dataTypes;

	// Remove auto dataType and get content-type in the process
	while ( dataTypes[ 0 ] === "*" ) {
		dataTypes.shift();
		if ( ct === undefined ) {
			ct = s.mimeType || jqXHR.getResponseHeader("Content-Type");
		}
	}

	// Check if we're dealing with a known content-type
	if ( ct ) {
		for ( type in contents ) {
			if ( contents[ type ] && contents[ type ].test( ct ) ) {
				dataTypes.unshift( type );
				break;
			}
		}
	}

	// Check to see if we have a response for the expected dataType
	if ( dataTypes[ 0 ] in responses ) {
		finalDataType = dataTypes[ 0 ];
	} else {
		// Try convertible dataTypes
		for ( type in responses ) {
			if ( !dataTypes[ 0 ] || s.converters[ type + " " + dataTypes[0] ] ) {
				finalDataType = type;
				break;
			}
			if ( !firstDataType ) {
				firstDataType = type;
			}
		}
		// Or just use first one
		finalDataType = finalDataType || firstDataType;
	}

	// If we found a dataType
	// We add the dataType to the list if needed
	// and return the corresponding response
	if ( finalDataType ) {
		if ( finalDataType !== dataTypes[ 0 ] ) {
			dataTypes.unshift( finalDataType );
		}
		return responses[ finalDataType ];
	}
}

/* Chain conversions given the request and the original response
 * Also sets the responseXXX fields on the jqXHR instance
 */
function ajaxConvert( s, response, jqXHR, isSuccess ) {
	var conv2, current, conv, tmp, prev,
		converters = {},
		// Work with a copy of dataTypes in case we need to modify it for conversion
		dataTypes = s.dataTypes.slice();

	// Create converters map with lowercased keys
	if ( dataTypes[ 1 ] ) {
		for ( conv in s.converters ) {
			converters[ conv.toLowerCase() ] = s.converters[ conv ];
		}
	}

	current = dataTypes.shift();

	// Convert to each sequential dataType
	while ( current ) {

		if ( s.responseFields[ current ] ) {
			jqXHR[ s.responseFields[ current ] ] = response;
		}

		// Apply the dataFilter if provided
		if ( !prev && isSuccess && s.dataFilter ) {
			response = s.dataFilter( response, s.dataType );
		}

		prev = current;
		current = dataTypes.shift();

		if ( current ) {

		// There's only work to do if current dataType is non-auto
			if ( current === "*" ) {

				current = prev;

			// Convert response if prev dataType is non-auto and differs from current
			} else if ( prev !== "*" && prev !== current ) {

				// Seek a direct converter
				conv = converters[ prev + " " + current ] || converters[ "* " + current ];

				// If none found, seek a pair
				if ( !conv ) {
					for ( conv2 in converters ) {

						// If conv2 outputs current
						tmp = conv2.split( " " );
						if ( tmp[ 1 ] === current ) {

							// If prev can be converted to accepted input
							conv = converters[ prev + " " + tmp[ 0 ] ] ||
								converters[ "* " + tmp[ 0 ] ];
							if ( conv ) {
								// Condense equivalence converters
								if ( conv === true ) {
									conv = converters[ conv2 ];

								// Otherwise, insert the intermediate dataType
								} else if ( converters[ conv2 ] !== true ) {
									current = tmp[ 0 ];
									dataTypes.unshift( tmp[ 1 ] );
								}
								break;
							}
						}
					}
				}

				// Apply converter (if not an equivalence)
				if ( conv !== true ) {

					// Unless errors are allowed to bubble, catch and return them
					if ( conv && s[ "throws" ] ) {
						response = conv( response );
					} else {
						try {
							response = conv( response );
						} catch ( e ) {
							return { state: "parsererror", error: conv ? e : "No conversion from " + prev + " to " + current };
						}
					}
				}
			}
		}
	}

	return { state: "success", data: response };
}

jQuery.extend({

	// Counter for holding the number of active queries
	active: 0,

	// Last-Modified header cache for next request
	lastModified: {},
	etag: {},

	ajaxSettings: {
		url: ajaxLocation,
		type: "GET",
		isLocal: rlocalProtocol.test( ajaxLocParts[ 1 ] ),
		global: true,
		processData: true,
		async: true,
		contentType: "application/x-www-form-urlencoded; charset=UTF-8",
		/*
		timeout: 0,
		data: null,
		dataType: null,
		username: null,
		password: null,
		cache: null,
		throws: false,
		traditional: false,
		headers: {},
		*/

		accepts: {
			"*": allTypes,
			text: "text/plain",
			html: "text/html",
			xml: "application/xml, text/xml",
			json: "application/json, text/javascript"
		},

		contents: {
			xml: /xml/,
			html: /html/,
			json: /json/
		},

		responseFields: {
			xml: "responseXML",
			text: "responseText",
			json: "responseJSON"
		},

		// Data converters
		// Keys separate source (or catchall "*") and destination types with a single space
		converters: {

			// Convert anything to text
			"* text": String,

			// Text to html (true = no transformation)
			"text html": true,

			// Evaluate text as a json expression
			"text json": jQuery.parseJSON,

			// Parse text as xml
			"text xml": jQuery.parseXML
		},

		// For options that shouldn't be deep extended:
		// you can add your own custom options here if
		// and when you create one that shouldn't be
		// deep extended (see ajaxExtend)
		flatOptions: {
			url: true,
			context: true
		}
	},

	// Creates a full fledged settings object into target
	// with both ajaxSettings and settings fields.
	// If target is omitted, writes into ajaxSettings.
	ajaxSetup: function( target, settings ) {
		return settings ?

			// Building a settings object
			ajaxExtend( ajaxExtend( target, jQuery.ajaxSettings ), settings ) :

			// Extending ajaxSettings
			ajaxExtend( jQuery.ajaxSettings, target );
	},

	ajaxPrefilter: addToPrefiltersOrTransports( prefilters ),
	ajaxTransport: addToPrefiltersOrTransports( transports ),

	// Main method
	ajax: function( url, options ) {

		// If url is an object, simulate pre-1.5 signature
		if ( typeof url === "object" ) {
			options = url;
			url = undefined;
		}

		// Force options to be an object
		options = options || {};

		var transport,
			// URL without anti-cache param
			cacheURL,
			// Response headers
			responseHeadersString,
			responseHeaders,
			// timeout handle
			timeoutTimer,
			// Cross-domain detection vars
			parts,
			// To know if global events are to be dispatched
			fireGlobals,
			// Loop variable
			i,
			// Create the final options object
			s = jQuery.ajaxSetup( {}, options ),
			// Callbacks context
			callbackContext = s.context || s,
			// Context for global events is callbackContext if it is a DOM node or jQuery collection
			globalEventContext = s.context && ( callbackContext.nodeType || callbackContext.jquery ) ?
				jQuery( callbackContext ) :
				jQuery.event,
			// Deferreds
			deferred = jQuery.Deferred(),
			completeDeferred = jQuery.Callbacks("once memory"),
			// Status-dependent callbacks
			statusCode = s.statusCode || {},
			// Headers (they are sent all at once)
			requestHeaders = {},
			requestHeadersNames = {},
			// The jqXHR state
			state = 0,
			// Default abort message
			strAbort = "canceled",
			// Fake xhr
			jqXHR = {
				readyState: 0,

				// Builds headers hashtable if needed
				getResponseHeader: function( key ) {
					var match;
					if ( state === 2 ) {
						if ( !responseHeaders ) {
							responseHeaders = {};
							while ( (match = rheaders.exec( responseHeadersString )) ) {
								responseHeaders[ match[1].toLowerCase() ] = match[ 2 ];
							}
						}
						match = responseHeaders[ key.toLowerCase() ];
					}
					return match == null ? null : match;
				},

				// Raw string
				getAllResponseHeaders: function() {
					return state === 2 ? responseHeadersString : null;
				},

				// Caches the header
				setRequestHeader: function( name, value ) {
					var lname = name.toLowerCase();
					if ( !state ) {
						name = requestHeadersNames[ lname ] = requestHeadersNames[ lname ] || name;
						requestHeaders[ name ] = value;
					}
					return this;
				},

				// Overrides response content-type header
				overrideMimeType: function( type ) {
					if ( !state ) {
						s.mimeType = type;
					}
					return this;
				},

				// Status-dependent callbacks
				statusCode: function( map ) {
					var code;
					if ( map ) {
						if ( state < 2 ) {
							for ( code in map ) {
								// Lazy-add the new callback in a way that preserves old ones
								statusCode[ code ] = [ statusCode[ code ], map[ code ] ];
							}
						} else {
							// Execute the appropriate callbacks
							jqXHR.always( map[ jqXHR.status ] );
						}
					}
					return this;
				},

				// Cancel the request
				abort: function( statusText ) {
					var finalText = statusText || strAbort;
					if ( transport ) {
						transport.abort( finalText );
					}
					done( 0, finalText );
					return this;
				}
			};

		// Attach deferreds
		deferred.promise( jqXHR ).complete = completeDeferred.add;
		jqXHR.success = jqXHR.done;
		jqXHR.error = jqXHR.fail;

		// Remove hash character (#7531: and string promotion)
		// Add protocol if not provided (prefilters might expect it)
		// Handle falsy url in the settings object (#10093: consistency with old signature)
		// We also use the url parameter if available
		s.url = ( ( url || s.url || ajaxLocation ) + "" ).replace( rhash, "" )
			.replace( rprotocol, ajaxLocParts[ 1 ] + "//" );

		// Alias method option to type as per ticket #12004
		s.type = options.method || options.type || s.method || s.type;

		// Extract dataTypes list
		s.dataTypes = jQuery.trim( s.dataType || "*" ).toLowerCase().match( rnotwhite ) || [ "" ];

		// A cross-domain request is in order when we have a protocol:host:port mismatch
		if ( s.crossDomain == null ) {
			parts = rurl.exec( s.url.toLowerCase() );
			s.crossDomain = !!( parts &&
				( parts[ 1 ] !== ajaxLocParts[ 1 ] || parts[ 2 ] !== ajaxLocParts[ 2 ] ||
					( parts[ 3 ] || ( parts[ 1 ] === "http:" ? "80" : "443" ) ) !==
						( ajaxLocParts[ 3 ] || ( ajaxLocParts[ 1 ] === "http:" ? "80" : "443" ) ) )
			);
		}

		// Convert data if not already a string
		if ( s.data && s.processData && typeof s.data !== "string" ) {
			s.data = jQuery.param( s.data, s.traditional );
		}

		// Apply prefilters
		inspectPrefiltersOrTransports( prefilters, s, options, jqXHR );

		// If request was aborted inside a prefilter, stop there
		if ( state === 2 ) {
			return jqXHR;
		}

		// We can fire global events as of now if asked to
		// Don't fire events if jQuery.event is undefined in an AMD-usage scenario (#15118)
		fireGlobals = jQuery.event && s.global;

		// Watch for a new set of requests
		if ( fireGlobals && jQuery.active++ === 0 ) {
			jQuery.event.trigger("ajaxStart");
		}

		// Uppercase the type
		s.type = s.type.toUpperCase();

		// Determine if request has content
		s.hasContent = !rnoContent.test( s.type );

		// Save the URL in case we're toying with the If-Modified-Since
		// and/or If-None-Match header later on
		cacheURL = s.url;

		// More options handling for requests with no content
		if ( !s.hasContent ) {

			// If data is available, append data to url
			if ( s.data ) {
				cacheURL = ( s.url += ( rquery.test( cacheURL ) ? "&" : "?" ) + s.data );
				// #9682: remove data so that it's not used in an eventual retry
				delete s.data;
			}

			// Add anti-cache in url if needed
			if ( s.cache === false ) {
				s.url = rts.test( cacheURL ) ?

					// If there is already a '_' parameter, set its value
					cacheURL.replace( rts, "$1_=" + nonce++ ) :

					// Otherwise add one to the end
					cacheURL + ( rquery.test( cacheURL ) ? "&" : "?" ) + "_=" + nonce++;
			}
		}

		// Set the If-Modified-Since and/or If-None-Match header, if in ifModified mode.
		if ( s.ifModified ) {
			if ( jQuery.lastModified[ cacheURL ] ) {
				jqXHR.setRequestHeader( "If-Modified-Since", jQuery.lastModified[ cacheURL ] );
			}
			if ( jQuery.etag[ cacheURL ] ) {
				jqXHR.setRequestHeader( "If-None-Match", jQuery.etag[ cacheURL ] );
			}
		}

		// Set the correct header, if data is being sent
		if ( s.data && s.hasContent && s.contentType !== false || options.contentType ) {
			jqXHR.setRequestHeader( "Content-Type", s.contentType );
		}

		// Set the Accepts header for the server, depending on the dataType
		jqXHR.setRequestHeader(
			"Accept",
			s.dataTypes[ 0 ] && s.accepts[ s.dataTypes[0] ] ?
				s.accepts[ s.dataTypes[0] ] + ( s.dataTypes[ 0 ] !== "*" ? ", " + allTypes + "; q=0.01" : "" ) :
				s.accepts[ "*" ]
		);

		// Check for headers option
		for ( i in s.headers ) {
			jqXHR.setRequestHeader( i, s.headers[ i ] );
		}

		// Allow custom headers/mimetypes and early abort
		if ( s.beforeSend && ( s.beforeSend.call( callbackContext, jqXHR, s ) === false || state === 2 ) ) {
			// Abort if not done already and return
			return jqXHR.abort();
		}

		// Aborting is no longer a cancellation
		strAbort = "abort";

		// Install callbacks on deferreds
		for ( i in { success: 1, error: 1, complete: 1 } ) {
			jqXHR[ i ]( s[ i ] );
		}

		// Get transport
		transport = inspectPrefiltersOrTransports( transports, s, options, jqXHR );

		// If no transport, we auto-abort
		if ( !transport ) {
			done( -1, "No Transport" );
		} else {
			jqXHR.readyState = 1;

			// Send global event
			if ( fireGlobals ) {
				globalEventContext.trigger( "ajaxSend", [ jqXHR, s ] );
			}
			// Timeout
			if ( s.async && s.timeout > 0 ) {
				timeoutTimer = setTimeout(function() {
					jqXHR.abort("timeout");
				}, s.timeout );
			}

			try {
				state = 1;
				transport.send( requestHeaders, done );
			} catch ( e ) {
				// Propagate exception as error if not done
				if ( state < 2 ) {
					done( -1, e );
				// Simply rethrow otherwise
				} else {
					throw e;
				}
			}
		}

		// Callback for when everything is done
		function done( status, nativeStatusText, responses, headers ) {
			var isSuccess, success, error, response, modified,
				statusText = nativeStatusText;

			// Called once
			if ( state === 2 ) {
				return;
			}

			// State is "done" now
			state = 2;

			// Clear timeout if it exists
			if ( timeoutTimer ) {
				clearTimeout( timeoutTimer );
			}

			// Dereference transport for early garbage collection
			// (no matter how long the jqXHR object will be used)
			transport = undefined;

			// Cache response headers
			responseHeadersString = headers || "";

			// Set readyState
			jqXHR.readyState = status > 0 ? 4 : 0;

			// Determine if successful
			isSuccess = status >= 200 && status < 300 || status === 304;

			// Get response data
			if ( responses ) {
				response = ajaxHandleResponses( s, jqXHR, responses );
			}

			// Convert no matter what (that way responseXXX fields are always set)
			response = ajaxConvert( s, response, jqXHR, isSuccess );

			// If successful, handle type chaining
			if ( isSuccess ) {

				// Set the If-Modified-Since and/or If-None-Match header, if in ifModified mode.
				if ( s.ifModified ) {
					modified = jqXHR.getResponseHeader("Last-Modified");
					if ( modified ) {
						jQuery.lastModified[ cacheURL ] = modified;
					}
					modified = jqXHR.getResponseHeader("etag");
					if ( modified ) {
						jQuery.etag[ cacheURL ] = modified;
					}
				}

				// if no content
				if ( status === 204 || s.type === "HEAD" ) {
					statusText = "nocontent";

				// if not modified
				} else if ( status === 304 ) {
					statusText = "notmodified";

				// If we have data, let's convert it
				} else {
					statusText = response.state;
					success = response.data;
					error = response.error;
					isSuccess = !error;
				}
			} else {
				// Extract error from statusText and normalize for non-aborts
				error = statusText;
				if ( status || !statusText ) {
					statusText = "error";
					if ( status < 0 ) {
						status = 0;
					}
				}
			}

			// Set data for the fake xhr object
			jqXHR.status = status;
			jqXHR.statusText = ( nativeStatusText || statusText ) + "";

			// Success/Error
			if ( isSuccess ) {
				deferred.resolveWith( callbackContext, [ success, statusText, jqXHR ] );
			} else {
				deferred.rejectWith( callbackContext, [ jqXHR, statusText, error ] );
			}

			// Status-dependent callbacks
			jqXHR.statusCode( statusCode );
			statusCode = undefined;

			if ( fireGlobals ) {
				globalEventContext.trigger( isSuccess ? "ajaxSuccess" : "ajaxError",
					[ jqXHR, s, isSuccess ? success : error ] );
			}

			// Complete
			completeDeferred.fireWith( callbackContext, [ jqXHR, statusText ] );

			if ( fireGlobals ) {
				globalEventContext.trigger( "ajaxComplete", [ jqXHR, s ] );
				// Handle the global AJAX counter
				if ( !( --jQuery.active ) ) {
					jQuery.event.trigger("ajaxStop");
				}
			}
		}

		return jqXHR;
	},

	getJSON: function( url, data, callback ) {
		return jQuery.get( url, data, callback, "json" );
	},

	getScript: function( url, callback ) {
		return jQuery.get( url, undefined, callback, "script" );
	}
});

jQuery.each( [ "get", "post" ], function( i, method ) {
	jQuery[ method ] = function( url, data, callback, type ) {
		// Shift arguments if data argument was omitted
		if ( jQuery.isFunction( data ) ) {
			type = type || callback;
			callback = data;
			data = undefined;
		}

		return jQuery.ajax({
			url: url,
			type: method,
			dataType: type,
			data: data,
			success: callback
		});
	};
});


jQuery._evalUrl = function( url ) {
	return jQuery.ajax({
		url: url,
		type: "GET",
		dataType: "script",
		async: false,
		global: false,
		"throws": true
	});
};


jQuery.fn.extend({
	wrapAll: function( html ) {
		var wrap;

		if ( jQuery.isFunction( html ) ) {
			return this.each(function( i ) {
				jQuery( this ).wrapAll( html.call(this, i) );
			});
		}

		if ( this[ 0 ] ) {

			// The elements to wrap the target around
			wrap = jQuery( html, this[ 0 ].ownerDocument ).eq( 0 ).clone( true );

			if ( this[ 0 ].parentNode ) {
				wrap.insertBefore( this[ 0 ] );
			}

			wrap.map(function() {
				var elem = this;

				while ( elem.firstElementChild ) {
					elem = elem.firstElementChild;
				}

				return elem;
			}).append( this );
		}

		return this;
	},

	wrapInner: function( html ) {
		if ( jQuery.isFunction( html ) ) {
			return this.each(function( i ) {
				jQuery( this ).wrapInner( html.call(this, i) );
			});
		}

		return this.each(function() {
			var self = jQuery( this ),
				contents = self.contents();

			if ( contents.length ) {
				contents.wrapAll( html );

			} else {
				self.append( html );
			}
		});
	},

	wrap: function( html ) {
		var isFunction = jQuery.isFunction( html );

		return this.each(function( i ) {
			jQuery( this ).wrapAll( isFunction ? html.call(this, i) : html );
		});
	},

	unwrap: function() {
		return this.parent().each(function() {
			if ( !jQuery.nodeName( this, "body" ) ) {
				jQuery( this ).replaceWith( this.childNodes );
			}
		}).end();
	}
});


jQuery.expr.filters.hidden = function( elem ) {
	// Support: Opera <= 12.12
	// Opera reports offsetWidths and offsetHeights less than zero on some elements
	return elem.offsetWidth <= 0 && elem.offsetHeight <= 0;
};
jQuery.expr.filters.visible = function( elem ) {
	return !jQuery.expr.filters.hidden( elem );
};




var r20 = /%20/g,
	rbracket = /\[\]$/,
	rCRLF = /\r?\n/g,
	rsubmitterTypes = /^(?:submit|button|image|reset|file)$/i,
	rsubmittable = /^(?:input|select|textarea|keygen)/i;

function buildParams( prefix, obj, traditional, add ) {
	var name;

	if ( jQuery.isArray( obj ) ) {
		// Serialize array item.
		jQuery.each( obj, function( i, v ) {
			if ( traditional || rbracket.test( prefix ) ) {
				// Treat each array item as a scalar.
				add( prefix, v );

			} else {
				// Item is non-scalar (array or object), encode its numeric index.
				buildParams( prefix + "[" + ( typeof v === "object" ? i : "" ) + "]", v, traditional, add );
			}
		});

	} else if ( !traditional && jQuery.type( obj ) === "object" ) {
		// Serialize object item.
		for ( name in obj ) {
			buildParams( prefix + "[" + name + "]", obj[ name ], traditional, add );
		}

	} else {
		// Serialize scalar item.
		add( prefix, obj );
	}
}

// Serialize an array of form elements or a set of
// key/values into a query string
jQuery.param = function( a, traditional ) {
	var prefix,
		s = [],
		add = function( key, value ) {
			// If value is a function, invoke it and return its value
			value = jQuery.isFunction( value ) ? value() : ( value == null ? "" : value );
			s[ s.length ] = encodeURIComponent( key ) + "=" + encodeURIComponent( value );
		};

	// Set traditional to true for jQuery <= 1.3.2 behavior.
	if ( traditional === undefined ) {
		traditional = jQuery.ajaxSettings && jQuery.ajaxSettings.traditional;
	}

	// If an array was passed in, assume that it is an array of form elements.
	if ( jQuery.isArray( a ) || ( a.jquery && !jQuery.isPlainObject( a ) ) ) {
		// Serialize the form elements
		jQuery.each( a, function() {
			add( this.name, this.value );
		});

	} else {
		// If traditional, encode the "old" way (the way 1.3.2 or older
		// did it), otherwise encode params recursively.
		for ( prefix in a ) {
			buildParams( prefix, a[ prefix ], traditional, add );
		}
	}

	// Return the resulting serialization
	return s.join( "&" ).replace( r20, "+" );
};

jQuery.fn.extend({
	serialize: function() {
		return jQuery.param( this.serializeArray() );
	},
	serializeArray: function() {
		return this.map(function() {
			// Can add propHook for "elements" to filter or add form elements
			var elements = jQuery.prop( this, "elements" );
			return elements ? jQuery.makeArray( elements ) : this;
		})
		.filter(function() {
			var type = this.type;

			// Use .is( ":disabled" ) so that fieldset[disabled] works
			return this.name && !jQuery( this ).is( ":disabled" ) &&
				rsubmittable.test( this.nodeName ) && !rsubmitterTypes.test( type ) &&
				( this.checked || !rcheckableType.test( type ) );
		})
		.map(function( i, elem ) {
			var val = jQuery( this ).val();

			return val == null ?
				null :
				jQuery.isArray( val ) ?
					jQuery.map( val, function( val ) {
						return { name: elem.name, value: val.replace( rCRLF, "\r\n" ) };
					}) :
					{ name: elem.name, value: val.replace( rCRLF, "\r\n" ) };
		}).get();
	}
});


jQuery.ajaxSettings.xhr = function() {
	try {
		return new XMLHttpRequest();
	} catch( e ) {}
};

var xhrId = 0,
	xhrCallbacks = {},
	xhrSuccessStatus = {
		// file protocol always yields status code 0, assume 200
		0: 200,
		// Support: IE9
		// #1450: sometimes IE returns 1223 when it should be 204
		1223: 204
	},
	xhrSupported = jQuery.ajaxSettings.xhr();

// Support: IE9
// Open requests must be manually aborted on unload (#5280)
// See https://support.microsoft.com/kb/2856746 for more info
if ( window.attachEvent ) {
	window.attachEvent( "onunload", function() {
		for ( var key in xhrCallbacks ) {
			xhrCallbacks[ key ]();
		}
	});
}

support.cors = !!xhrSupported && ( "withCredentials" in xhrSupported );
support.ajax = xhrSupported = !!xhrSupported;

jQuery.ajaxTransport(function( options ) {
	var callback;

	// Cross domain only allowed if supported through XMLHttpRequest
	if ( support.cors || xhrSupported && !options.crossDomain ) {
		return {
			send: function( headers, complete ) {
				var i,
					xhr = options.xhr(),
					id = ++xhrId;

				xhr.open( options.type, options.url, options.async, options.username, options.password );

				// Apply custom fields if provided
				if ( options.xhrFields ) {
					for ( i in options.xhrFields ) {
						xhr[ i ] = options.xhrFields[ i ];
					}
				}

				// Override mime type if needed
				if ( options.mimeType && xhr.overrideMimeType ) {
					xhr.overrideMimeType( options.mimeType );
				}

				// X-Requested-With header
				// For cross-domain requests, seeing as conditions for a preflight are
				// akin to a jigsaw puzzle, we simply never set it to be sure.
				// (it can always be set on a per-request basis or even using ajaxSetup)
				// For same-domain requests, won't change header if already provided.
				if ( !options.crossDomain && !headers["X-Requested-With"] ) {
					headers["X-Requested-With"] = "XMLHttpRequest";
				}

				// Set headers
				for ( i in headers ) {
					xhr.setRequestHeader( i, headers[ i ] );
				}

				// Callback
				callback = function( type ) {
					return function() {
						if ( callback ) {
							delete xhrCallbacks[ id ];
							callback = xhr.onload = xhr.onerror = null;

							if ( type === "abort" ) {
								xhr.abort();
							} else if ( type === "error" ) {
								complete(
									// file: protocol always yields status 0; see #8605, #14207
									xhr.status,
									xhr.statusText
								);
							} else {
								complete(
									xhrSuccessStatus[ xhr.status ] || xhr.status,
									xhr.statusText,
									// Support: IE9
									// Accessing binary-data responseText throws an exception
									// (#11426)
									typeof xhr.responseText === "string" ? {
										text: xhr.responseText
									} : undefined,
									xhr.getAllResponseHeaders()
								);
							}
						}
					};
				};

				// Listen to events
				xhr.onload = callback();
				xhr.onerror = callback("error");

				// Create the abort callback
				callback = xhrCallbacks[ id ] = callback("abort");

				try {
					// Do send the request (this may raise an exception)
					xhr.send( options.hasContent && options.data || null );
				} catch ( e ) {
					// #14683: Only rethrow if this hasn't been notified as an error yet
					if ( callback ) {
						throw e;
					}
				}
			},

			abort: function() {
				if ( callback ) {
					callback();
				}
			}
		};
	}
});




// Install script dataType
jQuery.ajaxSetup({
	accepts: {
		script: "text/javascript, application/javascript, application/ecmascript, application/x-ecmascript"
	},
	contents: {
		script: /(?:java|ecma)script/
	},
	converters: {
		"text script": function( text ) {
			jQuery.globalEval( text );
			return text;
		}
	}
});

// Handle cache's special case and crossDomain
jQuery.ajaxPrefilter( "script", function( s ) {
	if ( s.cache === undefined ) {
		s.cache = false;
	}
	if ( s.crossDomain ) {
		s.type = "GET";
	}
});

// Bind script tag hack transport
jQuery.ajaxTransport( "script", function( s ) {
	// This transport only deals with cross domain requests
	if ( s.crossDomain ) {
		var script, callback;
		return {
			send: function( _, complete ) {
				script = jQuery("<script>").prop({
					async: true,
					charset: s.scriptCharset,
					src: s.url
				}).on(
					"load error",
					callback = function( evt ) {
						script.remove();
						callback = null;
						if ( evt ) {
							complete( evt.type === "error" ? 404 : 200, evt.type );
						}
					}
				);
				document.head.appendChild( script[ 0 ] );
			},
			abort: function() {
				if ( callback ) {
					callback();
				}
			}
		};
	}
});




var oldCallbacks = [],
	rjsonp = /(=)\?(?=&|$)|\?\?/;

// Default jsonp settings
jQuery.ajaxSetup({
	jsonp: "callback",
	jsonpCallback: function() {
		var callback = oldCallbacks.pop() || ( jQuery.expando + "_" + ( nonce++ ) );
		this[ callback ] = true;
		return callback;
	}
});

// Detect, normalize options and install callbacks for jsonp requests
jQuery.ajaxPrefilter( "json jsonp", function( s, originalSettings, jqXHR ) {

	var callbackName, overwritten, responseContainer,
		jsonProp = s.jsonp !== false && ( rjsonp.test( s.url ) ?
			"url" :
			typeof s.data === "string" && !( s.contentType || "" ).indexOf("application/x-www-form-urlencoded") && rjsonp.test( s.data ) && "data"
		);

	// Handle iff the expected data type is "jsonp" or we have a parameter to set
	if ( jsonProp || s.dataTypes[ 0 ] === "jsonp" ) {

		// Get callback name, remembering preexisting value associated with it
		callbackName = s.jsonpCallback = jQuery.isFunction( s.jsonpCallback ) ?
			s.jsonpCallback() :
			s.jsonpCallback;

		// Insert callback into url or form data
		if ( jsonProp ) {
			s[ jsonProp ] = s[ jsonProp ].replace( rjsonp, "$1" + callbackName );
		} else if ( s.jsonp !== false ) {
			s.url += ( rquery.test( s.url ) ? "&" : "?" ) + s.jsonp + "=" + callbackName;
		}

		// Use data converter to retrieve json after script execution
		s.converters["script json"] = function() {
			if ( !responseContainer ) {
				jQuery.error( callbackName + " was not called" );
			}
			return responseContainer[ 0 ];
		};

		// force json dataType
		s.dataTypes[ 0 ] = "json";

		// Install callback
		overwritten = window[ callbackName ];
		window[ callbackName ] = function() {
			responseContainer = arguments;
		};

		// Clean-up function (fires after converters)
		jqXHR.always(function() {
			// Restore preexisting value
			window[ callbackName ] = overwritten;

			// Save back as free
			if ( s[ callbackName ] ) {
				// make sure that re-using the options doesn't screw things around
				s.jsonpCallback = originalSettings.jsonpCallback;

				// save the callback name for future use
				oldCallbacks.push( callbackName );
			}

			// Call if it was a function and we have a response
			if ( responseContainer && jQuery.isFunction( overwritten ) ) {
				overwritten( responseContainer[ 0 ] );
			}

			responseContainer = overwritten = undefined;
		});

		// Delegate to script
		return "script";
	}
});




// data: string of html
// context (optional): If specified, the fragment will be created in this context, defaults to document
// keepScripts (optional): If true, will include scripts passed in the html string
jQuery.parseHTML = function( data, context, keepScripts ) {
	if ( !data || typeof data !== "string" ) {
		return null;
	}
	if ( typeof context === "boolean" ) {
		keepScripts = context;
		context = false;
	}
	context = context || document;

	var parsed = rsingleTag.exec( data ),
		scripts = !keepScripts && [];

	// Single tag
	if ( parsed ) {
		return [ context.createElement( parsed[1] ) ];
	}

	parsed = jQuery.buildFragment( [ data ], context, scripts );

	if ( scripts && scripts.length ) {
		jQuery( scripts ).remove();
	}

	return jQuery.merge( [], parsed.childNodes );
};


// Keep a copy of the old load method
var _load = jQuery.fn.load;

/**
 * Load a url into a page
 */
jQuery.fn.load = function( url, params, callback ) {
	if ( typeof url !== "string" && _load ) {
		return _load.apply( this, arguments );
	}

	var selector, type, response,
		self = this,
		off = url.indexOf(" ");

	if ( off >= 0 ) {
		selector = jQuery.trim( url.slice( off ) );
		url = url.slice( 0, off );
	}

	// If it's a function
	if ( jQuery.isFunction( params ) ) {

		// We assume that it's the callback
		callback = params;
		params = undefined;

	// Otherwise, build a param string
	} else if ( params && typeof params === "object" ) {
		type = "POST";
	}

	// If we have elements to modify, make the request
	if ( self.length > 0 ) {
		jQuery.ajax({
			url: url,

			// if "type" variable is undefined, then "GET" method will be used
			type: type,
			dataType: "html",
			data: params
		}).done(function( responseText ) {

			// Save response for use in complete callback
			response = arguments;

			self.html( selector ?

				// If a selector was specified, locate the right elements in a dummy div
				// Exclude scripts to avoid IE 'Permission Denied' errors
				jQuery("<div>").append( jQuery.parseHTML( responseText ) ).find( selector ) :

				// Otherwise use the full result
				responseText );

		}).complete( callback && function( jqXHR, status ) {
			self.each( callback, response || [ jqXHR.responseText, status, jqXHR ] );
		});
	}

	return this;
};




// Attach a bunch of functions for handling common AJAX events
jQuery.each( [ "ajaxStart", "ajaxStop", "ajaxComplete", "ajaxError", "ajaxSuccess", "ajaxSend" ], function( i, type ) {
	jQuery.fn[ type ] = function( fn ) {
		return this.on( type, fn );
	};
});




jQuery.expr.filters.animated = function( elem ) {
	return jQuery.grep(jQuery.timers, function( fn ) {
		return elem === fn.elem;
	}).length;
};




var docElem = window.document.documentElement;

/**
 * Gets a window from an element
 */
function getWindow( elem ) {
	return jQuery.isWindow( elem ) ? elem : elem.nodeType === 9 && elem.defaultView;
}

jQuery.offset = {
	setOffset: function( elem, options, i ) {
		var curPosition, curLeft, curCSSTop, curTop, curOffset, curCSSLeft, calculatePosition,
			position = jQuery.css( elem, "position" ),
			curElem = jQuery( elem ),
			props = {};

		// Set position first, in-case top/left are set even on static elem
		if ( position === "static" ) {
			elem.style.position = "relative";
		}

		curOffset = curElem.offset();
		curCSSTop = jQuery.css( elem, "top" );
		curCSSLeft = jQuery.css( elem, "left" );
		calculatePosition = ( position === "absolute" || position === "fixed" ) &&
			( curCSSTop + curCSSLeft ).indexOf("auto") > -1;

		// Need to be able to calculate position if either
		// top or left is auto and position is either absolute or fixed
		if ( calculatePosition ) {
			curPosition = curElem.position();
			curTop = curPosition.top;
			curLeft = curPosition.left;

		} else {
			curTop = parseFloat( curCSSTop ) || 0;
			curLeft = parseFloat( curCSSLeft ) || 0;
		}

		if ( jQuery.isFunction( options ) ) {
			options = options.call( elem, i, curOffset );
		}

		if ( options.top != null ) {
			props.top = ( options.top - curOffset.top ) + curTop;
		}
		if ( options.left != null ) {
			props.left = ( options.left - curOffset.left ) + curLeft;
		}

		if ( "using" in options ) {
			options.using.call( elem, props );

		} else {
			curElem.css( props );
		}
	}
};

jQuery.fn.extend({
	offset: function( options ) {
		if ( arguments.length ) {
			return options === undefined ?
				this :
				this.each(function( i ) {
					jQuery.offset.setOffset( this, options, i );
				});
		}

		var docElem, win,
			elem = this[ 0 ],
			box = { top: 0, left: 0 },
			doc = elem && elem.ownerDocument;

		if ( !doc ) {
			return;
		}

		docElem = doc.documentElement;

		// Make sure it's not a disconnected DOM node
		if ( !jQuery.contains( docElem, elem ) ) {
			return box;
		}

		// Support: BlackBerry 5, iOS 3 (original iPhone)
		// If we don't have gBCR, just use 0,0 rather than error
		if ( typeof elem.getBoundingClientRect !== strundefined ) {
			box = elem.getBoundingClientRect();
		}
		win = getWindow( doc );
		return {
			top: box.top + win.pageYOffset - docElem.clientTop,
			left: box.left + win.pageXOffset - docElem.clientLeft
		};
	},

	position: function() {
		if ( !this[ 0 ] ) {
			return;
		}

		var offsetParent, offset,
			elem = this[ 0 ],
			parentOffset = { top: 0, left: 0 };

		// Fixed elements are offset from window (parentOffset = {top:0, left: 0}, because it is its only offset parent
		if ( jQuery.css( elem, "position" ) === "fixed" ) {
			// Assume getBoundingClientRect is there when computed position is fixed
			offset = elem.getBoundingClientRect();

		} else {
			// Get *real* offsetParent
			offsetParent = this.offsetParent();

			// Get correct offsets
			offset = this.offset();
			if ( !jQuery.nodeName( offsetParent[ 0 ], "html" ) ) {
				parentOffset = offsetParent.offset();
			}

			// Add offsetParent borders
			parentOffset.top += jQuery.css( offsetParent[ 0 ], "borderTopWidth", true );
			parentOffset.left += jQuery.css( offsetParent[ 0 ], "borderLeftWidth", true );
		}

		// Subtract parent offsets and element margins
		return {
			top: offset.top - parentOffset.top - jQuery.css( elem, "marginTop", true ),
			left: offset.left - parentOffset.left - jQuery.css( elem, "marginLeft", true )
		};
	},

	offsetParent: function() {
		return this.map(function() {
			var offsetParent = this.offsetParent || docElem;

			while ( offsetParent && ( !jQuery.nodeName( offsetParent, "html" ) && jQuery.css( offsetParent, "position" ) === "static" ) ) {
				offsetParent = offsetParent.offsetParent;
			}

			return offsetParent || docElem;
		});
	}
});

// Create scrollLeft and scrollTop methods
jQuery.each( { scrollLeft: "pageXOffset", scrollTop: "pageYOffset" }, function( method, prop ) {
	var top = "pageYOffset" === prop;

	jQuery.fn[ method ] = function( val ) {
		return access( this, function( elem, method, val ) {
			var win = getWindow( elem );

			if ( val === undefined ) {
				return win ? win[ prop ] : elem[ method ];
			}

			if ( win ) {
				win.scrollTo(
					!top ? val : window.pageXOffset,
					top ? val : window.pageYOffset
				);

			} else {
				elem[ method ] = val;
			}
		}, method, val, arguments.length, null );
	};
});

// Support: Safari<7+, Chrome<37+
// Add the top/left cssHooks using jQuery.fn.position
// Webkit bug: https://bugs.webkit.org/show_bug.cgi?id=29084
// Blink bug: https://code.google.com/p/chromium/issues/detail?id=229280
// getComputedStyle returns percent when specified for top/left/bottom/right;
// rather than make the css module depend on the offset module, just check for it here
jQuery.each( [ "top", "left" ], function( i, prop ) {
	jQuery.cssHooks[ prop ] = addGetHookIf( support.pixelPosition,
		function( elem, computed ) {
			if ( computed ) {
				computed = curCSS( elem, prop );
				// If curCSS returns percentage, fallback to offset
				return rnumnonpx.test( computed ) ?
					jQuery( elem ).position()[ prop ] + "px" :
					computed;
			}
		}
	);
});


// Create innerHeight, innerWidth, height, width, outerHeight and outerWidth methods
jQuery.each( { Height: "height", Width: "width" }, function( name, type ) {
	jQuery.each( { padding: "inner" + name, content: type, "": "outer" + name }, function( defaultExtra, funcName ) {
		// Margin is only for outerHeight, outerWidth
		jQuery.fn[ funcName ] = function( margin, value ) {
			var chainable = arguments.length && ( defaultExtra || typeof margin !== "boolean" ),
				extra = defaultExtra || ( margin === true || value === true ? "margin" : "border" );

			return access( this, function( elem, type, value ) {
				var doc;

				if ( jQuery.isWindow( elem ) ) {
					// As of 5/8/2012 this will yield incorrect results for Mobile Safari, but there
					// isn't a whole lot we can do. See pull request at this URL for discussion:
					// https://github.com/jquery/jquery/pull/764
					return elem.document.documentElement[ "client" + name ];
				}

				// Get document width or height
				if ( elem.nodeType === 9 ) {
					doc = elem.documentElement;

					// Either scroll[Width/Height] or offset[Width/Height] or client[Width/Height],
					// whichever is greatest
					return Math.max(
						elem.body[ "scroll" + name ], doc[ "scroll" + name ],
						elem.body[ "offset" + name ], doc[ "offset" + name ],
						doc[ "client" + name ]
					);
				}

				return value === undefined ?
					// Get width or height on the element, requesting but not forcing parseFloat
					jQuery.css( elem, type, extra ) :

					// Set width or height on the element
					jQuery.style( elem, type, value, extra );
			}, type, chainable ? margin : undefined, chainable, null );
		};
	});
});


// The number of elements contained in the matched element set
jQuery.fn.size = function() {
	return this.length;
};

jQuery.fn.andSelf = jQuery.fn.addBack;




// Register as a named AMD module, since jQuery can be concatenated with other
// files that may use define, but not via a proper concatenation script that
// understands anonymous AMD modules. A named AMD is safest and most robust
// way to register. Lowercase jquery is used because AMD module names are
// derived from file names, and jQuery is normally delivered in a lowercase
// file name. Do this after creating the global so that if an AMD module wants
// to call noConflict to hide this version of jQuery, it will work.

// Note that for maximum portability, libraries that are not jQuery should
// declare themselves as anonymous modules, and avoid setting a global if an
// AMD loader is present. jQuery is a special case. For more information, see
// https://github.com/jrburke/requirejs/wiki/Updating-existing-libraries#wiki-anon

if ( typeof define === "function" && define.amd ) {
	define( "jquery", [], function() {
		return jQuery;
	});
}




var
	// Map over jQuery in case of overwrite
	_jQuery = window.jQuery,

	// Map over the $ in case of overwrite
	_$ = window.$;

jQuery.noConflict = function( deep ) {
	if ( window.$ === jQuery ) {
		window.$ = _$;
	}

	if ( deep && window.jQuery === jQuery ) {
		window.jQuery = _jQuery;
	}

	return jQuery;
};

// Expose jQuery and $ identifiers, even in AMD
// (#7102#comment:10, https://github.com/jquery/jquery/pull/557)
// and CommonJS for browser emulators (#13566)
if ( typeof noGlobal === strundefined ) {
	window.jQuery = window.$ = jQuery;
}




return jQuery;

}));

/*! tether 1.4.0 */

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(factory);
  } else if (typeof exports === 'object') {
    module.exports = factory(require, exports, module);
  } else {
    root.Tether = factory();
  }
}(this, function(require, exports, module) {

'use strict';

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }

var TetherBase = undefined;
if (typeof TetherBase === 'undefined') {
  TetherBase = { modules: [] };
}

var zeroElement = null;

// Same as native getBoundingClientRect, except it takes into account parent <frame> offsets
// if the element lies within a nested document (<frame> or <iframe>-like).
function getActualBoundingClientRect(node) {
  var boundingRect = node.getBoundingClientRect();

  // The original object returned by getBoundingClientRect is immutable, so we clone it
  // We can't use extend because the properties are not considered part of the object by hasOwnProperty in IE9
  var rect = {};
  for (var k in boundingRect) {
    rect[k] = boundingRect[k];
  }

  if (node.ownerDocument !== document) {
    var _frameElement = node.ownerDocument.defaultView.frameElement;
    if (_frameElement) {
      var frameRect = getActualBoundingClientRect(_frameElement);
      rect.top += frameRect.top;
      rect.bottom += frameRect.top;
      rect.left += frameRect.left;
      rect.right += frameRect.left;
    }
  }

  return rect;
}

function getScrollParents(el) {
  // In firefox if the el is inside an iframe with display: none; window.getComputedStyle() will return null;
  // https://bugzilla.mozilla.org/show_bug.cgi?id=548397
  var computedStyle = getComputedStyle(el) || {};
  var position = computedStyle.position;
  var parents = [];

  if (position === 'fixed') {
    return [el];
  }

  var parent = el;
  while ((parent = parent.parentNode) && parent && parent.nodeType === 1) {
    var style = undefined;
    try {
      style = getComputedStyle(parent);
    } catch (err) {}

    if (typeof style === 'undefined' || style === null) {
      parents.push(parent);
      return parents;
    }

    var _style = style;
    var overflow = _style.overflow;
    var overflowX = _style.overflowX;
    var overflowY = _style.overflowY;

    if (/(auto|scroll)/.test(overflow + overflowY + overflowX)) {
      if (position !== 'absolute' || ['relative', 'absolute', 'fixed'].indexOf(style.position) >= 0) {
        parents.push(parent);
      }
    }
  }

  parents.push(el.ownerDocument.body);

  // If the node is within a frame, account for the parent window scroll
  if (el.ownerDocument !== document) {
    parents.push(el.ownerDocument.defaultView);
  }

  return parents;
}

var uniqueId = (function () {
  var id = 0;
  return function () {
    return ++id;
  };
})();

var zeroPosCache = {};
var getOrigin = function getOrigin() {
  // getBoundingClientRect is unfortunately too accurate.  It introduces a pixel or two of
  // jitter as the user scrolls that messes with our ability to detect if two positions
  // are equivilant or not.  We place an element at the top left of the page that will
  // get the same jitter, so we can cancel the two out.
  var node = zeroElement;
  if (!node || !document.body.contains(node)) {
    node = document.createElement('div');
    node.setAttribute('data-tether-id', uniqueId());
    extend(node.style, {
      top: 0,
      left: 0,
      position: 'absolute'
    });

    document.body.appendChild(node);

    zeroElement = node;
  }

  var id = node.getAttribute('data-tether-id');
  if (typeof zeroPosCache[id] === 'undefined') {
    zeroPosCache[id] = getActualBoundingClientRect(node);

    // Clear the cache when this position call is done
    defer(function () {
      delete zeroPosCache[id];
    });
  }

  return zeroPosCache[id];
};

function removeUtilElements() {
  if (zeroElement) {
    document.body.removeChild(zeroElement);
  }
  zeroElement = null;
};

function getBounds(el) {
  var doc = undefined;
  if (el === document) {
    doc = document;
    el = document.documentElement;
  } else {
    doc = el.ownerDocument;
  }

  var docEl = doc.documentElement;

  var box = getActualBoundingClientRect(el);

  var origin = getOrigin();

  box.top -= origin.top;
  box.left -= origin.left;

  if (typeof box.width === 'undefined') {
    box.width = document.body.scrollWidth - box.left - box.right;
  }
  if (typeof box.height === 'undefined') {
    box.height = document.body.scrollHeight - box.top - box.bottom;
  }

  box.top = box.top - docEl.clientTop;
  box.left = box.left - docEl.clientLeft;
  box.right = doc.body.clientWidth - box.width - box.left;
  box.bottom = doc.body.clientHeight - box.height - box.top;

  return box;
}

function getOffsetParent(el) {
  return el.offsetParent || document.documentElement;
}

var _scrollBarSize = null;
function getScrollBarSize() {
  if (_scrollBarSize) {
    return _scrollBarSize;
  }
  var inner = document.createElement('div');
  inner.style.width = '100%';
  inner.style.height = '200px';

  var outer = document.createElement('div');
  extend(outer.style, {
    position: 'absolute',
    top: 0,
    left: 0,
    pointerEvents: 'none',
    visibility: 'hidden',
    width: '200px',
    height: '150px',
    overflow: 'hidden'
  });

  outer.appendChild(inner);

  document.body.appendChild(outer);

  var widthContained = inner.offsetWidth;
  outer.style.overflow = 'scroll';
  var widthScroll = inner.offsetWidth;

  if (widthContained === widthScroll) {
    widthScroll = outer.clientWidth;
  }

  document.body.removeChild(outer);

  var width = widthContained - widthScroll;

  _scrollBarSize = { width: width, height: width };
  return _scrollBarSize;
}

function extend() {
  var out = arguments.length <= 0 || arguments[0] === undefined ? {} : arguments[0];

  var args = [];

  Array.prototype.push.apply(args, arguments);

  args.slice(1).forEach(function (obj) {
    if (obj) {
      for (var key in obj) {
        if (({}).hasOwnProperty.call(obj, key)) {
          out[key] = obj[key];
        }
      }
    }
  });

  return out;
}

function removeClass(el, name) {
  if (typeof el.classList !== 'undefined') {
    name.split(' ').forEach(function (cls) {
      if (cls.trim()) {
        el.classList.remove(cls);
      }
    });
  } else {
    var regex = new RegExp('(^| )' + name.split(' ').join('|') + '( |$)', 'gi');
    var className = getClassName(el).replace(regex, ' ');
    setClassName(el, className);
  }
}

function addClass(el, name) {
  if (typeof el.classList !== 'undefined') {
    name.split(' ').forEach(function (cls) {
      if (cls.trim()) {
        el.classList.add(cls);
      }
    });
  } else {
    removeClass(el, name);
    var cls = getClassName(el) + (' ' + name);
    setClassName(el, cls);
  }
}

function hasClass(el, name) {
  if (typeof el.classList !== 'undefined') {
    return el.classList.contains(name);
  }
  var className = getClassName(el);
  return new RegExp('(^| )' + name + '( |$)', 'gi').test(className);
}

function getClassName(el) {
  // Can't use just SVGAnimatedString here since nodes within a Frame in IE have
  // completely separately SVGAnimatedString base classes
  if (el.className instanceof el.ownerDocument.defaultView.SVGAnimatedString) {
    return el.className.baseVal;
  }
  return el.className;
}

function setClassName(el, className) {
  el.setAttribute('class', className);
}

function updateClasses(el, add, all) {
  // Of the set of 'all' classes, we need the 'add' classes, and only the
  // 'add' classes to be set.
  all.forEach(function (cls) {
    if (add.indexOf(cls) === -1 && hasClass(el, cls)) {
      removeClass(el, cls);
    }
  });

  add.forEach(function (cls) {
    if (!hasClass(el, cls)) {
      addClass(el, cls);
    }
  });
}

var deferred = [];

var defer = function defer(fn) {
  deferred.push(fn);
};

var flush = function flush() {
  var fn = undefined;
  while (fn = deferred.pop()) {
    fn();
  }
};

var Evented = (function () {
  function Evented() {
    _classCallCheck(this, Evented);
  }

  _createClass(Evented, [{
    key: 'on',
    value: function on(event, handler, ctx) {
      var once = arguments.length <= 3 || arguments[3] === undefined ? false : arguments[3];

      if (typeof this.bindings === 'undefined') {
        this.bindings = {};
      }
      if (typeof this.bindings[event] === 'undefined') {
        this.bindings[event] = [];
      }
      this.bindings[event].push({ handler: handler, ctx: ctx, once: once });
    }
  }, {
    key: 'once',
    value: function once(event, handler, ctx) {
      this.on(event, handler, ctx, true);
    }
  }, {
    key: 'off',
    value: function off(event, handler) {
      if (typeof this.bindings === 'undefined' || typeof this.bindings[event] === 'undefined') {
        return;
      }

      if (typeof handler === 'undefined') {
        delete this.bindings[event];
      } else {
        var i = 0;
        while (i < this.bindings[event].length) {
          if (this.bindings[event][i].handler === handler) {
            this.bindings[event].splice(i, 1);
          } else {
            ++i;
          }
        }
      }
    }
  }, {
    key: 'trigger',
    value: function trigger(event) {
      if (typeof this.bindings !== 'undefined' && this.bindings[event]) {
        var i = 0;

        for (var _len = arguments.length, args = Array(_len > 1 ? _len - 1 : 0), _key = 1; _key < _len; _key++) {
          args[_key - 1] = arguments[_key];
        }

        while (i < this.bindings[event].length) {
          var _bindings$event$i = this.bindings[event][i];
          var handler = _bindings$event$i.handler;
          var ctx = _bindings$event$i.ctx;
          var once = _bindings$event$i.once;

          var context = ctx;
          if (typeof context === 'undefined') {
            context = this;
          }

          handler.apply(context, args);

          if (once) {
            this.bindings[event].splice(i, 1);
          } else {
            ++i;
          }
        }
      }
    }
  }]);

  return Evented;
})();

TetherBase.Utils = {
  getActualBoundingClientRect: getActualBoundingClientRect,
  getScrollParents: getScrollParents,
  getBounds: getBounds,
  getOffsetParent: getOffsetParent,
  extend: extend,
  addClass: addClass,
  removeClass: removeClass,
  hasClass: hasClass,
  updateClasses: updateClasses,
  defer: defer,
  flush: flush,
  uniqueId: uniqueId,
  Evented: Evented,
  getScrollBarSize: getScrollBarSize,
  removeUtilElements: removeUtilElements
};
/* globals TetherBase, performance */

'use strict';

var _slicedToArray = (function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i['return']) _i['return'](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError('Invalid attempt to destructure non-iterable instance'); } }; })();

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

var _get = function get(_x6, _x7, _x8) { var _again = true; _function: while (_again) { var object = _x6, property = _x7, receiver = _x8; _again = false; if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { _x6 = parent; _x7 = property; _x8 = receiver; _again = true; desc = parent = undefined; continue _function; } } else if ('value' in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } } };

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }

function _inherits(subClass, superClass) { if (typeof superClass !== 'function' && superClass !== null) { throw new TypeError('Super expression must either be null or a function, not ' + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

if (typeof TetherBase === 'undefined') {
  throw new Error('You must include the utils.js file before tether.js');
}

var _TetherBase$Utils = TetherBase.Utils;
var getScrollParents = _TetherBase$Utils.getScrollParents;
var getBounds = _TetherBase$Utils.getBounds;
var getOffsetParent = _TetherBase$Utils.getOffsetParent;
var extend = _TetherBase$Utils.extend;
var addClass = _TetherBase$Utils.addClass;
var removeClass = _TetherBase$Utils.removeClass;
var updateClasses = _TetherBase$Utils.updateClasses;
var defer = _TetherBase$Utils.defer;
var flush = _TetherBase$Utils.flush;
var getScrollBarSize = _TetherBase$Utils.getScrollBarSize;
var removeUtilElements = _TetherBase$Utils.removeUtilElements;

function within(a, b) {
  var diff = arguments.length <= 2 || arguments[2] === undefined ? 1 : arguments[2];

  return a + diff >= b && b >= a - diff;
}

var transformKey = (function () {
  if (typeof document === 'undefined') {
    return '';
  }
  var el = document.createElement('div');

  var transforms = ['transform', 'WebkitTransform', 'OTransform', 'MozTransform', 'msTransform'];
  for (var i = 0; i < transforms.length; ++i) {
    var key = transforms[i];
    if (el.style[key] !== undefined) {
      return key;
    }
  }
})();

var tethers = [];

var position = function position() {
  tethers.forEach(function (tether) {
    tether.position(false);
  });
  flush();
};

function now() {
  if (typeof performance !== 'undefined' && typeof performance.now !== 'undefined') {
    return performance.now();
  }
  return +new Date();
}

(function () {
  var lastCall = null;
  var lastDuration = null;
  var pendingTimeout = null;

  var tick = function tick() {
    if (typeof lastDuration !== 'undefined' && lastDuration > 16) {
      // We voluntarily throttle ourselves if we can't manage 60fps
      lastDuration = Math.min(lastDuration - 16, 250);

      // Just in case this is the last event, remember to position just once more
      pendingTimeout = setTimeout(tick, 250);
      return;
    }

    if (typeof lastCall !== 'undefined' && now() - lastCall < 10) {
      // Some browsers call events a little too frequently, refuse to run more than is reasonable
      return;
    }

    if (pendingTimeout != null) {
      clearTimeout(pendingTimeout);
      pendingTimeout = null;
    }

    lastCall = now();
    position();
    lastDuration = now() - lastCall;
  };

  if (typeof window !== 'undefined' && typeof window.addEventListener !== 'undefined') {
    ['resize', 'scroll', 'touchmove'].forEach(function (event) {
      window.addEventListener(event, tick);
    });
  }
})();

var MIRROR_LR = {
  center: 'center',
  left: 'right',
  right: 'left'
};

var MIRROR_TB = {
  middle: 'middle',
  top: 'bottom',
  bottom: 'top'
};

var OFFSET_MAP = {
  top: 0,
  left: 0,
  middle: '50%',
  center: '50%',
  bottom: '100%',
  right: '100%'
};

var autoToFixedAttachment = function autoToFixedAttachment(attachment, relativeToAttachment) {
  var left = attachment.left;
  var top = attachment.top;

  if (left === 'auto') {
    left = MIRROR_LR[relativeToAttachment.left];
  }

  if (top === 'auto') {
    top = MIRROR_TB[relativeToAttachment.top];
  }

  return { left: left, top: top };
};

var attachmentToOffset = function attachmentToOffset(attachment) {
  var left = attachment.left;
  var top = attachment.top;

  if (typeof OFFSET_MAP[attachment.left] !== 'undefined') {
    left = OFFSET_MAP[attachment.left];
  }

  if (typeof OFFSET_MAP[attachment.top] !== 'undefined') {
    top = OFFSET_MAP[attachment.top];
  }

  return { left: left, top: top };
};

function addOffset() {
  var out = { top: 0, left: 0 };

  for (var _len = arguments.length, offsets = Array(_len), _key = 0; _key < _len; _key++) {
    offsets[_key] = arguments[_key];
  }

  offsets.forEach(function (_ref) {
    var top = _ref.top;
    var left = _ref.left;

    if (typeof top === 'string') {
      top = parseFloat(top, 10);
    }
    if (typeof left === 'string') {
      left = parseFloat(left, 10);
    }

    out.top += top;
    out.left += left;
  });

  return out;
}

function offsetToPx(offset, size) {
  if (typeof offset.left === 'string' && offset.left.indexOf('%') !== -1) {
    offset.left = parseFloat(offset.left, 10) / 100 * size.width;
  }
  if (typeof offset.top === 'string' && offset.top.indexOf('%') !== -1) {
    offset.top = parseFloat(offset.top, 10) / 100 * size.height;
  }

  return offset;
}

var parseOffset = function parseOffset(value) {
  var _value$split = value.split(' ');

  var _value$split2 = _slicedToArray(_value$split, 2);

  var top = _value$split2[0];
  var left = _value$split2[1];

  return { top: top, left: left };
};
var parseAttachment = parseOffset;

var TetherClass = (function (_Evented) {
  _inherits(TetherClass, _Evented);

  function TetherClass(options) {
    var _this = this;

    _classCallCheck(this, TetherClass);

    _get(Object.getPrototypeOf(TetherClass.prototype), 'constructor', this).call(this);
    this.position = this.position.bind(this);

    tethers.push(this);

    this.history = [];

    this.setOptions(options, false);

    TetherBase.modules.forEach(function (module) {
      if (typeof module.initialize !== 'undefined') {
        module.initialize.call(_this);
      }
    });

    this.position();
  }

  _createClass(TetherClass, [{
    key: 'getClass',
    value: function getClass() {
      var key = arguments.length <= 0 || arguments[0] === undefined ? '' : arguments[0];
      var classes = this.options.classes;

      if (typeof classes !== 'undefined' && classes[key]) {
        return this.options.classes[key];
      } else if (this.options.classPrefix) {
        return this.options.classPrefix + '-' + key;
      } else {
        return key;
      }
    }
  }, {
    key: 'setOptions',
    value: function setOptions(options) {
      var _this2 = this;

      var pos = arguments.length <= 1 || arguments[1] === undefined ? true : arguments[1];

      var defaults = {
        offset: '0 0',
        targetOffset: '0 0',
        targetAttachment: 'auto auto',
        classPrefix: 'tether'
      };

      this.options = extend(defaults, options);

      var _options = this.options;
      var element = _options.element;
      var target = _options.target;
      var targetModifier = _options.targetModifier;

      this.element = element;
      this.target = target;
      this.targetModifier = targetModifier;

      if (this.target === 'viewport') {
        this.target = document.body;
        this.targetModifier = 'visible';
      } else if (this.target === 'scroll-handle') {
        this.target = document.body;
        this.targetModifier = 'scroll-handle';
      }

      ['element', 'target'].forEach(function (key) {
        if (typeof _this2[key] === 'undefined') {
          throw new Error('Tether Error: Both element and target must be defined');
        }

        if (typeof _this2[key].jquery !== 'undefined') {
          _this2[key] = _this2[key][0];
        } else if (typeof _this2[key] === 'string') {
          _this2[key] = document.querySelector(_this2[key]);
        }
      });

      addClass(this.element, this.getClass('element'));
      if (!(this.options.addTargetClasses === false)) {
        addClass(this.target, this.getClass('target'));
      }

      if (!this.options.attachment) {
        throw new Error('Tether Error: You must provide an attachment');
      }

      this.targetAttachment = parseAttachment(this.options.targetAttachment);
      this.attachment = parseAttachment(this.options.attachment);
      this.offset = parseOffset(this.options.offset);
      this.targetOffset = parseOffset(this.options.targetOffset);

      if (typeof this.scrollParents !== 'undefined') {
        this.disable();
      }

      if (this.targetModifier === 'scroll-handle') {
        this.scrollParents = [this.target];
      } else {
        this.scrollParents = getScrollParents(this.target);
      }

      if (!(this.options.enabled === false)) {
        this.enable(pos);
      }
    }
  }, {
    key: 'getTargetBounds',
    value: function getTargetBounds() {
      if (typeof this.targetModifier !== 'undefined') {
        if (this.targetModifier === 'visible') {
          if (this.target === document.body) {
            return { top: pageYOffset, left: pageXOffset, height: innerHeight, width: innerWidth };
          } else {
            var bounds = getBounds(this.target);

            var out = {
              height: bounds.height,
              width: bounds.width,
              top: bounds.top,
              left: bounds.left
            };

            out.height = Math.min(out.height, bounds.height - (pageYOffset - bounds.top));
            out.height = Math.min(out.height, bounds.height - (bounds.top + bounds.height - (pageYOffset + innerHeight)));
            out.height = Math.min(innerHeight, out.height);
            out.height -= 2;

            out.width = Math.min(out.width, bounds.width - (pageXOffset - bounds.left));
            out.width = Math.min(out.width, bounds.width - (bounds.left + bounds.width - (pageXOffset + innerWidth)));
            out.width = Math.min(innerWidth, out.width);
            out.width -= 2;

            if (out.top < pageYOffset) {
              out.top = pageYOffset;
            }
            if (out.left < pageXOffset) {
              out.left = pageXOffset;
            }

            return out;
          }
        } else if (this.targetModifier === 'scroll-handle') {
          var bounds = undefined;
          var target = this.target;
          if (target === document.body) {
            target = document.documentElement;

            bounds = {
              left: pageXOffset,
              top: pageYOffset,
              height: innerHeight,
              width: innerWidth
            };
          } else {
            bounds = getBounds(target);
          }

          var style = getComputedStyle(target);

          var hasBottomScroll = target.scrollWidth > target.clientWidth || [style.overflow, style.overflowX].indexOf('scroll') >= 0 || this.target !== document.body;

          var scrollBottom = 0;
          if (hasBottomScroll) {
            scrollBottom = 15;
          }

          var height = bounds.height - parseFloat(style.borderTopWidth) - parseFloat(style.borderBottomWidth) - scrollBottom;

          var out = {
            width: 15,
            height: height * 0.975 * (height / target.scrollHeight),
            left: bounds.left + bounds.width - parseFloat(style.borderLeftWidth) - 15
          };

          var fitAdj = 0;
          if (height < 408 && this.target === document.body) {
            fitAdj = -0.00011 * Math.pow(height, 2) - 0.00727 * height + 22.58;
          }

          if (this.target !== document.body) {
            out.height = Math.max(out.height, 24);
          }

          var scrollPercentage = this.target.scrollTop / (target.scrollHeight - height);
          out.top = scrollPercentage * (height - out.height - fitAdj) + bounds.top + parseFloat(style.borderTopWidth);

          if (this.target === document.body) {
            out.height = Math.max(out.height, 24);
          }

          return out;
        }
      } else {
        return getBounds(this.target);
      }
    }
  }, {
    key: 'clearCache',
    value: function clearCache() {
      this._cache = {};
    }
  }, {
    key: 'cache',
    value: function cache(k, getter) {
      // More than one module will often need the same DOM info, so
      // we keep a cache which is cleared on each position call
      if (typeof this._cache === 'undefined') {
        this._cache = {};
      }

      if (typeof this._cache[k] === 'undefined') {
        this._cache[k] = getter.call(this);
      }

      return this._cache[k];
    }
  }, {
    key: 'enable',
    value: function enable() {
      var _this3 = this;

      var pos = arguments.length <= 0 || arguments[0] === undefined ? true : arguments[0];

      if (!(this.options.addTargetClasses === false)) {
        addClass(this.target, this.getClass('enabled'));
      }
      addClass(this.element, this.getClass('enabled'));
      this.enabled = true;

      this.scrollParents.forEach(function (parent) {
        if (parent !== _this3.target.ownerDocument) {
          parent.addEventListener('scroll', _this3.position);
        }
      });

      if (pos) {
        this.position();
      }
    }
  }, {
    key: 'disable',
    value: function disable() {
      var _this4 = this;

      removeClass(this.target, this.getClass('enabled'));
      removeClass(this.element, this.getClass('enabled'));
      this.enabled = false;

      if (typeof this.scrollParents !== 'undefined') {
        this.scrollParents.forEach(function (parent) {
          parent.removeEventListener('scroll', _this4.position);
        });
      }
    }
  }, {
    key: 'destroy',
    value: function destroy() {
      var _this5 = this;

      this.disable();

      tethers.forEach(function (tether, i) {
        if (tether === _this5) {
          tethers.splice(i, 1);
        }
      });

      // Remove any elements we were using for convenience from the DOM
      if (tethers.length === 0) {
        removeUtilElements();
      }
    }
  }, {
    key: 'updateAttachClasses',
    value: function updateAttachClasses(elementAttach, targetAttach) {
      var _this6 = this;

      elementAttach = elementAttach || this.attachment;
      targetAttach = targetAttach || this.targetAttachment;
      var sides = ['left', 'top', 'bottom', 'right', 'middle', 'center'];

      if (typeof this._addAttachClasses !== 'undefined' && this._addAttachClasses.length) {
        // updateAttachClasses can be called more than once in a position call, so
        // we need to clean up after ourselves such that when the last defer gets
        // ran it doesn't add any extra classes from previous calls.
        this._addAttachClasses.splice(0, this._addAttachClasses.length);
      }

      if (typeof this._addAttachClasses === 'undefined') {
        this._addAttachClasses = [];
      }
      var add = this._addAttachClasses;

      if (elementAttach.top) {
        add.push(this.getClass('element-attached') + '-' + elementAttach.top);
      }
      if (elementAttach.left) {
        add.push(this.getClass('element-attached') + '-' + elementAttach.left);
      }
      if (targetAttach.top) {
        add.push(this.getClass('target-attached') + '-' + targetAttach.top);
      }
      if (targetAttach.left) {
        add.push(this.getClass('target-attached') + '-' + targetAttach.left);
      }

      var all = [];
      sides.forEach(function (side) {
        all.push(_this6.getClass('element-attached') + '-' + side);
        all.push(_this6.getClass('target-attached') + '-' + side);
      });

      defer(function () {
        if (!(typeof _this6._addAttachClasses !== 'undefined')) {
          return;
        }

        updateClasses(_this6.element, _this6._addAttachClasses, all);
        if (!(_this6.options.addTargetClasses === false)) {
          updateClasses(_this6.target, _this6._addAttachClasses, all);
        }

        delete _this6._addAttachClasses;
      });
    }
  }, {
    key: 'position',
    value: function position() {
      var _this7 = this;

      var flushChanges = arguments.length <= 0 || arguments[0] === undefined ? true : arguments[0];

      // flushChanges commits the changes immediately, leave true unless you are positioning multiple
      // tethers (in which case call Tether.Utils.flush yourself when you're done)

      if (!this.enabled) {
        return;
      }

      this.clearCache();

      // Turn 'auto' attachments into the appropriate corner or edge
      var targetAttachment = autoToFixedAttachment(this.targetAttachment, this.attachment);

      this.updateAttachClasses(this.attachment, targetAttachment);

      var elementPos = this.cache('element-bounds', function () {
        return getBounds(_this7.element);
      });

      var width = elementPos.width;
      var height = elementPos.height;

      if (width === 0 && height === 0 && typeof this.lastSize !== 'undefined') {
        var _lastSize = this.lastSize;

        // We cache the height and width to make it possible to position elements that are
        // getting hidden.
        width = _lastSize.width;
        height = _lastSize.height;
      } else {
        this.lastSize = { width: width, height: height };
      }

      var targetPos = this.cache('target-bounds', function () {
        return _this7.getTargetBounds();
      });
      var targetSize = targetPos;

      // Get an actual px offset from the attachment
      var offset = offsetToPx(attachmentToOffset(this.attachment), { width: width, height: height });
      var targetOffset = offsetToPx(attachmentToOffset(targetAttachment), targetSize);

      var manualOffset = offsetToPx(this.offset, { width: width, height: height });
      var manualTargetOffset = offsetToPx(this.targetOffset, targetSize);

      // Add the manually provided offset
      offset = addOffset(offset, manualOffset);
      targetOffset = addOffset(targetOffset, manualTargetOffset);

      // It's now our goal to make (element position + offset) == (target position + target offset)
      var left = targetPos.left + targetOffset.left - offset.left;
      var top = targetPos.top + targetOffset.top - offset.top;

      for (var i = 0; i < TetherBase.modules.length; ++i) {
        var _module2 = TetherBase.modules[i];
        var ret = _module2.position.call(this, {
          left: left,
          top: top,
          targetAttachment: targetAttachment,
          targetPos: targetPos,
          elementPos: elementPos,
          offset: offset,
          targetOffset: targetOffset,
          manualOffset: manualOffset,
          manualTargetOffset: manualTargetOffset,
          scrollbarSize: scrollbarSize,
          attachment: this.attachment
        });

        if (ret === false) {
          return false;
        } else if (typeof ret === 'undefined' || typeof ret !== 'object') {
          continue;
        } else {
          top = ret.top;
          left = ret.left;
        }
      }

      // We describe the position three different ways to give the optimizer
      // a chance to decide the best possible way to position the element
      // with the fewest repaints.
      var next = {
        // It's position relative to the page (absolute positioning when
        // the element is a child of the body)
        page: {
          top: top,
          left: left
        },

        // It's position relative to the viewport (fixed positioning)
        viewport: {
          top: top - pageYOffset,
          bottom: pageYOffset - top - height + innerHeight,
          left: left - pageXOffset,
          right: pageXOffset - left - width + innerWidth
        }
      };

      var doc = this.target.ownerDocument;
      var win = doc.defaultView;

      var scrollbarSize = undefined;
      if (win.innerHeight > doc.documentElement.clientHeight) {
        scrollbarSize = this.cache('scrollbar-size', getScrollBarSize);
        next.viewport.bottom -= scrollbarSize.height;
      }

      if (win.innerWidth > doc.documentElement.clientWidth) {
        scrollbarSize = this.cache('scrollbar-size', getScrollBarSize);
        next.viewport.right -= scrollbarSize.width;
      }

      if (['', 'static'].indexOf(doc.body.style.position) === -1 || ['', 'static'].indexOf(doc.body.parentElement.style.position) === -1) {
        // Absolute positioning in the body will be relative to the page, not the 'initial containing block'
        next.page.bottom = doc.body.scrollHeight - top - height;
        next.page.right = doc.body.scrollWidth - left - width;
      }

      if (typeof this.options.optimizations !== 'undefined' && this.options.optimizations.moveElement !== false && !(typeof this.targetModifier !== 'undefined')) {
        (function () {
          var offsetParent = _this7.cache('target-offsetparent', function () {
            return getOffsetParent(_this7.target);
          });
          var offsetPosition = _this7.cache('target-offsetparent-bounds', function () {
            return getBounds(offsetParent);
          });
          var offsetParentStyle = getComputedStyle(offsetParent);
          var offsetParentSize = offsetPosition;

          var offsetBorder = {};
          ['Top', 'Left', 'Bottom', 'Right'].forEach(function (side) {
            offsetBorder[side.toLowerCase()] = parseFloat(offsetParentStyle['border' + side + 'Width']);
          });

          offsetPosition.right = doc.body.scrollWidth - offsetPosition.left - offsetParentSize.width + offsetBorder.right;
          offsetPosition.bottom = doc.body.scrollHeight - offsetPosition.top - offsetParentSize.height + offsetBorder.bottom;

          if (next.page.top >= offsetPosition.top + offsetBorder.top && next.page.bottom >= offsetPosition.bottom) {
            if (next.page.left >= offsetPosition.left + offsetBorder.left && next.page.right >= offsetPosition.right) {
              // We're within the visible part of the target's scroll parent
              var scrollTop = offsetParent.scrollTop;
              var scrollLeft = offsetParent.scrollLeft;

              // It's position relative to the target's offset parent (absolute positioning when
              // the element is moved to be a child of the target's offset parent).
              next.offset = {
                top: next.page.top - offsetPosition.top + scrollTop - offsetBorder.top,
                left: next.page.left - offsetPosition.left + scrollLeft - offsetBorder.left
              };
            }
          }
        })();
      }

      // We could also travel up the DOM and try each containing context, rather than only
      // looking at the body, but we're gonna get diminishing returns.

      this.move(next);

      this.history.unshift(next);

      if (this.history.length > 3) {
        this.history.pop();
      }

      if (flushChanges) {
        flush();
      }

      return true;
    }

    // THE ISSUE
  }, {
    key: 'move',
    value: function move(pos) {
      var _this8 = this;

      if (!(typeof this.element.parentNode !== 'undefined')) {
        return;
      }

      var same = {};

      for (var type in pos) {
        same[type] = {};

        for (var key in pos[type]) {
          var found = false;

          for (var i = 0; i < this.history.length; ++i) {
            var point = this.history[i];
            if (typeof point[type] !== 'undefined' && !within(point[type][key], pos[type][key])) {
              found = true;
              break;
            }
          }

          if (!found) {
            same[type][key] = true;
          }
        }
      }

      var css = { top: '', left: '', right: '', bottom: '' };

      var transcribe = function transcribe(_same, _pos) {
        var hasOptimizations = typeof _this8.options.optimizations !== 'undefined';
        var gpu = hasOptimizations ? _this8.options.optimizations.gpu : null;
        if (gpu !== false) {
          var yPos = undefined,
              xPos = undefined;
          if (_same.top) {
            css.top = 0;
            yPos = _pos.top;
          } else {
            css.bottom = 0;
            yPos = -_pos.bottom;
          }

          if (_same.left) {
            css.left = 0;
            xPos = _pos.left;
          } else {
            css.right = 0;
            xPos = -_pos.right;
          }

          if (window.matchMedia) {
            // HubSpot/tether#207
            var retina = window.matchMedia('only screen and (min-resolution: 1.3dppx)').matches || window.matchMedia('only screen and (-webkit-min-device-pixel-ratio: 1.3)').matches;
            if (!retina) {
              xPos = Math.round(xPos);
              yPos = Math.round(yPos);
            }
          }

          css[transformKey] = 'translateX(' + xPos + 'px) translateY(' + yPos + 'px)';

          if (transformKey !== 'msTransform') {
            // The Z transform will keep this in the GPU (faster, and prevents artifacts),
            // but IE9 doesn't support 3d transforms and will choke.
            css[transformKey] += " translateZ(0)";
          }
        } else {
          if (_same.top) {
            css.top = _pos.top + 'px';
          } else {
            css.bottom = _pos.bottom + 'px';
          }

          if (_same.left) {
            css.left = _pos.left + 'px';
          } else {
            css.right = _pos.right + 'px';
          }
        }
      };

      var moved = false;
      if ((same.page.top || same.page.bottom) && (same.page.left || same.page.right)) {
        css.position = 'absolute';
        transcribe(same.page, pos.page);
      } else if ((same.viewport.top || same.viewport.bottom) && (same.viewport.left || same.viewport.right)) {
        css.position = 'fixed';
        transcribe(same.viewport, pos.viewport);
      } else if (typeof same.offset !== 'undefined' && same.offset.top && same.offset.left) {
        (function () {
          css.position = 'absolute';
          var offsetParent = _this8.cache('target-offsetparent', function () {
            return getOffsetParent(_this8.target);
          });

          if (getOffsetParent(_this8.element) !== offsetParent) {
            defer(function () {
              _this8.element.parentNode.removeChild(_this8.element);
              offsetParent.appendChild(_this8.element);
            });
          }

          transcribe(same.offset, pos.offset);
          moved = true;
        })();
      } else {
        css.position = 'absolute';
        transcribe({ top: true, left: true }, pos.page);
      }

      if (!moved) {
        if (this.options.bodyElement) {
          this.options.bodyElement.appendChild(this.element);
        } else {
          var offsetParentIsBody = true;
          var currentNode = this.element.parentNode;
          while (currentNode && currentNode.nodeType === 1 && currentNode.tagName !== 'BODY') {
            if (getComputedStyle(currentNode).position !== 'static') {
              offsetParentIsBody = false;
              break;
            }

            currentNode = currentNode.parentNode;
          }

          if (!offsetParentIsBody) {
            this.element.parentNode.removeChild(this.element);
            this.element.ownerDocument.body.appendChild(this.element);
          }
        }
      }

      // Any css change will trigger a repaint, so let's avoid one if nothing changed
      var writeCSS = {};
      var write = false;
      for (var key in css) {
        var val = css[key];
        var elVal = this.element.style[key];

        if (elVal !== val) {
          write = true;
          writeCSS[key] = val;
        }
      }

      if (write) {
        defer(function () {
          extend(_this8.element.style, writeCSS);
          _this8.trigger('repositioned');
        });
      }
    }
  }]);

  return TetherClass;
})(Evented);

TetherClass.modules = [];

TetherBase.position = position;

var Tether = extend(TetherClass, TetherBase);
/* globals TetherBase */

'use strict';

var _slicedToArray = (function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i['return']) _i['return'](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError('Invalid attempt to destructure non-iterable instance'); } }; })();

var _TetherBase$Utils = TetherBase.Utils;
var getBounds = _TetherBase$Utils.getBounds;
var extend = _TetherBase$Utils.extend;
var updateClasses = _TetherBase$Utils.updateClasses;
var defer = _TetherBase$Utils.defer;

var BOUNDS_FORMAT = ['left', 'top', 'right', 'bottom'];

function getBoundingRect(tether, to) {
  if (to === 'scrollParent') {
    to = tether.scrollParents[0];
  } else if (to === 'window') {
    to = [pageXOffset, pageYOffset, innerWidth + pageXOffset, innerHeight + pageYOffset];
  }

  if (to === document) {
    to = to.documentElement;
  }

  if (typeof to.nodeType !== 'undefined') {
    (function () {
      var node = to;
      var size = getBounds(to);
      var pos = size;
      var style = getComputedStyle(to);

      to = [pos.left, pos.top, size.width + pos.left, size.height + pos.top];

      // Account any parent Frames scroll offset
      if (node.ownerDocument !== document) {
        var win = node.ownerDocument.defaultView;
        to[0] += win.pageXOffset;
        to[1] += win.pageYOffset;
        to[2] += win.pageXOffset;
        to[3] += win.pageYOffset;
      }

      BOUNDS_FORMAT.forEach(function (side, i) {
        side = side[0].toUpperCase() + side.substr(1);
        if (side === 'Top' || side === 'Left') {
          to[i] += parseFloat(style['border' + side + 'Width']);
        } else {
          to[i] -= parseFloat(style['border' + side + 'Width']);
        }
      });
    })();
  }

  return to;
}

TetherBase.modules.push({
  position: function position(_ref) {
    var _this = this;

    var top = _ref.top;
    var left = _ref.left;
    var targetAttachment = _ref.targetAttachment;

    if (!this.options.constraints) {
      return true;
    }

    var _cache = this.cache('element-bounds', function () {
      return getBounds(_this.element);
    });

    var height = _cache.height;
    var width = _cache.width;

    if (width === 0 && height === 0 && typeof this.lastSize !== 'undefined') {
      var _lastSize = this.lastSize;

      // Handle the item getting hidden as a result of our positioning without glitching
      // the classes in and out
      width = _lastSize.width;
      height = _lastSize.height;
    }

    var targetSize = this.cache('target-bounds', function () {
      return _this.getTargetBounds();
    });

    var targetHeight = targetSize.height;
    var targetWidth = targetSize.width;

    var allClasses = [this.getClass('pinned'), this.getClass('out-of-bounds')];

    this.options.constraints.forEach(function (constraint) {
      var outOfBoundsClass = constraint.outOfBoundsClass;
      var pinnedClass = constraint.pinnedClass;

      if (outOfBoundsClass) {
        allClasses.push(outOfBoundsClass);
      }
      if (pinnedClass) {
        allClasses.push(pinnedClass);
      }
    });

    allClasses.forEach(function (cls) {
      ['left', 'top', 'right', 'bottom'].forEach(function (side) {
        allClasses.push(cls + '-' + side);
      });
    });

    var addClasses = [];

    var tAttachment = extend({}, targetAttachment);
    var eAttachment = extend({}, this.attachment);

    this.options.constraints.forEach(function (constraint) {
      var to = constraint.to;
      var attachment = constraint.attachment;
      var pin = constraint.pin;

      if (typeof attachment === 'undefined') {
        attachment = '';
      }

      var changeAttachX = undefined,
          changeAttachY = undefined;
      if (attachment.indexOf(' ') >= 0) {
        var _attachment$split = attachment.split(' ');

        var _attachment$split2 = _slicedToArray(_attachment$split, 2);

        changeAttachY = _attachment$split2[0];
        changeAttachX = _attachment$split2[1];
      } else {
        changeAttachX = changeAttachY = attachment;
      }

      var bounds = getBoundingRect(_this, to);

      if (changeAttachY === 'target' || changeAttachY === 'both') {
        if (top < bounds[1] && tAttachment.top === 'top') {
          top += targetHeight;
          tAttachment.top = 'bottom';
        }

        if (top + height > bounds[3] && tAttachment.top === 'bottom') {
          top -= targetHeight;
          tAttachment.top = 'top';
        }
      }

      if (changeAttachY === 'together') {
        if (tAttachment.top === 'top') {
          if (eAttachment.top === 'bottom' && top < bounds[1]) {
            top += targetHeight;
            tAttachment.top = 'bottom';

            top += height;
            eAttachment.top = 'top';
          } else if (eAttachment.top === 'top' && top + height > bounds[3] && top - (height - targetHeight) >= bounds[1]) {
            top -= height - targetHeight;
            tAttachment.top = 'bottom';

            eAttachment.top = 'bottom';
          }
        }

        if (tAttachment.top === 'bottom') {
          if (eAttachment.top === 'top' && top + height > bounds[3]) {
            top -= targetHeight;
            tAttachment.top = 'top';

            top -= height;
            eAttachment.top = 'bottom';
          } else if (eAttachment.top === 'bottom' && top < bounds[1] && top + (height * 2 - targetHeight) <= bounds[3]) {
            top += height - targetHeight;
            tAttachment.top = 'top';

            eAttachment.top = 'top';
          }
        }

        if (tAttachment.top === 'middle') {
          if (top + height > bounds[3] && eAttachment.top === 'top') {
            top -= height;
            eAttachment.top = 'bottom';
          } else if (top < bounds[1] && eAttachment.top === 'bottom') {
            top += height;
            eAttachment.top = 'top';
          }
        }
      }

      if (changeAttachX === 'target' || changeAttachX === 'both') {
        if (left < bounds[0] && tAttachment.left === 'left') {
          left += targetWidth;
          tAttachment.left = 'right';
        }

        if (left + width > bounds[2] && tAttachment.left === 'right') {
          left -= targetWidth;
          tAttachment.left = 'left';
        }
      }

      if (changeAttachX === 'together') {
        if (left < bounds[0] && tAttachment.left === 'left') {
          if (eAttachment.left === 'right') {
            left += targetWidth;
            tAttachment.left = 'right';

            left += width;
            eAttachment.left = 'left';
          } else if (eAttachment.left === 'left') {
            left += targetWidth;
            tAttachment.left = 'right';

            left -= width;
            eAttachment.left = 'right';
          }
        } else if (left + width > bounds[2] && tAttachment.left === 'right') {
          if (eAttachment.left === 'left') {
            left -= targetWidth;
            tAttachment.left = 'left';

            left -= width;
            eAttachment.left = 'right';
          } else if (eAttachment.left === 'right') {
            left -= targetWidth;
            tAttachment.left = 'left';

            left += width;
            eAttachment.left = 'left';
          }
        } else if (tAttachment.left === 'center') {
          if (left + width > bounds[2] && eAttachment.left === 'left') {
            left -= width;
            eAttachment.left = 'right';
          } else if (left < bounds[0] && eAttachment.left === 'right') {
            left += width;
            eAttachment.left = 'left';
          }
        }
      }

      if (changeAttachY === 'element' || changeAttachY === 'both') {
        if (top < bounds[1] && eAttachment.top === 'bottom') {
          top += height;
          eAttachment.top = 'top';
        }

        if (top + height > bounds[3] && eAttachment.top === 'top') {
          top -= height;
          eAttachment.top = 'bottom';
        }
      }

      if (changeAttachX === 'element' || changeAttachX === 'both') {
        if (left < bounds[0]) {
          if (eAttachment.left === 'right') {
            left += width;
            eAttachment.left = 'left';
          } else if (eAttachment.left === 'center') {
            left += width / 2;
            eAttachment.left = 'left';
          }
        }

        if (left + width > bounds[2]) {
          if (eAttachment.left === 'left') {
            left -= width;
            eAttachment.left = 'right';
          } else if (eAttachment.left === 'center') {
            left -= width / 2;
            eAttachment.left = 'right';
          }
        }
      }

      if (typeof pin === 'string') {
        pin = pin.split(',').map(function (p) {
          return p.trim();
        });
      } else if (pin === true) {
        pin = ['top', 'left', 'right', 'bottom'];
      }

      pin = pin || [];

      var pinned = [];
      var oob = [];

      if (top < bounds[1]) {
        if (pin.indexOf('top') >= 0) {
          top = bounds[1];
          pinned.push('top');
        } else {
          oob.push('top');
        }
      }

      if (top + height > bounds[3]) {
        if (pin.indexOf('bottom') >= 0) {
          top = bounds[3] - height;
          pinned.push('bottom');
        } else {
          oob.push('bottom');
        }
      }

      if (left < bounds[0]) {
        if (pin.indexOf('left') >= 0) {
          left = bounds[0];
          pinned.push('left');
        } else {
          oob.push('left');
        }
      }

      if (left + width > bounds[2]) {
        if (pin.indexOf('right') >= 0) {
          left = bounds[2] - width;
          pinned.push('right');
        } else {
          oob.push('right');
        }
      }

      if (pinned.length) {
        (function () {
          var pinnedClass = undefined;
          if (typeof _this.options.pinnedClass !== 'undefined') {
            pinnedClass = _this.options.pinnedClass;
          } else {
            pinnedClass = _this.getClass('pinned');
          }

          addClasses.push(pinnedClass);
          pinned.forEach(function (side) {
            addClasses.push(pinnedClass + '-' + side);
          });
        })();
      }

      if (oob.length) {
        (function () {
          var oobClass = undefined;
          if (typeof _this.options.outOfBoundsClass !== 'undefined') {
            oobClass = _this.options.outOfBoundsClass;
          } else {
            oobClass = _this.getClass('out-of-bounds');
          }

          addClasses.push(oobClass);
          oob.forEach(function (side) {
            addClasses.push(oobClass + '-' + side);
          });
        })();
      }

      if (pinned.indexOf('left') >= 0 || pinned.indexOf('right') >= 0) {
        eAttachment.left = tAttachment.left = false;
      }
      if (pinned.indexOf('top') >= 0 || pinned.indexOf('bottom') >= 0) {
        eAttachment.top = tAttachment.top = false;
      }

      if (tAttachment.top !== targetAttachment.top || tAttachment.left !== targetAttachment.left || eAttachment.top !== _this.attachment.top || eAttachment.left !== _this.attachment.left) {
        _this.updateAttachClasses(eAttachment, tAttachment);
        _this.trigger('update', {
          attachment: eAttachment,
          targetAttachment: tAttachment
        });
      }
    });

    defer(function () {
      if (!(_this.options.addTargetClasses === false)) {
        updateClasses(_this.target, addClasses, allClasses);
      }
      updateClasses(_this.element, addClasses, allClasses);
    });

    return { top: top, left: left };
  }
});
/* globals TetherBase */

'use strict';

var _TetherBase$Utils = TetherBase.Utils;
var getBounds = _TetherBase$Utils.getBounds;
var updateClasses = _TetherBase$Utils.updateClasses;
var defer = _TetherBase$Utils.defer;

TetherBase.modules.push({
  position: function position(_ref) {
    var _this = this;

    var top = _ref.top;
    var left = _ref.left;

    var _cache = this.cache('element-bounds', function () {
      return getBounds(_this.element);
    });

    var height = _cache.height;
    var width = _cache.width;

    var targetPos = this.getTargetBounds();

    var bottom = top + height;
    var right = left + width;

    var abutted = [];
    if (top <= targetPos.bottom && bottom >= targetPos.top) {
      ['left', 'right'].forEach(function (side) {
        var targetPosSide = targetPos[side];
        if (targetPosSide === left || targetPosSide === right) {
          abutted.push(side);
        }
      });
    }

    if (left <= targetPos.right && right >= targetPos.left) {
      ['top', 'bottom'].forEach(function (side) {
        var targetPosSide = targetPos[side];
        if (targetPosSide === top || targetPosSide === bottom) {
          abutted.push(side);
        }
      });
    }

    var allClasses = [];
    var addClasses = [];

    var sides = ['left', 'top', 'right', 'bottom'];
    allClasses.push(this.getClass('abutted'));
    sides.forEach(function (side) {
      allClasses.push(_this.getClass('abutted') + '-' + side);
    });

    if (abutted.length) {
      addClasses.push(this.getClass('abutted'));
    }

    abutted.forEach(function (side) {
      addClasses.push(_this.getClass('abutted') + '-' + side);
    });

    defer(function () {
      if (!(_this.options.addTargetClasses === false)) {
        updateClasses(_this.target, addClasses, allClasses);
      }
      updateClasses(_this.element, addClasses, allClasses);
    });

    return true;
  }
});
/* globals TetherBase */

'use strict';

var _slicedToArray = (function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i['return']) _i['return'](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError('Invalid attempt to destructure non-iterable instance'); } }; })();

TetherBase.modules.push({
  position: function position(_ref) {
    var top = _ref.top;
    var left = _ref.left;

    if (!this.options.shift) {
      return;
    }

    var shift = this.options.shift;
    if (typeof this.options.shift === 'function') {
      shift = this.options.shift.call(this, { top: top, left: left });
    }

    var shiftTop = undefined,
        shiftLeft = undefined;
    if (typeof shift === 'string') {
      shift = shift.split(' ');
      shift[1] = shift[1] || shift[0];

      var _shift = shift;

      var _shift2 = _slicedToArray(_shift, 2);

      shiftTop = _shift2[0];
      shiftLeft = _shift2[1];

      shiftTop = parseFloat(shiftTop, 10);
      shiftLeft = parseFloat(shiftLeft, 10);
    } else {
      shiftTop = shift.top;
      shiftLeft = shift.left;
    }

    top += shiftTop;
    left += shiftLeft;

    return { top: top, left: left };
  }
});
return Tether;

}));

/*!
 * Bootstrap v3.3.7 (http://getbootstrap.com)
 * Copyright 2011-2016 Twitter, Inc.
 * Licensed under the MIT license
 */

if (typeof jQuery === 'undefined') {
  throw new Error('Bootstrap\'s JavaScript requires jQuery')
}

+function ($) {
  'use strict';
  var version = $.fn.jquery.split(' ')[0].split('.')
  if ((version[0] < 2 && version[1] < 9) || (version[0] == 1 && version[1] == 9 && version[2] < 1) || (version[0] > 3)) {
    throw new Error('Bootstrap\'s JavaScript requires jQuery version 1.9.1 or higher, but lower than version 4')
  }
}(jQuery);

/* ========================================================================
 * Bootstrap: transition.js v3.3.7
 * http://getbootstrap.com/javascript/#transitions
 * ========================================================================
 * Copyright 2011-2016 Twitter, Inc.
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 * ======================================================================== */


+function ($) {
  'use strict';

  // CSS TRANSITION SUPPORT (Shoutout: http://www.modernizr.com/)
  // ============================================================

  function transitionEnd() {
    var el = document.createElement('bootstrap')

    var transEndEventNames = {
      WebkitTransition : 'webkitTransitionEnd',
      MozTransition    : 'transitionend',
      OTransition      : 'oTransitionEnd otransitionend',
      transition       : 'transitionend'
    }

    for (var name in transEndEventNames) {
      if (el.style[name] !== undefined) {
        return { end: transEndEventNames[name] }
      }
    }

    return false // explicit for ie8 (  ._.)
  }

  // http://blog.alexmaccaw.com/css-transitions
  $.fn.emulateTransitionEnd = function (duration) {
    var called = false
    var $el = this
    $(this).one('bsTransitionEnd', function () { called = true })
    var callback = function () { if (!called) $($el).trigger($.support.transition.end) }
    setTimeout(callback, duration)
    return this
  }

  $(function () {
    $.support.transition = transitionEnd()

    if (!$.support.transition) return

    $.event.special.bsTransitionEnd = {
      bindType: $.support.transition.end,
      delegateType: $.support.transition.end,
      handle: function (e) {
        if ($(e.target).is(this)) return e.handleObj.handler.apply(this, arguments)
      }
    }
  })

}(jQuery);

/* ========================================================================
 * Bootstrap: alert.js v3.3.7
 * http://getbootstrap.com/javascript/#alerts
 * ========================================================================
 * Copyright 2011-2016 Twitter, Inc.
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 * ======================================================================== */


+function ($) {
  'use strict';

  // ALERT CLASS DEFINITION
  // ======================

  var dismiss = '[data-dismiss="alert"]'
  var Alert   = function (el) {
    $(el).on('click', dismiss, this.close)
  }

  Alert.VERSION = '3.3.7'

  Alert.TRANSITION_DURATION = 150

  Alert.prototype.close = function (e) {
    var $this    = $(this)
    var selector = $this.attr('data-target')

    if (!selector) {
      selector = $this.attr('href')
      selector = selector && selector.replace(/.*(?=#[^\s]*$)/, '') // strip for ie7
    }

    var $parent = $(selector === '#' ? [] : selector)

    if (e) e.preventDefault()

    if (!$parent.length) {
      $parent = $this.closest('.alert')
    }

    $parent.trigger(e = $.Event('close.bs.alert'))

    if (e.isDefaultPrevented()) return

    $parent.removeClass('in')

    function removeElement() {
      // detach from parent, fire event then clean up data
      $parent.detach().trigger('closed.bs.alert').remove()
    }

    $.support.transition && $parent.hasClass('fade') ?
      $parent
        .one('bsTransitionEnd', removeElement)
        .emulateTransitionEnd(Alert.TRANSITION_DURATION) :
      removeElement()
  }


  // ALERT PLUGIN DEFINITION
  // =======================

  function Plugin(option) {
    return this.each(function () {
      var $this = $(this)
      var data  = $this.data('bs.alert')

      if (!data) $this.data('bs.alert', (data = new Alert(this)))
      if (typeof option == 'string') data[option].call($this)
    })
  }

  var old = $.fn.alert

  $.fn.alert             = Plugin
  $.fn.alert.Constructor = Alert


  // ALERT NO CONFLICT
  // =================

  $.fn.alert.noConflict = function () {
    $.fn.alert = old
    return this
  }


  // ALERT DATA-API
  // ==============

  $(document).on('click.bs.alert.data-api', dismiss, Alert.prototype.close)

}(jQuery);

/* ========================================================================
 * Bootstrap: button.js v3.3.7
 * http://getbootstrap.com/javascript/#buttons
 * ========================================================================
 * Copyright 2011-2016 Twitter, Inc.
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 * ======================================================================== */


+function ($) {
  'use strict';

  // BUTTON PUBLIC CLASS DEFINITION
  // ==============================

  var Button = function (element, options) {
    this.$element  = $(element)
    this.options   = $.extend({}, Button.DEFAULTS, options)
    this.isLoading = false
  }

  Button.VERSION  = '3.3.7'

  Button.DEFAULTS = {
    loadingText: 'loading...'
  }

  Button.prototype.setState = function (state) {
    var d    = 'disabled'
    var $el  = this.$element
    var val  = $el.is('input') ? 'val' : 'html'
    var data = $el.data()

    state += 'Text'

    if (data.resetText == null) $el.data('resetText', $el[val]())

    // push to event loop to allow forms to submit
    setTimeout($.proxy(function () {
      $el[val](data[state] == null ? this.options[state] : data[state])

      if (state == 'loadingText') {
        this.isLoading = true
        $el.addClass(d).attr(d, d).prop(d, true)
      } else if (this.isLoading) {
        this.isLoading = false
        $el.removeClass(d).removeAttr(d).prop(d, false)
      }
    }, this), 0)
  }

  Button.prototype.toggle = function () {
    var changed = true
    var $parent = this.$element.closest('[data-toggle="buttons"]')

    if ($parent.length) {
      var $input = this.$element.find('input')
      if ($input.prop('type') == 'radio') {
        if ($input.prop('checked')) changed = false
        $parent.find('.active').removeClass('active')
        this.$element.addClass('active')
      } else if ($input.prop('type') == 'checkbox') {
        if (($input.prop('checked')) !== this.$element.hasClass('active')) changed = false
        this.$element.toggleClass('active')
      }
      $input.prop('checked', this.$element.hasClass('active'))
      if (changed) $input.trigger('change')
    } else {
      this.$element.attr('aria-pressed', !this.$element.hasClass('active'))
      this.$element.toggleClass('active')
    }
  }


  // BUTTON PLUGIN DEFINITION
  // ========================

  function Plugin(option) {
    return this.each(function () {
      var $this   = $(this)
      var data    = $this.data('bs.button')
      var options = typeof option == 'object' && option

      if (!data) $this.data('bs.button', (data = new Button(this, options)))

      if (option == 'toggle') data.toggle()
      else if (option) data.setState(option)
    })
  }

  var old = $.fn.button

  $.fn.button             = Plugin
  $.fn.button.Constructor = Button


  // BUTTON NO CONFLICT
  // ==================

  $.fn.button.noConflict = function () {
    $.fn.button = old
    return this
  }


  // BUTTON DATA-API
  // ===============

  $(document)
    .on('click.bs.button.data-api', '[data-toggle^="button"]', function (e) {
      var $btn = $(e.target).closest('.btn')
      Plugin.call($btn, 'toggle')
      if (!($(e.target).is('input[type="radio"], input[type="checkbox"]'))) {
        // Prevent double click on radios, and the double selections (so cancellation) on checkboxes
        e.preventDefault()
        // The target component still receive the focus
        if ($btn.is('input,button')) $btn.trigger('focus')
        else $btn.find('input:visible,button:visible').first().trigger('focus')
      }
    })
    .on('focus.bs.button.data-api blur.bs.button.data-api', '[data-toggle^="button"]', function (e) {
      $(e.target).closest('.btn').toggleClass('focus', /^focus(in)?$/.test(e.type))
    })

}(jQuery);

/* ========================================================================
 * Bootstrap: carousel.js v3.3.7
 * http://getbootstrap.com/javascript/#carousel
 * ========================================================================
 * Copyright 2011-2016 Twitter, Inc.
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 * ======================================================================== */


+function ($) {
  'use strict';

  // CAROUSEL CLASS DEFINITION
  // =========================

  var Carousel = function (element, options) {
    this.$element    = $(element)
    this.$indicators = this.$element.find('.carousel-indicators')
    this.options     = options
    this.paused      = null
    this.sliding     = null
    this.interval    = null
    this.$active     = null
    this.$items      = null

    this.options.keyboard && this.$element.on('keydown.bs.carousel', $.proxy(this.keydown, this))

    this.options.pause == 'hover' && !('ontouchstart' in document.documentElement) && this.$element
      .on('mouseenter.bs.carousel', $.proxy(this.pause, this))
      .on('mouseleave.bs.carousel', $.proxy(this.cycle, this))
  }

  Carousel.VERSION  = '3.3.7'

  Carousel.TRANSITION_DURATION = 600

  Carousel.DEFAULTS = {
    interval: 5000,
    pause: 'hover',
    wrap: true,
    keyboard: true
  }

  Carousel.prototype.keydown = function (e) {
    if (/input|textarea/i.test(e.target.tagName)) return
    switch (e.which) {
      case 37: this.prev(); break
      case 39: this.next(); break
      default: return
    }

    e.preventDefault()
  }

  Carousel.prototype.cycle = function (e) {
    e || (this.paused = false)

    this.interval && clearInterval(this.interval)

    this.options.interval
      && !this.paused
      && (this.interval = setInterval($.proxy(this.next, this), this.options.interval))

    return this
  }

  Carousel.prototype.getItemIndex = function (item) {
    this.$items = item.parent().children('.item')
    return this.$items.index(item || this.$active)
  }

  Carousel.prototype.getItemForDirection = function (direction, active) {
    var activeIndex = this.getItemIndex(active)
    var willWrap = (direction == 'prev' && activeIndex === 0)
                || (direction == 'next' && activeIndex == (this.$items.length - 1))
    if (willWrap && !this.options.wrap) return active
    var delta = direction == 'prev' ? -1 : 1
    var itemIndex = (activeIndex + delta) % this.$items.length
    return this.$items.eq(itemIndex)
  }

  Carousel.prototype.to = function (pos) {
    var that        = this
    var activeIndex = this.getItemIndex(this.$active = this.$element.find('.item.active'))

    if (pos > (this.$items.length - 1) || pos < 0) return

    if (this.sliding)       return this.$element.one('slid.bs.carousel', function () { that.to(pos) }) // yes, "slid"
    if (activeIndex == pos) return this.pause().cycle()

    return this.slide(pos > activeIndex ? 'next' : 'prev', this.$items.eq(pos))
  }

  Carousel.prototype.pause = function (e) {
    e || (this.paused = true)

    if (this.$element.find('.next, .prev').length && $.support.transition) {
      this.$element.trigger($.support.transition.end)
      this.cycle(true)
    }

    this.interval = clearInterval(this.interval)

    return this
  }

  Carousel.prototype.next = function () {
    if (this.sliding) return
    return this.slide('next')
  }

  Carousel.prototype.prev = function () {
    if (this.sliding) return
    return this.slide('prev')
  }

  Carousel.prototype.slide = function (type, next) {
    var $active   = this.$element.find('.item.active')
    var $next     = next || this.getItemForDirection(type, $active)
    var isCycling = this.interval
    var direction = type == 'next' ? 'left' : 'right'
    var that      = this

    if ($next.hasClass('active')) return (this.sliding = false)

    var relatedTarget = $next[0]
    var slideEvent = $.Event('slide.bs.carousel', {
      relatedTarget: relatedTarget,
      direction: direction
    })
    this.$element.trigger(slideEvent)
    if (slideEvent.isDefaultPrevented()) return

    this.sliding = true

    isCycling && this.pause()

    if (this.$indicators.length) {
      this.$indicators.find('.active').removeClass('active')
      var $nextIndicator = $(this.$indicators.children()[this.getItemIndex($next)])
      $nextIndicator && $nextIndicator.addClass('active')
    }

    var slidEvent = $.Event('slid.bs.carousel', { relatedTarget: relatedTarget, direction: direction }) // yes, "slid"
    if ($.support.transition && this.$element.hasClass('slide')) {
      $next.addClass(type)
      $next[0].offsetWidth // force reflow
      $active.addClass(direction)
      $next.addClass(direction)
      $active
        .one('bsTransitionEnd', function () {
          $next.removeClass([type, direction].join(' ')).addClass('active')
          $active.removeClass(['active', direction].join(' '))
          that.sliding = false
          setTimeout(function () {
            that.$element.trigger(slidEvent)
          }, 0)
        })
        .emulateTransitionEnd(Carousel.TRANSITION_DURATION)
    } else {
      $active.removeClass('active')
      $next.addClass('active')
      this.sliding = false
      this.$element.trigger(slidEvent)
    }

    isCycling && this.cycle()

    return this
  }


  // CAROUSEL PLUGIN DEFINITION
  // ==========================

  function Plugin(option) {
    return this.each(function () {
      var $this   = $(this)
      var data    = $this.data('bs.carousel')
      var options = $.extend({}, Carousel.DEFAULTS, $this.data(), typeof option == 'object' && option)
      var action  = typeof option == 'string' ? option : options.slide

      if (!data) $this.data('bs.carousel', (data = new Carousel(this, options)))
      if (typeof option == 'number') data.to(option)
      else if (action) data[action]()
      else if (options.interval) data.pause().cycle()
    })
  }

  var old = $.fn.carousel

  $.fn.carousel             = Plugin
  $.fn.carousel.Constructor = Carousel


  // CAROUSEL NO CONFLICT
  // ====================

  $.fn.carousel.noConflict = function () {
    $.fn.carousel = old
    return this
  }


  // CAROUSEL DATA-API
  // =================

  var clickHandler = function (e) {
    var href
    var $this   = $(this)
    var $target = $($this.attr('data-target') || (href = $this.attr('href')) && href.replace(/.*(?=#[^\s]+$)/, '')) // strip for ie7
    if (!$target.hasClass('carousel')) return
    var options = $.extend({}, $target.data(), $this.data())
    var slideIndex = $this.attr('data-slide-to')
    if (slideIndex) options.interval = false

    Plugin.call($target, options)

    if (slideIndex) {
      $target.data('bs.carousel').to(slideIndex)
    }

    e.preventDefault()
  }

  $(document)
    .on('click.bs.carousel.data-api', '[data-slide]', clickHandler)
    .on('click.bs.carousel.data-api', '[data-slide-to]', clickHandler)

  $(window).on('load', function () {
    $('[data-ride="carousel"]').each(function () {
      var $carousel = $(this)
      Plugin.call($carousel, $carousel.data())
    })
  })

}(jQuery);

/* ========================================================================
 * Bootstrap: collapse.js v3.3.7
 * http://getbootstrap.com/javascript/#collapse
 * ========================================================================
 * Copyright 2011-2016 Twitter, Inc.
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 * ======================================================================== */

/* jshint latedef: false */

+function ($) {
  'use strict';

  // COLLAPSE PUBLIC CLASS DEFINITION
  // ================================

  var Collapse = function (element, options) {
    this.$element      = $(element)
    this.options       = $.extend({}, Collapse.DEFAULTS, options)
    this.$trigger      = $('[data-toggle="collapse"][href="#' + element.id + '"],' +
                           '[data-toggle="collapse"][data-target="#' + element.id + '"]')
    this.transitioning = null

    if (this.options.parent) {
      this.$parent = this.getParent()
    } else {
      this.addAriaAndCollapsedClass(this.$element, this.$trigger)
    }

    if (this.options.toggle) this.toggle()
  }

  Collapse.VERSION  = '3.3.7'

  Collapse.TRANSITION_DURATION = 350

  Collapse.DEFAULTS = {
    toggle: true
  }

  Collapse.prototype.dimension = function () {
    var hasWidth = this.$element.hasClass('width')
    return hasWidth ? 'width' : 'height'
  }

  Collapse.prototype.show = function () {
    if (this.transitioning || this.$element.hasClass('in')) return

    var activesData
    var actives = this.$parent && this.$parent.children('.panel').children('.in, .collapsing')

    if (actives && actives.length) {
      activesData = actives.data('bs.collapse')
      if (activesData && activesData.transitioning) return
    }

    var startEvent = $.Event('show.bs.collapse')
    this.$element.trigger(startEvent)
    if (startEvent.isDefaultPrevented()) return

    if (actives && actives.length) {
      Plugin.call(actives, 'hide')
      activesData || actives.data('bs.collapse', null)
    }

    var dimension = this.dimension()

    this.$element
      .removeClass('collapse')
      .addClass('collapsing')[dimension](0)
      .attr('aria-expanded', true)

    this.$trigger
      .removeClass('collapsed')
      .attr('aria-expanded', true)

    this.transitioning = 1

    var complete = function () {
      this.$element
        .removeClass('collapsing')
        .addClass('collapse in')[dimension]('')
      this.transitioning = 0
      this.$element
        .trigger('shown.bs.collapse')
    }

    if (!$.support.transition) return complete.call(this)

    var scrollSize = $.camelCase(['scroll', dimension].join('-'))

    this.$element
      .one('bsTransitionEnd', $.proxy(complete, this))
      .emulateTransitionEnd(Collapse.TRANSITION_DURATION)[dimension](this.$element[0][scrollSize])
  }

  Collapse.prototype.hide = function () {
    if (this.transitioning || !this.$element.hasClass('in')) return

    var startEvent = $.Event('hide.bs.collapse')
    this.$element.trigger(startEvent)
    if (startEvent.isDefaultPrevented()) return

    var dimension = this.dimension()

    this.$element[dimension](this.$element[dimension]())[0].offsetHeight

    this.$element
      .addClass('collapsing')
      .removeClass('collapse in')
      .attr('aria-expanded', false)

    this.$trigger
      .addClass('collapsed')
      .attr('aria-expanded', false)

    this.transitioning = 1

    var complete = function () {
      this.transitioning = 0
      this.$element
        .removeClass('collapsing')
        .addClass('collapse')
        .trigger('hidden.bs.collapse')
    }

    if (!$.support.transition) return complete.call(this)

    this.$element
      [dimension](0)
      .one('bsTransitionEnd', $.proxy(complete, this))
      .emulateTransitionEnd(Collapse.TRANSITION_DURATION)
  }

  Collapse.prototype.toggle = function () {
    this[this.$element.hasClass('in') ? 'hide' : 'show']()
  }

  Collapse.prototype.getParent = function () {
    return $(this.options.parent)
      .find('[data-toggle="collapse"][data-parent="' + this.options.parent + '"]')
      .each($.proxy(function (i, element) {
        var $element = $(element)
        this.addAriaAndCollapsedClass(getTargetFromTrigger($element), $element)
      }, this))
      .end()
  }

  Collapse.prototype.addAriaAndCollapsedClass = function ($element, $trigger) {
    var isOpen = $element.hasClass('in')

    $element.attr('aria-expanded', isOpen)
    $trigger
      .toggleClass('collapsed', !isOpen)
      .attr('aria-expanded', isOpen)
  }

  function getTargetFromTrigger($trigger) {
    var href
    var target = $trigger.attr('data-target')
      || (href = $trigger.attr('href')) && href.replace(/.*(?=#[^\s]+$)/, '') // strip for ie7

    return $(target)
  }


  // COLLAPSE PLUGIN DEFINITION
  // ==========================

  function Plugin(option) {
    return this.each(function () {
      var $this   = $(this)
      var data    = $this.data('bs.collapse')
      var options = $.extend({}, Collapse.DEFAULTS, $this.data(), typeof option == 'object' && option)

      if (!data && options.toggle && /show|hide/.test(option)) options.toggle = false
      if (!data) $this.data('bs.collapse', (data = new Collapse(this, options)))
      if (typeof option == 'string') data[option]()
    })
  }

  var old = $.fn.collapse

  $.fn.collapse             = Plugin
  $.fn.collapse.Constructor = Collapse


  // COLLAPSE NO CONFLICT
  // ====================

  $.fn.collapse.noConflict = function () {
    $.fn.collapse = old
    return this
  }


  // COLLAPSE DATA-API
  // =================

  $(document).on('click.bs.collapse.data-api', '[data-toggle="collapse"]', function (e) {
    var $this   = $(this)

    if (!$this.attr('data-target')) e.preventDefault()

    var $target = getTargetFromTrigger($this)
    var data    = $target.data('bs.collapse')
    var option  = data ? 'toggle' : $this.data()

    Plugin.call($target, option)
  })

}(jQuery);

/* ========================================================================
 * Bootstrap: dropdown.js v3.3.7
 * http://getbootstrap.com/javascript/#dropdowns
 * ========================================================================
 * Copyright 2011-2016 Twitter, Inc.
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 * ======================================================================== */


+function ($) {
  'use strict';

  // DROPDOWN CLASS DEFINITION
  // =========================

  var backdrop = '.dropdown-backdrop'
  var toggle   = '[data-toggle="dropdown"]'
  var Dropdown = function (element) {
    $(element).on('click.bs.dropdown', this.toggle)
  }

  Dropdown.VERSION = '3.3.7'

  function getParent($this) {
    var selector = $this.attr('data-target')

    if (!selector) {
      selector = $this.attr('href')
      selector = selector && /#[A-Za-z]/.test(selector) && selector.replace(/.*(?=#[^\s]*$)/, '') // strip for ie7
    }

    var $parent = selector && $(selector)

    return $parent && $parent.length ? $parent : $this.parent()
  }

  function clearMenus(e) {
    if (e && e.which === 3) return
    $(backdrop).remove()
    $(toggle).each(function () {
      var $this         = $(this)
      var $parent       = getParent($this)
      var relatedTarget = { relatedTarget: this }

      if (!$parent.hasClass('open')) return

      if (e && e.type == 'click' && /input|textarea/i.test(e.target.tagName) && $.contains($parent[0], e.target)) return

      $parent.trigger(e = $.Event('hide.bs.dropdown', relatedTarget))

      if (e.isDefaultPrevented()) return

      $this.attr('aria-expanded', 'false')
      $parent.removeClass('open').trigger($.Event('hidden.bs.dropdown', relatedTarget))
    })
  }

  Dropdown.prototype.toggle = function (e) {
    var $this = $(this)

    if ($this.is('.disabled, :disabled')) return

    var $parent  = getParent($this)
    var isActive = $parent.hasClass('open')

    clearMenus()

    if (!isActive) {
      if ('ontouchstart' in document.documentElement && !$parent.closest('.navbar-nav').length) {
        // if mobile we use a backdrop because click events don't delegate
        $(document.createElement('div'))
          .addClass('dropdown-backdrop')
          .insertAfter($(this))
          .on('click', clearMenus)
      }

      var relatedTarget = { relatedTarget: this }
      $parent.trigger(e = $.Event('show.bs.dropdown', relatedTarget))

      if (e.isDefaultPrevented()) return

      $this
        .trigger('focus')
        .attr('aria-expanded', 'true')

      $parent
        .toggleClass('open')
        .trigger($.Event('shown.bs.dropdown', relatedTarget))
    }

    return false
  }

  Dropdown.prototype.keydown = function (e) {
    if (!/(38|40|27|32)/.test(e.which) || /input|textarea/i.test(e.target.tagName)) return

    var $this = $(this)

    e.preventDefault()
    e.stopPropagation()

    if ($this.is('.disabled, :disabled')) return

    var $parent  = getParent($this)
    var isActive = $parent.hasClass('open')

    if (!isActive && e.which != 27 || isActive && e.which == 27) {
      if (e.which == 27) $parent.find(toggle).trigger('focus')
      return $this.trigger('click')
    }

    var desc = ' li:not(.disabled):visible a'
    var $items = $parent.find('.dropdown-menu' + desc)

    if (!$items.length) return

    var index = $items.index(e.target)

    if (e.which == 38 && index > 0)                 index--         // up
    if (e.which == 40 && index < $items.length - 1) index++         // down
    if (!~index)                                    index = 0

    $items.eq(index).trigger('focus')
  }


  // DROPDOWN PLUGIN DEFINITION
  // ==========================

  function Plugin(option) {
    return this.each(function () {
      var $this = $(this)
      var data  = $this.data('bs.dropdown')

      if (!data) $this.data('bs.dropdown', (data = new Dropdown(this)))
      if (typeof option == 'string') data[option].call($this)
    })
  }

  var old = $.fn.dropdown

  $.fn.dropdown             = Plugin
  $.fn.dropdown.Constructor = Dropdown


  // DROPDOWN NO CONFLICT
  // ====================

  $.fn.dropdown.noConflict = function () {
    $.fn.dropdown = old
    return this
  }


  // APPLY TO STANDARD DROPDOWN ELEMENTS
  // ===================================

  $(document)
    .on('click.bs.dropdown.data-api', clearMenus)
    .on('click.bs.dropdown.data-api', '.dropdown form', function (e) { e.stopPropagation() })
    .on('click.bs.dropdown.data-api', toggle, Dropdown.prototype.toggle)
    .on('keydown.bs.dropdown.data-api', toggle, Dropdown.prototype.keydown)
    .on('keydown.bs.dropdown.data-api', '.dropdown-menu', Dropdown.prototype.keydown)

}(jQuery);

/* ========================================================================
 * Bootstrap: modal.js v3.3.7
 * http://getbootstrap.com/javascript/#modals
 * ========================================================================
 * Copyright 2011-2016 Twitter, Inc.
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 * ======================================================================== */


+function ($) {
  'use strict';

  // MODAL CLASS DEFINITION
  // ======================

  var Modal = function (element, options) {
    this.options             = options
    this.$body               = $(document.body)
    this.$element            = $(element)
    this.$dialog             = this.$element.find('.modal-dialog')
    this.$backdrop           = null
    this.isShown             = null
    this.originalBodyPad     = null
    this.scrollbarWidth      = 0
    this.ignoreBackdropClick = false

    if (this.options.remote) {
      this.$element
        .find('.modal-content')
        .load(this.options.remote, $.proxy(function () {
          this.$element.trigger('loaded.bs.modal')
        }, this))
    }
  }

  Modal.VERSION  = '3.3.7'

  Modal.TRANSITION_DURATION = 300
  Modal.BACKDROP_TRANSITION_DURATION = 150

  Modal.DEFAULTS = {
    backdrop: true,
    keyboard: true,
    show: true
  }

  Modal.prototype.toggle = function (_relatedTarget) {
    return this.isShown ? this.hide() : this.show(_relatedTarget)
  }

  Modal.prototype.show = function (_relatedTarget) {
    var that = this
    var e    = $.Event('show.bs.modal', { relatedTarget: _relatedTarget })

    this.$element.trigger(e)

    if (this.isShown || e.isDefaultPrevented()) return

    this.isShown = true

    this.checkScrollbar()
    this.setScrollbar()
    this.$body.addClass('modal-open')

    this.escape()
    this.resize()

    this.$element.on('click.dismiss.bs.modal', '[data-dismiss="modal"]', $.proxy(this.hide, this))

    this.$dialog.on('mousedown.dismiss.bs.modal', function () {
      that.$element.one('mouseup.dismiss.bs.modal', function (e) {
        if ($(e.target).is(that.$element)) that.ignoreBackdropClick = true
      })
    })

    this.backdrop(function () {
      var transition = $.support.transition && that.$element.hasClass('fade')

      if (!that.$element.parent().length) {
        that.$element.appendTo(that.$body) // don't move modals dom position
      }

      that.$element
        .show()
        .scrollTop(0)

      that.adjustDialog()

      if (transition) {
        that.$element[0].offsetWidth // force reflow
      }

      that.$element.addClass('in')

      that.enforceFocus()

      var e = $.Event('shown.bs.modal', { relatedTarget: _relatedTarget })

      transition ?
        that.$dialog // wait for modal to slide in
          .one('bsTransitionEnd', function () {
            that.$element.trigger('focus').trigger(e)
          })
          .emulateTransitionEnd(Modal.TRANSITION_DURATION) :
        that.$element.trigger('focus').trigger(e)
    })
  }

  Modal.prototype.hide = function (e) {
    if (e) e.preventDefault()

    e = $.Event('hide.bs.modal')

    this.$element.trigger(e)

    if (!this.isShown || e.isDefaultPrevented()) return

    this.isShown = false

    this.escape()
    this.resize()

    $(document).off('focusin.bs.modal')

    this.$element
      .removeClass('in')
      .off('click.dismiss.bs.modal')
      .off('mouseup.dismiss.bs.modal')

    this.$dialog.off('mousedown.dismiss.bs.modal')

    $.support.transition && this.$element.hasClass('fade') ?
      this.$element
        .one('bsTransitionEnd', $.proxy(this.hideModal, this))
        .emulateTransitionEnd(Modal.TRANSITION_DURATION) :
      this.hideModal()
  }

  Modal.prototype.enforceFocus = function () {
    $(document)
      .off('focusin.bs.modal') // guard against infinite focus loop
      .on('focusin.bs.modal', $.proxy(function (e) {
        if (document !== e.target &&
            this.$element[0] !== e.target &&
            !this.$element.has(e.target).length) {
          this.$element.trigger('focus')
        }
      }, this))
  }

  Modal.prototype.escape = function () {
    if (this.isShown && this.options.keyboard) {
      this.$element.on('keydown.dismiss.bs.modal', $.proxy(function (e) {
        e.which == 27 && this.hide()
      }, this))
    } else if (!this.isShown) {
      this.$element.off('keydown.dismiss.bs.modal')
    }
  }

  Modal.prototype.resize = function () {
    if (this.isShown) {
      $(window).on('resize.bs.modal', $.proxy(this.handleUpdate, this))
    } else {
      $(window).off('resize.bs.modal')
    }
  }

  Modal.prototype.hideModal = function () {
    var that = this
    this.$element.hide()
    this.backdrop(function () {
      that.$body.removeClass('modal-open')
      that.resetAdjustments()
      that.resetScrollbar()
      that.$element.trigger('hidden.bs.modal')
    })
  }

  Modal.prototype.removeBackdrop = function () {
    this.$backdrop && this.$backdrop.remove()
    this.$backdrop = null
  }

  Modal.prototype.backdrop = function (callback) {
    var that = this
    var animate = this.$element.hasClass('fade') ? 'fade' : ''

    if (this.isShown && this.options.backdrop) {
      var doAnimate = $.support.transition && animate

      this.$backdrop = $(document.createElement('div'))
        .addClass('modal-backdrop ' + animate)
        .appendTo(this.$body)

      this.$element.on('click.dismiss.bs.modal', $.proxy(function (e) {
        if (this.ignoreBackdropClick) {
          this.ignoreBackdropClick = false
          return
        }
        if (e.target !== e.currentTarget) return
        this.options.backdrop == 'static'
          ? this.$element[0].focus()
          : this.hide()
      }, this))

      if (doAnimate) this.$backdrop[0].offsetWidth // force reflow

      this.$backdrop.addClass('in')

      if (!callback) return

      doAnimate ?
        this.$backdrop
          .one('bsTransitionEnd', callback)
          .emulateTransitionEnd(Modal.BACKDROP_TRANSITION_DURATION) :
        callback()

    } else if (!this.isShown && this.$backdrop) {
      this.$backdrop.removeClass('in')

      var callbackRemove = function () {
        that.removeBackdrop()
        callback && callback()
      }
      $.support.transition && this.$element.hasClass('fade') ?
        this.$backdrop
          .one('bsTransitionEnd', callbackRemove)
          .emulateTransitionEnd(Modal.BACKDROP_TRANSITION_DURATION) :
        callbackRemove()

    } else if (callback) {
      callback()
    }
  }

  // these following methods are used to handle overflowing modals

  Modal.prototype.handleUpdate = function () {
    this.adjustDialog()
  }

  Modal.prototype.adjustDialog = function () {
    var modalIsOverflowing = this.$element[0].scrollHeight > document.documentElement.clientHeight

    this.$element.css({
      paddingLeft:  !this.bodyIsOverflowing && modalIsOverflowing ? this.scrollbarWidth : '',
      paddingRight: this.bodyIsOverflowing && !modalIsOverflowing ? this.scrollbarWidth : ''
    })
  }

  Modal.prototype.resetAdjustments = function () {
    this.$element.css({
      paddingLeft: '',
      paddingRight: ''
    })
  }

  Modal.prototype.checkScrollbar = function () {
    var fullWindowWidth = window.innerWidth
    if (!fullWindowWidth) { // workaround for missing window.innerWidth in IE8
      var documentElementRect = document.documentElement.getBoundingClientRect()
      fullWindowWidth = documentElementRect.right - Math.abs(documentElementRect.left)
    }
    this.bodyIsOverflowing = document.body.clientWidth < fullWindowWidth
    this.scrollbarWidth = this.measureScrollbar()
  }

  Modal.prototype.setScrollbar = function () {
    var bodyPad = parseInt((this.$body.css('padding-right') || 0), 10)
    this.originalBodyPad = document.body.style.paddingRight || ''
    if (this.bodyIsOverflowing) this.$body.css('padding-right', bodyPad + this.scrollbarWidth)
  }

  Modal.prototype.resetScrollbar = function () {
    this.$body.css('padding-right', this.originalBodyPad)
  }

  Modal.prototype.measureScrollbar = function () { // thx walsh
    var scrollDiv = document.createElement('div')
    scrollDiv.className = 'modal-scrollbar-measure'
    this.$body.append(scrollDiv)
    var scrollbarWidth = scrollDiv.offsetWidth - scrollDiv.clientWidth
    this.$body[0].removeChild(scrollDiv)
    return scrollbarWidth
  }


  // MODAL PLUGIN DEFINITION
  // =======================

  function Plugin(option, _relatedTarget) {
    return this.each(function () {
      var $this   = $(this)
      var data    = $this.data('bs.modal')
      var options = $.extend({}, Modal.DEFAULTS, $this.data(), typeof option == 'object' && option)

      if (!data) $this.data('bs.modal', (data = new Modal(this, options)))
      if (typeof option == 'string') data[option](_relatedTarget)
      else if (options.show) data.show(_relatedTarget)
    })
  }

  var old = $.fn.modal

  $.fn.modal             = Plugin
  $.fn.modal.Constructor = Modal


  // MODAL NO CONFLICT
  // =================

  $.fn.modal.noConflict = function () {
    $.fn.modal = old
    return this
  }


  // MODAL DATA-API
  // ==============

  $(document).on('click.bs.modal.data-api', '[data-toggle="modal"]', function (e) {
    var $this   = $(this)
    var href    = $this.attr('href')
    var $target = $($this.attr('data-target') || (href && href.replace(/.*(?=#[^\s]+$)/, ''))) // strip for ie7
    var option  = $target.data('bs.modal') ? 'toggle' : $.extend({ remote: !/#/.test(href) && href }, $target.data(), $this.data())

    if ($this.is('a')) e.preventDefault()

    $target.one('show.bs.modal', function (showEvent) {
      if (showEvent.isDefaultPrevented()) return // only register focus restorer if modal will actually get shown
      $target.one('hidden.bs.modal', function () {
        $this.is(':visible') && $this.trigger('focus')
      })
    })
    Plugin.call($target, option, this)
  })

}(jQuery);

/* ========================================================================
 * Bootstrap: tooltip.js v3.3.7
 * http://getbootstrap.com/javascript/#tooltip
 * Inspired by the original jQuery.tipsy by Jason Frame
 * ========================================================================
 * Copyright 2011-2016 Twitter, Inc.
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 * ======================================================================== */


+function ($) {
  'use strict';

  // TOOLTIP PUBLIC CLASS DEFINITION
  // ===============================

  var Tooltip = function (element, options) {
    this.type       = null
    this.options    = null
    this.enabled    = null
    this.timeout    = null
    this.hoverState = null
    this.$element   = null
    this.inState    = null

    this.init('tooltip', element, options)
  }

  Tooltip.VERSION  = '3.3.7'

  Tooltip.TRANSITION_DURATION = 150

  Tooltip.DEFAULTS = {
    animation: true,
    placement: 'top',
    selector: false,
    template: '<div class="tooltip" role="tooltip"><div class="tooltip-arrow"></div><div class="tooltip-inner"></div></div>',
    trigger: 'hover focus',
    title: '',
    delay: 0,
    html: false,
    container: false,
    viewport: {
      selector: 'body',
      padding: 0
    }
  }

  Tooltip.prototype.init = function (type, element, options) {
    this.enabled   = true
    this.type      = type
    this.$element  = $(element)
    this.options   = this.getOptions(options)
    this.$viewport = this.options.viewport && $($.isFunction(this.options.viewport) ? this.options.viewport.call(this, this.$element) : (this.options.viewport.selector || this.options.viewport))
    this.inState   = { click: false, hover: false, focus: false }

    if (this.$element[0] instanceof document.constructor && !this.options.selector) {
      throw new Error('`selector` option must be specified when initializing ' + this.type + ' on the window.document object!')
    }

    var triggers = this.options.trigger.split(' ')

    for (var i = triggers.length; i--;) {
      var trigger = triggers[i]

      if (trigger == 'click') {
        this.$element.on('click.' + this.type, this.options.selector, $.proxy(this.toggle, this))
      } else if (trigger != 'manual') {
        var eventIn  = trigger == 'hover' ? 'mouseenter' : 'focusin'
        var eventOut = trigger == 'hover' ? 'mouseleave' : 'focusout'

        this.$element.on(eventIn  + '.' + this.type, this.options.selector, $.proxy(this.enter, this))
        this.$element.on(eventOut + '.' + this.type, this.options.selector, $.proxy(this.leave, this))
      }
    }

    this.options.selector ?
      (this._options = $.extend({}, this.options, { trigger: 'manual', selector: '' })) :
      this.fixTitle()
  }

  Tooltip.prototype.getDefaults = function () {
    return Tooltip.DEFAULTS
  }

  Tooltip.prototype.getOptions = function (options) {
    options = $.extend({}, this.getDefaults(), this.$element.data(), options)

    if (options.delay && typeof options.delay == 'number') {
      options.delay = {
        show: options.delay,
        hide: options.delay
      }
    }

    return options
  }

  Tooltip.prototype.getDelegateOptions = function () {
    var options  = {}
    var defaults = this.getDefaults()

    this._options && $.each(this._options, function (key, value) {
      if (defaults[key] != value) options[key] = value
    })

    return options
  }

  Tooltip.prototype.enter = function (obj) {
    var self = obj instanceof this.constructor ?
      obj : $(obj.currentTarget).data('bs.' + this.type)

    if (!self) {
      self = new this.constructor(obj.currentTarget, this.getDelegateOptions())
      $(obj.currentTarget).data('bs.' + this.type, self)
    }

    if (obj instanceof $.Event) {
      self.inState[obj.type == 'focusin' ? 'focus' : 'hover'] = true
    }

    if (self.tip().hasClass('in') || self.hoverState == 'in') {
      self.hoverState = 'in'
      return
    }

    clearTimeout(self.timeout)

    self.hoverState = 'in'

    if (!self.options.delay || !self.options.delay.show) return self.show()

    self.timeout = setTimeout(function () {
      if (self.hoverState == 'in') self.show()
    }, self.options.delay.show)
  }

  Tooltip.prototype.isInStateTrue = function () {
    for (var key in this.inState) {
      if (this.inState[key]) return true
    }

    return false
  }

  Tooltip.prototype.leave = function (obj) {
    var self = obj instanceof this.constructor ?
      obj : $(obj.currentTarget).data('bs.' + this.type)

    if (!self) {
      self = new this.constructor(obj.currentTarget, this.getDelegateOptions())
      $(obj.currentTarget).data('bs.' + this.type, self)
    }

    if (obj instanceof $.Event) {
      self.inState[obj.type == 'focusout' ? 'focus' : 'hover'] = false
    }

    if (self.isInStateTrue()) return

    clearTimeout(self.timeout)

    self.hoverState = 'out'

    if (!self.options.delay || !self.options.delay.hide) return self.hide()

    self.timeout = setTimeout(function () {
      if (self.hoverState == 'out') self.hide()
    }, self.options.delay.hide)
  }

  Tooltip.prototype.show = function () {
    var e = $.Event('show.bs.' + this.type)

    if (this.hasContent() && this.enabled) {
      this.$element.trigger(e)

      var inDom = $.contains(this.$element[0].ownerDocument.documentElement, this.$element[0])
      if (e.isDefaultPrevented() || !inDom) return
      var that = this

      var $tip = this.tip()

      var tipId = this.getUID(this.type)

      this.setContent()
      $tip.attr('id', tipId)
      this.$element.attr('aria-describedby', tipId)

      if (this.options.animation) $tip.addClass('fade')

      var placement = typeof this.options.placement == 'function' ?
        this.options.placement.call(this, $tip[0], this.$element[0]) :
        this.options.placement

      var autoToken = /\s?auto?\s?/i
      var autoPlace = autoToken.test(placement)
      if (autoPlace) placement = placement.replace(autoToken, '') || 'top'

      $tip
        .detach()
        .css({ top: 0, left: 0, display: 'block' })
        .addClass(placement)
        .data('bs.' + this.type, this)

      this.options.container ? $tip.appendTo(this.options.container) : $tip.insertAfter(this.$element)
      this.$element.trigger('inserted.bs.' + this.type)

      var pos          = this.getPosition()
      var actualWidth  = $tip[0].offsetWidth
      var actualHeight = $tip[0].offsetHeight

      if (autoPlace) {
        var orgPlacement = placement
        var viewportDim = this.getPosition(this.$viewport)

        placement = placement == 'bottom' && pos.bottom + actualHeight > viewportDim.bottom ? 'top'    :
                    placement == 'top'    && pos.top    - actualHeight < viewportDim.top    ? 'bottom' :
                    placement == 'right'  && pos.right  + actualWidth  > viewportDim.width  ? 'left'   :
                    placement == 'left'   && pos.left   - actualWidth  < viewportDim.left   ? 'right'  :
                    placement

        $tip
          .removeClass(orgPlacement)
          .addClass(placement)
      }

      var calculatedOffset = this.getCalculatedOffset(placement, pos, actualWidth, actualHeight)

      this.applyPlacement(calculatedOffset, placement)

      var complete = function () {
        var prevHoverState = that.hoverState
        that.$element.trigger('shown.bs.' + that.type)
        that.hoverState = null

        if (prevHoverState == 'out') that.leave(that)
      }

      $.support.transition && this.$tip.hasClass('fade') ?
        $tip
          .one('bsTransitionEnd', complete)
          .emulateTransitionEnd(Tooltip.TRANSITION_DURATION) :
        complete()
    }
  }

  Tooltip.prototype.applyPlacement = function (offset, placement) {
    var $tip   = this.tip()
    var width  = $tip[0].offsetWidth
    var height = $tip[0].offsetHeight

    // manually read margins because getBoundingClientRect includes difference
    var marginTop = parseInt($tip.css('margin-top'), 10)
    var marginLeft = parseInt($tip.css('margin-left'), 10)

    // we must check for NaN for ie 8/9
    if (isNaN(marginTop))  marginTop  = 0
    if (isNaN(marginLeft)) marginLeft = 0

    offset.top  += marginTop
    offset.left += marginLeft

    // $.fn.offset doesn't round pixel values
    // so we use setOffset directly with our own function B-0
    $.offset.setOffset($tip[0], $.extend({
      using: function (props) {
        $tip.css({
          top: Math.round(props.top),
          left: Math.round(props.left)
        })
      }
    }, offset), 0)

    $tip.addClass('in')

    // check to see if placing tip in new offset caused the tip to resize itself
    var actualWidth  = $tip[0].offsetWidth
    var actualHeight = $tip[0].offsetHeight

    if (placement == 'top' && actualHeight != height) {
      offset.top = offset.top + height - actualHeight
    }

    var delta = this.getViewportAdjustedDelta(placement, offset, actualWidth, actualHeight)

    if (delta.left) offset.left += delta.left
    else offset.top += delta.top

    var isVertical          = /top|bottom/.test(placement)
    var arrowDelta          = isVertical ? delta.left * 2 - width + actualWidth : delta.top * 2 - height + actualHeight
    var arrowOffsetPosition = isVertical ? 'offsetWidth' : 'offsetHeight'

    $tip.offset(offset)
    this.replaceArrow(arrowDelta, $tip[0][arrowOffsetPosition], isVertical)
  }

  Tooltip.prototype.replaceArrow = function (delta, dimension, isVertical) {
    this.arrow()
      .css(isVertical ? 'left' : 'top', 50 * (1 - delta / dimension) + '%')
      .css(isVertical ? 'top' : 'left', '')
  }

  Tooltip.prototype.setContent = function () {
    var $tip  = this.tip()
    var title = this.getTitle()

    $tip.find('.tooltip-inner')[this.options.html ? 'html' : 'text'](title)
    $tip.removeClass('fade in top bottom left right')
  }

  Tooltip.prototype.hide = function (callback) {
    var that = this
    var $tip = $(this.$tip)
    var e    = $.Event('hide.bs.' + this.type)

    function complete() {
      if (that.hoverState != 'in') $tip.detach()
      if (that.$element) { // TODO: Check whether guarding this code with this `if` is really necessary.
        that.$element
          .removeAttr('aria-describedby')
          .trigger('hidden.bs.' + that.type)
      }
      callback && callback()
    }

    this.$element.trigger(e)

    if (e.isDefaultPrevented()) return

    $tip.removeClass('in')

    $.support.transition && $tip.hasClass('fade') ?
      $tip
        .one('bsTransitionEnd', complete)
        .emulateTransitionEnd(Tooltip.TRANSITION_DURATION) :
      complete()

    this.hoverState = null

    return this
  }

  Tooltip.prototype.fixTitle = function () {
    var $e = this.$element
    if ($e.attr('title') || typeof $e.attr('data-original-title') != 'string') {
      $e.attr('data-original-title', $e.attr('title') || '').attr('title', '')
    }
  }

  Tooltip.prototype.hasContent = function () {
    return this.getTitle()
  }

  Tooltip.prototype.getPosition = function ($element) {
    $element   = $element || this.$element

    var el     = $element[0]
    var isBody = el.tagName == 'BODY'

    var elRect    = el.getBoundingClientRect()
    if (elRect.width == null) {
      // width and height are missing in IE8, so compute them manually; see https://github.com/twbs/bootstrap/issues/14093
      elRect = $.extend({}, elRect, { width: elRect.right - elRect.left, height: elRect.bottom - elRect.top })
    }
    var isSvg = window.SVGElement && el instanceof window.SVGElement
    // Avoid using $.offset() on SVGs since it gives incorrect results in jQuery 3.
    // See https://github.com/twbs/bootstrap/issues/20280
    var elOffset  = isBody ? { top: 0, left: 0 } : (isSvg ? null : $element.offset())
    var scroll    = { scroll: isBody ? document.documentElement.scrollTop || document.body.scrollTop : $element.scrollTop() }
    var outerDims = isBody ? { width: $(window).width(), height: $(window).height() } : null

    return $.extend({}, elRect, scroll, outerDims, elOffset)
  }

  Tooltip.prototype.getCalculatedOffset = function (placement, pos, actualWidth, actualHeight) {
    return placement == 'bottom' ? { top: pos.top + pos.height,   left: pos.left + pos.width / 2 - actualWidth / 2 } :
           placement == 'top'    ? { top: pos.top - actualHeight, left: pos.left + pos.width / 2 - actualWidth / 2 } :
           placement == 'left'   ? { top: pos.top + pos.height / 2 - actualHeight / 2, left: pos.left - actualWidth } :
        /* placement == 'right' */ { top: pos.top + pos.height / 2 - actualHeight / 2, left: pos.left + pos.width }

  }

  Tooltip.prototype.getViewportAdjustedDelta = function (placement, pos, actualWidth, actualHeight) {
    var delta = { top: 0, left: 0 }
    if (!this.$viewport) return delta

    var viewportPadding = this.options.viewport && this.options.viewport.padding || 0
    var viewportDimensions = this.getPosition(this.$viewport)

    if (/right|left/.test(placement)) {
      var topEdgeOffset    = pos.top - viewportPadding - viewportDimensions.scroll
      var bottomEdgeOffset = pos.top + viewportPadding - viewportDimensions.scroll + actualHeight
      if (topEdgeOffset < viewportDimensions.top) { // top overflow
        delta.top = viewportDimensions.top - topEdgeOffset
      } else if (bottomEdgeOffset > viewportDimensions.top + viewportDimensions.height) { // bottom overflow
        delta.top = viewportDimensions.top + viewportDimensions.height - bottomEdgeOffset
      }
    } else {
      var leftEdgeOffset  = pos.left - viewportPadding
      var rightEdgeOffset = pos.left + viewportPadding + actualWidth
      if (leftEdgeOffset < viewportDimensions.left) { // left overflow
        delta.left = viewportDimensions.left - leftEdgeOffset
      } else if (rightEdgeOffset > viewportDimensions.right) { // right overflow
        delta.left = viewportDimensions.left + viewportDimensions.width - rightEdgeOffset
      }
    }

    return delta
  }

  Tooltip.prototype.getTitle = function () {
    var title
    var $e = this.$element
    var o  = this.options

    title = $e.attr('data-original-title')
      || (typeof o.title == 'function' ? o.title.call($e[0]) :  o.title)

    return title
  }

  Tooltip.prototype.getUID = function (prefix) {
    do prefix += ~~(Math.random() * 1000000)
    while (document.getElementById(prefix))
    return prefix
  }

  Tooltip.prototype.tip = function () {
    if (!this.$tip) {
      this.$tip = $(this.options.template)
      if (this.$tip.length != 1) {
        throw new Error(this.type + ' `template` option must consist of exactly 1 top-level element!')
      }
    }
    return this.$tip
  }

  Tooltip.prototype.arrow = function () {
    return (this.$arrow = this.$arrow || this.tip().find('.tooltip-arrow'))
  }

  Tooltip.prototype.enable = function () {
    this.enabled = true
  }

  Tooltip.prototype.disable = function () {
    this.enabled = false
  }

  Tooltip.prototype.toggleEnabled = function () {
    this.enabled = !this.enabled
  }

  Tooltip.prototype.toggle = function (e) {
    var self = this
    if (e) {
      self = $(e.currentTarget).data('bs.' + this.type)
      if (!self) {
        self = new this.constructor(e.currentTarget, this.getDelegateOptions())
        $(e.currentTarget).data('bs.' + this.type, self)
      }
    }

    if (e) {
      self.inState.click = !self.inState.click
      if (self.isInStateTrue()) self.enter(self)
      else self.leave(self)
    } else {
      self.tip().hasClass('in') ? self.leave(self) : self.enter(self)
    }
  }

  Tooltip.prototype.destroy = function () {
    var that = this
    clearTimeout(this.timeout)
    this.hide(function () {
      that.$element.off('.' + that.type).removeData('bs.' + that.type)
      if (that.$tip) {
        that.$tip.detach()
      }
      that.$tip = null
      that.$arrow = null
      that.$viewport = null
      that.$element = null
    })
  }


  // TOOLTIP PLUGIN DEFINITION
  // =========================

  function Plugin(option) {
    return this.each(function () {
      var $this   = $(this)
      var data    = $this.data('bs.tooltip')
      var options = typeof option == 'object' && option

      if (!data && /destroy|hide/.test(option)) return
      if (!data) $this.data('bs.tooltip', (data = new Tooltip(this, options)))
      if (typeof option == 'string') data[option]()
    })
  }

  var old = $.fn.tooltip

  $.fn.tooltip             = Plugin
  $.fn.tooltip.Constructor = Tooltip


  // TOOLTIP NO CONFLICT
  // ===================

  $.fn.tooltip.noConflict = function () {
    $.fn.tooltip = old
    return this
  }

}(jQuery);

/* ========================================================================
 * Bootstrap: popover.js v3.3.7
 * http://getbootstrap.com/javascript/#popovers
 * ========================================================================
 * Copyright 2011-2016 Twitter, Inc.
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 * ======================================================================== */


+function ($) {
  'use strict';

  // POPOVER PUBLIC CLASS DEFINITION
  // ===============================

  var Popover = function (element, options) {
    this.init('popover', element, options)
  }

  if (!$.fn.tooltip) throw new Error('Popover requires tooltip.js')

  Popover.VERSION  = '3.3.7'

  Popover.DEFAULTS = $.extend({}, $.fn.tooltip.Constructor.DEFAULTS, {
    placement: 'right',
    trigger: 'click',
    content: '',
    template: '<div class="popover" role="tooltip"><div class="arrow"></div><h3 class="popover-title"></h3><div class="popover-content"></div></div>'
  })


  // NOTE: POPOVER EXTENDS tooltip.js
  // ================================

  Popover.prototype = $.extend({}, $.fn.tooltip.Constructor.prototype)

  Popover.prototype.constructor = Popover

  Popover.prototype.getDefaults = function () {
    return Popover.DEFAULTS
  }

  Popover.prototype.setContent = function () {
    var $tip    = this.tip()
    var title   = this.getTitle()
    var content = this.getContent()

    $tip.find('.popover-title')[this.options.html ? 'html' : 'text'](title)
    $tip.find('.popover-content').children().detach().end()[ // we use append for html objects to maintain js events
      this.options.html ? (typeof content == 'string' ? 'html' : 'append') : 'text'
    ](content)

    $tip.removeClass('fade top bottom left right in')

    // IE8 doesn't accept hiding via the `:empty` pseudo selector, we have to do
    // this manually by checking the contents.
    if (!$tip.find('.popover-title').html()) $tip.find('.popover-title').hide()
  }

  Popover.prototype.hasContent = function () {
    return this.getTitle() || this.getContent()
  }

  Popover.prototype.getContent = function () {
    var $e = this.$element
    var o  = this.options

    return $e.attr('data-content')
      || (typeof o.content == 'function' ?
            o.content.call($e[0]) :
            o.content)
  }

  Popover.prototype.arrow = function () {
    return (this.$arrow = this.$arrow || this.tip().find('.arrow'))
  }


  // POPOVER PLUGIN DEFINITION
  // =========================

  function Plugin(option) {
    return this.each(function () {
      var $this   = $(this)
      var data    = $this.data('bs.popover')
      var options = typeof option == 'object' && option

      if (!data && /destroy|hide/.test(option)) return
      if (!data) $this.data('bs.popover', (data = new Popover(this, options)))
      if (typeof option == 'string') data[option]()
    })
  }

  var old = $.fn.popover

  $.fn.popover             = Plugin
  $.fn.popover.Constructor = Popover


  // POPOVER NO CONFLICT
  // ===================

  $.fn.popover.noConflict = function () {
    $.fn.popover = old
    return this
  }

}(jQuery);

/* ========================================================================
 * Bootstrap: scrollspy.js v3.3.7
 * http://getbootstrap.com/javascript/#scrollspy
 * ========================================================================
 * Copyright 2011-2016 Twitter, Inc.
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 * ======================================================================== */


+function ($) {
  'use strict';

  // SCROLLSPY CLASS DEFINITION
  // ==========================

  function ScrollSpy(element, options) {
    this.$body          = $(document.body)
    this.$scrollElement = $(element).is(document.body) ? $(window) : $(element)
    this.options        = $.extend({}, ScrollSpy.DEFAULTS, options)
    this.selector       = (this.options.target || '') + ' .nav li > a'
    this.offsets        = []
    this.targets        = []
    this.activeTarget   = null
    this.scrollHeight   = 0

    this.$scrollElement.on('scroll.bs.scrollspy', $.proxy(this.process, this))
    this.refresh()
    this.process()
  }

  ScrollSpy.VERSION  = '3.3.7'

  ScrollSpy.DEFAULTS = {
    offset: 10
  }

  ScrollSpy.prototype.getScrollHeight = function () {
    return this.$scrollElement[0].scrollHeight || Math.max(this.$body[0].scrollHeight, document.documentElement.scrollHeight)
  }

  ScrollSpy.prototype.refresh = function () {
    var that          = this
    var offsetMethod  = 'offset'
    var offsetBase    = 0

    this.offsets      = []
    this.targets      = []
    this.scrollHeight = this.getScrollHeight()

    if (!$.isWindow(this.$scrollElement[0])) {
      offsetMethod = 'position'
      offsetBase   = this.$scrollElement.scrollTop()
    }

    this.$body
      .find(this.selector)
      .map(function () {
        var $el   = $(this)
        var href  = $el.data('target') || $el.attr('href')
        var $href = /^#./.test(href) && $(href)

        return ($href
          && $href.length
          && $href.is(':visible')
          && [[$href[offsetMethod]().top + offsetBase, href]]) || null
      })
      .sort(function (a, b) { return a[0] - b[0] })
      .each(function () {
        that.offsets.push(this[0])
        that.targets.push(this[1])
      })
  }

  ScrollSpy.prototype.process = function () {
    var scrollTop    = this.$scrollElement.scrollTop() + this.options.offset
    var scrollHeight = this.getScrollHeight()
    var maxScroll    = this.options.offset + scrollHeight - this.$scrollElement.height()
    var offsets      = this.offsets
    var targets      = this.targets
    var activeTarget = this.activeTarget
    var i

    if (this.scrollHeight != scrollHeight) {
      this.refresh()
    }

    if (scrollTop >= maxScroll) {
      return activeTarget != (i = targets[targets.length - 1]) && this.activate(i)
    }

    if (activeTarget && scrollTop < offsets[0]) {
      this.activeTarget = null
      return this.clear()
    }

    for (i = offsets.length; i--;) {
      activeTarget != targets[i]
        && scrollTop >= offsets[i]
        && (offsets[i + 1] === undefined || scrollTop < offsets[i + 1])
        && this.activate(targets[i])
    }
  }

  ScrollSpy.prototype.activate = function (target) {
    this.activeTarget = target

    this.clear()

    var selector = this.selector +
      '[data-target="' + target + '"],' +
      this.selector + '[href="' + target + '"]'

    var active = $(selector)
      .parents('li')
      .addClass('active')

    if (active.parent('.dropdown-menu').length) {
      active = active
        .closest('li.dropdown')
        .addClass('active')
    }

    active.trigger('activate.bs.scrollspy')
  }

  ScrollSpy.prototype.clear = function () {
    $(this.selector)
      .parentsUntil(this.options.target, '.active')
      .removeClass('active')
  }


  // SCROLLSPY PLUGIN DEFINITION
  // ===========================

  function Plugin(option) {
    return this.each(function () {
      var $this   = $(this)
      var data    = $this.data('bs.scrollspy')
      var options = typeof option == 'object' && option

      if (!data) $this.data('bs.scrollspy', (data = new ScrollSpy(this, options)))
      if (typeof option == 'string') data[option]()
    })
  }

  var old = $.fn.scrollspy

  $.fn.scrollspy             = Plugin
  $.fn.scrollspy.Constructor = ScrollSpy


  // SCROLLSPY NO CONFLICT
  // =====================

  $.fn.scrollspy.noConflict = function () {
    $.fn.scrollspy = old
    return this
  }


  // SCROLLSPY DATA-API
  // ==================

  $(window).on('load.bs.scrollspy.data-api', function () {
    $('[data-spy="scroll"]').each(function () {
      var $spy = $(this)
      Plugin.call($spy, $spy.data())
    })
  })

}(jQuery);

/* ========================================================================
 * Bootstrap: tab.js v3.3.7
 * http://getbootstrap.com/javascript/#tabs
 * ========================================================================
 * Copyright 2011-2016 Twitter, Inc.
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 * ======================================================================== */


+function ($) {
  'use strict';

  // TAB CLASS DEFINITION
  // ====================

  var Tab = function (element) {
    // jscs:disable requireDollarBeforejQueryAssignment
    this.element = $(element)
    // jscs:enable requireDollarBeforejQueryAssignment
  }

  Tab.VERSION = '3.3.7'

  Tab.TRANSITION_DURATION = 150

  Tab.prototype.show = function () {
    var $this    = this.element
    var $ul      = $this.closest('ul:not(.dropdown-menu)')
    var selector = $this.data('target')

    if (!selector) {
      selector = $this.attr('href')
      selector = selector && selector.replace(/.*(?=#[^\s]*$)/, '') // strip for ie7
    }

    if ($this.parent('li').hasClass('active')) return

    var $previous = $ul.find('.active:last a')
    var hideEvent = $.Event('hide.bs.tab', {
      relatedTarget: $this[0]
    })
    var showEvent = $.Event('show.bs.tab', {
      relatedTarget: $previous[0]
    })

    $previous.trigger(hideEvent)
    $this.trigger(showEvent)

    if (showEvent.isDefaultPrevented() || hideEvent.isDefaultPrevented()) return

    var $target = $(selector)

    this.activate($this.closest('li'), $ul)
    this.activate($target, $target.parent(), function () {
      $previous.trigger({
        type: 'hidden.bs.tab',
        relatedTarget: $this[0]
      })
      $this.trigger({
        type: 'shown.bs.tab',
        relatedTarget: $previous[0]
      })
    })
  }

  Tab.prototype.activate = function (element, container, callback) {
    var $active    = container.find('> .active')
    var transition = callback
      && $.support.transition
      && ($active.length && $active.hasClass('fade') || !!container.find('> .fade').length)

    function next() {
      $active
        .removeClass('active')
        .find('> .dropdown-menu > .active')
          .removeClass('active')
        .end()
        .find('[data-toggle="tab"]')
          .attr('aria-expanded', false)

      element
        .addClass('active')
        .find('[data-toggle="tab"]')
          .attr('aria-expanded', true)

      if (transition) {
        element[0].offsetWidth // reflow for transition
        element.addClass('in')
      } else {
        element.removeClass('fade')
      }

      if (element.parent('.dropdown-menu').length) {
        element
          .closest('li.dropdown')
            .addClass('active')
          .end()
          .find('[data-toggle="tab"]')
            .attr('aria-expanded', true)
      }

      callback && callback()
    }

    $active.length && transition ?
      $active
        .one('bsTransitionEnd', next)
        .emulateTransitionEnd(Tab.TRANSITION_DURATION) :
      next()

    $active.removeClass('in')
  }


  // TAB PLUGIN DEFINITION
  // =====================

  function Plugin(option) {
    return this.each(function () {
      var $this = $(this)
      var data  = $this.data('bs.tab')

      if (!data) $this.data('bs.tab', (data = new Tab(this)))
      if (typeof option == 'string') data[option]()
    })
  }

  var old = $.fn.tab

  $.fn.tab             = Plugin
  $.fn.tab.Constructor = Tab


  // TAB NO CONFLICT
  // ===============

  $.fn.tab.noConflict = function () {
    $.fn.tab = old
    return this
  }


  // TAB DATA-API
  // ============

  var clickHandler = function (e) {
    e.preventDefault()
    Plugin.call($(this), 'show')
  }

  $(document)
    .on('click.bs.tab.data-api', '[data-toggle="tab"]', clickHandler)
    .on('click.bs.tab.data-api', '[data-toggle="pill"]', clickHandler)

}(jQuery);

/* ========================================================================
 * Bootstrap: affix.js v3.3.7
 * http://getbootstrap.com/javascript/#affix
 * ========================================================================
 * Copyright 2011-2016 Twitter, Inc.
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 * ======================================================================== */


+function ($) {
  'use strict';

  // AFFIX CLASS DEFINITION
  // ======================

  var Affix = function (element, options) {
    this.options = $.extend({}, Affix.DEFAULTS, options)

    this.$target = $(this.options.target)
      .on('scroll.bs.affix.data-api', $.proxy(this.checkPosition, this))
      .on('click.bs.affix.data-api',  $.proxy(this.checkPositionWithEventLoop, this))

    this.$element     = $(element)
    this.affixed      = null
    this.unpin        = null
    this.pinnedOffset = null

    this.checkPosition()
  }

  Affix.VERSION  = '3.3.7'

  Affix.RESET    = 'affix affix-top affix-bottom'

  Affix.DEFAULTS = {
    offset: 0,
    target: window
  }

  Affix.prototype.getState = function (scrollHeight, height, offsetTop, offsetBottom) {
    var scrollTop    = this.$target.scrollTop()
    var position     = this.$element.offset()
    var targetHeight = this.$target.height()

    if (offsetTop != null && this.affixed == 'top') return scrollTop < offsetTop ? 'top' : false

    if (this.affixed == 'bottom') {
      if (offsetTop != null) return (scrollTop + this.unpin <= position.top) ? false : 'bottom'
      return (scrollTop + targetHeight <= scrollHeight - offsetBottom) ? false : 'bottom'
    }

    var initializing   = this.affixed == null
    var colliderTop    = initializing ? scrollTop : position.top
    var colliderHeight = initializing ? targetHeight : height

    if (offsetTop != null && scrollTop <= offsetTop) return 'top'
    if (offsetBottom != null && (colliderTop + colliderHeight >= scrollHeight - offsetBottom)) return 'bottom'

    return false
  }

  Affix.prototype.getPinnedOffset = function () {
    if (this.pinnedOffset) return this.pinnedOffset
    this.$element.removeClass(Affix.RESET).addClass('affix')
    var scrollTop = this.$target.scrollTop()
    var position  = this.$element.offset()
    return (this.pinnedOffset = position.top - scrollTop)
  }

  Affix.prototype.checkPositionWithEventLoop = function () {
    setTimeout($.proxy(this.checkPosition, this), 1)
  }

  Affix.prototype.checkPosition = function () {
    if (!this.$element.is(':visible')) return

    var height       = this.$element.height()
    var offset       = this.options.offset
    var offsetTop    = offset.top
    var offsetBottom = offset.bottom
    var scrollHeight = Math.max($(document).height(), $(document.body).height())

    if (typeof offset != 'object')         offsetBottom = offsetTop = offset
    if (typeof offsetTop == 'function')    offsetTop    = offset.top(this.$element)
    if (typeof offsetBottom == 'function') offsetBottom = offset.bottom(this.$element)

    var affix = this.getState(scrollHeight, height, offsetTop, offsetBottom)

    if (this.affixed != affix) {
      if (this.unpin != null) this.$element.css('top', '')

      var affixType = 'affix' + (affix ? '-' + affix : '')
      var e         = $.Event(affixType + '.bs.affix')

      this.$element.trigger(e)

      if (e.isDefaultPrevented()) return

      this.affixed = affix
      this.unpin = affix == 'bottom' ? this.getPinnedOffset() : null

      this.$element
        .removeClass(Affix.RESET)
        .addClass(affixType)
        .trigger(affixType.replace('affix', 'affixed') + '.bs.affix')
    }

    if (affix == 'bottom') {
      this.$element.offset({
        top: scrollHeight - height - offsetBottom
      })
    }
  }


  // AFFIX PLUGIN DEFINITION
  // =======================

  function Plugin(option) {
    return this.each(function () {
      var $this   = $(this)
      var data    = $this.data('bs.affix')
      var options = typeof option == 'object' && option

      if (!data) $this.data('bs.affix', (data = new Affix(this, options)))
      if (typeof option == 'string') data[option]()
    })
  }

  var old = $.fn.affix

  $.fn.affix             = Plugin
  $.fn.affix.Constructor = Affix


  // AFFIX NO CONFLICT
  // =================

  $.fn.affix.noConflict = function () {
    $.fn.affix = old
    return this
  }


  // AFFIX DATA-API
  // ==============

  $(window).on('load', function () {
    $('[data-spy="affix"]').each(function () {
      var $spy = $(this)
      var data = $spy.data()

      data.offset = data.offset || {}

      if (data.offsetBottom != null) data.offset.bottom = data.offsetBottom
      if (data.offsetTop    != null) data.offset.top    = data.offsetTop

      Plugin.call($spy, data)
    })
  })

}(jQuery);

/*!
 * Chart.js
 * http://chartjs.org/
 * Version: 1.0.2
 *
 * Copyright 2015 Nick Downie
 * Released under the MIT license
 * https://github.com/nnnick/Chart.js/blob/master/LICENSE.md
 */


(function(){

	"use strict";

	//Declare root variable - window in the browser, global on the server
	var root = this,
		previous = root.Chart;

	//Occupy the global variable of Chart, and create a simple base class
	var Chart = function(context){
		var chart = this;
		this.canvas = context.canvas;

		this.ctx = context;

		//Variables global to the chart
		var computeDimension = function(element,dimension)
		{
			if (element['offset'+dimension])
			{
				return element['offset'+dimension];
			}
			else
			{
				return document.defaultView.getComputedStyle(element).getPropertyValue(dimension);
			}
		}

		var width = this.width = computeDimension(context.canvas,'Width');
		var height = this.height = computeDimension(context.canvas,'Height');

		// Firefox requires this to work correctly
		context.canvas.width  = width;
		context.canvas.height = height;

		var width = this.width = context.canvas.width;
		var height = this.height = context.canvas.height;
		this.aspectRatio = this.width / this.height;
		//High pixel density displays - multiply the size of the canvas height/width by the device pixel ratio, then scale.
		helpers.retinaScale(this);

		return this;
	};
	//Globally expose the defaults to allow for user updating/changing
	Chart.defaults = {
		global: {
			// Boolean - Whether to animate the chart
			animation: true,

			// Number - Number of animation steps
			animationSteps: 60,

			// String - Animation easing effect
			animationEasing: "easeOutQuart",

			// Boolean - If we should show the scale at all
			showScale: true,

			// Boolean - If we want to override with a hard coded scale
			scaleOverride: false,

			// ** Required if scaleOverride is true **
			// Number - The number of steps in a hard coded scale
			scaleSteps: null,
			// Number - The value jump in the hard coded scale
			scaleStepWidth: null,
			// Number - The scale starting value
			scaleStartValue: null,

			// String - Colour of the scale line
			scaleLineColor: "rgba(0,0,0,.1)",

			// Number - Pixel width of the scale line
			scaleLineWidth: 1,

			// Boolean - Whether to show labels on the scale
			scaleShowLabels: true,

			// Interpolated JS string - can access value
			scaleLabel: "<%=value%>",

			// Boolean - Whether the scale should stick to integers, and not show any floats even if drawing space is there
			scaleIntegersOnly: true,

			// Boolean - Whether the scale should start at zero, or an order of magnitude down from the lowest value
			scaleBeginAtZero: false,

			// String - Scale label font declaration for the scale label
			scaleFontFamily: "'Helvetica Neue', 'Helvetica', 'Arial', sans-serif",

			// Number - Scale label font size in pixels
			scaleFontSize: 12,

			// String - Scale label font weight style
			scaleFontStyle: "normal",

			// String - Scale label font colour
			scaleFontColor: "#666",

			// Boolean - whether or not the chart should be responsive and resize when the browser does.
			responsive: false,

			// Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
			maintainAspectRatio: true,

			// Boolean - Determines whether to draw tooltips on the canvas or not - attaches events to touchmove & mousemove
			showTooltips: true,

			// Boolean - Determines whether to draw built-in tooltip or call custom tooltip function
			customTooltips: false,

			// Array - Array of string names to attach tooltip events
			tooltipEvents: ["mousemove", "touchstart", "touchmove", "mouseout"],

			// String - Tooltip background colour
			tooltipFillColor: "rgba(0,0,0,0.8)",

			// String - Tooltip label font declaration for the scale label
			tooltipFontFamily: "'Helvetica Neue', 'Helvetica', 'Arial', sans-serif",

			// Number - Tooltip label font size in pixels
			tooltipFontSize: 14,

			// String - Tooltip font weight style
			tooltipFontStyle: "normal",

			// String - Tooltip label font colour
			tooltipFontColor: "#fff",

			// String - Tooltip title font declaration for the scale label
			tooltipTitleFontFamily: "'Helvetica Neue', 'Helvetica', 'Arial', sans-serif",

			// Number - Tooltip title font size in pixels
			tooltipTitleFontSize: 14,

			// String - Tooltip title font weight style
			tooltipTitleFontStyle: "bold",

			// String - Tooltip title font colour
			tooltipTitleFontColor: "#fff",

			// Number - pixel width of padding around tooltip text
			tooltipYPadding: 6,

			// Number - pixel width of padding around tooltip text
			tooltipXPadding: 6,

			// Number - Size of the caret on the tooltip
			tooltipCaretSize: 8,

			// Number - Pixel radius of the tooltip border
			tooltipCornerRadius: 6,

			// Number - Pixel offset from point x to tooltip edge
			tooltipXOffset: 10,

			// String - Template string for single tooltips
			tooltipTemplate: "<%if (label){%><%=label%>: <%}%><%= value %>",

			// String - Template string for single tooltips
			multiTooltipTemplate: "<%= value %>",

			// String - Colour behind the legend colour block
			multiTooltipKeyBackground: '#fff',

			// Function - Will fire on animation progression.
			onAnimationProgress: function(){},

			// Function - Will fire on animation completion.
			onAnimationComplete: function(){}

		}
	};

	//Create a dictionary of chart types, to allow for extension of existing types
	Chart.types = {};

	//Global Chart helpers object for utility methods and classes
	var helpers = Chart.helpers = {};

		//-- Basic js utility methods
	var each = helpers.each = function(loopable,callback,self){
			var additionalArgs = Array.prototype.slice.call(arguments, 3);
			// Check to see if null or undefined firstly.
			if (loopable){
				if (loopable.length === +loopable.length){
					var i;
					for (i=0; i<loopable.length; i++){
						callback.apply(self,[loopable[i], i].concat(additionalArgs));
					}
				}
				else{
					for (var item in loopable){
						callback.apply(self,[loopable[item],item].concat(additionalArgs));
					}
				}
			}
		},
		clone = helpers.clone = function(obj){
			var objClone = {};
			each(obj,function(value,key){
				if (obj.hasOwnProperty(key)) objClone[key] = value;
			});
			return objClone;
		},
		extend = helpers.extend = function(base){
			each(Array.prototype.slice.call(arguments,1), function(extensionObject) {
				each(extensionObject,function(value,key){
					if (extensionObject.hasOwnProperty(key)) base[key] = value;
				});
			});
			return base;
		},
		merge = helpers.merge = function(base,master){
			//Merge properties in left object over to a shallow clone of object right.
			var args = Array.prototype.slice.call(arguments,0);
			args.unshift({});
			return extend.apply(null, args);
		},
		indexOf = helpers.indexOf = function(arrayToSearch, item){
			if (Array.prototype.indexOf) {
				return arrayToSearch.indexOf(item);
			}
			else{
				for (var i = 0; i < arrayToSearch.length; i++) {
					if (arrayToSearch[i] === item) return i;
				}
				return -1;
			}
		},
		where = helpers.where = function(collection, filterCallback){
			var filtered = [];

			helpers.each(collection, function(item){
				if (filterCallback(item)){
					filtered.push(item);
				}
			});

			return filtered;
		},
		findNextWhere = helpers.findNextWhere = function(arrayToSearch, filterCallback, startIndex){
			// Default to start of the array
			if (!startIndex){
				startIndex = -1;
			}
			for (var i = startIndex + 1; i < arrayToSearch.length; i++) {
				var currentItem = arrayToSearch[i];
				if (filterCallback(currentItem)){
					return currentItem;
				}
			}
		},
		findPreviousWhere = helpers.findPreviousWhere = function(arrayToSearch, filterCallback, startIndex){
			// Default to end of the array
			if (!startIndex){
				startIndex = arrayToSearch.length;
			}
			for (var i = startIndex - 1; i >= 0; i--) {
				var currentItem = arrayToSearch[i];
				if (filterCallback(currentItem)){
					return currentItem;
				}
			}
		},
		inherits = helpers.inherits = function(extensions){
			//Basic javascript inheritance based on the model created in Backbone.js
			var parent = this;
			var ChartElement = (extensions && extensions.hasOwnProperty("constructor")) ? extensions.constructor : function(){ return parent.apply(this, arguments); };

			var Surrogate = function(){ this.constructor = ChartElement;};
			Surrogate.prototype = parent.prototype;
			ChartElement.prototype = new Surrogate();

			ChartElement.extend = inherits;

			if (extensions) extend(ChartElement.prototype, extensions);

			ChartElement.__super__ = parent.prototype;

			return ChartElement;
		},
		noop = helpers.noop = function(){},
		uid = helpers.uid = (function(){
			var id=0;
			return function(){
				return "chart-" + id++;
			};
		})(),
		warn = helpers.warn = function(str){
			//Method for warning of errors
			if (window.console && typeof window.console.warn == "function") console.warn(str);
		},
		amd = helpers.amd = (typeof define == 'function' && define.amd),
		//-- Math methods
		isNumber = helpers.isNumber = function(n){
			return !isNaN(parseFloat(n)) && isFinite(n);
		},
		max = helpers.max = function(array){
			return Math.max.apply( Math, array );
		},
		min = helpers.min = function(array){
			return Math.min.apply( Math, array );
		},
		cap = helpers.cap = function(valueToCap,maxValue,minValue){
			if(isNumber(maxValue)) {
				if( valueToCap > maxValue ) {
					return maxValue;
				}
			}
			else if(isNumber(minValue)){
				if ( valueToCap < minValue ){
					return minValue;
				}
			}
			return valueToCap;
		},
		getDecimalPlaces = helpers.getDecimalPlaces = function(num){
			if (num%1!==0 && isNumber(num)){
				return num.toString().split(".")[1].length;
			}
			else {
				return 0;
			}
		},
		toRadians = helpers.radians = function(degrees){
			return degrees * (Math.PI/180);
		},
		// Gets the angle from vertical upright to the point about a centre.
		getAngleFromPoint = helpers.getAngleFromPoint = function(centrePoint, anglePoint){
			var distanceFromXCenter = anglePoint.x - centrePoint.x,
				distanceFromYCenter = anglePoint.y - centrePoint.y,
				radialDistanceFromCenter = Math.sqrt( distanceFromXCenter * distanceFromXCenter + distanceFromYCenter * distanceFromYCenter);


			var angle = Math.PI * 2 + Math.atan2(distanceFromYCenter, distanceFromXCenter);

			//If the segment is in the top left quadrant, we need to add another rotation to the angle
			if (distanceFromXCenter < 0 && distanceFromYCenter < 0){
				angle += Math.PI*2;
			}

			return {
				angle: angle,
				distance: radialDistanceFromCenter
			};
		},
		aliasPixel = helpers.aliasPixel = function(pixelWidth){
			return (pixelWidth % 2 === 0) ? 0 : 0.5;
		},
		splineCurve = helpers.splineCurve = function(FirstPoint,MiddlePoint,AfterPoint,t){
			//Props to Rob Spencer at scaled innovation for his post on splining between points
			//http://scaledinnovation.com/analytics/splines/aboutSplines.html
			var d01=Math.sqrt(Math.pow(MiddlePoint.x-FirstPoint.x,2)+Math.pow(MiddlePoint.y-FirstPoint.y,2)),
				d12=Math.sqrt(Math.pow(AfterPoint.x-MiddlePoint.x,2)+Math.pow(AfterPoint.y-MiddlePoint.y,2)),
				fa=t*d01/(d01+d12),// scaling factor for triangle Ta
				fb=t*d12/(d01+d12);
			return {
				inner : {
					x : MiddlePoint.x-fa*(AfterPoint.x-FirstPoint.x),
					y : MiddlePoint.y-fa*(AfterPoint.y-FirstPoint.y)
				},
				outer : {
					x: MiddlePoint.x+fb*(AfterPoint.x-FirstPoint.x),
					y : MiddlePoint.y+fb*(AfterPoint.y-FirstPoint.y)
				}
			};
		},
		calculateOrderOfMagnitude = helpers.calculateOrderOfMagnitude = function(val){
			return Math.floor(Math.log(val) / Math.LN10);
		},
		calculateScaleRange = helpers.calculateScaleRange = function(valuesArray, drawingSize, textSize, startFromZero, integersOnly){

			//Set a minimum step of two - a point at the top of the graph, and a point at the base
			var minSteps = 2,
				maxSteps = Math.floor(drawingSize/(textSize * 1.5)),
				skipFitting = (minSteps >= maxSteps);

			var maxValue = max(valuesArray),
				minValue = min(valuesArray);

			// We need some degree of seperation here to calculate the scales if all the values are the same
			// Adding/minusing 0.5 will give us a range of 1.
			if (maxValue === minValue){
				maxValue += 0.5;
				// So we don't end up with a graph with a negative start value if we've said always start from zero
				if (minValue >= 0.5 && !startFromZero){
					minValue -= 0.5;
				}
				else{
					// Make up a whole number above the values
					maxValue += 0.5;
				}
			}

			var	valueRange = Math.abs(maxValue - minValue),
				rangeOrderOfMagnitude = calculateOrderOfMagnitude(valueRange),
				graphMax = Math.ceil(maxValue / (1 * Math.pow(10, rangeOrderOfMagnitude))) * Math.pow(10, rangeOrderOfMagnitude),
				graphMin = (startFromZero) ? 0 : Math.floor(minValue / (1 * Math.pow(10, rangeOrderOfMagnitude))) * Math.pow(10, rangeOrderOfMagnitude),
				graphRange = graphMax - graphMin,
				stepValue = Math.pow(10, rangeOrderOfMagnitude),
				numberOfSteps = Math.round(graphRange / stepValue);

			//If we have more space on the graph we'll use it to give more definition to the data
			while((numberOfSteps > maxSteps || (numberOfSteps * 2) < maxSteps) && !skipFitting) {
				if(numberOfSteps > maxSteps){
					stepValue *=2;
					numberOfSteps = Math.round(graphRange/stepValue);
					// Don't ever deal with a decimal number of steps - cancel fitting and just use the minimum number of steps.
					if (numberOfSteps % 1 !== 0){
						skipFitting = true;
					}
				}
				//We can fit in double the amount of scale points on the scale
				else{
					//If user has declared ints only, and the step value isn't a decimal
					if (integersOnly && rangeOrderOfMagnitude >= 0){
						//If the user has said integers only, we need to check that making the scale more granular wouldn't make it a float
						if(stepValue/2 % 1 === 0){
							stepValue /=2;
							numberOfSteps = Math.round(graphRange/stepValue);
						}
						//If it would make it a float break out of the loop
						else{
							break;
						}
					}
					//If the scale doesn't have to be an int, make the scale more granular anyway.
					else{
						stepValue /=2;
						numberOfSteps = Math.round(graphRange/stepValue);
					}

				}
			}

			if (skipFitting){
				numberOfSteps = minSteps;
				stepValue = graphRange / numberOfSteps;
			}

			return {
				steps : numberOfSteps,
				stepValue : stepValue,
				min : graphMin,
				max	: graphMin + (numberOfSteps * stepValue)
			};

		},
		/* jshint ignore:start */
		// Blows up jshint errors based on the new Function constructor
		//Templating methods
		//Javascript micro templating by John Resig - source at http://ejohn.org/blog/javascript-micro-templating/
		template = helpers.template = function(templateString, valuesObject){

			// If templateString is function rather than string-template - call the function for valuesObject

			if(templateString instanceof Function){
			 	return templateString(valuesObject);
		 	}

			var cache = {};
			function tmpl(str, data){
				// Figure out if we're getting a template, or if we need to
				// load the template - and be sure to cache the result.
				var fn = !/\W/.test(str) ?
				cache[str] = cache[str] :

				// Generate a reusable function that will serve as a template
				// generator (and which will be cached).
				new Function("obj",
					"var p=[],print=function(){p.push.apply(p,arguments);};" +

					// Introduce the data as local variables using with(){}
					"with(obj){p.push('" +

					// Convert the template into pure JavaScript
					str
						.replace(/[\r\t\n]/g, " ")
						.split("<%").join("\t")
						.replace(/((^|%>)[^\t]*)'/g, "$1\r")
						.replace(/\t=(.*?)%>/g, "',$1,'")
						.split("\t").join("');")
						.split("%>").join("p.push('")
						.split("\r").join("\\'") +
					"');}return p.join('');"
				);

				// Provide some basic currying to the user
				return data ? fn( data ) : fn;
			}
			return tmpl(templateString,valuesObject);
		},
		/* jshint ignore:end */
		generateLabels = helpers.generateLabels = function(templateString,numberOfSteps,graphMin,stepValue){
			var labelsArray = new Array(numberOfSteps);
			if (labelTemplateString){
				each(labelsArray,function(val,index){
					labelsArray[index] = template(templateString,{value: (graphMin + (stepValue*(index+1)))});
				});
			}
			return labelsArray;
		},
		//--Animation methods
		//Easing functions adapted from Robert Penner's easing equations
		//http://www.robertpenner.com/easing/
		easingEffects = helpers.easingEffects = {
			linear: function (t) {
				return t;
			},
			easeInQuad: function (t) {
				return t * t;
			},
			easeOutQuad: function (t) {
				return -1 * t * (t - 2);
			},
			easeInOutQuad: function (t) {
				if ((t /= 1 / 2) < 1) return 1 / 2 * t * t;
				return -1 / 2 * ((--t) * (t - 2) - 1);
			},
			easeInCubic: function (t) {
				return t * t * t;
			},
			easeOutCubic: function (t) {
				return 1 * ((t = t / 1 - 1) * t * t + 1);
			},
			easeInOutCubic: function (t) {
				if ((t /= 1 / 2) < 1) return 1 / 2 * t * t * t;
				return 1 / 2 * ((t -= 2) * t * t + 2);
			},
			easeInQuart: function (t) {
				return t * t * t * t;
			},
			easeOutQuart: function (t) {
				return -1 * ((t = t / 1 - 1) * t * t * t - 1);
			},
			easeInOutQuart: function (t) {
				if ((t /= 1 / 2) < 1) return 1 / 2 * t * t * t * t;
				return -1 / 2 * ((t -= 2) * t * t * t - 2);
			},
			easeInQuint: function (t) {
				return 1 * (t /= 1) * t * t * t * t;
			},
			easeOutQuint: function (t) {
				return 1 * ((t = t / 1 - 1) * t * t * t * t + 1);
			},
			easeInOutQuint: function (t) {
				if ((t /= 1 / 2) < 1) return 1 / 2 * t * t * t * t * t;
				return 1 / 2 * ((t -= 2) * t * t * t * t + 2);
			},
			easeInSine: function (t) {
				return -1 * Math.cos(t / 1 * (Math.PI / 2)) + 1;
			},
			easeOutSine: function (t) {
				return 1 * Math.sin(t / 1 * (Math.PI / 2));
			},
			easeInOutSine: function (t) {
				return -1 / 2 * (Math.cos(Math.PI * t / 1) - 1);
			},
			easeInExpo: function (t) {
				return (t === 0) ? 1 : 1 * Math.pow(2, 10 * (t / 1 - 1));
			},
			easeOutExpo: function (t) {
				return (t === 1) ? 1 : 1 * (-Math.pow(2, -10 * t / 1) + 1);
			},
			easeInOutExpo: function (t) {
				if (t === 0) return 0;
				if (t === 1) return 1;
				if ((t /= 1 / 2) < 1) return 1 / 2 * Math.pow(2, 10 * (t - 1));
				return 1 / 2 * (-Math.pow(2, -10 * --t) + 2);
			},
			easeInCirc: function (t) {
				if (t >= 1) return t;
				return -1 * (Math.sqrt(1 - (t /= 1) * t) - 1);
			},
			easeOutCirc: function (t) {
				return 1 * Math.sqrt(1 - (t = t / 1 - 1) * t);
			},
			easeInOutCirc: function (t) {
				if ((t /= 1 / 2) < 1) return -1 / 2 * (Math.sqrt(1 - t * t) - 1);
				return 1 / 2 * (Math.sqrt(1 - (t -= 2) * t) + 1);
			},
			easeInElastic: function (t) {
				var s = 1.70158;
				var p = 0;
				var a = 1;
				if (t === 0) return 0;
				if ((t /= 1) == 1) return 1;
				if (!p) p = 1 * 0.3;
				if (a < Math.abs(1)) {
					a = 1;
					s = p / 4;
				} else s = p / (2 * Math.PI) * Math.asin(1 / a);
				return -(a * Math.pow(2, 10 * (t -= 1)) * Math.sin((t * 1 - s) * (2 * Math.PI) / p));
			},
			easeOutElastic: function (t) {
				var s = 1.70158;
				var p = 0;
				var a = 1;
				if (t === 0) return 0;
				if ((t /= 1) == 1) return 1;
				if (!p) p = 1 * 0.3;
				if (a < Math.abs(1)) {
					a = 1;
					s = p / 4;
				} else s = p / (2 * Math.PI) * Math.asin(1 / a);
				return a * Math.pow(2, -10 * t) * Math.sin((t * 1 - s) * (2 * Math.PI) / p) + 1;
			},
			easeInOutElastic: function (t) {
				var s = 1.70158;
				var p = 0;
				var a = 1;
				if (t === 0) return 0;
				if ((t /= 1 / 2) == 2) return 1;
				if (!p) p = 1 * (0.3 * 1.5);
				if (a < Math.abs(1)) {
					a = 1;
					s = p / 4;
				} else s = p / (2 * Math.PI) * Math.asin(1 / a);
				if (t < 1) return -0.5 * (a * Math.pow(2, 10 * (t -= 1)) * Math.sin((t * 1 - s) * (2 * Math.PI) / p));
				return a * Math.pow(2, -10 * (t -= 1)) * Math.sin((t * 1 - s) * (2 * Math.PI) / p) * 0.5 + 1;
			},
			easeInBack: function (t) {
				var s = 1.70158;
				return 1 * (t /= 1) * t * ((s + 1) * t - s);
			},
			easeOutBack: function (t) {
				var s = 1.70158;
				return 1 * ((t = t / 1 - 1) * t * ((s + 1) * t + s) + 1);
			},
			easeInOutBack: function (t) {
				var s = 1.70158;
				if ((t /= 1 / 2) < 1) return 1 / 2 * (t * t * (((s *= (1.525)) + 1) * t - s));
				return 1 / 2 * ((t -= 2) * t * (((s *= (1.525)) + 1) * t + s) + 2);
			},
			easeInBounce: function (t) {
				return 1 - easingEffects.easeOutBounce(1 - t);
			},
			easeOutBounce: function (t) {
				if ((t /= 1) < (1 / 2.75)) {
					return 1 * (7.5625 * t * t);
				} else if (t < (2 / 2.75)) {
					return 1 * (7.5625 * (t -= (1.5 / 2.75)) * t + 0.75);
				} else if (t < (2.5 / 2.75)) {
					return 1 * (7.5625 * (t -= (2.25 / 2.75)) * t + 0.9375);
				} else {
					return 1 * (7.5625 * (t -= (2.625 / 2.75)) * t + 0.984375);
				}
			},
			easeInOutBounce: function (t) {
				if (t < 1 / 2) return easingEffects.easeInBounce(t * 2) * 0.5;
				return easingEffects.easeOutBounce(t * 2 - 1) * 0.5 + 1 * 0.5;
			}
		},
		//Request animation polyfill - http://www.paulirish.com/2011/requestanimationframe-for-smart-animating/
		requestAnimFrame = helpers.requestAnimFrame = (function(){
			return window.requestAnimationFrame ||
				window.webkitRequestAnimationFrame ||
				window.mozRequestAnimationFrame ||
				window.oRequestAnimationFrame ||
				window.msRequestAnimationFrame ||
				function(callback) {
					return window.setTimeout(callback, 1000 / 60);
				};
		})(),
		cancelAnimFrame = helpers.cancelAnimFrame = (function(){
			return window.cancelAnimationFrame ||
				window.webkitCancelAnimationFrame ||
				window.mozCancelAnimationFrame ||
				window.oCancelAnimationFrame ||
				window.msCancelAnimationFrame ||
				function(callback) {
					return window.clearTimeout(callback, 1000 / 60);
				};
		})(),
		animationLoop = helpers.animationLoop = function(callback,totalSteps,easingString,onProgress,onComplete,chartInstance){

			var currentStep = 0,
				easingFunction = easingEffects[easingString] || easingEffects.linear;

			var animationFrame = function(){
				currentStep++;
				var stepDecimal = currentStep/totalSteps;
				var easeDecimal = easingFunction(stepDecimal);

				callback.call(chartInstance,easeDecimal,stepDecimal, currentStep);
				onProgress.call(chartInstance,easeDecimal,stepDecimal);
				if (currentStep < totalSteps){
					chartInstance.animationFrame = requestAnimFrame(animationFrame);
				} else{
					onComplete.apply(chartInstance);
				}
			};
			requestAnimFrame(animationFrame);
		},
		//-- DOM methods
		getRelativePosition = helpers.getRelativePosition = function(evt){
			var mouseX, mouseY;
			var e = evt.originalEvent || evt,
				canvas = evt.currentTarget || evt.srcElement,
				boundingRect = canvas.getBoundingClientRect();

			if (e.touches){
				mouseX = e.touches[0].clientX - boundingRect.left;
				mouseY = e.touches[0].clientY - boundingRect.top;

			}
			else{
				mouseX = e.clientX - boundingRect.left;
				mouseY = e.clientY - boundingRect.top;
			}

			return {
				x : mouseX,
				y : mouseY
			};

		},
		addEvent = helpers.addEvent = function(node,eventType,method){
			if (node.addEventListener){
				node.addEventListener(eventType,method);
			} else if (node.attachEvent){
				node.attachEvent("on"+eventType, method);
			} else {
				node["on"+eventType] = method;
			}
		},
		removeEvent = helpers.removeEvent = function(node, eventType, handler){
			if (node.removeEventListener){
				node.removeEventListener(eventType, handler, false);
			} else if (node.detachEvent){
				node.detachEvent("on"+eventType,handler);
			} else{
				node["on" + eventType] = noop;
			}
		},
		bindEvents = helpers.bindEvents = function(chartInstance, arrayOfEvents, handler){
			// Create the events object if it's not already present
			if (!chartInstance.events) chartInstance.events = {};

			each(arrayOfEvents,function(eventName){
				chartInstance.events[eventName] = function(){
					handler.apply(chartInstance, arguments);
				};
				addEvent(chartInstance.chart.canvas,eventName,chartInstance.events[eventName]);
			});
		},
		unbindEvents = helpers.unbindEvents = function (chartInstance, arrayOfEvents) {
			each(arrayOfEvents, function(handler,eventName){
				removeEvent(chartInstance.chart.canvas, eventName, handler);
			});
		},
		getMaximumWidth = helpers.getMaximumWidth = function(domNode){
			var container = domNode.parentNode;
			// TODO = check cross browser stuff with this.
			return container.clientWidth;
		},
		getMaximumHeight = helpers.getMaximumHeight = function(domNode){
			var container = domNode.parentNode;
			// TODO = check cross browser stuff with this.
			return container.clientHeight;
		},
		getMaximumSize = helpers.getMaximumSize = helpers.getMaximumWidth, // legacy support
		retinaScale = helpers.retinaScale = function(chart){
			var ctx = chart.ctx,
				width = chart.canvas.width,
				height = chart.canvas.height;

			if (window.devicePixelRatio) {
				ctx.canvas.style.width = width + "px";
				ctx.canvas.style.height = height + "px";
				ctx.canvas.height = height * window.devicePixelRatio;
				ctx.canvas.width = width * window.devicePixelRatio;
				ctx.scale(window.devicePixelRatio, window.devicePixelRatio);
			}
		},
		//-- Canvas methods
		clear = helpers.clear = function(chart){
			chart.ctx.clearRect(0,0,chart.width,chart.height);
		},
		fontString = helpers.fontString = function(pixelSize,fontStyle,fontFamily){
			return fontStyle + " " + pixelSize+"px " + fontFamily;
		},
		longestText = helpers.longestText = function(ctx,font,arrayOfStrings){
			ctx.font = font;
			var longest = 0;
			each(arrayOfStrings,function(string){
				var textWidth = ctx.measureText(string).width;
				longest = (textWidth > longest) ? textWidth : longest;
			});
			return longest;
		},
		drawRoundedRectangle = helpers.drawRoundedRectangle = function(ctx,x,y,width,height,radius){
			ctx.beginPath();
			ctx.moveTo(x + radius, y);
			ctx.lineTo(x + width - radius, y);
			ctx.quadraticCurveTo(x + width, y, x + width, y + radius);
			ctx.lineTo(x + width, y + height - radius);
			ctx.quadraticCurveTo(x + width, y + height, x + width - radius, y + height);
			ctx.lineTo(x + radius, y + height);
			ctx.quadraticCurveTo(x, y + height, x, y + height - radius);
			ctx.lineTo(x, y + radius);
			ctx.quadraticCurveTo(x, y, x + radius, y);
			ctx.closePath();
		};


	//Store a reference to each instance - allowing us to globally resize chart instances on window resize.
	//Destroy method on the chart will remove the instance of the chart from this reference.
	Chart.instances = {};

	Chart.Type = function(data,options,chart){
		this.options = options;
		this.chart = chart;
		this.id = uid();
		//Add the chart instance to the global namespace
		Chart.instances[this.id] = this;

		// Initialize is always called when a chart type is created
		// By default it is a no op, but it should be extended
		if (options.responsive){
			this.resize();
		}
		this.initialize.call(this,data);
	};

	//Core methods that'll be a part of every chart type
	extend(Chart.Type.prototype,{
		initialize : function(){return this;},
		clear : function(){
			clear(this.chart);
			return this;
		},
		stop : function(){
			// Stops any current animation loop occuring
			cancelAnimFrame(this.animationFrame);
			return this;
		},
		resize : function(callback){
			this.stop();
			var canvas = this.chart.canvas,
				newWidth = getMaximumWidth(this.chart.canvas),
				newHeight = this.options.maintainAspectRatio ? newWidth / this.chart.aspectRatio : getMaximumHeight(this.chart.canvas);

			canvas.width = this.chart.width = newWidth;
			canvas.height = this.chart.height = newHeight;

			retinaScale(this.chart);

			if (typeof callback === "function"){
				callback.apply(this, Array.prototype.slice.call(arguments, 1));
			}
			return this;
		},
		reflow : noop,
		render : function(reflow){
			if (reflow){
				this.reflow();
			}
			if (this.options.animation && !reflow){
				helpers.animationLoop(
					this.draw,
					this.options.animationSteps,
					this.options.animationEasing,
					this.options.onAnimationProgress,
					this.options.onAnimationComplete,
					this
				);
			}
			else{
				this.draw();
				this.options.onAnimationComplete.call(this);
			}
			return this;
		},
		generateLegend : function(){
			return template(this.options.legendTemplate,this);
		},
		destroy : function(){
			this.clear();
			unbindEvents(this, this.events);
			var canvas = this.chart.canvas;

			// Reset canvas height/width attributes starts a fresh with the canvas context
			canvas.width = this.chart.width;
			canvas.height = this.chart.height;

			// < IE9 doesn't support removeProperty
			if (canvas.style.removeProperty) {
				canvas.style.removeProperty('width');
				canvas.style.removeProperty('height');
			} else {
				canvas.style.removeAttribute('width');
				canvas.style.removeAttribute('height');
			}

			delete Chart.instances[this.id];
		},
		showTooltip : function(ChartElements, forceRedraw){
			// Only redraw the chart if we've actually changed what we're hovering on.
			if (typeof this.activeElements === 'undefined') this.activeElements = [];

			var isChanged = (function(Elements){
				var changed = false;

				if (Elements.length !== this.activeElements.length){
					changed = true;
					return changed;
				}

				each(Elements, function(element, index){
					if (element !== this.activeElements[index]){
						changed = true;
					}
				}, this);
				return changed;
			}).call(this, ChartElements);

			if (!isChanged && !forceRedraw){
				return;
			}
			else{
				this.activeElements = ChartElements;
			}
			this.draw();
			if(this.options.customTooltips){
				this.options.customTooltips(false);
			}
			if (ChartElements.length > 0){
				// If we have multiple datasets, show a MultiTooltip for all of the data points at that index
				if (this.datasets && this.datasets.length > 1) {
					var dataArray,
						dataIndex;

					for (var i = this.datasets.length - 1; i >= 0; i--) {
						dataArray = this.datasets[i].points || this.datasets[i].bars || this.datasets[i].segments;
						dataIndex = indexOf(dataArray, ChartElements[0]);
						if (dataIndex !== -1){
							break;
						}
					}
					var tooltipLabels = [],
						tooltipColors = [],
						medianPosition = (function(index) {

							// Get all the points at that particular index
							var Elements = [],
								dataCollection,
								xPositions = [],
								yPositions = [],
								xMax,
								yMax,
								xMin,
								yMin;
							helpers.each(this.datasets, function(dataset){
								dataCollection = dataset.points || dataset.bars || dataset.segments;
								if (dataCollection[dataIndex] && dataCollection[dataIndex].hasValue()){
									Elements.push(dataCollection[dataIndex]);
								}
							});

							helpers.each(Elements, function(element) {
								xPositions.push(element.x);
								yPositions.push(element.y);


								//Include any colour information about the element
								tooltipLabels.push(helpers.template(this.options.multiTooltipTemplate, element));
								tooltipColors.push({
									fill: element._saved.fillColor || element.fillColor,
									stroke: element._saved.strokeColor || element.strokeColor
								});

							}, this);

							yMin = min(yPositions);
							yMax = max(yPositions);

							xMin = min(xPositions);
							xMax = max(xPositions);

							return {
								x: (xMin > this.chart.width/2) ? xMin : xMax,
								y: (yMin + yMax)/2
							};
						}).call(this, dataIndex);

					new Chart.MultiTooltip({
						x: medianPosition.x,
						y: medianPosition.y,
						xPadding: this.options.tooltipXPadding,
						yPadding: this.options.tooltipYPadding,
						xOffset: this.options.tooltipXOffset,
						fillColor: this.options.tooltipFillColor,
						textColor: this.options.tooltipFontColor,
						fontFamily: this.options.tooltipFontFamily,
						fontStyle: this.options.tooltipFontStyle,
						fontSize: this.options.tooltipFontSize,
						titleTextColor: this.options.tooltipTitleFontColor,
						titleFontFamily: this.options.tooltipTitleFontFamily,
						titleFontStyle: this.options.tooltipTitleFontStyle,
						titleFontSize: this.options.tooltipTitleFontSize,
						cornerRadius: this.options.tooltipCornerRadius,
						labels: tooltipLabels,
						legendColors: tooltipColors,
						legendColorBackground : this.options.multiTooltipKeyBackground,
						title: ChartElements[0].label,
						chart: this.chart,
						ctx: this.chart.ctx,
						custom: this.options.customTooltips
					}).draw();

				} else {
					each(ChartElements, function(Element) {
						var tooltipPosition = Element.tooltipPosition();
						new Chart.Tooltip({
							x: Math.round(tooltipPosition.x),
							y: Math.round(tooltipPosition.y),
							xPadding: this.options.tooltipXPadding,
							yPadding: this.options.tooltipYPadding,
							fillColor: this.options.tooltipFillColor,
							textColor: this.options.tooltipFontColor,
							fontFamily: this.options.tooltipFontFamily,
							fontStyle: this.options.tooltipFontStyle,
							fontSize: this.options.tooltipFontSize,
							caretHeight: this.options.tooltipCaretSize,
							cornerRadius: this.options.tooltipCornerRadius,
							text: template(this.options.tooltipTemplate, Element),
							chart: this.chart,
							custom: this.options.customTooltips
						}).draw();
					}, this);
				}
			}
			return this;
		},
		toBase64Image : function(){
			return this.chart.canvas.toDataURL.apply(this.chart.canvas, arguments);
		}
	});

	Chart.Type.extend = function(extensions){

		var parent = this;

		var ChartType = function(){
			return parent.apply(this,arguments);
		};

		//Copy the prototype object of the this class
		ChartType.prototype = clone(parent.prototype);
		//Now overwrite some of the properties in the base class with the new extensions
		extend(ChartType.prototype, extensions);

		ChartType.extend = Chart.Type.extend;

		if (extensions.name || parent.prototype.name){

			var chartName = extensions.name || parent.prototype.name;
			//Assign any potential default values of the new chart type

			//If none are defined, we'll use a clone of the chart type this is being extended from.
			//I.e. if we extend a line chart, we'll use the defaults from the line chart if our new chart
			//doesn't define some defaults of their own.

			var baseDefaults = (Chart.defaults[parent.prototype.name]) ? clone(Chart.defaults[parent.prototype.name]) : {};

			Chart.defaults[chartName] = extend(baseDefaults,extensions.defaults);

			Chart.types[chartName] = ChartType;

			//Register this new chart type in the Chart prototype
			Chart.prototype[chartName] = function(data,options){
				var config = merge(Chart.defaults.global, Chart.defaults[chartName], options || {});
				return new ChartType(data,config,this);
			};
		} else{
			warn("Name not provided for this chart, so it hasn't been registered");
		}
		return parent;
	};

	Chart.Element = function(configuration){
		extend(this,configuration);
		this.initialize.apply(this,arguments);
		this.save();
	};
	extend(Chart.Element.prototype,{
		initialize : function(){},
		restore : function(props){
			if (!props){
				extend(this,this._saved);
			} else {
				each(props,function(key){
					this[key] = this._saved[key];
				},this);
			}
			return this;
		},
		save : function(){
			this._saved = clone(this);
			delete this._saved._saved;
			return this;
		},
		update : function(newProps){
			each(newProps,function(value,key){
				this._saved[key] = this[key];
				this[key] = value;
			},this);
			return this;
		},
		transition : function(props,ease){
			each(props,function(value,key){
				this[key] = ((value - this._saved[key]) * ease) + this._saved[key];
			},this);
			return this;
		},
		tooltipPosition : function(){
			return {
				x : this.x,
				y : this.y
			};
		},
		hasValue: function(){
			return isNumber(this.value);
		}
	});

	Chart.Element.extend = inherits;


	Chart.Point = Chart.Element.extend({
		display: true,
		inRange: function(chartX,chartY){
			var hitDetectionRange = this.hitDetectionRadius + this.radius;
			return ((Math.pow(chartX-this.x, 2)+Math.pow(chartY-this.y, 2)) < Math.pow(hitDetectionRange,2));
		},
		draw : function(){
			if (this.display){
				var ctx = this.ctx;
				ctx.beginPath();

				ctx.arc(this.x, this.y, this.radius, 0, Math.PI*2);
				ctx.closePath();

				ctx.strokeStyle = this.strokeColor;
				ctx.lineWidth = this.strokeWidth;

				ctx.fillStyle = this.fillColor;

				ctx.fill();
				ctx.stroke();
			}


			//Quick debug for bezier curve splining
			//Highlights control points and the line between them.
			//Handy for dev - stripped in the min version.

			// ctx.save();
			// ctx.fillStyle = "black";
			// ctx.strokeStyle = "black"
			// ctx.beginPath();
			// ctx.arc(this.controlPoints.inner.x,this.controlPoints.inner.y, 2, 0, Math.PI*2);
			// ctx.fill();

			// ctx.beginPath();
			// ctx.arc(this.controlPoints.outer.x,this.controlPoints.outer.y, 2, 0, Math.PI*2);
			// ctx.fill();

			// ctx.moveTo(this.controlPoints.inner.x,this.controlPoints.inner.y);
			// ctx.lineTo(this.x, this.y);
			// ctx.lineTo(this.controlPoints.outer.x,this.controlPoints.outer.y);
			// ctx.stroke();

			// ctx.restore();



		}
	});

	Chart.Arc = Chart.Element.extend({
		inRange : function(chartX,chartY){

			var pointRelativePosition = helpers.getAngleFromPoint(this, {
				x: chartX,
				y: chartY
			});

			//Check if within the range of the open/close angle
			var betweenAngles = (pointRelativePosition.angle >= this.startAngle && pointRelativePosition.angle <= this.endAngle),
				withinRadius = (pointRelativePosition.distance >= this.innerRadius && pointRelativePosition.distance <= this.outerRadius);

			return (betweenAngles && withinRadius);
			//Ensure within the outside of the arc centre, but inside arc outer
		},
		tooltipPosition : function(){
			var centreAngle = this.startAngle + ((this.endAngle - this.startAngle) / 2),
				rangeFromCentre = (this.outerRadius - this.innerRadius) / 2 + this.innerRadius;
			return {
				x : this.x + (Math.cos(centreAngle) * rangeFromCentre),
				y : this.y + (Math.sin(centreAngle) * rangeFromCentre)
			};
		},
		draw : function(animationPercent){

			var easingDecimal = animationPercent || 1;

			var ctx = this.ctx;

			ctx.beginPath();

			ctx.arc(this.x, this.y, this.outerRadius, this.startAngle, this.endAngle);

			ctx.arc(this.x, this.y, this.innerRadius, this.endAngle, this.startAngle, true);

			ctx.closePath();
			ctx.strokeStyle = this.strokeColor;
			ctx.lineWidth = this.strokeWidth;

			ctx.fillStyle = this.fillColor;

			ctx.fill();
			ctx.lineJoin = 'bevel';

			if (this.showStroke){
				ctx.stroke();
			}
		}
	});

	Chart.Rectangle = Chart.Element.extend({
		draw : function(){
			var ctx = this.ctx,
				halfWidth = this.width/2,
				leftX = this.x - halfWidth,
				rightX = this.x + halfWidth,
				top = this.base - (this.base - this.y),
				halfStroke = this.strokeWidth / 2;

			// Canvas doesn't allow us to stroke inside the width so we can
			// adjust the sizes to fit if we're setting a stroke on the line
			if (this.showStroke){
				leftX += halfStroke;
				rightX -= halfStroke;
				top += halfStroke;
			}

			ctx.beginPath();

			ctx.fillStyle = this.fillColor;
			ctx.strokeStyle = this.strokeColor;
			ctx.lineWidth = this.strokeWidth;

			// It'd be nice to keep this class totally generic to any rectangle
			// and simply specify which border to miss out.
			ctx.moveTo(leftX, this.base);
			ctx.lineTo(leftX, top);
			ctx.lineTo(rightX, top);
			ctx.lineTo(rightX, this.base);
			ctx.fill();
			if (this.showStroke){
				ctx.stroke();
			}
		},
		height : function(){
			return this.base - this.y;
		},
		inRange : function(chartX,chartY){
			return (chartX >= this.x - this.width/2 && chartX <= this.x + this.width/2) && (chartY >= this.y && chartY <= this.base);
		}
	});

	Chart.Tooltip = Chart.Element.extend({
		draw : function(){

			var ctx = this.chart.ctx;

			ctx.font = fontString(this.fontSize,this.fontStyle,this.fontFamily);

			this.xAlign = "center";
			this.yAlign = "above";

			//Distance between the actual element.y position and the start of the tooltip caret
			var caretPadding = this.caretPadding = 2;

			var tooltipWidth = ctx.measureText(this.text).width + 2*this.xPadding,
				tooltipRectHeight = this.fontSize + 2*this.yPadding,
				tooltipHeight = tooltipRectHeight + this.caretHeight + caretPadding;

			if (this.x + tooltipWidth/2 >this.chart.width){
				this.xAlign = "left";
			} else if (this.x - tooltipWidth/2 < 0){
				this.xAlign = "right";
			}

			if (this.y - tooltipHeight < 0){
				this.yAlign = "below";
			}


			var tooltipX = this.x - tooltipWidth/2,
				tooltipY = this.y - tooltipHeight;

			ctx.fillStyle = this.fillColor;

			// Custom Tooltips
			if(this.custom){
				this.custom(this);
			}
			else{
				switch(this.yAlign)
				{
				case "above":
					//Draw a caret above the x/y
					ctx.beginPath();
					ctx.moveTo(this.x,this.y - caretPadding);
					ctx.lineTo(this.x + this.caretHeight, this.y - (caretPadding + this.caretHeight));
					ctx.lineTo(this.x - this.caretHeight, this.y - (caretPadding + this.caretHeight));
					ctx.closePath();
					ctx.fill();
					break;
				case "below":
					tooltipY = this.y + caretPadding + this.caretHeight;
					//Draw a caret below the x/y
					ctx.beginPath();
					ctx.moveTo(this.x, this.y + caretPadding);
					ctx.lineTo(this.x + this.caretHeight, this.y + caretPadding + this.caretHeight);
					ctx.lineTo(this.x - this.caretHeight, this.y + caretPadding + this.caretHeight);
					ctx.closePath();
					ctx.fill();
					break;
				}

				switch(this.xAlign)
				{
				case "left":
					tooltipX = this.x - tooltipWidth + (this.cornerRadius + this.caretHeight);
					break;
				case "right":
					tooltipX = this.x - (this.cornerRadius + this.caretHeight);
					break;
				}

				drawRoundedRectangle(ctx,tooltipX,tooltipY,tooltipWidth,tooltipRectHeight,this.cornerRadius);

				ctx.fill();

				ctx.fillStyle = this.textColor;
				ctx.textAlign = "center";
				ctx.textBaseline = "middle";
				ctx.fillText(this.text, tooltipX + tooltipWidth/2, tooltipY + tooltipRectHeight/2);
			}
		}
	});

	Chart.MultiTooltip = Chart.Element.extend({
		initialize : function(){
			this.font = fontString(this.fontSize,this.fontStyle,this.fontFamily);

			this.titleFont = fontString(this.titleFontSize,this.titleFontStyle,this.titleFontFamily);

			this.height = (this.labels.length * this.fontSize) + ((this.labels.length-1) * (this.fontSize/2)) + (this.yPadding*2) + this.titleFontSize *1.5;

			this.ctx.font = this.titleFont;

			var titleWidth = this.ctx.measureText(this.title).width,
				//Label has a legend square as well so account for this.
				labelWidth = longestText(this.ctx,this.font,this.labels) + this.fontSize + 3,
				longestTextWidth = max([labelWidth,titleWidth]);

			this.width = longestTextWidth + (this.xPadding*2);


			var halfHeight = this.height/2;

			//Check to ensure the height will fit on the canvas
			if (this.y - halfHeight < 0 ){
				this.y = halfHeight;
			} else if (this.y + halfHeight > this.chart.height){
				this.y = this.chart.height - halfHeight;
			}

			//Decide whether to align left or right based on position on canvas
			if (this.x > this.chart.width/2){
				this.x -= this.xOffset + this.width;
			} else {
				this.x += this.xOffset;
			}


		},
		getLineHeight : function(index){
			var baseLineHeight = this.y - (this.height/2) + this.yPadding,
				afterTitleIndex = index-1;

			//If the index is zero, we're getting the title
			if (index === 0){
				return baseLineHeight + this.titleFontSize/2;
			} else{
				return baseLineHeight + ((this.fontSize*1.5*afterTitleIndex) + this.fontSize/2) + this.titleFontSize * 1.5;
			}

		},
		draw : function(){
			// Custom Tooltips
			if(this.custom){
				this.custom(this);
			}
			else{
				drawRoundedRectangle(this.ctx,this.x,this.y - this.height/2,this.width,this.height,this.cornerRadius);
				var ctx = this.ctx;
				ctx.fillStyle = this.fillColor;
				ctx.fill();
				ctx.closePath();

				ctx.textAlign = "left";
				ctx.textBaseline = "middle";
				ctx.fillStyle = this.titleTextColor;
				ctx.font = this.titleFont;

				ctx.fillText(this.title,this.x + this.xPadding, this.getLineHeight(0));

				ctx.font = this.font;
				helpers.each(this.labels,function(label,index){
					ctx.fillStyle = this.textColor;
					ctx.fillText(label,this.x + this.xPadding + this.fontSize + 3, this.getLineHeight(index + 1));

					//A bit gnarly, but clearing this rectangle breaks when using explorercanvas (clears whole canvas)
					//ctx.clearRect(this.x + this.xPadding, this.getLineHeight(index + 1) - this.fontSize/2, this.fontSize, this.fontSize);
					//Instead we'll make a white filled block to put the legendColour palette over.

					ctx.fillStyle = this.legendColorBackground;
					ctx.fillRect(this.x + this.xPadding, this.getLineHeight(index + 1) - this.fontSize/2, this.fontSize, this.fontSize);

					ctx.fillStyle = this.legendColors[index].fill;
					ctx.fillRect(this.x + this.xPadding, this.getLineHeight(index + 1) - this.fontSize/2, this.fontSize, this.fontSize);


				},this);
			}
		}
	});

	Chart.Scale = Chart.Element.extend({
		initialize : function(){
			this.fit();
		},
		buildYLabels : function(){
			this.yLabels = [];

			var stepDecimalPlaces = getDecimalPlaces(this.stepValue);

			for (var i=0; i<=this.steps; i++){
				this.yLabels.push(template(this.templateString,{value:(this.min + (i * this.stepValue)).toFixed(stepDecimalPlaces)}));
			}
			this.yLabelWidth = (this.display && this.showLabels) ? longestText(this.ctx,this.font,this.yLabels) : 0;
		},
		addXLabel : function(label){
			this.xLabels.push(label);
			this.valuesCount++;
			this.fit();
		},
		removeXLabel : function(){
			this.xLabels.shift();
			this.valuesCount--;
			this.fit();
		},
		// Fitting loop to rotate x Labels and figure out what fits there, and also calculate how many Y steps to use
		fit: function(){
			// First we need the width of the yLabels, assuming the xLabels aren't rotated

			// To do that we need the base line at the top and base of the chart, assuming there is no x label rotation
			this.startPoint = (this.display) ? this.fontSize : 0;
			this.endPoint = (this.display) ? this.height - (this.fontSize * 1.5) - 5 : this.height; // -5 to pad labels

			// Apply padding settings to the start and end point.
			this.startPoint += this.padding;
			this.endPoint -= this.padding;

			// Cache the starting height, so can determine if we need to recalculate the scale yAxis
			var cachedHeight = this.endPoint - this.startPoint,
				cachedYLabelWidth;

			// Build the current yLabels so we have an idea of what size they'll be to start
			/*
			 *	This sets what is returned from calculateScaleRange as static properties of this class:
			 *
				this.steps;
				this.stepValue;
				this.min;
				this.max;
			 *
			 */
			this.calculateYRange(cachedHeight);

			// With these properties set we can now build the array of yLabels
			// and also the width of the largest yLabel
			this.buildYLabels();

			this.calculateXLabelRotation();

			while((cachedHeight > this.endPoint - this.startPoint)){
				cachedHeight = this.endPoint - this.startPoint;
				cachedYLabelWidth = this.yLabelWidth;

				this.calculateYRange(cachedHeight);
				this.buildYLabels();

				// Only go through the xLabel loop again if the yLabel width has changed
				if (cachedYLabelWidth < this.yLabelWidth){
					this.calculateXLabelRotation();
				}
			}

		},
		calculateXLabelRotation : function(){
			//Get the width of each grid by calculating the difference
			//between x offsets between 0 and 1.

			this.ctx.font = this.font;

			var firstWidth = this.ctx.measureText(this.xLabels[0]).width,
				lastWidth = this.ctx.measureText(this.xLabels[this.xLabels.length - 1]).width,
				firstRotated,
				lastRotated;


			this.xScalePaddingRight = lastWidth/2 + 3;
			this.xScalePaddingLeft = (firstWidth/2 > this.yLabelWidth + 10) ? firstWidth/2 : this.yLabelWidth + 10;

			this.xLabelRotation = 0;
			if (this.display){
				var originalLabelWidth = longestText(this.ctx,this.font,this.xLabels),
					cosRotation,
					firstRotatedWidth;
				this.xLabelWidth = originalLabelWidth;
				//Allow 3 pixels x2 padding either side for label readability
				var xGridWidth = Math.floor(this.calculateX(1) - this.calculateX(0)) - 6;

				//Max label rotate should be 90 - also act as a loop counter
				while ((this.xLabelWidth > xGridWidth && this.xLabelRotation === 0) || (this.xLabelWidth > xGridWidth && this.xLabelRotation <= 90 && this.xLabelRotation > 0)){
					cosRotation = Math.cos(toRadians(this.xLabelRotation));

					firstRotated = cosRotation * firstWidth;
					lastRotated = cosRotation * lastWidth;

					// We're right aligning the text now.
					if (firstRotated + this.fontSize / 2 > this.yLabelWidth + 8){
						this.xScalePaddingLeft = firstRotated + this.fontSize / 2;
					}
					this.xScalePaddingRight = this.fontSize/2;


					this.xLabelRotation++;
					this.xLabelWidth = cosRotation * originalLabelWidth;

				}
				if (this.xLabelRotation > 0){
					this.endPoint -= Math.sin(toRadians(this.xLabelRotation))*originalLabelWidth + 3;
				}
			}
			else{
				this.xLabelWidth = 0;
				this.xScalePaddingRight = this.padding;
				this.xScalePaddingLeft = this.padding;
			}

		},
		// Needs to be overidden in each Chart type
		// Otherwise we need to pass all the data into the scale class
		calculateYRange: noop,
		drawingArea: function(){
			return this.startPoint - this.endPoint;
		},
		calculateY : function(value){
			var scalingFactor = this.drawingArea() / (this.min - this.max);
			return this.endPoint - (scalingFactor * (value - this.min));
		},
		calculateX : function(index){
			var isRotated = (this.xLabelRotation > 0),
				// innerWidth = (this.offsetGridLines) ? this.width - offsetLeft - this.padding : this.width - (offsetLeft + halfLabelWidth * 2) - this.padding,
				innerWidth = this.width - (this.xScalePaddingLeft + this.xScalePaddingRight),
				valueWidth = innerWidth/Math.max((this.valuesCount - ((this.offsetGridLines) ? 0 : 1)), 1),
				valueOffset = (valueWidth * index) + this.xScalePaddingLeft;

			if (this.offsetGridLines){
				valueOffset += (valueWidth/2);
			}

			return Math.round(valueOffset);
		},
		update : function(newProps){
			helpers.extend(this, newProps);
			this.fit();
		},
		draw : function(){
			var ctx = this.ctx,
				yLabelGap = (this.endPoint - this.startPoint) / this.steps,
				xStart = Math.round(this.xScalePaddingLeft);
			if (this.display){
				ctx.fillStyle = this.textColor;
				ctx.font = this.font;
				each(this.yLabels,function(labelString,index){
					var yLabelCenter = this.endPoint - (yLabelGap * index),
						linePositionY = Math.round(yLabelCenter),
						drawHorizontalLine = this.showHorizontalLines;

					ctx.textAlign = "right";
					ctx.textBaseline = "middle";
					if (this.showLabels){
						ctx.fillText(labelString,xStart - 10,yLabelCenter);
					}

					// This is X axis, so draw it
					if (index === 0 && !drawHorizontalLine){
						drawHorizontalLine = true;
					}

					if (drawHorizontalLine){
						ctx.beginPath();
					}

					if (index > 0){
						// This is a grid line in the centre, so drop that
						ctx.lineWidth = this.gridLineWidth;
						ctx.strokeStyle = this.gridLineColor;
					} else {
						// This is the first line on the scale
						ctx.lineWidth = this.lineWidth;
						ctx.strokeStyle = this.lineColor;
					}

					linePositionY += helpers.aliasPixel(ctx.lineWidth);

					if(drawHorizontalLine){
						ctx.moveTo(xStart, linePositionY);
						ctx.lineTo(this.width, linePositionY);
						ctx.stroke();
						ctx.closePath();
					}

					ctx.lineWidth = this.lineWidth;
					ctx.strokeStyle = this.lineColor;
					ctx.beginPath();
					ctx.moveTo(xStart - 5, linePositionY);
					ctx.lineTo(xStart, linePositionY);
					ctx.stroke();
					ctx.closePath();

				},this);

				each(this.xLabels,function(label,index){
					var xPos = this.calculateX(index) + aliasPixel(this.lineWidth),
						// Check to see if line/bar here and decide where to place the line
						linePos = this.calculateX(index - (this.offsetGridLines ? 0.5 : 0)) + aliasPixel(this.lineWidth),
						isRotated = (this.xLabelRotation > 0),
						drawVerticalLine = this.showVerticalLines;

					// This is Y axis, so draw it
					if (index === 0 && !drawVerticalLine){
						drawVerticalLine = true;
					}

					if (drawVerticalLine){
						ctx.beginPath();
					}

					if (index > 0){
						// This is a grid line in the centre, so drop that
						ctx.lineWidth = this.gridLineWidth;
						ctx.strokeStyle = this.gridLineColor;
					} else {
						// This is the first line on the scale
						ctx.lineWidth = this.lineWidth;
						ctx.strokeStyle = this.lineColor;
					}

					if (drawVerticalLine){
						ctx.moveTo(linePos,this.endPoint);
						ctx.lineTo(linePos,this.startPoint - 3);
						ctx.stroke();
						ctx.closePath();
					}


					ctx.lineWidth = this.lineWidth;
					ctx.strokeStyle = this.lineColor;


					// Small lines at the bottom of the base grid line
					ctx.beginPath();
					ctx.moveTo(linePos,this.endPoint);
					ctx.lineTo(linePos,this.endPoint + 5);
					ctx.stroke();
					ctx.closePath();

					ctx.save();
					ctx.translate(xPos,(isRotated) ? this.endPoint + 12 : this.endPoint + 8);
					ctx.rotate(toRadians(this.xLabelRotation)*-1);
					ctx.font = this.font;
					ctx.textAlign = (isRotated) ? "right" : "center";
					ctx.textBaseline = (isRotated) ? "middle" : "top";
					ctx.fillText(label, 0, 0);
					ctx.restore();
				},this);

			}
		}

	});

	Chart.RadialScale = Chart.Element.extend({
		initialize: function(){
			this.size = min([this.height, this.width]);
			this.drawingArea = (this.display) ? (this.size/2) - (this.fontSize/2 + this.backdropPaddingY) : (this.size/2);
		},
		calculateCenterOffset: function(value){
			// Take into account half font size + the yPadding of the top value
			var scalingFactor = this.drawingArea / (this.max - this.min);

			return (value - this.min) * scalingFactor;
		},
		update : function(){
			if (!this.lineArc){
				this.setScaleSize();
			} else {
				this.drawingArea = (this.display) ? (this.size/2) - (this.fontSize/2 + this.backdropPaddingY) : (this.size/2);
			}
			this.buildYLabels();
		},
		buildYLabels: function(){
			this.yLabels = [];

			var stepDecimalPlaces = getDecimalPlaces(this.stepValue);

			for (var i=0; i<=this.steps; i++){
				this.yLabels.push(template(this.templateString,{value:(this.min + (i * this.stepValue)).toFixed(stepDecimalPlaces)}));
			}
		},
		getCircumference : function(){
			return ((Math.PI*2) / this.valuesCount);
		},
		setScaleSize: function(){
			/*
			 * Right, this is really confusing and there is a lot of maths going on here
			 * The gist of the problem is here: https://gist.github.com/nnnick/696cc9c55f4b0beb8fe9
			 *
			 * Reaction: https://dl.dropboxusercontent.com/u/34601363/toomuchscience.gif
			 *
			 * Solution:
			 *
			 * We assume the radius of the polygon is half the size of the canvas at first
			 * at each index we check if the text overlaps.
			 *
			 * Where it does, we store that angle and that index.
			 *
			 * After finding the largest index and angle we calculate how much we need to remove
			 * from the shape radius to move the point inwards by that x.
			 *
			 * We average the left and right distances to get the maximum shape radius that can fit in the box
			 * along with labels.
			 *
			 * Once we have that, we can find the centre point for the chart, by taking the x text protrusion
			 * on each side, removing that from the size, halving it and adding the left x protrusion width.
			 *
			 * This will mean we have a shape fitted to the canvas, as large as it can be with the labels
			 * and position it in the most space efficient manner
			 *
			 * https://dl.dropboxusercontent.com/u/34601363/yeahscience.gif
			 */


			// Get maximum radius of the polygon. Either half the height (minus the text width) or half the width.
			// Use this to calculate the offset + change. - Make sure L/R protrusion is at least 0 to stop issues with centre points
			var largestPossibleRadius = min([(this.height/2 - this.pointLabelFontSize - 5), this.width/2]),
				pointPosition,
				i,
				textWidth,
				halfTextWidth,
				furthestRight = this.width,
				furthestRightIndex,
				furthestRightAngle,
				furthestLeft = 0,
				furthestLeftIndex,
				furthestLeftAngle,
				xProtrusionLeft,
				xProtrusionRight,
				radiusReductionRight,
				radiusReductionLeft,
				maxWidthRadius;
			this.ctx.font = fontString(this.pointLabelFontSize,this.pointLabelFontStyle,this.pointLabelFontFamily);
			for (i=0;i<this.valuesCount;i++){
				// 5px to space the text slightly out - similar to what we do in the draw function.
				pointPosition = this.getPointPosition(i, largestPossibleRadius);
				textWidth = this.ctx.measureText(template(this.templateString, { value: this.labels[i] })).width + 5;
				if (i === 0 || i === this.valuesCount/2){
					// If we're at index zero, or exactly the middle, we're at exactly the top/bottom
					// of the radar chart, so text will be aligned centrally, so we'll half it and compare
					// w/left and right text sizes
					halfTextWidth = textWidth/2;
					if (pointPosition.x + halfTextWidth > furthestRight) {
						furthestRight = pointPosition.x + halfTextWidth;
						furthestRightIndex = i;
					}
					if (pointPosition.x - halfTextWidth < furthestLeft) {
						furthestLeft = pointPosition.x - halfTextWidth;
						furthestLeftIndex = i;
					}
				}
				else if (i < this.valuesCount/2) {
					// Less than half the values means we'll left align the text
					if (pointPosition.x + textWidth > furthestRight) {
						furthestRight = pointPosition.x + textWidth;
						furthestRightIndex = i;
					}
				}
				else if (i > this.valuesCount/2){
					// More than half the values means we'll right align the text
					if (pointPosition.x - textWidth < furthestLeft) {
						furthestLeft = pointPosition.x - textWidth;
						furthestLeftIndex = i;
					}
				}
			}

			xProtrusionLeft = furthestLeft;

			xProtrusionRight = Math.ceil(furthestRight - this.width);

			furthestRightAngle = this.getIndexAngle(furthestRightIndex);

			furthestLeftAngle = this.getIndexAngle(furthestLeftIndex);

			radiusReductionRight = xProtrusionRight / Math.sin(furthestRightAngle + Math.PI/2);

			radiusReductionLeft = xProtrusionLeft / Math.sin(furthestLeftAngle + Math.PI/2);

			// Ensure we actually need to reduce the size of the chart
			radiusReductionRight = (isNumber(radiusReductionRight)) ? radiusReductionRight : 0;
			radiusReductionLeft = (isNumber(radiusReductionLeft)) ? radiusReductionLeft : 0;

			this.drawingArea = largestPossibleRadius - (radiusReductionLeft + radiusReductionRight)/2;

			//this.drawingArea = min([maxWidthRadius, (this.height - (2 * (this.pointLabelFontSize + 5)))/2])
			this.setCenterPoint(radiusReductionLeft, radiusReductionRight);

		},
		setCenterPoint: function(leftMovement, rightMovement){

			var maxRight = this.width - rightMovement - this.drawingArea,
				maxLeft = leftMovement + this.drawingArea;

			this.xCenter = (maxLeft + maxRight)/2;
			// Always vertically in the centre as the text height doesn't change
			this.yCenter = (this.height/2);
		},

		getIndexAngle : function(index){
			var angleMultiplier = (Math.PI * 2) / this.valuesCount;
			// Start from the top instead of right, so remove a quarter of the circle

			return index * angleMultiplier - (Math.PI/2);
		},
		getPointPosition : function(index, distanceFromCenter){
			var thisAngle = this.getIndexAngle(index);
			return {
				x : (Math.cos(thisAngle) * distanceFromCenter) + this.xCenter,
				y : (Math.sin(thisAngle) * distanceFromCenter) + this.yCenter
			};
		},
		draw: function(){
			if (this.display){
				var ctx = this.ctx;
				each(this.yLabels, function(label, index){
					// Don't draw a centre value
					if (index > 0){
						var yCenterOffset = index * (this.drawingArea/this.steps),
							yHeight = this.yCenter - yCenterOffset,
							pointPosition;

						// Draw circular lines around the scale
						if (this.lineWidth > 0){
							ctx.strokeStyle = this.lineColor;
							ctx.lineWidth = this.lineWidth;

							if(this.lineArc){
								ctx.beginPath();
								ctx.arc(this.xCenter, this.yCenter, yCenterOffset, 0, Math.PI*2);
								ctx.closePath();
								ctx.stroke();
							} else{
								ctx.beginPath();
								for (var i=0;i<this.valuesCount;i++)
								{
									pointPosition = this.getPointPosition(i, this.calculateCenterOffset(this.min + (index * this.stepValue)));
									if (i === 0){
										ctx.moveTo(pointPosition.x, pointPosition.y);
									} else {
										ctx.lineTo(pointPosition.x, pointPosition.y);
									}
								}
								ctx.closePath();
								ctx.stroke();
							}
						}
						if(this.showLabels){
							ctx.font = fontString(this.fontSize,this.fontStyle,this.fontFamily);
							if (this.showLabelBackdrop){
								var labelWidth = ctx.measureText(label).width;
								ctx.fillStyle = this.backdropColor;
								ctx.fillRect(
									this.xCenter - labelWidth/2 - this.backdropPaddingX,
									yHeight - this.fontSize/2 - this.backdropPaddingY,
									labelWidth + this.backdropPaddingX*2,
									this.fontSize + this.backdropPaddingY*2
								);
							}
							ctx.textAlign = 'center';
							ctx.textBaseline = "middle";
							ctx.fillStyle = this.fontColor;
							ctx.fillText(label, this.xCenter, yHeight);
						}
					}
				}, this);

				if (!this.lineArc){
					ctx.lineWidth = this.angleLineWidth;
					ctx.strokeStyle = this.angleLineColor;
					for (var i = this.valuesCount - 1; i >= 0; i--) {
						if (this.angleLineWidth > 0){
							var outerPosition = this.getPointPosition(i, this.calculateCenterOffset(this.max));
							ctx.beginPath();
							ctx.moveTo(this.xCenter, this.yCenter);
							ctx.lineTo(outerPosition.x, outerPosition.y);
							ctx.stroke();
							ctx.closePath();
						}
						// Extra 3px out for some label spacing
						var pointLabelPosition = this.getPointPosition(i, this.calculateCenterOffset(this.max) + 5);
						ctx.font = fontString(this.pointLabelFontSize,this.pointLabelFontStyle,this.pointLabelFontFamily);
						ctx.fillStyle = this.pointLabelFontColor;

						var labelsCount = this.labels.length,
							halfLabelsCount = this.labels.length/2,
							quarterLabelsCount = halfLabelsCount/2,
							upperHalf = (i < quarterLabelsCount || i > labelsCount - quarterLabelsCount),
							exactQuarter = (i === quarterLabelsCount || i === labelsCount - quarterLabelsCount);
						if (i === 0){
							ctx.textAlign = 'center';
						} else if(i === halfLabelsCount){
							ctx.textAlign = 'center';
						} else if (i < halfLabelsCount){
							ctx.textAlign = 'left';
						} else {
							ctx.textAlign = 'right';
						}

						// Set the correct text baseline based on outer positioning
						if (exactQuarter){
							ctx.textBaseline = 'middle';
						} else if (upperHalf){
							ctx.textBaseline = 'bottom';
						} else {
							ctx.textBaseline = 'top';
						}

						ctx.fillText(this.labels[i], pointLabelPosition.x, pointLabelPosition.y);
					}
				}
			}
		}
	});

	// Attach global event to resize each chart instance when the browser resizes
	helpers.addEvent(window, "resize", (function(){
		// Basic debounce of resize function so it doesn't hurt performance when resizing browser.
		var timeout;
		return function(){
			clearTimeout(timeout);
			timeout = setTimeout(function(){
				each(Chart.instances,function(instance){
					// If the responsive flag is set in the chart instance config
					// Cascade the resize event down to the chart.
					if (instance.options.responsive){
						instance.resize(instance.render, true);
					}
				});
			}, 50);
		};
	})());


	if (amd) {
		define(function(){
			return Chart;
		});
	} else if (typeof module === 'object' && module.exports) {
		module.exports = Chart;
	}

	root.Chart = Chart;

	Chart.noConflict = function(){
		root.Chart = previous;
		return Chart;
	};

}).call(this);

(function(){
	"use strict";

	var root = this,
		Chart = root.Chart,
		helpers = Chart.helpers;


	var defaultConfig = {
		//Boolean - Whether the scale should start at zero, or an order of magnitude down from the lowest value
		scaleBeginAtZero : true,

		//Boolean - Whether grid lines are shown across the chart
		scaleShowGridLines : true,

		//String - Colour of the grid lines
		scaleGridLineColor : "rgba(0,0,0,.05)",

		//Number - Width of the grid lines
		scaleGridLineWidth : 1,

		//Boolean - Whether to show horizontal lines (except X axis)
		scaleShowHorizontalLines: true,

		//Boolean - Whether to show vertical lines (except Y axis)
		scaleShowVerticalLines: true,

		//Boolean - If there is a stroke on each bar
		barShowStroke : true,

		//Number - Pixel width of the bar stroke
		barStrokeWidth : 2,

		//Number - Spacing between each of the X value sets
		barValueSpacing : 5,

		//Number - Spacing between data sets within X values
		barDatasetSpacing : 1,

		//String - A legend template
		legendTemplate : "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].fillColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>"

	};


	Chart.Type.extend({
		name: "Bar",
		defaults : defaultConfig,
		initialize:  function(data){

			//Expose options as a scope variable here so we can access it in the ScaleClass
			var options = this.options;

			this.ScaleClass = Chart.Scale.extend({
				offsetGridLines : true,
				calculateBarX : function(datasetCount, datasetIndex, barIndex){
					//Reusable method for calculating the xPosition of a given bar based on datasetIndex & width of the bar
					var xWidth = this.calculateBaseWidth(),
						xAbsolute = this.calculateX(barIndex) - (xWidth/2),
						barWidth = this.calculateBarWidth(datasetCount);

					return xAbsolute + (barWidth * datasetIndex) + (datasetIndex * options.barDatasetSpacing) + barWidth/2;
				},
				calculateBaseWidth : function(){
					return (this.calculateX(1) - this.calculateX(0)) - (2*options.barValueSpacing);
				},
				calculateBarWidth : function(datasetCount){
					//The padding between datasets is to the right of each bar, providing that there are more than 1 dataset
					var baseWidth = this.calculateBaseWidth() - ((datasetCount - 1) * options.barDatasetSpacing);

					return (baseWidth / datasetCount);
				}
			});

			this.datasets = [];

			//Set up tooltip events on the chart
			if (this.options.showTooltips){
				helpers.bindEvents(this, this.options.tooltipEvents, function(evt){
					var activeBars = (evt.type !== 'mouseout') ? this.getBarsAtEvent(evt) : [];

					this.eachBars(function(bar){
						bar.restore(['fillColor', 'strokeColor']);
					});
					helpers.each(activeBars, function(activeBar){
						activeBar.fillColor = activeBar.highlightFill;
						activeBar.strokeColor = activeBar.highlightStroke;
					});
					this.showTooltip(activeBars);
				});
			}

			//Declare the extension of the default point, to cater for the options passed in to the constructor
			this.BarClass = Chart.Rectangle.extend({
				strokeWidth : this.options.barStrokeWidth,
				showStroke : this.options.barShowStroke,
				ctx : this.chart.ctx
			});

			//Iterate through each of the datasets, and build this into a property of the chart
			helpers.each(data.datasets,function(dataset,datasetIndex){

				var datasetObject = {
					label : dataset.label || null,
					fillColor : dataset.fillColor,
					strokeColor : dataset.strokeColor,
					bars : []
				};

				this.datasets.push(datasetObject);

				helpers.each(dataset.data,function(dataPoint,index){
					//Add a new point for each piece of data, passing any required data to draw.
					datasetObject.bars.push(new this.BarClass({
						value : dataPoint,
						label : data.labels[index],
						datasetLabel: dataset.label,
						strokeColor : dataset.strokeColor,
						fillColor : dataset.fillColor,
						highlightFill : dataset.highlightFill || dataset.fillColor,
						highlightStroke : dataset.highlightStroke || dataset.strokeColor
					}));
				},this);

			},this);

			this.buildScale(data.labels);

			this.BarClass.prototype.base = this.scale.endPoint;

			this.eachBars(function(bar, index, datasetIndex){
				helpers.extend(bar, {
					width : this.scale.calculateBarWidth(this.datasets.length),
					x: this.scale.calculateBarX(this.datasets.length, datasetIndex, index),
					y: this.scale.endPoint
				});
				bar.save();
			}, this);

			this.render();
		},
		update : function(){
			this.scale.update();
			// Reset any highlight colours before updating.
			helpers.each(this.activeElements, function(activeElement){
				activeElement.restore(['fillColor', 'strokeColor']);
			});

			this.eachBars(function(bar){
				bar.save();
			});
			this.render();
		},
		eachBars : function(callback){
			helpers.each(this.datasets,function(dataset, datasetIndex){
				helpers.each(dataset.bars, callback, this, datasetIndex);
			},this);
		},
		getBarsAtEvent : function(e){
			var barsArray = [],
				eventPosition = helpers.getRelativePosition(e),
				datasetIterator = function(dataset){
					barsArray.push(dataset.bars[barIndex]);
				},
				barIndex;

			for (var datasetIndex = 0; datasetIndex < this.datasets.length; datasetIndex++) {
				for (barIndex = 0; barIndex < this.datasets[datasetIndex].bars.length; barIndex++) {
					if (this.datasets[datasetIndex].bars[barIndex].inRange(eventPosition.x,eventPosition.y)){
						helpers.each(this.datasets, datasetIterator);
						return barsArray;
					}
				}
			}

			return barsArray;
		},
		buildScale : function(labels){
			var self = this;

			var dataTotal = function(){
				var values = [];
				self.eachBars(function(bar){
					values.push(bar.value);
				});
				return values;
			};

			var scaleOptions = {
				templateString : this.options.scaleLabel,
				height : this.chart.height,
				width : this.chart.width,
				ctx : this.chart.ctx,
				textColor : this.options.scaleFontColor,
				fontSize : this.options.scaleFontSize,
				fontStyle : this.options.scaleFontStyle,
				fontFamily : this.options.scaleFontFamily,
				valuesCount : labels.length,
				beginAtZero : this.options.scaleBeginAtZero,
				integersOnly : this.options.scaleIntegersOnly,
				calculateYRange: function(currentHeight){
					var updatedRanges = helpers.calculateScaleRange(
						dataTotal(),
						currentHeight,
						this.fontSize,
						this.beginAtZero,
						this.integersOnly
					);
					helpers.extend(this, updatedRanges);
				},
				xLabels : labels,
				font : helpers.fontString(this.options.scaleFontSize, this.options.scaleFontStyle, this.options.scaleFontFamily),
				lineWidth : this.options.scaleLineWidth,
				lineColor : this.options.scaleLineColor,
				showHorizontalLines : this.options.scaleShowHorizontalLines,
				showVerticalLines : this.options.scaleShowVerticalLines,
				gridLineWidth : (this.options.scaleShowGridLines) ? this.options.scaleGridLineWidth : 0,
				gridLineColor : (this.options.scaleShowGridLines) ? this.options.scaleGridLineColor : "rgba(0,0,0,0)",
				padding : (this.options.showScale) ? 0 : (this.options.barShowStroke) ? this.options.barStrokeWidth : 0,
				showLabels : this.options.scaleShowLabels,
				display : this.options.showScale
			};

			if (this.options.scaleOverride){
				helpers.extend(scaleOptions, {
					calculateYRange: helpers.noop,
					steps: this.options.scaleSteps,
					stepValue: this.options.scaleStepWidth,
					min: this.options.scaleStartValue,
					max: this.options.scaleStartValue + (this.options.scaleSteps * this.options.scaleStepWidth)
				});
			}

			this.scale = new this.ScaleClass(scaleOptions);
		},
		addData : function(valuesArray,label){
			//Map the values array for each of the datasets
			helpers.each(valuesArray,function(value,datasetIndex){
				//Add a new point for each piece of data, passing any required data to draw.
				this.datasets[datasetIndex].bars.push(new this.BarClass({
					value : value,
					label : label,
					x: this.scale.calculateBarX(this.datasets.length, datasetIndex, this.scale.valuesCount+1),
					y: this.scale.endPoint,
					width : this.scale.calculateBarWidth(this.datasets.length),
					base : this.scale.endPoint,
					strokeColor : this.datasets[datasetIndex].strokeColor,
					fillColor : this.datasets[datasetIndex].fillColor
				}));
			},this);

			this.scale.addXLabel(label);
			//Then re-render the chart.
			this.update();
		},
		removeData : function(){
			this.scale.removeXLabel();
			//Then re-render the chart.
			helpers.each(this.datasets,function(dataset){
				dataset.bars.shift();
			},this);
			this.update();
		},
		reflow : function(){
			helpers.extend(this.BarClass.prototype,{
				y: this.scale.endPoint,
				base : this.scale.endPoint
			});
			var newScaleProps = helpers.extend({
				height : this.chart.height,
				width : this.chart.width
			});
			this.scale.update(newScaleProps);
		},
		draw : function(ease){
			var easingDecimal = ease || 1;
			this.clear();

			var ctx = this.chart.ctx;

			this.scale.draw(easingDecimal);

			//Draw all the bars for each dataset
			helpers.each(this.datasets,function(dataset,datasetIndex){
				helpers.each(dataset.bars,function(bar,index){
					if (bar.hasValue()){
						bar.base = this.scale.endPoint;
						//Transition then draw
						bar.transition({
							x : this.scale.calculateBarX(this.datasets.length, datasetIndex, index),
							y : this.scale.calculateY(bar.value),
							width : this.scale.calculateBarWidth(this.datasets.length)
						}, easingDecimal).draw();
					}
				},this);

			},this);
		}
	});


}).call(this);

(function(){
	"use strict";

	var root = this,
		Chart = root.Chart,
		//Cache a local reference to Chart.helpers
		helpers = Chart.helpers;

	var defaultConfig = {
		//Boolean - Whether we should show a stroke on each segment
		segmentShowStroke : true,

		//String - The colour of each segment stroke
		segmentStrokeColor : "#fff",

		//Number - The width of each segment stroke
		segmentStrokeWidth : 2,

		//The percentage of the chart that we cut out of the middle.
		percentageInnerCutout : 50,

		//Number - Amount of animation steps
		animationSteps : 100,

		//String - Animation easing effect
		animationEasing : "easeOutBounce",

		//Boolean - Whether we animate the rotation of the Doughnut
		animateRotate : true,

		//Boolean - Whether we animate scaling the Doughnut from the centre
		animateScale : false,

		//String - A legend template
		legendTemplate : "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<segments.length; i++){%><li><span style=\"background-color:<%=segments[i].fillColor%>\"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>"

	};


	Chart.Type.extend({
		//Passing in a name registers this chart in the Chart namespace
		name: "Doughnut",
		//Providing a defaults will also register the deafults in the chart namespace
		defaults : defaultConfig,
		//Initialize is fired when the chart is initialized - Data is passed in as a parameter
		//Config is automatically merged by the core of Chart.js, and is available at this.options
		initialize:  function(data){

			//Declare segments as a static property to prevent inheriting across the Chart type prototype
			this.segments = [];
			this.outerRadius = (helpers.min([this.chart.width,this.chart.height]) -	this.options.segmentStrokeWidth/2)/2;

			this.SegmentArc = Chart.Arc.extend({
				ctx : this.chart.ctx,
				x : this.chart.width/2,
				y : this.chart.height/2
			});

			//Set up tooltip events on the chart
			if (this.options.showTooltips){
				helpers.bindEvents(this, this.options.tooltipEvents, function(evt){
					var activeSegments = (evt.type !== 'mouseout') ? this.getSegmentsAtEvent(evt) : [];

					helpers.each(this.segments,function(segment){
						segment.restore(["fillColor"]);
					});
					helpers.each(activeSegments,function(activeSegment){
						activeSegment.fillColor = activeSegment.highlightColor;
					});
					this.showTooltip(activeSegments);
				});
			}
			this.calculateTotal(data);

			helpers.each(data,function(datapoint, index){
				this.addData(datapoint, index, true);
			},this);

			this.render();
		},
		getSegmentsAtEvent : function(e){
			var segmentsArray = [];

			var location = helpers.getRelativePosition(e);

			helpers.each(this.segments,function(segment){
				if (segment.inRange(location.x,location.y)) segmentsArray.push(segment);
			},this);
			return segmentsArray;
		},
		addData : function(segment, atIndex, silent){
			var index = atIndex || this.segments.length;
			this.segments.splice(index, 0, new this.SegmentArc({
				value : segment.value,
				outerRadius : (this.options.animateScale) ? 0 : this.outerRadius,
				innerRadius : (this.options.animateScale) ? 0 : (this.outerRadius/100) * this.options.percentageInnerCutout,
				fillColor : segment.color,
				highlightColor : segment.highlight || segment.color,
				showStroke : this.options.segmentShowStroke,
				strokeWidth : this.options.segmentStrokeWidth,
				strokeColor : this.options.segmentStrokeColor,
				startAngle : Math.PI * 1.5,
				circumference : (this.options.animateRotate) ? 0 : this.calculateCircumference(segment.value),
				label : segment.label
			}));
			if (!silent){
				this.reflow();
				this.update();
			}
		},
		calculateCircumference : function(value){
			return (Math.PI*2)*(Math.abs(value) / this.total);
		},
		calculateTotal : function(data){
			this.total = 0;
			helpers.each(data,function(segment){
				this.total += Math.abs(segment.value);
			},this);
		},
		update : function(){
			this.calculateTotal(this.segments);

			// Reset any highlight colours before updating.
			helpers.each(this.activeElements, function(activeElement){
				activeElement.restore(['fillColor']);
			});

			helpers.each(this.segments,function(segment){
				segment.save();
			});
			this.render();
		},

		removeData: function(atIndex){
			var indexToDelete = (helpers.isNumber(atIndex)) ? atIndex : this.segments.length-1;
			this.segments.splice(indexToDelete, 1);
			this.reflow();
			this.update();
		},

		reflow : function(){
			helpers.extend(this.SegmentArc.prototype,{
				x : this.chart.width/2,
				y : this.chart.height/2
			});
			this.outerRadius = (helpers.min([this.chart.width,this.chart.height]) -	this.options.segmentStrokeWidth/2)/2;
			helpers.each(this.segments, function(segment){
				segment.update({
					outerRadius : this.outerRadius,
					innerRadius : (this.outerRadius/100) * this.options.percentageInnerCutout
				});
			}, this);
		},
		draw : function(easeDecimal){
			var animDecimal = (easeDecimal) ? easeDecimal : 1;
			this.clear();
			helpers.each(this.segments,function(segment,index){
				segment.transition({
					circumference : this.calculateCircumference(segment.value),
					outerRadius : this.outerRadius,
					innerRadius : (this.outerRadius/100) * this.options.percentageInnerCutout
				},animDecimal);

				segment.endAngle = segment.startAngle + segment.circumference;

				segment.draw();
				if (index === 0){
					segment.startAngle = Math.PI * 1.5;
				}
				//Check to see if it's the last segment, if not get the next and update the start angle
				if (index < this.segments.length-1){
					this.segments[index+1].startAngle = segment.endAngle;
				}
			},this);

		}
	});

	Chart.types.Doughnut.extend({
		name : "Pie",
		defaults : helpers.merge(defaultConfig,{percentageInnerCutout : 0})
	});

}).call(this);
(function(){
	"use strict";

	var root = this,
		Chart = root.Chart,
		helpers = Chart.helpers;

	var defaultConfig = {

		///Boolean - Whether grid lines are shown across the chart
		scaleShowGridLines : true,

		//String - Colour of the grid lines
		scaleGridLineColor : "rgba(0,0,0,.05)",

		//Number - Width of the grid lines
		scaleGridLineWidth : 1,

		//Boolean - Whether to show horizontal lines (except X axis)
		scaleShowHorizontalLines: true,

		//Boolean - Whether to show vertical lines (except Y axis)
		scaleShowVerticalLines: true,

		//Boolean - Whether the line is curved between points
		bezierCurve : true,

		//Number - Tension of the bezier curve between points
		bezierCurveTension : 0.4,

		//Boolean - Whether to show a dot for each point
		pointDot : true,

		//Number - Radius of each point dot in pixels
		pointDotRadius : 4,

		//Number - Pixel width of point dot stroke
		pointDotStrokeWidth : 1,

		//Number - amount extra to add to the radius to cater for hit detection outside the drawn point
		pointHitDetectionRadius : 20,

		//Boolean - Whether to show a stroke for datasets
		datasetStroke : true,

		//Number - Pixel width of dataset stroke
		datasetStrokeWidth : 2,

		//Boolean - Whether to fill the dataset with a colour
		datasetFill : true,

		//String - A legend template
		legendTemplate : "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].strokeColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>"

	};


	Chart.Type.extend({
		name: "Line",
		defaults : defaultConfig,
		initialize:  function(data){
			//Declare the extension of the default point, to cater for the options passed in to the constructor
			this.PointClass = Chart.Point.extend({
				strokeWidth : this.options.pointDotStrokeWidth,
				radius : this.options.pointDotRadius,
				display: this.options.pointDot,
				hitDetectionRadius : this.options.pointHitDetectionRadius,
				ctx : this.chart.ctx,
				inRange : function(mouseX){
					return (Math.pow(mouseX-this.x, 2) < Math.pow(this.radius + this.hitDetectionRadius,2));
				}
			});

			this.datasets = [];

			//Set up tooltip events on the chart
			if (this.options.showTooltips){
				helpers.bindEvents(this, this.options.tooltipEvents, function(evt){
					var activePoints = (evt.type !== 'mouseout') ? this.getPointsAtEvent(evt) : [];
					this.eachPoints(function(point){
						point.restore(['fillColor', 'strokeColor']);
					});
					helpers.each(activePoints, function(activePoint){
						activePoint.fillColor = activePoint.highlightFill;
						activePoint.strokeColor = activePoint.highlightStroke;
					});
					this.showTooltip(activePoints);
				});
			}

			//Iterate through each of the datasets, and build this into a property of the chart
			helpers.each(data.datasets,function(dataset){

				var datasetObject = {
					label : dataset.label || null,
					fillColor : dataset.fillColor,
					strokeColor : dataset.strokeColor,
					pointColor : dataset.pointColor,
					pointStrokeColor : dataset.pointStrokeColor,
					points : []
				};

				this.datasets.push(datasetObject);


				helpers.each(dataset.data,function(dataPoint,index){
					//Add a new point for each piece of data, passing any required data to draw.
					datasetObject.points.push(new this.PointClass({
						value : dataPoint,
						label : data.labels[index],
						datasetLabel: dataset.label,
						strokeColor : dataset.pointStrokeColor,
						fillColor : dataset.pointColor,
						highlightFill : dataset.pointHighlightFill || dataset.pointColor,
						highlightStroke : dataset.pointHighlightStroke || dataset.pointStrokeColor
					}));
				},this);

				this.buildScale(data.labels);


				this.eachPoints(function(point, index){
					helpers.extend(point, {
						x: this.scale.calculateX(index),
						y: this.scale.endPoint
					});
					point.save();
				}, this);

			},this);


			this.render();
		},
		update : function(){
			this.scale.update();
			// Reset any highlight colours before updating.
			helpers.each(this.activeElements, function(activeElement){
				activeElement.restore(['fillColor', 'strokeColor']);
			});
			this.eachPoints(function(point){
				point.save();
			});
			this.render();
		},
		eachPoints : function(callback){
			helpers.each(this.datasets,function(dataset){
				helpers.each(dataset.points,callback,this);
			},this);
		},
		getPointsAtEvent : function(e){
			var pointsArray = [],
				eventPosition = helpers.getRelativePosition(e);
			helpers.each(this.datasets,function(dataset){
				helpers.each(dataset.points,function(point){
					if (point.inRange(eventPosition.x,eventPosition.y)) pointsArray.push(point);
				});
			},this);
			return pointsArray;
		},
		buildScale : function(labels){
			var self = this;

			var dataTotal = function(){
				var values = [];
				self.eachPoints(function(point){
					values.push(point.value);
				});

				return values;
			};

			var scaleOptions = {
				templateString : this.options.scaleLabel,
				height : this.chart.height,
				width : this.chart.width,
				ctx : this.chart.ctx,
				textColor : this.options.scaleFontColor,
				fontSize : this.options.scaleFontSize,
				fontStyle : this.options.scaleFontStyle,
				fontFamily : this.options.scaleFontFamily,
				valuesCount : labels.length,
				beginAtZero : this.options.scaleBeginAtZero,
				integersOnly : this.options.scaleIntegersOnly,
				calculateYRange : function(currentHeight){
					var updatedRanges = helpers.calculateScaleRange(
						dataTotal(),
						currentHeight,
						this.fontSize,
						this.beginAtZero,
						this.integersOnly
					);
					helpers.extend(this, updatedRanges);
				},
				xLabels : labels,
				font : helpers.fontString(this.options.scaleFontSize, this.options.scaleFontStyle, this.options.scaleFontFamily),
				lineWidth : this.options.scaleLineWidth,
				lineColor : this.options.scaleLineColor,
				showHorizontalLines : this.options.scaleShowHorizontalLines,
				showVerticalLines : this.options.scaleShowVerticalLines,
				gridLineWidth : (this.options.scaleShowGridLines) ? this.options.scaleGridLineWidth : 0,
				gridLineColor : (this.options.scaleShowGridLines) ? this.options.scaleGridLineColor : "rgba(0,0,0,0)",
				padding: (this.options.showScale) ? 0 : this.options.pointDotRadius + this.options.pointDotStrokeWidth,
				showLabels : this.options.scaleShowLabels,
				display : this.options.showScale
			};

			if (this.options.scaleOverride){
				helpers.extend(scaleOptions, {
					calculateYRange: helpers.noop,
					steps: this.options.scaleSteps,
					stepValue: this.options.scaleStepWidth,
					min: this.options.scaleStartValue,
					max: this.options.scaleStartValue + (this.options.scaleSteps * this.options.scaleStepWidth)
				});
			}


			this.scale = new Chart.Scale(scaleOptions);
		},
		addData : function(valuesArray,label){
			//Map the values array for each of the datasets

			helpers.each(valuesArray,function(value,datasetIndex){
				//Add a new point for each piece of data, passing any required data to draw.
				this.datasets[datasetIndex].points.push(new this.PointClass({
					value : value,
					label : label,
					x: this.scale.calculateX(this.scale.valuesCount+1),
					y: this.scale.endPoint,
					strokeColor : this.datasets[datasetIndex].pointStrokeColor,
					fillColor : this.datasets[datasetIndex].pointColor
				}));
			},this);

			this.scale.addXLabel(label);
			//Then re-render the chart.
			this.update();
		},
		removeData : function(){
			this.scale.removeXLabel();
			//Then re-render the chart.
			helpers.each(this.datasets,function(dataset){
				dataset.points.shift();
			},this);
			this.update();
		},
		reflow : function(){
			var newScaleProps = helpers.extend({
				height : this.chart.height,
				width : this.chart.width
			});
			this.scale.update(newScaleProps);
		},
		draw : function(ease){
			var easingDecimal = ease || 1;
			this.clear();

			var ctx = this.chart.ctx;

			// Some helper methods for getting the next/prev points
			var hasValue = function(item){
				return item.value !== null;
			},
			nextPoint = function(point, collection, index){
				return helpers.findNextWhere(collection, hasValue, index) || point;
			},
			previousPoint = function(point, collection, index){
				return helpers.findPreviousWhere(collection, hasValue, index) || point;
			};

			this.scale.draw(easingDecimal);


			helpers.each(this.datasets,function(dataset){
				var pointsWithValues = helpers.where(dataset.points, hasValue);

				//Transition each point first so that the line and point drawing isn't out of sync
				//We can use this extra loop to calculate the control points of this dataset also in this loop

				helpers.each(dataset.points, function(point, index){
					if (point.hasValue()){
						point.transition({
							y : this.scale.calculateY(point.value),
							x : this.scale.calculateX(index)
						}, easingDecimal);
					}
				},this);


				// Control points need to be calculated in a seperate loop, because we need to know the current x/y of the point
				// This would cause issues when there is no animation, because the y of the next point would be 0, so beziers would be skewed
				if (this.options.bezierCurve){
					helpers.each(pointsWithValues, function(point, index){
						var tension = (index > 0 && index < pointsWithValues.length - 1) ? this.options.bezierCurveTension : 0;
						point.controlPoints = helpers.splineCurve(
							previousPoint(point, pointsWithValues, index),
							point,
							nextPoint(point, pointsWithValues, index),
							tension
						);

						// Prevent the bezier going outside of the bounds of the graph

						// Cap puter bezier handles to the upper/lower scale bounds
						if (point.controlPoints.outer.y > this.scale.endPoint){
							point.controlPoints.outer.y = this.scale.endPoint;
						}
						else if (point.controlPoints.outer.y < this.scale.startPoint){
							point.controlPoints.outer.y = this.scale.startPoint;
						}

						// Cap inner bezier handles to the upper/lower scale bounds
						if (point.controlPoints.inner.y > this.scale.endPoint){
							point.controlPoints.inner.y = this.scale.endPoint;
						}
						else if (point.controlPoints.inner.y < this.scale.startPoint){
							point.controlPoints.inner.y = this.scale.startPoint;
						}
					},this);
				}


				//Draw the line between all the points
				ctx.lineWidth = this.options.datasetStrokeWidth;
				ctx.strokeStyle = dataset.strokeColor;
				ctx.beginPath();

				helpers.each(pointsWithValues, function(point, index){
					if (index === 0){
						ctx.moveTo(point.x, point.y);
					}
					else{
						if(this.options.bezierCurve){
							var previous = previousPoint(point, pointsWithValues, index);

							ctx.bezierCurveTo(
								previous.controlPoints.outer.x,
								previous.controlPoints.outer.y,
								point.controlPoints.inner.x,
								point.controlPoints.inner.y,
								point.x,
								point.y
							);
						}
						else{
							ctx.lineTo(point.x,point.y);
						}
					}
				}, this);

				ctx.stroke();

				if (this.options.datasetFill && pointsWithValues.length > 0){
					//Round off the line by going to the base of the chart, back to the start, then fill.
					ctx.lineTo(pointsWithValues[pointsWithValues.length - 1].x, this.scale.endPoint);
					ctx.lineTo(pointsWithValues[0].x, this.scale.endPoint);
					ctx.fillStyle = dataset.fillColor;
					ctx.closePath();
					ctx.fill();
				}

				//Now draw the points over the line
				//A little inefficient double looping, but better than the line
				//lagging behind the point positions
				helpers.each(pointsWithValues,function(point){
					point.draw();
				});
			},this);
		}
	});


}).call(this);

(function(){
	"use strict";

	var root = this,
		Chart = root.Chart,
		//Cache a local reference to Chart.helpers
		helpers = Chart.helpers;

	var defaultConfig = {
		//Boolean - Show a backdrop to the scale label
		scaleShowLabelBackdrop : true,

		//String - The colour of the label backdrop
		scaleBackdropColor : "rgba(255,255,255,0.75)",

		// Boolean - Whether the scale should begin at zero
		scaleBeginAtZero : true,

		//Number - The backdrop padding above & below the label in pixels
		scaleBackdropPaddingY : 2,

		//Number - The backdrop padding to the side of the label in pixels
		scaleBackdropPaddingX : 2,

		//Boolean - Show line for each value in the scale
		scaleShowLine : true,

		//Boolean - Stroke a line around each segment in the chart
		segmentShowStroke : true,

		//String - The colour of the stroke on each segement.
		segmentStrokeColor : "#fff",

		//Number - The width of the stroke value in pixels
		segmentStrokeWidth : 2,

		//Number - Amount of animation steps
		animationSteps : 100,

		//String - Animation easing effect.
		animationEasing : "easeOutBounce",

		//Boolean - Whether to animate the rotation of the chart
		animateRotate : true,

		//Boolean - Whether to animate scaling the chart from the centre
		animateScale : false,

		//String - A legend template
		legendTemplate : "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<segments.length; i++){%><li><span style=\"background-color:<%=segments[i].fillColor%>\"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>"
	};


	Chart.Type.extend({
		//Passing in a name registers this chart in the Chart namespace
		name: "PolarArea",
		//Providing a defaults will also register the deafults in the chart namespace
		defaults : defaultConfig,
		//Initialize is fired when the chart is initialized - Data is passed in as a parameter
		//Config is automatically merged by the core of Chart.js, and is available at this.options
		initialize:  function(data){
			this.segments = [];
			//Declare segment class as a chart instance specific class, so it can share props for this instance
			this.SegmentArc = Chart.Arc.extend({
				showStroke : this.options.segmentShowStroke,
				strokeWidth : this.options.segmentStrokeWidth,
				strokeColor : this.options.segmentStrokeColor,
				ctx : this.chart.ctx,
				innerRadius : 0,
				x : this.chart.width/2,
				y : this.chart.height/2
			});
			this.scale = new Chart.RadialScale({
				display: this.options.showScale,
				fontStyle: this.options.scaleFontStyle,
				fontSize: this.options.scaleFontSize,
				fontFamily: this.options.scaleFontFamily,
				fontColor: this.options.scaleFontColor,
				showLabels: this.options.scaleShowLabels,
				showLabelBackdrop: this.options.scaleShowLabelBackdrop,
				backdropColor: this.options.scaleBackdropColor,
				backdropPaddingY : this.options.scaleBackdropPaddingY,
				backdropPaddingX: this.options.scaleBackdropPaddingX,
				lineWidth: (this.options.scaleShowLine) ? this.options.scaleLineWidth : 0,
				lineColor: this.options.scaleLineColor,
				lineArc: true,
				width: this.chart.width,
				height: this.chart.height,
				xCenter: this.chart.width/2,
				yCenter: this.chart.height/2,
				ctx : this.chart.ctx,
				templateString: this.options.scaleLabel,
				valuesCount: data.length
			});

			this.updateScaleRange(data);

			this.scale.update();

			helpers.each(data,function(segment,index){
				this.addData(segment,index,true);
			},this);

			//Set up tooltip events on the chart
			if (this.options.showTooltips){
				helpers.bindEvents(this, this.options.tooltipEvents, function(evt){
					var activeSegments = (evt.type !== 'mouseout') ? this.getSegmentsAtEvent(evt) : [];
					helpers.each(this.segments,function(segment){
						segment.restore(["fillColor"]);
					});
					helpers.each(activeSegments,function(activeSegment){
						activeSegment.fillColor = activeSegment.highlightColor;
					});
					this.showTooltip(activeSegments);
				});
			}

			this.render();
		},
		getSegmentsAtEvent : function(e){
			var segmentsArray = [];

			var location = helpers.getRelativePosition(e);

			helpers.each(this.segments,function(segment){
				if (segment.inRange(location.x,location.y)) segmentsArray.push(segment);
			},this);
			return segmentsArray;
		},
		addData : function(segment, atIndex, silent){
			var index = atIndex || this.segments.length;

			this.segments.splice(index, 0, new this.SegmentArc({
				fillColor: segment.color,
				highlightColor: segment.highlight || segment.color,
				label: segment.label,
				value: segment.value,
				outerRadius: (this.options.animateScale) ? 0 : this.scale.calculateCenterOffset(segment.value),
				circumference: (this.options.animateRotate) ? 0 : this.scale.getCircumference(),
				startAngle: Math.PI * 1.5
			}));
			if (!silent){
				this.reflow();
				this.update();
			}
		},
		removeData: function(atIndex){
			var indexToDelete = (helpers.isNumber(atIndex)) ? atIndex : this.segments.length-1;
			this.segments.splice(indexToDelete, 1);
			this.reflow();
			this.update();
		},
		calculateTotal: function(data){
			this.total = 0;
			helpers.each(data,function(segment){
				this.total += segment.value;
			},this);
			this.scale.valuesCount = this.segments.length;
		},
		updateScaleRange: function(datapoints){
			var valuesArray = [];
			helpers.each(datapoints,function(segment){
				valuesArray.push(segment.value);
			});

			var scaleSizes = (this.options.scaleOverride) ?
				{
					steps: this.options.scaleSteps,
					stepValue: this.options.scaleStepWidth,
					min: this.options.scaleStartValue,
					max: this.options.scaleStartValue + (this.options.scaleSteps * this.options.scaleStepWidth)
				} :
				helpers.calculateScaleRange(
					valuesArray,
					helpers.min([this.chart.width, this.chart.height])/2,
					this.options.scaleFontSize,
					this.options.scaleBeginAtZero,
					this.options.scaleIntegersOnly
				);

			helpers.extend(
				this.scale,
				scaleSizes,
				{
					size: helpers.min([this.chart.width, this.chart.height]),
					xCenter: this.chart.width/2,
					yCenter: this.chart.height/2
				}
			);

		},
		update : function(){
			this.calculateTotal(this.segments);

			helpers.each(this.segments,function(segment){
				segment.save();
			});
			
			this.reflow();
			this.render();
		},
		reflow : function(){
			helpers.extend(this.SegmentArc.prototype,{
				x : this.chart.width/2,
				y : this.chart.height/2
			});
			this.updateScaleRange(this.segments);
			this.scale.update();

			helpers.extend(this.scale,{
				xCenter: this.chart.width/2,
				yCenter: this.chart.height/2
			});

			helpers.each(this.segments, function(segment){
				segment.update({
					outerRadius : this.scale.calculateCenterOffset(segment.value)
				});
			}, this);

		},
		draw : function(ease){
			var easingDecimal = ease || 1;
			//Clear & draw the canvas
			this.clear();
			helpers.each(this.segments,function(segment, index){
				segment.transition({
					circumference : this.scale.getCircumference(),
					outerRadius : this.scale.calculateCenterOffset(segment.value)
				},easingDecimal);

				segment.endAngle = segment.startAngle + segment.circumference;

				// If we've removed the first segment we need to set the first one to
				// start at the top.
				if (index === 0){
					segment.startAngle = Math.PI * 1.5;
				}

				//Check to see if it's the last segment, if not get the next and update the start angle
				if (index < this.segments.length - 1){
					this.segments[index+1].startAngle = segment.endAngle;
				}
				segment.draw();
			}, this);
			this.scale.draw();
		}
	});

}).call(this);
(function(){
	"use strict";

	var root = this,
		Chart = root.Chart,
		helpers = Chart.helpers;



	Chart.Type.extend({
		name: "Radar",
		defaults:{
			//Boolean - Whether to show lines for each scale point
			scaleShowLine : true,

			//Boolean - Whether we show the angle lines out of the radar
			angleShowLineOut : true,

			//Boolean - Whether to show labels on the scale
			scaleShowLabels : false,

			// Boolean - Whether the scale should begin at zero
			scaleBeginAtZero : true,

			//String - Colour of the angle line
			angleLineColor : "rgba(0,0,0,.1)",

			//Number - Pixel width of the angle line
			angleLineWidth : 1,

			//String - Point label font declaration
			pointLabelFontFamily : "'Arial'",

			//String - Point label font weight
			pointLabelFontStyle : "normal",

			//Number - Point label font size in pixels
			pointLabelFontSize : 10,

			//String - Point label font colour
			pointLabelFontColor : "#666",

			//Boolean - Whether to show a dot for each point
			pointDot : true,

			//Number - Radius of each point dot in pixels
			pointDotRadius : 3,

			//Number - Pixel width of point dot stroke
			pointDotStrokeWidth : 1,

			//Number - amount extra to add to the radius to cater for hit detection outside the drawn point
			pointHitDetectionRadius : 20,

			//Boolean - Whether to show a stroke for datasets
			datasetStroke : true,

			//Number - Pixel width of dataset stroke
			datasetStrokeWidth : 2,

			//Boolean - Whether to fill the dataset with a colour
			datasetFill : true,

			//String - A legend template
			legendTemplate : "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].strokeColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>"

		},

		initialize: function(data){
			this.PointClass = Chart.Point.extend({
				strokeWidth : this.options.pointDotStrokeWidth,
				radius : this.options.pointDotRadius,
				display: this.options.pointDot,
				hitDetectionRadius : this.options.pointHitDetectionRadius,
				ctx : this.chart.ctx
			});

			this.datasets = [];

			this.buildScale(data);

			//Set up tooltip events on the chart
			if (this.options.showTooltips){
				helpers.bindEvents(this, this.options.tooltipEvents, function(evt){
					var activePointsCollection = (evt.type !== 'mouseout') ? this.getPointsAtEvent(evt) : [];

					this.eachPoints(function(point){
						point.restore(['fillColor', 'strokeColor']);
					});
					helpers.each(activePointsCollection, function(activePoint){
						activePoint.fillColor = activePoint.highlightFill;
						activePoint.strokeColor = activePoint.highlightStroke;
					});

					this.showTooltip(activePointsCollection);
				});
			}

			//Iterate through each of the datasets, and build this into a property of the chart
			helpers.each(data.datasets,function(dataset){

				var datasetObject = {
					label: dataset.label || null,
					fillColor : dataset.fillColor,
					strokeColor : dataset.strokeColor,
					pointColor : dataset.pointColor,
					pointStrokeColor : dataset.pointStrokeColor,
					points : []
				};

				this.datasets.push(datasetObject);

				helpers.each(dataset.data,function(dataPoint,index){
					//Add a new point for each piece of data, passing any required data to draw.
					var pointPosition;
					if (!this.scale.animation){
						pointPosition = this.scale.getPointPosition(index, this.scale.calculateCenterOffset(dataPoint));
					}
					datasetObject.points.push(new this.PointClass({
						value : dataPoint,
						label : data.labels[index],
						datasetLabel: dataset.label,
						x: (this.options.animation) ? this.scale.xCenter : pointPosition.x,
						y: (this.options.animation) ? this.scale.yCenter : pointPosition.y,
						strokeColor : dataset.pointStrokeColor,
						fillColor : dataset.pointColor,
						highlightFill : dataset.pointHighlightFill || dataset.pointColor,
						highlightStroke : dataset.pointHighlightStroke || dataset.pointStrokeColor
					}));
				},this);

			},this);

			this.render();
		},
		eachPoints : function(callback){
			helpers.each(this.datasets,function(dataset){
				helpers.each(dataset.points,callback,this);
			},this);
		},

		getPointsAtEvent : function(evt){
			var mousePosition = helpers.getRelativePosition(evt),
				fromCenter = helpers.getAngleFromPoint({
					x: this.scale.xCenter,
					y: this.scale.yCenter
				}, mousePosition);

			var anglePerIndex = (Math.PI * 2) /this.scale.valuesCount,
				pointIndex = Math.round((fromCenter.angle - Math.PI * 1.5) / anglePerIndex),
				activePointsCollection = [];

			// If we're at the top, make the pointIndex 0 to get the first of the array.
			if (pointIndex >= this.scale.valuesCount || pointIndex < 0){
				pointIndex = 0;
			}

			if (fromCenter.distance <= this.scale.drawingArea){
				helpers.each(this.datasets, function(dataset){
					activePointsCollection.push(dataset.points[pointIndex]);
				});
			}

			return activePointsCollection;
		},

		buildScale : function(data){
			this.scale = new Chart.RadialScale({
				display: this.options.showScale,
				fontStyle: this.options.scaleFontStyle,
				fontSize: this.options.scaleFontSize,
				fontFamily: this.options.scaleFontFamily,
				fontColor: this.options.scaleFontColor,
				showLabels: this.options.scaleShowLabels,
				showLabelBackdrop: this.options.scaleShowLabelBackdrop,
				backdropColor: this.options.scaleBackdropColor,
				backdropPaddingY : this.options.scaleBackdropPaddingY,
				backdropPaddingX: this.options.scaleBackdropPaddingX,
				lineWidth: (this.options.scaleShowLine) ? this.options.scaleLineWidth : 0,
				lineColor: this.options.scaleLineColor,
				angleLineColor : this.options.angleLineColor,
				angleLineWidth : (this.options.angleShowLineOut) ? this.options.angleLineWidth : 0,
				// Point labels at the edge of each line
				pointLabelFontColor : this.options.pointLabelFontColor,
				pointLabelFontSize : this.options.pointLabelFontSize,
				pointLabelFontFamily : this.options.pointLabelFontFamily,
				pointLabelFontStyle : this.options.pointLabelFontStyle,
				height : this.chart.height,
				width: this.chart.width,
				xCenter: this.chart.width/2,
				yCenter: this.chart.height/2,
				ctx : this.chart.ctx,
				templateString: this.options.scaleLabel,
				labels: data.labels,
				valuesCount: data.datasets[0].data.length
			});

			this.scale.setScaleSize();
			this.updateScaleRange(data.datasets);
			this.scale.buildYLabels();
		},
		updateScaleRange: function(datasets){
			var valuesArray = (function(){
				var totalDataArray = [];
				helpers.each(datasets,function(dataset){
					if (dataset.data){
						totalDataArray = totalDataArray.concat(dataset.data);
					}
					else {
						helpers.each(dataset.points, function(point){
							totalDataArray.push(point.value);
						});
					}
				});
				return totalDataArray;
			})();


			var scaleSizes = (this.options.scaleOverride) ?
				{
					steps: this.options.scaleSteps,
					stepValue: this.options.scaleStepWidth,
					min: this.options.scaleStartValue,
					max: this.options.scaleStartValue + (this.options.scaleSteps * this.options.scaleStepWidth)
				} :
				helpers.calculateScaleRange(
					valuesArray,
					helpers.min([this.chart.width, this.chart.height])/2,
					this.options.scaleFontSize,
					this.options.scaleBeginAtZero,
					this.options.scaleIntegersOnly
				);

			helpers.extend(
				this.scale,
				scaleSizes
			);

		},
		addData : function(valuesArray,label){
			//Map the values array for each of the datasets
			this.scale.valuesCount++;
			helpers.each(valuesArray,function(value,datasetIndex){
				var pointPosition = this.scale.getPointPosition(this.scale.valuesCount, this.scale.calculateCenterOffset(value));
				this.datasets[datasetIndex].points.push(new this.PointClass({
					value : value,
					label : label,
					x: pointPosition.x,
					y: pointPosition.y,
					strokeColor : this.datasets[datasetIndex].pointStrokeColor,
					fillColor : this.datasets[datasetIndex].pointColor
				}));
			},this);

			this.scale.labels.push(label);

			this.reflow();

			this.update();
		},
		removeData : function(){
			this.scale.valuesCount--;
			this.scale.labels.shift();
			helpers.each(this.datasets,function(dataset){
				dataset.points.shift();
			},this);
			this.reflow();
			this.update();
		},
		update : function(){
			this.eachPoints(function(point){
				point.save();
			});
			this.reflow();
			this.render();
		},
		reflow: function(){
			helpers.extend(this.scale, {
				width : this.chart.width,
				height: this.chart.height,
				size : helpers.min([this.chart.width, this.chart.height]),
				xCenter: this.chart.width/2,
				yCenter: this.chart.height/2
			});
			this.updateScaleRange(this.datasets);
			this.scale.setScaleSize();
			this.scale.buildYLabels();
		},
		draw : function(ease){
			var easeDecimal = ease || 1,
				ctx = this.chart.ctx;
			this.clear();
			this.scale.draw();

			helpers.each(this.datasets,function(dataset){

				//Transition each point first so that the line and point drawing isn't out of sync
				helpers.each(dataset.points,function(point,index){
					if (point.hasValue()){
						point.transition(this.scale.getPointPosition(index, this.scale.calculateCenterOffset(point.value)), easeDecimal);
					}
				},this);



				//Draw the line between all the points
				ctx.lineWidth = this.options.datasetStrokeWidth;
				ctx.strokeStyle = dataset.strokeColor;
				ctx.beginPath();
				helpers.each(dataset.points,function(point,index){
					if (index === 0){
						ctx.moveTo(point.x,point.y);
					}
					else{
						ctx.lineTo(point.x,point.y);
					}
				},this);
				ctx.closePath();
				ctx.stroke();

				ctx.fillStyle = dataset.fillColor;
				ctx.fill();

				//Now draw the points over the line
				//A little inefficient double looping, but better than the line
				//lagging behind the point positions
				helpers.each(dataset.points,function(point){
					if (point.hasValue()){
						point.draw();
					}
				});

			},this);

		}

	});





}).call(this);
/*
 * metismenu - v1.1.3
 * Easy menu jQuery plugin for Twitter Bootstrap 3
 * https://github.com/onokumus/metisMenu
 *
 * Made by Osman Nuri Okumus
 * Under MIT License
 */
;(function($, window, document, undefined) {

    var pluginName = "metisMenu",
        defaults = {
            toggle: true,
            doubleTapToGo: false
        };

    function Plugin(element, options) {
        this.element = $(element);
        this.settings = $.extend({}, defaults, options);
        this._defaults = defaults;
        this._name = pluginName;
        this.init();
    }

    Plugin.prototype = {
        init: function() {

            var $this = this.element,
                $toggle = this.settings.toggle,
                obj = this;

            if (this.isIE() <= 9) {
                $this.find("li.active").has("ul").children("ul").collapse("show");
                $this.find("li").not(".active").has("ul").children("ul").collapse("hide");
            } else {
                $this.find("li.active").has("ul").children("ul").addClass("collapse in");
                $this.find("li").not(".active").has("ul").children("ul").addClass("collapse");
            }

            //add the "doubleTapToGo" class to active items if needed
            if (obj.settings.doubleTapToGo) {
                $this.find("li.active").has("ul").children("a").addClass("doubleTapToGo");
            }

            $this.find("li").has("ul").children("a").on("click" + "." + pluginName, function(e) {
                e.preventDefault();

                //Do we need to enable the double tap
                if (obj.settings.doubleTapToGo) {

                    //if we hit a second time on the link and the href is valid, navigate to that url
                    if (obj.doubleTapToGo($(this)) && $(this).attr("href") !== "#" && $(this).attr("href") !== "") {
                        e.stopPropagation();
                        document.location = $(this).attr("href");
                        return;
                    }
                }

                $(this).parent("li").toggleClass("active").children("ul").collapse("toggle");

                if ($toggle) {
                    $(this).parent("li").siblings().removeClass("active").children("ul.in").collapse("hide");
                }

            });
        },

        isIE: function() { //https://gist.github.com/padolsey/527683
            var undef,
                v = 3,
                div = document.createElement("div"),
                all = div.getElementsByTagName("i");

            while (
                div.innerHTML = "<!--[if gt IE " + (++v) + "]><i></i><![endif]-->",
                all[0]
            ) {
                return v > 4 ? v : undef;
            }
        },

        //Enable the link on the second click.
        doubleTapToGo: function(elem) {
            var $this = this.element;

            //if the class "doubleTapToGo" exists, remove it and return
            if (elem.hasClass("doubleTapToGo")) {
                elem.removeClass("doubleTapToGo");
                return true;
            }

            //does not exists, add a new class and return false
            if (elem.parent().children("ul").length) {
                 //first remove all other class
                $this.find(".doubleTapToGo").removeClass("doubleTapToGo");
                //add the class on the current element
                elem.addClass("doubleTapToGo");
                return false;
            }
        },

        remove: function() {
            this.element.off("." + pluginName);
            this.element.removeData(pluginName);
        }

    };

    $.fn[pluginName] = function(options) {
        this.each(function () {
            var el = $(this);
            if (el.data(pluginName)) {
                el.data(pluginName).remove();
            }
            el.data(pluginName, new Plugin(this, options));
        });
        return this;
    };

})(jQuery, window, document);
/**
 * @preserve Jodit v2.5.62
 * @homepage http://xdsoft.net/jodit/
 * @author Chupurnov Valeriy (<chupurnov@gmail.com>)
 * @license Free for Non-Commercial http://xdsoft.net/jodit/license.html
 */
!function(a,b){"use strict";a.Jodit=b()}(this,function(){"use strict";function a(d,e,f,g){b=a.modules.Dom;var h,i,j=this,k=!!d&&b(d).eq(0);return c+=1,k&&k.length&&(k.data("jodit")?j=k.data("jodit"):j.init(k[0],a.extendOptions(!0,{},a.defaultOptions,b.isPlainObject(e)?e:{})),"string"==typeof e&&j[e]&&b.isFunction(j[e])&&(h=j[e](f,g),void 0!==h&&(i=h))),i}var b,c=1,d=0,e=[],f="2.5.62";return a.prototype.getDocument=function(){return this.iframe&&this.iframe.contentWindow?this.iframe.contentWindow.document:this.editor.ownerDocument||this.editor.document},a.prototype.getWindow=function(){var a=this.getDocument();return a.defaultView||a.parentWindow},a.prototype.execCommand=function(b,c,d){var e,f=this;if(f.events.fire("beforeCommand",[b,c,d])===!1)return!1;switch(b){case"applyCSSProperty":f.applyCSSProperty(d,c);break;case"fontSize":f.applyCSSProperty("font-size",d+"px");break;case"formatBlock":d=/h([1-6])/.test(c)?f.browser("msie")?"Heading "+/h([1-6])/.exec(c)[1]:c:"<"+c+">",f.getDocument().execCommand(b.toLowerCase(),!1,d);break;default:f.getDocument().execCommand(b.toLowerCase(),c||!1,d||null),e=f.$editor.find(Object.keys(a.OLDTAG).join(",")),e.length&&(f.selection.save(),e.each(function(){f.node.replace(this,a.OLDTAG[this.tagName],!0)}),this.selection.restore())}this.events.fire("afterCommand",[b,c,d])},a.prototype.applyCSSProperty=function(c,d){var e,f=this,g=b('<span style="'+c+": "+d+';">'+a.INVISIBLE_SPACE+"</span>",f.getDocument()).get(0);this.selection.isCollapsed()?(this.selection.insertNode(g),f.selection.focusTo(g)):(f.selection.save(),f.getDocument().execCommand("fontSize",!1,4),f.$editor.find("font").each(function(){e=g.cloneNode(!0),f.node.replace(this,e)}),f.selection.restore())},a.prototype.getJodit=function(){return this},a.prototype.getMode=function(){return this.mode},a.prototype.getRealMode=function(){return this.mode!==a.MODE_SPLIT?this.mode:document.activeElement&&"TEXTAREA"===document.activeElement.tagName?a.MODE_TEXTAREA:a.MODE_WYSIWYG},a.prototype.setMode=function(c){var d={mode:c},f=this,g=["jodit_wysiwyg_mode","jodit_area_mode","jodit_split_mode"],h=f.mode;f.events.fire("beforeSetMode",[d])!==!1&&(f.syncCode(!0),f.mode=b.inArray(d.mode,[a.MODE_TEXTAREA,a.MODE_WYSIWYG,a.MODE_SPLIT])!==-1?d.mode:a.MODE_WYSIWYG,f.options.saveModeInCookie&&f.cookie.set("jodit_default_mode",f.mode,31),f.$toolbar.children().each(function(c,d){var e,g=d.control;e=void 0===g||void 0===g.mode?a.MODE_WYSIWYG:g.mode,b(d).toggleClass("disabled",e!==a.MODE_SPLIT&&e!==f.mode)}),f.$wysiwyg.removeClass(g).addClass(g[f.mode-1]),e[e.length]=setTimeout(function(){f.getMode()!==a.MODE_WYSIWYG||h!==a.MODE_TEXTAREA&&!f.options.autofocus?f.getMode()!==a.MODE_TEXTAREA||h!==a.MODE_WYSIWYG&&!f.options.autofocus||f.$area[0].focus():(f.$editor.focus(),f.$editor.find("p").length?f.selection.moveCursorTo(f.$editor.find("p")[0]):f.selection.moveCursorTo(f.editor))},300),f.mode!==a.MODE_TEXTAREA?f.observer&&f.observer.changed():f.observerarea&&f.observerarea.changed(),f.events.fire("afterSetMode"))},a.prototype.toggleMode=function(){var c=this.getMode();b.inArray(c+1,[a.MODE_TEXTAREA,a.MODE_WYSIWYG,this.options.useSplitMode?a.MODE_SPLIT:9])!==-1?c+=1:c=a.MODE_WYSIWYG,this.setMode(c)},a.prototype.val=function(a){return void 0===a?this.getElementValue():(this.events.fire("beforeSetValue",a),a=this.setElementValue(a),this.setEditorValue(a),this.setAreaValue(a),void this.events.fire("afterSetValue"))},a.prototype.syncCode=function(b){var c=this,f=function(){var b,d=c.getRealMode();c.events.fire("beforeSyncCode")!==!1&&(b=d===a.MODE_WYSIWYG?c.getEditorValue():c.getAreaValue(),c.setElementValue(b),b=c.val(),d===a.MODE_WYSIWYG?c.setAreaValue(b):c.setEditorValue(b),!c.val()||""===c.helper.clear(c.$editor.text())&&!c.$editor.find("img,table,hr,button,input,ul,ol").length?c.placeholder.show():(c.placeholder.hide(),c.observe()),c.events.fire("afterSyncCode"))};b||0===c.options.syncCodeTimeout?f():(clearTimeout(d),d=setTimeout(f,c.options.syncCodeTimeout),e[e.length]=d)},a.prototype.checkTargets=function(a){var c=this;c.$toolbar.find("li").removeClass("active"),b.each(c.options.controls,function(d,e){var f,g,h,i,j=d||"empty",k=function(a){var d=0,e=0;b.each(a,function(a,f){b.isFunction(f)?f.apply(c,[i.css(a).toString().toLowerCase(),c])&&(d+=1):i.css(a).toString().toLowerCase()===f&&(d+=1),e+=1}),e===d&&c.$toolbar.find(".toolbar-"+j).addClass("active")};if(e.tags||e.options&&e.options.tags)for(f=e.tags||e.options&&e.options.tags,g=a;g&&!b(g).hasClass("jodit_editor")&&1===g.nodeType;)b.inArray(g.tagName.toLowerCase(),f)!==-1&&c.$toolbar.find(".toolbar-"+j).addClass("active"),g=g.parentNode;if(e.css||e.options&&e.options.css)for(h=e.css||e.options&&e.options.css,i=b(a);i&&i[0]&&1===i[0].nodeType&&!i.hasClass("jodit_editor");)k(h),i=i.parent()})},a.prototype.observe=function(){if(!a.prototype.observe.work){a.prototype.observe.work=!0;var c=this;c.observer.block(1),c.$editor.find("img, table, iframe").each(function(d,e){var f;c.browser("msie")&&b(e).attr("unselectable","on"),c.resizer&&("IFRAME"===e.tagName&&c.options.useIframeResizer||"IMG"===e.tagName&&c.options.useImageResizer||"TABLE"===e.tagName&&c.options.useTableResizer)&&c.resizer.bind(b(e)),"TABLE"===e.tagName&&c.options.useTableProcessor&&!b(e).data("table-processor")&&void 0!==a.modules.TableProcessor&&(f=new a.modules.TableProcessor(c,e),c.tableprocessor.instances.push(f),b(e).data("table-processor",f)),"IMG"===e.tagName&&c.options.useImageProcessor&&!b(e).data("image-processor")&&void 0!==a.modules.ImageProcessor&&(f=new a.modules.ImageProcessor(c,e),c.imageprocessor.instances.push(f),b(e).data("image-processor",f))}),c.observer.block(0),a.prototype.observe.work=!1}},a.prototype.observe.work=!1,a.prototype.getVersion=function(){return f},a.prototype.getEditorValue=function(){var a=b(this.$editor[0].cloneNode(!0));return a.find(".jodit_marker").remove(),a.find(".jodit_focused_image,.jodit_focused_cell").removeClass("jodit_focused_image jodit_focused_cell"),a.html()},a.prototype.setEditorValue=function(a){var b={value:a};this.events.fire("beforeSetEditorValue",[b]),this.$editor.html(b.value),this.events.fire("afterSetEditorValue")},a.prototype.getAreaValue=function(){return this.$area.val()},a.prototype.setAreaValue=function(a){var b={value:a};this.events.fire("beforeSetAreaValue",[b]),this.$area.val(b.value),this.events.fire("afterSetAreaValue")},a.prototype.getElementValue=function(){return this.$element.val()},a.prototype.setElementValue=function(a){var b,c=this.getElementValue(),d={value:a};return this.events.fire("beforeSetElementValue",[d])===!1?"":(b=this.helper.clear(d.value,this.options.removeEmptyBlocks),this.$element.val(b),this.events.fire("afterSetElementValue"),c!==b&&(this.options.triggerChangeEvent&&this.$element.trigger("change"),this.events.fire("changeElementValue")),b)},a.prototype.closeToolbarPopap=function(){(this.$toolbar_popap.is(":visible")||this.$toolbar.find(".jodit_dropdownlist").is(":visible"))&&(this.$toolbar.find(".jodit_with_dropdownlist").removeClass("jodit_dropdown_open active jodit_right"),this.$toolbar_popap.hide(),this.$toolbar_popap.appendTo("body"))},a.prototype.buildToolbarButton=function(a,c,d){var e,f=b('<li><a href="javascript:void(0)"></a><div class="jodit_tooltip"></div></li>'),g="string"===b.type(a)?a:a.name||"empty",h=f.find("a");return f[0].control=c,f.addClass("toolbar-"+g),h.append(this.icons.getSVGIcon(g)),void 0!==c&&b.isPlainObject(c)||(c={command:g}),void 0!==d&&h.html(d),c.list&&(e=b('<ul class="jodit_dropdownlist"></ul>'),b.each(c.list,b.proxy(function(a,b){var d=this.buildToolbarButton(a,{exec:c.exec,command:c.command,args:[c.args&&c.args[0]||a,c.args&&c.args[1]||b]},c.template&&c.template.call(this,a,b));e.append(d)},this)),f.append(e),f.addClass("jodit_with_dropdownlist")),c.iconURL&&h.css({"background-image":"url("+c.iconURL+")"}),c.tooltip?f.find(".jodit_tooltip").html(this.i18n(c.tooltip)):f.find(".jodit_tooltip").remove(),f.on("mousedown",b.proxy(function(a){return!f.hasClass("disabled")&&(a.stopImmediatePropagation(),a.preventDefault(),this.$toolbar.find(".jodit_with_dropdownlist").not(f).removeClass("jodit_dropdown_open active"),this.$toolbar_popap.hide(),e?f.toggleClass("jodit_dropdown_open active"):void 0!==c.exec&&b.isFunction(c.exec)?c.exec.call(this,a,c,f):void 0!==c.popap&&b.isFunction(c.popap)?(f.toggleClass("active"),this.$toolbar_popap.show(),f.append(this.$toolbar_popap),this.$toolbar_popap.empty(),this.$toolbar_popap.html(c.popap.call(this,this.selection.current(),c))):((c.command||g)&&this.execCommand(c.command||g,c.args&&c.args[0]||null,c.args&&c.args[1]||null),this.checkTargets(this.selection.current())),void this.syncCode(!0))},this)),f},a.prototype.buildToolbar=function(){var a=this,c=!1;a.$toolbar=b('<ul class="jodit_toolbar toolbar-head"></ul>'),a.options.toolbar||a.$toolbar.hide(),a.$wysiwyg.addClass("jodit_toolbar_size-"+(["middle","large","small"].indexOf(a.options.toolbarButtonSize.toLowerCase())!==-1?a.options.toolbarButtonSize.toLowerCase():"middle")),a.$toolbar_popap=b('<div class="jodit_toolbar_popap non-selected"></div>'),a.$toolbar_dummy=b('<div class="jodit_toolbar_dummy"></div>'),a.$toolbar_popap.on("mousedown",function(a){a.stopImmediatePropagation()}),b(a.getWindow()).add(a.$editor).off("mousedown.editor"+a.id).on("mousedown.editor"+a.id,function(){a.closeToolbarPopap()}),b.each(a.options.buttons,function(d){if(a.options.removeButtons.indexOf(a.options.buttons[d])===-1)if("|"!==a.options.buttons[d]){var e=a.options.buttons[d];c=!1,b.isPlainObject(e)||void 0===a.options.controls[e]||(e=a.options.controls[e]),a.$toolbar.append(a.buildToolbarButton(a.options.buttons[d],e))}else c||(c=!0,a.$toolbar.append('<li class="separator"></li>'))}),a.$workflow.before(a.$toolbar[0]),a.$workflow.before(a.$toolbar_dummy[0]),a.events.on("afterResize afterInit",function(){var c,d=a.$wysiwyg.width();a.$toolbar.find("li").addClass("jodit-btn-hidden"),c=d>=a.options.sizeLG?a.options.buttons:d>=a.options.sizeMD?a.options.buttonsMD:d>=a.options.sizeSM?a.options.buttonsSM:a.options.buttonsXS,b.each(c,function(b,c){a.$toolbar.find("li.toolbar-"+c).removeClass("jodit-btn-hidden")})})},a.prototype.init=function(b,d){var e,f,g=this;for(g.element=b,g.mode=a.MODE_WYSIWYG,g.$element=a.modules.Dom(b),g.options=d,g.id="jodit"+(b.hasAttribute("id")?b.getAttribute("id"):"editor"+c),f=g.id;void 0!==a.editors[f];)c+=1,f=g.id+c;g.id=f,g.build(),g.$element.data("jodit",g),e=g.options.defaultMode||a.MODE_WYSIWYG,g.options.saveModeInCookie&&g.cookie.get("jodit_default_mode")&&(e=parseInt(g.cookie.get("jodit_default_mode"),10)||a.MODE_WYSIWYG),g.setMode(e),g.observe(),a.editors[g.id]=g,g.events.fire("afterInit")},a.prototype.loadModules=function(c){var d,e=c||Object.keys(a.modules);if(this.base=new a.modules.Base(this),e.length){for(d=0;d<e.length;d+=1)this[e[d].toLowerCase()]||"Dom"===e[d]||(this[e[d].toLowerCase()]=new a.modules[e[d]](this));for(d=0;d<e.length;d+=1)"base"!==e[d].toLowerCase()&&(this[e[d].toLowerCase()]=b.extend(this[e[d].toLowerCase()],this.base),b.isFunction(this[e[d].toLowerCase()].init)&&this[e[d].toLowerCase()].init())}return this},a.prototype.destroyModules=function(c){var d,e=c||Object.keys(a.modules);if(e.length)for(d=0;d<e.length;d+=1)b.isFunction(this[e[d].toLowerCase()].destroy)&&this[e[d].toLowerCase()].destroy(),delete this[e[d].toLowerCase()];return this},a.prototype.focus=function(){var b=this;switch(b.getRealMode()){case a.MODE_WYSIWYG:b.selection.isFocused()||b.$editor.focus();break;case a.MODE_TEXTAREA:b.$area.is(":visible")?b.$area.focus():b.events.fire("focus")}},a.prototype.build=function(){var c,d,e=this;e.windows=[window],e.$wysiwyg=b('<div id="'+e.id+'" class="jodit"></div>'),e.$workflow=b('<div class="jodit_workflow"></div>'),e.options.iframe?e.$iframe=b('<iframe style="display:block;" src="about:blank" frameBorder="0"></iframe>'):e.$editor=b('<div class="jodit_editor" contenteditable="true" tabindex="'+e.options.tabIndex+'"/>'),e.$wysiwyg.addClass("jodit_"+(e.options.theme||"default")+"_theme"),e.$area=b('<textarea class="jodit_area_editor"/>'),e.$area_flow=b('<div class="jodit_area_flow"></div>'),e.$area_flow.append(e.$area),e.$wysiwyg_flow=b('<div class="jodit_wysiwyg_flow"></div>'),e.options.iframe?e.$wysiwyg_flow.append(e.$iframe):e.$wysiwyg_flow.append(e.$editor),e.options.zIndex&&e.$wysiwyg.css({zIndex:parseInt(e.options.zIndex,10)}),e.area=e.$area.get(0),e.$progress_bar=b('<div class="jodit_progress_bar"><div></div></div>'),e.$workflow.append(e.$wysiwyg_flow).append(e.$area_flow).append(e.$progress_bar),e.$wysiwyg.append(e.$workflow),e.$element.after(e.$wysiwyg),e.options.iframe?(e.iframe=e.$iframe[0],d=e.getDocument(),d.open(),d.write("<!DOCTYPE html>"),d.write('<html class="jodit"><head>'+(e.options.iframeBaseUrl?'<base href="'+e.options.iframeBaseUrl+'"/>':"")+'</head><body class="jodit_editor" style="outline:none" contenteditable="true"></body></html>'),d.close(),e.$editor=b(d.body),e.options.iframeCSSLinks&&e.options.iframeCSSLinks.forEach(function(a){var c=b('<link rel="stylesheet" href="'+a+'">',d).get(0);d.head.appendChild(c)}),e.options.iframeStyle&&(c=d.createElement("style"),c.innerHTML=e.options.iframeStyle,e.getDocument().head.appendChild(c)),e.$iframe.css({width:"auto"===e.options.width?"100%":e.options.width,height:e.options.height,minHeight:e.options.minHeight}),e.$editor.css("minHeight",e.options.minHeight),e.windows.push(e.getWindow())):e.$editor.css({width:e.options.width,height:e.options.height,minHeight:e.options.minHeight}),e.options.editorCssClass&&e.$editor.addClass(e.options.editorCssClass),e.editor=e.$editor.get(0),e.icons=new a.modules.Icons(e),e.loadModules(["Events"]),e.buildToolbar(),e.loadModules(),e.val(e.$element.val()),e.$element.hide(),e.events.on("afterResize afterInit",function(){var a=window.pageYOffset||document.documentElement.scrollTop;a+e.options.offsetTopForAssix>e.$wysiwyg.offset().top&&a+e.options.offsetTopForAssix<e.$wysiwyg.offset().top+e.$wysiwyg.outerHeight()?(e.$toolbar.addClass("jodit_sticky").css({top:e.options.offsetTopForAssix,width:e.$wysiwyg.width()}),e.$toolbar_dummy.css({height:e.$toolbar.height()})):e.$toolbar.hasClass("jodit_sticky")&&e.$toolbar.css({width:"auto",top:"auto"}).removeClass("jodit_sticky")}),b(window).on("resize.jodit_"+e.id+" scroll.jodit_"+e.id,function(){e.events.fire("afterResize")}),e.$editor.on("click",function(a){e.checkTargets(a.target||a.srcElement)}),e.$editor.on("keydown",function(c){var d;switch(c.which){case a.keys.TAB:if(d=e.selection.current(),e.node.hasParent(d,"TD|TH"))return d=e.node.parentNode(d,"TD|TH"),c.shiftKey?e.selection.focusTo(b(d).closest("table").data("table-processor").prevCell(d)):e.selection.focusTo(b(d).closest("table").data("table-processor").nextCell(d,!0)),!1;if(e.node.hasParent(d,"PRE"))return d=e.getDocument().createTextNode("\t"),e.selection.insertNode(d),e.selection.insertNode(e.getDocument().createTextNode(a.NBSP)),!1}}),b().add(e.$area).add(e.$editor).on("keydown mousedown mouseup change update drop drag",function(){e.syncCode()}),e.options.iframe&&"auto"===e.options.height&&(e.getDocument().documentElement.style.overflowY="hidden",e.events.on("changeElementValue afterInit",function(){e.$iframe.css("height",e.$editor.outerHeight())})),e.options.direction&&(e.$editor.css("direction","rtl"===e.options.direction.toLowerCase()?"rtl":"ltr"),e.$area.css("direction","rtl"===e.options.direction.toLowerCase()?"rtl":"ltr")),e.options.spellcheck&&(e.$editor.attr("spellcheck",!0),e.$area.attr("spellcheck",!0)),e.$workflow.on("click",function(){e.focus()}),e.$editor.on("paste",function(a){if(e.events.fire("beforePaste",[a])===!1)return a.preventDefault(),!1;if(a&&a.clipboardData&&a.clipboardData.getData){var c,d,f=a.clipboardData.types,g="";if(b.isArray(f))for(c=0;c<f.length;c+=1)d+=f[c]+";";else d=f;/text\/html/.test(d)?g=a.clipboardData.getData("text/html"):/text\/rtf/.test(d)&&e.browser("safari")?g=a.clipboardData.getData("text/rtf"):/text\/plain/.test(d)&&!e.browser("mozilla")&&(g=e.helper.htmlentities(a.clipboardData.getData("text/plain")).replace(/\n/g,"<br/>")),(""!==g||e.node.isNode(g))&&(g=e.events.fire("processPaste",[a,g]),("string"==typeof g||e.node.isNode(g))&&(e.selection.insertHTML(g),e.syncCode(!0)),a.preventDefault(),a.stopPropagation())}return e.events.fire("afterPaste",[a])!==!1&&void 0});try{e.getDocument().execCommand("enableObjectResizing",!1,!1),e.getDocument().execCommand("enableInlineTableEditing",!1,!1)}catch(a){}},a.prototype.destroy=function(){e.forEach(function(a){clearTimeout(a)}),this.destroyModules(),b(window).off(".jodit_"+this.id).off(".jodit"+this.id),this.$wysiwyg.remove(),delete this.$wysiwyg,this.$element.show(),this.$element.removeData("jodit"),delete a.editors[this.id]},a.editors={},a.MODE_WYSIWYG=1,a.MODE_TEXTAREA=2,a.MODE_SPLIT=3,a.keys={CTRL:17,TAB:9,ENTER:13,BACKSPACE:8,LEFT:37,TOP:38,RIGHT:39,BOTTOM:40,Z:90,Y:89},a.RIGHT_CLICK=3,a.NBSP=" ",a.NBSP_REG_EXP=/\u00A0/g,a.INVISIBLE_SPACE="\ufeff",a.INVISIBLE_SPACE_REG_EXP=/[\uFEFF\u200b]/g,a.INVISIBLE_SPACE_REG_EXP_START=/^[\uFEFF]+/g,a.INVISIBLE_SPACE_REG_EXP_END=/[\uFEFF]+$/g,a.SPACE_REG_EXP_START=/^[\s\n\t\r\uFEFF\u200b]+/g,a.SPACE_REG_EXP_END=/[\s\n\t\r\uFEFF\u200b]+$/g,a.ENTER_BR="br",a.ENTER_P="p",a.ENTER_DIV="div",a.modules={},a.BLOCKS=["P","DIV","H1","H2","H3","H4","H5","H6","HR","ADDRESS","PRE","FORM","TABLE","LI","OL","UL","TD","TH","CAPTION","BLOCKQUOTE","CENTER","DL","DT","DD","SCRIPT","NOSCRIPT","STYLE"],a.ALONE=["BASE","BR","EMBED","HR","IMG","INPUT","LINK","META"],a.OLDTAG={I:"EM",B:"STRONG",STRIKE:"S"},a.modules={},a.plugins={},a.lang={en:{}},a.modules.Base=function(a){this.parent=a,this.ctrlKey=function(a){if(navigator.userAgent.indexOf("Mac OS X")!==-1){if(a.metaKey&&!a.altKey)return!0}else if(a.ctrlKey&&!a.altKey)return!0;return!1}},a.extendOptions=function(c){var d,e,f,g={},h=!1,i=0,j=arguments.length;for("[object Boolean]"===Object.prototype.toString.call(c)&&(h=c,i+=1),f=function(c){for(e=Object.keys(c),d=0;d<e.length;d+=1)h&&b.isPlainObject(c[e[d]])?g[e[d]]=a.extendOptions(!0,null===g[e[d]]?{}:g[e[d]],c[e[d]]):g[e[d]]=c[e[d]]};i<j;)void 0!==arguments[i]&&f(arguments[i]),i+=1;return g},a.prototype.i18n=function(b){var c,d=Array.prototype.slice.call(arguments),e=function(a){return a.replace(/([0-9]{1})\$/gm,function(a,b){return b=parseInt(b,10),void 0!==d[b]?d[b]:""})};return c=void 0!==this.options&&void 0!==a.lang[this.options.language]?a.lang[this.options.language]:void 0!==a.lang[a.defaultOptions.language]?a.lang[a.defaultOptions.language]:a.lang.en,e(void 0!==this.options&&void 0!==this.options.i18n[this.options.language]&&this.options.i18n[this.options.language][b]?this.options.i18n[this.options.language][b]:void 0!==c[b]?c[b]:void 0!==a.lang.en[b]?a.lang.en[b]:b)},a.defaultOptions={},a}),function(){"use strict";function a(){this.detach=this.remove,this.trigger=this.fire}function b(a){var b=!!a&&void 0!==a.length&&a.length,c=l.type(a);return"function"!==c&&!l.isWindow(a)&&("array"===c||0===b||"number"==typeof b&&b>0&&void 0!==a[b-1])}var c={},d=c.toString,e=c.hasOwnProperty,f=[],g=f.indexOf,h=Array.prototype.slice,i=/[\s]+/,j=document,k="_store_dataset",l=function(b,c){var d,e=new a;return b&&("function"==typeof b?window.addEventListener("load",b,!1):(d=e.getBySelector(b,c||j),d.length&&d.forEach(function(a){e.push(a)}))),e};l.fn={jqcut:"1.9.1",each:function(a){var b;for(b=0;b<this.length&&a.call(this[b],b,this[b])!==!1;b+=1);return this},not:function(a){var b=this;return l(a).each(function(){b.indexOf(this)!==-1&&b.splice(b.indexOf(this),1)}),this},getOffsetRect:function(a){var b=a.getBoundingClientRect(),c=a.ownerDocument,d=c.body,e=c.documentElement,f=c.defaultView||c.parentWindow,g=f.pageYOffset||e.scrollTop||d.scrollTop,h=f.pageXOffset||e.scrollLeft||d.scrollLeft,i=e.clientTop||d.clientTop||0,j=e.clientLeft||d.clientLeft||0,k=b.top+g-i,l=b.left+h-j;return{top:Math.round(k),left:Math.round(l)}},offset:function(){return this.length?this.getOffsetRect(this[0]):null},fire:function(a){a=a?a.split(i):[];var b,c=function(){var c,d=this,e=a[b].split(".")[0];j.createEvent?(c=j.createEvent("HTMLEvents"),c.initEvent(e,!0,!0)):(c=j.createEventObject(),c.eventType=e),c.eventName=e,j.createEvent?d.dispatchEvent(c):d.fireEvent("on"+c.eventType,c)};for(b=0;b<a.length;b+=1)this.each(c);return this},on:function(a,b,c){l.isFunction(b)&&(c=b,b=!1);var d,e,f,g,h,j,k=this,m=c,n=function(){var d,e=this,g=function(a,b){for(var c=a;c!==b&&c.parentNode&&c.parentNode!==e;)c=c.parentNode;return c===b},i=function(d){var f=e,h=function(){return window.performance&&window.performance.now?window.performance.now():(new Date).getTime()},i=h();if(d.start=function(){var a=h()-i;return i=h(),a},d=d||window.event,d.originalEvent=d,void 0===d.which&&void 0!==d.keyCode&&(d.which=d.keyCode),void 0===d.target&&(d.target=d.srcElement),void 0!==d.type&&d.type.match(/^touch/)&&void 0!==d.changedTouches&&void 0!==d.changedTouches[0]&&["pageX","pageY","clientX","clientY","identifier"].forEach(function(a){d[a]=d.changedTouches[0][a]}),b&&(f=!1,k.find(b).each(function(){if(g(d.target,this))return f=this,!1})),d.jodit_events=a,f&&c&&l.isFunction(c)&&!1===c.call(f,d))return d.preventDefault(),!1};for(e._rt_handlers||(e._rt_handlers={}),e._rt_namespaces||(e._rt_namespaces={}),e._rt_handlers[j]||(e._rt_handlers[j]=[]),d=0;d<e._rt_handlers[j].length;d+=1)if(e._rt_handlers[j][d].callback===c)return;i.callback=c,e._rt_handlers[j].push(i),e._rt_namespaces[h]||(e._rt_namespaces[h]=[]),e._rt_namespaces[h].push({event:f,callback:i}),e.addEventListener(f,i)};for(a=a?a.split(i):[],d=0;d<a.length;d+=1){switch(e=a[d].split("."),f=e[0],g=f,h=e[1]?e[1]:"window",j=f+"."+h,this.each(n),g){case"click":f="touchend";break;case"mouseup":f="touchcancel",j=f+"."+h,this.each(n),f="touchend";break;case"mousemove":f="touchmove";break;case"mousedown":f="touchstart"}g!==f&&(j=f+"."+h,this.each(n),c=m)}return this},off:function(a,b){a=a?a.split(i):[];var c,d,e=this,f=function(){var f=this;b?f.removeEventListener(a[c],b,!1):f._rt_handlers&&f._rt_handlers[a[c]]?(f._rt_handlers[a[c]].forEach(function(b){f.removeEventListener(a[c],b,!1)}),delete f._rt_handlers[a[c]]):/^\./.test(a[c])&&f._rt_namespaces&&(d=a[c].substr(1),f._rt_namespaces[d]&&(f._rt_namespaces[d].forEach(function(a){e.off(a.event,a.callback)}),delete f._rt_namespaces[d]))};for(c=0;c<a.length;c+=1)this.each(f);return this},one:function(a,b,c){var d,e=this;return l.isFunction(b)&&(c=b,b=!1),d=function(){c.apply(this,h.call(arguments)),e.off(a,d)},e.on(a,b,c),this},css:function(a,b){var c,d;return l.isPlainObject(a)||void 0!==b?(d=function(a,b,c){void 0!==c&&null!==c&&/^left|top|bottom|right|width|min|max|height|margin|padding/i.test(b)&&/^[\-\+]?[0-9\.]+$/.test(c.toString())&&(c=parseInt(c,10)+"px"),a.style[b]=c},this.each(function(){if(l.isPlainObject(a)){var c,e=Object.keys(a);for(c=0;c<e.length;c+=1)d(this,l.camelCase(e[c]),a[e[c]])}else d(this,l.camelCase(a),b)}),this):(this.length&&(c=window.getComputedStyle(this[0]).getPropertyValue(l.fromCamelCase(a)),/^left|top|bottom|right|width|min|max|height|margin|padding/i.test(a)&&/^[\-\+]?[0-9]+px$/.test(c.toString())&&(c=parseInt(c,10))),c)},is:function(a){var b,c=!1,d=this.toArray();if(":visible"===a){for(b=0;b<this.length;b+=1)if(d[b].offsetWidth||d[b].offsetHeight)return!0;return!1}return l(a,d[0].parentNode).each(function(){var a=this;if(l.each(d,function(b,d){if(a===d)return c=!0,!1}),c)return!1}),c},outerHeight:function(){return this.length&&void 0!==this[0].offsetHeight?parseInt(this[0].offsetHeight,10):0},width:function(a){return void 0===a?this.length?parseInt(window.getComputedStyle(this[0]).getPropertyValue("width"),10):0:(this.css("width",a),this)},height:function(a){return void 0===a?this.length?parseInt(window.getComputedStyle(this[0]).getPropertyValue("height"),10):0:(this.css("height",a),this)},outerWidth:function(){return this.length&&void 0!==this[0].offsetWidth?parseInt(this[0].offsetWidth,10):0},next:function(){return this.length?l(this[0].nextSibling):l()},prev:function(){return this.length?l(this[0].previousSibling):l()},closest:function(a){if(this.length){if("function"==typeof Element.prototype.closest)return l(this[0].closest(a));for(var b=this[0],c=Element.prototype.msMatchesSelector||Element.prototype.mozMatchesSelector||Element.prototype.webkitMatchesSelector||function(a){for(var c=(b.document||b.ownerDocument).querySelectorAll(a),d=0;c[d]&&c[d]!==b;)d+=1;return Boolean(c[d])};b&&1===b.nodeType;){if(c(a))return l(b);b=b.parentNode}}return l()},type:l.type,eachClasses:function(a,b,c){this.each(function(){var d,e=a&&"string"===l.type(a)?a.split(i):a||[],f=this.hasAttribute&&this.hasAttribute("class")?this.getAttribute("class").split(i):[],g=f.join(" ");for(d=0;d<e.length;d+=1)if(b(f,e[d])===!1)return!1;!c&&g!==f.join(" ")&&this.setAttribute&&this.setAttribute("class",f.join(" "))})},hasClass:function(a){var b=!1;return this.eachClasses(a,function(a,c){a.indexOf(c)!==-1&&(b=!0)},!0),b},addClass:function(a){return this.eachClasses(a,function(a,b){a.indexOf(b)===-1&&a.push(b)}),this},removeClass:function(a){return this.eachClasses(a,function(a,b){a.indexOf(b)!==-1&&a.splice(a.indexOf(b),1)}),this},toggleClass:function(a,b){return void 0===b?this.eachClasses(a,function(a,b){a.indexOf(b)!==-1?a.splice(a.indexOf(b),1):a.push(b)}):b?this.addClass(a):this.removeClass(a),this},text:function(a){return void 0!==a?(this.each(function(){this.innerText=a.toString()}),this):this[0]?this[0].innerText:""},html:function(a){return void 0!==a?("string"!==l.type(a)?this.empty().append(a):this.each(function(){this.innerHTML=a}),this):this[0]?this[0].innerHTML:""},empty:function(){return this.html("")},val:function(a){if(this.length){if(void 0===a)return this[0].value;this.each(function(){void 0!==this.value&&(this.value=a)})}return void 0===a?"":this},getChildren:function(a){var b,c=[];return this.each(function(){for(b=this.firstChild;b;)b&&(a||b.nodeType!==Node.TEXT_NODE)&&c.push(b),b=b.nextSibling}),c},contents:function(){return l(this.getChildren(!0))},children:function(){return l(this.getChildren())},find:function(a){var b=[],c=this;return c.each(function(){b=b.concat(c.getBySelector(a,this))}),l(b)},getBySelector:function(b,c){var d,e,f,g,i,k,m,n=this,o=[],p=function(a){d=d.concat(n.getBySelector(m[a],c))};for(i="string"!==l.type(b)||/^</.test(b)?[b]:b?b.split(","):[],k=0;k<i.length;k+=1)if(void 0!==i[k]){if(m=i[k],d=[],m instanceof Array){l.each(m,p),o=o.concat(d);continue}if(m instanceof a||void 0!==window.jQuery&&m instanceof jQuery){o=o.concat(h.call(m));continue}if(m instanceof Node||m instanceof Text||m.ownerDocument&&m instanceof(m.ownerDocument.defaultView||m.ownerDocument.parentWindow).HTMLElement||m instanceof Window||["htmldocument","window","htmlelement","htmlbodyelement","text","documentfragment"].indexOf(l.type(m))!==-1){o.push(m);continue}if("string"==typeof m){if(g=l.trim(m),/^#/.test(g)||/^(body|html|base|script|link|img|iframe|div|h[1-6]|p|table|tr|tbody|thead|tfoot|td|th|span|strong|ul|li|ol|a|body|label|option|select|input|button|textarea|i|b|strike|font|hr)(\s|\t|\n|$|>|:|\.|\#|\[)/i.test(g)||/^\./.test(g)||/^\>/.test(g)){/^\>/.test(g)&&(g=":scope"+g);try{d=c.querySelectorAll?h.call(c.querySelectorAll(g)||[]):[],o=o.concat(d);continue}catch(a){return[]}}for(e=(c&&void 0!==c.createElement?c:j).createElement("div"),e.innerHTML=m,f=e.firstChild;f;)d.push(f),f=f.nextSibling;o=o.concat(d)}}return o},prop:function(a,b){var c=this;return this.each(function(){return void 0===b?(c=this[a],!1):void(this[a]=b)}),c},attr:function(a,b){var c;return l.isPlainObject(a)||void 0!==b?this.each(function(){if(l.isPlainObject(a)){var c,d=Object.keys(a);for(c=0;c<d.length;c+=1)this.setAttribute(d[c],a[d[c]])}else this.setAttribute(a,b)}):(this.each(function(){return c=this.getAttribute(a),!1}),c)},removeAttr:function(a){return this.each(function(){"checked"===a&&(this[a]=!1),this.removeAttribute(a)}),this},hide:function(){return this.each(function(){this.style.display="none"}),this},show:function(){return this.each(function(){this.style.display="block"}),this},remove:function(){return this.each(function(){this.parentNode&&this.parentNode.removeChild&&this.parentNode.removeChild(this)})},before:function(a){var b=this;return b&&b.length&&l(a).each(function(){b[0].parentNode.insertBefore(this,b[0])}),this},after:function(a){var b,c=this;return c&&c.length&&(b=c[0].parentNode,l(a).each(function(){b.lastChild===c[0]?b.appendChild(this):c[0].parentNode.insertBefore(this,c[0].nextSibling)})),this},focus:function(){return this.length&&this[0]&&void 0!==this[0].focus&&j.hasFocus&&j.hasFocus()&&this[0].focus(),this},appendTo:function(a){return l(a).append(this),this},parent:function(){return l(this[0].parentNode)},index:function(){var a,b=0;if(this.length)for(a=this[0];a.previousSibling;)a=a.previousSibling,3===a.nodeType&&/^\s*$/.test(a.data)||(b+=1);return b},add:function(a){var b=this.slice(0);return b.push.apply(b,l(a)),l(b)},data:function(a,b){if(a=l.camelCase(a),this.length){if(void 0===b){if(void 0!==this[0].dataset){if(void 0!==this[0].dataset[a])return this[0].dataset[a]}else if(this[0].hasAttribute("data-"+a))return this[0].getAttribute("data-"+a);if(void 0!==this[0][k]&&void 0!==this[0][k][a])return this[0][k][a];return}return this.each(function(){void 0===this[k]&&(this[k]={}),null===b&&void 0!==this[k][a]?delete this[k][a]:this[k][a]=b}),this}return void 0===b?void 0:this},removeData:function(a){return this.data(a,null),this},append:function(a){return this.each(function(){var b=this;l(a).each(function(){b.appendChild&&b.appendChild(this)})})},length:0,eq:function(a){return this[a]?l(this[a]):l()},get:function(a){return this[a]},toArray:function(){return h.call(this)},push:function(){f.push.apply(this,arguments)},slice:function(){return f.slice.apply(this,arguments)},splice:function(){return f.splice.apply(this,arguments)},indexOf:function(){return f.indexOf.apply(this,arguments)},concat:function(){return f.concat.apply(this,arguments)}},a.prototype=l.fn,a.prototype.constructor=a,l.camelCase=function(a){return a.replace(/-(.{1})/g,function(a,b){return b.toUpperCase()})},l.fromCamelCase=function(a){return a.replace(/([A-Z]+)/g,function(a,b){return"-"+b.toLowerCase()})},l.type=function(a){return null===a?"null":"object"==typeof a||"function"==typeof a?c[d.call(a)]||"object":typeof a},l.isWindow=function(a){return null!==a&&a===a.window},l.isArray=Array.isArray,l.isPlainObject=function(a){return"object"===l.type(a)&&!a.nodeType&&!l.isWindow(a)&&!(a.constructor&&!e.call(a.constructor.prototype,"isPrototypeOf"))},l.each=function(a,c){var d,e,f;if(b(a))for(d=a.length,f=0;f<d&&c.call(a[f],f,a[f])!==!1;f+=1);else for(e=Object.keys(a),f=0;f<e.length&&c.call(a[e[f]],e[f],a[e[f]])!==!1;f+=1);return a},l.each(["Boolean","Number","String","Function","Array","Date","RegExp","Object","Error","Symbol","HTMLDocument","Window","HTMLElement","HTMLBodyElement","Text","DocumentFragment"],function(a,b){c["[object "+b+"]"]=b.toLowerCase()}),l.isNumeric=function(a){return!isNaN(a-parseFloat(a))},l.isFunction=function(a){return a&&"function"==typeof a},l.inArray=function(a,b,c){return null===b?-1:g.call(b,a,c)},l.trim=function(a,b){void 0===b&&(b="\\s");var c=new RegExp("^"+b,"g"),d=new RegExp(b+"$","g");return a.toString().replace(c,"").replace(d,"")},l.proxy=function(a,b){return function(){return a.apply(b,arguments)}},l.eventLog=!1,l.extend=l.fn.extend=function(a){var b,c,d,e,f,g,h,i,j=a||{},k=1,m=arguments.length,n=!1;for("boolean"==typeof j&&(n=j,j=arguments[k]||{},k+=1),"object"==typeof j||l.isFunction(j)||(j={}),k===m&&(j=this,k+=1),k;k<m;k+=1)if(b=arguments[k],null!==b&&void 0!==b)for(i=Object.keys(b),h=0;h<i.length;h+=1)c=i[h],d=j[c],e=b[c],j!==e&&(n&&e&&(l.isPlainObject(e)||l.isArray(e))?(f=l.isArray(e),f?(f=!1,g=d&&l.isArray(d)?d:[]):g=d&&l.isPlainObject(d)?d:{},j[c]=l.extend(n,g,e)):void 0!==e&&(j[c]=e));return j},l.defaultAjaxOptions={dataType:"json",type:"GET",url:"",async:!0,data:null,contentType:"application/x-www-form-urlencoded; charset=UTF-8",
headers:{},error:!1,success:!1,xhr:function(){return new XMLHttpRequest}},l.ajax=function(a){a=l.extend(!0,{},l.defaultAjaxOptions,a);var b=a.xhr(),c={status:!1,response:!1,is_done:!1,is_error:!1,__done:!1,__error:!1,__fire_done:function(a){c.is_done&&c.__done&&c.__done.call(b,c.response,c.status)},__fire_error:function(a){c.is_error&&c.__error&&(b.getMessage=function(){return c.status},c.__error.call(b,c.response,c.status))},done:function(a){c.__done=a,c.__fire_done()},error:function(a){c.__error=a,c.__fire_error()},abort:function(){b.abort()}},d=function(a,b){if("string"==typeof a||a instanceof window.FormData)return a;var c,e,f,g=[];for(c in a)a.hasOwnProperty(c)&&(e=b?b+"["+c+"]":c,f=a[c],g.push("object"==typeof f?d(f,e):encodeURIComponent(e)+"="+encodeURIComponent(f)));return g.join("&")};return b.onreadystatechange=function(d){if(b.readyState===XMLHttpRequest.DONE){var e=d.target.responseText;if(200===b.status){switch(a.dataType){case"json":try{e=JSON.parse(e)}catch(c){return b.getMessage=function(){return c.message},void(a.error&&"function"==typeof a.error&&a.error(b,b.status))}}a.success&&"function"==typeof a.success&&a.success(e),c.response=e,c.status=b.status,c.is_done=!0,c.__fire_done()}else a.error&&"function"==typeof a.error&&a.error(b,b.status),c.response=e,c.status=b.status,c.is_error=!0,c.__fire_error()}},b.open(a.type,a.url,a.async),b.setRequestHeader("X-REQUESTED-WITH","XMLHttpRequest"),a.contentType&&b.setRequestHeader("Content-type",a.contentType),a.headers&&l.each(a.headers,function(a,c){b.setRequestHeader(a,c)}),a.data&&b.send(d(a.data)),c},void 0!==window.Jodit?Jodit.modules.Dom=l:window.$=l}(),function(a){"use strict";Jodit.modules.Helper=function(b){var c=function(a){if("rgba(0, 0, 0, 0)"===a||""===a)return NaN;if(!a)return"#000000";if("#"===a.substr(0,1))return a;var b,c,d,e,f,g=/([\s\n\t\r]*?)rgb\((\d+), (\d+), (\d+)\)/.exec(a)||/([\s\n\t\r]*?)rgba\((\d+), (\d+), (\d+), ([\d\.]+)\)/.exec(a);if(!g)return"#000000";for(c=parseInt(g[2],10),d=parseInt(g[3],10),e=parseInt(g[4],10),f=e|d<<8|c<<16,b=f.toString(16).toUpperCase();b.length<6;)b="0"+b;return g[1]+"#"+b},d=function(b){var d,e=["#"];if(b=c(b),!b)return NaN;if(b=a.trim(b.toUpperCase()),b=b.substr(1),3===b.length){for(d=0;d<3;d+=1)e.push(b[d]),e.push(b[d]);return e.join("")}return b.length>6&&(b=b.substr(0,6)),"#"+b},e=function(a){return/^[0-9]$/.test(a)?a+"px":a},f=function(a){return a.replace(Jodit.INVISIBLE_SPACE_REG_EXP_START,"").replace(Jodit.INVISIBLE_SPACE_REG_EXP_END,"")},g=function(a){return a.replace(Jodit.SPACE_REG_EXP_START,"").replace(Jodit.SPACE_REG_EXP_END,"")},h=function(a,b){return a=g(a).replace(Jodit.INVISIBLE_SPACE_REG_EXP,"").replace(/[\s]*class=""/g,""),b&&(a=a.replace(/<p[^>]*>[\s\n\r\t]*(&nbsp;|<br>|<br\/>)?[\s\n\r\t]*<\/p>[\n\r]*/g,"")),a},i=function(a){return a.replace(/</gi,"&lt;").replace(/>/gi,"&gt;").replace(/"/gi,"&quot;").replace(/'/gi,"&apos;")},j=function(a){var b,c,d,e=["style","script","applet","embed","noframes","noscript"];for(a=a.replace(/[.\s\S\w\W<>]*<body[^>]*>([.\s\S\w\W<>]*)<\/body>[.\s\S\w\W<>]*/g,"$1").replace(/<p(.*?)class="?'?MsoListParagraph"?'? ([\s\S]*?)>([\s\S]*?)<\/p>/gi,"<ul><li>$3</li></ul>").replace(/<p(.*?)class="?'?NumberedText"?'? ([\s\S]*?)>([\s\S]*?)<\/p>/gi,"<ol><li>$3</li></ol>").replace(/<p(.*?)class="?'?MsoListParagraphCxSpFirst"?'?([\s\S]*?)(level\d)?([\s\S]*?)>([\s\S]*?)<\/p>/gi,"<ul><li$3>$5</li>").replace(/<p(.*?)class="?'?NumberedTextCxSpFirst"?'?([\s\S]*?)(level\d)?([\s\S]*?)>([\s\S]*?)<\/p>/gi,"<ol><li$3>$5</li>").replace(/<p(.*?)class="?'?MsoListParagraphCxSpMiddle"?'?([\s\S]*?)(level\d)?([\s\S]*?)>([\s\S]*?)<\/p>/gi,"<li$3>$5</li>").replace(/<p(.*?)class="?'?NumberedTextCxSpMiddle"?'?([\s\S]*?)(level\d)?([\s\S]*?)>([\s\S]*?)<\/p>/gi,"<li$3>$5</li>").replace(/<p(.*?)class="?'?MsoListParagraphCxSpLast"?'?([\s\S]*?)(level\d)?([\s\S]*?)>([\s\S]*?)<\/p>/gi,"<li$3>$5</li></ul>").replace(/<p(.*?)class="?'?NumberedTextCxSpLast"?'?([\s\S]*?)(level\d)?([\s\S]*?)>([\s\S]*?)<\/p>/gi,"<li$3>$5</li></ol>").replace(/<span([^<]*?)style="?'?mso-list:Ignore"?'?([\s\S]*?)>([\s\S]*?)<span/gi,"<span><span").replace(/<!--\[if \!supportLists\]-->([\s\S]*?)<!--\[endif\]-->/gi,"").replace(/<!\[if \!supportLists\]>([\s\S]*?)<!\[endif\]>/gi,"").replace(/(\n|\r| class=(")?Mso[a-zA-Z0-9]+(")?)/gi," ").replace(/<!--[\s\S]*?-->/gi,"").replace(/<(\/)*(meta|link|span|\\?xml:|st1:|o:|font)(.*?)>/gi,""),b=0;b<e.length;b+=1)c=new RegExp("<"+e[b]+".*?"+e[b]+"(.*?)>","gi"),a=a.replace(c,"");a=a.replace(/([\w\-]*)=("[^<>"]*"|'[^<>']*'|\w+)/gi,"").replace(/&nbsp;/gi," ");do d=a,a=a.replace(/<[^\/>][^>]*><\/[^>]+>/gi,"");while(a!==d);return a=h(a,!0).replace(/<a>(.[^<]+)<\/a>/gi,"$1"),a.replace(/<lilevel([^1])([^>]*)>/gi,'<li data-indent="true"$2>').replace(/<lilevel1([^>]*)>/gi,"<li$1>")},k=function(c,d){var e,f=a(c).offset();return b.options.iframe&&(e=b.$iframe.offset(),f.left+=e.left,f.top+=e.top),d?(e=b.$workflow.offset(),{left:f.left-e.left,top:f.top-e.top}):f},l=function(a){var c=b.node.create("DIV");return c.innerText=a,c.innerHTML},m=function(a,b,c){var d,e,f,g;return function(){e=arguments,f=!0,c=c||this,d||(g=function(){f?(a.apply(c,e),f=!1,d=setTimeout(g,b)):d=null})()}},n=function(a,b,c,d){3===arguments.length&&"boolean"!=typeof c&&(d=c,c=!1);var e;return function(){var f=arguments;d=d||this,c&&!e&&a.apply(d,f),clearTimeout(e),e=setTimeout(function(){c||a.apply(d,f),e=null},b)}},o=function(a){var b=new RegExp("^(https?:\\/\\/)((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.?)+[a-z]{2,}|((\\d{1,3}\\.){3}\\d{1,3}))(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*(\\?[;&a-z\\d%_.~+=-]*)?(\\#[-a-z\\d_]*)?$","i");return b.test(a)},p=function(a){return a.replace(/([^:])[\\\/]+/g,"$1/")},q=function(a){return a.replace(/([^:])[\\\/]+/g,"$1/")},r=function(a){return/<([A-Za-z][A-Za-z0-9]*)\b[^>]*>(.*?)<\/\1>/m.test(a)},s=function(b){if(a.isNumeric(b))return b;var c=b.substr(-2,2).toUpperCase(),d=["KB","MB","GB","TB"],e=b.substr(0,b.length-2);return d.indexOf(c)!==-1?e*Math.pow(1024,d.indexOf(c)+1):parseInt(b,10)},t=function(a){var b,c,d={},e=a.substr(1).split("&");for(b=0;b<e.length;b+=1)c=e[b].split("="),d[decodeURIComponent(c[0])]=decodeURIComponent(c[1]||"");return d},u=function(a,b,c){if(!o(a))return a;var d,e=document.createElement("a"),f=/(?:http?s?:\/\/)?(?:www\.)?(?:vimeo\.com)\/?(.+)/g;switch(e.href=a,b||(b=400),c||(c=345),e.hostname){case"www.vimeo.com":case"vimeo.com":return f.test(a)?a.replace(f,'<iframe width="'+b+'" height="'+c+'" src="//player.vimeo.com/video/$1" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>'):a;case"youtube.com":case"www.youtube.com":case"youtu.be":case"www.youtu.be":return d=t(e.search),d.v?'<iframe width="'+b+'" height="'+c+'" src="//www.youtube.com/embed/'+d.v+'" frameborder="0" allowfullscreen></iframe>':a}return a},v={parseQuery:t,convertMediaURLToVideoEmbed:u,isHTML:r,humanSizeToBytes:s,pathNormalize:p,urlNormalize:q,throttle:m,debounce:n,htmlspecialchars:l,isURL:o,offset:k,normalizeSize:e,trimInvisible:f,trim:g,htmlentities:i,clear:h,cleanFromWord:j,normalizeColor:d,colorToHex:c};return v}}(Jodit.modules.Dom),function(a){"use strict";Jodit.defaultOptions={zIndex:0,offsetTopForAssix:0,syncCodeTimeout:30,toolbarButtonSize:"middle",theme:"default",saveModeInCookie:!1,spellcheck:!0,editorCssClass:!1,removeEmptyBlocks:!0,triggerChangeEvent:!0,iframe:!1,iframeBaseUrl:!1,iframeStyle:'html{margin: 0px;}body{padding:10px;background:transparent;color:#000;position:relative;z-index: 2;user-select:auto;margin:0px;overflow:auto;}body:after{content:"";clear:both;display:block}table{width:100%;border-collapse:collapse} th,td{border:1px solid #ccc;-webkit-user-select:text;-moz-user-select:text;-ms-user-select:text;user-select:text}td.jodit_focused_cell,th.jodit_focused_cell{border: 1px double blue}p{margin-top:0;}.jodit_editor .jodit_iframe_wrapper{display: block;clear: both;user-select: none;position: relative;}.jodit_editor .jodit_iframe_wrapper:after {position:absolute;content:"";z-index:1;top:0;left:0;right: 0;bottom: 0;cursor: pointer;display: block;background: rgba(0, 0, 0, 0);}',iframeCSSLinks:[],iframeIncludeJoditStyle:!0,width:"auto",height:"auto",minHeight:100,direction:"",language:"en",i18n:Jodit.lang,tabIndex:-1,autofocus:!1,toolbar:!0,autosave:!1,autosaveCallback:!1,interval:60,enter:Jodit.ENTER_P,defaultMode:Jodit.MODE_WYSIWYG,useSplitMode:!1,colors:{greyscale:["#000000","#434343","#666666","#999999","#B7B7B7","#CCCCCC","#D9D9D9","#EFEFEF","#F3F3F3","#FFFFFF"],palette:["#980000","#FF0000","#FF9900","#FFFF00","#00F0F0","#00FFFF","#4A86E8","#0000FF","#9900FF","#FF00FF"],full:["#E6B8AF","#F4CCCC","#FCE5CD","#FFF2CC","#D9EAD3","#D0E0E3","#C9DAF8","#CFE2F3","#D9D2E9","#EAD1DC","#DD7E6B","#EA9999","#F9CB9C","#FFE599","#B6D7A8","#A2C4C9","#A4C2F4","#9FC5E8","#B4A7D6","#D5A6BD","#CC4125","#E06666","#F6B26B","#FFD966","#93C47D","#76A5AF","#6D9EEB","#6FA8DC","#8E7CC3","#C27BA0","#A61C00","#CC0000","#E69138","#F1C232","#6AA84F","#45818E","#3C78D8","#3D85C6","#674EA7","#A64D79","#85200C","#990000","#B45F06","#BF9000","#38761D","#134F5C","#1155CC","#0B5394","#351C75","#733554","#5B0F00","#660000","#783F04","#7F6000","#274E13","#0C343D","#1C4587","#073763","#20124D","#4C1130"]},colorPickerDefaultTab:"background",imageDefaultWidth:300,removeButtons:[],sizeLG:900,sizeMD:700,sizeSM:400,buttons:["source","|","bold","italic","|","ul","ol","|","font","fontsize","brush","paragraph","|","image","video","table","link","|","left","center","right","justify","|","undo","redo","|","hr","eraser","fullsize","about"],buttonsMD:["source","|","bold","italic","|","ul","ol","|","font","fontsize","brush","paragraph","|","image","table","link","|","left","center","right","justify","|","undo","redo","|","hr","eraser","fullsize"],buttonsSM:["source","|","bold","italic","|","ul","ol","|","fontsize","brush","paragraph","|","image","table","link","|","left","center","right","|","undo","redo","|","eraser","fullsize"],buttonsXS:["bold","image","|","brush","paragraph","|","left","center","right","|","undo","redo","|","eraser"],controls:{about:{exec:function(a,b,c){var d=new Jodit.modules.Dialog(this);d.setTitle(this.i18n("About Jodit")),d.setContent('<div class="jodit_about">                            <div>'+this.i18n("Jodit Editor")+" v."+this.getVersion()+" "+this.i18n("Free Non-commercial Version")+'</div>                            <div><a href="http://xdsoft.net/jodit/" target="_blank">http://xdsoft.net/jodit/</a></div>                            <div><a href="http://xdsoft.net/jodit/doc/" target="_blank">'+this.i18n("Jodit User's Guide")+"</a> "+this.i18n("contains detailed help for using")+"</div>                            <div>"+this.i18n("For information about the license, please go to our website:")+'</div>                            <div><a href="http://xdsoft.net/jodit/license.html" target="_blank">http://xdsoft.net/jodit/license.html</a></div>                            <div><a href="http://xdsoft.net/jodit/#download" target="_blank">'+this.i18n("Buy full version")+"</a></div>                            <div>"+this.i18n("Copyright © XDSoft.net - Chupurnov Valeriy. All rights reserved.")+"</div>                        </div>"),d.open()},tooltip:"About Jodit",mode:Jodit.MODE_TEXTAREA+Jodit.MODE_WYSIWYG},fullsize:{exec:function(a,b,c){this.events.fire("toggleFullsize")},tooltip:"Open editor in fullsize",mode:Jodit.MODE_TEXTAREA+Jodit.MODE_WYSIWYG},eraser:{exec:function(){this.execCommand("removeFormat",!1,!1)},tooltip:"Clear Formatting"},brush:{css:{backgroundColor:function(b,c){var d=this,e=function(c,f){var g,h;if(a.isPlainObject(c)){for(h=Object.keys(c),g=0;g<h.length;g+=1)if(e(c[h[g]]))return!0}else if(a.isArray(c))return c.indexOf(d.helper.normalizeColor(b))!==-1;return!1};return e(d.options.colors)}},popap:function(b){var c,d,e,f,g=this.selection.get(),h="",i="";return g&&g.anchorNode&&a(g.anchorNode.parentNode).attr("style")&&a(g.anchorNode.parentNode).attr("style").indexOf("background")!==-1&&a(g.anchorNode.parentNode).css("background-color")&&(c=g.anchorNode.parentNode,i=a(c).css("background-color")),g&&g.anchorNode&&a(g.anchorNode.parentNode).attr("style")&&a(g.anchorNode.parentNode).attr("style").indexOf("color")!==-1&&a(g.anchorNode.parentNode).css("color")&&(c=g.anchorNode.parentNode,h=a(c).css("color")),d=this.form.buildColorPicker(a.proxy(function(b){c?a(c).css("background-color",b):this.applyCSSProperty("background-color",b),this.closeToolbarPopap()},this),i),e=this.form.buildColorPicker(a.proxy(function(b){c?a(c).css("color",b):this.applyCSSProperty("color",b),this.closeToolbarPopap()},this),h),f="background"===this.options.colorPickerDefaultTab?{Background:d,Text:e}:{Text:e,Background:d},this.form.buildTabs(f)},tooltip:"Fill color or set the text color"},redo:{mode:Jodit.MODE_SPLIT,exec:function(){this.getRealMode()!==Jodit.MODE_TEXTAREA?this.observer.redo():this.observerarea.redo()},tooltip:"Redo"},undo:{mode:Jodit.MODE_SPLIT,exec:function(){this.getRealMode()!==Jodit.MODE_TEXTAREA?this.observer.undo():this.observerarea.undo()},tooltip:"Undo"},bold:{tags:["b","strong"],css:{fontWeight:"bold"},tooltip:"Bold",hotkey:{ctrl:1,key:66}},italic:{tags:["i","em"],css:{fontStyle:"italic"},tooltip:"Italic",hotkey:{ctrl:1,key:73}},ul:{command:"insertUnorderedList",controlName:"ul",exec:function(b,c){var d,e,f=this.selection,g=f.current();this.node.hasParent(g,"p")?(e=this.node.parentNode(g,"p"),d=0===this.helper.trim(e.innerText).length?a("<"+c.controlName+">\n<li>"+Jodit.INVISIBLE_SPACE+"</li>\n</"+c.controlName+">\n").get(0):a("<"+c.controlName+">\n<li> "+e.innerHTML+" </li>\n</"+c.controlName+">\n").get(0),this.node.replace(e,d,!0,!0),f.moveCursorTo(a(d).find("li").get(0))):this.execCommand(c.command)},tags:["ul"],tooltip:"Insert Unordered List"},ol:{command:"insertOrderedList",controlName:"ol",exec:function(a,b){this.options.controls.ul.exec.call(this,a,b)},tags:["ol"],tooltip:"Insert Ordered List"},center:{command:"justifyCenter",tags:["center"],css:{textAlign:"center"},tooltip:"Justify Center"},justify:{command:"justifyFull",css:{textAlign:"justify"},tooltip:"Justify Full"},left:{command:"justifyLeft",css:{textAlign:"left"},tooltip:"Justify Left"},right:{command:"justifyRight",css:{textAlign:"right"},tooltip:"Justify Right"},hr:{command:"insertHorizontalRule",tags:["hr"],tooltip:"Insert Horizontal Line"},image:{popap:function(b){var c=this;return c.form.imageSelector({filebrowser:function(a){if(a.files&&a.files.length){var b;for(b=0;b<a.files.length;b+=1)c.selection.insertImage(a.baseurl+a.files[b]);c.closeToolbarPopap(),c.syncCode()}},upload:function(a){var b;if(a.files&&a.files.length)for(b=0;b<a.files.length;b+=1)c.selection.insertImage(a.baseurl+a.files[b]);c.closeToolbarPopap(),c.syncCode()},url:function(b,d){var e=a(c.form.getCurrentImage()||a("<img/>",c.getDocument()).get(0));e.attr("src",b),e.attr("alt",d),c.form.getCurrentImage()||c.selection.insertImage(e.get(0)),c.closeToolbarPopap(),c.syncCode()}},b)},tags:["img"],tooltip:"Insert Image"},link:{popap:function(b){var c,d=this.selection.get(),e=a('<div><input required name="url" placeholder="http://" type="text"/><input name="text" placeholder="'+this.i18n("Anchor")+'" type="text"/><label><input name="target" type="checkbox"/> '+this.i18n("Open in new tab")+'</label><label><input name="nofollow" type="checkbox"/> '+this.i18n("No follow")+'</label><button type="button">'+this.i18n("Insert")+"</button></div>");return d&&d.anchorNode&&"A"===d.anchorNode.parentNode.tagName?(e.find("input[name=url]").val(d.anchorNode.parentNode.href),e.find("input[name=text]").val(d.anchorNode.parentNode.text),e.find("input[name=target]").get(0).checked="_blank"===d.anchorNode.parentNode.target,e.find("input[name=nofollow]").get(0).checked="nofollow"===d.anchorNode.parentNode.rel,""===d.toString()&&(c=d.anchorNode.parentNode)):e.find("input[name=text]").val(d.toString()),this.selection.save(),e.find("button").on("click",a.proxy(function(){this.selection.restore();var b=a(c||"<a></a>");return b.attr("href",e.find("input[name=url]").val()),b.text(e.find("input[name=text]").val()),e.find("input[name=target]").get(0).checked?b.attr("target","_blank"):b.removeAttr("target"),e.find("input[name=nofollow]").get(0).checked?b.attr("rel","nofollow"):b.removeAttr("rel"),c||this.execCommand("insertHTML",!1,b.prop("outerHTML")),this.closeToolbarPopap(),this.syncCode(),!1},this)),e},tags:["a"],tooltip:"Insert link"},video:{popap:function(b){var c=this,d=a('<form><input required name="code" placeholder="http://" type="url"/><button type="submit">'+this.i18n("Insert")+"</button></form>"),e=a('<form><textarea required name="code" placeholder="'+c.i18n("Embed code")+'"></textarea><button type="submit">'+this.i18n("Insert")+"</button></form>"),f={},g=function(a){c.selection.restore(),c.selection.insertHTML(a),c.closeToolbarPopap(),c.syncCode()};return f[c.icons.getSVGIcon("link")+"&nbsp;"+c.i18n("Link")]=d,f[c.icons.getSVGIcon("source")+"&nbsp;"+c.i18n("Code")]=e,c.selection.save(),e.on("submit",function(){var b=a(this);return a.trim(b.find("textarea[name=code]").val())?(g(b.find("textarea[name=code]").val()),!1):(b.find("textarea[name=code]").focus().addClass("jodit_error"),!1)}),d.on("submit",function(){var b=a(this);return c.helper.isURL(b.find("input[name=code]").val())?(g(c.helper.convertMediaURLToVideoEmbed(b.find("input[name=code]").val())),!1):(b.find("input[name=code]").focus().addClass("jodit_error"),!1)}),c.form.buildTabs(f)},tags:["iframe"],tooltip:"Insert youtube/vimeo video"},fontsize:{command:"fontSize",list:["8","9","10","11","12","14","18","24","30","36","48","60","72","96"],template:function(a,b){return b},tooltip:"Font size"},font:{command:"applyCSSProperty",args:[null,"font-family"],list:{"Helvetica,sans-serif":"Helvetica","Arial,Helvetica,sans-serif":"Arial","Georgia,serif":"Georgia","Impact,Charcoal,sans-serif":"Impact","Tahoma,Geneva,sans-serif":"Tahoma","'Times New Roman',Times,serif":"Times New Roman","Verdana,Geneva,sans-serif":"Verdana"},template:function(a,b){return'<span style="font-family: '+a+'"> '+b+" </span>"},tooltip:"Font family"},paragraph:{command:"formatBlock",list:{p:"Normal",h1:"Heading 1",h2:"Heading 2",h3:"Heading 3",h4:"Heading 4",blockquote:"Quote",pre:"Code"},template:function(a,b){return"<"+a+' class="jodit_list_element"><span>'+this.i18n(b)+"</span></"+a+"></li>"},tooltip:"Insert format block"},table:{cols:10,popap:function(b,c){function d(b){var c=(b+1)*m;if(p.length>c){for(e=c;e<p.length;e+=1)j[0].removeChild(p[e]),delete p[e];p=p.splice(0,c)}for(e=0;e<c;e+=1)p[e]||(h=document.createElement("div"),h.index=e,p.push(h));j.append(p),j.css("width",a(p[0]).outerWidth()*m+"px")}var e,f,g,h,i=this,j=a('<form class="jodit_form_inserter"><label><span>1</span> &times; <span>1</span></label></form>'),k=1,l=1,m=c.cols,n=j.find("span").eq(0),o=j.find("span").eq(1),p=[];return d(1),p[0].className="hovered",j.on("mousemove","div",function a(b){if(g=isNaN(b)?this.index:b,k=Math.ceil((g+1)/m),l=g%m+1,d(k),l===m||l<m-1&&m>10)return m=l===m?m+1:m-1,a(l+(k-1)*m-1);for(e=0;e<p.length;e+=1)l>=e%m+1&&k>=Math.ceil((e+1)/m)?p[e].className="hovered":p[e].className="";o.text(l),n.text(k)}),j.on("mousedown","div",function(a){a.preventDefault(),g=this.index,k=Math.ceil((g+1)/m),l=g%m+1;var b,c,d,h,j=i.getDocument().createElement("table"),n=(100/l).toFixed(7);for(e=1;e<=k;e+=1){for(c=i.getDocument().createElement("tr"),f=1;f<=l;f+=1)d=i.getDocument().createElement("td"),d.style.width=n+"%",b||(b=d),h=i.getDocument().createElement("br"),d.appendChild(h),c.appendChild(i.getDocument().createTextNode("\n")),c.appendChild(i.getDocument().createTextNode("\t")),c.appendChild(d);j.appendChild(i.getDocument().createTextNode("\n")),j.appendChild(c)}i.selection.insertNode(i.getDocument().createTextNode("\n")),i.selection.insertNode(j),i.selection.moveCursorTo(b),i.closeToolbarPopap(),i.syncCode()}),j},tags:["table"],tooltip:"Insert table"},source:{mode:Jodit.MODE_SPLIT,exec:function(){this.toggleMode()},tooltip:"Change mode"}}}}(Jodit.modules.Dom),function(a){"use strict";Jodit.modules.Selection=function(b){var c={},d=b.getDocument(),e=b.getWindow(),f=b.$editor,g=function(){return e.getSelection?e.getSelection():d.getSelection?d.getSelection():d.selection.createRange()},h=function(a){var b,c=g(),e=[];if(c&&c.getRangeAt&&c.rangeCount)for(b=0;b<c.rangeCount;b+=1)e.push(c.getRangeAt(b));else e=d.createRange?[d.createRange()]:[];return void 0!==a?e[a]:e},i=function(){var a=h(0),c=a.startContainer;if(c){for(;c;)if(c===b.$editor[0])return c=c.parentNode,!0;return!1}},j=function(a){var c=h(0),d=c.startContainer,e=c.endContainer;if(d===e)return a.call(d,d),void b.node.each(d,a);for(;d&&d!==e;)d=b.node.findNext(d,f[0]),d&&a.call(d,d);for(d=c.startContainer;d&&d!==c.commonAncestorContainer;)a.call(d,d),d=d.parentNode},k=function(){var a=g();if(!(a.removeAllRanges&&a.removeAllRanges()||a.empty&&a.empty()))return a.clear&&a.clear(),a},l=function(a,c){function e(a){for(;a;){if(a.nodeType===Node.ELEMENT_NODE||a.nodeType===Node.TEXT_NODE&&a.data&&/[\r\n\s]/.test(a.data))return a;a=a.nextSibling}}var f,h,i,j,k=g();if(a){if(/^(LI|DT|DD)$/.test(a.nodeName)&&(j=e(a.firstChild),j&&/^(UL|OL|DL)$/.test(j.nodeName)&&a.insertBefore(d.createTextNode(" "),a.firstChild)),f=d.createRange(),b.browser("msie")||a.normalize(),a.hasChildNodes()){for(h=a;h;){if(h.nodeType===Node.TEXT_NODE){f.setStart(h,c?h.nodeValue.length:0),f.setEnd(h,c?h.nodeValue.length:0);break}i=h,h=b.node.findNext(h,a)}h||(f.setStart(i,0),f.setEnd(i,0))}else"BR"===a.nodeName?a.nextSibling&&b.node.isBlock(a.nextSibling)?(f.setStartBefore(a),f.setEndBefore(a)):(f.setStartAfter(a),f.setEndAfter(a)):a.nodeType===Node.TEXT_NODE?(f.setStart(a,c?a.nodeValue.length:0),f.setEnd(a,c?a.nodeValue.length:0)):(f.setStart(a,0),f.setEnd(a,0));k.removeAllRanges(),k.addRange(f)}},m=function(a){var c,d,e,g=h(0),i=g.startContainer,j=!1,k=b.node.getParentNode(i,b.node.isBlock),l=g.startOffset;if(i.nodeType===Node.ELEMENT_NODE&&i.hasChildNodes()&&(j=l>i.childNodes.length-1,i=i.childNodes[Math.min(l,i.childNodes.length-1)]||i,l=j&&i.nodeType===Node.TEXT_NODE?i.nodeValue.length:0),i.nodeType===Node.TEXT_NODE&&(e=d=i.nodeValue,Jodit.SPACE_REG_EXP_START.test(d)&&l&&(e=d.replace(Jodit.INVISIBLE_SPACE_REG_EXP_START,""),l-=d.length-e.length),d=e,Jodit.SPACE_REG_EXP_END.test(d)&&l&&(e=b.helper.trim(d),l>e.length&&(l=e.length)),l<0&&(l=0)),i.nodeType===Node.TEXT_NODE&&(a?l>0:l<e.length))return!1;if(a&&i.nodeType===Node.ELEMENT_NODE&&(k&&i===k.firstChild||i===f[0].firstChild))return!0;if(!a&&i.nodeType===Node.ELEMENT_NODE&&(k&&i===k.lastChild||i===f[0].lastChild))return!0;if("TABLE"===i.nodeName||i.previousSibling&&"TABLE"===i.previousSibling.nodeName)return j&&!a||!j&&a;for(c=i,i.nodeType===Node.TEXT_NODE&&(a&&0===l?c=b.node.find(c,k,"lastChild","previousSibling"):a||l!==e.length||(c=b.node.findNext(c,k)));c;){if(c.nodeType===Node.ELEMENT_NODE){if("BR"!==c.nodeName)return!1}else if(c.nodeType===Node.TEXT_NODE&&!/^[ \t\r\n]*$/.test(c.nodeValue)&&!b.node.isEmptyTextNode(c))return!1;c=a?b.node.find(c,k,"lastChild","previousSibling",!1):b.node.findNext(c,k)}return!0},n=function(){return b.getDocument().hasFocus()&&b.$editor[0]===b.getDocument().activeElement},o=function(a){var b,c=d.createRange();c.setStartAfter(a),c.collapse(!0),b=g(),k(),b.addRange(c)},p=function(){var a,b=h();for(a=0;a<b.length;a+=1)if(!b[a].collapsed)return!1;return!0},q=0,r=function(a){var c,e;c=g(),n()||(b.getWindow().focus(),b.$editor.focus()),p()||b.execCommand("Delete"),c.rangeCount&&(e=h(0),b.browser("webkit")?e.collapsed||d.execCommand("delete"):e.deleteContents(),e.insertNode(a),a.firstChild&&(e=e.cloneRange(),e.setStartAfter(a.firstChild),e.collapse(!0),c.removeAllRanges(),c.addRange(e))),clearTimeout(q),q=setTimeout(function(){b.syncCode()},300)},s=function(a){var c,e,f=d.createElement("DIV"),g=d.createDocumentFragment();for(n()||b.editor.focus(),"string"==typeof a?f.innerHTML=a:b.node.isNode(a)&&f.appendChild(a),c=f.lastChild;f.firstChild;)g.appendChild(f.firstChild);for(r(g),e=b.editor.lastChild;e&&e.nodeType===Node.TEXT_NODE&&e.previousSibling&&/^\s*$/.test(e.data);)e=e.previousSibling;c&&(e&&c===e&&1===c.nodeType&&b.editor.appendChild(d.createElement("br")),o(c))},t=function(c,e){var f,g="string"===a.type(c)?a("<img/>",d):a(c);p()||b.execCommand("Delete"),"string"===a.type(c)&&g.attr("src",c),f=b.options.imageDefaultWidth,f&&"auto"!==f&&String(f).indexOf("px")<0&&String(f).indexOf("%")<0&&(f+="px"),g.css(e||{width:f}),g.one("load",function(){(this.naturalHeight<a(this).height()||this.naturalWidth<a(this).width())&&(g.css("width",""),g.css("height","")),b.syncCode()}).each(function(){this.complete&&a(this).trigger("load")}),r(g[0]),b.events.fire("afterInsertImage",[g[0]])},u=function(a){var b=d.createRange();a.forEach(function(a){b.selectNode(a)}),k(),g().addRange(b)},v=function(a){u([a])},w=function(a,b,c,e){c||(c=a),void 0===e&&(e=b);var f,h=g();h&&(h.collapse&&h.extend?(h.collapse(a,b),h.extend(c,e)):(f=d.createRange(),f.setStart(a,b),f.setEnd(c,e),k(),h.addRange(f)))},x=function(){f.find(".jodit_marker").remove()},y=function(a,b){return'<span class="jodit_marker" data-start="'+(a?1:0)+'" data-index="'+b+'"  style="display: inline-block; line-height: 0;">&#8203;</span>'},z=function(c,e,f){var g,h,i,j;try{if(j=c.cloneRange(),j.collapse(!!e),g=a(y(e,f),d).get(0),j.insertNode(g),e&&c.collapsed)for(h=g.nextSibling;h&&h.nodeType===Node.TEXT_NODE&&!h.textContent.length;)h.parentNode.removeChild(h),h=g.nextSibling;if(e&&!c.collapsed&&(h=g.nextSibling,h&&b.node.isBlock(h))){i=[h];do h=i[0],i=h?a(h).contents():[];while(i[0]&&b.node.isBlock(i[0]));h.firstChild?h.insertBefore(g,h.firstChild):h.appendChild(g)}if(!e&&!c.collapsed){if(h=g.previousSibling,h&&b.node.isBlock(h)){i=[h];do h=i[i.length-1],i=h?a(h).contents():[];while(i[i.length-1]&&b.node.isBlock(i[i.length-1]));a(h).append(g)}g.parentNode&&["TD","TH"].indexOf(g.parentNode.tagName)!==-1&&g.parentNode.previousSibling&&!g.previousSibling&&b.node.isBlock(g.parentNode.previousSibling)&&g.parentNode.previousSibling.appendChild(g)}return g}catch(a){return console.log(a),null}},A=!1,B=function(){if(!A||i){x();var a,b=h();for(a=0;a<b.length;a+=1)b[a].startContainer!==d&&(z(b[a],!0,a),z(b[a],!1,a));A=!0}},C=function(a,b){var c,e,f=null;if(x(),void 0!==d.caretPositionFromPoint?(c=d.caretPositionFromPoint(a,b),f=d.createRange(),f.setStart(c.offsetNode,c.offset),f.setEnd(c.offsetNode,c.offset)):void 0!==d.caretRangeFromPoint&&(c=d.caretRangeFromPoint(a,b),f=d.createRange(),f.setStart(c.startContainer,c.startOffset),f.setEnd(c.startContainer,c.startOffset)),null!==f)k(),g().addRange(f);else if(void 0!==d.body.createTextRange)try{f=d.body.createTextRange(),f.moveToPoint(a,b),console.log(f),e=f.duplicate(),e.moveToPoint(a,b),f.setEndPoint("EndToEnd",e),f.select()}catch(a){return!1}A=!1,B()},D=function(c){function e(a,b,c){var d,e=a.previousSibling,f=a.nextSibling;return!(!e||!f||e.nodeType!==Node.TEXT_NODE||f.nodeType!==Node.TEXT_NODE)&&(d=e.textContent.length,c?(f.textContent=e.textContent+f.textContent,e.parentNode.removeChild(e),a.parentNode.removeChild(a),function(){b.setStart(f,d)}):(e.textContent=e.textContent+f.textContent,f.parentNode.removeChild(f),a.parentNode.removeChild(a),function(){b.setEnd(e,d)}))}var h,i,j,l,m,n,o,p,q,r,s,t,u=f.find('.jodit_marker[data-start="1"]'),v=!1;if(A=!1,!u.length)return!1;for(o=g(),f.focus(),k(),t=0;t<u.length;t+=1){if(h=u.eq(t).data("index"),i=u.get(t),l=d.createRange(),j=f.find('.jodit_marker[data-start="0"][data-index="'+h+'"]').get(0))try{if(b.node.clearEmptyTextSibling(i),b.node.clearEmptyTextSibling(j),i.nextSibling===j||j.nextSibling===i){if(r=b.node.clearEmptyTextSibling(i.nextSibling===j?i:j,!0),r&&r.nodeType===Node.TEXT_NODE)for(;r&&r.previousSibling&&r.previousSibling.nodeType===Node.TEXT_NODE;)r.previousSibling.textContent=r.previousSibling.textContent+r.textContent,r=r.previousSibling,r.nextSibling.parentNode.removeChild(r.nextSibling);if(s=b.node.clearEmptyTextSibling(i.nextSibling===j?j:i),s&&s.nodeType===Node.TEXT_NODE)for(;s&&s.nextSibling&&s.nextSibling.nodeType===Node.TEXT_NODE;)s.nextSibling.textContent=s.textContent+s.nextSibling.textContent,s=s.nextSibling,s.previousSibling.parentNode.removeChild(s.previousSibling);r&&b.node.isAlone(r)&&(r=null),s&&b.node.isAlone(s)&&(s=null),r&&s&&r.nodeType===Node.TEXT_NODE&&s.nodeType===Node.TEXT_NODE?(i.parentNode.removeChild(i),j.parentNode.removeChild(j),p=r.textContent.length,r.textContent=r.textContent+s.textContent,s.parentNode.removeChild(s),l.setStart(r,p),l.setEnd(r,p),v=!0):!r&&s&&s.nodeType===Node.TEXT_NODE?(j.parentNode.removeChild(j),l.setStart(s,0),l.setEnd(s,0),v=!0):!s&&r&&r.nodeType===Node.TEXT_NODE&&(i.parentNode.removeChild(i),j.parentNode.removeChild(j),l.setStart(r,r.textContent.length),l.setEnd(r,r.textContent.length),v=!0)}v||(i.nextSibling===j?(m=e(j,l,!0),m||l.setStartAfter(j),n=e(i,l,!1),n||l.setEndBefore(i)):(i.previousSibling===j&&(i=j,j=i.nextSibling),j.nextSibling&&"BR"===j.nextSibling.tagName||!j.nextSibling&&b.node.isBlock(i.previousSibling)||i.previousSibling&&"BR"===i.previousSibling.tagName||(i.style.display="inline",j.style.display="inline",q=a(d.createTextNode("​"))),m=e(i,l,!0),m||(a(i).before(q),l.setStartBefore(i)),n=e(j,l,!1),n||(a(j).after(q),l.setEndAfter(j))),m&&m(),n&&n())}catch(a){console.log(a)}o.addRange(l)}c||x()},E=function(){if(void 0!==e.getSelection){var a=g();if(a.rangeCount>0)return h(0).commonAncestorContainer}return b.editor},F=function(a){var b=d.createRange();b.selectNodeContents(a),b.collapse(!1),k(),g().addRange(b)};return c={insertAtPoint:C,selectImage:v,selectNodes:u,each:j,moveCursorTo:l,cursorInEdge:m,insertHTML:s,insertNode:r,isCollapsed:p,set:w,get:g,range:h,empty:k,unmark:x,marker:y,save:B,mark:z,restore:D,current:E,isFocused:n,focusAfter:o,focusTo:F,insertImage:t}}}(Jodit.modules.Dom),function(a){"use strict";Jodit.modules.Snapshot=function(a){var b,c=a.editor,d=function(a){if(!a.parentNode)return 0;var b,c,d=a.parentNode.childNodes,e=0;for(c=0;c<d.length;c+=1){if(!b||d[c].nodeType===Node.TEXT_NODE&&""===d[c].textContent||b.nodeType===Node.TEXT_NODE&&d[c].nodeType===Node.TEXT_NODE||(e+=1),d[c]===a)return e;b=d[c]}},e=function(a){var b=[];if(!a.parentNode)return[];for(;a&&a!==c;)b.push(d(a)),a=a.parentNode;return b.reverse()},f=function(a,b){for(;a&&a.nodeType===Node.TEXT_NODE;)a=a.previousSibling,a&&a.nodeType===Node.TEXT_NODE&&(b+=a.textContent.length);return b},g=function(){var b,d={};return d.html=c.innerHTML,b=a.selection.range(0),b&&(d.range={startContainer:e(b.startContainer),startOffset:f(b.startContainer,b.startOffset),endContainer:e(b.endContainer),endOffset:f(b.endContainer,b.endOffset)}),d},h=function(a){var b,d=c;for(b=0;d&&b<a.length;b+=1)d=d.childNodes[a[b]];return d},i=function(a,b){return a.html===b.html&&JSON.stringify(a.range)===JSON.stringify(b.range)},j=function(b){c.innerHTML=b.html,b.range&&a.selection.set(h(b.range.startContainer),b.range.startOffset,h(b.range.endContainer),b.range.endOffset)};return b={make:g,equal:i,restore:j}}}(Jodit.modules.Dom),function(a){"use strict";Jodit.modules.Browser=function(){return function(a){var b=navigator.userAgent.toLowerCase(),c=/(firefox)[\s\/]([\w.]+)/.exec(b)||/(chrome)[\s\/]([\w.]+)/.exec(b)||/(webkit)[\s\/]([\w.]+)/.exec(b)||/(opera)(?:.*version)[\s\/]([\w.]+)/.exec(b)||/(msie)[\s]([\w.]+)/.exec(b)||/(trident)\/([\w.]+)/.exec(b)||b.indexOf("compatible")<0||[];return"version"===a?c[2]:"webkit"===a?"chrome"===c[1]||"webkit"===c[1]:"ff"===a?"firefox"===c[1]:"msie"===a?"trident"===c[1]||"msie"===c[1]:c[1]===a}}}(Jodit.modules.Dom),function(a){"use strict";Jodit.defaultOptions=a.extend(!0,Jodit.defaultOptions,{}),Jodit.modules.ContextMenu=function(b){var c,d,e=function(){d=a('<div class="jodit_context_menu"></div>'),document.body.appendChild(d[0])},f=function(){d.removeClass("jodit_context_menu-show"),a(window).off("mouseup",f)},g=function(e,g,h){"array"===a.type(h)&&(d.html(""),h.forEach(function(e){if(e){var g=a('<a href="javascript:void(0)">'+(e.icon?b.icons.getSVGIcon(e.icon):"")+"<span></span></a>");
g.on("click",function(a){return e.exec.call(c,a),f(),!1}),g.find("span").text(b.i18n(e.title)),d.append(g)}}),d.css({left:e,top:g}),a(window).on("mouseup",f),d.addClass("jodit_context_menu-show"))};return c={init:e,show:g,hide:f}}}(Jodit.modules.Dom),function(a){"use strict";Jodit.modules.Cookie=function(a){var b={},c=function(a,b,c){var d,e;c?(e=new Date,e.setTime(e.getTime()+24*c*60*60*1e3),d="; expires="+e.toGMTString()):d="",document.cookie=a+"="+b+d+"; path=/"},d=function(a){var b,c,d=a+"=",e=document.cookie.split(";");for(b=0;b<e.length;b+=1){for(c=e[b];" "===c.charAt(0);)c=c.substring(1,c.length);if(0===c.indexOf(d))return c.substring(d.length,c.length)}return null},e=function(a){c(a,"",-1)};return b={set:c,get:d,remove:e}}}(Jodit.modules.Dom),function(a){"use strict";Jodit.defaultOptions=a.extend(!0,Jodit.defaultOptions,{dialog:{zIndex:100002,resizable:!0,draggable:!0,fullsize:!1,fullsizeButton:!1}}),Jodit.modules.Dialog=function(b,c){var d,e,f,g,h,i,j,k=a.extend({},Jodit.defaultOptions.dialog,b&&b.options?b.options.dialog:{},c),l=a('<div style="z-index:'+k.zIndex+'" class="jodit jodit_dialog_box"><div class="jodit_dialog_overlay"></div><div class="jodit_dialog"><div class="jodit_dialog_header non-selected"><h4></h4><a href="javascript:void(0)" title="Close" class="jodit_close">'+(b&&b.icons?b.icons.getSVGIcon("cancel"):"&times;")+'</a></div><div class="jodit_dialog_content"></div><div class="jodit_dialog_footer"></div>'+(k.resizable?'<div class="jodit_dialog_resizer"></div>':"")+"</div></div>"),m=document.body,n=!1,o=l.find(".jodit_dialog"),p=l.find(".jodit_dialog_resizer"),q=!1,r=function(){return o},s=function(){return l},t=function(a,b){a&&o.css({width:a}),b&&o.css({height:b})},u=function(a,b){var c=parseInt(window.innerWidth,10),d=parseInt(window.innerHeight,10),g=c/2-o.outerWidth()/2,h=d/2-o.outerHeight()/2;void 0!==a&&void 0!==b&&(e=a,f=b,q=Math.abs(a-g)>100||Math.abs(b-h)>100),o.css({left:(a||g)+"px",top:(b||h)+"px"})},v=function(a){l.$header.children().detach(),l.$header.empty(),l.$header.append(a)},w=function(a){l.$content.children().detach(),l.$content.empty(),l.$content.append(a)},x=function(a){l.$footer.children().detach(),l.$footer.empty(),l.$footer.append(a),o.toggleClass("with_footer",!!a)},y=function(){var b,c,d,e=0;return a(".jodit_dialog_box").each(function(){b=a(this).data("jodit_dialog"),c=parseInt(this.style.zIndex||0,10),b.isOpened()&&c>e&&(d=b,e=c)}),d},z=function(){var b=0;a(".jodit_dialog_box").each(function(){b=Math.max(parseInt(a(this).css("z-index"),10),b)}),l.css("z-index",b+1)},A=!1,B=function(b){return"boolean"!=typeof b&&(b=!l.hasClass("jodit_dialog_box-fullsize")),l.toggleClass("jodit_dialog_box-fullsize",b),a(document.body).add(document.body.parentNode).toggleClass("jodit_fullsize_box",b),A=b,b},C=function(a,c,g,h){b&&b.events&&b.events.fire(d,"beforeOpen")===!1||(n=g===!0,k.fullsizeButton&&l.$close.after(l.$fullsize),void 0!==c&&v(c),a&&w(a),l.addClass("active"),h&&l.addClass("jodit_modal"),u(e,f),z(),k.fullsize&&B(!0),b&&b.events&&b.events.fire(d,"afterOpen"))},D=function(){return l.hasClass("active")},E=!1,F=!1,G={x:0,y:0,w:0,h:0},H=function(a){(F||E)&&(F=!1,E=!1,b&&b.events&&b.events.fire(d,"endResize endMove"))},I=function(a){k.draggable&&(F=!0,g=a.clientX,h=a.clientY,G.x=parseInt(o.css("left"),10),G.y=parseInt(o.css("top"),10),z(),a.preventDefault(),b&&b.events&&b.events.fire(d,"startMove"))},J=function(a){F&&k.draggable&&(u(G.x+a.clientX-g,G.y+a.clientY-h),b&&b.events&&b.events.fire(d,"move",[a.clientX-g,a.clientY-h]),a.stopImmediatePropagation(),a.preventDefault()),E&&k.resizable&&(t(G.w+a.clientX-g,G.h+a.clientY-h),b&&b.events&&b.events.fire(d,"resizeDialog",[a.clientX-g,a.clientY-h]),a.stopImmediatePropagation(),a.preventDefault())},K=function(){l.remove(),l.empty(),a(window).off("onmouseup",H).off("onmousemove",J).off("keydown",i).off("resize",j)},L=function(){b&&b.events&&b.events.fire(d,"beforeClose"),l.removeClass("active"),A&&B(!1),n&&K(),b&&b.events.fire(d,"afterClose")},M=function(a){E=!0,g=a.clientX,h=a.clientY,G.w=o.outerWidth(),G.h=o.outerHeight(),b.events&&b.events.fire(d,"startResize")};return b&&b.options&&b.options.textIcons&&l.addClass("jodit_text_icons"),l.$header=l.find(".jodit_dialog_header>h4"),l.$content=l.find(".jodit_dialog_content"),l.$footer=l.find(".jodit_dialog_footer"),l.$close=l.find(".jodit_dialog_header>a.jodit_close"),l.$fullsize=a('<a href="javascript:void(0)" class="jodit_dialog_header_fullsize">'+(b&&b.icons?b.icons.getSVGIcon(k.fullsize?"fullsize":"shrink"):"")+"</a>"),a(m).append(l),l.on("close_dialog",function(){L()}),l.$close.on("mousedown",function(){L()}),l.$fullsize.on("click",function(){var a=B();b&&b.icons&&(this.innerHTML=b.icons.getSVGIcon(a?"shrink":"fullsize"))}),i=function(a){if(D()&&27===a.which){var b=y();b?b.close():L(),a.stopImmediatePropagation()}},j=function(a){!k.resizable||q||!D()||e||f||u()},a(window).on("mousemove",J).on("mouseup",H).on("keydown",i).on("resize",j),l.find(".jodit_dialog_header").on("mousedown",I),k.resizable&&p.on("mousedown",M),d={getBox:s,getDialog:r,getMaxZIndexDialog:y,setMaxZIndex:z,setTitle:v,setContent:w,setPosition:u,setFooter:x,setSize:t,maximization:B,open:C,isOpened:D,close:L,destroy:K},l.data("jodit_dialog",d),d},Jodit.Alert=function(b,c,d){"function"==typeof c&&(d=c,c=void 0);var e=new Jodit.modules.Dialog,f=a('<div class="jodit_alert"></div>').html(b),g=a('<a href="javascript:void(0)" style="float:left;" class="jodit_button">'+Jodit.prototype.i18n("Ok")+"</a>");g.on("click",function(){d&&"function"==typeof d&&d(e)===!1||e.close()}),e.setFooter([g]),e.open(f,c||"&nbsp;",!0,!0),g.focus()},Jodit.Promt=function(b,c,d,e){var f,g,h=new Jodit.modules.Dialog,i=new Jodit.modules.Icons,j=a('<form class="jodit_promt"></form>'),k=a('<input autofocus="true" type="text"/>'),l=a("<label></label>");"function"==typeof c&&(d=c,c=void 0),e&&k.attr("placeholder",e),l.html(b),j.append(l),j.append(k),f=a('<a href="javascript:void(0)" style="float:right;" class="jodit_button">'+i.getSVGIcon("cancel")+"&nbsp;"+Jodit.prototype.i18n("Cancel")+"</a>").on("click",h.close),g=a('<a href="javascript:void(0)" style="float:left;" class="jodit_button">'+i.getSVGIcon("check")+"&nbsp;"+Jodit.prototype.i18n("Ok")+"</a>"),g.on("click",function(){d&&"function"==typeof d&&d(k.val())===!1||h.close()}),j.on("submit",function(){return g.trigger("click"),!1}),h.setFooter([g,f]),h.open(j,c||"&nbsp;",!0,!0),k.focus()},Jodit.Confirm=function(b,c,d){var e,f,g=new Jodit.modules.Dialog,h=new Jodit.modules.Icons,i=a('<form class="jodit_promt"></form>'),j=a("<label></label>");"function"==typeof c&&(d=c,c=void 0),j.html(b),i.append(j),e=a('<a href="javascript:void(0)" style="float:right;" class="jodit_button">'+h.getSVGIcon("cancel")+"&nbsp;"+Jodit.prototype.i18n("Cancel")+"</a>").on("click",function(){d&&d(!1),g.close()}),f=a('<a href="javascript:void(0)" style="float:left;" class="jodit_button">'+h.getSVGIcon("check")+"&nbsp;"+Jodit.prototype.i18n("Yes")+"</a>").on("click",function(){d&&d(!0),g.close()}),i.on("submit",function(){return f.trigger("click"),!1}),g.setFooter([f,e]),g.open(i,c||"&nbsp;",!0,!0),f.focus()}}(Jodit.modules.Dom),function(a){"use strict";Jodit.modules.EnterKey=function(b){var c,d=b.getDocument(),e=function(){return b.selection.insertHTML("<br>"+Jodit.INVISIBLE_SPACE),!1},f=function(a,c){for(var d=c?"previousSibling":"nextSibling",e=a[d];b.node.isEmptyTextNode(e);)e.parentNode.removeChild(e),e=a[d];void 0===c&&f(a,!0)},g=function(a){return a.nextSibling&&a.nextSibling.nodeType===Node.TEXT_NODE&&"\n"===a.nextSibling.nodeValue?a.nextSibling:a},h=function(a,b,c){var e=a?d.createElement(a):b.cloneNode(!1);return e.hasAttribute("id")&&e.removeAttribute("id"),e.innerHTML=void 0!==c?c:Jodit.INVISIBLE_SPACE,e},i=function(c){var d,e,i,j=b.selection;return!j.range(0).collapsed||(i=j.current(),f(i),!(b.node.hasParent(i,"pre|td|th|blockquote")&&j.cursorInEdge(c)&&(d=b.node.parentNode(i,"pre|table|blockquote"),!b.node.find(d,b.editor,!1,c?"previousSibling":"nextSibling",!0,function(a){return a.nodeType!==Node.TEXT_NODE||this.parent.helper.trim(a.nodeValue).length})))||(e=h(b.options.enter),a(g(d))[c?"before":"after"](e),j.moveCursorTo(e),!1))},j=function(a){var c,d=b.selection,e=d.current();return b.$editor.find("img.jodit_focused_image").length?(b.$editor.find("img.jodit_focused_image").remove(),!1):e.nodeType===Node.TEXT_NODE&&0===b.helper.trim(e.nodeValue).length&&b.node.isBlock(e.parentNode)&&e.parentNode!==b.editor&&0===b.helper.trim(e.parentNode.innerHTML).length?(c=b.node.find(e.parentNode,b.editor,!1,"previousSibling",!0,b.node.isBlock),c&&d.moveCursorTo(c),e.parentNode.parentNode.removeChild(e.parentNode),!1):void 0},k=function(c){var i,j,k,l,m,n,o=b.selection,p=o.current(),q=d.createTextNode("\n");if(o.isCollapsed()||b.execCommand("Delete"),b.node.hasParent(p,"pre"))return f(p),o.range(0).deleteContents(),o.insertNode(q),n=d.createTextNode(Jodit.INVISIBLE_SPACE),o.insertNode(n),o.focusAfter(q),!1;if(b.node.hasParent(p,"li")&&(o.range(0).deleteContents(),i=b.node.parentNode(p,"li"),l=a.trim(b.helper.trimInvisible(i.textContent||i.innerText||"")),0===l.length&&(n=b.node.create("text",Jodit.INVISIBLE_SPACE),a(b.node.parentNode(p,"ol|ul")).after(n),a(b.node.parentNode(p,"li")).remove(),p=o.current(),o.moveCursorTo(n))),b.options.enter===Jodit.ENTER_BR||c.shiftKey||b.node.hasParent(p,"TH|TD|BLOCKQUOTE"))return e();if(b.node.hasParent(p,"H[1-6]|P|DIV|LI|A")){if(i=b.node.parentNode(p,"H[1-6]|P|DIV|LI|A",!0),f(i),o.cursorInEdge())return m=h([b.options.enter.toLowerCase(),"li"].indexOf(i.tagName.toLowerCase())===-1&&b.options.enter,i),a(g(i)).after(m),o.moveCursorTo(m),!1;if(o.cursorInEdge(!0))return m=i.parentNode.insertBefore(h(!1,i),i),o.moveCursorTo(i),!1;if(b.node.canSplitBlock(i)&&(k=o.range(0).cloneRange(),k.setEndAfter(i),j=k.extractContents(),j.firstChild))return m=j.firstChild,a(g(i)).after(j),m.normalize(),o.moveCursorTo(m),!1;m=h(b.options.enter),a(g(i)).after(m),o.moveCursorTo(m)}else m=h(b.options.enter),o.insertNode(m),o.moveCursorTo(m);return!1};return b.$editor.on("keydown",function(a){return a.which===Jodit.keys.BACKSPACE?j(a):a.which===Jodit.keys.ENTER?k(a):a.which===Jodit.keys.RIGHT||a.which===Jodit.keys.BOTTOM||a.which===Jodit.keys.LEFT||a.which===Jodit.keys.TOP?i(a.which===Jodit.keys.LEFT||a.which===Jodit.keys.TOP):void 0}),c={createNewBlock:h,enter:k,backspace:j,enterBr:e}}}(Jodit.modules.Dom),function(a){"use strict";Jodit.modules.Events=function(b){var c,d={},e=[],f=function(){return c},g=function(c,e,f){var g,h;if("string"===a.type(c)&&(f=e,e=c,c=b),void 0!==c.handlers){for(e=e.split(/[\s,]+/),g=0;g<e.length;g+=1)if(void 0!==c.handlers[e[g]])if(void 0!==f)for(h=0;h<c.handlers[e[g]].length;h+=1)c.handlers[e[g]][h]===f&&c.handlers[e[g]].splice(h,1);else delete c.handlers[e[g]];return d}},h=function(c,e,f){var g;for("string"===a.type(c)&&(f=e,e=c,c=b),e=e.split(/[\s,]+/),g=0;g<e.length;g+=1)void 0===c.handlers&&(c.handlers={}),void 0===c.handlers[e[g]]&&(c.handlers[e[g]]=[]),c.handlers[e[g]].push(f);return d},i=function(d,f,g){var h,i,j,k;if("string"===a.type(d)&&(g=f,f=d,d=b),void 0!==d.handlers){for(f=f.split(/[\s,]+/),h=0;h<f.length;h+=1)if(void 0!==d.handlers[f[h]]){for(e.push(f[h]),i=0;i<d.handlers[f[h]].length;i+=1)c=f[h],k=d.handlers[f[h]][i].apply(d,g||[]),void 0!==k&&(j=k);e.pop()}return j}};return d={current:f,on:h,off:g,fire:i}}}(Jodit.modules.Dom),function(a){"use strict";Jodit.defaultOptions=a.extend(!0,Jodit.defaultOptions,{filebrowser:{filter:function(a,b){return b=b.toLowerCase(),"string"==typeof a?a.toLowerCase().indexOf(b)!==-1:"string"==typeof a.name?a.name.toLowerCase().indexOf(b)!==-1:"string"!=typeof a.file||a.file.toLowerCase().indexOf(b)!==-1},sortBy:"changed",sort:function(a,b,c,d){var e,f,g=function(a,b){return a<b?-1:a>b?1:0};if("string"==typeof a)return g(a.toLowerCase(),b.toLowerCase());if(void 0===a[c]||"name"===c)return"string"==typeof a.name?g(a.name.toLowerCase(),b.name.toLowerCase()):"string"==typeof a.file?g(a.file.toLowerCase(),b.file.toLowerCase()):0;switch(c){case"changed":return e=new Date(a.changed),f=new Date(b.changed),f-e;case"size":return d.helper.humanSizeToBytes(a.size)-d.helper.humanSizeToBytes(b.size)}},editImage:!0,preview:!0,showPreviewNavigation:!0,showSelectButtonInPreview:!0,contextMenu:!0,howLongShowMsg:3e3,createNewFolder:!0,deleteFolder:!0,moveFolder:!0,moveFile:!0,showFoldersPanel:!0,width:763,height:400,buttons:["upload","remove","update","select","edit","tiles","list","filter","sort"],view:"tiles",isSuccess:function(a){return!a.error},getMsg:function(a){return a.msg},showFileName:!0,showFileSize:!0,showFileChangeTime:!0,getThumbTemplate:function(a,b,c){var d,e,f,g=this.options.filebrowser,h=(new Date).getTime();return"string"==typeof a?(d=a,e=a):(void 0!==a.file&&(d=a.file,e=a.file),a.thumb&&(e=a.thumb)),f='<div class="jodit_filebrowser_files_item-info">'+(g.showFileName?'<span class="jodit_filebrowser_files_item-info-filename">'+d+"</span>":"")+(g.showFileSize&&a.size?'<span class="jodit_filebrowser_files_item-info-filesize">'+a.size+"</span>":"")+(g.showFileChangeTime&&a.changed?'<span class="jodit_filebrowser_files_item-info-filesize">'+a.changed+"</span>":"")+"</div>",'<a class="jodit_filebrowser_files_item" href="'+this.helper.urlNormalize(c+b+d)+'" data-path="'+this.helper.pathNormalize(b?b+"/":"/")+'" data-name="'+d+'" title="'+d+'" data-url="'+this.helper.urlNormalize(c+b+d)+'"><img src="'+this.helper.urlNormalize(c+b+e)+"?_tmst="+h+'" alt="'+d+'">'+(g.showFileName||g.showFileSize&&a.size||g.showFileChangeTime&&a.changed?f:"")+"</a>"},ajax:{url:"",async:!0,beforeSend:function(){return!0},data:{},cache:!0,contentType:"application/x-www-form-urlencoded; charset=UTF-8",type:"POST",processData:!0,dataType:"json",headers:{},prepareData:function(a){return a},process:function(a){return{files:a.files||[],path:a.path,baseurl:a.baseurl,error:a.error,msg:a.msg}}},create:{data:{action:"create"}},getfilebyurl:{data:{action:"getfilebyurl"}},resize:{data:{action:"resize"}},crop:{data:{action:"crop"}},move:{data:{action:"move"}},remove:{data:{action:"remove"}},items:{data:{action:"items"}},folder:{data:{action:"folder"}},uploader:null}}),Jodit.modules.FileBrowser=function(b){var c,d,e,f,g={},h=b.options.filebrowser,i="",j="",k="tiles",l="changed",m=function(){return j},n=!1,o=new Jodit.modules.Dialog(b,{fullsizeButton:!0}),p=a('<div class="jodit_filebrowser_loader"><i class="jodit_icon-loader"></i></div>'),q=a('<div class="jodit_filebrowser non-selected">'+(h.showFoldersPanel?'<div class="jodit_filebrowser_tree"></div>':"")+'<div class="jodit_filebrowser_files"></div><div class="jodit_filebrowser_status"></div></div>'),r=q.find(".jodit_filebrowser_status"),s={upload:a('<div class="jodit_uploadfile_button jodit_button">'+b.icons.getSVGIcon("plus")+'<input type="file" accept="image/*" tabindex="-1" dir="auto" multiple=""/></div>'),remove:a('<div class="jodit_button disabled">'+b.icons.getSVGIcon("bin")+"</div>"),update:a('<div class="jodit_button">'+b.icons.getSVGIcon("update")+"</div>"),select:a('<div class="jodit_button disabled">'+b.icons.getSVGIcon("check")+"</div>"),edit:a('<div class="jodit_button disabled">'+b.icons.getSVGIcon("pencil")+"</div>"),addfolder:a('<a class="jodit_button addfolder" href="javascript:void(0)">'+b.icons.getSVGIcon("plus")+" Folder</div>"),tiles:a('<div class="jodit_button jodit_button_tiles disabled" href="javascript:void(0)">'+b.icons.getSVGIcon("th")+"</div>"),list:a('<div class="jodit_button disabled" href="javascript:void(0)">'+b.icons.getSVGIcon("th-list")+"</div>"),filter:a('<input class="jodit_input" type="text" placeholder="'+b.i18n("Filter")+'"/>'),sort:a('<select class="jodit_input"><option value="changed">'+b.i18n("Sort by changed")+'</option><option value="name">'+b.i18n("Sort by name")+'</option><option value="size">'+b.i18n("Sort by size")+"</option></select>")},t=q.find(".jodit_filebrowser_tree"),u=q.find(".jodit_filebrowser_files"),v=function(){return o.isOpened()&&q.is(":visible")},w=0,x=function(a,b){clearTimeout(w),r.removeClass("success").text(a).addClass("active"),b&&r.addClass("success"),w=setTimeout(function(){r.removeClass("active")},h.howLongShowMsg)},y=function(c,d){var e=[];a.each(c,function(c,f){var g=a('<a class="jodit_filebrowser_tree_item" href="javascript:void(0)" data-path="'+b.helper.pathNormalize(d+f)+'/"><span>'+f+"</span></a>");e.push(g),h.deleteFolder&&".."!==f&&"."!==f&&g.append(a('<i class="remove" data-path="'+b.helper.pathNormalize(d+f+"/")+'">&times;</i>'))}),t.html(e),h.createNewFolder&&t.append(s.addfolder)},z="",A=function(c,g,i){void 0===c&&void 0!==d&&(c=d,g=e,i=f),void 0===c&&(c=[]);var j=[],k=h.getThumbTemplate;"function"==typeof h.sort&&c.sort(function(a,c){return h.sort(a,c,l,b)}),a.each(c,function(a,c){(void 0===h.filter||h.filter(c,z))&&j.push(k.call(b,c,g,i))}),u.html(j),d=c,e=g,f=i},B=function(){s.remove.toggleClass("disabled",!u.find(">a.active").length),s.select.toggleClass("disabled",!u.find(">a.active").length),s.edit.toggleClass("disabled",1!==u.find(">a.active").length)},C=0,D=function(b,c,d){var e,f=a.extend(!0,{},void 0!==h[b]?h[b]:h.ajax);return f.data=f.prepareData(f.data),e=a.ajax(f),e.done(c),e.error(d),e},E=function(a,b,c){var d="getfilebyurl";h[d].data.url=a,D(d,function(a){h.isSuccess(a)?b(a.path,a.name):c(h.getMsg(a))},function(a){c(h.getMsg(a))})},F=function(a){a?h.items.data.path=a:a=h.items.data.path||"",h.items.url&&(u.addClass("active").html(p[0].cloneNode(!0)),C.abort&&C.abort(),C=D("items",function(a){var b=h.items.process.call(g,a);j=b.baseurl,A(b.files,b.path,b.baseurl),B()},function(a){x(b.i18n("Error on load list"))}))},G=0,H=function(a){a&&(h.folder.data.path=a),i=a,c&&c.setPath(i),h.showFoldersPanel&&(h.folder.url?(t.addClass("active").html(p[0].cloneNode(!0)),G.abort&&G.abort(),G=D("folder",function(a){var b=h.folder.process.call(g,a);i=b.path,y(b.files,b.path)},function(a){x(b.i18n("Error on load folders"))})):t.removeClass("active")),F(a)},I=function(a,b){h.create.data.path=b,h.create.data.name=a,D("create",function(a){h.isSuccess(a)?H(b):x(h.getMsg(a))},function(a){x(a)})},J=function(a,b){h.move.data.filepath=a,h.move.data.path=b,D("move",function(a){h.isSuccess(a)?H(b):x(h.getMsg(a))},function(a){x(a)})},K=function(a,b){h.remove.data.path=a,h.remove.data.target=b,D("remove",function(a){a=h.remove.process.call(g,a),h.isSuccess(a)?(B(),x(h.getMsg(a),!0)):x(h.getMsg(a))},function(a){x(a)})},L=function(){o.close()},M=function(b){return function(){if(u.find(">a.active").length){var c=[];u.find(">a.active").each(function(){c.push(a(this).data("url"))}),L(),a.isFunction(b)&&b({baseurl:"",files:c})}return!1}},N=function(c){if(h.items.url){u.off("dblclick").on("dblclick",">a",M(c)),s.select.off("click").on("click",M(c)),H(i);var d,e,f=['<span class="jodit_dialog_header_title">'+b.i18n("File Browser")+"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>"];for(d=0;d<h.buttons.length;d+=1)void 0!==s[h.buttons[d]]&&(h.editImage&&void 0!==Jodit.modules.ImageEditor||"edit"!==h.buttons[d])?f.push(s[h.buttons[d]]):a.isFunction(h.buttons[d])?f.push(h.buttons[d].call(b)):a.isPlainObject(h.buttons[d])&&h.buttons[d].exec&&h.buttons[d].name&&(e=a('<div class="jodit_button">'+b.icons.getSVGIcon(h.buttons[d].icon||h.buttons[d].name)+"</div>"),f.push(e),e.on("click",h.buttons[d].exec));o.open(q,f)}},O=function(a){x(h.getMsg(a))},P=function(a,c){h.isSuccess(c)?x(b.i18n("Files [1$] was uploaded",a.files.join(",")),!0):x(h.getMsg(c)),F()},Q=function(){k="list"===h.view?"list":"tiles",b.cookie.get("jodit_filebrowser_view")&&(k="list"===b.cookie.get("jodit_filebrowser_view")?"list":"tiles"),s[k].removeClass("disabled"),u.addClass("jodit_filebrowser_files_view-"+k),l=["changed","name","size"].indexOf(h.sortBy)!==-1?h.sortBy:"changed",b.cookie.get("jodit_filebrowser_sortby")&&(l=["changed","name","size"].indexOf(b.cookie.get("jodit_filebrowser_sortby"))!==-1?b.cookie.get("jodit_filebrowser_sortby"):"changed"),s.sort.val(l),j=a("base",b.getDocument()).length?a("base",b.getDocument()).attr("href"):location.protocol+"//"+location.host,void 0!==Jodit.modules.Uploader&&(c=new Jodit.modules.Uploader(b,a.extend(!0,{},b.options.uploader,h.uploader)),c.setPath(i),c.bind(q,P,O),c.bind(s.upload,P,O))},R=function(a,c,d,e,f){b.imageeditor&&b.imageeditor.open(a,function(a,b,g,j){void 0===h[b.action]&&(h[b.action]={}),void 0===h[b.action].data&&(h[b.action].data={action:b.action}),h[b.action].data.newname=a,h[b.action].data.box=b.box,h[b.action].data.path=d||i,h[b.action].data.file=c,D(b.action,function(a){h.isSuccess(a)?(H(),g(),e&&e()):(j(h.getMsg(a)),f&&f(h.getMsg(a)))},function(a){j(h.getMsg(a)),f&&f(h.getMsg(a))})})},S=!1,T={x:0,y:0},U={x:0,y:0};return s.tiles.add(s.list).on("click",function(){a(this).hasClass("jodit_button_tiles")?(k="tiles",s.list.addClass("disabled")):(k="list",s.tiles.addClass("disabled")),a(this).removeClass("disabled"),u.removeClass("jodit_filebrowser_files_view-tiles jodit_filebrowser_files_view-list"),u.addClass("jodit_filebrowser_files_view-"+k),b.cookie.set("jodit_filebrowser_view",k,31)}),s.sort.on("change",function(a){l=s.sort.val(),b.cookie.set("jodit_filebrowser_sortby",l,31),A()}).on("click mousedown",function(a){a.stopPropagation()}),s.filter.on("click mousedown",function(a){a.stopPropagation()}).on("keydown",b.helper.debounce(function(a){z=s.filter.val(),A()},300)),s.remove.on("click",function(){u.find(">a.active").length&&Jodit.Confirm(b.i18n("Are you shure?"),"",function(b){b&&(u.find(">a.active").each(function(){K(i,a(this).data("name"))}),B(),H(i))})}),s.edit.on("click",function(){1===u.find(">a.active").length&&R(u.find(">a.active").attr("href"),u.find(">a.active").data("name"))}),s.update.on("click",function(){H()}),t.on("click","a>i.remove",function(c){var d=this.parentNode,e=a(d).data("path");return Jodit.Confirm(b.i18n("Are you shure?"),"",function(b){b&&(K(e,a(d).data("name")),H(i))}),c.stopImmediatePropagation(),!1}).on("click","a",function(){a(this).hasClass("addfolder")?Jodit.Promt(b.i18n("Enter Directory name"),b.i18n("Create directory"),function(a){I(a,i)},b.i18n("type name")):H(a(this).data("path"))}).on("dragstart","a",function(a){n=this}).on("drop","a",function(b){if(h.moveFolder&&n){var c=a(n).data("path");if(!h.moveFolder&&a(n).hasClass("jodit_filebrowser_tree_item"))return!1;if(a(n).hasClass("jodit_filebrowser_files_item")&&(c+=a(n).data("name"),!h.moveFile))return!1;J(c,a(this).data("path"))}}),u.on("mousedown","a>img",function(b){U.x=b.clientX,U.y=b.clientY,T=a(this).offset(),S=a(this.cloneNode(!0)),S.css({"z-index":1e14,position:"fixed",display:"none",left:T.left,top:T.top,width:a(this).width(),height:a(this).height()}).appendTo("body").get(0)}).on("dragstart","a",function(b){n=this,b.originalEvent.dataTransfer.setData("text/plain",a(this).attr("href")),b.stopPropagation()}).on("contextmenu","a",function(c){if(h.contextMenu){var d=this;return b.contextmenu.show(c.pageX,c.pageY,[!(!h.editImage||void 0===Jodit.modules.ImageEditor)&&{icon:"pencil",title:"Edit",exec:function(){R(a(d).attr("href"),a(d).data("name"))}},{icon:"bin",title:"Delete",exec:function(){K(i,a(d).data("name")),B(),H(i)}},!!h.preview&&{icon:"eye",title:"Preview",exec:function(){var c=new Jodit.modules.Dialog(b),e=a('<div class="jodit_filebrowser_preview"><i class="jodit_icon-loader"></i></div>'),f=a(d).attr("href"),g=a(s.select[0].cloneNode(!0)),i=document.createElement("img"),j=function(){a(i).one("load",function(){if(e.empty(),h.showPreviewNavigation){var g=a('<a href="javascript:void(0)" class="jodit_filebrowser_preview_navigation jodit_filebrowser_preview_navigation-next">'+b.icons.getSVGIcon("angle-right")+"</a>"),k=a('<a href="javascript:void(0)" class="jodit_filebrowser_preview_navigation jodit_filebrowser_preview_navigation-prev">'+b.icons.getSVGIcon("angle-left")+"</a>");a(d).prev().length&&e.append(k),a(d).next().length&&e.append(g),a(g).add(k).on("click",function(b){d=a(this).hasClass("jodit_filebrowser_preview_navigation-next")?a(d).next().get(0):a(d).prev().get(0),e.html('<i class="jodit_icon-loader"></i>'),f=a(d).attr("href"),i.setAttribute("src",f),j()})}e.append(i),c.setPosition()}).each(function(){this.complete&&a(this).trigger("load")})};j(),i.setAttribute("src",f),c.setContent(e),h.showSelectButtonInPreview&&(g.removeAttr("disabled"),c.setTitle(g),g.on("click",function(){u.find(">a.active").removeClass("active"),a(d).addClass("active"),s.select.trigger("click"),c.close()})),c.open()}},{icon:"upload",title:"Download",exec:function(){window.open(a(d).attr("href"))}}]),!1}}).on("click",function(a){g.ctrlKey(a)||(u.find(">a").removeClass("active"),B())}).on("click","a",function(b){return g.ctrlKey(b)||u.find("a").removeClass("active"),a(this).toggleClass("active"),B(),b.stopPropagation(),!1}),a(document).on("dragover",function(b){v()&&S&&void 0!==b.originalEvent.clientX&&a(S).css({left:b.originalEvent.clientX+20,top:b.originalEvent.clientY+20,display:"block"})}),a(window).on("keydown",function(a){v()&&46===a.which&&s.remove.trigger("click")}).on("mouseup dragend",function(b){S&&(a(S).remove(),S=!1)}),o.setSize(h.width,h.height),h.getfilebyurl=a.extend(!0,{},h.ajax,h.getfilebyurl),h.crop=a.extend(!0,{},h.ajax,h.crop),h.resize=a.extend(!0,{},h.ajax,h.resize),h.create=a.extend(!0,{},h.ajax,h.create),h.move=a.extend(!0,{},h.ajax,h.move),h.remove=a.extend(!0,{},h.ajax,h.remove),h.folder=a.extend(!0,{},h.ajax,h.folder),h.items=a.extend(!0,{},h.ajax,h.items),g={init:Q,open:N,status:x,create:I,move:J,remove:K,getPathByUrl:E,getBaseUrl:m,openImageEditor:R,isOpened:v}}}(Jodit.modules.Dom),function(a){"use strict";Jodit.modules.Form=function(b){var c,d,e=function(c,d){var e=b.helper.normalizeColor(d),f=a('<div class="jodit_colorpicker"></div>'),g=function(b,c){var d,f,h,i=[];if(a.isPlainObject(b))for(f=Object.keys(b),d=0;d<f.length;d+=1)i.push('<div class="jodit_colorpicker_group jodit_colorpicker_group-'+f[d]+'">'),i.push(g(b[f[d]],f[d])),i.push("</div>");else if(a.isArray(b))for(d=0;d<b.length;d+=1)h=b[d],i.push("<a "+(e===h?' class="active" ':"")+' title="'+h+'" style="background-color:'+h+'" data-color="'+h+'" href="javascript:void(0)"><i class="icon"></i></a>');return i.join("")};return f.append(g(b.options.colors)),f.append('<a data-color="" href="javascript:void(0)">'+b.icons.getSVGIcon("eraser")+"</a>"),f.on("mousedown","a",function(d){var e,g=this;return f.find("a.active").removeClass("active"),a(g).addClass("active"),e=a(g).data("color"),c&&a.isFunction(c)&&c.call(b,e),d.stopPropagation(),!1}),f},f=function(d){var e=a('<div class="jodit_tabs"></div>'),f=a('<div class="jodit_tabs_wrapper"></div>'),g=a('<div class="jodit_tabs_buttons"></div>'),h=0;return e.append(g),e.append(f),a.each(d,function(d,e){var i=a('<div class="jodit_tab"></div>'),j=a('<a href="javascript:void(0);"></a>');j.html(b.i18n(d)),g.append(j),a.isFunction(e)?i.append('<div class="jodit_tab_empty"></div>'):i.append(e),f.append(i),j.on("mousedown",function(b){return g.find("a").removeClass("active"),f.find(".jodit_tab").removeClass("active"),j.addClass("active"),i.addClass("active"),a.isFunction(e)&&e.call(c),b.stopPropagation(),!1}),h+=1}),h?(g.find("a").css("width",(100/h).toFixed(10)+"%"),g.find("a:first-child").addClass("active"),f.find(".jodit_tab:first-child").addClass("active"),e):null},g=function(){return d},h=function(c,e){var g,h,i={};return c.upload&&b.options.uploader&&b.options.uploader.url&&(g=a('<div class="jodit_draganddrop_file_box"><strong>'+b.i18n("Drop image")+"</strong><span><br> "+b.i18n("or click")+'</span><input type="file" accept="image/*" tabindex="-1" dir="auto" multiple=""/></div>'),b.uploader.bind(g,function(d,e){a.isFunction(c.upload)&&c.upload.call(b,d)},function(a){b.events.fire("errorPopap",[b.options.uploader.getMsg(a)])}),i[b.icons.getSVGIcon("upload")+b.i18n("Upload")]=g),c.filebrowser&&b.filebrowser&&(b.options.filebrowser.url||b.options.filebrowser.ajax.url||b.options.filebrowser.items.url)&&(i[b.icons.getSVGIcon("folder")+b.i18n("Browse")]=function(){b.closeToolbarPopap(),b.filebrowser.open(c.filebrowser)}),c.url&&(h=a('<div><input required name="url" placeholder="http://" type="text"/><input name="text" placeholder="'+b.i18n("Alternative text")+'" type="text"/><button type="submit">'+b.i18n("Insert")+"</button></div>"),d=null,e&&(a(e).is("img")||a(e).find("img").length)&&(d=a(e).is("img")?e:a(e).find("img").get(0),h.find("input[name=url]").val(a(d).attr("src")),h.find("input[name=text]").val(a(d).attr("alt")),h.find("button").text("Update")),h.find("button").on("click",function(){return a.isFunction(c.url)&&c.url.call(b,h.find("input[name=url]").val(),h.find("input[name=text]").val()),!1}),i[b.icons.getSVGIcon("link")+" URL"]=h),f(i)};return c={buildColorPicker:e,buildTabs:f,imageSelector:h,getCurrentImage:g}}}(Jodit.modules.Dom),function(a){"use strict";Jodit.defaultOptions=a.extend(!0,Jodit.defaultOptions,{textIcons:!1}),Jodit.modules.Icons=function(a){var b,c="jodit_fontawesome_box",d=!1,e=document.createElement("div"),f=function(a){if(d)return'<i class="jodit_icon jodit_icon-text">'+a+"</i>";var b=window.location.href.replace(window.location.hash,"");return'<svg class="jodit_icon jodit_icon_'+a+'" ><use xlink:href="'+b+"#jodit-"+a+'" /></svg>'},g="PHN2ZyB2aWV3Qm94PSIwIDAgMTYgMTYiIHdpZHRoPSIxNiIgaGVpZ2h0PSIxNiI+PHN5bWJvbCB2aWV3Qm94PSIwIDAgMTc5MiAxNzkyIiBpZD0iam9kaXQtYWJvdXQiPjxwYXRoIGQ9Ik0xMDg4IDEyNTZ2MjQwcTAgMTYtMTIgMjh0LTI4IDEyaC0yNDBxLTE2IDAtMjgtMTJ0LTEyLTI4di0yNDBxMC0xNiAxMi0yOHQyOC0xMmgyNDBxMTYgMCAyOCAxMnQxMiAyOHptMzE2LTYwMHEwIDU0LTE1LjUgMTAxdC0zNSA3Ni41LTU1IDU5LjUtNTcuNSA0My41LTYxIDM1LjVxLTQxIDIzLTY4LjUgNjV0LTI3LjUgNjdxMCAxNy0xMiAzMi41dC0yOCAxNS41aC0yNDBxLTE1IDAtMjUuNS0xOC41dC0xMC41LTM3LjV2LTQ1cTAtODMgNjUtMTU2LjV0MTQzLTEwOC41cTU5LTI3IDg0LTU2dDI1LTc2cTAtNDItNDYuNS03NHQtMTA3LjUtMzJxLTY1IDAtMTA4IDI5LTM1IDI1LTEwNyAxMTUtMTMgMTYtMzEgMTYtMTIgMC0yNS04bC0xNjQtMTI1cS0xMy0xMC0xNS41LTI1dDUuNS0yOHExNjAtMjY2IDQ2NC0yNjYgODAgMCAxNjEgMzF0MTQ2IDgzIDEwNiAxMjcuNSA0MSAxNTguNXoiLz48L3N5bWJvbD48c3ltYm9sIHZpZXdCb3g9IjAgMCAxOC4xNTEgMTguMTUxIiBpZD0iam9kaXQtYWRkY29sdW1uIj4gPGc+IDxnPiA8cGF0aCBzdHlsZT0iZmlsbDojMDMwMTA0OyIgZD0iTTYuMjM3LDE2LjU0NkgzLjY0OVYxLjYwNGg1LjkxNnY1LjcyOGMwLjQ3NC0wLjEyMiwwLjk2OC0wLjE5NCwxLjQ3OS0wLjE5NA0KCQkJYzAuMDQyLDAsMC4wODMsMC4wMDYsMC4xMjUsMC4wMDZWMEgyLjA0NHYxOC4xNWg1LjkzNEM3LjI5NSwxNy43MzYsNi43MDQsMTcuMTksNi4yMzcsMTYuNTQ2eiIvPiA8cGF0aCBzdHlsZT0iZmlsbDojMDMwMTA0OyIgZD0iTTExLjE2OSw4LjI3NWMtMi43MjMsMC00LjkzOCwyLjIxNS00LjkzOCw0LjkzOHMyLjIxNSw0LjkzOCw0LjkzOCw0LjkzOHM0LjkzOC0yLjIxNSw0LjkzOC00LjkzOA0KCQkJUzEzLjg5Miw4LjI3NSwxMS4xNjksOC4yNzV6IE0xMS4xNjksMTYuODFjLTEuOTgzLDAtMy41OTgtMS42MTItMy41OTgtMy41OThjMC0xLjk4MywxLjYxNC0zLjU5NywzLjU5OC0zLjU5Nw0KCQkJczMuNTk3LDEuNjEzLDMuNTk3LDMuNTk3QzE0Ljc2NiwxNS4xOTgsMTMuMTUzLDE2LjgxLDExLjE2OSwxNi44MXoiLz4gPHBvbHlnb24gc3R5bGU9ImZpbGw6IzAzMDEwNDsiIHBvaW50cz0iMTEuNzkyLDExLjA3MyAxMC41MDIsMTEuMDczIDEwLjUwMiwxMi41NzggOS4wMywxMi41NzggOS4wMywxMy44NjggMTAuNTAyLDEzLjg2OCANCgkJCTEwLjUwMiwxNS4zNTIgMTEuNzkyLDE1LjM1MiAxMS43OTIsMTMuODY4IDEzLjMwOSwxMy44NjggMTMuMzA5LDEyLjU3OCAxMS43OTIsMTIuNTc4IAkJIi8+IDwvZz4gPC9nPiAgICAgICAgICAgICAgICA8L3N5bWJvbD48c3ltYm9sIHZpZXdCb3g9IjAgMCA0MzIgNDMyIiBpZD0iam9kaXQtYWRkcm93Ij4gPGc+IDxnPiA8cG9seWdvbiBwb2ludHM9IjIwMy42ODgsOTYgMCw5NiAwLDE0NCAxNTUuNjg4LDE0NCAJCSIvPiA8cG9seWdvbiBwb2ludHM9IjE1NS43MTksMjg4IDAsMjg4IDAsMzM2IDIwMy43MTksMzM2IAkJIi8+IDxyZWN0IHg9IjI1MiIgeT0iOTYiIHdpZHRoPSI4NCIgaGVpZ2h0PSI0OCIvPiA8cmVjdCB3aWR0aD0iMzM2IiBoZWlnaHQ9IjQ4Ii8+IDxyZWN0IHg9IjI1MiIgeT0iMjg4IiB3aWR0aD0iODQiIGhlaWdodD0iNDgiLz4gPHJlY3QgeT0iMzg0IiB3aWR0aD0iMzM2IiBoZWlnaHQ9IjQ4Ii8+IDxwYXRoIGQ9Ik05Ny44NDQsMjMwLjEyNWMtMy43MDEtMy43MDMtNS44NTYtOC45MDYtNS44NTYtMTQuMTQxczIuMTU0LTEwLjQzOCw1Ljg1Ni0xNC4xNDFsOS44NDQtOS44NDRIMHY0OGgxMDcuNzE5DQoJCQlMOTcuODQ0LDIzMC4xMjV6Ii8+IDxwb2x5Z29uIHBvaW50cz0iMjMyLDE3NiAyMzIsOTYgMTEyLDIxNiAyMzIsMzM2IDIzMiwyNTYgNDMyLDI1NiA0MzIsMTc2IAkJIi8+IDwvZz4gICAgICAgICAgICAgICAgPC9nPiAgICAgICAgICAgICAgICA8L3N5bWJvbD48c3ltYm9sIHZpZXdCb3g9IjAgMCAxNzkyIDE3OTIiIGlkPSJqb2RpdC1hbmdsZS1sZWZ0Ij48cGF0aCBkPSJNMTIwMyA1NDRxMCAxMy0xMCAyM2wtMzkzIDM5MyAzOTMgMzkzcTEwIDEwIDEwIDIzdC0xMCAyM2wtNTAgNTBxLTEwIDEwLTIzIDEwdC0yMy0xMGwtNDY2LTQ2NnEtMTAtMTAtMTAtMjN0MTAtMjNsNDY2LTQ2NnExMC0xMCAyMy0xMHQyMyAxMGw1MCA1MHExMCAxMCAxMCAyM3oiLz48L3N5bWJvbD48c3ltYm9sIHZpZXdCb3g9IjAgMCAxNzkyIDE3OTIiIGlkPSJqb2RpdC1hbmdsZS1yaWdodCI+PHBhdGggZD0iTTExNzEgOTYwcTAgMTMtMTAgMjNsLTQ2NiA0NjZxLTEwIDEwLTIzIDEwdC0yMy0xMGwtNTAtNTBxLTEwLTEwLTEwLTIzdDEwLTIzbDM5My0zOTMtMzkzLTM5M3EtMTAtMTAtMTAtMjN0MTAtMjNsNTAtNTBxMTAtMTAgMjMtMTB0MjMgMTBsNDY2IDQ2NnExMCAxMCAxMCAyM3oiLz48L3N5bWJvbD48c3ltYm9sIHZpZXdCb3g9IjAgMCAxNzkyIDE3OTIiIGlkPSJqb2RpdC1hcnJvd3MtYWx0Ij48cGF0aCBkPSJNMTQxMSA1NDFsLTM1NSAzNTUgMzU1IDM1NSAxNDQtMTQ0cTI5LTMxIDcwLTE0IDM5IDE3IDM5IDU5djQ0OHEwIDI2LTE5IDQ1dC00NSAxOWgtNDQ4cS00MiAwLTU5LTQwLTE3LTM5IDE0LTY5bDE0NC0xNDQtMzU1LTM1NS0zNTUgMzU1IDE0NCAxNDRxMzEgMzAgMTQgNjktMTcgNDAtNTkgNDBoLTQ0OHEtMjYgMC00NS0xOXQtMTktNDV2LTQ0OHEwLTQyIDQwLTU5IDM5LTE3IDY5IDE0bDE0NCAxNDQgMzU1LTM1NS0zNTUtMzU1LTE0NCAxNDRxLTE5IDE5LTQ1IDE5LTEyIDAtMjQtNS00MC0xNy00MC01OXYtNDQ4cTAtMjYgMTktNDV0NDUtMTloNDQ4cTQyIDAgNTkgNDAgMTcgMzktMTQgNjlsLTE0NCAxNDQgMzU1IDM1NSAzNTUtMzU1LTE0NC0xNDRxLTMxLTMwLTE0LTY5IDE3LTQwIDU5LTQwaDQ0OHEyNiAwIDQ1IDE5dDE5IDQ1djQ0OHEwIDQyLTM5IDU5LTEzIDUtMjUgNS0yNiAwLTQ1LTE5eiIvPjwvc3ltYm9sPjxzeW1ib2wgdmlld0JveD0iMCAwIDE3OTIgMTc5MiIgaWQ9ImpvZGl0LWFycm93cy1oIj48cGF0aCBkPSJNMTc5MiA4OTZxMCAyNi0xOSA0NWwtMjU2IDI1NnEtMTkgMTktNDUgMTl0LTQ1LTE5LTE5LTQ1di0xMjhoLTEwMjR2MTI4cTAgMjYtMTkgNDV0LTQ1IDE5LTQ1LTE5bC0yNTYtMjU2cS0xOS0xOS0xOS00NXQxOS00NWwyNTYtMjU2cTE5LTE5IDQ1LTE5dDQ1IDE5IDE5IDQ1djEyOGgxMDI0di0xMjhxMC0yNiAxOS00NXQ0NS0xOSA0NSAxOWwyNTYgMjU2cTE5IDE5IDE5IDQ1eiIvPjwvc3ltYm9sPjxzeW1ib2wgdmlld0JveD0iMCAwIDE3OTIgMTc5MiIgaWQ9ImpvZGl0LWF0dGFjaG1lbnQiPjxwYXRoIGQ9Ik0xNTk2IDEzODVxMCAxMTctNzkgMTk2dC0xOTYgNzlxLTEzNSAwLTIzNS0xMDBsLTc3Ny03NzZxLTExMy0xMTUtMTEzLTI3MSAwLTE1OSAxMTAtMjcwdDI2OS0xMTFxMTU4IDAgMjczIDExM2w2MDUgNjA2cTEwIDEwIDEwIDIyIDAgMTYtMzAuNSA0Ni41dC00Ni41IDMwLjVxLTEzIDAtMjMtMTBsLTYwNi02MDdxLTc5LTc3LTE4MS03Ny0xMDYgMC0xNzkgNzV0LTczIDE4MXEwIDEwNSA3NiAxODFsNzc2IDc3N3E2MyA2MyAxNDUgNjMgNjQgMCAxMDYtNDJ0NDItMTA2cTAtODItNjMtMTQ1bC01ODEtNTgxcS0yNi0yNC02MC0yNC0yOSAwLTQ4IDE5dC0xOSA0OHEwIDMyIDI1IDU5bDQxMCA0MTBxMTAgMTAgMTAgMjIgMCAxNi0zMSA0N3QtNDcgMzFxLTEyIDAtMjItMTBsLTQxMC00MTBxLTYzLTYxLTYzLTE0OSAwLTgyIDU3LTEzOXQxMzktNTdxODggMCAxNDkgNjNsNTgxIDU4MXExMDAgOTggMTAwIDIzNXoiLz48L3N5bWJvbD48c3ltYm9sIHZpZXdCb3g9IjAgMCAxNzkyIDE3OTIiIGlkPSJqb2RpdC1iaW4iPjxwYXRoIGQ9Ik03MDQgMTM3NnYtNzA0cTAtMTQtOS0yM3QtMjMtOWgtNjRxLTE0IDAtMjMgOXQtOSAyM3Y3MDRxMCAxNCA5IDIzdDIzIDloNjRxMTQgMCAyMy05dDktMjN6bTI1NiAwdi03MDRxMC0xNC05LTIzdC0yMy05aC02NHEtMTQgMC0yMyA5dC05IDIzdjcwNHEwIDE0IDkgMjN0MjMgOWg2NHExNCAwIDIzLTl0OS0yM3ptMjU2IDB2LTcwNHEwLTE0LTktMjN0LTIzLTloLTY0cS0xNCAwLTIzIDl0LTkgMjN2NzA0cTAgMTQgOSAyM3QyMyA5aDY0cTE0IDAgMjMtOXQ5LTIzem0tNTQ0LTk5Mmg0NDhsLTQ4LTExN3EtNy05LTE3LTExaC0zMTdxLTEwIDItMTcgMTF6bTkyOCAzMnY2NHEwIDE0LTkgMjN0LTIzIDloLTk2djk0OHEwIDgzLTQ3IDE0My41dC0xMTMgNjAuNWgtODMycS02NiAwLTExMy01OC41dC00Ny0xNDEuNXYtOTUyaC05NnEtMTQgMC0yMy05dC05LTIzdi02NHEwLTE0IDktMjN0MjMtOWgzMDlsNzAtMTY3cTE1LTM3IDU0LTYzdDc5LTI2aDMyMHE0MCAwIDc5IDI2dDU0IDYzbDcwIDE2N2gzMDlxMTQgMCAyMyA5dDkgMjN6Ii8+PC9zeW1ib2w+PHN5bWJvbCB2aWV3Qm94PSIwIDAgMTc5MiAxNzkyIiBpZD0iam9kaXQtYm9sZCI+PHBhdGggZD0iTTc0NyAxNTIxcTc0IDMyIDE0MCAzMiAzNzYgMCAzNzYtMzM1IDAtMTE0LTQxLTE4MC0yNy00NC02MS41LTc0dC02Ny41LTQ2LjUtODAuNS0yNS04NC0xMC41LTk0LjUtMnEtNzMgMC0xMDEgMTAgMCA1My0uNSAxNTl0LS41IDE1OHEwIDgtMSA2Ny41dC0uNSA5Ni41IDQuNSA4My41IDEyIDY2LjV6bS0xNC03NDZxNDIgNyAxMDkgNyA4MiAwIDE0My0xM3QxMTAtNDQuNSA3NC41LTg5LjUgMjUuNS0xNDJxMC03MC0yOS0xMjIuNXQtNzktODItMTA4LTQzLjUtMTI0LTE0cS01MCAwLTEzMCAxMyAwIDUwIDQgMTUxdDQgMTUycTAgMjctLjUgODB0LS41IDc5cTAgNDYgMSA2OXptLTU0MSA4ODlsMi05NHExNS00IDg1LTE2dDEwNi0yN3E3LTEyIDEyLjUtMjd0OC41LTMzLjUgNS41LTMyLjUgMy0zNy41LjUtMzR2LTY1LjVxMC05ODItMjItMTAyNS00LTgtMjItMTQuNXQtNDQuNS0xMS00OS41LTctNDguNS00LjUtMzAuNS0zbC00LTgzcTk4LTIgMzQwLTExLjV0MzczLTkuNXEyMyAwIDY4LjUuNXQ2Ny41LjVxNzAgMCAxMzYuNSAxM3QxMjguNSA0MiAxMDggNzEgNzQgMTA0LjUgMjggMTM3LjVxMCA1Mi0xNi41IDk1LjV0LTM5IDcyLTY0LjUgNTcuNS03MyA0NS04NCA0MHExNTQgMzUgMjU2LjUgMTM0dDEwMi41IDI0OHEwIDEwMC0zNSAxNzkuNXQtOTMuNSAxMzAuNS0xMzggODUuNS0xNjMuNSA0OC41LTE3NiAxNHEtNDQgMC0xMzItM3QtMTMyLTNxLTEwNiAwLTMwNyAxMXQtMjMxIDEyeiIvPjwvc3ltYm9sPjxzeW1ib2wgdmlld0JveD0iMCAwIDE3OTIgMTc5MiIgaWQ9ImpvZGl0LWJydXNoIj48cGF0aCBkPSJNODk2IDExNTJxMC0zNi0yMC02OS0xLTEtMTUuNS0yMi41dC0yNS41LTM4LTI1LTQ0LTIxLTUwLjVxLTQtMTYtMjEtMTZ0LTIxIDE2cS03IDIzLTIxIDUwLjV0LTI1IDQ0LTI1LjUgMzgtMTUuNSAyMi41cS0yMCAzMy0yMCA2OSAwIDUzIDM3LjUgOTAuNXQ5MC41IDM3LjUgOTAuNS0zNy41IDM3LjUtOTAuNXptNTEyLTEyOHEwIDIxMi0xNTAgMzYydC0zNjIgMTUwLTM2Mi0xNTAtMTUwLTM2MnEwLTE0NSA4MS0yNzUgNi05IDYyLjUtOTAuNXQxMDEtMTUxIDk5LjUtMTc4IDgzLTIwMS41cTktMzAgMzQtNDd0NTEtMTcgNTEuNSAxNyAzMy41IDQ3cTI4IDkzIDgzIDIwMS41dDk5LjUgMTc4IDEwMSAxNTEgNjIuNSA5MC41cTgxIDEyNyA4MSAyNzV6Ii8+PC9zeW1ib2w+PHN5bWJvbCB2aWV3Qm94PSIwIDAgMTc5MiAxNzkyIiBpZD0iam9kaXQtY2FuY2VsIj48cGF0aCBkPSJNMTQ5MCAxMzIycTAgNDAtMjggNjhsLTEzNiAxMzZxLTI4IDI4LTY4IDI4dC02OC0yOGwtMjk0LTI5NC0yOTQgMjk0cS0yOCAyOC02OCAyOHQtNjgtMjhsLTEzNi0xMzZxLTI4LTI4LTI4LTY4dDI4LTY4bDI5NC0yOTQtMjk0LTI5NHEtMjgtMjgtMjgtNjh0MjgtNjhsMTM2LTEzNnEyOC0yOCA2OC0yOHQ2OCAyOGwyOTQgMjk0IDI5NC0yOTRxMjgtMjggNjgtMjh0NjggMjhsMTM2IDEzNnEyOCAyOCAyOCA2OHQtMjggNjhsLTI5NCAyOTQgMjk0IDI5NHEyOCAyOCAyOCA2OHoiLz48L3N5bWJvbD48c3ltYm9sIHZpZXdCb3g9IjAgMCAxNzkyIDE3OTIiIGlkPSJqb2RpdC1jZW50ZXIiPjxwYXRoIGQ9Ik0xNzkyIDEzNDR2MTI4cTAgMjYtMTkgNDV0LTQ1IDE5aC0xNjY0cS0yNiAwLTQ1LTE5dC0xOS00NXYtMTI4cTAtMjYgMTktNDV0NDUtMTloMTY2NHEyNiAwIDQ1IDE5dDE5IDQ1em0tMzg0LTM4NHYxMjhxMCAyNi0xOSA0NXQtNDUgMTloLTg5NnEtMjYgMC00NS0xOXQtMTktNDV2LTEyOHEwLTI2IDE5LTQ1dDQ1LTE5aDg5NnEyNiAwIDQ1IDE5dDE5IDQ1em0yNTYtMzg0djEyOHEwIDI2LTE5IDQ1dC00NSAxOWgtMTQwOHEtMjYgMC00NS0xOXQtMTktNDV2LTEyOHEwLTI2IDE5LTQ1dDQ1LTE5aDE0MDhxMjYgMCA0NSAxOXQxOSA0NXptLTM4NC0zODR2MTI4cTAgMjYtMTkgNDV0LTQ1IDE5aC02NDBxLTI2IDAtNDUtMTl0LTE5LTQ1di0xMjhxMC0yNiAxOS00NXQ0NS0xOWg2NDBxMjYgMCA0NSAxOXQxOSA0NXoiLz48L3N5bWJvbD48c3ltYm9sIHZpZXdCb3g9IjAgMCAxNzkyIDE3OTIiIGlkPSJqb2RpdC1jaGFpbi1icm9rZW4iPjxwYXRoIGQ9Ik01MDMgMTI3MWwtMjU2IDI1NnEtMTAgOS0yMyA5LTEyIDAtMjMtOS05LTEwLTktMjN0OS0yM2wyNTYtMjU2cTEwLTkgMjMtOXQyMyA5cTkgMTAgOSAyM3QtOSAyM3ptMTY5IDQxdjMyMHEwIDE0LTkgMjN0LTIzIDktMjMtOS05LTIzdi0zMjBxMC0xNCA5LTIzdDIzLTkgMjMgOSA5IDIzem0tMjI0LTIyNHEwIDE0LTkgMjN0LTIzIDloLTMyMHEtMTQgMC0yMy05dC05LTIzIDktMjMgMjMtOWgzMjBxMTQgMCAyMyA5dDkgMjN6bTEyNjQgMTI4cTAgMTIwLTg1IDIwM2wtMTQ3IDE0NnEtODMgODMtMjAzIDgzLTEyMSAwLTIwNC04NWwtMzM0LTMzNXEtMjEtMjEtNDItNTZsMjM5LTE4IDI3MyAyNzRxMjcgMjcgNjggMjcuNXQ2OC0yNi41bDE0Ny0xNDZxMjgtMjggMjgtNjcgMC00MC0yOC02OGwtMjc0LTI3NSAxOC0yMzlxMzUgMjEgNTYgNDJsMzM2IDMzNnE4NCA4NiA4NCAyMDR6bS02MTctNzI0bC0yMzkgMTgtMjczLTI3NHEtMjgtMjgtNjgtMjgtMzkgMC02OCAyN2wtMTQ3IDE0NnEtMjggMjgtMjggNjcgMCA0MCAyOCA2OGwyNzQgMjc0LTE4IDI0MHEtMzUtMjEtNTYtNDJsLTMzNi0zMzZxLTg0LTg2LTg0LTIwNCAwLTEyMCA4NS0yMDNsMTQ3LTE0NnE4My04MyAyMDMtODMgMTIxIDAgMjA0IDg1bDMzNCAzMzVxMjEgMjEgNDIgNTZ6bTYzMyA4NHEwIDE0LTkgMjN0LTIzIDloLTMyMHEtMTQgMC0yMy05dC05LTIzIDktMjMgMjMtOWgzMjBxMTQgMCAyMyA5dDkgMjN6bS01NDQtNTQ0djMyMHEwIDE0LTkgMjN0LTIzIDktMjMtOS05LTIzdi0zMjBxMC0xNCA5LTIzdDIzLTkgMjMgOSA5IDIzem00MDcgMTUxbC0yNTYgMjU2cS0xMSA5LTIzIDl0LTIzLTlxLTktMTAtOS0yM3Q5LTIzbDI1Ni0yNTZxMTAtOSAyMy05dDIzIDlxOSAxMCA5IDIzdC05IDIzeiIvPjwvc3ltYm9sPjxzeW1ib2wgdmlld0JveD0iMCAwIDE3OTIgMTc5MiIgaWQ9ImpvZGl0LWNoZWNrLXNxdWFyZSI+PHBhdGggZD0iTTgxMyAxMjk5bDYxNC02MTRxMTktMTkgMTktNDV0LTE5LTQ1bC0xMDItMTAycS0xOS0xOS00NS0xOXQtNDUgMTlsLTQ2NyA0NjctMjExLTIxMXEtMTktMTktNDUtMTl0LTQ1IDE5bC0xMDIgMTAycS0xOSAxOS0xOSA0NXQxOSA0NWwzNTggMzU4cTE5IDE5IDQ1IDE5dDQ1LTE5em04NTEtODgzdjk2MHEwIDExOS04NC41IDIwMy41dC0yMDMuNSA4NC41aC05NjBxLTExOSAwLTIwMy41LTg0LjV0LTg0LjUtMjAzLjV2LTk2MHEwLTExOSA4NC41LTIwMy41dDIwMy41LTg0LjVoOTYwcTExOSAwIDIwMy41IDg0LjV0ODQuNSAyMDMuNXoiLz48L3N5bWJvbD48c3ltYm9sIHZpZXdCb3g9IjAgMCAxNzkyIDE3OTIiIGlkPSJqb2RpdC1jaGVjayI+PHBhdGggZD0iTTE0NzIgOTMwdjMxOHEwIDExOS04NC41IDIwMy41dC0yMDMuNSA4NC41aC04MzJxLTExOSAwLTIwMy41LTg0LjV0LTg0LjUtMjAzLjV2LTgzMnEwLTExOSA4NC41LTIwMy41dDIwMy41LTg0LjVoODMycTYzIDAgMTE3IDI1IDE1IDcgMTggMjMgMyAxNy05IDI5bC00OSA0OXEtMTAgMTAtMjMgMTAtMyAwLTktMi0yMy02LTQ1LTZoLTgzMnEtNjYgMC0xMTMgNDd0LTQ3IDExM3Y4MzJxMCA2NiA0NyAxMTN0MTEzIDQ3aDgzMnE2NiAwIDExMy00N3Q0Ny0xMTN2LTI1NHEwLTEzIDktMjJsNjQtNjRxMTAtMTAgMjMtMTAgNiAwIDEyIDMgMjAgOCAyMCAyOXptMjMxLTQ4OWwtODE0IDgxNHEtMjQgMjQtNTcgMjR0LTU3LTI0bC00MzAtNDMwcS0yNC0yNC0yNC01N3QyNC01N2wxMTAtMTEwcTI0LTI0IDU3LTI0dDU3IDI0bDI2MyAyNjMgNjQ3LTY0N3EyNC0yNCA1Ny0yNHQ1NyAyNGwxMTAgMTEwcTI0IDI0IDI0IDU3dC0yNCA1N3oiLz48L3N5bWJvbD48c3ltYm9sIHZpZXdCb3g9IjAgMCAxNzkyIDE3OTIiIGlkPSJqb2RpdC1jcm9wIj48cGF0aCBkPSJNNjIxIDEyODBoNTk1di01OTV6bS00NS00NWw1OTUtNTk1aC01OTV2NTk1em0xMTUyIDc3djE5MnEwIDE0LTkgMjN0LTIzIDloLTIyNHYyMjRxMCAxNC05IDIzdC0yMyA5aC0xOTJxLTE0IDAtMjMtOXQtOS0yM3YtMjI0aC04NjRxLTE0IDAtMjMtOXQtOS0yM3YtODY0aC0yMjRxLTE0IDAtMjMtOXQtOS0yM3YtMTkycTAtMTQgOS0yM3QyMy05aDIyNHYtMjI0cTAtMTQgOS0yM3QyMy05aDE5MnExNCAwIDIzIDl0OSAyM3YyMjRoODUxbDI0Ni0yNDdxMTAtOSAyMy05dDIzIDlxOSAxMCA5IDIzdC05IDIzbC0yNDcgMjQ2djg1MWgyMjRxMTQgMCAyMyA5dDkgMjN6Ii8+PC9zeW1ib2w+PHN5bWJvbCB2aWV3Qm94PSIwIDAgMTc5MiAxNzkyIiBpZD0iam9kaXQtZGVkZW50Ij48cGF0aCBkPSJNMzg0IDU0NHY1NzZxMCAxMy05LjUgMjIuNXQtMjIuNSA5LjVxLTE0IDAtMjMtOWwtMjg4LTI4OHEtOS05LTktMjN0OS0yM2wyODgtMjg4cTktOSAyMy05IDEzIDAgMjIuNSA5LjV0OS41IDIyLjV6bTE0MDggNzY4djE5MnEwIDEzLTkuNSAyMi41dC0yMi41IDkuNWgtMTcyOHEtMTMgMC0yMi41LTkuNXQtOS41LTIyLjV2LTE5MnEwLTEzIDkuNS0yMi41dDIyLjUtOS41aDE3MjhxMTMgMCAyMi41IDkuNXQ5LjUgMjIuNXptMC0zODR2MTkycTAgMTMtOS41IDIyLjV0LTIyLjUgOS41aC0xMDg4cS0xMyAwLTIyLjUtOS41dC05LjUtMjIuNXYtMTkycTAtMTMgOS41LTIyLjV0MjIuNS05LjVoMTA4OHExMyAwIDIyLjUgOS41dDkuNSAyMi41em0wLTM4NHYxOTJxMCAxMy05LjUgMjIuNXQtMjIuNSA5LjVoLTEwODhxLTEzIDAtMjIuNS05LjV0LTkuNS0yMi41di0xOTJxMC0xMyA5LjUtMjIuNXQyMi41LTkuNWgxMDg4cTEzIDAgMjIuNSA5LjV0OS41IDIyLjV6bTAtMzg0djE5MnEwIDEzLTkuNSAyMi41dC0yMi41IDkuNWgtMTcyOHEtMTMgMC0yMi41LTkuNXQtOS41LTIyLjV2LTE5MnEwLTEzIDkuNS0yMi41dDIyLjUtOS41aDE3MjhxMTMgMCAyMi41IDkuNXQ5LjUgMjIuNXoiLz48L3N5bWJvbD48c3ltYm9sIHZpZXdCb3g9IjAgMCAxNiAxNiIgaWQ9ImpvZGl0LWVyYXNlciI+PHBhdGggZD0iTTE2IDl2LTZoLTN2LTFjMC0wLjU1LTAuNDUtMS0xLTFoLTExYy0wLjU1IDAtMSAwLjQ1LTEgMXYzYzAgMC41NSAwLjQ1IDEgMSAxaDExYzAuNTUgMCAxLTAuNDUgMS0xdi0xaDJ2NGgtOXYyaC0wLjVjLTAuMjc2IDAtMC41IDAuMjI0LTAuNSAwLjV2NWMwIDAuMjc2IDAuMjI0IDAuNSAwLjUgMC41aDJjMC4yNzYgMCAwLjUtMC4yMjQgMC41LTAuNXYtNWMwLTAuMjc2LTAuMjI0LTAuNS0wLjUtMC41aC0wLjV2LTFoOXpNMTIgM2gtMTF2LTFoMTF2MXoiLz48L3N5bWJvbD48c3ltYm9sIHZpZXdCb3g9IjAgMCAxNzkyIDE3OTIiIGlkPSJqb2RpdC1leWUiPjxwYXRoIGQ9Ik0xNjY0IDk2MHEtMTUyLTIzNi0zODEtMzUzIDYxIDEwNCA2MSAyMjUgMCAxODUtMTMxLjUgMzE2LjV0LTMxNi41IDEzMS41LTMxNi41LTEzMS41LTEzMS41LTMxNi41cTAtMTIxIDYxLTIyNS0yMjkgMTE3LTM4MSAzNTMgMTMzIDIwNSAzMzMuNSAzMjYuNXQ0MzQuNSAxMjEuNSA0MzQuNS0xMjEuNSAzMzMuNS0zMjYuNXptLTcyMC0zODRxMC0yMC0xNC0zNHQtMzQtMTRxLTEyNSAwLTIxNC41IDg5LjV0LTg5LjUgMjE0LjVxMCAyMCAxNCAzNHQzNCAxNCAzNC0xNCAxNC0zNHEwLTg2IDYxLTE0N3QxNDctNjFxMjAgMCAzNC0xNHQxNC0zNHptODQ4IDM4NHEwIDM0LTIwIDY5LTE0MCAyMzAtMzc2LjUgMzY4LjV0LTQ5OS41IDEzOC41LTQ5OS41LTEzOS0zNzYuNS0zNjhxLTIwLTM1LTIwLTY5dDIwLTY5cTE0MC0yMjkgMzc2LjUtMzY4dDQ5OS41LTEzOSA0OTkuNSAxMzkgMzc2LjUgMzY4cTIwIDM1IDIwIDY5eiIvPjwvc3ltYm9sPjxzeW1ib2wgdmlld0JveD0iMCAwIDE3OTIgMTc5MiIgaWQ9ImpvZGl0LWZvbGRlciI+PHBhdGggZD0iTTE3MjggNjA4djcwNHEwIDkyLTY2IDE1OHQtMTU4IDY2aC0xMjE2cS05MiAwLTE1OC02NnQtNjYtMTU4di05NjBxMC05MiA2Ni0xNTh0MTU4LTY2aDMyMHE5MiAwIDE1OCA2NnQ2NiAxNTh2MzJoNjcycTkyIDAgMTU4IDY2dDY2IDE1OHoiLz48L3N5bWJvbD48c3ltYm9sIHZpZXdCb3g9IjAgMCAxNzkyIDE3OTIiIGlkPSJqb2RpdC1mb250Ij48cGF0aCBkPSJNNzg5IDU1OWwtMTcwIDQ1MHEzMyAwIDEzNi41IDJ0MTYwLjUgMnExOSAwIDU3LTItODctMjUzLTE4NC00NTJ6bS03MjUgMTEwNWwyLTc5cTIzLTcgNTYtMTIuNXQ1Ny0xMC41IDQ5LjUtMTQuNSA0NC41LTI5IDMxLTUwLjVsMjM3LTYxNiAyODAtNzI0aDEyOHE4IDE0IDExIDIxbDIwNSA0ODBxMzMgNzggMTA2IDI1Ny41dDExNCAyNzQuNXExNSAzNCA1OCAxNDQuNXQ3MiAxNjguNXEyMCA0NSAzNSA1NyAxOSAxNSA4OCAyOS41dDg0IDIwLjVxNiAzOCA2IDU3IDAgNC0uNSAxM3QtLjUgMTNxLTYzIDAtMTkwLTh0LTE5MS04cS03NiAwLTIxNSA3dC0xNzggOHEwLTQzIDQtNzhsMTMxLTI4cTEgMCAxMi41LTIuNXQxNS41LTMuNSAxNC41LTQuNSAxNS02LjUgMTEtOCA5LTExIDIuNS0xNHEwLTE2LTMxLTk2LjV0LTcyLTE3Ny41LTQyLTEwMGwtNDUwLTJxLTI2IDU4LTc2LjUgMTk1LjV0LTUwLjUgMTYyLjVxMCAyMiAxNCAzNy41dDQzLjUgMjQuNSA0OC41IDEzLjUgNTcgOC41IDQxIDRxMSAxOSAxIDU4IDAgOS0yIDI3LTU4IDAtMTc0LjUtMTB0LTE3NC41LTEwcS04IDAtMjYuNSA0dC0yMS41IDRxLTgwIDE0LTE4OCAxNHoiLz48L3N5bWJvbD48c3ltYm9sIHZpZXdCb3g9IjAgMCAxNzkyIDE3OTIiIGlkPSJqb2RpdC1mb250c2l6ZSI+PHBhdGggZD0iTTE3NDQgMTQwOHEzMyAwIDQyIDE4LjV0LTExIDQ0LjVsLTEyNiAxNjJxLTIwIDI2LTQ5IDI2dC00OS0yNmwtMTI2LTE2MnEtMjAtMjYtMTEtNDQuNXQ0Mi0xOC41aDgwdi0xMDI0aC04MHEtMzMgMC00Mi0xOC41dDExLTQ0LjVsMTI2LTE2MnEyMC0yNiA0OS0yNnQ0OSAyNmwxMjYgMTYycTIwIDI2IDExIDQ0LjV0LTQyIDE4LjVoLTgwdjEwMjRoODB6bS0xNjYzLTEyNzlsNTQgMjdxMTIgNSAyMTEgNSA0NCAwIDEzMi0ydDEzMi0ycTM2IDAgMTA3LjUuNXQxMDcuNS41aDI5M3E2IDAgMjEgLjV0MjAuNSAwIDE2LTMgMTcuNS05IDE1LTE3LjVsNDItMXE0IDAgMTQgLjV0MTQgLjVxMiAxMTIgMiAzMzYgMCA4MC01IDEwOS0zOSAxNC02OCAxOC0yNS00NC01NC0xMjgtMy05LTExLTQ4dC0xNC41LTczLjUtNy41LTM1LjVxLTYtOC0xMi0xMi41dC0xNS41LTYtMTMtMi41LTE4LS41LTE2LjUuNXEtMTcgMC02Ni41LS41dC03NC41LS41LTY0IDItNzEgNnEtOSA4MS04IDEzNiAwIDk0IDIgMzg4dDIgNDU1cTAgMTYtMi41IDcxLjV0MCA5MS41IDEyLjUgNjlxNDAgMjEgMTI0IDQyLjV0MTIwIDM3LjVxNSA0MCA1IDUwIDAgMTQtMyAyOWwtMzQgMXEtNzYgMi0yMTgtOHQtMjA3LTEwcS01MCAwLTE1MSA5dC0xNTIgOXEtMy01MS0zLTUydi05cTE3LTI3IDYxLjUtNDN0OTguNS0yOSA3OC0yN3ExOS00MiAxOS0zODMgMC0xMDEtMy0zMDN0LTMtMzAzdi0xMTdxMC0yIC41LTE1LjV0LjUtMjUtMS0yNS41LTMtMjQtNS0xNHEtMTEtMTItMTYyLTEyLTMzIDAtOTMgMTJ0LTgwIDI2cS0xOSAxMy0zNCA3Mi41dC0zMS41IDExMS00Mi41IDUzLjVxLTQyLTI2LTU2LTQ0di0zODN6Ii8+PC9zeW1ib2w+PHN5bWJvbCB2aWV3Qm94PSIwIDAgMTc5MiAxNzkyIiBpZD0iam9kaXQtZnVsbHNpemUiPjxwYXRoIGQ9Ik04ODMgMTA1NnEwIDEzLTEwIDIzbC0zMzIgMzMyIDE0NCAxNDRxMTkgMTkgMTkgNDV0LTE5IDQ1LTQ1IDE5aC00NDhxLTI2IDAtNDUtMTl0LTE5LTQ1di00NDhxMC0yNiAxOS00NXQ0NS0xOSA0NSAxOWwxNDQgMTQ0IDMzMi0zMzJxMTAtMTAgMjMtMTB0MjMgMTBsMTE0IDExNHExMCAxMCAxMCAyM3ptNzgxLTg2NHY0NDhxMCAyNi0xOSA0NXQtNDUgMTktNDUtMTlsLTE0NC0xNDQtMzMyIDMzMnEtMTAgMTAtMjMgMTB0LTIzLTEwbC0xMTQtMTE0cS0xMC0xMC0xMC0yM3QxMC0yM2wzMzItMzMyLTE0NC0xNDRxLTE5LTE5LTE5LTQ1dDE5LTQ1IDQ1LTE5aDQ0OHEyNiAwIDQ1IDE5dDE5IDQ1eiIvPjwvc3ltYm9sPjxzeW1ib2wgdmlld0JveD0iMCAwIDE3OTIgMTc5MiIgaWQ9ImpvZGl0LWhyIj48cGF0aCBkPSJNMTYwMCA3MzZ2MTkycTAgNDAtMjggNjh0LTY4IDI4aC0xMjE2cS00MCAwLTY4LTI4dC0yOC02OHYtMTkycTAtNDAgMjgtNjh0NjgtMjhoMTIxNnE0MCAwIDY4IDI4dDI4IDY4eiIvPjwvc3ltYm9sPjxzeW1ib2wgdmlld0JveD0iMCAwIDE3OTIgMTc5MiIgaWQ9ImpvZGl0LWltYWdlIj48cGF0aCBkPSJNNTc2IDU3NnEwIDgwLTU2IDEzNnQtMTM2IDU2LTEzNi01Ni01Ni0xMzYgNTYtMTM2IDEzNi01NiAxMzYgNTYgNTYgMTM2em0xMDI0IDM4NHY0NDhoLTE0MDh2LTE5MmwzMjAtMzIwIDE2MCAxNjAgNTEyLTUxMnptOTYtNzA0aC0xNjAwcS0xMyAwLTIyLjUgOS41dC05LjUgMjIuNXYxMjE2cTAgMTMgOS41IDIyLjV0MjIuNSA5LjVoMTYwMHExMyAwIDIyLjUtOS41dDkuNS0yMi41di0xMjE2cTAtMTMtOS41LTIyLjV0LTIyLjUtOS41em0xNjAgMzJ2MTIxNnEwIDY2LTQ3IDExM3QtMTEzIDQ3aC0xNjAwcS02NiAwLTExMy00N3QtNDctMTEzdi0xMjE2cTAtNjYgNDctMTEzdDExMy00N2gxNjAwcTY2IDAgMTEzIDQ3dDQ3IDExM3oiLz48L3N5bWJvbD48c3ltYm9sIHZpZXdCb3g9IjAgMCAxNzkyIDE3OTIiIGlkPSJqb2RpdC1pbmZvLWNpcmNsZSI+PHBhdGggZD0iTTExNTIgMTM3NnYtMTYwcTAtMTQtOS0yM3QtMjMtOWgtOTZ2LTUxMnEwLTE0LTktMjN0LTIzLTloLTMyMHEtMTQgMC0yMyA5dC05IDIzdjE2MHEwIDE0IDkgMjN0MjMgOWg5NnYzMjBoLTk2cS0xNCAwLTIzIDl0LTkgMjN2MTYwcTAgMTQgOSAyM3QyMyA5aDQ0OHExNCAwIDIzLTl0OS0yM3ptLTEyOC04OTZ2LTE2MHEwLTE0LTktMjN0LTIzLTloLTE5MnEtMTQgMC0yMyA5dC05IDIzdjE2MHEwIDE0IDkgMjN0MjMgOWgxOTJxMTQgMCAyMy05dDktMjN6bTY0MCA0MTZxMCAyMDktMTAzIDM4NS41dC0yNzkuNSAyNzkuNS0zODUuNSAxMDMtMzg1LjUtMTAzLTI3OS41LTI3OS41LTEwMy0zODUuNSAxMDMtMzg1LjUgMjc5LjUtMjc5LjUgMzg1LjUtMTAzIDM4NS41IDEwMyAyNzkuNSAyNzkuNSAxMDMgMzg1LjV6Ii8+PC9zeW1ib2w+PHN5bWJvbCB2aWV3Qm94PSIwIDAgMTc5MiAxNzkyIiBpZD0iam9kaXQtaXRhbGljIj48cGF0aCBkPSJNMzg0IDE2NjJsMTctODVxNi0yIDgxLjUtMjEuNXQxMTEuNS0zNy41cTI4LTM1IDQxLTEwMSAxLTcgNjItMjg5dDExNC01NDMuNSA1Mi0yOTYuNXYtMjVxLTI0LTEzLTU0LjUtMTguNXQtNjkuNS04LTU4LTUuNWwxOS0xMDNxMzMgMiAxMjAgNi41dDE0OS41IDcgMTIwLjUgMi41cTQ4IDAgOTguNS0yLjV0MTIxLTcgOTguNS02LjVxLTUgMzktMTkgODktMzAgMTAtMTAxLjUgMjguNXQtMTA4LjUgMzMuNXEtOCAxOS0xNCA0Mi41dC05IDQwLTcuNSA0NS41LTYuNSA0MnEtMjcgMTQ4LTg3LjUgNDE5LjV0LTc3LjUgMzU1LjVxLTIgOS0xMyA1OHQtMjAgOTAtMTYgODMuNS02IDU3LjVsMSAxOHExNyA0IDE4NSAzMS0zIDQ0LTE2IDk5LTExIDAtMzIuNSAxLjV0LTMyLjUgMS41cS0yOSAwLTg3LTEwdC04Ni0xMHEtMTM4LTItMjA2LTItNTEgMC0xNDMgOXQtMTIxIDExeiIvPjwvc3ltYm9sPjxzeW1ib2wgdmlld0JveD0iMCAwIDE3OTIgMTc5MiIgaWQ9ImpvZGl0LWp1c3RpZnkiPjxwYXRoIGQ9Ik0xNzkyIDEzNDR2MTI4cTAgMjYtMTkgNDV0LTQ1IDE5aC0xNjY0cS0yNiAwLTQ1LTE5dC0xOS00NXYtMTI4cTAtMjYgMTktNDV0NDUtMTloMTY2NHEyNiAwIDQ1IDE5dDE5IDQ1em0wLTM4NHYxMjhxMCAyNi0xOSA0NXQtNDUgMTloLTE2NjRxLTI2IDAtNDUtMTl0LTE5LTQ1di0xMjhxMC0yNiAxOS00NXQ0NS0xOWgxNjY0cTI2IDAgNDUgMTl0MTkgNDV6bTAtMzg0djEyOHEwIDI2LTE5IDQ1dC00NSAxOWgtMTY2NHEtMjYgMC00NS0xOXQtMTktNDV2LTEyOHEwLTI2IDE5LTQ1dDQ1LTE5aDE2NjRxMjYgMCA0NSAxOXQxOSA0NXptMC0zODR2MTI4cTAgMjYtMTkgNDV0LTQ1IDE5aC0xNjY0cS0yNiAwLTQ1LTE5dC0xOS00NXYtMTI4cTAtMjYgMTktNDV0NDUtMTloMTY2NHEyNiAwIDQ1IDE5dDE5IDQ1eiIvPjwvc3ltYm9sPjxzeW1ib2wgdmlld0JveD0iMCAwIDE3OTIgMTc5MiIgaWQ9ImpvZGl0LWxlZnQiPjxwYXRoIGQ9Ik0xNzkyIDEzNDR2MTI4cTAgMjYtMTkgNDV0LTQ1IDE5aC0xNjY0cS0yNiAwLTQ1LTE5dC0xOS00NXYtMTI4cTAtMjYgMTktNDV0NDUtMTloMTY2NHEyNiAwIDQ1IDE5dDE5IDQ1em0tMzg0LTM4NHYxMjhxMCAyNi0xOSA0NXQtNDUgMTloLTEyODBxLTI2IDAtNDUtMTl0LTE5LTQ1di0xMjhxMC0yNiAxOS00NXQ0NS0xOWgxMjgwcTI2IDAgNDUgMTl0MTkgNDV6bTI1Ni0zODR2MTI4cTAgMjYtMTkgNDV0LTQ1IDE5aC0xNTM2cS0yNiAwLTQ1LTE5dC0xOS00NXYtMTI4cTAtMjYgMTktNDV0NDUtMTloMTUzNnEyNiAwIDQ1IDE5dDE5IDQ1em0tMzg0LTM4NHYxMjhxMCAyNi0xOSA0NXQtNDUgMTloLTExNTJxLTI2IDAtNDUtMTl0LTE5LTQ1di0xMjhxMC0yNiAxOS00NXQ0NS0xOWgxMTUycTI2IDAgNDUgMTl0MTkgNDV6Ii8+PC9zeW1ib2w+PHN5bWJvbCB2aWV3Qm94PSIwIDAgMTc5MiAxNzkyIiBpZD0iam9kaXQtbGluayI+PHBhdGggZD0iTTE1MjAgMTIxNnEwLTQwLTI4LTY4bC0yMDgtMjA4cS0yOC0yOC02OC0yOC00MiAwLTcyIDMyIDMgMyAxOSAxOC41dDIxLjUgMjEuNSAxNSAxOSAxMyAyNS41IDMuNSAyNy41cTAgNDAtMjggNjh0LTY4IDI4cS0xNSAwLTI3LjUtMy41dC0yNS41LTEzLTE5LTE1LTIxLjUtMjEuNS0xOC41LTE5cS0zMyAzMS0zMyA3MyAwIDQwIDI4IDY4bDIwNiAyMDdxMjcgMjcgNjggMjcgNDAgMCA2OC0yNmwxNDctMTQ2cTI4LTI4IDI4LTY3em0tNzAzLTcwNXEwLTQwLTI4LTY4bC0yMDYtMjA3cS0yOC0yOC02OC0yOC0zOSAwLTY4IDI3bC0xNDcgMTQ2cS0yOCAyOC0yOCA2NyAwIDQwIDI4IDY4bDIwOCAyMDhxMjcgMjcgNjggMjcgNDIgMCA3Mi0zMS0zLTMtMTktMTguNXQtMjEuNS0yMS41LTE1LTE5LTEzLTI1LjUtMy41LTI3LjVxMC00MCAyOC02OHQ2OC0yOHExNSAwIDI3LjUgMy41dDI1LjUgMTMgMTkgMTUgMjEuNSAyMS41IDE4LjUgMTlxMzMtMzEgMzMtNzN6bTg5NSA3MDVxMCAxMjAtODUgMjAzbC0xNDcgMTQ2cS04MyA4My0yMDMgODMtMTIxIDAtMjA0LTg1bC0yMDYtMjA3cS04My04My04My0yMDMgMC0xMjMgODgtMjA5bC04OC04OHEtODYgODgtMjA4IDg4LTEyMCAwLTIwNC04NGwtMjA4LTIwOHEtODQtODQtODQtMjA0dDg1LTIwM2wxNDctMTQ2cTgzLTgzIDIwMy04MyAxMjEgMCAyMDQgODVsMjA2IDIwN3E4MyA4MyA4MyAyMDMgMCAxMjMtODggMjA5bDg4IDg4cTg2LTg4IDIwOC04OCAxMjAgMCAyMDQgODRsMjA4IDIwOHE4NCA4NCA4NCAyMDR6Ii8+PC9zeW1ib2w+PHN5bWJvbCB2aWV3Qm94PSIwIDAgMTc5MiAxNzkyIiBpZD0iam9kaXQtbG9jayI+PHBhdGggZD0iTTY0MCA3NjhoNTEydi0xOTJxMC0xMDYtNzUtMTgxdC0xODEtNzUtMTgxIDc1LTc1IDE4MXYxOTJ6bTgzMiA5NnY1NzZxMCA0MC0yOCA2OHQtNjggMjhoLTk2MHEtNDAgMC02OC0yOHQtMjgtNjh2LTU3NnEwLTQwIDI4LTY4dDY4LTI4aDMydi0xOTJxMC0xODQgMTMyLTMxNnQzMTYtMTMyIDMxNiAxMzIgMTMyIDMxNnYxOTJoMzJxNDAgMCA2OCAyOHQyOCA2OHoiLz48L3N5bWJvbD48c3ltYm9sIHZpZXdCb3g9IjAgMCAxNzkyIDE3OTIiIGlkPSJqb2RpdC1tZW51Ij48cGF0aCBkPSJNMTY2NCAxMzQ0djEyOHEwIDI2LTE5IDQ1dC00NSAxOWgtMTQwOHEtMjYgMC00NS0xOXQtMTktNDV2LTEyOHEwLTI2IDE5LTQ1dDQ1LTE5aDE0MDhxMjYgMCA0NSAxOXQxOSA0NXptMC01MTJ2MTI4cTAgMjYtMTkgNDV0LTQ1IDE5aC0xNDA4cS0yNiAwLTQ1LTE5dC0xOS00NXYtMTI4cTAtMjYgMTktNDV0NDUtMTloMTQwOHEyNiAwIDQ1IDE5dDE5IDQ1em0wLTUxMnYxMjhxMCAyNi0xOSA0NXQtNDUgMTloLTE0MDhxLTI2IDAtNDUtMTl0LTE5LTQ1di0xMjhxMC0yNiAxOS00NXQ0NS0xOWgxNDA4cTI2IDAgNDUgMTl0MTkgNDV6Ii8+PC9zeW1ib2w+PHN5bWJvbCB2aWV3Qm94PSIwIDAgMzEyLjAwMDAwMCAzMTIuMDAwMDAwIiBpZD0iam9kaXQtbWVyZ2UiPiA8ZyB0cmFuc2Zvcm09InRyYW5zbGF0ZSgwLjAwMDAwMCwzMTIuMDAwMDAwKSBzY2FsZSgwLjEwMDAwMCwtMC4xMDAwMDApIiBzdHJva2U9Im5vbmUiPiA8cGF0aCBkPSJNNTAgMzEwOSBjMCAtNyAtMTEgLTIyIC0yNSAtMzUgbC0yNSAtMjMgMCAtOTYxIDAgLTk2MSAzMiAtMjkgMzIKLTMwIDUwMSAtMiA1MDAgLTMgMyAtNTAyIDIgLTUwMiAzMSAtMzAgMzEgLTMxIDk1OCAwIDk1OCAwIDIzIDI1IGMxMyAxMyAzMAoyNSAzNyAyNSA5IDAgMTIgMTk5IDEyIDk2MCAwIDY4NiAtMyA5NjAgLTExIDk2MCAtNiAwIC0yNCAxMiAtNDAgMjggbC0yOSAyNwotNTAzIDUgLTUwMiA1IC01IDUwMiAtNSA1MDMgLTI4IDI5IGMtMTUgMTYgLTI3IDM0IC0yNyA0MCAwIDggLTI3NCAxMSAtOTYwCjExIC03MTAgMCAtOTYwIC0zIC05NjAgLTExeiBtMTczOCAtNjk4IGwyIC00NTMgLTQwIC00MCBjLTIyIC0yMiAtNDAgLTQzIC00MAotNDcgMCAtNCAzNiAtNDIgNzkgLTg1IDg4IC04NyA4MiAtODcgMTQxIC0yMyBsMjYgMjcgNDU1IC0yIDQ1NCAtMyAwIC03NzUgMAotNzc1IC03NzUgMCAtNzc1IDAgLTMgNDUwIC0yIDQ0OSA0NyA0OCA0NyA0OCAtODIgODAgYy00NCA0NCAtODQgODAgLTg3IDgwCi0zIDAgLTI1IC0xOCAtNDggLTQwIGwtNDEgLTQwIC00NTYgMiAtNDU1IDMgLTMgNzY1IGMtMSA0MjEgMCA3NzEgMyA3NzggMyAxMAoxNjQgMTIgNzc3IDEwIGw3NzMgLTMgMyAtNDU0eiIvPiA8cGF0aCBkPSJNNjA3IDI0OTIgYy00MiAtNDIgLTc3IC04MiAtNzcgLTg3IDAgLTYgODYgLTk2IDE5MCAtMjAwIDEwNSAtMTA0CjE5MCAtMTk3IDE5MCAtMjA1IDAgLTggLTQxIC01NiAtOTIgLTEwNyAtNjUgLTY1IC04NyAtOTQgLTc3IC05OCA4IC0zIDEzOCAtNAoyODkgLTMgbDI3NSAzIDMgMjc1IGMxIDE1MSAwIDI4MSAtMyAyODkgLTQgMTAgLTM1IC0xNCAtMTAzIC04MiAtNTQgLTUzIC0xMDMKLTk3IC0xMDkgLTk3IC03IDAgLTk5IDg4IC0yMDYgMTk1IC0xMDcgMTA3IC0xOTYgMTk1IC0xOTggMTk1IC0zIDAgLTM5IC0zNQotODIgLTc4eiIvPiA8cGF0aCBkPSJNMTQ3MCAxNjM5IGMtNDcgLTQ5IC04NyAtOTEgLTg5IC05NCAtNSAtNiAxNDkgLTE2NSAxNjAgLTE2NSA5IDAKMTg5IDE3OSAxODkgMTg4IDAgMTIgLTE1NCAxNjIgLTE2NSAxNjEgLTYgMCAtNDggLTQxIC05NSAtOTB6Ii8+IDxwYXRoIGQ9Ik0xNzk3IDEzMDMgYy05IC04IC05IC01NjggMCAtNTc2IDQgLTQgNTAgMzYgMTAzIDg4IDU0IDUyIDEwMSA5NQoxMDYgOTUgNSAwIDk1IC04NSAxOTkgLTE5MCAxMDQgLTEwNCAxOTQgLTE5MCAyMDAgLTE5MCA2IDAgNDYgMzYgOTAgODAgbDc5Cjc5IC0xOTcgMTk2IGMtMTA4IDEwOCAtMTk3IDE5OSAtMTk3IDIwMyAwIDQgNDUgNTIgOTkgMTA2IDU1IDU1IDk4IDEwMyA5NQoxMDggLTYgMTAgLTU2OCAxMSAtNTc3IDF6Ii8+IDwvZz4gPC9zeW1ib2w+PHN5bWJvbCB2aWV3Qm94PSIwIDAgMTc5MiAxNzkyIiBpZD0iam9kaXQtb2wiPjxwYXRoIGQ9Ik0zODEgMTYyMHEwIDgwLTU0LjUgMTI2dC0xMzUuNSA0NnEtMTA2IDAtMTcyLTY2bDU3LTg4cTQ5IDQ1IDEwNiA0NSAyOSAwIDUwLjUtMTQuNXQyMS41LTQyLjVxMC02NC0xMDUtNTZsLTI2LTU2cTgtMTAgMzIuNS00My41dDQyLjUtNTQgMzctMzguNXYtMXEtMTYgMC00OC41IDF0LTQ4LjUgMXY1M2gtMTA2di0xNTJoMzMzdjg4bC05NSAxMTVxNTEgMTIgODEgNDl0MzAgODh6bTItNjI3djE1OWgtMzYycS02LTM2LTYtNTQgMC01MSAyMy41LTkzdDU2LjUtNjggNjYtNDcuNSA1Ni41LTQzLjUgMjMuNS00NXEwLTI1LTE0LjUtMzguNXQtMzkuNS0xMy41cS00NiAwLTgxIDU4bC04NS01OXEyNC01MSA3MS41LTc5LjV0MTA1LjUtMjguNXE3MyAwIDEyMyA0MS41dDUwIDExMi41cTAgNTAtMzQgOTEuNXQtNzUgNjQuNS03NS41IDUwLjUtMzUuNSA1Mi41aDEyN3YtNjBoMTA1em0xNDA5IDMxOXYxOTJxMCAxMy05LjUgMjIuNXQtMjIuNSA5LjVoLTEyMTZxLTEzIDAtMjIuNS05LjV0LTkuNS0yMi41di0xOTJxMC0xNCA5LTIzdDIzLTloMTIxNnExMyAwIDIyLjUgOS41dDkuNSAyMi41em0tMTQwOC04OTl2OTloLTMzNXYtOTloMTA3cTAtNDEgLjUtMTIydC41LTEyMXYtMTJoLTJxLTggMTctNTAgNTRsLTcxLTc2IDEzNi0xMjdoMTA2djQwNGgxMDh6bTE0MDggMzg3djE5MnEwIDEzLTkuNSAyMi41dC0yMi41IDkuNWgtMTIxNnEtMTMgMC0yMi41LTkuNXQtOS41LTIyLjV2LTE5MnEwLTE0IDktMjN0MjMtOWgxMjE2cTEzIDAgMjIuNSA5LjV0OS41IDIyLjV6bTAtNTEydjE5MnEwIDEzLTkuNSAyMi41dC0yMi41IDkuNWgtMTIxNnEtMTMgMC0yMi41LTkuNXQtOS41LTIyLjV2LTE5MnEwLTEzIDkuNS0yMi41dDIyLjUtOS41aDEyMTZxMTMgMCAyMi41IDkuNXQ5LjUgMjIuNXoiLz48L3N5bWJvbD48c3ltYm9sIHZpZXdCb3g9IjAgMCAxNzkyIDE3OTIiIGlkPSJqb2RpdC1wYXJhZ3JhcGgiPjxwYXRoIGQ9Ik0xNTM0IDE4OXY3M3EwIDI5LTE4LjUgNjF0LTQyLjUgMzJxLTUwIDAtNTQgMS0yNiA2LTMyIDMxLTMgMTEtMyA2NHYxMTUycTAgMjUtMTggNDN0LTQzIDE4aC0xMDhxLTI1IDAtNDMtMTh0LTE4LTQzdi0xMjE4aC0xNDN2MTIxOHEwIDI1LTE3LjUgNDN0LTQzLjUgMThoLTEwOHEtMjYgMC00My41LTE4dC0xNy41LTQzdi00OTZxLTE0Ny0xMi0yNDUtNTktMTI2LTU4LTE5Mi0xNzktNjQtMTE3LTY0LTI1OSAwLTE2NiA4OC0yODYgODgtMTE4IDIwOS0xNTkgMTExLTM3IDQxNy0zN2g0NzlxMjUgMCA0MyAxOHQxOCA0M3oiLz48L3N5bWJvbD48c3ltYm9sIHZpZXdCb3g9IjAgMCAxNzkyIDE3OTIiIGlkPSJqb2RpdC1wZW5jaWwiPjxwYXRoIGQ9Ik00OTEgMTUzNmw5MS05MS0yMzUtMjM1LTkxIDkxdjEwN2gxMjh2MTI4aDEwN3ptNTIzLTkyOHEwLTIyLTIyLTIyLTEwIDAtMTcgN2wtNTQyIDU0MnEtNyA3LTcgMTcgMCAyMiAyMiAyMiAxMCAwIDE3LTdsNTQyLTU0MnE3LTcgNy0xN3ptLTU0LTE5Mmw0MTYgNDE2LTgzMiA4MzJoLTQxNnYtNDE2em02ODMgOTZxMCA1My0zNyA5MGwtMTY2IDE2Ni00MTYtNDE2IDE2Ni0xNjVxMzYtMzggOTAtMzggNTMgMCA5MSAzOGwyMzUgMjM0cTM3IDM5IDM3IDkxeiIvPjwvc3ltYm9sPjxzeW1ib2wgdmlld0JveD0iMCAwIDE3OTIgMTc5MiIgaWQ9ImpvZGl0LXBsdXMiPjxwYXRoIGQ9Ik0xNjAwIDczNnYxOTJxMCA0MC0yOCA2OHQtNjggMjhoLTQxNnY0MTZxMCA0MC0yOCA2OHQtNjggMjhoLTE5MnEtNDAgMC02OC0yOHQtMjgtNjh2LTQxNmgtNDE2cS00MCAwLTY4LTI4dC0yOC02OHYtMTkycTAtNDAgMjgtNjh0NjgtMjhoNDE2di00MTZxMC00MCAyOC02OHQ2OC0yOGgxOTJxNDAgMCA2OCAyOHQyOCA2OHY0MTZoNDE2cTQwIDAgNjggMjh0MjggNjh6Ii8+PC9zeW1ib2w+PHN5bWJvbCB2aWV3Qm94PSIwIDAgMTc5MiAxNzkyIiBpZD0iam9kaXQtcmVkbyI+PHBhdGggZD0iTTE2NjQgMjU2djQ0OHEwIDI2LTE5IDQ1dC00NSAxOWgtNDQ4cS00MiAwLTU5LTQwLTE3LTM5IDE0LTY5bDEzOC0xMzhxLTE0OC0xMzctMzQ5LTEzNy0xMDQgMC0xOTguNSA0MC41dC0xNjMuNSAxMDkuNS0xMDkuNSAxNjMuNS00MC41IDE5OC41IDQwLjUgMTk4LjUgMTA5LjUgMTYzLjUgMTYzLjUgMTA5LjUgMTk4LjUgNDAuNXExMTkgMCAyMjUtNTJ0MTc5LTE0N3E3LTEwIDIzLTEyIDE0IDAgMjUgOWwxMzcgMTM4cTkgOCA5LjUgMjAuNXQtNy41IDIyLjVxLTEwOSAxMzItMjY0IDIwNC41dC0zMjcgNzIuNXEtMTU2IDAtMjk4LTYxdC0yNDUtMTY0LTE2NC0yNDUtNjEtMjk4IDYxLTI5OCAxNjQtMjQ1IDI0NS0xNjQgMjk4LTYxcTE0NyAwIDI4NC41IDU1LjV0MjQ0LjUgMTU2LjVsMTMwLTEyOXEyOS0zMSA3MC0xNCAzOSAxNyAzOSA1OXoiLz48L3N5bWJvbD48c3ltYm9sIHZpZXdCb3g9IjAgMCAyNCAyNCIgaWQ9ImpvZGl0LXJlc2l6ZSI+PGc+PGcgaWQ9InJlc2l6ZS1BcnRib2FyZC0xIiB0cmFuc2Zvcm09InRyYW5zbGF0ZSgtMjUxLjAwMDAwMCwgLTQ0My4wMDAwMDApIj48cGF0aCBkPSJNMjUyLDQ0OCBMMjU2LDQ0OCBMMjU2LDQ0NCBMMjUyLDQ0NCBMMjUyLDQ0OCBaIE0yNTcsNDQ4IEwyNjksNDQ4IEwyNjksNDQ2IEwyNTcsNDQ2IEwyNTcsNDQ4IFogTTI1Nyw0NjQgTDI2OSw0NjQgTDI2OSw0NjIgTDI1Nyw0NjIgTDI1Nyw0NjQgWiBNMjcwLDQ0NCBMMjcwLDQ0OCBMMjc0LDQ0OCBMMjc0LDQ0NCBMMjcwLDQ0NCBaIE0yNTIsNDYyIEwyNTIsNDY2IEwyNTYsNDY2IEwyNTYsNDYyIEwyNTIsNDYyIFogTTI3MCw0NjIgTDI3MCw0NjYgTDI3NCw0NjYgTDI3NCw0NjIgTDI3MCw0NjIgWiBNMjU0LDQ2MSBMMjU2LDQ2MSBMMjU2LDQ0OSBMMjU0LDQ0OSBMMjU0LDQ2MSBaIE0yNzAsNDYxIEwyNzIsNDYxIEwyNzIsNDQ5IEwyNzAsNDQ5IEwyNzAsNDYxIFoiIGlkPSJyZXNpemUtZWRpdG9yLWNyb3AtZ2x5cGgiLz48L2c+PC9nPjwvc3ltYm9sPjxzeW1ib2wgdmlld0JveD0iMCAwIDE3OTIgMTc5MiIgaWQ9ImpvZGl0LXJlc2l6ZXIiPjxwYXRoIGQ9Ik04NDQgNDcycTAgNjAtMTkgMTEzLjV0LTYzIDkyLjUtMTA1IDM5cS03NiAwLTEzOC01Ny41dC05Mi0xMzUuNS0zMC0xNTFxMC02MCAxOS0xMTMuNXQ2My05Mi41IDEwNS0zOXE3NyAwIDEzOC41IDU3LjV0OTEuNSAxMzUgMzAgMTUxLjV6bS0zNDIgNDgzcTAgODAtNDIgMTM5dC0xMTkgNTlxLTc2IDAtMTQxLjUtNTUuNXQtMTAwLjUtMTMzLjUtMzUtMTUycTAtODAgNDItMTM5LjV0MTE5LTU5LjVxNzYgMCAxNDEuNSA1NS41dDEwMC41IDEzNCAzNSAxNTIuNXptMzk0LTI3cTExOCAwIDI1NSA5Ny41dDIyOSAyMzcgOTIgMjU0LjVxMCA0Ni0xNyA3Ni41dC00OC41IDQ1LTY0LjUgMjAtNzYgNS41cS02OCAwLTE4Ny41LTQ1dC0xODIuNS00NXEtNjYgMC0xOTIuNSA0NC41dC0yMDAuNSA0NC41cS0xODMgMC0xODMtMTQ2IDAtODYgNTYtMTkxLjV0MTM5LjUtMTkyLjUgMTg3LjUtMTQ2IDE5My01OXptMjM5LTIxMXEtNjEgMC0xMDUtMzl0LTYzLTkyLjUtMTktMTEzLjVxMC03NCAzMC0xNTEuNXQ5MS41LTEzNSAxMzguNS01Ny41cTYxIDAgMTA1IDM5dDYzIDkyLjUgMTkgMTEzLjVxMCA3My0zMCAxNTF0LTkyIDEzNS41LTEzOCA1Ny41em00MzItMTA0cTc3IDAgMTE5IDU5LjV0NDIgMTM5LjVxMCA3NC0zNSAxNTJ0LTEwMC41IDEzMy41LTE0MS41IDU1LjVxLTc3IDAtMTE5LTU5dC00Mi0xMzlxMC03NCAzNS0xNTIuNXQxMDAuNS0xMzQgMTQxLjUtNTUuNXoiLz48L3N5bWJvbD48c3ltYm9sIHZpZXdCb3g9IjAgMCAxNzkyIDE3OTIiIGlkPSJqb2RpdC1yaWdodCI+PHBhdGggZD0iTTE3OTIgMTM0NHYxMjhxMCAyNi0xOSA0NXQtNDUgMTloLTE2NjRxLTI2IDAtNDUtMTl0LTE5LTQ1di0xMjhxMC0yNiAxOS00NXQ0NS0xOWgxNjY0cTI2IDAgNDUgMTl0MTkgNDV6bTAtMzg0djEyOHEwIDI2LTE5IDQ1dC00NSAxOWgtMTI4MHEtMjYgMC00NS0xOXQtMTktNDV2LTEyOHEwLTI2IDE5LTQ1dDQ1LTE5aDEyODBxMjYgMCA0NSAxOXQxOSA0NXptMC0zODR2MTI4cTAgMjYtMTkgNDV0LTQ1IDE5aC0xNTM2cS0yNiAwLTQ1LTE5dC0xOS00NXYtMTI4cTAtMjYgMTktNDV0NDUtMTloMTUzNnEyNiAwIDQ1IDE5dDE5IDQ1em0wLTM4NHYxMjhxMCAyNi0xOSA0NXQtNDUgMTloLTExNTJxLTI2IDAtNDUtMTl0LTE5LTQ1di0xMjhxMC0yNiAxOS00NXQ0NS0xOWgxMTUycTI2IDAgNDUgMTl0MTkgNDV6Ii8+PC9zeW1ib2w+PHN5bWJvbCB2aWV3Qm94PSIwIDAgMTc5MiAxNzkyIiBpZD0iam9kaXQtc2F2ZSI+PHBhdGggZD0iTTUxMiAxNTM2aDc2OHYtMzg0aC03Njh2Mzg0em04OTYgMGgxMjh2LTg5NnEwLTE0LTEwLTM4LjV0LTIwLTM0LjVsLTI4MS0yODFxLTEwLTEwLTM0LTIwdC0zOS0xMHY0MTZxMCA0MC0yOCA2OHQtNjggMjhoLTU3NnEtNDAgMC02OC0yOHQtMjgtNjh2LTQxNmgtMTI4djEyODBoMTI4di00MTZxMC00MCAyOC02OHQ2OC0yOGg4MzJxNDAgMCA2OCAyOHQyOCA2OHY0MTZ6bS0zODQtOTI4di0zMjBxMC0xMy05LjUtMjIuNXQtMjIuNS05LjVoLTE5MnEtMTMgMC0yMi41IDkuNXQtOS41IDIyLjV2MzIwcTAgMTMgOS41IDIyLjV0MjIuNSA5LjVoMTkycTEzIDAgMjIuNS05LjV0OS41LTIyLjV6bTY0MCAzMnY5MjhxMCA0MC0yOCA2OHQtNjggMjhoLTEzNDRxLTQwIDAtNjgtMjh0LTI4LTY4di0xMzQ0cTAtNDAgMjgtNjh0NjgtMjhoOTI4cTQwIDAgODggMjB0NzYgNDhsMjgwIDI4MHEyOCAyOCA0OCA3NnQyMCA4OHoiLz48L3N5bWJvbD48c3ltYm9sIHZpZXdCb3g9IjAgMCAxNzkyIDE3OTIiIGlkPSJqb2RpdC1zaHJpbmsiPjxwYXRoIGQ9Ik04OTYgOTYwdjQ0OHEwIDI2LTE5IDQ1dC00NSAxOS00NS0xOWwtMTQ0LTE0NC0zMzIgMzMycS0xMCAxMC0yMyAxMHQtMjMtMTBsLTExNC0xMTRxLTEwLTEwLTEwLTIzdDEwLTIzbDMzMi0zMzItMTQ0LTE0NHEtMTktMTktMTktNDV0MTktNDUgNDUtMTloNDQ4cTI2IDAgNDUgMTl0MTkgNDV6bTc1NS02NzJxMCAxMy0xMCAyM2wtMzMyIDMzMiAxNDQgMTQ0cTE5IDE5IDE5IDQ1dC0xOSA0NS00NSAxOWgtNDQ4cS0yNiAwLTQ1LTE5dC0xOS00NXYtNDQ4cTAtMjYgMTktNDV0NDUtMTkgNDUgMTlsMTQ0IDE0NCAzMzItMzMycTEwLTEwIDIzLTEwdDIzIDEwbDExNCAxMTRxMTAgMTAgMTAgMjN6Ii8+PC9zeW1ib2w+PHN5bWJvbCB2aWV3Qm94PSIwIDAgMTc5MiAxNzkyIiBpZD0iam9kaXQtc291cmNlIj48cGF0aCBkPSJNNTUzIDEzOTlsLTUwIDUwcS0xMCAxMC0yMyAxMHQtMjMtMTBsLTQ2Ni00NjZxLTEwLTEwLTEwLTIzdDEwLTIzbDQ2Ni00NjZxMTAtMTAgMjMtMTB0MjMgMTBsNTAgNTBxMTAgMTAgMTAgMjN0LTEwIDIzbC0zOTMgMzkzIDM5MyAzOTNxMTAgMTAgMTAgMjN0LTEwIDIzem01OTEtMTA2N2wtMzczIDEyOTFxLTQgMTMtMTUuNSAxOS41dC0yMy41IDIuNWwtNjItMTdxLTEzLTQtMTkuNS0xNS41dC0yLjUtMjQuNWwzNzMtMTI5MXE0LTEzIDE1LjUtMTkuNXQyMy41LTIuNWw2MiAxN3ExMyA0IDE5LjUgMTUuNXQyLjUgMjQuNXptNjU3IDY1MWwtNDY2IDQ2NnEtMTAgMTAtMjMgMTB0LTIzLTEwbC01MC01MHEtMTAtMTAtMTAtMjN0MTAtMjNsMzkzLTM5My0zOTMtMzkzcS0xMC0xMC0xMC0yM3QxMC0yM2w1MC01MHExMC0xMCAyMy0xMHQyMyAxMGw0NjYgNDY2cTEwIDEwIDEwIDIzdC0xMCAyM3oiLz48L3N5bWJvbD48c3ltYm9sIHZpZXdCb3g9IjAgMCA0OCA0OCIgaWQ9ImpvZGl0LXNwbGl0ZyI+PHBhdGggZD0iTTYgMThoNHYtNGgtNHY0em0wLThoNHYtNGgtNHY0em04IDMyaDR2LTRoLTR2NHptMC0xNmg0di00aC00djR6bS04IDBoNHYtNGgtNHY0em0wIDE2aDR2LTRoLTR2NHptMC04aDR2LTRoLTR2NHptOC0yNGg0di00aC00djR6bTI0IDI0aDR2LTRoLTR2NHptLTE2IDhoNHYtMzZoLTR2MzZ6bTE2IDBoNHYtNGgtNHY0em0wLTE2aDR2LTRoLTR2NHptMC0yMHY0aDR2LTRoLTR6bTAgMTJoNHYtNGgtNHY0em0tOC04aDR2LTRoLTR2NHptMCAzMmg0di00aC00djR6bTAtMTZoNHYtNGgtNHY0eiIvPjxwYXRoIGQ9Ik0wIDBoNDh2NDhoLTQ4eiIgZmlsbD0ibm9uZSIvPjwvc3ltYm9sPjxzeW1ib2wgdmlld0JveD0iMCAwIDQ4IDQ4IiBpZD0iam9kaXQtc3BsaXR2Ij48cGF0aCBkPSJNNiA0Mmg0di00aC00djR6bTQtMjhoLTR2NGg0di00em0tNCAyMGg0di00aC00djR6bTggOGg0di00aC00djR6bS00LTM2aC00djRoNHYtNHptOCAwaC00djRoNHYtNHptMTYgMGgtNHY0aDR2LTR6bS04IDhoLTR2NGg0di00em0wLThoLTR2NGg0di00em0xMiAyOGg0di00aC00djR6bS0xNiA4aDR2LTRoLTR2NHptLTE2LTE2aDM2di00aC0zNnY0em0zMi0yMHY0aDR2LTRoLTR6bTAgMTJoNHYtNGgtNHY0em0tMTYgMTZoNHYtNGgtNHY0em04IDhoNHYtNGgtNHY0em04IDBoNHYtNGgtNHY0eiIvPjxwYXRoIGQ9Ik0wIDBoNDh2NDhoLTQ4eiIgZmlsbD0ibm9uZSIvPjwvc3ltYm9sPjxzeW1ib2wgdmlld0JveD0iMCAwIDE3OTIgMTc5MiIgaWQ9ImpvZGl0LXN0cmlrZXRocm91Z2giPjxwYXRoIGQ9Ik0xNzYwIDg5NnExNCAwIDIzIDl0OSAyM3Y2NHEwIDE0LTkgMjN0LTIzIDloLTE3MjhxLTE0IDAtMjMtOXQtOS0yM3YtNjRxMC0xNCA5LTIzdDIzLTloMTcyOHptLTEyNzctNjRxLTI4LTM1LTUxLTgwLTQ4LTk3LTQ4LTE4OCAwLTE4MSAxMzQtMzA5IDEzMy0xMjcgMzkzLTEyNyA1MCAwIDE2NyAxOSA2NiAxMiAxNzcgNDggMTAgMzggMjEgMTE4IDE0IDEyMyAxNCAxODMgMCAxOC01IDQ1bC0xMiAzLTg0LTYtMTQtMnEtNTAtMTQ5LTEwMy0yMDUtODgtOTEtMjEwLTkxLTExNCAwLTE4MiA1OS02NyA1OC02NyAxNDYgMCA3MyA2NiAxNDB0Mjc5IDEyOXE2OSAyMCAxNzMgNjYgNTggMjggOTUgNTJoLTc0M3ptNTA3IDI1Nmg0MTFxNyAzOSA3IDkyIDAgMTExLTQxIDIxMi0yMyA1NS03MSAxMDQtMzcgMzUtMTA5IDgxLTgwIDQ4LTE1MyA2Ni04MCAyMS0yMDMgMjEtMTE0IDAtMTk1LTIzbC0xNDAtNDBxLTU3LTE2LTcyLTI4LTgtOC04LTIydi0xM3EwLTEwOC0yLTE1Ni0xLTMwIDAtNjhsMi0zN3YtNDRsMTAyLTJxMTUgMzQgMzAgNzF0MjIuNSA1NiAxMi41IDI3cTM1IDU3IDgwIDk0IDQzIDM2IDEwNSA1NyA1OSAyMiAxMzIgMjIgNjQgMCAxMzktMjcgNzctMjYgMTIyLTg2IDQ3LTYxIDQ3LTEyOSAwLTg0LTgxLTE1Ny0zNC0yOS0xMzctNzF6Ii8+PC9zeW1ib2w+PHN5bWJvbCB2aWV3Qm94PSIwIDAgMTc5MiAxNzkyIiBpZD0iam9kaXQtdGFibGUiPjxwYXRoIGQ9Ik01NzYgMTM3NnYtMTkycTAtMTQtOS0yM3QtMjMtOWgtMzIwcS0xNCAwLTIzIDl0LTkgMjN2MTkycTAgMTQgOSAyM3QyMyA5aDMyMHExNCAwIDIzLTl0OS0yM3ptMC0zODR2LTE5MnEwLTE0LTktMjN0LTIzLTloLTMyMHEtMTQgMC0yMyA5dC05IDIzdjE5MnEwIDE0IDkgMjN0MjMgOWgzMjBxMTQgMCAyMy05dDktMjN6bTUxMiAzODR2LTE5MnEwLTE0LTktMjN0LTIzLTloLTMyMHEtMTQgMC0yMyA5dC05IDIzdjE5MnEwIDE0IDkgMjN0MjMgOWgzMjBxMTQgMCAyMy05dDktMjN6bS01MTItNzY4di0xOTJxMC0xNC05LTIzdC0yMy05aC0zMjBxLTE0IDAtMjMgOXQtOSAyM3YxOTJxMCAxNCA5IDIzdDIzIDloMzIwcTE0IDAgMjMtOXQ5LTIzem01MTIgMzg0di0xOTJxMC0xNC05LTIzdC0yMy05aC0zMjBxLTE0IDAtMjMgOXQtOSAyM3YxOTJxMCAxNCA5IDIzdDIzIDloMzIwcTE0IDAgMjMtOXQ5LTIzem01MTIgMzg0di0xOTJxMC0xNC05LTIzdC0yMy05aC0zMjBxLTE0IDAtMjMgOXQtOSAyM3YxOTJxMCAxNCA5IDIzdDIzIDloMzIwcTE0IDAgMjMtOXQ5LTIzem0tNTEyLTc2OHYtMTkycTAtMTQtOS0yM3QtMjMtOWgtMzIwcS0xNCAwLTIzIDl0LTkgMjN2MTkycTAgMTQgOSAyM3QyMyA5aDMyMHExNCAwIDIzLTl0OS0yM3ptNTEyIDM4NHYtMTkycTAtMTQtOS0yM3QtMjMtOWgtMzIwcS0xNCAwLTIzIDl0LTkgMjN2MTkycTAgMTQgOSAyM3QyMyA5aDMyMHExNCAwIDIzLTl0OS0yM3ptMC0zODR2LTE5MnEwLTE0LTktMjN0LTIzLTloLTMyMHEtMTQgMC0yMyA5dC05IDIzdjE5MnEwIDE0IDkgMjN0MjMgOWgzMjBxMTQgMCAyMy05dDktMjN6bTEyOC0zMjB2MTA4OHEwIDY2LTQ3IDExM3QtMTEzIDQ3aC0xMzQ0cS02NiAwLTExMy00N3QtNDctMTEzdi0xMDg4cTAtNjYgNDctMTEzdDExMy00N2gxMzQ0cTY2IDAgMTEzIDQ3dDQ3IDExM3oiLz48L3N5bWJvbD48c3ltYm9sIHZpZXdCb3g9IjAgMCAxNzkyIDE3OTIiIGlkPSJqb2RpdC10aC1saXN0Ij48cGF0aCBkPSJNNTEyIDEyNDh2MTkycTAgNDAtMjggNjh0LTY4IDI4aC0zMjBxLTQwIDAtNjgtMjh0LTI4LTY4di0xOTJxMC00MCAyOC02OHQ2OC0yOGgzMjBxNDAgMCA2OCAyOHQyOCA2OHptMC01MTJ2MTkycTAgNDAtMjggNjh0LTY4IDI4aC0zMjBxLTQwIDAtNjgtMjh0LTI4LTY4di0xOTJxMC00MCAyOC02OHQ2OC0yOGgzMjBxNDAgMCA2OCAyOHQyOCA2OHptMTI4MCA1MTJ2MTkycTAgNDAtMjggNjh0LTY4IDI4aC05NjBxLTQwIDAtNjgtMjh0LTI4LTY4di0xOTJxMC00MCAyOC02OHQ2OC0yOGg5NjBxNDAgMCA2OCAyOHQyOCA2OHptLTEyODAtMTAyNHYxOTJxMCA0MC0yOCA2OHQtNjggMjhoLTMyMHEtNDAgMC02OC0yOHQtMjgtNjh2LTE5MnEwLTQwIDI4LTY4dDY4LTI4aDMyMHE0MCAwIDY4IDI4dDI4IDY4em0xMjgwIDUxMnYxOTJxMCA0MC0yOCA2OHQtNjggMjhoLTk2MHEtNDAgMC02OC0yOHQtMjgtNjh2LTE5MnEwLTQwIDI4LTY4dDY4LTI4aDk2MHE0MCAwIDY4IDI4dDI4IDY4em0wLTUxMnYxOTJxMCA0MC0yOCA2OHQtNjggMjhoLTk2MHEtNDAgMC02OC0yOHQtMjgtNjh2LTE5MnEwLTQwIDI4LTY4dDY4LTI4aDk2MHE0MCAwIDY4IDI4dDI4IDY4eiIvPjwvc3ltYm9sPjxzeW1ib2wgdmlld0JveD0iMCAwIDE3OTIgMTc5MiIgaWQ9ImpvZGl0LXRoIj48cGF0aCBkPSJNNTEyIDEyNDh2MTkycTAgNDAtMjggNjh0LTY4IDI4aC0zMjBxLTQwIDAtNjgtMjh0LTI4LTY4di0xOTJxMC00MCAyOC02OHQ2OC0yOGgzMjBxNDAgMCA2OCAyOHQyOCA2OHptMC01MTJ2MTkycTAgNDAtMjggNjh0LTY4IDI4aC0zMjBxLTQwIDAtNjgtMjh0LTI4LTY4di0xOTJxMC00MCAyOC02OHQ2OC0yOGgzMjBxNDAgMCA2OCAyOHQyOCA2OHptNjQwIDUxMnYxOTJxMCA0MC0yOCA2OHQtNjggMjhoLTMyMHEtNDAgMC02OC0yOHQtMjgtNjh2LTE5MnEwLTQwIDI4LTY4dDY4LTI4aDMyMHE0MCAwIDY4IDI4dDI4IDY4em0tNjQwLTEwMjR2MTkycTAgNDAtMjggNjh0LTY4IDI4aC0zMjBxLTQwIDAtNjgtMjh0LTI4LTY4di0xOTJxMC00MCAyOC02OHQ2OC0yOGgzMjBxNDAgMCA2OCAyOHQyOCA2OHptNjQwIDUxMnYxOTJxMCA0MC0yOCA2OHQtNjggMjhoLTMyMHEtNDAgMC02OC0yOHQtMjgtNjh2LTE5MnEwLTQwIDI4LTY4dDY4LTI4aDMyMHE0MCAwIDY4IDI4dDI4IDY4em02NDAgNTEydjE5MnEwIDQwLTI4IDY4dC02OCAyOGgtMzIwcS00MCAwLTY4LTI4dC0yOC02OHYtMTkycTAtNDAgMjgtNjh0NjgtMjhoMzIwcTQwIDAgNjggMjh0MjggNjh6bS02NDAtMTAyNHYxOTJxMCA0MC0yOCA2OHQtNjggMjhoLTMyMHEtNDAgMC02OC0yOHQtMjgtNjh2LTE5MnEwLTQwIDI4LTY4dDY4LTI4aDMyMHE0MCAwIDY4IDI4dDI4IDY4em02NDAgNTEydjE5MnEwIDQwLTI4IDY4dC02OCAyOGgtMzIwcS00MCAwLTY4LTI4dC0yOC02OHYtMTkycTAtNDAgMjgtNjh0NjgtMjhoMzIwcTQwIDAgNjggMjh0MjggNjh6bTAtNTEydjE5MnEwIDQwLTI4IDY4dC02OCAyOGgtMzIwcS00MCAwLTY4LTI4dC0yOC02OHYtMTkycTAtNDAgMjgtNjh0NjgtMjhoMzIwcTQwIDAgNjggMjh0MjggNjh6Ii8+PC9zeW1ib2w+PHN5bWJvbCB2aWV3Qm94PSIwIDAgMTc5MiAxNzkyIiBpZD0iam9kaXQtdWwiPjxwYXRoIGQ9Ik0zODQgMTQwOHEwIDgwLTU2IDEzNnQtMTM2IDU2LTEzNi01Ni01Ni0xMzYgNTYtMTM2IDEzNi01NiAxMzYgNTYgNTYgMTM2em0wLTUxMnEwIDgwLTU2IDEzNnQtMTM2IDU2LTEzNi01Ni01Ni0xMzYgNTYtMTM2IDEzNi01NiAxMzYgNTYgNTYgMTM2em0xNDA4IDQxNnYxOTJxMCAxMy05LjUgMjIuNXQtMjIuNSA5LjVoLTEyMTZxLTEzIDAtMjIuNS05LjV0LTkuNS0yMi41di0xOTJxMC0xMyA5LjUtMjIuNXQyMi41LTkuNWgxMjE2cTEzIDAgMjIuNSA5LjV0OS41IDIyLjV6bS0xNDA4LTkyOHEwIDgwLTU2IDEzNnQtMTM2IDU2LTEzNi01Ni01Ni0xMzYgNTYtMTM2IDEzNi01NiAxMzYgNTYgNTYgMTM2em0xNDA4IDQxNnYxOTJxMCAxMy05LjUgMjIuNXQtMjIuNSA5LjVoLTEyMTZxLTEzIDAtMjIuNS05LjV0LTkuNS0yMi41di0xOTJxMC0xMyA5LjUtMjIuNXQyMi41LTkuNWgxMjE2cTEzIDAgMjIuNSA5LjV0OS41IDIyLjV6bTAtNTEydjE5MnEwIDEzLTkuNSAyMi41dC0yMi41IDkuNWgtMTIxNnEtMTMgMC0yMi41LTkuNXQtOS41LTIyLjV2LTE5MnEwLTEzIDkuNS0yMi41dDIyLjUtOS41aDEyMTZxMTMgMCAyMi41IDkuNXQ5LjUgMjIuNXoiLz48L3N5bWJvbD48c3ltYm9sIHZpZXdCb3g9IjAgMCAxNzkyIDE3OTIiIGlkPSJqb2RpdC11bmRlcmxpbmUiPjxwYXRoIGQ9Ik0xNzYgMjIzcS0zNy0yLTQ1LTRsLTMtODhxMTMtMSA0MC0xIDYwIDAgMTEyIDQgMTMyIDcgMTY2IDcgODYgMCAxNjgtMyAxMTYtNCAxNDYtNSA1NiAwIDg2LTJsLTEgMTQgMiA2NHY5cS02MCA5LTEyNCA5LTYwIDAtNzkgMjUtMTMgMTQtMTMgMTMyIDAgMTMgLjUgMzIuNXQuNSAyNS41bDEgMjI5IDE0IDI4MHE2IDEyNCA1MSAyMDIgMzUgNTkgOTYgOTIgODggNDcgMTc3IDQ3IDEwNCAwIDE5MS0yOCA1Ni0xOCA5OS01MSA0OC0zNiA2NS02NCAzNi01NiA1My0xMTQgMjEtNzMgMjEtMjI5IDAtNzktMy41LTEyOHQtMTEtMTIyLjUtMTMuNS0xNTkuNWwtNC01OXEtNS02Ny0yNC04OC0zNC0zNS03Ny0zNGwtMTAwIDItMTQtMyAyLTg2aDg0bDIwNSAxMHE3NiAzIDE5Ni0xMGwxOCAycTYgMzggNiA1MSAwIDctNCAzMS00NSAxMi04NCAxMy03MyAxMS03OSAxNy0xNSAxNS0xNSA0MSAwIDcgMS41IDI3dDEuNSAzMXE4IDE5IDIyIDM5NiA2IDE5NS0xNSAzMDQtMTUgNzYtNDEgMTIyLTM4IDY1LTExMiAxMjMtNzUgNTctMTgyIDg5LTEwOSAzMy0yNTUgMzMtMTY3IDAtMjg0LTQ2LTExOS00Ny0xNzktMTIyLTYxLTc2LTgzLTE5NS0xNi04MC0xNi0yMzd2LTMzM3EwLTE4OC0xNy0yMTMtMjUtMzYtMTQ3LTM5em0xNDg4IDE0MDl2LTY0cTAtMTQtOS0yM3QtMjMtOWgtMTQ3MnEtMTQgMC0yMyA5dC05IDIzdjY0cTAgMTQgOSAyM3QyMyA5aDE0NzJxMTQgMCAyMy05dDktMjN6Ii8+PC9zeW1ib2w+PHN5bWJvbCB2aWV3Qm94PSIwIDAgMTc5MiAxNzkyIiBpZD0iam9kaXQtdW5kbyI+PHBhdGggZD0iTTE2NjQgODk2cTAgMTU2LTYxIDI5OHQtMTY0IDI0NS0yNDUgMTY0LTI5OCA2MXEtMTcyIDAtMzI3LTcyLjV0LTI2NC0yMDQuNXEtNy0xMC02LjUtMjIuNXQ4LjUtMjAuNWwxMzctMTM4cTEwLTkgMjUtOSAxNiAyIDIzIDEyIDczIDk1IDE3OSAxNDd0MjI1IDUycTEwNCAwIDE5OC41LTQwLjV0MTYzLjUtMTA5LjUgMTA5LjUtMTYzLjUgNDAuNS0xOTguNS00MC41LTE5OC41LTEwOS41LTE2My41LTE2My41LTEwOS41LTE5OC41LTQwLjVxLTk4IDAtMTg4IDM1LjV0LTE2MCAxMDEuNWwxMzcgMTM4cTMxIDMwIDE0IDY5LTE3IDQwLTU5IDQwaC00NDhxLTI2IDAtNDUtMTl0LTE5LTQ1di00NDhxMC00MiA0MC01OSAzOS0xNyA2OSAxNGwxMzAgMTI5cTEwNy0xMDEgMjQ0LjUtMTU2LjV0Mjg0LjUtNTUuNXExNTYgMCAyOTggNjF0MjQ1IDE2NCAxNjQgMjQ1IDYxIDI5OHoiLz48L3N5bWJvbD48c3ltYm9sIHZpZXdCb3g9IjAgMCAxNzkyIDE3OTIiIGlkPSJqb2RpdC11bmxpbmsiPjxwYXRoIGQ9Ik01MDMgMTI3MWwtMjU2IDI1NnEtMTAgOS0yMyA5LTEyIDAtMjMtOS05LTEwLTktMjN0OS0yM2wyNTYtMjU2cTEwLTkgMjMtOXQyMyA5cTkgMTAgOSAyM3QtOSAyM3ptMTY5IDQxdjMyMHEwIDE0LTkgMjN0LTIzIDktMjMtOS05LTIzdi0zMjBxMC0xNCA5LTIzdDIzLTkgMjMgOSA5IDIzem0tMjI0LTIyNHEwIDE0LTkgMjN0LTIzIDloLTMyMHEtMTQgMC0yMy05dC05LTIzIDktMjMgMjMtOWgzMjBxMTQgMCAyMyA5dDkgMjN6bTEyNjQgMTI4cTAgMTIwLTg1IDIwM2wtMTQ3IDE0NnEtODMgODMtMjAzIDgzLTEyMSAwLTIwNC04NWwtMzM0LTMzNXEtMjEtMjEtNDItNTZsMjM5LTE4IDI3MyAyNzRxMjcgMjcgNjggMjcuNXQ2OC0yNi41bDE0Ny0xNDZxMjgtMjggMjgtNjcgMC00MC0yOC02OGwtMjc0LTI3NSAxOC0yMzlxMzUgMjEgNTYgNDJsMzM2IDMzNnE4NCA4NiA4NCAyMDR6bS02MTctNzI0bC0yMzkgMTgtMjczLTI3NHEtMjgtMjgtNjgtMjgtMzkgMC02OCAyN2wtMTQ3IDE0NnEtMjggMjgtMjggNjcgMCA0MCAyOCA2OGwyNzQgMjc0LTE4IDI0MHEtMzUtMjEtNTYtNDJsLTMzNi0zMzZxLTg0LTg2LTg0LTIwNCAwLTEyMCA4NS0yMDNsMTQ3LTE0NnE4My04MyAyMDMtODMgMTIxIDAgMjA0IDg1bDMzNCAzMzVxMjEgMjEgNDIgNTZ6bTYzMyA4NHEwIDE0LTkgMjN0LTIzIDloLTMyMHEtMTQgMC0yMy05dC05LTIzIDktMjMgMjMtOWgzMjBxMTQgMCAyMyA5dDkgMjN6bS01NDQtNTQ0djMyMHEwIDE0LTkgMjN0LTIzIDktMjMtOS05LTIzdi0zMjBxMC0xNCA5LTIzdDIzLTkgMjMgOSA5IDIzem00MDcgMTUxbC0yNTYgMjU2cS0xMSA5LTIzIDl0LTIzLTlxLTktMTAtOS0yM3Q5LTIzbDI1Ni0yNTZxMTAtOSAyMy05dDIzIDlxOSAxMCA5IDIzdC05IDIzeiIvPjwvc3ltYm9sPjxzeW1ib2wgdmlld0JveD0iMCAwIDE3OTIgMTc5MiIgaWQ9ImpvZGl0LXVubG9jayI+PHBhdGggZD0iTTE3MjggNTc2djI1NnEwIDI2LTE5IDQ1dC00NSAxOWgtNjRxLTI2IDAtNDUtMTl0LTE5LTQ1di0yNTZxMC0xMDYtNzUtMTgxdC0xODEtNzUtMTgxIDc1LTc1IDE4MXYxOTJoOTZxNDAgMCA2OCAyOHQyOCA2OHY1NzZxMCA0MC0yOCA2OHQtNjggMjhoLTk2MHEtNDAgMC02OC0yOHQtMjgtNjh2LTU3NnEwLTQwIDI4LTY4dDY4LTI4aDY3MnYtMTkycTAtMTg1IDEzMS41LTMxNi41dDMxNi41LTEzMS41IDMxNi41IDEzMS41IDEzMS41IDMxNi41eiIvPjwvc3ltYm9sPjxzeW1ib2wgdmlld0JveD0iMCAwIDE3OTIgMTc5MiIgaWQ9ImpvZGl0LXVwZGF0ZSI+PHBhdGggZD0iTTE2MzkgMTA1NnEwIDUtMSA3LTY0IDI2OC0yNjggNDM0LjV0LTQ3OCAxNjYuNXEtMTQ2IDAtMjgyLjUtNTV0LTI0My41LTE1N2wtMTI5IDEyOXEtMTkgMTktNDUgMTl0LTQ1LTE5LTE5LTQ1di00NDhxMC0yNiAxOS00NXQ0NS0xOWg0NDhxMjYgMCA0NSAxOXQxOSA0NS0xOSA0NWwtMTM3IDEzN3E3MSA2NiAxNjEgMTAydDE4NyAzNnExMzQgMCAyNTAtNjV0MTg2LTE3OXExMS0xNyA1My0xMTcgOC0yMyAzMC0yM2gxOTJxMTMgMCAyMi41IDkuNXQ5LjUgMjIuNXptMjUtODAwdjQ0OHEwIDI2LTE5IDQ1dC00NSAxOWgtNDQ4cS0yNiAwLTQ1LTE5dC0xOS00NSAxOS00NWwxMzgtMTM4cS0xNDgtMTM3LTM0OS0xMzctMTM0IDAtMjUwIDY1dC0xODYgMTc5cS0xMSAxNy01MyAxMTctOCAyMy0zMCAyM2gtMTk5cS0xMyAwLTIyLjUtOS41dC05LjUtMjIuNXYtN3E2NS0yNjggMjcwLTQzNC41dDQ4MC0xNjYuNXExNDYgMCAyODQgNTUuNXQyNDUgMTU2LjVsMTMwLTEyOXExOS0xOSA0NS0xOXQ0NSAxOSAxOSA0NXoiLz48L3N5bWJvbD48c3ltYm9sIHZpZXdCb3g9IjAgMCAxNzkyIDE3OTIiIGlkPSJqb2RpdC11cGxvYWQiPjxwYXRoIGQ9Ik0xMzQ0IDE0NzJxMC0yNi0xOS00NXQtNDUtMTktNDUgMTktMTkgNDUgMTkgNDUgNDUgMTkgNDUtMTkgMTktNDV6bTI1NiAwcTAtMjYtMTktNDV0LTQ1LTE5LTQ1IDE5LTE5IDQ1IDE5IDQ1IDQ1IDE5IDQ1LTE5IDE5LTQ1em0xMjgtMjI0djMyMHEwIDQwLTI4IDY4dC02OCAyOGgtMTQ3MnEtNDAgMC02OC0yOHQtMjgtNjh2LTMyMHEwLTQwIDI4LTY4dDY4LTI4aDQyN3EyMSA1NiA3MC41IDkydDExMC41IDM2aDI1NnE2MSAwIDExMC41LTM2dDcwLjUtOTJoNDI3cTQwIDAgNjggMjh0MjggNjh6bS0zMjUtNjQ4cS0xNyA0MC01OSA0MGgtMjU2djQ0OHEwIDI2LTE5IDQ1dC00NSAxOWgtMjU2cS0yNiAwLTQ1LTE5dC0xOS00NXYtNDQ4aC0yNTZxLTQyIDAtNTktNDAtMTctMzkgMTQtNjlsNDQ4LTQ0OHExOC0xOSA0NS0xOXQ0NSAxOWw0NDggNDQ4cTMxIDMwIDE0IDY5eiIvPjwvc3ltYm9sPjxzeW1ib2wgdmlld0JveD0iMCAwIDE3OTIgMTc5MiIgaWQ9ImpvZGl0LXZhbGlnbiI+PHBhdGggZD0iTTEyMTYgMzIwcTAgMjYtMTkgNDV0LTQ1IDE5aC0xMjh2MTAyNGgxMjhxMjYgMCA0NSAxOXQxOSA0NS0xOSA0NWwtMjU2IDI1NnEtMTkgMTktNDUgMTl0LTQ1LTE5bC0yNTYtMjU2cS0xOS0xOS0xOS00NXQxOS00NSA0NS0xOWgxMjh2LTEwMjRoLTEyOHEtMjYgMC00NS0xOXQtMTktNDUgMTktNDVsMjU2LTI1NnExOS0xOSA0NS0xOXQ0NSAxOWwyNTYgMjU2cTE5IDE5IDE5IDQ1eiIvPjwvc3ltYm9sPjxzeW1ib2wgdmlld0JveD0iMCAwIDE3OTIgMTc5MiIgaWQ9ImpvZGl0LXZpZGVvIj48cGF0aCBkPSJNMTc5MiAzNTJ2MTA4OHEwIDQyLTM5IDU5LTEzIDUtMjUgNS0yNyAwLTQ1LTE5bC00MDMtNDAzdjE2NnEwIDExOS04NC41IDIwMy41dC0yMDMuNSA4NC41aC03MDRxLTExOSAwLTIwMy41LTg0LjV0LTg0LjUtMjAzLjV2LTcwNHEwLTExOSA4NC41LTIwMy41dDIwMy41LTg0LjVoNzA0cTExOSAwIDIwMy41IDg0LjV0ODQuNSAyMDMuNXYxNjVsNDAzLTQwMnExOC0xOSA0NS0xOSAxMiAwIDI1IDUgMzkgMTcgMzkgNTl6Ii8+PC9zeW1ib2w+PC9zdmc+";
return d=a&&a.options&&a.options.textIcons||void 0===window.atob,d?a.$wysiwyg&&a.$wysiwyg.addClass("jodit_text_icons"):document.getElementById(c)||(e.innerHTML=window.atob(g),e.setAttribute("id",c),e.style.display="none",document.body.appendChild(e)),b={getSVGIcon:f}}}(Jodit.modules.Dom),function(a){"use strict";Jodit.defaultOptions=a.extend(!0,Jodit.defaultOptions,{useImageProcessor:!0,imageprocessor:{}}),Jodit.modules.ImageProcessor=function(b,c){Jodit.modules.ImageProcessor.unique+=1;var d,e=b.id+"_image_"+Jodit.modules.TableProcessor.unique,f=!1,g=function(){"block"===a(c).css("display")&&a(c).css("display",""),"auto"===c.style.marginLeft&&"auto"===c.style.marginRight&&(c.style.marginLeft="",c.style.marginRight="")},h=function(){d.instances&&d.instances.length&&d.instances.forEach(function(a){a.destroy()}),a(b.getDocument()).off(".jodit"+e),a(b.getWindow()).off(".jodit"+e)};return c&&(b.events.on(c,"execCommand",function(d){switch(d){case"bin":c.parentNode.removeChild(c);break;case"left":case"right":case"center":case"normal":"normal"!==d?["right","left"].indexOf(d)!==-1?(a(c).css("float",d),g()):a(c).css("float","").css({display:"block","margin-left":"auto","margin-right":"auto"}):(a(c).css("float")&&["right","left"].indexOf(a(c).css("float").toLowerCase())!==-1&&a(c).css("float",""),g());break;case"bottom":case"middle":case"top":a(c).css("vertical-align",d)}b.syncCode()}),a(c).on("dragstart",function(a){a.originalEvent.dataTransfer&&(a.originalEvent.dataTransfer.dropEffect="move",a.originalEvent.dataTransfer.setData("text/plain","-"))}).on("mousedown changesize",function(d){a(c).addClass("jodit_focused_image");var e=b.helper.offset(c);b.events.fire("showPopap",[c,Math.round(e.left+a(c).outerWidth()/2),Math.round(e.top+a(c).outerHeight())]),f=!0,d.stopPropagation()}),b.$editor.on("scroll.jodit"+e,function(){b.events.fire("hidePopap")}),a(b.getWindow()).on("keyup.jodit"+e+" mouseup.jodit"+e,function(d){return c&&c.parentNode?void(f?(f=!1,b.events.fire("hidePopap")):(a(c).removeClass("jodit_focused_image"),b.$editor.find(".jodit_focused_cell").length||b.$editor.find(".jodit_focused_image").length||b.events.fire("hidePopap"))):void h()})),d={destroy:h,instances:[]}},Jodit.modules.ImageProcessor.unique=1}(Jodit.modules.Dom),function(a){"use strict";Jodit.modules.Node=function(b){var c,d=b.getDocument(),e=function(a,b,d,e,f,g){var h,i;if(a){if(!f&&a[d])return a[d];if(a!==b){if(h=a[e])return h;for(i=a.parentNode;i&&i!==b;i=i.parentNode)if(h=i[e],h&&(void 0===g||g.call(c,h)))return h}}},f=function(a,b){return e(a,b,"firstChild","nextSibling")},g=function(a,c){var d;return a=a.toLowerCase(),"text"===a?d=b.getDocument().createTextNode("string"==typeof c?c:""):(d=b.getDocument().createElement(d),d.innerHTML=c),d},h=function(b,c,e,f){var g="string"==typeof c?d.createElement(c):c;if(!f)for(;b.firstChild;)g.appendChild(b.firstChild);return e&&a.each(b.attributes,function(a,b){g.setAttribute(b.name,b.nodeValue)}),b.parentNode.replaceChild(g,b),g},i=function(a,b){var c="string"==typeof b?d.createElement(b):b,e=d.createElement("span");return a.parentNode.insertBefore(e,a),c.appendChild(a),e.parentNode.replaceChild(c,e),c},j=function(a,c,d){if(null!==a)for(;a.parentNode&&a!==b.editor;){if(c(a))return d?d(a):a;a=a.parentNode}},k=function(a,b,c){var d,e=a,f=function(a){return a.nodeName.toLowerCase().match(new RegExp("^("+b+")$","i"))},g=function(a){return a};if(e=j(a,f,g),!c||!e)return e;for(;e&&e.parentNode;)d=e,e=j(e.parentNode,f,g);return d},l=function(a,c){var d=a;do d=b.node.findNext(d,a),d&&c.call(d,d);while(d)},m=function(a,b){return!!k(a,b)},n=function(a){return a&&a.nodeType===Node.TEXT_NODE&&0===a.nodeValue.replace(Jodit.INVISIBLE_SPACE_REG_EXP,"").length},o=function(a,b){var c;do c&&c.parentNode.removeChild(c),c=a[b===!0?"previousSibling":"nextSibling"],a=c;while(a&&a.nodeType===Node.TEXT_NODE&&!a.textContent.length);return a||null},p=function(a){if(a){if(a.nodeType!==Node.ELEMENT_NODE)return!1;if(Jodit.BLOCKS.indexOf(a.tagName)!==-1)return!0}return!1},q=function(a){if(a){if(a.nodeType!==Node.ELEMENT_NODE)return!1;if(Jodit.ALONE.indexOf(a.tagName)!==-1)return!0}return!1},r=function(a){return"object"==typeof Node?a instanceof Node:"object"==typeof a&&"number"==typeof a.nodeType&&"string"==typeof a.nodeName},s=function(a){return"object"==typeof HTMLElement?a instanceof HTMLElement:"object"==typeof a&&null!==a&&1===a.nodeType&&"string"==typeof a.nodeName},t=function(b,c,e){function f(a,b){var c,d,e=0;if(a)for(c=a.nodeType,a=a.previousSibling;a;a=a.previousSibling)d=a.nodeType,(!b||d!==Node.TEXT_NODE||d!==c&&a.nodeValue.length)&&(e+=1,c=d);return e}function g(b){function c(a){var b=a.previousSibling&&"SPAN"===a.previousSibling.nodeName,c=a.nextSibling&&"SPAN"===a.nextSibling.nodeName;return b&&c}var d,e,f=b.childNodes,h=b.nodeType;for(d=f.length-1;d>=0;d-=1)g(f[d]);if(h!==Node.DOCUMENT_NODE){if(h===Node.TEXT_NODE&&b.nodeValue.length>0){if(e=a.trim(b.nodeValue).length,(!p(b.parentNode)||e>0||0===e)&&c(b))return}else if(h===Node.ELEMENT_NODE&&(f=b.childNodes,f.length||/^(br|hr|input|img)$/i.test(b.nodeName)))return;a(b).remove()}return b}var h,i,j,k=d.createRange();if(b&&c)return k.setStart(b.parentNode,f(b)),k.setEnd(c.parentNode,f(c)),h=k.extractContents(),k=d.createRange(),k.setStart(c.parentNode,f(c)+1),k.setEnd(b.parentNode,f(b)+1),i=k.extractContents(),j=b.parentNode,j.insertBefore(g(h),b),e?j.insertBefore(e,b):j.insertBefore(c,b),j.insertBefore(g(i),b),b.parentNode.removeChild(b),e||c},u=function(a){return a&&p(a)&&!/^(TD|TH|CAPTION|FORM)$/.test(a.nodeName)&&!/^(fixed|absolute)/i.test(a.style.position)};return c={wrap:i,isNode:r,isElement:s,isBlock:p,isAlone:q,canSplitBlock:u,split:t,create:g,replace:h,find:e,findNext:f,getParentNode:j,parentNode:k,each:l,hasParent:m,isEmptyTextNode:n,clearEmptyTextSibling:o}}}(Jodit.modules.Dom),function(a){"use strict";Jodit.defaultOptions=a.extend(!0,Jodit.defaultOptions,{observer:{subtree:!0,childList:!0,characterData:!0,attributes:!0}}),Jodit.modules.Observer=function(b){var c,d,e,f,g,h,i,j,k=new Undo.Stack,l=!1,m=b.editor,n=[],o=function(a){a===!0||a===!1?l=a:1===a?(j=l,l=!0):0===a&&(l=void 0!==j&&j)},p=Undo.Command.extend({constructor:function(a,b){this.oldValue=a,this.newValue=b},execute:function(){},undo:function(){o(!0),b.snapshot.restore(this.oldValue)},redo:function(){o(!0),b.snapshot.restore(this.newValue)}}),q=function(){b.getRealMode()===Jodit.MODE_WYSIWYG&&(e.toggleClass("disabled",!k.canRedo()),f.toggleClass("disabled",!k.canUndo()))},r=function(){k.redo()},s=function(){k.undo()},t=function(){i.disconnect&&i.disconnect(),n.forEach(function(a){clearTimeout(a)})},u=function(){g=b.snapshot.make(),e=b.$toolbar.find(".toolbar-redo"),f=b.$toolbar.find(".toolbar-undo"),k.changed(),i=new MutationObserver(function(a){return l?void o(!1):(clearTimeout(d),d=setTimeout(function(){h=b.snapshot.make(),b.snapshot.equal(h,g)||(k.execute(new p(g,h)),g=h)},300),void(n[n.length]=d))}),i.observe(m,b.options.observer),a(m).on("keydown",function(a){c.ctrlKey(a)&&(a.which===Jodit.keys.Z&&k.canUndo()?(k.undo(),a.preventDefault(),a.stopImmediatePropagation()):a.which===Jodit.keys.Y&&k.canRedo()&&(k.redo(),a.preventDefault(),a.stopImmediatePropagation()))})};return void 0===window.MutationObserver&&(window.MutationObserver=function(b){this.observe=function(c){a(c).on("keydown mousedown drop",b)}}),k.changed=q,c={changed:q,block:o,redo:r,undo:s,destroy:t,init:u}}}(Jodit.modules.Dom),function(a){"use strict";Jodit.modules.ObserverArea=function(b){var c,d,e,f,g,h,i,j={},k=new Undo.Stack,l=!1,m=b.area,n=function(a){if(document.activeElement!==a)return-1;if(void 0!==a.selectionStart)return{start:a.selectionStart,end:a.selectionEnd};if(a.createTextRange&&document.selection){var b,c,d,e=document.selection.createRange();if(e.parentElement()===a){for(d=a.createTextRange(),d.moveToBookmark(e.getBookmark()),b=0;d.compareEndPoints("EndToStart",d)>0;d.moveEnd("character",-1))b+=1;for(d.setEndPoint("StartToStart",a.createTextRange()),c={start:0,end:b};d.compareEndPoints("EndToStart",d)>0;d.moveEnd("character",-1))c.start+=1,c.end+=1;return c}}return-1},o=function(b,c){void 0===c&&(c=b),a(m).each(function(){if(void 0!==this.selectionStart)this.selectionStart=b,this.selectionEnd=c;else if(this.setSelectionRange)this.setSelectionRange(b,c);else if(this.createTextRange){var a=this.createTextRange();a.collapse(!0),a.moveEnd("character",c),a.moveStart("character",b),a.select()}})},p=Undo.Command.extend({constructor:function(a,b,c,d){this.selectionStart=c,this.selectionSecond=d,this.oldValue=a,this.newValue=b},execute:function(){},undo:function(){j.block(!0),b.setAreaValue(this.oldValue),o(this.selectionStart.start,this.selectionStart.end),b.events.fire("updateStackEditor")},redo:function(){j.block(!0),b.setAreaValue(this.newValue),o(this.selectionSecond.start,this.selectionSecond.end),b.events.fire("updateStackEditor")}}),q=function(){b.getRealMode()===Jodit.MODE_TEXTAREA&&(d.toggleClass("disabled",!k.canRedo()),e.toggleClass("disabled",!k.canUndo()))},r=function(a){l=!!a},s=function(){k.redo()},t=function(){k.undo()},u=function(){f=m.value,h={start:0},d=this.parent.$toolbar.find(".toolbar-redo"),e=this.parent.$toolbar.find(".toolbar-undo"),k.changed(),b.$area.on("keydown",function(a){var b,c,d,e,f,g;a.which===Jodit.keys.TAB&&(a.preventDefault(),b=n(m),c=b.start,d=b.end,g=m.value,f=g.substring(b.start,b.end),b.start!==b.end&&f.match(/\n/)?(e="\t"+f.replace(/\n/g,"\n\t"),d=c+e.length):(e="\t",d+=1,c=d),m.value=g.substring(0,b.start)+e+g.substring(b.end),o(c,d))}).on("keydown",function(a){j.ctrlKey(a)&&(a.which===Jodit.keys.Z&&k.canUndo()?(k.undo(),a.preventDefault(),a.stopImmediatePropagation()):a.which===Jodit.keys.Y&&k.canRedo()&&(k.redo(),a.preventDefault(),a.stopImmediatePropagation()))}).on("keydown mousedown change updatestack",function(a){return l||a.which===Jodit.keys.CTRL?void j.block(!1):(clearTimeout(c),void(c=setTimeout(function(){g=m.value,g!==f&&(i=n(m),k.execute(new p(f,g,h,i)),h=i,f=g)},300)))})};return k.changed=q,j={changed:q,block:r,redo:s,undo:t,init:u}}}(Jodit.modules.Dom),function(a){"use strict";Jodit.defaultOptions=a.extend(!0,Jodit.defaultOptions,{showPlaceholder:!0,useInputsPlaceholder:!0,placeholder:"Type something"}),Jodit.modules.Placeholder=function(b){var c,d,e=function(){d.remove()},f=function(){var c=0,e=0,f=0,g=0;b.$editor[0].firstChild&&b.$editor[0].firstChild.nodeType===Node.ELEMENT_NODE?(c=a(b.$editor[0].firstChild).css("margin-top"),e=a(b.$editor[0].firstChild).css("margin-left"),f=a(b.$editor[0].firstChild).css("padding-top"),g=a(b.$editor[0].firstChild).css("padding-left"),d.css({"font-size":a(b.$editor[0].firstChild).css("font-size"),"line-height":a(b.$editor[0].firstChild).css("line-height")})):d.css({"font-size":b.$editor.css("font-size"),"line-height":b.$editor.css("line-height")}),d.css({marginTop:Math.max(b.$editor.css("margin-top"),c),paddingTop:Math.max(b.$editor.css("padding-top"),f),paddingLeft:Math.max(b.$editor.css("padding-left"),g),marginLeft:Math.max(b.$editor.css("margin-left"),e)}),d.show()},g=function(){d.hide()},h=function(){d=a('<span class="jodit_placeholder">'+b.i18n(b.options.placeholder)+"</span>"),b.options.useInputsPlaceholder&&b.element.hasAttribute("placeholder")&&d.html(b.$element.attr("placeholder")),b.$workflow.append(d),f()};return c={init:h,destroy:e,show:f,hide:g}}}(Jodit.modules.Dom),function(a){"use strict";Jodit.modules.Plugins=function(a){var b,c,d={},e=function(){if(Jodit.plugins&&(c=Object.keys(Jodit.plugins),c.length))for(b=0;b<c.length;b+=1)d[c[b]]=new Jodit.plugins[c[b]](a)};return d={init:e}}}(Jodit.modules.Dom),function(a){"use strict";Jodit.defaultOptions=a.extend(!0,Jodit.defaultOptions,{popap:{img:[{name:"bin",tooltip:"Delete"},{name:"pencil",exec:function(a){this.plugins.image&&this.plugins.image.open&&this.plugins.image.open.call(a)},tooltip:"Edit"},{name:"valign",list:{top:"Top",middle:"Middle",bottom:"Bottom"},tooltip:"Vertical align"},{name:"left",list:{left:"Left",right:"Right",center:"Center",normal:"Normal"},tooltip:"Horizontal align"}],table:[{name:"brush",popap:function(b,c){var d,e,f,g=a(b).find(".jodit_focused_cell"),h=g.css("color"),i=g.css("background-color");return d=this.form.buildColorPicker(a.proxy(function(a){g.css("background-color",a),c&&c()},this),i),e=this.form.buildColorPicker(a.proxy(function(a){g.css("color",a),c&&c()},this),h),f=this.form.buildTabs({Background:d,Text:e})},tooltip:"Background"},{name:"valign",list:{top:"Top",middle:"Middle",bottom:"Bottom"},tooltip:"Vertical align"},"|",{name:"splitv",tooltip:"Split vertical"},{name:"splitg",tooltip:"Split horizontal"},"\n",{name:"merge",tooltip:"Merge"},{name:"addcolumn",list:{addcolumnbefore:"Insert column before",addcolumnafter:"Insert column after"},tooltip:"Add column"},{name:"addrow",list:{addrowbefore:"Insert row above",addrowafter:"Insert row below"},tooltip:"Add row"},{name:"bin",list:{bin:"Delete table",binrow:"Delete row",bincolumn:"Delete column",empty:"Empty cell"},tooltip:"Delete"}]}}),Jodit.modules.Popap=function(b){var c,d,e,f,g,h=function(){e.removeClass("active").find(".jodit_dropdown_open,.active").removeClass("jodit_dropdown_open active"),f.hide()},i=function(c){var e,j="string"===a.type(c)?c:c.name,k=a('<li><a href="javascript:void(0)"></a><div class="jodit_tooltip"></div></li>'),l=k.find("a");return k.addClass("toolbar-"+j),l.append(b.icons.getSVGIcon(j)),c.content&&l.html(b.i18n(c.content)),c.tooltip?k.find(".jodit_tooltip").html(b.i18n(c.tooltip)):k.find(".jodit_tooltip").remove(),c.list&&(e=a('<ul class="jodit_dropdownlist"></ul>'),a.each(c.list,function(a,b){var c=i({name:a,content:b});e.append(c)}),k.append(e),k.addClass("jodit_with_dropdownlist")),k.on("mouseup",function(a){a.stopPropagation()}).on("mousedown",a.proxy(function(a){k.hasClass("jodit_with_dropdownlist")?(g.find(".jodit_with_dropdownlist").not(k).removeClass("jodit_dropdown_open active"),k.toggleClass("jodit_dropdown_open active"),f.hide()):(g.find(".jodit_with_dropdownlist").removeClass("jodit_dropdown_open active"),c.popap?(f.show(),k.append(f),f.empty(),f.html(c.popap.call(b,d,function(){setTimeout(function(){f.hide()},30)}))):c.exec?(c.exec.call(b,d),h()):(b.events.fire(d,"execCommand",[j]),h())),a.stopPropagation(),a.preventDefault()},this)),k},j=function(c,f){h(),g.empty(),a.each(b.options.popap[d.tagName.toLowerCase()],function(a,b){switch(b){case"\n":g.append('<li class="next_row"></li>');break;case"|":g.append('<li class="separator"></li>');break;default:g.append(i(b))}}),e.addClass("active"),e.css({left:c+"px",top:f+"px",marginLeft:-Math.round(e.outerWidth()/2)+"px"})},k=function(){e||(e=a('<div class="jodit jodit_popap_inline"></div>'),g=a('<ul class="jodit_toolbar toolbar-popap"></ul>'),f=a('<div class="jodit_toolbar_popap non-selected"></div>'),f.on("mousedown",function(a){a.stopImmediatePropagation()}),b&&b.options&&b.options.textIcons&&e.addClass("jodit_text_icons"),e.append(g),a(document.body).append(e)),b.events.on("hidePopap",h),b.events.on("showPopap",function(a,b,c){d=a,j(b,c)})};return c={init:k}}}(Jodit.modules.Dom),function(a){"use strict";Jodit.defaultOptions=a.extend(!0,Jodit.defaultOptions,{useIframeResizer:!0,useTableResizer:!0,useImageResizer:!0,resizer:{min_width:10,min_height:10}}),Jodit.modules.Resizer=function(b){var c,d,e,f,g,h,i,j,k,l,m={},n=!1,o=!1,p=a('<div class="resizer"><i class="topleft"></i><i class="topright"></i><i class="bottomright"></i><i class="bottomleft"></i></div>'),q=function(c){var d,e=a(c);"IFRAME"===e[0].tagName&&(e.parent().hasClass("jodit_iframe_wrapper")?e=e.parent():(d=a('<span data-jodit-temp="1" contenteditable="false" draggable="true" class="jodit_iframe_wrapper"></span>'),d.css({display:"inline-block"===e.css("display")?"inline-block":"block",width:e.width(),height:e.height()}),b.node.wrap(e[0],d[0]),d.on("changesize",function(){a(c).attr({width:d.width(),height:d.height()})}),e=d)),e.off(".jodit-resizer").on("drag.jodit-resizer",function(a){p.hide()}).on("mousedown.jodit-resizer",function(a){e.get(0).clicked=!0}).on("mouseup.jodit-resizer",function(a){e.get(0).clicked&&setTimeout(function(){e.get(0).clicked=!1,p.$element=e,p.show().trigger("updatesize")},50)})};return p.find("i").on("mousedown",function(a){return p.$element.length&&p.$element.parent().length?(p.$element.get(0).clicked=!1,c=a.target||a.srcElement,a.preventDefault(),a.stopImmediatePropagation(),f=parseInt(p.$element.outerWidth(),10),g=parseInt(p.$element.outerHeight(),10),h=f/g,n=!0,o=!1,d=parseInt(a.clientX,10),void(e=parseInt(a.clientY,10))):(p.hide(),!1)}),a(b.windows).off(".jodit-resizer"+b.id+" updateresizer.jodit-resizer"+b.id).on("mousemove.jodit-resizer"+b.id,function(a){n&&(o=!0,k=parseInt(a.clientX,10)-d,l=parseInt(a.clientY,10)-e,"IMG"===p.$element[0].tagName?k?(j=f+(c.className.match(/left/)?-1:1)*k,i=Math.round(j/h)):(i=g+(c.className.match(/top/)?-1:1)*l,j=Math.round(i*h)):(j=f+(c.className.match(/left/)?-1:1)*k,i=g+(c.className.match(/top/)?-1:1)*l),j>b.options.resizer.min_width&&(j<p.parent().outerWidth()?p.$element.css("width",j+"px"):p.$element.css("width","100%")),i>b.options.resizer.min_height&&p.$element.css("height",i+"px"),p.trigger("updatesize"),a.stopImmediatePropagation())}).on("resize.jodit-resizer"+b.id+" updateresizer.jodit-resizer",function(a){p.is(":visible")&&p.trigger("updatesize")}).on("mouseup.jodit-resizer"+b.id+" keydown.jodit-resizer",function(a){p.is(":visible")&&(n?(o&&b.syncCode(),n=!1,o=!1,a.stopImmediatePropagation()):p.$element.get(0).clicked&&p.$element.length&&p.$element.parent().length||p.hide())}),b.$editor.on("scroll.jodit-resizer"+b.id,function(){p.is(":visible")&&p.hide()}),p.on("updatesize.jodit-resizer"+b.id,function(){if(p.is(":visible")){var a=b.helper.offset(p.$element,!0);p.css({top:a.top-1+"px",left:a.left-1+"px",width:p.$element.outerWidth()+2+"px",height:p.$element.outerHeight()+2+"px"}),p.$element.trigger("changesize")}}),b.$workflow.append(p),m={bind:q}}}(Jodit.modules.Dom),function(a){"use strict";Jodit.defaultOptions=a.extend(!0,Jodit.defaultOptions,{useTableProcessor:!0,tableprocessor:{accuracy:10,resizerOffset:20}}),Jodit.modules.TableProcessor=function(b,c){Jodit.modules.TableProcessor.unique+=1;var d,e,f,g=10,h=b.id+"_"+Jodit.modules.TableProcessor.unique,i=b.options.tableprocessor.accuracy,j=a(c),k=function(){return j.find("tr").length},l=function(){var b,c,d,e,f,g,h=j.find("tr"),i=function(a,b){a.data("colcount",b)};return h.length&&h.each(function(h,j){b=a(j),f=0,b.find("td,th").each(function(h,j){if(e=a(j).attr("colspan")?parseInt(a(j).attr("colspan"),10):1,a(j).attr("rowspan"))for(c=parseInt(a(j).attr("rowspan"),10)-1,d=b.next(),g=0;g<c;g+=1)i(d,e),d=d.next();f+=e}),i(b,f)}),f=0,h.each(function(b,c){f=Math.max(f,parseInt(a(c).data("colcount"),10)),a(c).removeData("colcount")}),f},m=!1,n=!1,o=!1,p=function(a,b){var c,d,e,f,g,h,i=o&&m?m:[],p=l(),q=k(),r=o&&n?n:j.find("td,th"),s=r.get(0),t=0,u=function(b,c,d,j){if(void 0===i[b]&&(i[b]=[]),i[b][c]=d,a&&a(b,c,d)===!1)return!1;if(void 0===j&&void 0!==d&&(g=0,e=0,d.hasAttribute("colspan")&&(g=parseInt(d.getAttribute("colspan"),10)),d.hasAttribute("rowspan")&&(e=parseInt(d.getAttribute("rowspan"),10)),g>1||e>1))for(f=b,h=c;f<b+e-1||h<c+g-1;)if(h<c+g-1?h+=1:(h=c,f+=1),u(f,h,d,!0)===!1)return!1};for(o&&!n&&(n=r),c=0;c<q;c+=1)for(d=0;d<p;d+=1)if(void 0===i[c]||void 0===i[c][d]){if(u(c,d,s)===!1)return s;t+=1,s=r[t]}else if(a&&a(c,d,i[c][d])===!1)return i[c][d];return m=i,i},q=function(a,b){var c,d,e=!1;return p(function(f,g,h){if(a!==h||e&&(b!==!0||c!==f)&&2!==b){if(e)return!1}else{if(c=f,d=g,!b)return!1;e=!0}}),[c,d]},r=function(a,b){var c;return p(function(d,e,f){if(c=f,a===d&&b===e)return!1}),c},s=function(){return j.find(".jodit_focused_cell")},t=function(){var a,b,c=s(),d=0,e=1e6,f=0,g=1e6;return!!c.length&&(c.each(function(c,h){var i=q(h);a=0,b=0,h.hasAttribute("colspan")&&(a=parseInt(h.getAttribute("colspan"),10),a&&(a-=1)),h.hasAttribute("rowspan")&&(b=parseInt(h.getAttribute("rowspan"),10),b&&(b-=1)),d=Math.max(i[1]+a,d),e=Math.min(i[1],e),f=Math.max(i[0]+b,f),g=Math.min(i[0],g)}),[g,e,f,d])},u=function(b,c,d){var e=a(b);e.addClass("jodit_iamalready_was").data(c,void 0===d?1:d)},v=function(){j.find(".jodit_iamalready_was").each(function(b,c){var d=a(c);d.removeClass("jodit_iamalready_was"),void 0!==d.data("append")&&(a(d.data("append")).append(c),d.removeData("append")),void 0!==d.data("before")&&(a(d.data("before")).before(c),d.removeData("before")),void 0!==d.data("width")&&(d.css("width",d.data("width")),d.removeData("width")),void 0!==d.data("colspan")&&(d.data("colspan")>1?d.attr("colspan",d.data("colspan")):d.removeAttr("colspan"),d.removeData("colspan")),void 0!==d.data("rowspan")&&(d.attr("rowspan",d.data("rowspan")),d.removeData("rowspan")),void 0!==d.data("remove")&&(d.removeData("remove"),d.remove())}),j.trigger("change")},w=function(c){s().each(function(d,e){var f,g=p(),h=q(e,2);for(f=0;f<g.length;f+=1)c?h[1]-1<0||g[f][h[1]]!==g[f][h[1]-1]?a(g[f][h[1]]).before(a(b.getDocument().createElement("td")).append("<br>")):u(g[f][h[1]],"colspan",parseInt(g[f][h[1]].getAttribute("colspan"),10)+1):h[1]+1>=g[f].length||g[f][h[1]]!==g[f][h[1]+1]?a(g[f][h[1]]).after(a(b.getDocument().createElement("td")).append("<br>")):u(g[f][h[1]],"colspan",parseInt(g[f][h[1]].getAttribute("colspan"),10)+1);v()}),a(b.windows).trigger("updateresizer.jodit-resizer")},x=function(c,d){var e=a(b.getDocument().createElement("td"));return e.append(d),a(c).append(e),e.get(0)},y=function(c){s().each(function(a,d){var e,f=p(),g=q(d,2),h=b.getDocument().createElement("tr");for(e=0;e<f[0].length;e+=1)c?g[0]-1<0||f[g[0]][e]!==f[g[0]-1][e]?x(h,"<br>"):u(f[g[0]][e],"rowspan",parseInt(f[g[0]][e].getAttribute("rowspan"),10)+1):g[0]+1>=f.length||f[g[0]][e]!==f[g[0]+1][e]?x(h,"<br>"):u(f[g[0]][e],"rowspan",parseInt(f[g[0]][e].getAttribute("rowspan"),10)+1);j.find("tr").eq(g[0])[c?"before":"after"](h),v()}),a(b.windows).trigger("updateresizer.jodit-resizer")},z=function(){var c,d=a(b.getDocument().createElement("tr"));for(c=1;c<=l();c+=1)x(d,"<br>");return j.append(d),d.get(0)},A=function(){s().each(function(a,b){var c,d=q(b);for(c=p(),a=0;a<c.length;a+=1)d[1]-1>=0&&c[a][d[1]-1]===c[a][d[1]]?u(c[a][d[1]-1],"colspan",parseInt(c[a][d[1]-1].getAttribute("colspan"),10)-1):d[1]+1<c[0].length&&c[a][d[1]+1]===c[a][d[1]]?u(c[a][d[1]],"colspan",parseInt(c[a][d[1]].getAttribute("colspan"),10)-1):u(c[a][d[1]],"remove");v()})},B=function(){var c=[];return 1===j.find("tr").length?(j.remove(),void b.events.fire("hidePopap")):void s().each(function(b,d){var e,f,g,h,i=q(d),j=d.parentNode;if(!c[i[0]]){for(c[i[0]]=!0,f=p(),e=0;e<f[0].length;e+=1)if(i[0]-1>=0&&f[i[0]-1][e]===f[i[0]][e])u(f[i[0]-1][e],"rowspan",parseInt(f[i[0]-1][e].getAttribute("rowspan"),10)-1);else if(i[0]+1<f.length&&f[i[0]+1][e]===f[i[0]][e]){for(g=e+1,h=!1;g<f[0].length&&!h;)f[i[0]+1][g]!==f[i[0]][e]&&j!==f[i[0]+1][g].parentNode&&(u(f[i[0]][e],"rowspan",parseInt(f[i[0]][e].getAttribute("rowspan"),10)-1),u(f[i[0]][e],"before",f[i[0]+1][g]),h=!0),g+=1;h||u(f[i[0]][e],"append",a(j).next())}v(),a(j).remove()}})},C=function(b,c,d,e){var f,g,h,k=p();for(f=0;f<k.length;f+=1)k[f][b]!==k[f][d?b-1:b+1]&&(g=a(k[f][b]).outerWidth(),h=(g+c)/j.outerWidth()*100,u(k[f][b],"width",h.toFixed(i)+"%"));e||v()},D=function(){var a,b,c,d,e,f;for(a=p(),c=0;c<a[0].length;c+=1){for(d=0,e=1e6,f=!1,b=0;b<a.length;b+=1){if(!a[b][c].hasAttribute("colspan")){f=!0;break}d=Math.max(d,parseInt(a[b][c].getAttribute("colspan"),10)),e=Math.min(d,parseInt(a[b][c].getAttribute("colspan"),10))}if(!f)for(b=0;b<a.length;b+=1)u(a[b][c],"colspan",d-e)}for(b=0;b<a.length;b+=1)for(c=0;c<a[b].length;c+=1)a[b][c].hasAttribute("rowspan")&&1===parseInt(a[b][c].getAttribute("rowspan"),10)&&a[b][c].removeAttribute("rowspan"),a[b][c].hasAttribute("colspan")&&1===parseInt(a[b][c].getAttribute("colspan"),10)&&a[b][c].removeAttribute("colspan");for(v(),a=p(),c=0;c<a[0].length;c+=1)C(c,0,!1,!0);v()},E=function(){var b,c,d,e,f,g=t(),h=0,k=[],l=0,m=0;g&&(g[0]-g[2]||g[1]-g[3])&&(p(function(i,j,n){if(i>=g[0]&&i<=g[2]&&j>=g[1]&&j<=g[3]){if(d=a(n),d.hasClass("jodit_iamalready_was"))return;d.addClass("jodit_iamalready_was"),i===g[0]&&(h+=d.outerWidth()),""!==a.trim(n.innerHTML.replace(/<br(\/)?>/g,""))&&k.push(n.innerHTML+"<br/>"),e=0,f=0,n.hasAttribute("colspan")&&(e=parseInt(n.getAttribute("colspan"),10)),n.hasAttribute("rowspan")&&(f=parseInt(n.getAttribute("rowspan"),10)),e>1&&(l+=e-1),f>1&&(m+=f-1),b?u(d,"remove",1):(b=a(n),c=j)}}),l=g[3]-g[1]+1,m=g[2]-g[0]+1,l>1&&b.attr("colspan",l),m>1&&b.attr("rowspan",m),u(b,"width",(h/j.outerWidth()*100).toFixed(i)+"%"),b.html(b.html()+k.join("")),C(c,0,!1,!0),v()),D()},F=function(c,d){var e,f=a(c);return f.next().length?f.next().get(0):f.parent().next().length?f.parent().next().find("td:first-child,th:first-child").get(0):!!d&&(e=z(),a(b.windows).trigger("updateresizer.jodit-resizer"),a(e).find("td:first-child,th:first-child").get(0))},G=function(b){var c=a(b);return c.prev().length?c.prev().get(0):c.parent().prev().length?c.parent().prev().find("td:last,th:last").get(0):b},H=function(){var c,d,e,f,g;s().each(function(h,k){g=a(k),c=q(g.get(0)),!g.attr("colspan")||parseInt(g.attr("colspan"),10)<2?p(function(a,b,d){c[1]===b&&c[0]!==a&&d!==g.get(0)&&u(d,"colspan",d.hasAttribute("colspan")?parseInt(d.getAttribute("colspan"),10)+1:2)}):u(g,"colspan",parseInt(g.attr("colspan"),10)-1),d=a(b.getDocument().createElement("td")).append("<br>"),g.attr("rowspan")&&u(d,"rowspan",g.attr("rowspan")),g.after(d),e=g.outerWidth(),f=e/j.outerWidth()/2,u(g,"width",(100*f).toFixed(i)+"%"),u(d,"width",(100*f).toFixed(i)+"%"),v(),g.removeClass("jodit_focused_cell")}),D()},I=function(){var c,d,e,f,g,h;s().each(function(i,j){h=a(j),d=a(b.getDocument().createElement("td")),d.append("<br>"),e=a(b.getDocument().createElement("tr")),c=q(j),!h.attr("rowspan")||parseInt(h.attr("rowspan"),10)<2?(p(function(a,b,d){c[0]===a&&c[1]!==b&&d!==h.get(0)&&u(d,"rowspan",d.hasAttribute("rowspan")?parseInt(d.getAttribute("rowspan"),10)+1:2)}),h.closest("tr").after(e),e.append(d)):(u(h,"rowspan",parseInt(h.attr("rowspan"),10)-1),p(function(b,d,e){b>c[0]&&b<c[0]+parseInt(h.attr("rowspan"),10)&&c[1]>d&&a(e).parent().index()===b&&(g=a(e)),c[0]<b&&e===h.get(0)&&(f=a(e.parentNode))}),g?g.after(d):f.prepend(d)),h.attr("colspan")&&u(d,"colspan",h.attr("colspan")),v(),h.removeClass("jodit_focused_cell")}),D()},J=function(){d.instances&&d.instances.length&&d.instances.forEach(function(a){a.destroy()}),a(b.getDocument()).add(document).off(".jodit"+h).off(".jodit-border-resizer"+h)},K=function(){var d,f,i,k,l,m,n,o,s,t;e||(e=a('<div class="border_resizer"><div></div></div>'),e.on("mousedown",function(c){d=!0,t=!1,f=parseInt(c.clientX,10),o=parseInt(e.css("left"),10),e.addClass("border_resizer_clicked"),b.$editor.addClass("non-selected"),k=q(e.targets[0],!0),l=q(e.targets[1]),m=1e8,n=0,p(function(c,d,e){d===k[1]&&(m=Math.min(b.helper.offset(e,!0).left,m)),d===l[1]&&(n=Math.max(b.helper.offset(e,!0).left+a(e).outerWidth(),n))})}),a(b.getDocument()).add(document).off(".jodit-border-resizer"+h).on("mousemove.jodit-border-resizer"+h,b.helper.throttle(function(a){return c&&c.parentNode?void(d&&(t=!0,s=parseInt(a.clientX,10)-f,o+s>m+b.options.tableprocessor.resizerOffset&&o+s<n-b.options.tableprocessor.resizerOffset&&e.css("left",o+s+"px"))):void J()},g)).on("mouseup.jodit-border-resizer"+h,function(a){return c&&c.parentNode?(d&&e.is(":visible")&&(e.removeClass("border_resizer_clicked"),e.hide()),t&&(o=parseInt(e.css("left"),10),C(k[1],s,!1,!0),C(l[1],-s,!0)),t=!1,d=!1,void b.$editor.removeClass("non-selected")):void J()}),b.$workflow.append(e)),j.off(".jodit-border-resizer").on("mousemove.jodit-border-resizer","td,th",b.helper.throttle(function(c){var d,f=c.target||c.srcElement;e.hasClass("border_resizer_clicked")||"TD"!==f.tagName&&"TH"!==f.tagName||(d=b.helper.offset(f,b.options.iframe),i=a(f).outerWidth(),Math.abs(d.left-c.clientX)<10&&a(f).prev().length||Math.abs(d.left+i-c.clientX)<10&&a(f).next().length?(e.target=a(f),e.$table=a(f).closest("table"),e.targets=[],Math.abs(d.left-c.clientX)<10?(k=q(f),e.targets[0]=r(k[0],k[1]-1),e.targets[1]=f):(k=q(f,!0),e.targets[0]=f,e.targets[1]=r(k[0],k[1]+1)),e.css({display:"block",left:(Math.abs(d.left-c.clientX)<10?b.helper.offset(f,!0).left:b.helper.offset(f,!0).left+i)+"px",top:b.helper.offset(j,!0).top+"px",height:j.outerHeight()+"px"})):e.css({display:"none"}))},g))},L=function(c,d){var e,f,g,h,i,k,l=c.target||c.srcElement;if(k=p(),a(l).addClass("jodit_focused_cell"),c.shiftKey||o){for(e=t(),g=e[0];g<=e[2];g+=1)for(f=e[1];f<=e[3];f+=1)a(k[g][f]).addClass("jodit_focused_cell");if(!d)return L(c,!0)}else j.find("td,th").not(l).removeClass("jodit_focused_cell"),e=t();s().length&&(h=k[e[2]][e[3]],i=k[e[2]][e[1]],b.events.fire("showPopap",[j.get(0),b.helper.offset(i).left+Math.round((b.helper.offset(h).left+a(h).outerWidth()-b.helper.offset(i).left)/2),b.helper.offset(h).top+a(h).outerHeight()]))};return c&&(K(),j.on("dragstart","td,th",function(a){o&&a.preventDefault()}).on("mousedown","td,th",function(a){L(a),o=!0,f=a.target||a.srcElement}).on("mousemove","td,th",b.helper.throttle(function(b){var c=b.target||b.srcElement;o&&f!==c&&(s().removeClass("jodit_focused_cell"),a(f).addClass("jodit_focused_cell"),b.start(),L(b),b.preventDefault())},g)),a(b.getDocument()).add(document).on("keyup.jodit"+h+" mouseup.jodit"+h,function(a){return c&&c.parentNode?void(o?(o=!1,a.stopImmediatePropagation()):(j.find("td,th").removeClass("jodit_focused_cell"),b.$editor.find(".jodit_focused_cell").length||b.$editor.find(".jodit_focused_image").length||b.events.fire("hidePopap"))):void J()}),j.length&&a(b.getDocument()).on("keydown.jodit"+h,function(a){if(!c||!c.parentNode)return void J();var d,e,g,h,i=b.selection;if(h=i.current(),(a.which===Jodit.keys.TOP||a.which===Jodit.keys.BOTTOM)&&h&&b.node.hasParent(h,"td|th")&&i.cursorInEdge(a.which===Jodit.keys.TOP)){f=b.node.parentNode(h,"td|th"),d=q(f),e=p(),g=d[0];do if(g+=a.which===Jodit.keys.TOP?-1:1,g>=0&&g<e.length&&e[g][d[1]]!==f){i.moveCursorTo(e[g][d[1]]),a.preventDefault();break}while(g>0&&g<e.length)}}),b.events.on(c,"execCommand",function(a){switch(a){case"splitv":I();break;case"splitg":H();break;case"merge":E();break;case"empty":s().empty();break;case"bin":j.remove();break;case"binrow":B();break;case"bincolumn":A();break;case"addcolumnafter":case"addcolumnbefore":w("addcolumnbefore"===a);break;case"addrowafter":case"addrowbefore":y("addrowbefore"===a);break;case"bottom":case"middle":case"top":s().css("vertical-align",a)}b.syncCode()})),d={instances:[],addColumn:w,addRow:y,appendCell:x,appendRow:z,deleteColumn:A,deleteRow:B,nextCell:F,prevCell:G,splitGorizontal:H,splitVertical:I,mergeSelected:E,destroy:J}},Jodit.modules.TableProcessor.unique=1}(Jodit.modules.Dom),function(){function a(a,b){var c,d;for(c in b)d=b[c],void 0!==d&&(a[c]=d);return a}var b=function(){},c=function(c,d){var e;return e=d&&d.hasOwnProperty("constructor")?d.constructor:function(){return c.apply(this,arguments)},b.prototype=c.prototype,e.prototype=new b,d&&a(e.prototype,d),e.prototype.constructor=e,e.__super__=c.prototype,e},d={version:"0.1.15"};d.Stack=function(){this.commands=[],this.stackPosition=-1,this.savePosition=-1},a(d.Stack.prototype,{execute:function(a){this._clearRedo(),a.execute(),this.commands.push(a),this.stackPosition+=1,this.changed()},undo:function(){this.commands[this.stackPosition]&&this.commands[this.stackPosition].undo(),this.stackPosition-=1,this.changed()},canUndo:function(){return this.stackPosition>=0},redo:function(){this.stackPosition+=1,this.commands[this.stackPosition]&&this.commands[this.stackPosition].redo(),this.changed()},canRedo:function(){return this.stackPosition<this.commands.length-1},save:function(){this.savePosition=this.stackPosition,this.changed()},dirty:function(){return this.stackPosition!=this.savePosition},_clearRedo:function(){this.commands=this.commands.slice(0,this.stackPosition+1)},changed:function(){}}),d.Command=function(a){this.name=a};var e=new Error("override me!");
a(d.Command.prototype,{execute:function(){throw e},undo:function(){throw e},redo:function(){this.execute()}}),d.Command.extend=function(a){var b=c(this,a);return b.extend=d.Command.extend,b},"function"==typeof define&&define.amd?define(d):"undefined"!=typeof module&&module.exports?module.exports=d:this.Undo=d}.call(this),function(a){"use strict";Jodit.defaultOptions=a.extend(!0,Jodit.defaultOptions,{enableDragAndDropFileToEditor:!0,uploader:{url:"",format:"json",pathVariableName:"path",filesVariableName:"files",data:!1,prepareData:function(a){return a},isSuccess:function(a){return!a.error},getMsg:function(a){return void 0!==a.msg.join?a.msg.join(" "):a.msg},process:function(a){return{files:a[this.options.uploader.filesVariableName]||[],path:a.path,baseurl:a.baseurl,error:a.error,msg:a.msg}},error:function(a){this.events.fire("errorPopap",[a.getMessage(),"error",4e3])},defaultHandlerSuccess:function(a,b){var c,d=this.options.uploader.filesVariableName;if(a[d]&&a[d].length)for(c=0;c<a[d].length;c+=1)this.selection.insertImage(a.baseurl+a[d][c])},defaultHandlerError:function(a){this.events.fire("errorPopap",[this.options.uploader.getMsg(a)])}}}),Jodit.modules.Uploader=function(b,c){this.parent=b;var d="",e=c||b.options.uploader,f=function(b){if(void 0!==window.FormData){if(b instanceof FormData)return b;if("string"===a.type(b))return b;var c,d=new FormData;for(c in b)b.hasOwnProperty(c)&&d.append(c,b[c]);return d}return b},g=function(c,d){a.ajax({xhr:function(){var a=new window.XMLHttpRequest;return void 0!==window.FormData?a.upload.addEventListener("progress",function(a){if(a.lengthComputable){var c=a.loaded/a.total;c=parseInt(100*c,10),b.$progress_bar.show().css("width",c+"%"),100===c&&b.$progress_bar.hide()}},!1):b.$progress_bar.hide(),a},type:"POST",enctype:void 0!==window.FormData&&"string"!==a.type(c)?"multipart/form-data":"application/x-www-form-urlencoded",data:f(c),url:e.url,headers:e.headers,cache:!1,contentType:(void 0===window.FormData||"string"===a.type(c))&&"application/x-www-form-urlencoded; charset=UTF-8",processData:void 0===window.FormData||"string"===a.type(c),dataType:e.format||"json",error:a.proxy(e.error,b),success:d})},h=function(c,f,h,i){var j,k,l,m,n=c.length;if(!n)return!1;for(k=new FormData,k.append(e.pathVariableName,d),j=0;j<n;j+=1)l=c[j].type.match(/\/([a-z0-9]+)/i)[1].toLowerCase(),k.append(e.filesVariableName+"["+j+"]",c[j],c[j].name||Math.random().toString().replace(".","")+"."+l);if(e.prepareData(k),i&&i(k),e.data&&a.isPlainObject(e.data))for(m=Object.keys(e.data),j=0;j<m.length;j+=1)k.append(m[j],e.data[m[j]]);g(k,function(c){if(e.isSuccess.call(b,c))a.isFunction(f||e.defaultHandlerSuccess)&&(f||e.defaultHandlerSuccess).call(b,e.process.call(b,c),c);else if(a.isFunction(h||e.defaultHandlerError))return void(h||e.defaultHandlerError).call(b,c)})},i=function(a){d=a},j=function(a){var b,c=atob(a.split(",")[1]),d=a.split(",")[0].split(":")[1].split(";")[0],e=new ArrayBuffer(c.length),f=new Uint8Array(e);for(b=0;b<c.length;b+=1)f[b]=c.charCodeAt(b);return new Blob([f],{type:d})},k=function(c,d,f){c=a(c),c.on("paste",function(a){var c,e,g,i,k=function(a){a.append("extension",g),a.append("mimetype",e.type)};if(a.clipboardData&&a.clipboardData.files&&a.clipboardData.files.length)return h(a.clipboardData.files,d,f),!1;if(b.browser("ff"))return void(a.clipboardData.types.length||"text/plain"===a.clipboardData.types[0]||(i=b.getDocument().createElement("div"),b.selection.insertNode(i),i.focus(),setTimeout(function(){if(i.firstChild&&i.firstChild.hasAttribute("src")){var a=i.firstChild.getAttribute("src");i.parentNode.removeChild(i),h([j(a)],d,f)}},200)));if(a.clipboardData&&a.clipboardData.items&&a.clipboardData.items.length)for(c=0;c<a.clipboardData.items.length;c+=1)if("file"===a.clipboardData.items[c].kind&&"image/png"===a.clipboardData.items[c].type){e=a.clipboardData.items[c].getAsFile(),g=e.type.match(/\/([a-z0-9]+)/i)[1].toLowerCase(),h([e],d,f,k),a.preventDefault();break}}).on("dragover",function(b){a(this).addClass("draghover"),b.preventDefault()}).on("dragleave dragend",function(b){a(this).removeClass("draghover"),b.preventDefault()}).on("drop",function(g){if(a(this).removeClass("draghover"),g.originalEvent.dataTransfer&&g.originalEvent.dataTransfer.files&&g.originalEvent.dataTransfer.files.length)g.preventDefault(),h(g.originalEvent.dataTransfer.files,d,f);else if(g.originalEvent.dataTransfer&&g.originalEvent.dataTransfer.getData("text/plain")&&"-"!==g.originalEvent.dataTransfer.getData("text/plain")&&c.get(0)===b.editor){if(g.preventDefault(),g.stopPropagation(),b.selection.insertAtPoint(g.originalEvent.clientX,g.originalEvent.clientY)===!1)return!1;if(d||e.defaultHandlerSuccess){var i={};i.baseurl="",i[b.options.uploader.filesVariableName]=[g.originalEvent.dataTransfer.getData("text/plain")],(d||e.defaultHandlerSuccess).call(b,i)}g.preventDefault()}}),c.find("input").on("change",function(){h(this.files,d,f)})},l=function(c,d,f){g({action:"uploadremote",url:c},function(c){if(e.isSuccess.call(b,c))a.isFunction(d)&&d.call(b,c,e.getMsg(c));else if(a.isFunction(f||e.defaultHandlerError))return void(f||e.defaultHandlerError).call(b,c,e.getMsg(c))})},m=function(){b.editor&&b.options.enableDragAndDropFileToEditor&&b.options.uploader&&b.options.uploader.url&&k(b.$editor)};return{init:m,bind:k,setPath:i,uploadRemoteImage:l}}}(Jodit.modules.Dom),function(a){"use strict";Jodit.defaultOptions=a.extend(!0,Jodit.defaultOptions,{beautyHTML:!0}),Jodit.plugins.Beautifier=function(a){a.options.beautyHTML&&a.events.on("beforeSetElementValue",function(b){function c(a){return a=a.replace(/\n\s*\n/g,"\n"),a=a.replace(/^[\s\n]*/,""),a=a.replace(/[\s\n]*$/,""),h=0,a}function d(){var a,b="";for(a=0;a<h;a+=1)b+="\t";return b}function e(a,b){var c=a.match(j);return(a.match(n)||c)&&(b=b.replace(/\s*$/,""),b+="\n"),c&&"/"===a.charAt(1)&&(h-=1),"\n"===b.charAt(b.length-1)&&(b+=d()),c&&"/"!==a.charAt(1)&&(h+=1),b+=a,(a.match(o)||a.match(j))&&(b=b.replace(/ *$/,""),b+="\n"),b}function f(a){var b,c="",d="",e=/\s*([^= ]+)(?:=((['"']).*?\3|[^ ]+))?/;a=a.replace(/\n/g," "),a=a.replace(/\s*=\s*/g,"="),a=a.replace(/[\s]{2,}/g," "),a=a.replace(/^\s+|\s+$/g," "),a.match(/\/$/)&&(d="/",a=a.replace(/\/+$/,""));do b=e.exec(a),b&&(b[2]?c+=b[1].toLowerCase()+"="+b[2]:b[1]&&(c+=b[1].toLowerCase()),c+=" ",a=a.substr(b[0].length));while(b);return c.replace(/\s*$/,"")+d+">"}function g(b){function g(){var a,q;for(a=h;h<b.length&&h<a+i;h+=1){if(j=h,-1===b.substr(h).indexOf("<"))return o+=b.substr(h),void(o=c(o));for(;j<b.length&&"<"!==b.charAt(j);)j+=1;for(h!==j&&(-1===m.indexOf(n)?(p=b.substr(h,j-h),p.match(/^\s+$/)||("\n"===o.charAt(o.length-1)?o+=d():"\n"===p.charAt(0)&&(o+="\n"+d(),p=p.replace(/^\s+/,"")),p=p.replace(/\s+/g," "),o+=p),p.match(/\n/)&&(o+="\n"+d())):o+=b.substr(h,j-h)),k=j;j<b.length&&">"!==b.charAt(j);)j+=1;if(n=b.substr(k,j-k),h=j,"!--"===n.substr(1,3)){if(!n.match(/--$/)){for(;"-->"!==b.substr(j,3);)j+=1;j+=2,n=b.substr(k,j-k),h=j}"\n"!==o.charAt(o.length-1)&&(o+="\n"),o+=d(),o+=n+">\n"}else"!"===n[1]?o=e(n+">",o):"?"===n[1]?o+=n+">\n":(q=n.match(/^<(script|style)/i),q?(q[1]=q[1].toLowerCase(),n=f(n),o=e(n,o),l=String(b.substr(h+1)).toLowerCase().indexOf("</"+q[1]),l&&(p=b.substr(h+1,l),h+=l,o+=p)):(n=f(n),o=e(n,o)))}h<b.length?g():c(o)}var h=0,j=0,k=null,l=null,n="",o="",p="";return g(),a.helper.clear(o)}if(a.getRealMode()===Jodit.MODE_WYSIWYG){var h=0,i=100,j=["blockquote","div","dl","fieldset","form","frameset","map","ol","p","select","td","th","tr","ul"],k=["area","body","head","hr","i?frame","link","meta","noscript","style","table","tbody","thead","tfoot"],l=["li","dt","dt","h[1-6]","option","script"],m=["<pre>"],n=new RegExp("^<(/?"+k.join("|/?")+"|"+l.join("|")+")[ >]"),o=new RegExp("^<(br|/?"+k.join("|/?")+"|/"+l.join("|/")+")[ >]");j=new RegExp("^</?("+j.join("|")+")[ >]"),b.value=g(b.value)}})}}(Jodit.modules.Dom),function(a){"use strict";Jodit.defaultOptions=a.extend(!0,Jodit.defaultOptions,{showPopapErrors:!0,showPopapErrorTime:3e3,showPopapErrorOffsetPx:3}),Jodit.plugins.ErrorPopaps=function(b){if(b.options.showPopapErrors){var c,d=a('<div class="jodit_error_box_for_popaps"></div>'),e=function(){c=5,d.children().each(function(d,e){a(e).css("bottom",c+"px"),c+=a(e).outerHeight()+b.options.showPopapErrorOffsetPx})};b.$workflow.append(d),b.events.on("errorPopap",function(c,f,g){var h=a('<div class="active '+(f||"")+'">'+c+"</div>");d.append(h),e(),setTimeout(function(){h.removeClass("active"),setTimeout(function(){h.remove(),e()},300)},g||b.options.showPopapErrorTime)})}}}(Jodit.modules.Dom),function(a){"use strict";Jodit.defaultOptions=a.extend(!0,Jodit.defaultOptions,{fullsize:!1,globalFullsize:!0}),Jodit.plugins.fullsize=function(b){var c=!1,d=function(){c?b.$wysiwyg.css({height:window.innerHeight,width:window.innerWidth}):b.$wysiwyg.css({height:null,width:null})},e=function(e){if(void 0===e&&(e=!b.$wysiwyg.hasClass("jodit_fullsize")),c=e,b.$wysiwyg.toggleClass("jodit_fullsize",e),b.$toolbar&&b.$toolbar.length&&(b.$toolbar.css("width","auto"),b.$toolbar.find(".toolbar-fullsize a").html(b.icons.getSVGIcon(e?"shrink":"fullsize"))),b.options.globalFullsize){var f=b.$wysiwyg[0].parentNode;do a(f).toggleClass("jodit_fullsize_box",e),f=f.parentNode;while(f);d()}b.events.fire("afterResize")};b.options.fullsize&&e(!0),b.options.globalFullsize&&a(window).on("resize",d),b.events.on("toggleFullsize",e)}}(Jodit.modules.Dom),function(a){"use strict";Jodit.defaultOptions=a.extend(!0,Jodit.defaultOptions,{image:{openOnDblClick:!0,editSrc:!0,useImageEditor:!0,editTitle:!0,editAlt:!0,editLink:!0,editSize:!0,editMargins:!0,editClass:!0,editStyle:!0,editId:!0,editAlign:!0,showPreview:!0,selectimageAfterClose:!0}}),Jodit.plugins.image=function(b){var c=function(c){var d,e=this,f=new Jodit.modules.Dialog(b),g={remove:a('<a href="javascript:void(0)" class="jodit_button">'+b.icons.getSVGIcon("bin")+" "+b.i18n("Delete")+"</a>")},h=a('<form class="jodit_properties"><div class="jodit_row"><div class="jodit_col5"><div class="jodit_properties_view_box"><div style="'+(b.options.image.showPreview?"":"display:none")+'" class="jodit_properties_image_view"><img id="imageViewSrc" src="" alt=""/></div><div style="'+(b.options.image.editSize?"":"display:none")+'" class="jodit_form_group jodit_properties_image_sizes"><input type="number" id="imageWidth"/><a class="jodit_lock_helper jodit_lock_size" href="javascript:void(0)">'+b.icons.getSVGIcon("lock")+'</a><input type="number" id="imageHeight"/></div></div></div><div id="tabsbox" class="jodit_col7"></div></div></form>'),i=a('<div style="'+(b.options.image.editMargins?"":"display:none")+'" class="jodit_form_group"><label for="marginTop">'+b.i18n("Margins")+'</label><div class="jodit_input_group"><input class="margins" type="text" placeholder="'+b.i18n("top")+'" id="marginTop"/><a style="width: 26px;text-align: center;" class="jodit_lock_helper jodit_lock_margin" href="javascript:void(0)">'+b.icons.getSVGIcon("lock")+'</a><input disabled="true" style="margin-left:-1px" class="jodit_col3 margins" type="text" placeholder="'+b.i18n("right")+'" id="marginRight"/><input disabled="true" style="margin-left:-1px" class="jodit_col3 margins" type="text" placeholder="'+b.i18n("bottom")+'" id="marginBottom"/><input disabled="true" style="margin-left:-1px" class="jodit_col3 margins" type="text" placeholder="'+b.i18n("left")+'" id="marginLeft"/></div></div><div style="'+(b.options.image.editStyle?"":"display:none")+'" class="jodit_form_group"><label for="style">'+b.i18n("Styles")+'</label><input type="text" id="style"/></div><div style="'+(b.options.image.editClass?"":"display:none")+'" class="jodit_form_group"><label for="classes">'+b.i18n("Classes")+'</label><input type="text" id="classes"/></div><div style="'+(b.options.image.editId?"":"display:none")+'" class="jodit_form_group"><label for="id">Id</label><input type="text" id="id"/></div><div style="'+(b.options.image.editAlign?"":"display:none")+'" class="jodit_form_group"><label for="align">'+b.i18n("Align")+'</label><select class="select"  id="align"><option value="">'+b.i18n("--Not Set--")+'</option><option value="left">'+b.i18n("Left")+'</option><option value="center">'+b.i18n("Center")+'</option><option value="right">'+b.i18n("Right")+"</option></optgroup></select></div>"),j=a('<div style="'+(b.options.image.editSrc?"":"display:none")+'" class="jodit_form_group"><label for="imageSrc">'+b.i18n("Src")+'</label><div class="jodit_input_group"><input type="text" id="imageSrc"/>'+(b.options.filebrowser.ajax.url||b.options.uploader.url?'<div class="jodit_input_group-buttons">'+(b.options.filebrowser.ajax.url||b.options.uploader.url?'<a class="jodit_button jodit_rechange" href="javascript:void(0)">'+b.icons.getSVGIcon("image")+"</a>":"")+(b.options.image.useImageEditor&&void 0!==Jodit.modules.ImageEditor&&b.options.filebrowser.ajax.url?'<a class="jodit_button jodit_use_image_editor" href="javascript:void(0)">'+b.icons.getSVGIcon("crop")+"</a>":"")+"</div>":"")+'</div></div><div style="'+(b.options.image.editTitle?"":"display:none")+'" class="jodit_form_group"><label for="imageTitle">'+b.i18n("Title")+'</label><input type="text" id="imageTitle"/></div><div style="'+(b.options.image.editAlt?"":"display:none")+'" class="jodit_form_group"><label for="imageAlt">'+b.i18n("Alternative")+'</label><input type="text" id="imageAlt"/></div><div style="'+(b.options.image.editLink?"":"display:none")+'" class="jodit_form_group"><label for="imageLink">'+b.i18n("Link")+'</label><input type="text" id="imageLink"/></div><div style="'+(b.options.image.editLink?"":"display:none")+'" class="jodit_form_group"><input type="checkbox" id="imageLinkOpenInNewTab"/> '+b.i18n("Open link in new tab")+"</div>"),k=e.naturalWidth/e.naturalHeight||1,l=0,m=j.find(".jodit_rechange"),n=h.find("#imageWidth"),o=h.find("#imageHeight"),p=function(){e.style.float&&["left","right"].indexOf(e.style.float.toLowerCase())!==-1?h.find("#align").val(a(e).css("float")):"block"===a(e).css("display")&&"auto"===e.style.marginLeft&&"auto"===e.style.marginRight&&h.find("#align").val("center")},q=function(){h.find("#id").val(e.getAttribute("id"))},r=function(){h.find("#style").val(e.getAttribute("style"))},s=function(){h.find("#classes").val((e.getAttribute("class")||"").replace(/jodit_focused_image[\s]*/,""))},t=function(){if(b.options.image.editMargins){var c=!1;h.find(".margins").each(function(){var d=e.style[this.id];d&&((/^[0-9]+$/.test(d)||/^[0-9]+px$/i.test(d))&&(d=parseInt(d,10)),a(this).val(d||""),c||"marginTop"===this.id||a(this).val()===h.find("#marginTop").val()||(h.find(".jodit_lock_margin").html(b.icons.getSVGIcon("unlock")),h.find(".margins").removeAttr("disabled"),c=!0))})}},u=function(){n.val(a(e).outerWidth()),o.val(a(e).outerHeight())},v=function(a){e.hasAttribute("title")&&h.find("#imageTitle").val(e.getAttribute("title")),e.hasAttribute("alt")&&h.find("#imageAlt").val(e.getAttribute("alt")),b.node.hasParent(e,"a")&&(h.find("#imageLink").val(b.node.parentNode(e,"a").getAttribute("href")),h.find("#imageLinkOpenInNewTab").get(0).checked="_blank"===b.node.parentNode(e,"a").getAttribute("target"))},w=function(){h.find("#imageSrc").val(e.getAttribute("src")),h.find("#imageViewSrc").attr("src",e.getAttribute("src"))},x=function(){w(),v(),u(),t(),s(),q(),p(),r()};return h.find("#tabsbox").html(b.form.buildTabs(function(){var a={};return a[b.i18n("Image")]=j,a[b.i18n("Advansed")]=i,a}())),x(),b.events.on(f,"afterClose",function(){b.closeToolbarPopap(),f.destroy(),e.parentNode&&b.options.image.selectimageAfterClose&&b.selection.selectImage(e)}),g.remove.on("click",function(){e.parentNode.removeChild(e),f.close()}),j.find(".jodit_use_image_editor").on("mousedown",function(a){if(b.options.image.useImageEditor&&void 0!==Jodit.modules.ImageEditor){var c=e.getAttribute("src"),d=document.createElement("a");if(d.href=c,d.host!==location.host)return void Jodit.Confirm(b.i18n("You can only edit your own images. Download this image on the host?"),function(a){a&&b.uploader&&b.uploader.uploadRemoteImage(d.href.toString(),function(a){Jodit.Alert(b.i18n("The image has been successfully uploaded to the host!"),function(a){"string"==typeof a.newpath&&(e.setAttribute("src",a.baseurl+a.newpath),w())})},function(a,c){Jodit.Alert(b.i18n("There was an error loading:"+c))})});b.filebrowser.getPathByUrl(d.href,function(a,f){b.filebrowser.openImageEditor(d.href,f,a,function(){var a=(new Date).getTime();e.setAttribute("src",c+(c.indexOf("?")!==-1?"":"?")+"&_tmp="+a),w()},function(a){Jodit.Alert(a)})},function(a){Jodit.Alert(a)})}}),m.on("mousedown",function(c){m.toggleClass("active"),b.$toolbar_popap.show(),m.append(b.$toolbar_popap),b.$toolbar_popap.addClass("jodit_right"),b.$toolbar_popap.empty(),b.$toolbar_popap.html(b.form.imageSelector({upload:function(a){var c=this.options.uploader.filesVariableName;a[c]&&a[c].length&&e.setAttribute("src",a.baseurl+a[c][0]),x(),b.closeToolbarPopap()},filebrowser:function(c){c&&c.files&&a.isArray(c.files)&&c.files.length&&(e.setAttribute("src",c.files[0]),b.closeToolbarPopap(),x())}},e)),c.stopPropagation()}),h.find(".jodit_lock_helper.jodit_lock_size").on("click",function(){a(this).html(b.icons.getSVGIcon(a(this).children().hasClass("jodit_icon_unlock")?"lock":"unlock")),n.trigger("change")}),h.find(".jodit_lock_helper.jodit_lock_margin").on("click",function(){a(this).html(b.icons.getSVGIcon(a(this).children().hasClass("jodit_icon_unlock")?"lock":"unlock")),a(this).children().hasClass("jodit_icon_unlock")?h.find(".margins").not("#marginTop").removeAttr("disabled"):h.find(".margins").not("#marginTop").attr("disabled",!0)}),h.find("#imageWidth,#imageHeight").on("change keydown mousedown paste",function(a){h.find(".jodit_lock_size").children().hasClass("jodit_icon_unlock")||(clearTimeout(l),l=setTimeout(function(){var b=parseInt(n.val(),10),c=parseInt(o.val(),10);"imageWidth"===a.target.id?o.val(Math.round(b/k)):n.val(Math.round(c*k))},100))}),f.setTitle([b.i18n("Image properties"),g.remove]),f.setContent(h),d=a('<a href="javascript:void(0)" style="float:right;" class="jodit_button">'+b.icons.getSVGIcon("cancel")+"&nbsp;"+b.i18n("Cancel")+"</a>").on("click",function(){f.close()}),f.setFooter([a('<a href="javascript:void(0)" style="float:left;" class="jodit_button">'+b.icons.getSVGIcon("check")+"&nbsp;"+b.i18n("Ok")+"</a>").on("click",function(){function c(a){return a=b.helper.trim(a),/^[0-9]+$/.test(a)?a+"px":a}function d(){"block"===a(e).css("display")&&a(e).css("display",""),"auto"===e.style.marginLeft&&"auto"===e.style.marginRight&&(e.style.marginLeft="",e.style.marginRight="")}var g;return b.options.image.editStyle&&(h.find("#style").val()?e.setAttribute("style",h.find("#style").val()):e.removeAttribute("style")),h.find("#imageSrc").val()?(e.setAttribute("src",h.find("#imageSrc").val()),h.find("#imageTitle").val()?e.setAttribute("title",h.find("#imageTitle").val()):e.removeAttribute("title"),h.find("#imageAlt").val()?e.setAttribute("alt",h.find("#imageAlt").val()):e.removeAttribute("alt"),h.find("#imageLink").val()?(b.node.hasParent(e,"a")||b.node.wrap(e,"a"),g=b.node.parentNode(e,"a"),g.setAttribute("href",h.find("#imageLink").val()),h.find("#imageLinkOpenInNewTab").get(0).checked?g.setAttribute("target","_blank"):g.removeAttribute("target")):b.node.hasParent(e,"a")&&(g=b.node.parentNode(e,"a"),g.parentNode.replaceChild(e,g)),a(e).css({width:b.helper.trim(n.val())?c(n.val()):null,height:b.helper.trim(o.val())?c(o.val()):null}),b.options.image.editMargins&&(h.find(".jodit_lock_margin").children().hasClass("jodit_icon_unlock")?h.find(".margins").each(function(){a(e).css(this.id,c(a(this).val()))}):a(e).css("margin",c(h.find("#marginTop").val()))),b.options.image.editClass&&(h.find("#classes").val()?e.setAttribute("class",h.find("#classes").val()):e.removeAttribute("class")),b.options.image.editId&&(h.find("#id").val()?e.setAttribute("id",h.find("#id").val()):e.removeAttribute("id")),b.options.image.editAlign&&(h.find("#align").val()?["right","left"].indexOf(h.find("#align").val().toLowerCase())!==-1?(a(e).css("float",h.find("#align").val()),d()):a(e).css("float","").css({display:"block","margin-left":"auto","margin-right":"auto"}):(a(e).css("float")&&["right","left"].indexOf(a(e).css("float").toLowerCase())!==-1&&a(e).css("float",""),d())),e.getAttribute("style")||e.removeAttribute("style"),void f.close()):(e.parentNode.removeChild(e),void f.close())}),d]),f.setSize(500),f.open(),c&&c.preventDefault(),!1},d=function(a){a.originalEvent.dataTransfer.dropEffect="move",a.originalEvent.dataTransfer.setData("text/plain",""),a.preventDefault()};b.options.image.openOnDblClick?b.$editor.on("dblclick","img",c):b.$editor.on("dblclick","img",function(a){b.selection.selectImage(this)}),b.$editor.on("dragenter",d).on("dragover",function(a){b.browser("mse")&&d(a)}).on("drop",function(c){var d,e,f=b.$editor.find("img.jodit_focused_image").get(0);if(f){if(c.preventDefault(),c.stopPropagation(),b.selection.insertAtPoint(c.originalEvent.clientX,c.originalEvent.clientY)===!1)return!1;e="A"!==f.parentNode.tagName||f.parentNode.textContent.length?f:f.parentNode,d=b.$editor.find(".jodit_marker"),d.length&&b.node.replace(d.get(0),e,!1,!0),a(f).removeClass("jodit_focused_image"),b.selection.unmark(),b.events.fire("hidePopap")}}),this.open=c}}(Jodit.modules.Dom),function(a){"use strict";Jodit.defaultOptions=a.extend(!0,Jodit.defaultOptions,{cleanHTML:{replaceNBSP:!0,cleanOnPaste:!0,allowTags:!1}}),Jodit.plugins.cleanHTML=function(b){if(b.options.cleanHTML.cleanOnPaste&&b.events.on("processPaste",function(a,c){return b.helper.cleanFromWord(c)}),b.options.cleanHTML.allowTags){var c=/([^\[]*)\[([^\]]+)\]/,d=/[\s]*,[\s]*/,e=/^(.*)[\s]*=[\s]*(.*)$/,f={};"string"==typeof b.options.cleanHTML.allowTags?b.options.cleanHTML.allowTags.split(d).map(function(a){a=b.helper.trim(a);var g=c.exec(a),h={},i=function(a){a=b.helper.trim(a);var c=e.exec(a);c?h[c[1]]=c[2]:h[a]=!0};g?(g[2]=g[2].split(d),g[1]&&(g[2].map(i),f[g[1].toUpperCase()]=h)):f[a.toUpperCase()]=!0}):f=b.options.cleanHTML.allowTags,b.events.on("beforeSetElementValue",function(a){if(b.getRealMode()===Jodit.MODE_WYSIWYG){var c,d,e,g=b.node.create("div"),h=[];for(g.innerHTML=a.value,c=g.firstChild;c;){if(c.tagName){if(!f[c.tagName]){h.push(c),c=b.node.find(c,g,"firstChild","nextSibling",!0);continue}if(f[c.tagName]!==!0&&c.attributes&&c.attributes.length){for(d=[],e=0;e<c.attributes.length;e+=1)(!f[c.tagName][c.attributes[e].name]||f[c.tagName][c.attributes[e].name]!==!0&&f[c.tagName][c.attributes[e].name]!==c.attributes[e].value)&&d.push(c.attributes[e].name);for(e=0;e<=d.length;e+=1)c.removeAttribute(d[e])}}c=b.node.find(c,g,"firstChild","nextSibling")}for(e=0;e<h.length;e+=1)h[e].parentNode&&h[e].parentNode.removeChild(h[e]);a.value=g.innerHTML}})}b.events.on("afterCommand",function(c){var d,e,f,g=b.selection;switch(c){case"insertHorizontalRule":d=b.$editor.find("hr[id=null]").get(0),d&&(e=b.node.find(d,b.editor,"firstChild","nextSibling",!1,b.node.isBlock),e||(e=b.enterkey.createNewBlock(b.options.enter),a(d).after(e)),g.moveCursorTo(e));break;case"removeFormat":if(e=g.current(),f=function(a){return a.nodeType===Node.ELEMENT_NODE?(a.hasAttribute("style")&&a.removeAttribute("style"),"FONT"===a.tagName&&(b.node.each(a,f),a=g.replace(a,"span")),a.normalize&&a.normalize()):b.options.cleanHTML.replaceNBSP&&a.nodeType===Node.TEXT_NODE&&a.nodeValue.match(Jodit.NBSP_REG_EXP)&&(a.nodeValue=a.nodeValue.replace(Jodit.NBSP_REG_EXP," ")),a},b.observer&&b.observer.block(!0),g.isCollapsed())for(;e&&e.nodeType!==Node.ELEMENT_NODE&&e!==b.editor;)e=f(e),e=e.parentNode;else g.each(f);b.observer&&b.observer.block(!1)}})}}(Jodit.modules.Dom),function(a){"use strict";Jodit.defaultOptions=a.extend(!0,Jodit.defaultOptions,{codeMirror:!0,codeMirrorOptions:{autofocus:!1,lineNumbers:!0,tabMode:"indent",indentWithTabs:!0,lineWrapping:!0,mode:"text/html",tabSize:2},codeMirrorUrlJS:"//cdnjs.cloudflare.com/ajax/libs/codemirror/5.17.0/codemirror.min.js",codeMirrorUrlCSS:"//cdnjs.cloudflare.com/ajax/libs/codemirror/5.17.0/codemirror.min.css",codeMirrorXMLUrl:"//cdnjs.cloudflare.com/ajax/libs/codemirror/5.17.0/mode/xml/xml.min.js"}),Jodit.plugins.codeMirror=function(b){if(b.options.codeMirror){var c,d=!1,e="jodit_codemirror",f=function(a){return"file:"===window.location.protocol&&/^\/\//.test(a)&&(a="https:"+a),a},g=function(){!d&&c&&b.getRealMode()===Jodit.MODE_TEXTAREA&&(b.val(c.getValue()),b.$area.trigger("updatestack"))},h=function(a,b){var c=document.createElement("script");c.className=e,c.type="text/javascript",c.src=f(a),void 0!==b&&(c.onload=b),document.body.appendChild(c)};b.events.on("afterSetAreaValue updateStackEditor",function(){if(("updateStackEditor"===b.events.current()||b.getMode()===Jodit.MODE_SPLIT&&b.getRealMode()===Jodit.MODE_WYSIWYG)&&c){var a=c.getValue(),e=b.getAreaValue();a!==e&&(d=!0,c.setValue(e),d=!1)}}),b.events.on("beforeSetMode afterResize",g),b.events.on("focus",function(){c&&c.focus()}),b.events.on("afterSetMode afterResize",function(){if(-1!==a.inArray(b.getMode(),[Jodit.MODE_TEXTAREA,Jodit.MODE_SPLIT])){var d=function(){void 0===c&&(void 0!==window.CodeMirror&&void 0!==window.CodeMirror.modes&&void 0!==window.CodeMirror.modes.xml?(c=window.CodeMirror.fromTextArea(b.area,b.options.codeMirrorOptions),c.on("change",g)):setTimeout(d,300)),c&&(c.setValue(b.getElementValue()),b.options.autofocus&&c.focus(),c.refresh(),c.clearHistory())};d()}}),void 0!==window.CodeMirror||a("script."+e).length||(h(b.options.codeMirrorUrlJS,function(){h(b.options.codeMirrorXMLUrl)}),a("body").append('<link rel="stylesheet" href="'+f(b.options.codeMirrorUrlCSS)+'">'))}}}(Jodit.modules.Dom),function(a){"use strict";Jodit.defaultOptions=a.extend(!0,Jodit.defaultOptions,{askBeforePasteHTML:!0}),Jodit.plugins.insertHTML=function(a){a.options.askBeforePasteHTML&&a.events.on("beforePaste",function(b){if(b&&b.clipboardData&&b.clipboardData.getData&&"text/plain"===b.clipboardData.types[0]){var c=b.clipboardData.getData("text/plain");if(a.helper.isHTML(c))return Jodit.Confirm(a.i18n("Your code is similar to HTML. Paste as HTML?"),a.i18n("Paste as HTML"),function(b){b?a.selection.insertHTML(c):a.selection.insertHTML(a.helper.htmlspecialchars(c)),a.syncCode(!0)}),!1}})}}(Jodit.modules.Dom),function(a){"use strict";Jodit.defaultOptions=a.extend(!0,Jodit.defaultOptions,{link:{processVideoLink:!0,processPastedLink:!0,openLinkDialogAfterPost:!0,removeLinkAfterFormat:!0}}),Jodit.plugins.link=function(a){a.options.link.processPastedLink&&a.events.on("processPaste",function(b,c){if(a.helper.isURL(c)){var d;return a.helper.convertMediaURLToVideoEmbed(c)!==c?d=a.helper.convertMediaURLToVideoEmbed(c):(d=a.getDocument().createElement("a"),d.setAttribute("href",c),d.innerText=c,a.options.link.openLinkDialogAfterPost&&setTimeout(function(){a.selection.selectNodes(Array.prototype.slice.call(d.childNodes))},100)),d}}),a.options.link.removeLinkAfterFormat&&a.events.on("afterCommand",function(b){var c,d,e=a.selection;"removeFormat"===b&&(d=e.current(),d&&"A"!==d.tagName&&(d=a.node.parentNode(d,"A")),d&&"A"===d.tagName&&(c=d.innerHTML===d.innerText?a.node.create("text",d.innerText):a.node.create("span",d.innerHTML),d.parentNode.replaceChild(c,d),a.selection.moveCursorTo(c,!0)))})}}(Jodit.modules.Dom),function(a){"use strict";Jodit.defaultOptions=a.extend(!0,Jodit.defaultOptions,{cleanWhitespace:!0,wrapSingleTextNodes:!0,removeEmptyNodes:!0}),Jodit.plugins.textProcessor=function(b){(b.options.wrapSingleTextNodes||b.options.removeEmptyNodes||b.options.cleanWhitespace)&&b.events.on("changeElementValue afterInit",function(){if(b.getRealMode()===Jodit.MODE_WYSIWYG){var c,d,e="",f="",g=[],h=0;if(b.observer.block(1),b.selection.restore(),b.selection.save(),b.options.wrapSingleTextNodes||b.options.removeEmptyNodes){for(c=[].slice.call(b.editor.childNodes),h=0;c&&h<c.length;h+=1)if(b.node.isBlock(c[h])&&""===c[h].textContent.length&&b.options.removeEmptyNodes)c[h].parentNode.removeChild(c[h]);else if(b.options.wrapSingleTextNodes)if(c[h].nodeType===Node.ELEMENT_NODE&&"img"===c[h].nodeName.toLowerCase())b.node.wrap(c[h],b.enterkey.createNewBlock(b.options.enter,!1,""));else if(c[h].nodeType!==Node.TEXT_NODE||0===b.helper.trim(c[h].nodeValue).length&&!f)"SPAN"===c[h].tagName&&"jodit_marker"===c[h].className||"BR"===c[h].tagName?(g.push(c[h]),f+=c[h].outerHTML):f.length?(b.node.replace(g[0],b.enterkey.createNewBlock(b.options.enter,!1,f),!1,!0),a(g).remove(),f="",g=[]):Jodit.INVISIBLE_SPACE_REG_EXP.test(c[h].nodeValue)&&c[h].parentNode.removeChild(c[h]);else for(f+=b.helper.htmlspecialchars(c[h].nodeValue),g.push(c[h]),d=c[h].nextSibling;d&&"SPAN"===d.tagName&&"jodit_marker"===d.className;)g.push(d),f+=a(d).prop("outerHTML"),d=d.nextSibling,h+=1;f.length&&(b.node.replace(g[0],b.enterkey.createNewBlock(b.options.enter,!1,f),!1,!0),a(g).remove())}if(b.options.cleanWhitespace)for(d=b.editor.firstChild;d;)d.nodeType===Node.TEXT_NODE&&"jodit_marker"!==d.parentNode.className&&(e=d.nodeValue,f=e.replace(Jodit.INVISIBLE_SPACE_REG_EXP,""),f!==e&&f.length&&(d.nodeValue=f)),d=b.node.find(d,b.editor,"firstChild","nextSibling",!1);b.selection.restore(),b.observer.block(0),b.syncCode()}})}}(Jodit.modules.Dom),Jodit.lang.de={"Type something":"Schreibe etwas","About Jodit":"Über Jodi","Jodit Editor":"Jodit Editor","Free Non-commercial Version":"Freie Nicht-kommerzielle Version","Jodit User's Guide":"Jodit Benutzerhandbuch","contains detailed help for using":"ausführliche Hilfe enthält für die Verwendung von","For information about the license, please go to our website:":"Für Informationen über die Lizenz, gehen Sie bitte auf unsere Webseite:","Buy full version":"Vollversion kaufen","Copyright © XDSoft.net - Chupurnov Valeriy. All rights reserved.":"Copyright © XDSoft.net - Chupurnov Valeriy. Alle Rechte vorbehalten.",Anchor:"Anker","Open in new tab":"In neuer Registerkarte öffnen","Open editor in fullsize":"Editor öffnen in voller Größe","Clear Formatting":"Formatierung löschen","Fill color or set the text color":"Füllen Sie Farbe oder stellen Sie die Textfarbe",Redo:"Redo",Undo:"Rückgängig machen",Bold:"Fett gedruckt",Italic:"Kursiv","Insert Unordered List":"Ungeordnete Liste einfügen","Insert Ordered List":"Legen Sie Sortierte Liste","Justify Center":"einmitten","Justify Full":"Begründen Voll","Justify Left":"Begründen links","Justify Right":"Begründen Rechts","Insert Horizontal Line":"Legen Horizontale Linie","Insert Image":"Bild einfügen","Insert link":"Link einfügen","Font size":"Schriftgröße","Font family":"Schriftfamilie","Insert format block":"Insert Format Block",Normal:"Normal","Heading 1":"Überschrift 1","Heading 2":"Überschrift 2","Heading 3":"Überschrift 3","Heading 4":"Überschrift 4",Quote:"Zitat",Code:"Code",Insert:"Einfügen","Insert table":"Tabelle einfügen","Change mode":"Änderungsmodus",Margins:"Die Margen",top:"oben",right:"Recht",bottom:"Boden",left:"links",Styles:"Styles",Classes:"Klassen",Align:"Ausrichten",Right:"Recht",Center:"Center",Left:"Links","--Not Set--":"--Nicht eingestellt--",Src:"src",Title:"Titel",Alternative:"Alternative (alt)",Link:"Link","Open link in new tab":"Link in neuem Tab öffnen",Image:"Image",Advansed:"Erweiterte","Image properties":"Bildeigenschaften",Cancel:"Stornieren",Ok:"OK","File Browser":"Dateibrowser","Error on load list":"Fehler beim Ladeliste","Error on load folders":"Fehler beim Laden Ordner","Are you shure?":"Bist du sicher?","Enter Directory name":"Geben Sie Verzeichnisname","Create directory":"Verzeichnis erstellen","type name":"Modellname","Drop image":"Bild Tropfen","or click":"oder klicken Sie auf","Alternative text":"Alternativer Text",Browse:"Blättern",Upload:"Hochladen",Background:"Hintergrund",Text:"Text",Top:"Oben",Middle:"Mitte",Bottom:"Boden","Insert column before":"Spalte einfügen vor","Insert column after":"Spalte einfügen nach","Insert row above":"Zeile einfügen oben","Insert row below":"Zeile unterhalb einfügen","Delete table":"Tabelle löschen","Delete row":"Zeile löschen","Delete column":"Spalte löschen","Empty cell":"Leere Zelle"
},Jodit.lang.en_example={"Type something":"Start writing...","About Jodit":"","Jodit Editor":"","Free Non-commercial Version":"","Jodit User's Guide":"","contains detailed help for using":"","For information about the license, please go to our website:":"","Buy full version":"","Copyright � XDSoft.net - Chupurnov Valeriy. All rights reserved.":"",Anchor:"","Open editor in fullsize":"","Clear Formatting":"","Fill color or set the text color":"",Redo:"",Undo:"",Bold:"",Italic:"","Insert Unordered List":"","Insert Ordered List":"","Justify Center":"","Justify Full":"","Justify Left":"","Justify Right":"","Insert Horizontal Line":"","Insert Image":"","Insert link":"","Font size":"","Font family":"","Insert format block":"",Normal:"","Heading 1":"","Heading 2":"","Heading 3":"","Heading 4":"",Quote:"",Code:"","Insert table":"","Change mode":"",Margins:"",top:"",right:"",bottom:"",left:"",Styles:"",Classes:"",Align:"",Right:"",Center:"",Left:"","--Not Set--":"",Src:"",Title:"",Alternative:"",Link:"","Open link in new tab":"",Image:"",Advansed:"","Image properties":"",Cancel:"",Ok:"","File Browser":"","Error on load list":"","Error on load folders":"","Are you shure?":"","Enter Directory name":"","Create directory":"","type name":"","Drop image":"","or click":"","Alternative text":"",Browse:"",Background:"",Text:"",Top:"",Middle:"",Bottom:"","Insert column before":"","Insert column after":"","Insert row above":"","Insert row below":"","Delete table":"","Delete row":"","Delete column":"","Empty cell":""},Jodit.lang.ru={"Type something":"Напишите что-либо","About Jodit":"О Jodit","Jodit Editor":"Редактор Jodit","Free Non-commercial Version":"Версия для не коммерческого использования","Jodit User's Guide":"Jodit Руководство пользователя","contains detailed help for using":"содержит детальную информацию по использованию","For information about the license, please go to our website:":"Для получения сведений о лицензии , пожалуйста, перейдите на наш сайт:","Buy full version":"Купить полную версию","Copyright © XDSoft.net - Chupurnov Valeriy. All rights reserved.":"Авторские права © XDSoft.net - Чупурнов Валерий. Все права защищены.",Anchor:"Анкор","Open in new tab":"Открывать ссылку в новой вкладке","Open editor in fullsize":"Открыть редактор в полном размере","Clear Formatting":"Очистить форматирование","Fill color or set the text color":"Цвет заливки или цвет текста",Redo:"Повтор",Undo:"Отмена",Bold:"Жирный",Italic:"Наклонный","Insert Unordered List":"Вставка маркированного списка","Insert Ordered List":"Вставить нумерованный список","Justify Center":"Выровнять по центру","Justify Full":"Выровнять по ширине","Justify Left":"Выровнять по левому краю","Justify Right":"Выровнять по правому краю","Insert Horizontal Line":"Вставить горизонтальную линию","Insert Image":"Вставить изображение","Insert link":"Вставить ссылку","Font size":"Размер шрифта","Font family":"Шрифт","Insert format block":"Вставить блочный элемент",Normal:"Нормальный текст","Heading 1":"Заголовок 1","Heading 2":"Заголовок 2","Heading 3":"Заголовок 3","Heading 4":"Заголовок 4",Quote:"Цитата",Code:"Код",Insert:"Вставить","Insert table":"Вставить таблицу","Change mode":"Источник",Margins:"Отступы",top:"сверху",right:"справа",bottom:"снизу",left:"слева",Styles:"Стили",Classes:"Классы",Align:"Выравнивание",Right:"По правому краю",Center:"По центру",Left:"По левому краю","--Not Set--":"--не устанавливать--",Src:"src",Title:"Заголовок",Alternative:"Альтернативный текст (alt)",Link:"Ссылка","Open link in new tab":"Открывать ссылку в новом окне",Image:"Изображение",Advansed:"Расширенные","Image properties":"Свойства изображения",Cancel:"Отмена",Ok:"Ok","Your code is similar to HTML. Paste as HTML?":"Ваш текст, который вы пытаетесь вставить похож на HTML. Вставить его как HTML?","Paste as HTML":"Вставить как HTML?","File Browser":"Браузер файлов","Error on load list":"Ошибка при загрузке списка изображений","Error on load folders":"Ошибка при загрузке списка директорий","Are you shure?":"Вы уверены?","Enter Directory name":"Введите название директории","Create directory":"Создать директорию","type name":"введите название","Drop image":"Перетащите сюда изображение","or click":"или нажмите","Alternative text":"Альтернативный текст",Browse:"Сервер",Upload:"Загрузка",Background:"Фон",Text:"Текст",Top:" К верху",Middle:"По середине",Bottom:"К низу","Insert column before":"Вставить столбец до","Insert column after":"Вставить столбец после","Insert row above":"Вставить ряд выше","Insert row below":"Вставить ряд ниже","Delete table":"Удалить таблицу","Delete row":"Удалять ряд","Delete column":"Удалить столбец","Empty cell":"Отчистить ячейку"};
Jodit.lang.de = {
    'Type something': 'Schreibe etwas',
    // About
    'About Jodit': 'Über Jodit',
    'Jodit Editor': 'Jodit Editor',
    'Free Non-commercial Version': 'Freie Nicht-kommerzielle Version',
    'Jodit User\'s Guide': 'Jodit Benutzerhandbuch',
    'contains detailed help for using': 'enthält eine ausführliche Hilfe zur Verwendung des Editors',
    'For information about the license, please go to our website:': 'Für Informationen über die Lizenz, gehen Sie bitte auf unsere Webseite:',
    'Buy full version': 'Vollversion kaufen',
    'Copyright © XDSoft.net - Chupurnov Valeriy. All rights reserved.': 'Copyright © XDSoft.net - Chupurnov Valeriy. Alle Rechte vorbehalten.',

    // Toolbar
    'Anchor': 'Anker',
    'Open in new tab': 'In neuer Registerkarte öffnen',
    'Open editor in fullsize': 'Editor öffnen in voller Größe',
    'Clear Formatting': 'Formatierung löschen',
    'Fill color or set the text color': 'Hintergrund- oder Textfarbe ändern',
    'Redo': 'Wiederholen',
    'Undo': 'Rückgängig',
    'Bold': 'Fett',
    'Italic': 'Kursiv',
    'Insert Unordered List': 'Ungeordnete Liste',
    'Insert Ordered List': 'Sortierte Liste',
    'Justify Center': 'zentrieren',
    'Justify Full': 'Blocksatz',
    'Justify Left': 'linksbündig',
    'Justify Right': 'rechtsbündig',
    'Insert Horizontal Line': 'Horizontale Linie',
    'Insert Image': 'Bild',
    'Insert link': 'Link',
    'Font size': 'Schriftgröße',
    'Font family': 'Schriftart',

    'Insert format block': 'Vordefinierte Formatierung',
    'Normal': 'Normal',
    'Heading 1': 'Überschrift 1',
    'Heading 2': 'Überschrift 2',
    'Heading 3': 'Überschrift 3',
    'Heading 4': 'Überschrift 4',
    'Quote': 'Zitat',
    'Code': 'Code',

    'Insert': 'Einfügen',
    'Insert table': 'Tabelle einfügen',
    'Change mode': 'Modus ändern',

    //plugin Image
    'Margins': 'Außenabstand',
    'top': 'oben',
    'right': 'rechts',
    'bottom': 'unten',
    'left': 'links',
    'Styles': 'Stile',
    'Classes': 'Klassen',
    'Align': 'Ausrichten',
    'Right': 'Rechts',
    'Center': 'Zentriert',
    'Left': 'Links',
    '--Not Set--': '--Nicht definiert--',
    'Src': 'src',
    'Title': 'Titel',
    'Alternative': 'Alternativtext (alt)',
    'Link': 'Link',
    'Open link in new tab': 'Link in neuem Tab öffnen',
    'Image': 'Bild',
    'Advansed': 'Erweitert',
    'Image properties': 'Bildeigenschaften',
    'Cancel': 'Abbrechen',
    'Ok': 'OK',

    // File Browser module
    'File Browser': 'Dateibrowser',
    'Error on load list': 'Fehler beim Laden der Liste',
    'Error on load folders': 'Fehler beim Laden des Ordners',
    'Are you shure?': 'Bist du sicher?',
    'Enter Directory name': 'Geben Sie einen Verzeichnisnamen an',
    'Create directory': 'Verzeichnis erstellen',
    'type name': 'Modellname',

    // Form module
    'Drop image': 'Bild löschen',
    'or click': 'oder klicken Sie auf',
    'Alternative text': 'Alternativer Text',
    'Browse': 'Durchsuchen',
    'Upload': 'Hochladen',

    'Background': 'Hintergrund',
    'Text': 'Text',

    // popap module
    'Top': 'Oben',
    'Middle': 'Mitte',
    'Bottom': 'Unten',
    'Insert column before': 'Spalte einfügen vor',
    'Insert column after': 'Spalte einfügen nach',
    'Insert row above': 'Zeile einfügen oben',
    'Insert row below': 'Zeile unterhalb einfügen',
    'Delete table': 'Tabelle löschen',
    'Delete row': 'Zeile löschen',
    'Delete column': 'Spalte löschen',
    'Empty cell': 'Leere Zelle'
};
!function(e){e(["jquery"],function(e){return function(){function t(e,t,n){return g({type:O.error,iconClass:m().iconClasses.error,message:e,optionsOverride:n,title:t})}function n(t,n){return t||(t=m()),v=e("#"+t.containerId),v.length?v:(n&&(v=d(t)),v)}function o(e,t,n){return g({type:O.info,iconClass:m().iconClasses.info,message:e,optionsOverride:n,title:t})}function s(e){C=e}function i(e,t,n){return g({type:O.success,iconClass:m().iconClasses.success,message:e,optionsOverride:n,title:t})}function a(e,t,n){return g({type:O.warning,iconClass:m().iconClasses.warning,message:e,optionsOverride:n,title:t})}function r(e,t){var o=m();v||n(o),u(e,o,t)||l(o)}function c(t){var o=m();return v||n(o),t&&0===e(":focus",t).length?void h(t):void(v.children().length&&v.remove())}function l(t){for(var n=v.children(),o=n.length-1;o>=0;o--)u(e(n[o]),t)}function u(t,n,o){var s=!(!o||!o.force)&&o.force;return!(!t||!s&&0!==e(":focus",t).length)&&(t[n.hideMethod]({duration:n.hideDuration,easing:n.hideEasing,complete:function(){h(t)}}),!0)}function d(t){return v=e("<div/>").attr("id",t.containerId).addClass(t.positionClass),v.appendTo(e(t.target)),v}function p(){return{tapToDismiss:!0,toastClass:"toast",containerId:"toast-container",debug:!1,showMethod:"fadeIn",showDuration:300,showEasing:"swing",onShown:void 0,hideMethod:"fadeOut",hideDuration:1e3,hideEasing:"swing",onHidden:void 0,closeMethod:!1,closeDuration:!1,closeEasing:!1,closeOnHover:!0,extendedTimeOut:1e3,iconClasses:{error:"toast-error",info:"toast-info",success:"toast-success",warning:"toast-warning"},iconClass:"toast-info",positionClass:"toast-top-right",timeOut:5e3,titleClass:"toast-title",messageClass:"toast-message",escapeHtml:!1,target:"body",closeHtml:'<button type="button">&times;</button>',closeClass:"toast-close-button",newestOnTop:!0,preventDuplicates:!1,progressBar:!1,progressClass:"toast-progress",rtl:!1}}function f(e){C&&C(e)}function g(t){function o(e){return null==e&&(e=""),e.replace(/&/g,"&amp;").replace(/"/g,"&quot;").replace(/'/g,"&#39;").replace(/</g,"&lt;").replace(/>/g,"&gt;")}function s(){c(),u(),d(),p(),g(),C(),l(),i()}function i(){var e="";switch(t.iconClass){case"toast-success":case"toast-info":e="polite";break;default:e="assertive"}I.attr("aria-live",e)}function a(){E.closeOnHover&&I.hover(H,D),!E.onclick&&E.tapToDismiss&&I.click(b),E.closeButton&&j&&j.click(function(e){e.stopPropagation?e.stopPropagation():void 0!==e.cancelBubble&&e.cancelBubble!==!0&&(e.cancelBubble=!0),E.onCloseClick&&E.onCloseClick(e),b(!0)}),E.onclick&&I.click(function(e){E.onclick(e),b()})}function r(){I.hide(),I[E.showMethod]({duration:E.showDuration,easing:E.showEasing,complete:E.onShown}),E.timeOut>0&&(k=setTimeout(b,E.timeOut),F.maxHideTime=parseFloat(E.timeOut),F.hideEta=(new Date).getTime()+F.maxHideTime,E.progressBar&&(F.intervalId=setInterval(x,10)))}function c(){t.iconClass&&I.addClass(E.toastClass).addClass(y)}function l(){E.newestOnTop?v.prepend(I):v.append(I)}function u(){if(t.title){var e=t.title;E.escapeHtml&&(e=o(t.title)),M.append(e).addClass(E.titleClass),I.append(M)}}function d(){if(t.message){var e=t.message;E.escapeHtml&&(e=o(t.message)),B.append(e).addClass(E.messageClass),I.append(B)}}function p(){E.closeButton&&(j.addClass(E.closeClass).attr("role","button"),I.prepend(j))}function g(){E.progressBar&&(q.addClass(E.progressClass),I.prepend(q))}function C(){E.rtl&&I.addClass("rtl")}function O(e,t){if(e.preventDuplicates){if(t.message===w)return!0;w=t.message}return!1}function b(t){var n=t&&E.closeMethod!==!1?E.closeMethod:E.hideMethod,o=t&&E.closeDuration!==!1?E.closeDuration:E.hideDuration,s=t&&E.closeEasing!==!1?E.closeEasing:E.hideEasing;if(!e(":focus",I).length||t)return clearTimeout(F.intervalId),I[n]({duration:o,easing:s,complete:function(){h(I),clearTimeout(k),E.onHidden&&"hidden"!==P.state&&E.onHidden(),P.state="hidden",P.endTime=new Date,f(P)}})}function D(){(E.timeOut>0||E.extendedTimeOut>0)&&(k=setTimeout(b,E.extendedTimeOut),F.maxHideTime=parseFloat(E.extendedTimeOut),F.hideEta=(new Date).getTime()+F.maxHideTime)}function H(){clearTimeout(k),F.hideEta=0,I.stop(!0,!0)[E.showMethod]({duration:E.showDuration,easing:E.showEasing})}function x(){var e=(F.hideEta-(new Date).getTime())/F.maxHideTime*100;q.width(e+"%")}var E=m(),y=t.iconClass||E.iconClass;if("undefined"!=typeof t.optionsOverride&&(E=e.extend(E,t.optionsOverride),y=t.optionsOverride.iconClass||y),!O(E,t)){T++,v=n(E,!0);var k=null,I=e("<div/>"),M=e("<div/>"),B=e("<div/>"),q=e("<div/>"),j=e(E.closeHtml),F={intervalId:null,hideEta:null,maxHideTime:null},P={toastId:T,state:"visible",startTime:new Date,options:E,map:t};return s(),r(),a(),f(P),E.debug&&console&&console.log(P),I}}function m(){return e.extend({},p(),b.options)}function h(e){v||(v=n()),e.is(":visible")||(e.remove(),e=null,0===v.children().length&&(v.remove(),w=void 0))}var v,C,w,T=0,O={error:"error",info:"info",success:"success",warning:"warning"},b={clear:r,remove:c,error:t,getContainer:n,info:o,options:{},subscribe:s,success:i,version:"2.1.3",warning:a};return b}()})}("function"==typeof define&&define.amd?define:function(e,t){"undefined"!=typeof module&&module.exports?module.exports=t(require("jquery")):window.toastr=t(window.jQuery)});
//# sourceMappingURL=toastr.js.map

/*****
* CONFIGURATION
*/
    //Main navigation
    $.navigation = $('nav > ul.nav');

	$.panelIconOpened = 'icon-arrow-up';
	$.panelIconClosed = 'icon-arrow-down';

	//Default colours
	$.brandPrimary =  '#20a8d8';
	$.brandSuccess =  '#4dbd74';
	$.brandInfo =     '#63c2de';
	$.brandWarning =  '#f8cb00';
	$.brandDanger =   '#f86c6b';

	$.grayDark =      '#2a2c36';
	$.gray =          '#55595c';
	$.grayLight =     '#818a91';
	$.grayLighter =   '#d1d4d7';
	$.grayLightest =  '#f8f9fa';

'use strict';

/****
* MAIN NAVIGATION
*/

$(document).ready(function($){

	// Add class .active to current link
	$.navigation.find('a').each(function(){

		var cUrl = String(window.location);

		if (cUrl.substr(cUrl.length - 1) == '#') {
			cUrl = cUrl.slice(0,-1);
		}

		if ($($(this))[0].href==cUrl) {
			$(this).addClass('active');

			$(this).parents('ul').add(this).each(function(){
			    $(this).parent().addClass('nt').addClass('open');
			});
		}
	});

	// Dropdown Menu
	$.navigation.on('click', 'a', function(e){

		if ($.ajaxLoad) {
			e.preventDefault();
		}

        if ($(this).hasClass('nav-dropdown-toggle')) {
			$(this).parent().removeClass('nt').toggleClass('open');
		}

	});

	function resizeBroadcast() {

		var timesRun = 0;
		var interval = setInterval(function(){
			timesRun += 1;
			if(timesRun === 5){
				clearInterval(interval);
			}
			window.dispatchEvent(new Event('resize'));
		}, 62.5);
	}

	/* ---------- Main Menu Open/Close, Min/Full ---------- */
	$('.navbar-toggler').click(function(){

		var bodyClass = localStorage.getItem('body-class');

		if ($(this).hasClass('layout-toggler') && $('body').hasClass('sidebar-off-canvas')) {
			$('body').toggleClass('sidebar-opened').parent().toggleClass('sidebar-opened');
			//resize charts
			resizeBroadcast();

		} else if ($(this).hasClass('layout-toggler') && ($('body').hasClass('sidebar-nav') || bodyClass == 'sidebar-nav')) {
			$('body').toggleClass('sidebar-nav');
			localStorage.setItem('body-class', 'sidebar-nav');
			if (bodyClass == 'sidebar-nav') {
				localStorage.clear();
			}
			//resize charts
			resizeBroadcast();
		} else {
			$('body').toggleClass('mobile-open');
		}
	});

	$('.aside-toggle').click(function(){
		$('body').toggleClass('aside-menu-open');

		//resize charts
		resizeBroadcast();
	});

	$('.sidebar-close').click(function(){
		$('body').toggleClass('sidebar-opened').parent().toggleClass('sidebar-opened');
	});

	/* ---------- Disable moving to top ---------- */
	$('a[href="#"][data-top!=true]').click(function(e){
		e.preventDefault();
	});

});

/****
* CARDS ACTIONS
*/

$(document).on('click', '.card-actions a', function(e){
	e.preventDefault();

	if ($(this).hasClass('btn-close')) {
		$(this).parent().parent().parent().fadeOut();
	} else if ($(this).hasClass('btn-minimize')) {
		var $target = $(this).parent().parent().next('.card-block');
		if (!$(this).hasClass('collapsed')) {
			$('i',$(this)).removeClass($.panelIconOpened).addClass($.panelIconClosed);
		} else {
			$('i',$(this)).removeClass($.panelIconClosed).addClass($.panelIconOpened);
		}

	} else if ($(this).hasClass('btn-setting')) {
		$('#myModal').modal('show');
	}

});

function capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}

function init(url) {

	/* ---------- Tooltip ---------- */
	$('[rel="tooltip"],[data-rel="tooltip"]').tooltip({"placement":"bottom",delay: { show: 400, hide: 200 }});

	/* ---------- Popover ---------- */
	$('[rel="popover"],[data-rel="popover"],[data-toggle="popover"]').popover();

}

/**
 * Bootstrap Multiselect (https://github.com/davidstutz/bootstrap-multiselect)
 * 
 * Apache License, Version 2.0:
 * Copyright (c) 2012 - 2015 David Stutz
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a
 * copy of the License at http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * 
 * BSD 3-Clause License:
 * Copyright (c) 2012 - 2015 David Stutz
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *    - Redistributions of source code must retain the above copyright notice,
 *      this list of conditions and the following disclaimer.
 *    - Redistributions in binary form must reproduce the above copyright notice,
 *      this list of conditions and the following disclaimer in the documentation
 *      and/or other materials provided with the distribution.
 *    - Neither the name of David Stutz nor the names of its contributors may be
 *      used to endorse or promote products derived from this software without
 *      specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
!function ($) {
    "use strict";// jshint ;_;

    if (typeof ko !== 'undefined' && ko.bindingHandlers && !ko.bindingHandlers.multiselect) {
        ko.bindingHandlers.multiselect = {
            after: ['options', 'value', 'selectedOptions'],

            init: function(element, valueAccessor, allBindings, viewModel, bindingContext) {
                var $element = $(element);
                var config = ko.toJS(valueAccessor());

                $element.multiselect(config);

                if (allBindings.has('options')) {
                    var options = allBindings.get('options');
                    if (ko.isObservable(options)) {
                        ko.computed({
                            read: function() {
                                options();
                                setTimeout(function() {
                                    var ms = $element.data('multiselect');
                                    if (ms)
                                        ms.updateOriginalOptions();//Not sure how beneficial this is.
                                    $element.multiselect('rebuild');
                                }, 1);
                            },
                            disposeWhenNodeIsRemoved: element
                        });
                    }
                }

                //value and selectedOptions are two-way, so these will be triggered even by our own actions.
                //It needs some way to tell if they are triggered because of us or because of outside change.
                //It doesn't loop but it's a waste of processing.
                if (allBindings.has('value')) {
                    var value = allBindings.get('value');
                    if (ko.isObservable(value)) {
                        ko.computed({
                            read: function() {
                                value();
                                setTimeout(function() {
                                    $element.multiselect('refresh');
                                }, 1);
                            },
                            disposeWhenNodeIsRemoved: element
                        }).extend({ rateLimit: 100, notifyWhenChangesStop: true });
                    }
                }

                //Switched from arrayChange subscription to general subscription using 'refresh'.
                //Not sure performance is any better using 'select' and 'deselect'.
                if (allBindings.has('selectedOptions')) {
                    var selectedOptions = allBindings.get('selectedOptions');
                    if (ko.isObservable(selectedOptions)) {
                        ko.computed({
                            read: function() {
                                selectedOptions();
                                setTimeout(function() {
                                    $element.multiselect('refresh');
                                }, 1);
                            },
                            disposeWhenNodeIsRemoved: element
                        }).extend({ rateLimit: 100, notifyWhenChangesStop: true });
                    }
                }

                ko.utils.domNodeDisposal.addDisposeCallback(element, function() {
                    $element.multiselect('destroy');
                });
            },

            update: function(element, valueAccessor, allBindings, viewModel, bindingContext) {
                var $element = $(element);
                var config = ko.toJS(valueAccessor());

                $element.multiselect('setOptions', config);
                $element.multiselect('rebuild');
            }
        };
    }

    function forEach(array, callback) {
        for (var index = 0; index < array.length; ++index) {
            callback(array[index], index);
        }
    }

    /**
     * Constructor to create a new multiselect using the given select.
     *
     * @param {jQuery} select
     * @param {Object} options
     * @returns {Multiselect}
     */
    function Multiselect(select, options) {

        this.$select = $(select);
        
        // Placeholder via data attributes
        if (this.$select.attr("data-placeholder")) {
            options.nonSelectedText = this.$select.data("placeholder");
        }
        
        this.options = this.mergeOptions($.extend({}, options, this.$select.data()));

        // Initialization.
        // We have to clone to create a new reference.
        this.originalOptions = this.$select.clone()[0].options;
        this.query = '';
        this.searchTimeout = null;
        this.lastToggledInput = null

        this.options.multiple = this.$select.attr('multiple') === "multiple";
        this.options.onChange = $.proxy(this.options.onChange, this);
        this.options.onDropdownShow = $.proxy(this.options.onDropdownShow, this);
        this.options.onDropdownHide = $.proxy(this.options.onDropdownHide, this);
        this.options.onDropdownShown = $.proxy(this.options.onDropdownShown, this);
        this.options.onDropdownHidden = $.proxy(this.options.onDropdownHidden, this);
        
        // Build select all if enabled.
        this.buildContainer();
        this.buildButton();
        this.buildDropdown();
        this.buildSelectAll();
        this.buildDropdownOptions();
        this.buildFilter();

        this.updateButtonText();
        this.updateSelectAll();

        if (this.options.disableIfEmpty && $('option', this.$select).length <= 0) {
            this.disable();
        }
        
        this.$select.hide().after(this.$container);
    };

    Multiselect.prototype = {

        defaults: {
            /**
             * Default text function will either print 'None selected' in case no
             * option is selected or a list of the selected options up to a length
             * of 3 selected options.
             * 
             * @param {jQuery} options
             * @param {jQuery} select
             * @returns {String}
             */
            buttonText: function(options, select) {
                if (options.length === 0) {
                    return this.nonSelectedText;
                }
                else if (this.allSelectedText 
                            && options.length === $('option', $(select)).length 
                            && $('option', $(select)).length !== 1 
                            && this.multiple) {

                    if (this.selectAllNumber) {
                        return this.allSelectedText + ' (' + options.length + ')';
                    }
                    else {
                        return this.allSelectedText;
                    }
                }
                else if (options.length > this.numberDisplayed) {
                    return options.length + ' ' + this.nSelectedText;
                }
                else {
                    var selected = '';
                    var delimiter = this.delimiterText;
                    
                    options.each(function() {
                        var label = ($(this).attr('label') !== undefined) ? $(this).attr('label') : $(this).text();
                        selected += label + delimiter;
                    });
                    
                    return selected.substr(0, selected.length - 2);
                }
            },
            /**
             * Updates the title of the button similar to the buttonText function.
             * 
             * @param {jQuery} options
             * @param {jQuery} select
             * @returns {@exp;selected@call;substr}
             */
            buttonTitle: function(options, select) {
                if (options.length === 0) {
                    return this.nonSelectedText;
                }
                else {
                    var selected = '';
                    var delimiter = this.delimiterText;
                    
                    options.each(function () {
                        var label = ($(this).attr('label') !== undefined) ? $(this).attr('label') : $(this).text();
                        selected += label + delimiter;
                    });
                    return selected.substr(0, selected.length - 2);
                }
            },
            /**
             * Create a label.
             *
             * @param {jQuery} element
             * @returns {String}
             */
            optionLabel: function(element){
                return $(element).attr('label') || $(element).text();
            },
            /**
             * Triggered on change of the multiselect.
             * 
             * Not triggered when selecting/deselecting options manually.
             * 
             * @param {jQuery} option
             * @param {Boolean} checked
             */
            onChange : function(option, checked) {

            },
            /**
             * Triggered when the dropdown is shown.
             *
             * @param {jQuery} event
             */
            onDropdownShow: function(event) {

            },
            /**
             * Triggered when the dropdown is hidden.
             *
             * @param {jQuery} event
             */
            onDropdownHide: function(event) {

            },
            /**
             * Triggered after the dropdown is shown.
             * 
             * @param {jQuery} event
             */
            onDropdownShown: function(event) {
                
            },
            /**
             * Triggered after the dropdown is hidden.
             * 
             * @param {jQuery} event
             */
            onDropdownHidden: function(event) {
                
            },
            /**
             * Triggered on select all.
             */
            onSelectAll: function() {
                
            },
            enableHTML: false,
            buttonClass: 'btn btn-default',
            inheritClass: false,
            buttonWidth: 'auto',
            buttonContainer: '<div class="btn-group" />',
            dropRight: false,
            selectedClass: 'active',
            // Maximum height of the dropdown menu.
            // If maximum height is exceeded a scrollbar will be displayed.
            maxHeight: false,
            checkboxName: false,
            includeSelectAllOption: false,
            includeSelectAllIfMoreThan: 0,
            selectAllText: ' Select all',
            selectAllValue: 'multiselect-all',
            selectAllName: false,
            selectAllNumber: true,
            enableFiltering: false,
            enableCaseInsensitiveFiltering: false,
            enableClickableOptGroups: false,
            filterPlaceholder: 'Search',
            // possible options: 'text', 'value', 'both'
            filterBehavior: 'text',
            includeFilterClearBtn: true,
            preventInputChangeEvent: false,
            nonSelectedText: 'None selected',
            nSelectedText: 'selected',
            allSelectedText: 'All selected',
            numberDisplayed: 3,
            disableIfEmpty: false,
            delimiterText: ', ',
            templates: {
                button: '<button type="button" class="multiselect dropdown-toggle" data-toggle="dropdown"><span class="multiselect-selected-text"></span> <b class="caret"></b></button>',
                ul: '<ul class="multiselect-container dropdown-menu"></ul>',
                filter: '<li class="multiselect-item filter"><div class="input-group"><span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span><input class="form-control multiselect-search" type="text"></div></li>',
                filterClearBtn: '<span class="input-group-btn"><button class="btn btn-default multiselect-clear-filter" type="button"><i class="glyphicon glyphicon-remove-circle"></i></button></span>',
                li: '<li><a tabindex="0"><label></label></a></li>',
                divider: '<li class="multiselect-item divider"></li>',
                liGroup: '<li class="multiselect-item multiselect-group"><label></label></li>'
            }
        },

        constructor: Multiselect,

        /**
         * Builds the container of the multiselect.
         */
        buildContainer: function() {
            this.$container = $(this.options.buttonContainer);
            this.$container.on('show.bs.dropdown', this.options.onDropdownShow);
            this.$container.on('hide.bs.dropdown', this.options.onDropdownHide);
            this.$container.on('shown.bs.dropdown', this.options.onDropdownShown);
            this.$container.on('hidden.bs.dropdown', this.options.onDropdownHidden);
        },

        /**
         * Builds the button of the multiselect.
         */
        buildButton: function() {
            this.$button = $(this.options.templates.button).addClass(this.options.buttonClass);
            if (this.$select.attr('class') && this.options.inheritClass) {
                this.$button.addClass(this.$select.attr('class'));
            }
            // Adopt active state.
            if (this.$select.prop('disabled')) {
                this.disable();
            }
            else {
                this.enable();
            }

            // Manually add button width if set.
            if (this.options.buttonWidth && this.options.buttonWidth !== 'auto') {
                this.$button.css({
                    'width' : this.options.buttonWidth,
                    'overflow' : 'hidden',
                    'text-overflow' : 'ellipsis'
                });
                this.$container.css({
                    'width': this.options.buttonWidth
                });
            }

            // Keep the tab index from the select.
            var tabindex = this.$select.attr('tabindex');
            if (tabindex) {
                this.$button.attr('tabindex', tabindex);
            }

            this.$container.prepend(this.$button);
        },

        /**
         * Builds the ul representing the dropdown menu.
         */
        buildDropdown: function() {

            // Build ul.
            this.$ul = $(this.options.templates.ul);

            if (this.options.dropRight) {
                this.$ul.addClass('pull-right');
            }

            // Set max height of dropdown menu to activate auto scrollbar.
            if (this.options.maxHeight) {
                // TODO: Add a class for this option to move the css declarations.
                this.$ul.css({
                    'max-height': this.options.maxHeight + 'px',
                    'overflow-y': 'auto',
                    'overflow-x': 'hidden'
                });
            }

            this.$container.append(this.$ul);
        },

        /**
         * Build the dropdown options and binds all nessecary events.
         * 
         * Uses createDivider and createOptionValue to create the necessary options.
         */
        buildDropdownOptions: function() {

            this.$select.children().each($.proxy(function(index, element) {

                var $element = $(element);
                // Support optgroups and options without a group simultaneously.
                var tag = $element.prop('tagName')
                    .toLowerCase();
            
                if ($element.prop('value') === this.options.selectAllValue) {
                    return;
                }

                if (tag === 'optgroup') {
                    this.createOptgroup(element);
                }
                else if (tag === 'option') {

                    if ($element.data('role') === 'divider') {
                        this.createDivider();
                    }
                    else {
                        this.createOptionValue(element);
                    }

                }

                // Other illegal tags will be ignored.
            }, this));

            // Bind the change event on the dropdown elements.
            $('li input', this.$ul).on('change', $.proxy(function(event) {
                var $target = $(event.target);

                var checked = $target.prop('checked') || false;
                var isSelectAllOption = $target.val() === this.options.selectAllValue;

                // Apply or unapply the configured selected class.
                if (this.options.selectedClass) {
                    if (checked) {
                        $target.closest('li')
                            .addClass(this.options.selectedClass);
                    }
                    else {
                        $target.closest('li')
                            .removeClass(this.options.selectedClass);
                    }
                }

                // Get the corresponding option.
                var value = $target.val();
                var $option = this.getOptionByValue(value);

                var $optionsNotThis = $('option', this.$select).not($option);
                var $checkboxesNotThis = $('input', this.$container).not($target);

                if (isSelectAllOption) {
                    if (checked) {
                        this.selectAll();
                    }
                    else {
                        this.deselectAll();
                    }
                }

                if(!isSelectAllOption){
                    if (checked) {
                        $option.prop('selected', true);

                        if (this.options.multiple) {
                            // Simply select additional option.
                            $option.prop('selected', true);
                        }
                        else {
                            // Unselect all other options and corresponding checkboxes.
                            if (this.options.selectedClass) {
                                $($checkboxesNotThis).closest('li').removeClass(this.options.selectedClass);
                            }

                            $($checkboxesNotThis).prop('checked', false);
                            $optionsNotThis.prop('selected', false);

                            // It's a single selection, so close.
                            this.$button.click();
                        }

                        if (this.options.selectedClass === "active") {
                            $optionsNotThis.closest("a").css("outline", "");
                        }
                    }
                    else {
                        // Unselect option.
                        $option.prop('selected', false);
                    }
                }

                this.$select.change();

                this.updateButtonText();
                this.updateSelectAll();

                this.options.onChange($option, checked);

                if(this.options.preventInputChangeEvent) {
                    return false;
                }
            }, this));

            $('li a', this.$ul).on('mousedown', function(e) {
                if (e.shiftKey) {
                    // Prevent selecting text by Shift+click
                    return false;
                }
            });
        
            $('li a', this.$ul).on('touchstart click', $.proxy(function(event) {
                event.stopPropagation();

                var $target = $(event.target);
                
                if (event.shiftKey && this.options.multiple) {
                    if($target.is("label")){ // Handles checkbox selection manually (see https://github.com/davidstutz/bootstrap-multiselect/issues/431)
                        event.preventDefault();
                        $target = $target.find("input");
                        $target.prop("checked", !$target.prop("checked"));
                    }
                    var checked = $target.prop('checked') || false;

                    if (this.lastToggledInput !== null && this.lastToggledInput !== $target) { // Make sure we actually have a range
                        var from = $target.closest("li").index();
                        var to = this.lastToggledInput.closest("li").index();
                        
                        if (from > to) { // Swap the indices
                            var tmp = to;
                            to = from;
                            from = tmp;
                        }
                        
                        // Make sure we grab all elements since slice excludes the last index
                        ++to;
                        
                        // Change the checkboxes and underlying options
                        var range = this.$ul.find("li").slice(from, to).find("input");
                        
                        range.prop('checked', checked);
                        
                        if (this.options.selectedClass) {
                            range.closest('li')
                                .toggleClass(this.options.selectedClass, checked);
                        }
                        
                        for (var i = 0, j = range.length; i < j; i++) {
                            var $checkbox = $(range[i]);

                            var $option = this.getOptionByValue($checkbox.val());

                            $option.prop('selected', checked);
                        }                   
                    }
                    
                    // Trigger the select "change" event
                    $target.trigger("change");
                }
                
                // Remembers last clicked option
                if($target.is("input") && !$target.closest("li").is(".multiselect-item")){
                    this.lastToggledInput = $target;
                }

                $target.blur();
            }, this));

            // Keyboard support.
            this.$container.off('keydown.multiselect').on('keydown.multiselect', $.proxy(function(event) {
                if ($('input[type="text"]', this.$container).is(':focus')) {
                    return;
                }

                if (event.keyCode === 9 && this.$container.hasClass('open')) {
                    this.$button.click();
                }
                else {
                    var $items = $(this.$container).find("li:not(.divider):not(.disabled) a").filter(":visible");

                    if (!$items.length) {
                        return;
                    }

                    var index = $items.index($items.filter(':focus'));

                    // Navigation up.
                    if (event.keyCode === 38 && index > 0) {
                        index--;
                    }
                    // Navigate down.
                    else if (event.keyCode === 40 && index < $items.length - 1) {
                        index++;
                    }
                    else if (!~index) {
                        index = 0;
                    }

                    var $current = $items.eq(index);
                    $current.focus();

                    if (event.keyCode === 32 || event.keyCode === 13) {
                        var $checkbox = $current.find('input');

                        $checkbox.prop("checked", !$checkbox.prop("checked"));
                        $checkbox.change();
                    }

                    event.stopPropagation();
                    event.preventDefault();
                }
            }, this));

            if(this.options.enableClickableOptGroups && this.options.multiple) {
                $('li.multiselect-group', this.$ul).on('click', $.proxy(function(event) {
                    event.stopPropagation();

                    var group = $(event.target).parent();

                    // Search all option in optgroup
                    var $options = group.nextUntil('li.multiselect-group');
                    var $visibleOptions = $options.filter(":visible:not(.disabled)");

                    // check or uncheck items
                    var allChecked = true;
                    var optionInputs = $visibleOptions.find('input');
                    optionInputs.each(function() {
                        allChecked = allChecked && $(this).prop('checked');
                    });

                    optionInputs.prop('checked', !allChecked).trigger('change');
               }, this));
            }
        },

        /**
         * Create an option using the given select option.
         *
         * @param {jQuery} element
         */
        createOptionValue: function(element) {
            var $element = $(element);
            if ($element.is(':selected')) {
                $element.prop('selected', true);
            }

            // Support the label attribute on options.
            var label = this.options.optionLabel(element);
            var value = $element.val();
            var inputType = this.options.multiple ? "checkbox" : "radio";

            var $li = $(this.options.templates.li);
            var $label = $('label', $li);
            $label.addClass(inputType);

            if (this.options.enableHTML) {
                $label.html(" " + label);
            }
            else {
                $label.text(" " + label);
            }
        
            var $checkbox = $('<input/>').attr('type', inputType);

            if (this.options.checkboxName) {
                $checkbox.attr('name', this.options.checkboxName);
            }
            $label.prepend($checkbox);

            var selected = $element.prop('selected') || false;
            $checkbox.val(value);

            if (value === this.options.selectAllValue) {
                $li.addClass("multiselect-item multiselect-all");
                $checkbox.parent().parent()
                    .addClass('multiselect-all');
            }

            $label.attr('title', $element.attr('title'));

            this.$ul.append($li);

            if ($element.is(':disabled')) {
                $checkbox.attr('disabled', 'disabled')
                    .prop('disabled', true)
                    .closest('a')
                    .attr("tabindex", "-1")
                    .closest('li')
                    .addClass('disabled');
            }

            $checkbox.prop('checked', selected);

            if (selected && this.options.selectedClass) {
                $checkbox.closest('li')
                    .addClass(this.options.selectedClass);
            }
        },

        /**
         * Creates a divider using the given select option.
         *
         * @param {jQuery} element
         */
        createDivider: function(element) {
            var $divider = $(this.options.templates.divider);
            this.$ul.append($divider);
        },

        /**
         * Creates an optgroup.
         *
         * @param {jQuery} group
         */
        createOptgroup: function(group) {
            var groupName = $(group).prop('label');

            // Add a header for the group.
            var $li = $(this.options.templates.liGroup);
            
            if (this.options.enableHTML) {
                $('label', $li).html(groupName);
            }
            else {
                $('label', $li).text(groupName);
            }
            
            if (this.options.enableClickableOptGroups) {
                $li.addClass('multiselect-group-clickable');
            }

            this.$ul.append($li);

            if ($(group).is(':disabled')) {
                $li.addClass('disabled');
            }

            // Add the options of the group.
            $('option', group).each($.proxy(function(index, element) {
                this.createOptionValue(element);
            }, this));
        },

        /**
         * Build the selct all.
         * 
         * Checks if a select all has already been created.
         */
        buildSelectAll: function() {
            if (typeof this.options.selectAllValue === 'number') {
                this.options.selectAllValue = this.options.selectAllValue.toString();
            }
            
            var alreadyHasSelectAll = this.hasSelectAll();

            if (!alreadyHasSelectAll && this.options.includeSelectAllOption && this.options.multiple
                    && $('option', this.$select).length > this.options.includeSelectAllIfMoreThan) {

                // Check whether to add a divider after the select all.
                if (this.options.includeSelectAllDivider) {
                    this.$ul.prepend($(this.options.templates.divider));
                }

                var $li = $(this.options.templates.li);
                $('label', $li).addClass("checkbox");
                
                if (this.options.enableHTML) {
                    $('label', $li).html(" " + this.options.selectAllText);
                }
                else {
                    $('label', $li).text(" " + this.options.selectAllText);
                }
                
                if (this.options.selectAllName) {
                    $('label', $li).prepend('<input type="checkbox" name="' + this.options.selectAllName + '" />');
                }
                else {
                    $('label', $li).prepend('<input type="checkbox" />');
                }
                
                var $checkbox = $('input', $li);
                $checkbox.val(this.options.selectAllValue);

                $li.addClass("multiselect-item multiselect-all");
                $checkbox.parent().parent()
                    .addClass('multiselect-all');

                this.$ul.prepend($li);

                $checkbox.prop('checked', false);
            }
        },

        /**
         * Builds the filter.
         */
        buildFilter: function() {

            // Build filter if filtering OR case insensitive filtering is enabled and the number of options exceeds (or equals) enableFilterLength.
            if (this.options.enableFiltering || this.options.enableCaseInsensitiveFiltering) {
                var enableFilterLength = Math.max(this.options.enableFiltering, this.options.enableCaseInsensitiveFiltering);

                if (this.$select.find('option').length >= enableFilterLength) {

                    this.$filter = $(this.options.templates.filter);
                    $('input', this.$filter).attr('placeholder', this.options.filterPlaceholder);
                    
                    // Adds optional filter clear button
                    if(this.options.includeFilterClearBtn){
                        var clearBtn = $(this.options.templates.filterClearBtn);
                        clearBtn.on('click', $.proxy(function(event){
                            clearTimeout(this.searchTimeout);
                            this.$filter.find('.multiselect-search').val('');
                            $('li', this.$ul).show().removeClass("filter-hidden");
                            this.updateSelectAll();
                        }, this));
                        this.$filter.find('.input-group').append(clearBtn);
                    }
                    
                    this.$ul.prepend(this.$filter);

                    this.$filter.val(this.query).on('click', function(event) {
                        event.stopPropagation();
                    }).on('input keydown', $.proxy(function(event) {
                        // Cancel enter key default behaviour
                        if (event.which === 13) {
                          event.preventDefault();
                        }
                        
                        // This is useful to catch "keydown" events after the browser has updated the control.
                        clearTimeout(this.searchTimeout);

                        this.searchTimeout = this.asyncFunction($.proxy(function() {

                            if (this.query !== event.target.value) {
                                this.query = event.target.value;

                                var currentGroup, currentGroupVisible;
                                $.each($('li', this.$ul), $.proxy(function(index, element) {
                                    var value = $('input', element).length > 0 ? $('input', element).val() : "";
                                    var text = $('label', element).text();

                                    var filterCandidate = '';
                                    if ((this.options.filterBehavior === 'text')) {
                                        filterCandidate = text;
                                    }
                                    else if ((this.options.filterBehavior === 'value')) {
                                        filterCandidate = value;
                                    }
                                    else if (this.options.filterBehavior === 'both') {
                                        filterCandidate = text + '\n' + value;
                                    }

                                    if (value !== this.options.selectAllValue && text) {
                                        // By default lets assume that element is not
                                        // interesting for this search.
                                        var showElement = false;

                                        if (this.options.enableCaseInsensitiveFiltering && filterCandidate.toLowerCase().indexOf(this.query.toLowerCase()) > -1) {
                                            showElement = true;
                                        }
                                        else if (filterCandidate.indexOf(this.query) > -1) {
                                            showElement = true;
                                        }

                                        // Toggle current element (group or group item) according to showElement boolean.
                                        $(element).toggle(showElement).toggleClass('filter-hidden', !showElement);
                                        
                                        // Differentiate groups and group items.
                                        if ($(element).hasClass('multiselect-group')) {
                                            // Remember group status.
                                            currentGroup = element;
                                            currentGroupVisible = showElement;
                                        }
                                        else {
                                            // Show group name when at least one of its items is visible.
                                            if (showElement) {
                                                $(currentGroup).show().removeClass('filter-hidden');
                                            }
                                            
                                            // Show all group items when group name satisfies filter.
                                            if (!showElement && currentGroupVisible) {
                                                $(element).show().removeClass('filter-hidden');
                                            }
                                        }
                                    }
                                }, this));
                            }

                            this.updateSelectAll();
                        }, this), 300, this);
                    }, this));
                }
            }
        },

        /**
         * Unbinds the whole plugin.
         */
        destroy: function() {
            this.$container.remove();
            this.$select.show();
            this.$select.data('multiselect', null);
        },

        /**
         * Refreshs the multiselect based on the selected options of the select.
         */
        refresh: function() {
            $('option', this.$select).each($.proxy(function(index, element) {
                var $input = $('li input', this.$ul).filter(function() {
                    return $(this).val() === $(element).val();
                });

                if ($(element).is(':selected')) {
                    $input.prop('checked', true);

                    if (this.options.selectedClass) {
                        $input.closest('li')
                            .addClass(this.options.selectedClass);
                    }
                }
                else {
                    $input.prop('checked', false);

                    if (this.options.selectedClass) {
                        $input.closest('li')
                            .removeClass(this.options.selectedClass);
                    }
                }

                if ($(element).is(":disabled")) {
                    $input.attr('disabled', 'disabled')
                        .prop('disabled', true)
                        .closest('li')
                        .addClass('disabled');
                }
                else {
                    $input.prop('disabled', false)
                        .closest('li')
                        .removeClass('disabled');
                }
            }, this));

            this.updateButtonText();
            this.updateSelectAll();
        },

        /**
         * Select all options of the given values.
         * 
         * If triggerOnChange is set to true, the on change event is triggered if
         * and only if one value is passed.
         * 
         * @param {Array} selectValues
         * @param {Boolean} triggerOnChange
         */
        select: function(selectValues, triggerOnChange) {
            if(!$.isArray(selectValues)) {
                selectValues = [selectValues];
            }

            for (var i = 0; i < selectValues.length; i++) {
                var value = selectValues[i];

                if (value === null || value === undefined) {
                    continue;
                }

                var $option = this.getOptionByValue(value);
                var $checkbox = this.getInputByValue(value);

                if($option === undefined || $checkbox === undefined) {
                    continue;
                }
                
                if (!this.options.multiple) {
                    this.deselectAll(false);
                }
                
                if (this.options.selectedClass) {
                    $checkbox.closest('li')
                        .addClass(this.options.selectedClass);
                }

                $checkbox.prop('checked', true);
                $option.prop('selected', true);
                
                if (triggerOnChange) {
                    this.options.onChange($option, true);
                }
            }

            this.updateButtonText();
            this.updateSelectAll();
        },

        /**
         * Clears all selected items.
         */
        clearSelection: function () {
            this.deselectAll(false);
            this.updateButtonText();
            this.updateSelectAll();
        },

        /**
         * Deselects all options of the given values.
         * 
         * If triggerOnChange is set to true, the on change event is triggered, if
         * and only if one value is passed.
         * 
         * @param {Array} deselectValues
         * @param {Boolean} triggerOnChange
         */
        deselect: function(deselectValues, triggerOnChange) {
            if(!$.isArray(deselectValues)) {
                deselectValues = [deselectValues];
            }

            for (var i = 0; i < deselectValues.length; i++) {
                var value = deselectValues[i];

                if (value === null || value === undefined) {
                    continue;
                }

                var $option = this.getOptionByValue(value);
                var $checkbox = this.getInputByValue(value);

                if($option === undefined || $checkbox === undefined) {
                    continue;
                }

                if (this.options.selectedClass) {
                    $checkbox.closest('li')
                        .removeClass(this.options.selectedClass);
                }

                $checkbox.prop('checked', false);
                $option.prop('selected', false);
                
                if (triggerOnChange) {
                    this.options.onChange($option, false);
                }
            }

            this.updateButtonText();
            this.updateSelectAll();
        },
        
        /**
         * Selects all enabled & visible options.
         *
         * If justVisible is true or not specified, only visible options are selected.
         *
         * @param {Boolean} justVisible
         * @param {Boolean} triggerOnSelectAll
         */
        selectAll: function (justVisible, triggerOnSelectAll) {
            var justVisible = typeof justVisible === 'undefined' ? true : justVisible;
            var allCheckboxes = $("li input[type='checkbox']:enabled", this.$ul);
            var visibleCheckboxes = allCheckboxes.filter(":visible");
            var allCheckboxesCount = allCheckboxes.length;
            var visibleCheckboxesCount = visibleCheckboxes.length;
            
            if(justVisible) {
                visibleCheckboxes.prop('checked', true);
                $("li:not(.divider):not(.disabled)", this.$ul).filter(":visible").addClass(this.options.selectedClass);
            }
            else {
                allCheckboxes.prop('checked', true);
                $("li:not(.divider):not(.disabled)", this.$ul).addClass(this.options.selectedClass);
            }
                
            if (allCheckboxesCount === visibleCheckboxesCount || justVisible === false) {
                $("option:enabled", this.$select).prop('selected', true);
            }
            else {
                var values = visibleCheckboxes.map(function() {
                    return $(this).val();
                }).get();
                
                $("option:enabled", this.$select).filter(function(index) {
                    return $.inArray($(this).val(), values) !== -1;
                }).prop('selected', true);
            }
            
            if (triggerOnSelectAll) {
                this.options.onSelectAll();
            }
        },

        /**
         * Deselects all options.
         * 
         * If justVisible is true or not specified, only visible options are deselected.
         * 
         * @param {Boolean} justVisible
         */
        deselectAll: function (justVisible) {
            var justVisible = typeof justVisible === 'undefined' ? true : justVisible;
            
            if(justVisible) {              
                var visibleCheckboxes = $("li input[type='checkbox']:not(:disabled)", this.$ul).filter(":visible");
                visibleCheckboxes.prop('checked', false);
                
                var values = visibleCheckboxes.map(function() {
                    return $(this).val();
                }).get();
                
                $("option:enabled", this.$select).filter(function(index) {
                    return $.inArray($(this).val(), values) !== -1;
                }).prop('selected', false);
                
                if (this.options.selectedClass) {
                    $("li:not(.divider):not(.disabled)", this.$ul).filter(":visible").removeClass(this.options.selectedClass);
                }
            }
            else {
                $("li input[type='checkbox']:enabled", this.$ul).prop('checked', false);
                $("option:enabled", this.$select).prop('selected', false);
                
                if (this.options.selectedClass) {
                    $("li:not(.divider):not(.disabled)", this.$ul).removeClass(this.options.selectedClass);
                }
            }
        },

        /**
         * Rebuild the plugin.
         * 
         * Rebuilds the dropdown, the filter and the select all option.
         */
        rebuild: function() {
            this.$ul.html('');

            // Important to distinguish between radios and checkboxes.
            this.options.multiple = this.$select.attr('multiple') === "multiple";

            this.buildSelectAll();
            this.buildDropdownOptions();
            this.buildFilter();

            this.updateButtonText();
            this.updateSelectAll();
            
            if (this.options.disableIfEmpty && $('option', this.$select).length <= 0) {
                this.disable();
            }
            else {
                this.enable();
            }
            
            if (this.options.dropRight) {
                this.$ul.addClass('pull-right');
            }
        },

        /**
         * The provided data will be used to build the dropdown.
         */
        dataprovider: function(dataprovider) {
            
            var groupCounter = 0;
            var $select = this.$select.empty();
            
            $.each(dataprovider, function (index, option) {
                var $tag;
                
                if ($.isArray(option.children)) { // create optiongroup tag
                    groupCounter++;
                    
                    $tag = $('<optgroup/>').attr({
                        label: option.label || 'Group ' + groupCounter,
                        disabled: !!option.disabled
                    });
                    
                    forEach(option.children, function(subOption) { // add children option tags
                        $tag.append($('<option/>').attr({
                            value: subOption.value,
                            label: subOption.label || subOption.value,
                            title: subOption.title,
                            selected: !!subOption.selected,
                            disabled: !!subOption.disabled
                        }));
                    });
                }
                else {
                    $tag = $('<option/>').attr({
                        value: option.value,
                        label: option.label || option.value,
                        title: option.title,
                        selected: !!option.selected,
                        disabled: !!option.disabled
                    });
                }
                
                $select.append($tag);
            });
            
            this.rebuild();
        },

        /**
         * Enable the multiselect.
         */
        enable: function() {
            this.$select.prop('disabled', false);
            this.$button.prop('disabled', false)
                .removeClass('disabled');
        },

        /**
         * Disable the multiselect.
         */
        disable: function() {
            this.$select.prop('disabled', true);
            this.$button.prop('disabled', true)
                .addClass('disabled');
        },

        /**
         * Set the options.
         *
         * @param {Array} options
         */
        setOptions: function(options) {
            this.options = this.mergeOptions(options);
        },

        /**
         * Merges the given options with the default options.
         *
         * @param {Array} options
         * @returns {Array}
         */
        mergeOptions: function(options) {
            return $.extend(true, {}, this.defaults, this.options, options);
        },

        /**
         * Checks whether a select all checkbox is present.
         *
         * @returns {Boolean}
         */
        hasSelectAll: function() {
            return $('li.multiselect-all', this.$ul).length > 0;
        },

        /**
         * Updates the select all checkbox based on the currently displayed and selected checkboxes.
         */
        updateSelectAll: function() {
            if (this.hasSelectAll()) {
                var allBoxes = $("li:not(.multiselect-item):not(.filter-hidden) input:enabled", this.$ul);
                var allBoxesLength = allBoxes.length;
                var checkedBoxesLength = allBoxes.filter(":checked").length;
                var selectAllLi  = $("li.multiselect-all", this.$ul);
                var selectAllInput = selectAllLi.find("input");
                
                if (checkedBoxesLength > 0 && checkedBoxesLength === allBoxesLength) {
                    selectAllInput.prop("checked", true);
                    selectAllLi.addClass(this.options.selectedClass);
                    this.options.onSelectAll();
                }
                else {
                    selectAllInput.prop("checked", false);
                    selectAllLi.removeClass(this.options.selectedClass);
                }
            }
        },

        /**
         * Update the button text and its title based on the currently selected options.
         */
        updateButtonText: function() {
            var options = this.getSelected();
            
            // First update the displayed button text.
            if (this.options.enableHTML) {
                $('.multiselect .multiselect-selected-text', this.$container).html(this.options.buttonText(options, this.$select));
            }
            else {
                $('.multiselect .multiselect-selected-text', this.$container).text(this.options.buttonText(options, this.$select));
            }
            
            // Now update the title attribute of the button.
            $('.multiselect', this.$container).attr('title', this.options.buttonTitle(options, this.$select));
        },

        /**
         * Get all selected options.
         *
         * @returns {jQUery}
         */
        getSelected: function() {
            return $('option', this.$select).filter(":selected");
        },

        /**
         * Gets a select option by its value.
         *
         * @param {String} value
         * @returns {jQuery}
         */
        getOptionByValue: function (value) {

            var options = $('option', this.$select);
            var valueToCompare = value.toString();

            for (var i = 0; i < options.length; i = i + 1) {
                var option = options[i];
                if (option.value === valueToCompare) {
                    return $(option);
                }
            }
        },

        /**
         * Get the input (radio/checkbox) by its value.
         *
         * @param {String} value
         * @returns {jQuery}
         */
        getInputByValue: function (value) {

            var checkboxes = $('li input', this.$ul);
            var valueToCompare = value.toString();

            for (var i = 0; i < checkboxes.length; i = i + 1) {
                var checkbox = checkboxes[i];
                if (checkbox.value === valueToCompare) {
                    return $(checkbox);
                }
            }
        },

        /**
         * Used for knockout integration.
         */
        updateOriginalOptions: function() {
            this.originalOptions = this.$select.clone()[0].options;
        },

        asyncFunction: function(callback, timeout, self) {
            var args = Array.prototype.slice.call(arguments, 3);
            return setTimeout(function() {
                callback.apply(self || window, args);
            }, timeout);
        },

        setAllSelectedText: function(allSelectedText) {
            this.options.allSelectedText = allSelectedText;
            this.updateButtonText();
        }
    };

    $.fn.multiselect = function(option, parameter, extraOptions) {
        return this.each(function() {
            var data = $(this).data('multiselect');
            var options = typeof option === 'object' && option;

            // Initialize the multiselect.
            if (!data) {
                data = new Multiselect(this, options);
                $(this).data('multiselect', data);
            }

            // Call multiselect method.
            if (typeof option === 'string') {
                data[option](parameter, extraOptions);
                
                if (option === 'destroy') {
                    $(this).data('multiselect', false);
                }
            }
        });
    };

    $.fn.multiselect.Constructor = Multiselect;

    $(function() {
        $("select[data-role=multiselect]").multiselect();
    });

}(window.jQuery);
