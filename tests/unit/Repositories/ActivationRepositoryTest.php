<?php

use Carbon\Carbon;
use Tests\TestCase;
use App\Repositories\ActivationRepository;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ActivationRepositoryTest extends TestCase
{
    public function test_activation_is_beeing_created()
    {
        $user = factory(App\Models\User::class)->create([
            'activated' => false
        ]);

        $repo = new ActivationRepository();
        $token = $repo->createActivation($user);

        $this->assertNotEmpty($token);
        $this->assertDatabaseHas('user_activations', [
            'user_id' => $user->id
        ]);
    }

     public function test_activation_is_beeing_re_created()
    {
        $user = factory(App\Models\User::class)->create([
            'activated' => false
        ]);

        $user->activation()->create([
            'token' => 'foobar',
            'user_id' => $user->id,
            'created_at' => new Carbon()
        ]);

        $repo = new ActivationRepository();
        $token = $repo->createActivation($user);

        $this->assertNotEquals($token, 'foobar');
    }

    public function test_get_activation_by_token()
    {
        $user = factory(App\Models\User::class)->create([
            'activated' => false
        ]);

        $org_activation = $user->activation()->create([
            'token' => 'foobar',
            'user_id' => $user->id,
            'created_at' => new Carbon()
        ]);

        $repo = new ActivationRepository();
        $activation = $repo->getActivationByToken('foobar');

        $this->assertEquals($org_activation->token, $activation->token);
        $this->assertEquals($org_activation->user_id, $activation->user_id);
        $this->assertEquals($org_activation->created_at, $activation->created_at);
    }

    public function test_get_activation_is_beeing_deleted()
    {
        $user = factory(App\Models\User::class)->create([
            'activated' => false
        ]);

        $org_activation = $user->activation()->create([
            'token' => 'foobar',
            'user_id' => $user->id,
            'created_at' => new Carbon()
        ]);

        $repo = new ActivationRepository();
        $repo->deleteActivation('foobar');

        $this->assertDatabaseMissing('user_activations', [
            'token' => 'foobar',
            'user_id' => $user->id
        ]);
    }
}