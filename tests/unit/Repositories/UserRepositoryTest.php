<?php

use Tests\TestCase;
use App\Models\User;
use App\Models\Setting;
use App\Repositories\UserRepository;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class UserRepositoryTest extends TestCase
{
    public function test_getUsersWithNonDefaultShiftlistEntries()
    {
        $user1 = factory(App\Models\User::class)->create();
        Setting::create(['user_id' => $user1->id, 'key' => 'shiftlist_default_morning_value', 'value' => '0800']);
        Setting::create(['user_id' => $user1->id, 'key' => 'shiftlist_default_evening_value', 'value' => '1700']);

        $user2 = factory(App\Models\User::class)->create();
        Setting::create(['user_id' => $user2->id, 'key' => 'shiftlist_default_morning_value', 'value' => '0800']);
        Setting::create(['user_id' => $user2->id, 'key' => 'shiftlist_default_evening_value', 'value' => '1700']);

        $user3 = factory(App\Models\User::class)->create();
        Setting::create(['user_id' => $user3->id, 'key' => 'shiftlist_default_morning_value', 'value' => '0700']);
        Setting::create(['user_id' => $user3->id, 'key' => 'shiftlist_default_evening_value', 'value' => '1700']);

        $user4 = factory(App\Models\User::class)->create();
        Setting::create(['user_id' => $user4->id, 'key' => 'shiftlist_default_morning_value', 'value' => '0800']);
        Setting::create(['user_id' => $user4->id, 'key' => 'shiftlist_default_evening_value', 'value' => '1800']);

        $user5 = factory(App\Models\User::class)->create();
        Setting::create(['user_id' => $user5->id, 'key' => 'shiftlist_default_morning_value', 'value' => '0700']);
        Setting::create(['user_id' => $user5->id, 'key' => 'shiftlist_default_evening_value', 'value' => '1800']);

        $user_repo = new UserRepository();
        $users = $user_repo->getUsersWithNonDefaultShiftlistEntries();

        $expected = collect([
            $user3,
            $user4,
            $user5
        ]);

        $this->assertEquals($expected->pluck('id'), $users->pluck('id'));
    }
}