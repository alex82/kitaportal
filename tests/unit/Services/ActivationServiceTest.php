<?php

use Tests\TestCase;
use App\Services\ActivationService;
use App\Repositories\ActivationRepository;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ActivationServiceTest extends TestCase
{
    use MailTracking;

    public function test_activation_mail_not_send_as_user_is_activated()
    {
        $user = factory(App\Models\User::class)->create([
            'activated' => true
        ]);
        
        $activationService = $this->getServiceToTest();
        $activationService->sendActivationMail($user);

        $this->seeEmailWasNotSent();
    }

    public function test_activation_mail_not_send_as_last_mail_is_to_close()
    {
        $user = factory(App\Models\User::class)->create([
            'activated' => false
        ]);

        $user->activation()->create([
            'token' => 'foobar',
            'user_id' => $user->id,
            'created_at' => date("Y-m-d H:00:00")
        ]);

        $activationService = $this->getServiceToTest();

        $activationService->sendActivationMail($user);
        $this->seeEmailWasNotSent();
    }

    public function test_user_gets_activated()
    {
        $user = factory(App\Models\User::class)->create([
            'activated' => false
        ]);

        $user->activation()->create([
            'token' => 'foobar',
            'user_id' => $user->id,
            'created_at' => date("Y-m-d H:00:00")
        ]);

        $activationService = $this->getServiceToTest();
        $activationService->activateUser('foobar');

        $this->assertDatabaseHas('users', [
            'id' => $user->id,
            'activated' => true
        ]);

        $this->assertDatabaseMissing('user_activations', [
            'token' => 'foobar',
            'user_id' => $user->id
        ]);
    }

    private function getServiceToTest()
    {
        return new ActivationService(new ActivationRepository());
    }
}