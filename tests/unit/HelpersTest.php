<?php

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class HelpersTest extends TestCase
{
    public function test_setting_can_be_loaded()
    {
        $user = factory(App\Models\User::class)->create();
        $this->actingAs($user);

        $user->settings()->create([
            'key' => 'foo',
            'value' => 'bar'
        ]);

        $this->assertEquals('bar', settings('foo'));
    }

    public function test_setting_returns_default_value()
    {
        $user = factory(App\Models\User::class)->create();
        $this->actingAs($user);

        $this->assertEquals('0800', settings(\App\Models\Setting::KEY_SHIFTLIST_DEFAULT_MORNING_VALUE));
    }

    public function test_setting_returns_overwritten_value()
    {
        $user = factory(App\Models\User::class)->create();
        $this->actingAs($user);

        $user->settings()->create([
            'key' => \App\Models\Setting::KEY_SHIFTLIST_DEFAULT_MORNING_VALUE,
            'value' => '0600'
        ]);

        $this->assertEquals('0600', settings(\App\Models\Setting::KEY_SHIFTLIST_DEFAULT_MORNING_VALUE));
    }

    public function test_undefined_setting_returns_null()
    {
        $user = factory(App\Models\User::class)->create();
        $this->actingAs($user);

        $this->assertNull(settings('foo'));
    }
}