<?php

use Carbon\Carbon;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Models\ClosureTime;
use App\Helpers\CalendarHelper;
use Tests\TestCase;

class CalendarHelperTest extends TestCase
{
    public function test_dayIsValidForChangeInShiftlist_daysInThePast()
    {
        $this->assertFalse(CalendarHelper::dayIsValidForChangeInShiftlist(Carbon::now()));
        $this->assertFalse(CalendarHelper::dayIsValidForChangeInShiftlist(Carbon::now()->subDay(1)));
        $this->assertFalse(CalendarHelper::dayIsValidForChangeInShiftlist(Carbon::now()->subDay(2)));
    }

    public  function test_dayIsValidForChangeInShiftlist_daysAreOnWeekend()
    {
        $this->assertFalse(CalendarHelper::dayIsValidForChangeInShiftlist(new Carbon('next saturday')));
        $this->assertFalse(CalendarHelper::dayIsValidForChangeInShiftlist(new Carbon('next sunday')));
    }

    public function test_dayIsValidForChangeInShiftlist_nowIsAfter6Pm()
    {
        $now_with_6pm = Carbon::create(null, null, null, 19);
        Carbon::setTestNow($now_with_6pm);
        $this->assertFalse(CalendarHelper::dayIsValidForChangeInShiftlist(
            Carbon::now()->addDay(1)
        ));

        // clear Carbon now!
        Carbon::setTestNow();
    }

    public function test_dayIsValidForChangeInShiftlist_nowIsFriday()
    {
        $now_is_friday = new Carbon('next friday');
        $now_is_friday->hour = 10;
        Carbon::setTestNow($now_is_friday);

        $this->assertTrue(CalendarHelper::dayIsValidForChangeInShiftlist(Carbon::now()->addDay(3)), 'now is before 6pm');
        $this->assertTrue(CalendarHelper::dayIsValidForChangeInShiftlist(Carbon::now()->addDay(4)), 'now is before 6pm but checking tuesday');

        $now_is_friday = new Carbon('next friday');
        $now_is_friday->hour = 18;
        Carbon::setTestNow($now_is_friday);

        $this->assertFalse(CalendarHelper::dayIsValidForChangeInShiftlist(Carbon::now()->addDay(3)), 'now is after 6pm');
        $this->assertTrue(CalendarHelper::dayIsValidForChangeInShiftlist(Carbon::now()->addDay(4)), 'now is after 6pm but checking tuesday');

        // clear Carbon now!
        Carbon::setTestNow();
    }

    public function test_dayIsValidForChangeInShiftlist_nowIsSaturday()
    {
        $now_is_saturday = new Carbon('next saturday');
        $now_is_saturday->hour = 10;
        Carbon::setTestNow($now_is_saturday);

        $this->assertFalse(CalendarHelper::dayIsValidForChangeInShiftlist(Carbon::now()->addDay(2)), 'now is before 6pm');
        $this->assertTrue(CalendarHelper::dayIsValidForChangeInShiftlist(Carbon::now()->addDay(3)), 'now is before 6pm but checking tuesday');

        $now_is_saturday = new Carbon('next saturday');
        $now_is_saturday->hour = 18;
        Carbon::setTestNow($now_is_saturday);

        $this->assertFalse(CalendarHelper::dayIsValidForChangeInShiftlist(Carbon::now()->addDay(2)), 'now is after 6pm');
        $this->assertTrue(CalendarHelper::dayIsValidForChangeInShiftlist(Carbon::now()->addDay(3)), 'now is after 6pm but checking tuesday');

        // clear Carbon now!
        Carbon::setTestNow();
    }

    public function test_dayIsValidForChangeInShiftlist_nowIsSunday()
    {
        $now_is_sunday = new Carbon('next sunday');
        $now_is_sunday->hour = 10;
        Carbon::setTestNow($now_is_sunday);

        $this->assertFalse(CalendarHelper::dayIsValidForChangeInShiftlist(Carbon::now()->addDay(1)), 'now is before 6pm');
        $this->assertTrue(CalendarHelper::dayIsValidForChangeInShiftlist(Carbon::now()->addDay(2)), 'now is before 6pm but checking tuesday');

        $now_is_sunday = new Carbon('next sunday');
        $now_is_sunday->hour = 18;
        Carbon::setTestNow($now_is_sunday);

        $this->assertFalse(CalendarHelper::dayIsValidForChangeInShiftlist(Carbon::now()->addDay(1)), 'now is after 6pm');
        $this->assertTrue(CalendarHelper::dayIsValidForChangeInShiftlist(Carbon::now()->addDay(2)), 'now is after 6pm but checking tuesday');

        // clear Carbon now!
        Carbon::setTestNow();
    }

    public function test_isWeekend()
    {
        $this->assertFalse(CalendarHelper::isWeekend(Carbon::create(2016, 11, 25)));

        $this->assertTrue(CalendarHelper::isWeekend(Carbon::create(2016, 11, 26)));

        $this->assertTrue(CalendarHelper::isWeekend(Carbon::create(2016, 11, 27)));

        $this->assertFalse(CalendarHelper::isWeekend(Carbon::create(2016, 11, 28)));
    }

    public function test_dayIsPublicHoliday()
    {
        // Neujahrstag
        $this->assertTrue(CalendarHelper::dayIsPublicHoliday(Carbon::create(2016, 1, 1)));
        $this->assertFalse(CalendarHelper::dayIsPublicHoliday(Carbon::create(2016, 1, 2)));

        // Tag der Arbeit
        $this->assertTrue(CalendarHelper::dayIsPublicHoliday(Carbon::create(2016, 5, 1)));
        $this->assertFalse(CalendarHelper::dayIsPublicHoliday(Carbon::create(2016, 5, 2)));

        // Tag der Deutschen Einheit
        $this->assertTrue(CalendarHelper::dayIsPublicHoliday(Carbon::create(2016, 10, 3)));
        $this->assertFalse(CalendarHelper::dayIsPublicHoliday(Carbon::create(2016, 10, 4)));

        // Erster Weihnachtstag
        $this->assertTrue(CalendarHelper::dayIsPublicHoliday(Carbon::create(2016, 12, 25)));
        $this->assertFalse(CalendarHelper::dayIsPublicHoliday(Carbon::create(2016, 12, 24)));

        // Zweiter Weihnachtstag
        $this->assertTrue(CalendarHelper::dayIsPublicHoliday(Carbon::create(2016, 12, 26)));
        $this->assertFalse(CalendarHelper::dayIsPublicHoliday(Carbon::create(2016, 12, 27)));

        // Karfreitag
        $this->assertTrue(CalendarHelper::dayIsPublicHoliday(Carbon::create(2016, 3, 25)));
        $this->assertFalse(CalendarHelper::dayIsPublicHoliday(Carbon::create(2016, 3, 26)));

        // Ostermontag
        $this->assertTrue(CalendarHelper::dayIsPublicHoliday(Carbon::create(2016, 3, 28)));
        $this->assertFalse(CalendarHelper::dayIsPublicHoliday(Carbon::create(2016, 3, 29)));

        // Himmelfahrt
        $this->assertTrue(CalendarHelper::dayIsPublicHoliday(Carbon::create(2016, 5, 5)));
        $this->assertFalse(CalendarHelper::dayIsPublicHoliday(Carbon::create(2016, 5, 6)));

        // Pfingstmontag
        $this->assertTrue(CalendarHelper::dayIsPublicHoliday(Carbon::create(2016, 5, 16)));
        $this->assertFalse(CalendarHelper::dayIsPublicHoliday(Carbon::create(2016, 5, 17)));

        // Buß- und Bettag
        $this->assertTrue(CalendarHelper::dayIsPublicHoliday(Carbon::create(2016, 11, 16)));
        $this->assertFalse(CalendarHelper::dayIsPublicHoliday(Carbon::create(2016, 11, 17)));
    }

    public function test_dayIsClosureTime()
    {
        $closure_time = factory(ClosureTime::class)->states(ClosureTime::TYPE_FULL_DAY_CLOSED)->create();

        $this->assertTrue(CalendarHelper::dayIsClosureTime($closure_time->from));
        $this->assertFalse(CalendarHelper::dayIsClosureTime($closure_time->from->subDay()));
    }

    public function test_afternoonIsClosureTime()
    {
        $closure_time = factory(ClosureTime::class)->states(ClosureTime::TYPE_MORNING_CLOSED)->create();

        $this->assertTrue(CalendarHelper::afternoonIsClosureTime($closure_time->from));
        $this->assertFalse(CalendarHelper::afternoonIsClosureTime($closure_time->from->subDay()));
    }
    
    public function test_getClosureTimes()
    {
        $closure_time = factory(ClosureTime::class)->states(ClosureTime::TYPE_FULL_DAY_CLOSED)->create();

        $this->assertDatabaseHas('closure_times', [
            'to' => $closure_time->to,
            'from' => $closure_time->from,
            'type' => $closure_time->type
        ]);

        $closure_time = factory(ClosureTime::class)->states(ClosureTime::TYPE_MORNING_CLOSED)->create();

        $this->assertDatabaseHas('closure_times', [
            'to' => $closure_time->to,
            'from' => $closure_time->from,
            'type' => $closure_time->type
        ]);
        
        ClosureTime::all()->each(function ($closure_time) {
            $closure_time->delete();
        });

        factory(ClosureTime::class)->states(ClosureTime::TYPE_FULL_DAY_CLOSED)->create([
            'from' => '2016-11-20',
            'to' => '2016-11-25'
        ]);
        factory(ClosureTime::class)->states(ClosureTime::TYPE_FULL_DAY_CLOSED)->create([
            'from' => '2016-11-28',
            'to' => null
        ]);

        $this->assertEquals([
            '2016-11-20',
            '2016-11-21',
            '2016-11-22',
            '2016-11-23',
            '2016-11-24',
            '2016-11-25',
            '2016-11-28',
        ], CalendarHelper::getClosureTimes('2016', ClosureTime::TYPE_FULL_DAY_CLOSED)->toArray());
    }
}