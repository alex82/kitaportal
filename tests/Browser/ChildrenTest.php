<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class ChildrenTest extends DuskTestCase
{
    public function testLogin()
    {
        $user = $this->getUserByName('parent');

        $this->browse(function ($browser) use ($user) {
            /* @var \Laravel\Dusk\Browser $browser */
            $browser->loginAs($user)
                ->visit('/news')
                ->assertSee('Test Environment');
        });
    }

    public function testParentCanAddNewChild()
    {
        $this->browse(function ($browser) {
            /* @var \Laravel\Dusk\Browser $browser */
            $browser
                ->visit('/children/create')
                ->type('#firstname', 'firstnameChild')
                ->type('#lastname', 'lastnameChild')
                ->select('#group', '1')
                ->press('Speichern')
                ->assertSee('firstnameChild')
                ->assertSee('lastnameChild');
        });
    }

    public function testParentCanEditAChild()
    {
        $child = $this->getFirstChildren();

        $this->browse(function ($browser) use ($child) {
            /* @var \Laravel\Dusk\Browser $browser */
            $browser
                ->visit('/children/'.$child->id.'/edit')
                ->type('#firstname', 'firstnameChildEdited')
                ->type('#lastname', 'lastnameChildEdited')
                ->select('#group', '2')
                ->press('Speichern')
                ->assertSee('firstnameChildEdited')
                ->assertSee('lastnameChildEdited');
        });
    }

    public function testParentCanDeleteAChild()
    {
        $child = $this->getFirstChildren();

        $this->browse(function ($browser) use ($child) {
            /* @var \Laravel\Dusk\Browser $browser */
            $browser
                ->visit('/children')
                ->press('button[type="button"][data-id="'.$child->id.'"]')
                ->press('Ja, löschen')
                ->assertDontSee($child->firstname)
                ->assertDontSee($child->lastname);
        });
    }

    private function getFirstChildren()
    {
        return $this->getUserByName('parent')->children()->first();
    }
}
