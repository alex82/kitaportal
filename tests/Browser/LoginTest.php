<?php

namespace Tests\Browser;

use App\Models\User;
use Tests\DuskTestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class LoginTest extends DuskTestCase
{

    /**
     * @var User
     */
    protected $newUser;

    /**
     * @var User
     */
    protected $confirmedUser;

    /**
     * @var User
     */
    protected $activatedUser;

    public function setUp()
    {
        parent::setUp();

        $faker = \Faker\Factory::create();

        $email = $faker->unique()->safeEmail;
        User::on('mysql_testing_remote')->insert([
            'name' => $faker->firstName.' '.$faker->lastName,
            'password' => bcrypt('secret'),
            'email' => $email,
            'remember_token' => str_random(10),
            'confirmed' => false,
            'activated' => false
        ]);
        $this->newUser = User::on('mysql_testing_remote')->where('email', $email)->firstOrFail();

        $email = $faker->unique()->safeEmail;
        User::on('mysql_testing_remote')->insert([
            'name' => $faker->firstName.' '.$faker->lastName,
            'password' => bcrypt('secret'),
            'email' => $email,
            'remember_token' => str_random(10),
            'confirmed' => false,
            'activated' => true
        ]);
        $this->confirmedUser = User::on('mysql_testing_remote')->where('email', $email)->firstOrFail();

        $email = $faker->unique()->safeEmail;
        User::on('mysql_testing_remote')->insert([
            'name' => $faker->firstName.' '.$faker->lastName,
            'password' => bcrypt('secret'),
            'email' => $email,
            'remember_token' => str_random(10),
            'confirmed' => true,
            'activated' => true
        ]);
        $this->activatedUser = User::on('mysql_testing_remote')->where('email', $email)->firstOrFail();
    }

    /**
     * @return void
     */
    public function testNewUserCanNotLogIn()
    {
        $user = $this->newUser;

        $this->browse(function ($browser) use ($user) {
            $browser->visit('/login')
                ->assertSee('Test Environment')
                ->type('email', $user->email)
                ->type('password', 'secret')
                ->press('Login')
                ->assertSee('Sie müssen Ihren Account erst bestätigen');
        });
    }

    /**
     * @return void
     */
    public function testActivatedUserCanNotLogIn()
    {
        $user = $this->confirmedUser;

        $this->browse(function ($browser) use ($user) {
            $browser->visit('/login')
                ->type('email', $user->email)
                ->type('password', 'secret')
                ->press('Login')
                ->assertSee('Ihr Account wurde durch die Kita-Leitung noch nicht freigeschaltet');
        });
    }

    /**
     * @return void
     */
    public function testActivatedAndConfirmedUserCanLogIn()
    {
        $user = $this->activatedUser;

        $this->browse(function ($browser) use ($user) {
            $browser->visit('/login')
                ->type('email', $user->email)
                ->type('password', 'secret')
                ->press('Login')
                ->assertSee('Neuigkeiten');
        });
    }
}
