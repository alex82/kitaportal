<?php

namespace Tests\Browser;

use App\Models\User;
use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class ParentManager extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function testParentManager()
    {
        $parent = $this->getUserByEmail('ParentForParentManagerTest@example.com');

        $this->browse(function ($browser) use ($parent) {
            /* @var Browser $browser */
            /* @var User $parent */
            $browser->visit('/login')
                ->assertSee('Test Environment')
                ->type('email', 'manager@example.com')
                ->type('password', 'secret')
                ->press('Login')
                ->assertSee('Neuigkeiten');

            $browser->visit('/parent')
                ->assertSee('Eltern')
                ->assertSee($parent->name);
            $activationStatus = $browser->text("//tr[contains(.,'".$parent->email."')]/td[6]");
            $this->assertEquals('Nein', $activationStatus);

            $browser->visit('/parent/'.$parent->id.'/edit')
                ->type('name', $parent->name.'Edited')
                ->press('#activated_switch')
                ->press('Speichern')
                ->assertSee('User erfolgreich geändert');

            $activationStatus = $browser->text("//tr[contains(.,'".$parent->email."')]/td[6]");
            $this->assertEquals('Ja', $activationStatus);
        });
    }
}
