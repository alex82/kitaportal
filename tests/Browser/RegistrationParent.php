<?php

namespace Tests\Browser;

use App\Models\User;
use Tests\DuskTestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class RegistrationParent extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function testRegistrationOfParent()
    {
        $faker = \Faker\Factory::create();
        $email = $faker->unique()->safeEmail;

        $this->browse(function ($browser) use ($email) {
            /* @var \Laravel\Dusk\Browser $browser */
            $browser->visit('/register')
                ->assertSee('Test Environment')
                ->type('name', 'Tester')
                ->type('email', $email)
                ->type('password', 'password')
                ->type('password_confirmation', 'password')
                ->type('input[name="child[0][firstname]"]', 'Vorname')
                ->type('input[name="child[0][lastname]"]', 'Nachname')
                ->select('select[name="child[0][group]"]', '1')
                ->press('Registrieren')
                ->assertSee('Vielen Dank für Ihre Registrierung');
        });

        $newUser = $this->getUserByEmail($email);
        $this->assertInstanceOf(User::class, $newUser);
    }
}
