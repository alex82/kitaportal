<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class ManagerShiftlist extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function testManagerShiftlist()
    {
        $this->browse(function ($browser) {
            /* @var Browser $browser */
            $browser->visit('/login')
                ->assertSee('Test Environment')
                ->type('email', 'manager@example.com')
                ->type('password', 'secret')
                ->press('Login')
                ->assertSee('Neuigkeiten');

            $browser->visit('/shiftlist/view/2016-10-05')
                ->assertSee('Mittwoch, 05.Oktober 2016')
                ->assertSee('06:00 Uhr - 06:30 Uhr')
                ->assertSee('06:30 Uhr - 07:00 Uhr')
                ->assertSee('07:00 Uhr - 07:30 Uhr')
                ->assertSee('07:30 Uhr - 08:00 Uhr')
                ->assertSee('normaler KiTa-Alltag')
                ->assertSee('17:00 Uhr - 18:00 Uhr');

            $browser->press('a#next-day')
                ->assertSee('Donnerstag, 06.Oktober 2016');

            $browser->press('a#prev-day')
                ->press('a#prev-day')
                ->assertSee('Dienstag, 04.Oktober 2016');

            $browser->visit('/shiftlist/view')
                ->assertSee(strftime('%A, %d.%B %Y'));
        });
    }
}
