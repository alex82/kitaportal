<?php

namespace Tests\Browser;

use App\Models\User;
use Tests\DuskTestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class RegistrationNursery extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function testRegistrationOfNursery()
    {
        $faker = \Faker\Factory::create();
        $email = $faker->unique()->safeEmail;
        $name = $faker->name;

        $this->browse(function ($browser) use ($email, $name) {
            /* @var \Laravel\Dusk\Browser $browser */
            $browser->visit('/register')
                ->assertSee('Test Environment')
                ->type('name', $name)
                ->type('email', $email)
                ->type('password', 'password')
                ->type('password_confirmation', 'password')
                ->check('#nokids')
                ->press('Registrieren')
                ->assertSee('Vielen Dank für Ihre Registrierung');
        });

        $newUser = $this->getUserByEmail($email);
        $this->assertInstanceOf(User::class, $newUser);
    }
}
