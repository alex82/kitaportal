<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class ParentShiftlist extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function testExample()
    {
        $this->browse(function ($browser) {
            /* @var Browser $browser */
            $browser->visit('/login')
                ->assertSee('Test Environment')
                ->type('email', 'parent@example.com')
                ->type('password', 'secret')
                ->press('Login')
                ->assertSee('Neuigkeiten');

            $browser->visit('/shiftlist/2016/10')
                ->assertSee('Oktober 2016');

            $this->assertEquals('Sa 01.10.', $browser->text("//table[@id='shiftlist']/tbody/tr[1]/td[1]"));
            $this->assertEquals('Wochenende', $browser->text("//table[@id='shiftlist']/tbody/tr[1]/td[2]"));

            $this->assertEquals('So 02.10.', $browser->text("//table[@id='shiftlist']/tbody/tr[2]/td[1]"));
            $this->assertEquals('Wochenende', $browser->text("//table[@id='shiftlist']/tbody/tr[2]/td[2]"));

            $this->assertEquals('Mo 03.10.', $browser->text("//table[@id='shiftlist']/tbody/tr[3]/td[1]"));
            $this->assertEquals('Feiertag', $browser->text("//table[@id='shiftlist']/tbody/tr[3]/td[2]"));

            $browser->press('a#next-month')
                ->assertSee('November 2016');

            $browser->press('a#prev-month')
                ->press('a#prev-month')
                ->assertSee('September 2016');

            $browser->visit('/shiftlist')
                ->assertSee(strftime('%B %Y'));

            $next_month = new \Carbon\Carbon('next month');
            $browser->visit('/shiftlist/' . $next_month->format('Y/m'))
                ->select("/descendant::select[contains(concat(' ', normalize-space(@class), ' '), ' shiftlist-select ')][1]", '0630')
                ->pause(1)
                ->select("/descendant::select[contains(concat(' ', normalize-space(@class), ' '), ' shiftlist-select ')][2]", '1800')
                ->pause(1)
                ->waitUntil('return $.active == 0;', 5);

            $browser->visit('/shiftlist/' . $next_month->format('Y/m'));
            $this->assertEquals('0630', $browser->value("/descendant::select[contains(concat(' ', normalize-space(@class), ' '), ' shiftlist-select ')][1]"));
            $this->assertEquals('1800', $browser->value("/descendant::select[contains(concat(' ', normalize-space(@class), ' '), ' shiftlist-select ')][2]"));
        });
    }
}
