<?php

namespace Tests\Browser;

use App\Models\Comment;
use App\Models\User;
use DB;
use Config;
use App\Models\Post;
use Faker\Factory;
use Tests\DuskTestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class NewsTest extends DuskTestCase
{

    public function testLogin()
    {
        $user = $this->getUserByName('admin');

        $this->browse(function ($browser) use ($user) {
            /* @var \Laravel\Dusk\Browser $browser */
            $browser->loginAs($user)
                ->visit('/news')
                ->waitForText('Test Environment');
        });
    }

    public function testNewsShownAfterLogin()
    {
        $this->browse(function ($browser) {
            /* @var \Laravel\Dusk\Browser $browser */
            $browser
                ->visit('/news')
                ->assertSee('Test Environment')
                ->assertSee('My Test Post')
                ->assertSee('Kommentare');
        });
    }

    public function testUserCanCommentAPost()
    {
        $this->browse(function ($browser) {
            /* @var \Laravel\Dusk\Browser $browser */
            $browser
                ->visit('/news/1')
                ->assertSee('Test Environment')
                ->type('comment', 'this is my test comment')
                ->press('Speichern')
                ->assertSee('Vielen Dank für Ihren Kommentar');

            Comment::on('mysql_testing_remote')->where('text', 'this is my test comment')->get()->each(function ($comment) {
                $comment->approved = 1;
                $comment->save();
            });

            $browser->visit('/news/1')->assertSee('this is my test comment');
        });
    }

    public function testUserCanCommentAPostButItsNotVisible()
    {
        $this->browse(function ($browser) {
            /* @var \Laravel\Dusk\Browser $browser */
            $browser
                ->visit('/news/1')
                ->assertSee('Test Environment')
                ->type('comment', 'this is my not approved test comment')
                ->press('Speichern')
                ->assertSee('Vielen Dank für Ihren Kommentar');

            $browser->assertDontSee('this is my not approved test comment');
        });
    }

    public function testUserCanNotAddEmptyComment()
    {
        $this->browse(function ($browser) {
            /* @var \Laravel\Dusk\Browser $browser */
            $browser
                ->visit('/news/1')
                ->assertSee('Test Environment')
                ->press('Speichern')
                ->assertSee('Bitte geben Sie einen Text ein');
        });
    }

    public function testUserCanCreateANewPost()
    {
        $this->browse(function ($browser) {
            /* @var \Laravel\Dusk\Browser $browser */
            $browser
                ->visit('/news')
                ->assertSee('Test Environment')
                ->click('#add_news')
                ->type('#title', 'New title')
                ->type('.jodit_editor', 'New text')
                ->press('Speichern')
                ->assertSee('Beitrag erfolgreich gespeichert')
                ->assertSee('New title')
                ->assertSee('New text');
        });
    }

    public function testUserCanNotCreateANewPostWithoutTitle()
    {
        $this->browse(function ($browser) {
            /* @var \Laravel\Dusk\Browser $browser */
            $browser
                ->visit('/news')
                ->assertSee('Test Environment')
                ->click('#add_news')
                ->type('.jodit_editor', 'New text')
                ->press('Speichern')
                ->assertSee('Bitte geben Sie einen Titel ein');
        });
    }

    public function testUserCanNotCreateANewPostWithTooLongTitle()
    {
        $this->browse(function ($browser) {
            /* @var \Laravel\Dusk\Browser $browser */
            $browser
                ->visit('/news')
                ->assertSee('Test Environment')
                ->click('#add_news')
                ->type('#title', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem.')
                ->type('.jodit_editor', 'New text')
                ->press('Speichern')
                ->assertSee('Der Titel darf nicht mehr als 255 Zeichen haben');
        });
    }

    public function testUserCanNotCreateANewPostWithoutText()
    {
        $this->browse(function ($browser) {
            /* @var \Laravel\Dusk\Browser $browser */
            $browser
                ->visit('/news')
                ->assertSee('Test Environment')
                ->click('#add_news')
                ->type('#title', 'New title')
                ->press('Speichern')
                ->assertSee('Bitte geben Sie einen Text ein');
        });
    }

    public function testUserCanNotCreateANewPostWithoutAnyContent()
    {
        $this->browse(function ($browser) {
            /* @var \Laravel\Dusk\Browser $browser */
            $browser
                ->visit('/news')
                ->assertSee('Test Environment')
                ->click('#add_news')
                ->press('Speichern')
                ->assertSee('Bitte geben Sie einen Titel ein')
                ->assertSee('Bitte geben Sie einen Text ein');
        });
    }
}
