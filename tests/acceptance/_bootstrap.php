<?php
exec('php artisan migrate:reset --database=mysql_testing');
exec('php artisan migrate --database=mysql_testing');
exec('php artisan db:seed --database=mysql_testing');

require __DIR__.'/../../bootstrap/autoload.php';
$app = require __DIR__.'/../../bootstrap/app.php';
$app->instance('request', new \Illuminate\Http\Request);
$app->make('Illuminate\Contracts\Http\Kernel')->bootstrap();

Session::put('selected_database', 'mysql_testing');
Config::set('database.default', 'mysql_testing');