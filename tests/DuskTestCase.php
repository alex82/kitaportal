<?php

namespace Tests;

setlocale(LC_TIME, 'de_DE.utf8', 'de');

use Laravel\Dusk\TestCase as BaseTestCase;
use Facebook\WebDriver\Remote\RemoteWebDriver;
use Facebook\WebDriver\Remote\DesiredCapabilities;

abstract class DuskTestCase extends BaseTestCase
{
    use CreatesApplication;

    /**
     * Prepare for Dusk test execution.
     *
     * @beforeClass
     * @return void
     */
    public static function prepare()
    {
        //static::startChromeDriver();
    }

    /**
     * Create the RemoteWebDriver instance.
     *
     * @return \Facebook\WebDriver\Remote\RemoteWebDriver
     */
    protected function driver()
    {
        return RemoteWebDriver::create(
            'http://10.0.2.2:9515', DesiredCapabilities::chrome()
        );
    }

    /**
     * @param $name
     *
     * @return \Illuminate\Database\Eloquent\Model|static
     */
    protected function getUserByName($name)
    {
        return \App\Models\User::on('mysql_testing_remote')->where('name', $name)->firstOrFail();
    }

    /**
     * @param $email
     *
     * @return \Illuminate\Database\Eloquent\Model|static
     */
    protected function getUserByEmail($email)
    {
        return \App\Models\User::on('mysql_testing_remote')->where('email', $email)->firstOrFail();
    }
}
