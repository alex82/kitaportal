<?php

use Tests\TestCase;
use App\Models\User;
use App\Models\Role;
use App\Models\NurseryGroup;
use App\Models\NurseryBuilding;
use App\Repositories\PostRepository;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class PostRepositoryTest extends TestCase
{
    public function test_adminCanSeeAllPosts()
    {
        $this->tryUserCanSeeAllPosts('admin');
    }

    public function test_managerCanSeeAllPosts()
    {
        $this->tryUserCanSeeAllPosts('manager');
    }

    public function test_nurseryCanSeeAllPosts()
    {
        $this->tryUserCanSeeAllPosts('nursery');
    }

    public function test_parentCanSeeDefaultPosts()
    {
        $user = factory(App\Models\User::class)->create();

        Auth::shouldReceive('user')
            ->andReturn($user);

        $defaultPost = $this->getDefaultPost();
        $this->assertTrue(PostRepository::postIsVisibleForCurrentUser($defaultPost));
    }

    public function test_parentCanNotSeeLimitedPosts()
    {
        $user = factory(App\Models\User::class)->create();

        Auth::shouldReceive('user')
            ->andReturn($user);

        $post = $this->getPostLimitedToGroup();
        $this->assertFalse(PostRepository::postIsVisibleForCurrentUser($post));
    }

    public function test_parentCanSeePostForOwnGroup()
    {
        $child = factory(App\Models\Child::class)->create();
        $user = $child->user;
        $user->attachRole(Role::where('name', 'parent')->first());

        Auth::shouldReceive('user')
            ->andReturn($child->user);

        $post = $this->getPostLimitedToGroup($child->nursery_group);
        $this->assertTrue(PostRepository::postIsVisibleForCurrentUser($post));
    }

    public function test_parentCanNotSeePostForOtherGroup()
    {
        $child = factory(App\Models\Child::class)->create();
        $user = $child->user;
        $user->attachRole(Role::where('name', 'parent')->first());

        Auth::shouldReceive('user')
            ->andReturn($child->user);

        $group = factory(NurseryGroup::class)->create();
        $post = $this->getPostLimitedToGroup($group);
        $this->assertFalse(PostRepository::postIsVisibleForCurrentUser($post));
    }

    public function test_parentCanSeePostForOwnBuilding()
    {
        $child = factory(App\Models\Child::class)->create();
        $user = $child->user;
        $user->attachRole(Role::where('name', 'parent')->first());

        Auth::shouldReceive('user')
            ->andReturn($child->user);

        $post = $this->getPostLimitedToBuilding($child->nursery_group->building);
        $this->assertTrue(PostRepository::postIsVisibleForCurrentUser($post));
    }

    public function test_parentCanSeePostForOtherGroupButOwnBuilding()
    {
        $child = factory(App\Models\Child::class)->create();
        $user = $child->user;
        $user->attachRole(Role::where('name', 'parent')->first());

        Auth::shouldReceive('user')
            ->andReturn($child->user);

        $post = $this->getPostLimitedToBuilding($child->nursery_group->building);
        $group = factory(NurseryGroup::class)->create();
        $post->visibleForGroups()->attach($group);
        $this->assertTrue(PostRepository::postIsVisibleForCurrentUser($post));
    }

    public function test_parentCanSeePostForOwnGroupButOtherBuilding()
    {
        $child = factory(App\Models\Child::class)->create();
        $user = $child->user;
        $user->attachRole(Role::where('name', 'parent')->first());

        Auth::shouldReceive('user')
            ->andReturn($child->user);

        $post = $this->getPostLimitedToGroup($child->nursery_group);
        $building = factory(NurseryBuilding::class)->create();
        $post->visibleForBuildings()->attach($building);
        $this->assertTrue(PostRepository::postIsVisibleForCurrentUser($post));
    }

    ####################################################################################

    private function tryUserCanSeeAllPosts($username)
    {
        $user = User::where('name', $username)->first();

        Auth::shouldReceive('user')
            ->andReturn($user);

        $defaultPost = $this->getDefaultPost();
        $this->assertTrue(PostRepository::postIsVisibleForCurrentUser($defaultPost));

        $this->assertTrue(PostRepository::postIsVisibleForCurrentUser($this->getPostLimitedToGroup()));
    }

    private function getDefaultPost()
    {
        return factory(App\Models\Post::class)->create();
    }

    private function getPostLimitedToGroup($group = null)
    {
        $limitedPost = $this->getDefaultPost();
        $group = ($group instanceof  NurseryGroup) ? $group : NurseryGroup::first();
        $limitedPost->visibleForGroups()->attach($group);
        $limitedPost->visibleForBuildings()->attach($group->building);

        return $limitedPost;
    }

    private function getPostLimitedToBuilding($bulding = null)
    {
        $limitedPost = $this->getDefaultPost();
        $bulding = ($bulding instanceof  NurseryBuilding) ? $bulding : NurseryBuilding::first();
        $limitedPost->visibleForBuildings()->attach($bulding);

        return $limitedPost;
    }
}