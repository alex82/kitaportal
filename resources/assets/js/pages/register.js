require('../app');

var app = new Vue({
    el: '#app',
    data: {
        children: children,
        form_errors: form_errors,
        nokids: false
    },
    methods: {
        addChild: function () {
            this.children.push({firstname: '', lastname: '', group: 0});
        },
        removeChild: function (index) {
            this.children.splice(index, 1);
        },
        hasError: function (field, index) {
            return (form_errors.default['child.' + index + '.' + field]);
        }
    }
});