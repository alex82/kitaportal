require('../app');

const app = new Vue({
    el: '#app',
    data: {
        select_options_morning: [
            { text: "06:00 - 06:30", value: "0600"},
            { text: "06:30 - 07:00", value: "0630"},
            { text: "07:00 - 07:30", value: "0700"},
            { text: "07:30 - 08:00", value: "0730"},
            { text: "nach 08:00 Uhr", value: "0800", default: true}
        ],
        select_options_evening: [
            { text: "vor 17:00 Uhr", value: "1700", default: true},
            { text: "17:00 - 18:00", value: "1800"}
        ]
    }
});