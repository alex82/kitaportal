Hallo,<br/>
<br/>
ein neuer User hat sich registriert und bestätigt.<br/>
Bitte prüfen, freischalten und ggf. Rolle anpassen:<br/>
<br/>
Name: {{ $user->name }}<br/>
E-Mail: {{ $user->email }}<br/>
<br/>
Hier freischalten: <a href="{{ route('parent.edit', $user) }}">{{ route('parent.edit', $user) }}</a>