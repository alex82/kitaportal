Vielen Dank für Ihre Registrierung.<br/>
<br/>
Um diese abzuschließen rufen Sie bitte folgenden Link auf:<br/>
<br/>
<a href="{{ $link }}" target="_blank">{{ $link }}</a>
<br/>
<br/>
Vielen Dank