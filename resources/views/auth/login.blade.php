@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-lg-6 m-t-2 col-xs-12 m-x-auto pull-xs-none">
            @if (session('warning'))
                <div class="alert alert-warning">
                    {{ session('warning') }}
                </div>
            @endif

            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            <div class="">
                <form class="form-horizontal" role="form" method="POST" action="{{ secure_url('/login') }}">
                    {{ csrf_field() }}
                    <div class="card p-a-2">
                        <div class="card-block">
                            <h1>
                                Login
                                <img src="img/logo.png" height="20" class="pull-right m-t-1" alt="Kita-Portal" />
                            </h1>
                            <p class="text-muted">Hier einloggen</p>
                            <div class="input-group m-b-1 {{ $errors->has('email') ? ' has-danger' : '' }}">
                                <span class="input-group-addon"><i class="icon-user"></i></span>
                                <input type="email" name="email" class="form-control" placeholder="E-Mail" value="{{ old('email') }}" required>
                            </div>
                            <div class="input-group m-b-2 {{ $errors->has('password') ? ' has-danger' : '' }}">
                                <span class="input-group-addon"><i class="icon-lock"></i></span>
                                <input type="password" name="password" class="form-control" placeholder="Password">
                            </div>
                            <div class="row">
                                <div class="col-xs-6">
                                    <button type="submit" class="btn btn-primary p-x-2">Login</button>
                                </div>
                                <div class="col-xs-6 text-xs-right">
                                    <a href="{{ secure_url('/password/reset') }}" class="btn btn-link p-x-0">Passwort vergessen?</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card card-inverse card-primary p-y-3">
                        <div class="card-block text-xs-center">
                            <div>
                                <h2>Neu hier?</h2>
                                <p>
                                    Registrieren Sie sich jetzt kostenlos.<br/>
                                    Aus Sicherheitsgründen werden alle Accounts durch die Kita-Leitung freigeschaltet.
                                </p>
                                <a href="{{ secure_url('/register') }}" class="btn btn-primary active m-t-1">Jetzt registrieren!</a>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection