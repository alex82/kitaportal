@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 m-x-auto pull-xs-none vamiddle">
            <form class="form-horizontal" role="form" method="POST" action="{{ secure_url('/register') }}" novalidate>
                {{ csrf_field() }}
                <h1 class="m-b-2">Registrierung</h1>

                <div class="card">
                    <div class="card-header">
                        Allgemeines
                    </div>
                    <div class="card-block p-a-2">
                        @if (session('success_message'))
                            <div class="alert alert-success">
                                {{ session('success_message') }}
                            </div>
                        @endif

                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        <div class="form-group">
                            <div class="input-group m-b-1{{ $errors->has('name') ? ' has-danger' : '' }}">
                                <span class="input-group-addon"><i class="icon-user"></i></span>
                                <input type="text" name="name" class="form-control" placeholder="Name" value="{{ old('name') }}" required>
                            </div>

                            @if ($errors->has('name'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="form-group">
                            <div class="input-group m-b-1{{ $errors->has('email') ? ' has-danger' : '' }}">
                                <span class="input-group-addon">@</span>
                                <input type="text" class="form-control" name="email" placeholder="E-Mail" value="{{ old('email') }}" required>
                            </div>
                            @if ($errors->has('email'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                            @endif
                        </div>

                        <div class="form-group">
                            <div class="input-group m-b-1{{ $errors->has('password') ? ' has-danger' : '' }}">
                                <span class="input-group-addon"><i class="icon-lock"></i></span>
                                <input type="password" name="password" class="form-control" placeholder="Passwort" required>
                            </div>
                            @if ($errors->has('password'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                            @endif
                        </div>

                        <div class="input-group m-b-2">
                            <span class="input-group-addon"><i class="icon-lock"></i></span>
                            <input type="password" name="password_confirmation" class="form-control" placeholder="Passwort wiederholen">
                        </div>
                    </div>
                </div>

                <div class="card hidden" id="children">
                    <div class="card-header">
                        Kind(er)
                    </div>
                    <div class="card-block p-a-2">
                        <p class="text-muted" v-show="!nokids">Geben Sie hier bitte alle Kinder an, die derzeit in die Kita gehen.</p>

                        <table class="table" v-show="!nokids">
                            <thead>
                                <tr>
                                    <td></td>
                                    <td>Vorname</td>
                                    <td>Nachname</td>
                                    <td>Gruppe</td>
                                    <td></td>
                                </tr>
                            </thead>
                            <tbody>
                                <tr v-for="(child, index) in children">
                                    <td>
                                        <p class="form-control-static">@{{ (index+1) }}.</p>
                                    </td>
                                    <td v-bind:class="{ 'has-error': hasError('firstname', index) }">
                                        <input type="text" class="form-control" v-model="child.firstname" :name="'child[' + index + '][firstname]'" required>
                                    </td>
                                    <td v-bind:class="{ 'has-error': hasError('lastname', index) }">
                                        <input type="text" class="form-control" v-model="child.lastname" :name="'child[' + index + '][lastname]'" required>
                                    </td>
                                    <td v-bind:class="{ 'has-error': hasError('group', index) }">
                                        <select class="form-control nursery-group-select" v-model="child.group" :name="'child[' + index + '][group]'">
                                            <option value="0">Bitte Gruppe auswählen</option>
                                            @foreach($buildings as $building)
                                                <optgroup label="{{ $building->description }}">
                                                    @foreach($building->groups as $group)
                                                        <option value="{{ $group->id }}">{{ $group->description }}</option>
                                                    @endforeach
                                                </optgroup>
                                            @endforeach
                                        </select>
                                    </td>
                                    <td>
                                        <p class="form-control-static" v-if="index > 0">
                                            <a href="#" v-on:click.stop.prevent="removeChild(index)">
                                                <span class="glyphicon glyphicon-remove"></span>
                                            </a>
                                        </p>
                                    </td>
                                </tr>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td></td>
                                    <td colspan="4">
                                        <button type="button" class="btn btn-success btn-xs" v-on:click="addChild()">
                                            <span class="fa fa-plus" aria-hidden="true"></span>
                                            weiteres Kind
                                        </button>
                                    </td>
                                </tr>
                            </tfoot>
                        </table>
                        <div class="form-check">
                            <label class="form-check-label text-danger">
                                <input class="form-check-input" type="checkbox" value="1" name="nokids" id="nokids" v-model="nokids">
                                <strong>Ich gehöre zum Kita-Team!</strong>
                            </label>
                        </div>
                    </div>
                </div>

                <div class="card">
                    <div class="card-block p-a-2">
                        Mit Ihrer Registrierung erklären Sie sich mit unserer <a href="/imprint#dse" target="_blank">Datenschutzerklärung</a> einverstanden.
                    </div>
                </div>

                <button type="submit" class="btn btn-block btn-success">Registrieren</button>
            </form>
        </div>
    </div>
</div>
@endsection

@section('scripts')
    <script>

        function verticalAlignMiddle()
        {
            var bodyHeight = $(window).height();
            var formHeight = $('.vamiddle').height();
            var marginTop = (bodyHeight / 2) - (formHeight / 2);
            if (marginTop > 0)
            {
                $('.vamiddle').css('margin-top', marginTop);
            }
        }
        $(document).ready(function()
        {
            verticalAlignMiddle();
            @if(old('nokids') == 1)
                $('#nokids').click();
            @endif
        });
        $(window).bind('resize', verticalAlignMiddle);

        $('#children').removeClass('hidden');

        var children = {!! json_encode(old('child', [['firstname' => '', 'lastname' => '', 'group' => 0]])) !!};
        var form_errors = {!! $errors->count() ? json_encode($errors->getBags()) : json_encode(['default' => []]) !!};
    </script>
    <script src="/js/register.js"></script>
@endsection