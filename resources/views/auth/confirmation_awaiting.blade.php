@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="alert alert-warning" role="alert">
                    Vielen Dank für Ihre Aktivierung.<br/>
                    Aus Sicherheitsgründen muss jeder Account durch die Kita-Leitung freigeschaltet werden.<br/>
                    Sie werden per E-Mail informiert, sobald dies geschehen ist.
                </div>
            </div>
        </div>
    </div>
@endsection
