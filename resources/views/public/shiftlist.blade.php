@extends('layouts.app')

@section('content')
    <div class="card">
        <div class="card-block">
            <h1 class="h2 text-xs-center">Früh- und Spätdienst</h1>

            <h2 class="h4 text-xs-center m-t-2">
                <a href="{{ route('public_shiftlist', ['day' => $day_to_show->copy()->subDay()->format('Y-m-d')]) }}" class="pull-left" id="prev-day">
                    <span class="fa fa-chevron-left"></span>
                </a>
                {{ $day_to_show->formatLocalized('%A, %d.%B %Y')}}
                <a href="{{ route('public_shiftlist', ['day' => $day_to_show->copy()->addDay()->format('Y-m-d')]) }}" class="pull-right" id="next-day">
                    <span class="fa fa-chevron-right"></span>
                </a>
            </h2>

            <table class="table m-t-2">
                <tbody>
                    @if(\App\Helpers\CalendarHelper::isWeekend($day_to_show))
                        <tr class="table-active">
                            <td class="text-sm-center align-middle p-y-3">
                                <span class="text-muted">Wochenende</span>
                            </td>
                        </tr>
                    @elseif(\App\Helpers\CalendarHelper::dayIsPublicHoliday($day_to_show))
                        <tr class="table-danger">
                            <td class="text-xs-center align-middle p-y-3">
                                <span class="text-danger">Feiertag</span>
                            </td>
                        </tr>

                    @elseif(\App\Helpers\CalendarHelper::dayIsClosureTime($day_to_show))
                        <tr class="table-danger">
                            <td class="text-xs-center align-middle p-y-3">
                                <span class="text-danger">Schließzeit</span>
                            </td>
                        </tr>
                    @else
                        <tr>
                            <th width="200">06:00 Uhr - 06:30 Uhr</th>
                            @include('public.partial.shiftlist_row', ['list' => $morning, 'key' => '0600'])
                        </tr>
                        <tr>
                            <th>06:30 Uhr - 07:00 Uhr</th>
                            @include('public.partial.shiftlist_row', ['list' => $morning, 'key' => '0630'])
                        </tr>
                        <tr>
                            <th>07:00 Uhr - 07:30 Uhr</th>
                            @include('public.partial.shiftlist_row', ['list' => $morning, 'key' => '0700'])
                        </tr>
                        <tr>
                            <th>07:30 Uhr - 08:00 Uhr</th>
                            @include('public.partial.shiftlist_row', ['list' => $morning, 'key' => '0730'])
                        </tr>
                        <tr>
                            <td colspan="3" class="table-info text-xs-center text-muted">normaler KiTa-Alltag</td>
                        </tr>
                        @if(!\App\Helpers\CalendarHelper::afternoonIsClosureTime($day_to_show))
                            <tr>
                                <th>17:00 Uhr - 18:00 Uhr</th>
                                @include('public.partial.shiftlist_row', ['list' => $evening, 'key' => '1800'])
                            </tr>
                        @else
                            <tr class="table-danger">
                                <td class="text-xs-center align-middle p-y-3" colspan="3">
                                    <span class="text-danger">Konzeptionsnachmittag</span>
                                </td>
                            </tr>
                        @endif
                    @endif
                </tbody>
            </table>

        </div>
    </div>
@endsection