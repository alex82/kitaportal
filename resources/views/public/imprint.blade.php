@extends(Auth::check() ? 'layouts.app_admin' : 'layouts.app')

@section('content')
    <div class="card">
        <div class="card-block">
            <h1 class="h2 text-xs-center">Impressum</h1>

            Angaben gemäß § 5 TMG:<br/>
            <br/>
            KINDERVEREINIGUNG Leipzig e. V.<br/>
            Frohburger Straße 33 C<br/>
            04277 Leipzig<br/>
            <br/><br/>
            <h2 class="h4">Vertreten durch:</h2>
            <br/>
            Stefan Schaller (Vorstandsvorsitzender)<br/>
            Matthias Heinz (Geschäftsführer)<br/>
            <br/><br/>
            <h2 class="h4">Kontakt:</h2>
            <br/>
            Telefon: 0341 - 2257440<br/>
            NUR ANRUFBEANTWORTER!<br/>
            <br/>
            Telefax: 0341 - 22574410<br/>
            <br/>
            E-Mail: gs(at)kv-leipzig.de
            <br/>
            E-Mail Webmaster: webmaster(at)kv-leipzig.de<br/>
            <br/><br/>
            <h2 class="h4">Registereintrag:</h2>
            <br/>
            Eintragung im Vereinsregister.<br/>
            Registergericht: Amtsgericht Leipzig<br/>
            Registernummer: 1291<br/>
            <br/><br/>
            <h2 class="h4">Umsatzsteuer-ID:</h2>
            <br/>
            Umsatzsteuer-Identifikationsnummer gemäß §27 a Umsatzsteuergesetz:
            231/140/01023
            <br/>
            <br/>
            Quelle: Impressumgenerator von <a href="http://www.e-recht24.de/" target="_blank">www.e-recht24.de</a>.
            <br/><br/>
            <h2 class="h4">Verbraucherschlichtungsverfahren nach dem Verbraucherstreitbeilegungsgesetz:</h2>

            Zur Teilnahme an einem Streitbeilegungsverfahren vor einer Verbraucherschlichtungsstelle sind wir nicht verpflichtet und nicht bereit.

            <br id="dse"/>
            <br/>
            <br/>

            <h2 class="h4">Datenschutzerklärung</h2>

            <br />
            <div align=“justify“>Wir erheben, verwenden und speichern Ihre personenbezogenen Daten ausschließlich im Rahmen der Bestimmungen des Bundesdatenschutzgesetzes der Bundesrepublik Deutschland. Nachfolgend unterrichten wir Sie über Art, Umfang und Zweck der Datenerhebung und Verwendung.
                <br /><br />
                <b>Erhebung und Verarbeitung von Daten</b>
                <div align=“justify“>Jeder Zugriff auf unsere Internetseite und jeder Abruf einer auf dieser Website hinterlegten Datei werden protokolliert. Die Speicherung dient internen systembezogenen und statistischen Zwecken. Protokolliert werden: Name der abgerufenen Datei, Datum und Uhrzeit des Abrufs, übertragene Datenmenge, Meldung über erfolgreichen Abruf, Webbrowser und anfragende Domain. Zusätzlich werden die IP Adressen der anfragenden Rechner protokolliert. Weitergehende personenbezogene Daten werden nur erfasst, wenn der Nutzer der Website und/oder Kunde Angaben freiwillig, etwa im Rahmen einer Anfrage oder Registrierung oder zum Abschluss eines Vertrages oder über die Einstellungen seines Browsers tätigt.
                    Unsere Internetseite verwendet Cookies. Ein Cookie ist eine Textdatei, die beim Besuch einer Internetseite verschickt und auf der Festplatte des Nutzer der Website und/oder Kunden zwischengespeichert wird. Wird der entsprechende Server unserer Webseite erneut vom Nutzer der Website und/oder Kunden aufgerufen, sendet der Browser des Nutzers der Wesbite und/oder des Kunden den zuvor empfangenen Cookie wieder zurück an den Server. Der Server kann dann die durch diese Prozedur erhaltenen Informationen auf verschiedene Arten auswerten. Durch Cookies können z. B. Werbeeinblendungen gesteuert oder das Navigieren auf einer Internetseite erleichtert werden. Wenn der Nutzer der Website und/oder Kunde die Nutzung von Cookies unterbinden will, kann er dies durch lokale Vornahme der Änderungen seiner Einstellungen in dem auf seinem Computer verwendeten Internetbrowser, also dem Programm zum Öffnen und Anzeigen von Internetseiten (z.B. Internet Explorer, Mozilla Firefox, Opera oder Safari) tun.
                </div><br />
                <b>Nutzung und Weitergabe personenbezogener Daten</b>
                <div align=“justify“>Soweit der Nutzer unserer Webseite personenbezogene Daten zur Verfügung gestellt hat, verwenden wir diese nur zur Beantwortung von Anfragen des Nutzers der Website und/oder Kunden, zur Abwicklung mit dem Nutzer der Website und/oder Kunden geschlossener Verträge und für die technische Administration. Personenbezogene Daten werden von uns an Dritte nur weitergegeben oder sonst übermittelt, wenn dies zum Zwecke der Vertragsabwicklung oder zu Abrechnungszwecken erforderlich ist oder der Nutzer der Website und/oder Kunde zuvor eingewilligt hat. Der Nutzer der Website und/oder Kunde hat das Recht, eine erteilte Einwilligung mit Wirkung für die Zukunft jederzeit zu widerrufen.

                    Die Löschung der gespeicherten personenbezogenen Daten erfolgt, wenn der Nutzer der Website und/oder Kunde die Einwilligung zur Speicherung widerruft, wenn ihre Kenntnis zur Erfüllung des mit der Speicherung verfolgten Zwecks nicht mehr erforderlich ist oder wenn ihre Speicherung aus sonstigen gesetzlichen Gründen unzulässig ist. Daten für Abrechnungszwecke und buchhalterische Zwecke werden von einem Löschungsverlangen nicht berührt. zwischen Ihnen und uns ein Vertragsverhältnis begründet, inhaltlich ausgestaltet oder geändert werden soll, erheben und verwenden wir personenbezogene Daten von Ihnen, soweit dies zu diesen Zwecken erforderlich ist.
                    <br />Auf Anordnung der zuständigen Stellen dürfen wir im Einzelfall Auskunft über diese Daten (Bestandsdaten) erteilen, soweit dies für Zwecke der Strafverfolgung, zur Gefahrenabwehr, zur Erfüllung der gesetzlichen Aufgaben der Verfassungsschutzbehörden oder des Militärischen Abschirmdienstes oder zur Durchsetzung der Rechte am geistigen Eigentum erforderlich ist.
                </div><br />
                <b>Auskunftsrecht</b>
                <div align=“justify“>Auf schriftliche Anfrage informieren wir den Nutzer der Website und/oder den Kunden über die zu seiner Person gespeicherten Daten. Die Anfrage ist an unsere im Impressum der Webseite angegebene Adresse zu richten.
                </div><br />
                <b>Cookies </b>

                <br />
                <i>Quelle: <a href=“http://www.itrecht-hannover.de“>IT Recht Hannover | Rechtsanwalt E. Strohmeyer</a></i>
            </div>

        </div>
    </div>
@endsection