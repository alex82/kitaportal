<td>
    @if($list->has($key))
        @foreach($list[$key] as $building => $items)
            <small>{{ $building }}</small>
            <br/>
            {{ $items->implode(', ') }}
            <br/>
            <br/>
        @endforeach
    @endif
</td>
<td class="text-xs-right">
    @if($list->has($key))
        @foreach($list[$key] as $items)
            + {{ $items->count() }}<br/>
        @endforeach
    @endif
</td>