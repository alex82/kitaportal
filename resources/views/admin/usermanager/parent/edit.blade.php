@extends('layouts.app_admin')

@section('head')
    <style>

    </style>
@endsection

@section('breadcrumb')
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{ route('user.index') }}">Benutzerverwaltung</a>
        </li>
        <li class="breadcrumb-item active">Benutzer bearbeiten</li>
    </ol>
@endsection

@section('content')
    <div class="card">
        <div class="card-block">
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            <form method="post" action="{{ route('parent.update', $user) }}">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <label for="name">Name:</label>
                            <input class="form-control" name="name" id="name" placeholder="" value="{{ old('name', $user->name) }}" type="text">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <label for="email">E-Mail:</label>
                            <input class="form-control" name="email" id="email" placeholder="" value="{{ old('email', $user->email) }}" type="text">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <label for="roles">Rollen:</label>
                            <div class="form-group">
                                @foreach($roles as $role)
                                    <label for="role_{{ $role->id }}" class="checkbox-inline">
                                        <input name="roles[]" id="role_{{ $role->id }}" value="{{ $role->id }}" type="checkbox" {{ (in_array($role->id, old('roles', [])) || $user->hasRole($role->name)) ? 'checked' : '' }}>
                                        {{ $role->display_name }}
                                    </label>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <label for="confirmed">User hat E-Mailadresse bestätigt:</label>
                            <br/>
                            <label class="switch switch-icon switch-pill switch-success" id="confirmed_switch">
                                <input class="switch-input" id="confirmed" name="confirmed" value="1" {{ $user->confirmed ? 'checked' : '' }} type="checkbox">
                                <span class="switch-label" data-on="" data-off=""></span>
                                <span class="switch-handle"></span>
                            </label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <label for="activated">Aktiviert:</label>
                            <br/>
                            <label class="switch switch-icon switch-pill switch-success" id="activated_switch">
                                <input class="switch-input" id="activated" name="activated" value="1" {{ $user->activated ? 'checked' : '' }} type="checkbox">
                                <span class="switch-label" data-on="" data-off=""></span>
                                <span class="switch-handle"></span>
                            </label>
                        </div>
                    </div>
                </div>


                {{ csrf_field() }}
                {{ method_field('PUT') }}
                <button type="submit" class="btn btn-primary">Speichern</button>
            </form>
        </div>
    </div>
@endsection