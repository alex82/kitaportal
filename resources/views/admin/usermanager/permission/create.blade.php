@extends('layouts.app_admin')

@section('head')
    <style>

    </style>
@endsection

@section('breadcrumb')
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ route('user.index') }}">Benutzerverwaltung</a></li>
        <li class="breadcrumb-item"><a href="{{ route('permission.index') }}">Berechtigungsverwaltung</a></li>
        <li class="breadcrumb-item active">Neue Berechtigung</li>
    </ol>
@endsection

@section('content')
    <div class="card">
        <div class="card-block">
            @if (session('success'))
                <div class="alert alert-success">
                    {{ session('success') }}
                </div>
            @endif

            <form method="post" action="{{ route('permission.store') }}">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <label for="name">Name:</label>
                            <input class="form-control" name="name" id="name" placeholder="" value="{{ old('name') }}" type="text">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <label for="display_name">Anzeigename:</label>
                            <input class="form-control" name="display_name" id="display_name" placeholder="" value="{{ old('display_name') }}" type="text">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <label for="description">Beschreibung:</label>
                            <input class="form-control" name="description" id="description" placeholder="" value="{{ old('description') }}" type="text">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <label for="roles">Rollen:</label>
                            <div class="form-group">
                                @foreach($roles as $role)
                                    <label for="role_{{ $role->id }}" class="checkbox-inline">
                                        <input name="roles[]" id="role_{{ $role->id }}" value="{{ $role->id }}" type="checkbox" {{ (in_array($role->id, old('roles', []))) ? 'checked' : '' }}>
                                        {{ $role->display_name }}
                                    </label>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>

                {{ csrf_field() }}
                <button type="submit" class="btn btn-primary">Speichern</button>
            </form>
        </div>
    </div>
@endsection