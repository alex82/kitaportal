@extends('layouts.app_admin')

@section('head')
    <style>

    </style>
@endsection

@section('breadcrumb')
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ route('user.index') }}">Benutzerverwaltung</a></li>
        <li class="breadcrumb-item active">Berechtigungsverwaltung</li>
        <li class="breadcrumb-menu">
            <div class="btn-group" role="group" aria-label="">
                <a class="btn btn-success" href="{{ route('permission.create') }}"><i class="icon-plus"></i> &nbsp;Neue Berechtigung</a>
            </div>
        </li>
    </ol>
@endsection

@section('content')
    <div class="card">
        <div class="card-block">
            @if (session('success'))
                <div class="alert alert-success">
                    {{ session('success') }}
                </div>
            @endif

            <table class="table">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Anzeige-Name</th>
                        <th>Beschreibung</th>
                        <th>Rolle(n)</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($permissions as $permission)
                    <tr>
                        <td>{{ $permission->name }}</td>
                        <td>{{ $permission->display_name }}</td>
                        <td>{{ $permission->description }}</td>
                        <td>{{ $permission->roles->implode('display_name', ', ') }}</td>
                        <td>
                            <a class="btn btn-sm btn-primary" href="{{ route('permission.edit', $permission) }}"><i class="fa fa-pencil"></i></a>
                            <button type="button" class="btn btn-sm btn-danger" data-toggle="modal" data-target="#myModal" data-id="{{ $permission->id }}"><i class="fa fa-trash"></i></button>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h5 class="modal-title" id="myModalLabel">Berechtigung wirklich löschen?</h5>
                </div>
                <div class="modal-footer">
                    <form method="post" action="" id="deleteForm">
                        {{ method_field('DELETE') }}
                        {{ csrf_field() }}
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Abbrechen</button>
                        <button type="submit" class="btn btn-danger">Ja, löschen</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        var delete_url = '{{ route('permission.destroy', 'ITEMID') }}';
        $('#myModal').on('show.bs.modal', function (e) {
            $('#deleteForm').attr('action', delete_url.replace('ITEMID', $(e.relatedTarget).attr('data-id')));
        })
    </script>
@endsection