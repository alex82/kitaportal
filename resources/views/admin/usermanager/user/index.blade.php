@extends('layouts.app_admin')

@section('head')
    <style>

    </style>
@endsection

@section('breadcrumb')
    <ol class="breadcrumb">
        <li class="breadcrumb-item active">Benutzerverwaltung</li>
        <li class="breadcrumb-menu">
            <div class="btn-group" role="group" aria-label="">
                <a class="btn btn-default" href="{{ route('permission.index') }}">Berechtigungsverwaltung</a>
            </div>
        </li>
    </ol>
@endsection

@section('content')
    <div class="card">
        <div class="card-block">
            @if (session('success'))
                <div class="alert alert-success">
                    {{ session('success') }}
                </div>
            @endif

            {!! $dataTable->table(['class' => 'table table-sm vmiddle', 'id' => 'userlist']) !!}
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h5 class="modal-title" id="myModalLabel">User wirklich löschen?</h5>
                </div>
                <div class="modal-footer">
                    <form method="post" action="" id="deleteForm">
                        {{ method_field('DELETE') }}
                        {{ csrf_field() }}
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Abbrechen</button>
                        <button type="submit" class="btn btn-danger">Ja, löschen</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <link rel="stylesheet" href="//cdn.datatables.net/buttons/1.0.3/css/buttons.dataTables.min.css">
    <script src="//cdn.datatables.net/buttons/1.0.3/js/dataTables.buttons.min.js"></script>
    <script src="/vendor/datatables/buttons.server-side.js"></script>
    {!! $dataTable->scripts() !!}
    <script>
        var delete_url = '{{ route('user.destroy', 'USERID') }}';
        $('#myModal').on('show.bs.modal', function (e) {
            $('#deleteForm').attr('action', delete_url.replace('USERID', $(e.relatedTarget).attr('data-id')));
        })
    </script>
@endsection