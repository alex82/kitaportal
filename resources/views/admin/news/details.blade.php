@extends('layouts.app_admin')

@section('head')
    <style>
        h1 small {
            font-size: 40%;
        }

        .comment-block blockquote {
            font-size: 14px;
            font-weight: normal;
        }

        .comment {
            padding-bottom: 20px;
        }

        .comment blockquote {
            margin-bottom: 5px;
        }

        .news-block h2 {
            margin-top: 0;
        }

        .news-block h1 small {
            font-size: 45%;
        }
    </style>
@endsection

@section('breadcrumb')
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ secure_url('/news') }}">Neuigkeiten</a></li>
        <li class="breadcrumb-item active">Kommentare</li>
    </ol>
@endsection

@section('content')
    <div class="card">
        <div class="card-block news-block">
            @include('admin.news.partial.post_content', ['post' => $post])
        </div>
    </div>

    <div class="card">
        <div class="card-block comment-block">

            <h1 class="h3 card-title">Kommentare:</h1>
            <br/>

            @foreach($post->comments()->approved()->orderBy('created_at', 'DESC')->get() as $comment)
                <div class="comment">
                    <h2 class="h5">
                        <strong>{{ $comment->created_at->format('d.m.Y H:i') }} Uhr von {{ $comment->user->name }}:</strong>
                    </h2>
                    <blockquote>
                        {!! nl2br(e($comment->text)) !!}
                    </blockquote>
                    @permission('news-comment-delete')
                    <a href="#" data-id="{{ $comment->id }}" data-toggle="modal" data-target="#confirmDeleteCommentModal">
                        <small>Kommentar löschen</small>
                    </a>
                    @endpermission
                </div>
            @endforeach

            <hr/>

            <div class="row">
                <div class="col-sm-6">
                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    <form method="post" action="{{ route('news_create_comment', $post) }}">
                        <div class="form-group {{ $errors->has('comment') ? 'has-error' : '' }}">
                            <label for="comment">Neuer Kommentar:</label>
                            <textarea class="form-control" rows="3" id="comment" name="comment"></textarea>
                        </div>
                        {{ csrf_field() }}
                        <button type="submit" class="btn btn-success">Speichern</button>
                    </form>
                </div>
            </div>

            <br/>
            <br/>
        </div>
    </div>
@endsection

@section('modals')
    <!-- Modal -->
    <div class="modal fade" id="confirmDeleteCommentModal" tabindex="-1" role="dialog" aria-labelledby="confirmDeleteCommentModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="confirmDeleteCommentModalLabel">Bestätigung</h4>
                </div>
                <div class="modal-body">
                    Soll der Kommentar wirklich gelöscht werden?<br/>
                    Dieser Vorgang kann nicht rückgängig gemacht werden!
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Nein, Abbrechen</button>
                    <a href="" id="delete_comment_link" class="btn btn-danger">Ja, Kommentar löschen</a>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $(document).ready(function () {
            $('#confirmDeleteCommentModal').on('show.bs.modal', function (e) {
                $('#delete_comment_link').attr('href', '/news/comment/' + $(e.relatedTarget).attr('data-id') + '/delete');

            })
        });
    </script>
@endsection