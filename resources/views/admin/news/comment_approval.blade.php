@extends('layouts.app_admin')

@section('breadcrumb')
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ secure_url('/news') }}">Neuigkeiten</a></li>
        <li class="breadcrumb-item active">Kommentare freischalten</li>
    </ol>
@endsection

@section('content')
    <div class="col-lg-12">
        <div class="card">
            <div class="card-block">
                <h1 class="h2">Kommentare freischalten:</h1>

                @if($comments->count() == 0)
                    <strong>Keine Kommentare vorhanden!</strong>
                @endif
            </div>
        </div>
    </div>

    @foreach($comments as $comment)
        <div class="col-lg-6">
            <div class="card">
                <div class="card-block">
                    Kommentar zu "<strong>{{ $comment->post->title }}</strong>":
                    <br/>
                    <br/>
                    <blockquote>
                        {!! nl2br(e($comment->text)) !!}
                    </blockquote>
                    <br/>
                    <a href="{{ route('approve', $comment) }}" class="btn btn-success">freischalten</a>
                    <a href="{{ route('delete_comment', $comment) }}" class="btn btn-danger">löschen</a>
                </div>
            </div>
        </div>
    @endforeach

@endsection