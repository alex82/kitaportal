<h1 class="h3 card-title">
    {{ $post->title }}<br/>
    <small>{{ $post->created_at->format('d.m.Y H:i') }} Uhr von {{ $post->user->name }}</small>
</h1>
<hr/>
<p>{!! $post->text !!}</p>

@if($post->files()->count() > 0)
    <hr/>
    <dl>
        <dt>Anhänge:</dt>
        <dd>
            <ul class="list-inline">
                @foreach($post->files as $file)
                    <li><a href="{{ route('download_file', $file) }}" target="_blank">{{ $file->file_name }} ({{ formatBytes($file->size) }})</a></li>
                @endforeach
            </ul>
        </dd>
    </dl>
@endif