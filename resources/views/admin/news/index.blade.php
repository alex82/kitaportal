@extends('layouts.app_admin')

@section('head')
    <style>
        .news-block h1 {
            margin-top: 0;
        }

        .news-block h1 small {
            font-size: 45%;
        }

        @media (max-width: 991px) {
            .breadcrumb-menu {
                display: block!important;
            }
        }
    </style>
@endsection

@section('breadcrumb')
    <ol class="breadcrumb">
        <li class="breadcrumb-item active">Neuigkeiten</li>
        @permission('news-add')
        <li class="breadcrumb-menu">
            <div class="btn-group" role="group" aria-label="">
                <a class="btn btn-success" href="{{ route('news_create_post') }}" id="add_news"><i class="icon-plus"></i> &nbsp;Neuer Beitrag</a>
            </div>
        </li>
        @endpermission
    </ol>
@endsection

@section('content')
    <h1 class="h2">Neuigkeiten</h1>

    @if($unapprovedComments > 0 && (Laratrust::hasRole('admin') || Laratrust::hasRole('manager')))
        <div class="alert alert-info" role="alert">
            <strong>{{ $unapprovedComments }} {{ trans_choice('pluralization.new' ,$unapprovedComments) }} {{ trans_choice('pluralization.comment' ,$unapprovedComments) }}.</strong> Klicken Sie <a href="{{ route('show_approvals') }}" class="alert-link">hier</a> um {{ trans_choice('pluralization.this' ,$unapprovedComments) }} freizuschalten.
        </div>
    @endif

    @foreach($posts as $post)
        @if(\App\Repositories\PostRepository::postIsVisibleForCurrentUser($post))
            <div class="card">
                <div class="card-block news-block">
                    @include('admin.news.partial.post_content', ['post' => $post])
                </div>
                <div class="card-footer">
                    <a href="{{ route('news_show_comments', $post) }}" class="post-comments">{{ $post->comments()->approved()->count() }} {{ trans_choice('pluralization.comment' ,$post->comments()->approved()->count()) }} </a>

                    @permission('news-add')
                    &middot;
                    <a href="{{ route('news_edit_post', $post) }}">Beitrag bearbeiten</a>
                    @endpermission

                    @permission('news-delete')
                        &middot;
                        <a href="#" data-id="{{ $post->id }}" data-toggle="modal" data-target="#confirmDeletePostModal" class="delete-post">Beitrag löschen</a>
                    @endpermission

                    @role(['admin', 'manager', 'nursery'])
                        <span class="tag tag-info pull-right">Sichtbar für {{ \App\Repositories\PostRepository::getVisibleFor($post) }}</span>
                    @endrole
                </div>
            </div>
        @endif
    @endforeach

    <div class="col-sm-12">
        {{ $posts->links() }}
    </div>


    <!-- Modal -->
    <div class="modal fade" id="confirmDeletePostModal" tabindex="-1" role="dialog" aria-labelledby="confirmDeletePostModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="confirmDeletePostModalLabel">Bestätigung</h4>
                </div>
                <div class="modal-body">
                    Soll der Beitrag wirklich gelöscht werden?<br/>
                    Dieser Vorgang kann nicht rückgängig gemacht werden!
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Nein, Abbrechen</button>
                    <a href="" id="delete_post_link" class="btn btn-danger">Ja, Beitrag löschen</a>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $('#confirmDeletePostModal').on('show.bs.modal', function (e) {
            $('#delete_post_link').attr('href', '/news/' + $(e.relatedTarget).attr('data-id') + '/delete');
        })
    </script>
@endsection