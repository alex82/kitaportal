@extends('layouts.app_admin')

@section('head')
    <style>
        .has-error .note-editor {
            border-color: #a94442;
            box-shadow: 0 1px 1px rgba(0, 0, 0, 0.075) inset;
        }

        .popover-content{
             display: none;
         }
        .note-editor.fullscreen {
            background-color:#FFF;
        }
        .note-toolbar .note-icon-caret{
            display: none;
        }
        .note-toolbar .dropdown-menu{
            padding: 0.2rem;
        }
    </style>
@endsection

@section('breadcrumb')
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ secure_url('/news') }}">Neuigkeiten</a></li>
        <li class="breadcrumb-item active">Neuer Beitrag</li>
    </ol>
@endsection

@section('content')
    <form method="post" action="{{ route('news_store_post') }}"  enctype="multipart/form-data">

        <div class="card">
            <div class="card-header">
                Neuer Beitrag
            </div>
            <div class="card-block">
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                <div class="form-group {{ $errors->has('title') ? 'has-error' : '' }}">
                    <label for="title">Titel:</label>
                    <input type="text" class="form-control" id="title" name="title" value="{{ old('title') }}" autofocus/>
                </div>

                <div class="form-group {{ $errors->has('text') ? 'has-error' : '' }}">
                    <label for="text">Text:</label>
                    <textarea id="text" name="text">{!! old('text') !!}</textarea>
                </div>
            </div>
        </div>

        <div class="card">
            <div class="card-header">
                Dateianhänge
            </div>
            <div class="card-block">
                <div class="form-group {{ $errors->has('files') ? 'has-error' : '' }}">
                    <label for="files">Dateien anhängen:</label>
                    <input id="files" type="file" name="files[]" id="files" multiple>
                </div>
            </div>
        </div>

        <div class="card">
            <div class="card-header">
                Sichbarkeit des Beitrages:
            </div>
            <div class="card-block">
                <div class="form-group">
                    <span class="help-block">
                        Geben Sie hier an für wen dieser Beitrag sichtbar sein soll. Sind keine Gruppen/Gebäude ausgewählt ist der Beitrag für alle sichtbar.
                        <br/>Ein Beitrag kann für einzelnen Gebäude und/oder Gruppen freigeschaltet werden.
                    </span>
                </div>

                <div class="form-group {{ $errors->has('visible_for_groups') ? 'has-error' : '' }} row">
                    <div class="col-lg-3 col-md-4">
                        <label for="files">Sichtbar für Gruppe(n):</label>
                    </div>
                    <div class="col-lg-9 col-md-8">
                        <select id="visible_for_groups" name="visible_for_groups[]" multiple="multiple">
                            @foreach(\App\Models\NurseryGroup::all() as $group)
                                <option value="{{ $group->id }}">{{ $group->description }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="form-group {{ $errors->has('visible_for_buildings') ? 'has-error' : '' }} row">
                    <div class="col-lg-3 col-md-4">
                        <label for="files">Sichtbar für Gebäude:</label>
                    </div>
                    <div class="col-lg-9 col-md-8">
                        <select id="visible_for_buildings" name="visible_for_buildings[]" multiple="multiple">
                            @foreach(\App\Models\NurseryBuilding::all() as $building)
                                <option value="{{ $building->id }}">{{ $building->description }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
        </div>

        <div class="card">
            <div class="card-block">
                {{ csrf_field() }}
                <button type="submit" class="btn btn-success" id="submit_btn">Speichern</button>
            </div>
        </div>
    </form>
@endsection

@section('scripts')
    <script>
        $(document).ready(function () {
            @if(old('visible_for_groups') && collect(old('visible_for_groups'))->count() > 0)
             $('#visible_for_groups').val(['{!! collect(old('visible_for_groups'))->implode("','") !!}']);
            @endif
            $('#visible_for_groups').multiselect({
                nonSelectedText: 'Für alle Gruppen sichtbar',
                allSelectedText: 'Für alle Gruppen sichtbar',
                nSelectedText: 'Gruppen ausgewählt',
                numberDisplayed: 1,
            });

            @if(old('visible_for_buildings') && collect(old('visible_for_buildings'))->count() > 0)
                $('#visible_for_buildings').val(['{!! collect(old('visible_for_buildings'))->implode("','") !!}']);
            @endif
            $('#visible_for_buildings').multiselect({
                nonSelectedText: 'Für alle Gebäude sichtbar',
                allSelectedText: 'Für alle Gebäude sichtbar',
                nSelectedText: 'Gebäude ausgewählt',
                numberDisplayed: 1,
            });

            $('#submit_btn').click(function () {
                $('#text_textarea').val($('.jodit_editor').html());
            });

            var editor = new Jodit('#text', {
                buttons: ['bold', 'italic', '|', 'ul', 'ol', '|', 'font', 'fontsize', 'brush', 'paragraph', '|', 'table', 'link', '|', 'left', 'center', 'right', 'justify', '|', 'undo', 'redo', '|', 'hr', 'eraser'],
                language: 'de',
                minHeight: 300
            });
        });
    </script>
@endsection