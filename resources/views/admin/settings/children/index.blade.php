@extends('layouts.app_admin')

@section('head')
    <style>

    </style>
@endsection

@section('breadcrumb')
    <ol class="breadcrumb">
        <li class="breadcrumb-item active">Ihre Kinder</li>
    </ol>
@endsection

@section('content')
    <h1 class="h2">Ihre Kinder</h1>

    <div class="card">
        <div class="card-block">
            @if (session('success'))
                <div class="alert alert-success">
                    {{ session('success') }}
                </div>
            @endif

            <table class="table table-bordered table-sm vmiddle" id="childlist">
                <thead>
                    <tr>
                        <th>Vorname</th>
                        <th>Nachname</th>
                        <th>Gruppe</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($children as $child)
                    <tr>
                        <td>{{ $child->firstname }}</td>
                        <td>{{ $child->lastname }}</td>
                        <td>{{ $child->nursery_group->description }} ({{ $child->nursery_group->building->description }})</td>
                        <td>
                            <a class="btn btn-sm btn-primary" href="{{ route('children.edit', $child) }}"><i class="fa fa-pencil"></i></a>
                            <button type="button" class="btn btn-sm btn-danger" data-toggle="modal" data-target="#myModal" data-id="{{ $child->id }}"><i class="fa fa-trash"></i></button>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>

            <a class="btn btn-success" href="{{ route('children.create') }}" id="add_child"><i class="icon-plus"></i> &nbsp;Kind hinzufügen</a>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h5 class="modal-title" id="myModalLabel">Kind wirklich löschen?</h5>
                </div>
                <div class="modal-footer">
                    <form method="post" action="" id="deleteForm">
                        {{ method_field('DELETE') }}
                        {{ csrf_field() }}
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Abbrechen</button>
                        <button type="submit" class="btn btn-danger">Ja, löschen</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        var delete_url = '{{ route('children.destroy', 'USERID') }}';
        $('#myModal').on('show.bs.modal', function (e) {
            $('#deleteForm').attr('action', delete_url.replace('USERID', $(e.relatedTarget).attr('data-id')));
        })
    </script>
@endsection