@extends('layouts.app_admin')

@section('head')
    <style>

    </style>
@endsection

@section('breadcrumb')
    <ol class="breadcrumb">
        <li class="breadcrumb-item active">Ihre Kinder</li>
    </ol>
@endsection

@section('content')
    <h1 class="h2">Kind hinzufügen</h1>

    <div class="card">
        <div class="card-block">
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif


            <form method="post" action="{{ route('children.store') }}">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <label for="name">Vorname:</label>
                            <input class="form-control" name="firstname" id="firstname" placeholder="" value="{{ old('firstname') }}" type="text">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <label for="name">Nachname:</label>
                            <input class="form-control" name="lastname" id="lastname" placeholder="" value="{{ old('lastname') }}" type="text">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-sm-12">
                        <label for="group">Gruppe:</label>
                        <select class="form-control" name="group" id="group">
                            <option value="">Bitte auswählen</option>
                            @foreach($buildings as $building)
                                <optgroup label="{{ $building->description }}">
                                    @foreach($building->groups as $group)
                                        <option value="{{ $group->id }}" {{ (old('group') == $group->id) ? 'selected="selected"' : '' }}>{{ $group->description }}</option>
                                    @endforeach
                                </optgroup>
                            @endforeach
                        </select>
                    </div>
                </div>
                {{ csrf_field() }}
                <button type="submit" class="btn btn-primary">Speichern</button>
                <p class="text-muted m-t-2">
                    Hinweis: Die Kita-Leitung wird über das Anlegen neuer Kinder informiert!
                </p>
            </form>
        </div>
    </div>
@endsection