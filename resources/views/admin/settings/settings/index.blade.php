@extends('layouts.app_admin')

@section('head')
    <style>

    </style>
@endsection

@section('breadcrumb')
    <ol class="breadcrumb">
        <li class="breadcrumb-item active">Einstellungen</li>
    </ol>
@endsection

@section('content')
    <h1 class="h2">Einstellungen</h1>

    @if (session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
    @endif

    <form class="form-horizontal" method="post" action="{{ route('settings_save') }}">
        <div class="row">
            <div class="col-md-12 col-lg-8">
                <div class="card">
                    <div class="card-header">
                        <strong>Früh- / Spätdienstliste</strong>
                    </div>
                    <div class="card-block">
                        <div class="form-group row">
                            <label class="col-sm-4 form-control-label" for="select">Standard-Wert Früh</label>
                            <div class="col-sm-8">
                                {{
                                    Form::select(
                                        \App\Models\Setting::KEY_SHIFTLIST_DEFAULT_MORNING_VALUE,
                                        \App\Models\ShiftlistEntry::VALUES_MORNING ,
                                        old(\App\Models\Setting::KEY_SHIFTLIST_DEFAULT_MORNING_VALUE,
                                        settings(\App\Models\Setting::KEY_SHIFTLIST_DEFAULT_MORNING_VALUE)
                                    ), [
                                        'class' => 'form-control'
                                    ])
                                }}
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-sm-4 form-control-label" for="select">Standard-Wert Spät</label>
                            <div class="col-sm-8">
                                {{
                                    Form::select(
                                        \App\Models\Setting::KEY_SHIFTLIST_DEFAULT_EVENING_VALUE,
                                        \App\Models\ShiftlistEntry::VALUES_EVENING,
                                        old(\App\Models\Setting::KEY_SHIFTLIST_DEFAULT_EVENING_VALUE,
                                        settings(\App\Models\Setting::KEY_SHIFTLIST_DEFAULT_EVENING_VALUE)
                                    ), [
                                        'class' => 'form-control'
                                    ])
                                }}
                            </div>
                        </div>

                        <div class="alert alert-info">
                            Hier können Sie Standard-Werte für die Früh- / Spätdienstliste einstellen.<br/>
                            Diese Werte werden jeden Donnerstag Abend automatisch für die kommende Woche gesetzt, sollten Sie keine eigenen Angaben gemacht haben.<br/>
                            Sie können die Angaben in der Früh- / Spätdienstliste jederzeit manuell ändern!
                        </div>
                    </div>
                </div>
                {{ csrf_field() }}
                <input type="submit" value="Einstellungen speichern" class="btn btn-primary">
            </div>
        </div>
    </form>
@endsection