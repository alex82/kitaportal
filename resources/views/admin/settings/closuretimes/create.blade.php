@extends('layouts.app_admin')

@section('head')
    <style>

    </style>
@endsection

@section('breadcrumb')
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ route('closuretimes.index') }}">Schließzeiten</a></li>
        <li class="breadcrumb-item active">Schließzeit hinzufügen</li>
    </ol>
@endsection

@section('content')
    <h1 class="h2">Schließzeit hinzufügen</h1>

    <div class="card">
        <div class="card-block">
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif


            <form method="post" action="{{ route('closuretimes.store') }}">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <label for="from">Von:</label>
                            <input class="form-control datepicker" name="from" id="from" placeholder="" value="{{ old('from') }}" type="text">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <label for="to">Bis:</label>
                            <input class="form-control datepicker" name="to" id="to" placeholder="" value="{{ old('to') }}" type="text">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-sm-12">
                        <label for="type">Typ:</label>
                        <select class="form-control" name="type" id="type">
                            <option value="">Bitte auswählen</option>
                            <option value="{{ \App\Models\ClosureTime::TYPE_MORNING_CLOSED }}">Nachmittag geschlossen</option>
                            <option value="{{ \App\Models\ClosureTime::TYPE_FULL_DAY_CLOSED }}">Ganzer Tag geschlossen</option>
                        </select>
                    </div>
                </div>
                {{ csrf_field() }}
                <button type="submit" class="btn btn-primary">Speichern</button>
            </form>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $('.datepicker').datepicker({
            'orientation': 'bottom',
            'format': 'dd.mm.yyyy',
            'autoclose': true,
            'daysOfWeekDisabled': '0,6',
            'language': 'de'
        });
    </script>
@endsection