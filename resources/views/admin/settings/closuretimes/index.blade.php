@extends('layouts.app_admin')

@section('head')
    <style>

    </style>
@endsection

@section('breadcrumb')
    <ol class="breadcrumb">
        <li class="breadcrumb-item active">Schließzeiten</li>
    </ol>
@endsection

@section('content')
    <h1 class="h2">Schließzeiten</h1>

    <div class="card">
        <div class="card-block">
            @if (session('success'))
                <div class="alert alert-success">
                    {{ session('success') }}
                </div>
            @endif

            <table class="table table-bordered table-sm vmiddle" id="closuretimeslist">
                <thead>
                    <tr>
                        <th>Zeitraum</th>
                        <th>Typ</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                @foreach($closuretimes as $closuretime)
                    <tr>
                        <td>
                            @if($closuretime->from->format('d.m.Y') === $closuretime->to->format('d.m.Y'))
                                {{ $closuretime->from->format('d.m.Y') }}
                            @else
                                {{ $closuretime->from->format('d.m.Y') }} - {{ $closuretime->to->format('d.m.Y') }}
                            @endif
                        </td>
                        <td>{{ $closuretime->getTypeAsString() }}</td>
                        <td>
                            <a class="btn btn-sm btn-primary" href="{{ route('closuretimes.edit', $closuretime) }}"><i class="fa fa-pencil"></i></a>
                            <button type="button" class="btn btn-sm btn-danger" data-toggle="modal" data-target="#myModal" data-id="{{ $closuretime->id }}"><i class="fa fa-trash"></i></button>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>

            <a class="btn btn-success" href="{{ route('closuretimes.create') }}" id="add_closuretimes"><i class="icon-plus"></i> &nbsp;Eintrag hinzufügen</a>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h5 class="modal-title" id="myModalLabel">Eintrag wirklich löschen?</h5>
                </div>
                <div class="modal-footer">
                    <form method="post" action="" id="deleteForm">
                        {{ method_field('DELETE') }}
                        {{ csrf_field() }}
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Abbrechen</button>
                        <button type="submit" class="btn btn-danger">Ja, löschen</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        var delete_url = '{{ route('closuretimes.destroy', 'USERID') }}';
        $('#myModal').on('show.bs.modal', function (e) {
            $('#deleteForm').attr('action', delete_url.replace('USERID', $(e.relatedTarget).attr('data-id')));
        })
    </script>
@endsection