<td>
    @if($list->has($key))
        {{ $list[$key]->implode(', ') }}
    @endif
</td>
<td class="text-xs-right">
    @if($list->has($key))
        + {{ $list[$key]->count() }}
    @endif
</td>