<td>
    <span class="weekday">{{ $day->formatLocalized('%a') }}</span>
    {{ $day->formatLocalized('%d.%m.') }}

    @if($day->dayOfWeek == \Carbon\Carbon::MONDAY)
        <small class="pull-right text-muted hidden-md-down">KW {{ $day->weekOfYear }}</small>
    @endif
</td>