@extends('layouts.app_admin')

@section('head')
    <style>
        .weekday {
            display: inline-block;
            width: 20px;
        }

        .icon-updated {
            font-size: 20px;
        }

        .icon-failure {
            font-size: 20px;
        }

        @media (max-width: 767px) {
            .form-control {
                padding: 0;
            }

            .container {
                padding-left: 10px;
                padding-right: 10px;
            }
        }
    </style>
@endsection

@section('breadcrumb')
    <ol class="breadcrumb">
        <li class="breadcrumb-item active">Früh- / Spätdienstliste</li>
    </ol>
@endsection

@section('content')

    <h1 class="h2">Früh- / Spätdienstliste</h1>
    <div class="card">
        <div class="card-block table-responsive">
            <div class="alert alert-info alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                Geben Sie hier bitte an, wann Sie {{ trans_choice('pluralization.you', $user->children->count()) . ' ' . trans_choice('pluralization.child', $user->children->count()) }} in die Kita bringen bzw. abholen.
                @if($user->children->count() > 1)
                    <br/><strong>Diese Angaben gelten für alle Ihre Kinder gleichermaßen!</strong>
                @endif
            </div>

            <div class="alert alert-danger">
                @if (!is_null(Auth::user()->getSetting(\App\Models\Setting::KEY_SHIFTLIST_DEFAULT_MORNING_VALUE, false)) || !is_null(Auth::user()->getSetting(\App\Models\Setting::KEY_SHIFTLIST_DEFAULT_EVENING_VALUE, false)))
                    Sie haben Standardwerte für diese Liste festgelegt.
                    <br/>
                    Am <strong>{{ (new \Carbon\Carbon('next thursday'))->format('d.m.Y') }}</strong> werden automatisch für die <strong>kommende Woche</strong> ({{ (new \Carbon\Carbon('next thursday'))->addDay(4)->format('d.m.Y') }} - {{ (new \Carbon\Carbon('next thursday'))->addDay(8)->format('d.m.Y') }}) folgende Werte übernommen:<br/>
                    <strong>Früh: {{ \App\Models\ShiftlistEntry::VALUES_MORNING[settings(\App\Models\Setting::KEY_SHIFTLIST_DEFAULT_MORNING_VALUE)] }}</strong><br/>
                    <strong>Spät: {{ \App\Models\ShiftlistEntry::VALUES_EVENING[settings(\App\Models\Setting::KEY_SHIFTLIST_DEFAULT_EVENING_VALUE)] }}</strong>
                @else
                    Sie können unter <a href="{{ route('settings_index') }}" class="alert-link">Einstellungen</a> Standardwerte für diese Liste festlegen.
                @endif
            </div>

            <h2 class="text-xs-center m-t-3">
                <a href="{{ route('shiftlist_index', ['year' => $carbon->copy()->subMonth()->format('Y'), 'month' => $carbon->copy()->subMonth()->format('m')]) }}" class="pull-left" id="prev-month">
                    <span class="fa fa-chevron-left"></span>
                </a>
                {{ $carbon->formatLocalized('%B %Y')}}
                <a href="{{ route('shiftlist_index', ['year' => $carbon->copy()->addMonth()->format('Y'), 'month' => $carbon->copy()->addMonth()->format('m')]) }}" class="pull-right" id="next-month">
                    <span class="fa fa-chevron-right"></span>
                </a>
            </h2>
            <br/>
            <table class="table table-bordered table-sm vmiddle" id="shiftlist">
                <thead>
                    <tr>
                        <th>Tag</th>
                        <th class="text-xs-center">Früh</th>
                        <th class="text-xs-center">Spät</th>
                    </tr>
                </thead>
                <tbody>
                    @for($day = $carbon->copy(); $day->lte($carbon->copy()->lastOfMonth()); $day->addDay())
                        @if(\App\Helpers\CalendarHelper::isWeekend($day))
                            <tr class="table-active">
                                @include('admin.shiftlist.partial.weekday')
                                <td colspan="2" class="text-xs-center"><span class="text-muted">Wochenende</span></td>
                            </tr>

                        @elseif(\App\Helpers\CalendarHelper::dayIsPublicHoliday($day))
                            <tr class="table-danger">
                                @include('admin.shiftlist.partial.weekday')
                                <td colspan="2" class="text-xs-center"><span class="text-danger">Feiertag</span></td>
                            </tr>

                        @elseif(\App\Helpers\CalendarHelper::dayIsClosureTime($day))
                            <tr class="table-danger">
                                @include('admin.shiftlist.partial.weekday', ['$day' => $day])
                                <td colspan="2" class="text-xs-center"><span class="text-danger">Schließzeit</span></td>
                            </tr>

                        @elseif(\App\Helpers\CalendarHelper::afternoonIsClosureTime($day))
                            <tr>
                                @include('admin.shiftlist.partial.weekday')
                                <td class="text-xs-center">
                                    @if (\App\Helpers\CalendarHelper::dayIsValidForChangeInShiftlist($day))
                                        <shiftlist-select :options="select_options_morning" selected="{{ $user->getShiftlistValue($day, \App\Models\ShiftlistEntry::TYPE_MORNING) }}" day="{{ $day->format('Y-m-d') }}" userid="{{ $user->id }}"></shiftlist-select>
                                    @else
                                        <small class="font-italic text-muted">nicht mehr änderbar</small>
                                    @endif
                                </td>
                                <td class="table-danger text-xs-center">
                                    <span class="text-danger">Konzeptionsnachmittag</span>
                                </td>
                            </tr>

                        @else
                            <tr>
                                @include('admin.shiftlist.partial.weekday')
                                <td class="text-xs-center">
                                    @if (\App\Helpers\CalendarHelper::dayIsValidForChangeInShiftlist($day))
                                        <shiftlist-select :options="select_options_morning" selected="{{ $user->getShiftlistValue($day, \App\Models\ShiftlistEntry::TYPE_MORNING) }}" day="{{ $day->format('Y-m-d') }}" userid="{{ $user->id }}"></shiftlist-select>
                                    @else
                                        <small class="font-italic text-muted">nicht mehr änderbar</small>
                                    @endif
                                </td>
                                <td class="text-xs-center">
                                    @if (\App\Helpers\CalendarHelper::dayIsValidForChangeInShiftlist($day))
                                        <shiftlist-select :options="select_options_evening" selected="{{ $user->getShiftlistValue($day, \App\Models\ShiftlistEntry::TYPE_EVENING) }}" day="{{ $day->format('Y-m-d') }}" userid="{{ $user->id }}"></shiftlist-select>
                                    @else
                                        <small class="font-italic text-muted">nicht mehr änderbar</small>
                                    @endif
                                </td>
                            </tr>
                        @endif
                    @endfor
                </tbody>
            </table>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="/js/shiftlist.js"></script>
@endsection