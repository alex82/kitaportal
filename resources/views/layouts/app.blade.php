<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="/css/styles.css" rel="stylesheet">

    <!-- Scripts -->
    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>
</head>
<body>
    <div id="app">
        <noscript>
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="alert alert-danger">Bitte aktivieren Sie Javascript um dieses Portal vollständig nutzen zu können.</div>
                    </div>
                </div>
            </div>
        </noscript>

        @include('layouts.selenium_test_output')

        @yield('content')

        <br/>
        <br/>
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <a href="http://www.kv-leipzig.de" target="_blank"><img src="/img/kv.png" alt="Kindervereinigung Leipzig e.V." /></a>
                    <br/>
                    <a href="/imprint">Impressum</a>
                </div>
            </div>
        </div>
    </div>

    <!-- Scripts -->
    <script src="/js/frontend.js"></script>
    <script src="/js/app.js"></script>
    @yield('scripts')
</body>
</html>
