@if(\DB::connection()->getDatabaseName() == 'kitaportal_test')
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="alert alert-info">Test Environment</div>
            </div>
        </div>
    </div>
@endif