<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="/css/styles.css" rel="stylesheet">

    <link rel="stylesheet" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.min.css">

    <!-- Scripts -->
    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>

    @yield('head')
</head>
<body class="navbar-fixed sidebar-nav fixed-nav">
    <header class="navbar">
        <div class="container-fluid">
            <button class="navbar-toggler mobile-toggler hidden-lg-up" type="button">&#9776;</button>
            <a class="navbar-brand" href="{{ secure_url('/news') }}"></a>

            @if(Auth::user()->isImpersonating())
            <ul class="nav navbar-nav pull-right m-r-2">
                <li class="nav-item">
                    <a href="/users/stop">User Simulation beenden</a>
                </li>
            </ul>
            @endif
        </div>
    </header>

    <div class="sidebar">
        <nav class="sidebar-nav">
            <ul class="nav">
                <li class="nav-item">
                    <a class="nav-link {{ active_class(if_uri_pattern('news*')) }}" href="{{ secure_url('/news') }}">
                        <i class="fa fa-info"></i> Neuigkeiten

                        @if($unapproved_comment_count > 0 && (Laratrust::hasRole('admin') || Laratrust::hasRole('manager')))
                            <span class="tag tag-info">{{ $unapproved_comment_count }}</span>
                        @endif
                    </a>
                </li>

                @permission('shiftlist-view')
                <li class="nav-item">
                    <a class="nav-link {{ active_class(if_uri_pattern('shiftlist*')) }}" href="{{ route('shiftlist_view') }}"><i class="fa fa-clock-o"></i> Früh- / Spätdienstliste</a>
                </li>
                @endpermission

                @permission('shiftlist-edit')
                <li class="nav-item">
                    <a class="nav-link {{ active_class(if_uri_pattern('shiftlist*')) }}" href="{{ route('shiftlist_index') }}"><i class="fa fa-clock-o"></i> Früh- / Spätdienstliste</a>
                </li>
                @endpermission

                @permission('parentmanager-access')
                <li class="nav-item">
                    <a class="nav-link {{ active_class(if_uri_pattern('parent*')) }}" href="{{ route('parent.index') }}"><i class="fa fa-users"></i> Benutzerverwaltung</a>
                </li>
                @endpermission

                @permission('closuretimes-access')
                <li class="nav-item">
                    <a class="nav-link {{ active_class(if_uri_pattern('closuretimes*')) }}" href="{{ route('closuretimes.index') }}"><i class="fa fa-ban"></i> Schließzeiten</a>
                </li>
                @endpermission

                @permission('usermanager-access')
                <li class="nav-item">
                    <a class="nav-link {{ active_class(if_uri_pattern('user*') || if_uri_pattern('permission*')) }}" href="{{ route('user.index') }}"><i class="fa fa-users"></i> Benutzer</a>
                </li>
                @endpermission

                @permission('parentsettings-access')
                <li class="nav-item">
                    <a class="nav-link {{ active_class(if_uri_pattern('settings*')) }}" href="{{ route('settings_index') }}"><i class="fa fa-wrench"></i> Einstellungen</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link {{ active_class(if_uri_pattern('children*')) }}" href="{{ route('children.index') }}"><i class="fa fa-child"></i> Ihre Kinder</a>
                </li>
                @endpermission

                <li class="nav-item">
                    <a class="nav-link" href="{{ route('logout') }}"><i class="fa fa-sign-out"></i> Abmelden</a>
                </li>
            </ul>
        </nav>

        <div class="sidebar-bottom">
            <a href="http://www.kv-leipzig.de" target="_blank">
                <img src="/img/kv.png" alt="Kindervereinigung Leipzig e.V." class="hidden-md-down" />
            </a>
            <br/>
            <a href="/imprint">Impressum</a>
        </div>
    </div>

    <main class="main" id="app">
        @yield('breadcrumb')

        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    @if (session('success_message'))
                        <br/>
                        <div class="alert alert-success">
                            {{ session('success_message') }}
                        </div>
                    @endif

                    @if (session('error_message'))
                        <br/>
                        <div class="alert alert-danger">
                            {{ session('error_message') }}
                        </div>
                    @endif

                    @if (session()->has('flash_notification.message'))
                        <div class="alert alert-{{ session('flash_notification.level') }}">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>

                            {!! session('flash_notification.message') !!}
                        </div>
                    @endif

                    <h1 class="page-header">@yield('page_heading')</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>

            @include('layouts.selenium_test_output')

            @yield('content')
        </div>
    </main>

    @yield('modals')

    <!-- Scripts -->
    <script src="/js/app.js"></script>
    <script src="/js/frontend.js"></script>
    <script src="//cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/locales/bootstrap-datepicker.de.min.js"></script>

    <script>
        (function ($, DataTable) {

            // Datatable global configuration
            $.extend(true, DataTable.defaults, {
                language: {
                    "sEmptyTable":      "Keine Daten in der Tabelle vorhanden",
                    "sInfo":            "_START_ bis _END_ von _TOTAL_ Einträgen",
                    "sInfoEmpty":       "0 bis 0 von 0 Einträgen",
                    "sInfoFiltered":    "(gefiltert von _MAX_ Einträgen)",
                    "sInfoPostFix":     "",
                    "sInfoThousands":   ".",
                    "sLengthMenu":      "_MENU_ Einträge anzeigen",
                    "sLoadingRecords":  "Wird geladen...",
                    "sProcessing":      "Bitte warten...",
                    "sSearch":          "Suchen",
                    "sZeroRecords":     "Keine Einträge vorhanden.",
                    "oPaginate": {
                        "sFirst":       "Erste",
                        "sPrevious":    "Zurück",
                        "sNext":        "Nächste",
                        "sLast":        "Letzte"
                    },
                    "oAria": {
                        "sSortAscending":  ": aktivieren, um Spalte aufsteigend zu sortieren",
                        "sSortDescending": ": aktivieren, um Spalte absteigend zu sortieren"
                    }
                }
            });

        })(jQuery, jQuery.fn.dataTable);
    </script>
    @yield('scripts')
</body>
</html>
