<?php

return [
    'child' => 'Kind|Kinder',
    'you' => 'Ihr|Ihre',
    'comment' => 'Kommentar|Kommentare',
    'new' => 'neuer|neue',
    'this' => 'diesen|diese',
    'group' => 'Gruppe|Gruppen',
];