<?php

namespace App\Console\Commands;

use App\Models\Setting;
use App\Repositories\ShiftlistRepository;
use App\Repositories\UserRepository;
use Illuminate\Console\Command;
use App\Models\User;

class CreateAutoShiftlistEntries extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'create:autoshiftlist';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Creates entries for shiftlist';

    protected $userRepository;

    protected $shiftlistRepository;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(UserRepository $userRepository, ShiftlistRepository $shiftlistRepository)
    {
        parent::__construct();
        $this->userRepository = $userRepository;
        $this->shiftlistRepository = $shiftlistRepository;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $users = $this->userRepository->getUsersWithNonDefaultShiftlistEntries();
        $users->each(function ($user) {
            $this->shiftlistRepository->createEntriesForNextWeek(
                $user,
                $user->getSetting(Setting::KEY_SHIFTLIST_DEFAULT_MORNING_VALUE),
                $user->getSetting(Setting::KEY_SHIFTLIST_DEFAULT_EVENING_VALUE)
            );
            $this->info('processed user '.$user->id);
        });
    }
}
