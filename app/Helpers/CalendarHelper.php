<?php

namespace App\Helpers;

use App\Models\ClosureTime;
use Carbon\Carbon;
use Cache;

class CalendarHelper {

    /**
     * @param Carbon $carbon
     *
     * @return bool
     */
    public static function isWeekend(Carbon $carbon)
    {
        return ($carbon->dayOfWeek == Carbon::SATURDAY || $carbon->dayOfWeek == Carbon::SUNDAY);
    }


    /**
     * @param Carbon $carbon
     *
     * @return bool
     */
    public static function dayIsPublicHoliday(Carbon $carbon)
    {
        return in_array($carbon->format('md'), CalendarHelper::getPublicHolidayList($carbon->year));
    }

    /**
     * @param Carbon $carbon
     *
     * @return bool
     */
    public static function dayIsClosureTime(Carbon $carbon)
    {
        $closure_times = CalendarHelper::getClosureTimes($carbon->format('Y'), ClosureTime::TYPE_FULL_DAY_CLOSED);

        return $closure_times->contains($carbon->format('Y-m-d'));
    }

    /**
     * @param Carbon $carbon
     *
     * @return bool
     */
    public static function afternoonIsClosureTime(Carbon $carbon)
    {
        $closure_times = CalendarHelper::getClosureTimes($carbon->format('Y'), ClosureTime::TYPE_MORNING_CLOSED);

        return $closure_times->contains($carbon->format('Y-m-d'));
    }


    /**
     * @param $year
     *
     * @return mixed
     */
    public static function getPublicHolidayList($year)
    {
        return Cache::remember('public_holiday_'.$year, (365*24*60), function () use ($year) {
            $holiday = [ ];

            // fixed holidays
            $holiday[] = "0101"; // Neujahrstag
            $holiday[] = "0501"; // Tag der Arbeit
            $holiday[] = "1003"; // Tag der Deutschen Einheit
            $holiday[] = "1225"; // Erster Weihnachtstag
            $holiday[] = "1226"; // Zweiter Weihnachtstag

            // movable holidays
            try {
            $holiday[] = Carbon::createFromTimestamp(easter_date($year))->subDays(2)->format('md');         // Karfreitag
            $holiday[] = Carbon::createFromTimestamp(easter_date($year))->addDays(1)->format('md');         // Ostermontag
            $holiday[] = Carbon::createFromTimestamp(easter_date($year))->addDays(39)->format('md');        // Himmelfahrt
            $holiday[] = Carbon::createFromTimestamp(easter_date($year))->addDays(50)->format('md');        // Pfingstmontag
            $holiday[] = Carbon::createFromDate($year, 11, 23)->modify('last wednesday')->format('md');     // Buß- und Bettag
            } catch(\ErrorException $e) {
                dd($e->getMessage(), $year);
            }

            return $holiday;
        });
    }


    /**
     * @param $year
     * @param $type
     *
     * @return mixed
     */
    public static function getClosureTimes($year, $type)
    {
        return Cache::remember('closure_times_'.$year.'_'.$type, (24*60), function () use ($year, $type) {
            return ClosureTime::where('type', $type)
                ->where(function ($query) use ($year) {
                    $query->whereYear('to', $year);
                    $query->orWhere(function ($query) use ($year) {
                        $query->whereYear('from', $year);
                    });
                })
                ->get()
                ->flatMap(function ($closure_time) {
                    if (empty($closure_time->to)) {
                        return [$closure_time->from];
                    } else {
                        $dates = [];
                        for($date = $closure_time->from; $date->lte($closure_time->to); $date->addDay()) {
                            $dates[] = $date->copy();
                        }
                        return $dates;
                    }
                })
                ->transform(function ($closure_time) {
                    return $closure_time->format('Y-m-d');
                });
        });
    }


    /**
     * @param Carbon $day
     *
     * @return bool
     */
    public static function dayIsValidForChangeInShiftlist(Carbon $day)
    {
        $now = Carbon::now();

        if ($day->lte($now)) {
            return false;
        }

        if ($day->dayOfWeek == Carbon::SATURDAY || $day->dayOfWeek == Carbon::SUNDAY) {
            return false;
        }

        if ($now->hour >= 18 && $day->isTomorrow()) {
            return false;
        }

        if ($now->dayOfWeek == Carbon::FRIDAY && $now->hour >= 18 && $now->copy()->addDay(3)->isSameDay($day)) {
            return false;
        }

        if ($now->dayOfWeek == Carbon::SATURDAY && $now->copy()->addDay(2)->isSameDay($day)) {
            return false;
        }

        if ($now->dayOfWeek == Carbon::SUNDAY && $now->copy()->addDay()->isSameDay($day)) {
            return false;
        }

        return true;
    }
}