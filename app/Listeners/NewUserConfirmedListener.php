<?php

namespace App\Listeners;

use App\Models\User;
use App\Mail\NewUserConfirmed as NewUserConfirmedMail;
use App\Events\NewUserConfirmed;
use Illuminate\Support\Facades\Mail;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class NewUserConfirmedListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  NewUserConfirmed  $event
     * @return void
     */
    public function handle(NewUserConfirmed $event)
    {
        User::whereHas('roles', function ($query) {
            $query->where('name', 'manager');
        })->each(function ($manager) use ($event) {
            Mail::to($manager->email)->send(new NewUserConfirmedMail($event->user));
        });
    }
}
