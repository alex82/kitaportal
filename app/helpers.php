<?php

use Illuminate\Support\Debug\Dumper;
use Illuminate\Support\Facades\Auth;
use App\Models\Setting;

function formatBytes($bytes)
{
    $units = ['B', 'KB', 'MB', 'GB', 'TB', 'PB'];

    for ($i = 0; $bytes > 1024; $i++) {
        $bytes /= 1024;
    }

    return round($bytes, 2) . ' ' . $units[$i];
}

function d()
{
    array_map(function ($x) {
        (new Dumper)->dump($x);
    }, func_get_args());
}

function settings($key)
{
    return Auth::user()->getSetting($key);
}