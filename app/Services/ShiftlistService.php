<?php

namespace App\Services;

use Carbon\Carbon;
use App\Models\ShiftlistEntry;

class ShiftlistService {

    /**
     * @param string $day
     *
     * @return array
     */
    public static function getViewDataForDay($day)
    {
        try {
            $day_to_show = Carbon::parse($day);
        } catch (\Exception $e) {
            $day_to_show = Carbon::now();
        }

        $shiftlist_entries = ShiftlistEntry::where('day', $day)->with('child')->get()->sortBy(function ($item) {
            return $item->child->firstname;
        });

        $morning = self::prepareShiftlistItems($shiftlist_entries, 'start_time');
        $evening = self::prepareShiftlistItems($shiftlist_entries, 'end_time');

        return compact('day_to_show', 'morning', 'evening');
    }


    /**
     * @param $shiftlist_entries
     * @param $time_key
     *
     * @return mixed
     */
    public static function prepareShiftlistItems($shiftlist_entries, $time_key)
    {
        return $shiftlist_entries->filter(function ($item) {
            return !empty($item->start_time);
        })->groupBy($time_key)->map(function ($item) {
            return $item->groupBy(function ($entry) {
                return $entry->child->nursery_group->building->description;
            })->map(function ($entries) {
                return $entries->map(function ($entry) {
                    return $entry->child->firstname.' '.strtoupper($entry->child->lastname[0]);
                });
            });
        });
    }
}