<?php

namespace App\Services;

use App\Events\NewUserConfirmed;
use App\Models\User;
use Illuminate\Mail\Mailer;
use Illuminate\Mail\Message;
use App\Mail\UserActivation as UserActivationMail;
use Illuminate\Support\Facades\Mail;
use App\Repositories\ActivationRepository;

class ActivationService
{
    protected $activationRepository;

    protected $resendAfter = 24;

    public function __construct(ActivationRepository $activationRepository)
    {
        $this->activationRepository = $activationRepository;
    }

    public function sendActivationMail($user)
    {
        if ($user->activated || !$this->shouldSend($user)) {
            return;
        }

        $token = $this->activationRepository->createActivation($user);
        
        $link = route('user.activate', $token);

        Mail::to($user->email)->send(new UserActivationMail($link));
    }

    public function activateUser($token)
    {
        $activation = $this->activationRepository->getActivationByToken($token);

        if ($activation === null) {
            return null;
        }

        $user = User::find($activation->user_id);
        $user->activated = true;
        $user->save();

        $this->activationRepository->deleteActivation($token);

        event(new NewUserConfirmed($user));

        return $user;

    }

    private function shouldSend($user)
    {
        $activation = $user->activation;
        return $activation === null || strtotime($activation->created_at) + 60 * 60 * $this->resendAfter < time();
    }

}