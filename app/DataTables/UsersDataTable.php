<?php

namespace App\DataTables;

use App\Models\User;
use Yajra\Datatables\Services\DataTable;

class UsersDataTable extends DataTable
{
    /**
     * Display ajax response.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function ajax()
    {
        return $this->datatables
            ->eloquent($this->query())
            ->addColumn('roles', function ($user) {
                return $user->roles->implode('display_name', ',');
            })
            ->editColumn('confirmed', function ($user) {
                return $user->confirmed ? 'Ja' : 'Nein';
            })
            ->editColumn('activated', function ($user) {
                return $user->activated ? 'Ja' : 'Nein';
            })
            ->editColumn('created_at', function ($user) {
                return $user->created_at->format('d.m.Y H:i');
            })
            ->addColumn('action', function ($user) {
                $btns = [];
                $btns[] = '<a class="btn btn-sm btn-primary" href="'.route('user.edit', $user).'" title="bearbeiten"><i class="fa fa-pencil"></i></a>';

                if (!$user->hasRole('admin')) {
                    $btns[] = '<a class="btn btn-sm btn-primary" href="'.route('impersonate', $user).'" title="simulieren"><i class="fa fa-user-secret"></i></a>';
                }

                $btns[] = '<button type="button" class="btn btn-sm btn-danger" data-toggle="modal" data-target="#myModal" data-id="'.$user->id.'" title="löschen"><i class="fa fa-trash"></i></button>';

                return  implode('&nbsp;', $btns);
            })
            ->make(true);
    }

    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query()
    {
        $query = User::query();

        return $this->applyScopes($query);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->columns($this->getColumns())
                    ->ajax('')
                    ->addAction(['width' => '100px', 'title' => 'Aktion'])
                    ->parameters($this->getBuilderParameters());
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            ['data' => 'name', 'name' => 'name', 'title' => 'Name'],
            ['data' => 'email', 'name' => 'email', 'title' => 'E-Mail'],
            ['data' => 'roles', 'name' => 'roles', 'title' => 'Rolle(n)', 'searchable' => false, 'orderable' => false],
            ['data' => 'confirmed', 'name' => 'confirmed', 'title' => 'bestätigt'],
            ['data' => 'activated', 'name' => 'activated', 'title' => 'aktiviert'],
            ['data' => 'created_at', 'name' => 'created_at', 'title' => 'registriert']
        ];
    }

    protected function getBuilderParameters()
    {
        return [
            'order'   => [[0, 'asc']]
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'usersdatatables_' . time();
    }
}
