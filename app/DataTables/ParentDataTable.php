<?php

namespace App\DataTables;

use App\Models\User;
use Yajra\Datatables\Services\DataTable;

class ParentDataTable extends DataTable
{
    /**
     * Display ajax response.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function ajax()
    {
        return $this->datatables
            ->eloquent($this->query())
            ->addColumn('roles', function ($user) {
                return $user->roles->implode('display_name', ',');
            })
            ->editColumn('confirmed', function ($user) {
                return $user->confirmed ? 'Ja' : 'Nein';
            })
            ->editColumn('activated', function ($user) {
                return $user->activated ? 'Ja' : 'Nein';
            })
            ->editColumn('created_at', function ($user) {
                return $user->created_at->format('d.m.Y H:i');
            })
            ->addColumn('children', function ($user) {
                $count = $user->children()->count();
                $chilrenlist = $user->children->transform(function ($child) {
                    return $child->firstname.' '.$child->lastname.' ('.$child->nursery_group->description.')';
                })->implode('<br>');
                return '<span data-toggle="tooltip" data-html="true" data-placement="bottom" title="'.$chilrenlist.'">'.$count.'</span>';
            })
            ->addColumn('action', function ($user) {
                return  '<a class="btn btn-sm btn-primary" href="'.route('parent.edit', $user).'"><i class="fa fa-pencil"></i></a>&nbsp;'.
                '<button type="button" class="btn btn-sm btn-danger" data-toggle="modal" data-target="#myModal" data-id="'.$user->id.'"><i class="fa fa-trash"></i></button>';
            })
            ->make(true);
    }

    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query()
    {
        $query = User::query();
        $query->whereHas('roles', function ($query) {
            $query->where('name', 'parent');
            $query->orWhere('name', 'nursery');
        });

        return $this->applyScopes($query);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->columns($this->getColumns())
                    ->ajax('')
                    ->addAction(['width' => '80px', 'title' => 'Aktion'])
                    ->parameters($this->getBuilderParameters());
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            ['data' => 'name', 'name' => 'name', 'title' => 'Name'],
            ['data' => 'email', 'name' => 'email', 'title' => 'E-Mail'],
            ['data' => 'children', 'name' => 'children', 'title' => 'Kind(er)', 'searchable' => false, 'orderable' => false],
            ['data' => 'roles', 'name' => 'roles', 'title' => 'Rolle(n)', 'searchable' => false, 'orderable' => false],
            ['data' => 'confirmed', 'name' => 'confirmed', 'title' => 'bestätigt'],
            ['data' => 'activated', 'name' => 'activated', 'title' => 'aktiviert'],
            ['data' => 'created_at', 'name' => 'created_at', 'title' => 'registriert']
        ];
    }

    protected function getBuilderParameters()
    {
        return [
            'order'   => [[0, 'asc']],
            'drawCallback' => "function() { $('[data-toggle=\"tooltip\"]').tooltip(); }"
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'usersdatatables_' . time();
    }
}
