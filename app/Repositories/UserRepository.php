<?php
namespace App\Repositories;

use App\Models\User;

class UserRepository
{
    public function getUsersWithNonDefaultShiftlistEntries()
    {
        return User::whereHas('settings', function ($query) {
            $query->where(function ($query) {
                $query->where('key', '=', 'shiftlist_default_morning_value');
                $query->where('value', '!=', '0800');
            });

            $query->orWhere(function ($query) {
                $query->where('key', '=', 'shiftlist_default_evening_value');
                $query->where('value', '!=', '1700');
            });
        })->get();
    }
}