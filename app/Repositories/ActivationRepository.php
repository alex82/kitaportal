<?php
namespace App\Repositories;

use Carbon\Carbon;
use App\Models\UserActivation;

class ActivationRepository
{
    protected function getToken()
    {
        return hash_hmac('sha256', str_random(40), config('app.key'));
    }

    public function createActivation($user)
    {
        $activation = $user->activation;
        
        if (!$activation) {
            return $this->createToken($user);
        }
        return $this->regenerateToken($user);
    }

    private function regenerateToken($user)
    {
        $token = $this->getToken();

        $activation = $user->activation;

        if ($activation) {
            $activation->token = $token;
            $activation->created_at = new Carbon();
            $activation->save();
        }

        return $token;
    }

    private function createToken($user)
    {
        $token = $this->getToken();

        $user->activation()->create([
            'user_id' => $user->id,
            'token' => $token,
            'created_at' => new Carbon()
        ]);

        return $token;
    }


    public function getActivationByToken($token)
    {
        return UserActivation::where('token', $token)->first();
    }

    public function deleteActivation($token)
    {
        $activation = UserActivation::where('token', $token)->first();

        if ($activation) {
            $activation->delete();
        }
    }
}