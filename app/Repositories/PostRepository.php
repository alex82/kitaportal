<?php
namespace App\Repositories;

use App\Models\Post;
use Illuminate\Support\Facades\Auth;

class PostRepository
{
    public static function postIsVisibleForCurrentUser(Post $post)
    {
        $visibleForBuildings = $post->visibleForBuildings;
        $visibleForGroups = $post->visibleForGroups;

        // admin and manager/nursery should have access to everything
        if (Auth::user()->hasRole('admin') || Auth::user()->hasRole('manager') || Auth::user()->hasRole('nursery')) {
            return true;
        }

        // no buildings or groups set for post, visible for all
        if ($visibleForBuildings->count() == 0 && $visibleForGroups->count() == 0) {
            return true;
        }

        $userChildGroups = Auth::user()->children->map(function ($child) {
            return $child->nursery_group;
        })->unique();

        $groupFound = false;
        $visibleForGroups->each(function ($group) use ($userChildGroups, &$groupFound) {
            if ($userChildGroups->contains($group)) {
                $groupFound = true;
                return;
            }
        });

        if ($groupFound) {
            return true;
        }

        $userChildBuildings = $userChildGroups->transform(function ($group) {
            return $group->building;
        })->unique();

        $buildingFound = false;
        $visibleForBuildings->each(function ($building) use ($userChildBuildings, &$buildingFound) {
            if ($userChildBuildings->contains($building)) {
                $buildingFound = true;
                return;
            }
        });


        if ($buildingFound) {
            return true;
        }

        return false;
    }

    public static function getVisibleFor(Post $post)
    {
        $visibleForBuildings = $post->visibleForBuildings->pluck('description');
        $visibleForGroups = $post->visibleForGroups->pluck('description');

        if ($visibleForBuildings->count() == 0 && $visibleForGroups->count() == 0) {
            return 'Alle';
        }

        $output = [];

        if ($visibleForBuildings->count() > 0) {
            $output[] = 'Gebäude: '.$visibleForBuildings->implode(', ');
        }

        if ($visibleForGroups->count() > 0) {
            $output[] = trans_choice('pluralization.group', $visibleForGroups->count()).': '.$visibleForGroups->implode(', ');
        }

        return implode(' / ', $output);
    }
}