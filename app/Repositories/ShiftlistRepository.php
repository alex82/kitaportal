<?php
namespace App\Repositories;

use App\Models\Child;
use App\Models\User;
use App\Models\ShiftlistEntry;
use Carbon\Carbon;

class ShiftlistRepository
{
    public function createEntriesForNextWeek(User $user, $start_time, $end_time)
    {
        $next_monday = new Carbon('next monday');
        $next_friday = $next_monday->copy()->addDays(4);

        $user->children->each(function ($child ) use ($next_monday, $next_friday, $start_time, $end_time) {
            // load current settings
            $current_entries = $child->shiftlist_entries()->whereBetween('day', [
                $next_monday->format('Y-m-d'),
                $next_friday->format('Y-m-d')
            ])->get();

            for($day = $next_monday; $day <= $next_friday; $day->addDay()) {
                if (!$current_entries->contains('day', $day->format('Y-m-d'))) {
                    $child->shiftlist_entries()->create([
                        'day' => $day,
                        'start_time' => $start_time,
                        'end_time' => $end_time
                    ]);
                }
            }
        });
    }
}