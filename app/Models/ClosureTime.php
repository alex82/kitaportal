<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ClosureTime extends Model
{
    const TYPE_FULL_DAY_CLOSED = 0;
    const TYPE_MORNING_CLOSED = 1;

    protected $fillable = ['from', 'to', 'type'];

    protected $casts = [
        'from' => 'date',
        'to' => 'date'
    ];

    public function getTypeAsString()
    {
        switch ($this->type) {
            case self::TYPE_FULL_DAY_CLOSED:
                return 'Ganzer Tag geschlossen';
                break;

            case self::TYPE_MORNING_CLOSED:
                return 'Nachmittag geschlossen';
                break;

            default:
                throw new \Exception('Unknown Closure Time type '.$this->type);
                break;
        }
    }
}
