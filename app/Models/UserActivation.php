<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserActivation extends Model
{
    protected $guarded = [];

    public $timestamps = false;
    public $incrementing = false;
    public $primaryKey = 'token';
}
