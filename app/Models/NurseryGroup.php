<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class NurseryGroup extends Model
{
    protected $fillable = ['description', 'nursery_building_id'];

    public function building()
    {
        return $this->belongsTo(NurseryBuilding::class, 'nursery_building_id');
    }

    public function children()
    {
        return $this->hasMany(Child::class);
    }
}
