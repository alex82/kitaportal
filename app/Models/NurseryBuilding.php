<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class NurseryBuilding extends Model
{
    protected $fillable = ['description'];

    public function groups()
    {
        return $this->hasMany(NurseryGroup::class);
    }
}
