<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $fillable = ['title', 'text', 'user_id'];

    public function comments()
    {
        return $this->hasMany(Comment::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
    
    public function files()
    {
        return $this->morphMany(File::class, 'related');
    }

    public function visibleForBuildings()
    {
        return $this->morphedByMany(NurseryBuilding::class, 'related', 'post_visibility');
}

    public function visibleForGroups()
    {
        return $this->morphedByMany(NurseryGroup::class, 'related', 'post_visibility');
    }
}
