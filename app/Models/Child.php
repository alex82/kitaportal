<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Child extends Model
{
    protected $fillable = ['firstname', 'lastname', 'nursery_group_id', 'user_id'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function shiftlist_entries()
    {
        return $this->hasMany(ShiftlistEntry::class);
    }

    public function nursery_group()
    {
        return $this->belongsTo(NurseryGroup::class);
    }

    /**
     * @param $day
     *
     * @return ShiftlistEntry
     */
    public function getShiftlistEntryForDay($day)
    {
        $shiftlist_entry = $this->shiftlist_entries()->where('day', $day)->first();

        if (!$shiftlist_entry) {
            $shiftlist_entry = $this->shiftlist_entries()->create([
                'day' => $day
            ]);
        }

        return $shiftlist_entry;
    }
}
