<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ShiftlistEntry extends Model
{
    const TYPE_MORNING = 'start';
    const TYPE_EVENING = 'end';

    const VALUES_MORNING = [
        '0800' => 'nach 08:00 Uhr',
        '0730' => '07:30 - 08:00',
        '0700' => '07:00 - 07:30',
        '0630' => '06:30 - 07:00',
        '0600' => '06:00 - 06:30'
    ];

    const VALUES_EVENING = [
        '1700' => 'vor 17:00 Uhr',
        '1800' => '17:00 - 18:00'
    ];

    protected $fillable = ['children_id', 'day', 'start_time', 'end_time'];

    public function child()
    {
        return $this->belongsTo(Child::class);
    }

    public function saveTime($time) {
        if (in_array($time, array_keys(self::VALUES_MORNING))) {
            $this->start_time = $time;
        } else {
            $this->end_time = $time;
        }
        $this->save();

        return $this;
    }
}
