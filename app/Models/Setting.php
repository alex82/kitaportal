<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    const KEY_SHIFTLIST_DEFAULT_MORNING_VALUE = 'shiftlist_default_morning_value';
    const KEY_SHIFTLIST_DEFAULT_EVENING_VALUE = 'shiftlist_default_evening_value';

    const DEFAULTS = [
        Setting::KEY_SHIFTLIST_DEFAULT_MORNING_VALUE => '0800',
        Setting::KEY_SHIFTLIST_DEFAULT_EVENING_VALUE => '1700'
    ];

    protected $fillable = ['user_id', 'key', 'value'];

    public static function getDefault($key)
    {
        return array_get(Setting::DEFAULTS, $key, null);
    }
}
