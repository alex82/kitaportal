<?php namespace App\Http\ViewComposers;

use App\Models\Comment;
use Illuminate\View\View;

class GlobalComposer {

    public function compose(View $view)
    {
        $view->with('unapproved_comment_count', Comment::unApproved()->count());
    }

}