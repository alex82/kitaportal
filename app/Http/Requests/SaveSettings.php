<?php

namespace App\Http\Requests;

use App\Models\Setting;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class SaveSettings extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::user()->hasRole('parent');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            Setting::KEY_SHIFTLIST_DEFAULT_MORNING_VALUE => 'in:0600,0630,0700,0730,0800',
            Setting::KEY_SHIFTLIST_DEFAULT_EVENING_VALUE => 'in:1700,1800'
        ];
    }
}
