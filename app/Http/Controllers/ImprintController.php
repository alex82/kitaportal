<?php

namespace App\Http\Controllers;


use Auth;
use App\Services\ShiftlistService;
use App\Http\Controllers\Controller;

class ImprintController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    public function index($day = null)
    {
        return view('public.imprint');
    }
}
