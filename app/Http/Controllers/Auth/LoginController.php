<?php

namespace App\Http\Controllers\Auth;

use Auth;
use Illuminate\Http\Request;
use App\Services\ActivationService;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\Models\User;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/news';

    /**
     * @var ActivationService
     */
    protected $activationService;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(ActivationService $activationService)
    {
        $this->middleware('guest', ['except' => ['logout', 'impersonate', 'stopImpersonate']]);
        $this->activationService = $activationService;
    }

    /**
     * The user has been authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  mixed  $user
     * @return mixed
     */
    protected function authenticated(Request $request, $user)
    {
        if (!$user->activated) {
            auth()->logout(); // logout first, if mail sending fails
            $this->activationService->sendActivationMail($user);
            return back()->with('warning', 'Sie müssen Ihren Account erst bestätigen. Sie haben dazu eine E-Mail erhalten.');
        }

        if (!$user->confirmed) {
            auth()->logout();
            return back()->with('warning', 'Ihr Account wurde durch die Kita-Leitung noch nicht freigeschaltet.');
        }

        return redirect()->intended($this->redirectPath());
    }


    /**
     * @param $id
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function impersonate($id)
    {
        $user = User::find($id);

        // Guard against administrator impersonate
        if (!$user->hasRole('admin')) {
            Auth::user()->setImpersonating($user->id);
        } else {
            flash()->error('Impersonate disabled for this user.');
        }

        return redirect()->back();
    }


    /**
     * @return \Illuminate\Http\RedirectResponse
     */
    public function stopImpersonate()
    {
        Auth::user()->stopImpersonating();

        flash()->success('Willkommen zurück!');

        return redirect()->back();
    }
}
