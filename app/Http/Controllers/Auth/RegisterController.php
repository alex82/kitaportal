<?php

namespace App\Http\Controllers\Auth;

use App\Models\Child;
use App\Models\Role;
use App\Models\NurseryBuilding;
use App\Models\NurseryGroup;
use App\Models\User;
use App\Services\ActivationService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Auth\Events\Registered;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/register/successful';

    /**
     * @var ActivationService
     */
    protected $activationService;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(ActivationService $activationService)
    {
        $this->middleware('guest');
        $this->activationService = $activationService;
    }

    /**
     * Show the application registration form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showRegistrationForm()
    {
        $buildings = NurseryBuilding::with('groups')->orderBy('description')->get();

        return view('auth.register')->with(compact('buildings'));
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        $validator = Validator::make($data, [
            'name' => 'max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
            'child' => 'required_unless:nokids,1',
        ], [
            'child.required' => 'Bitte geben Sie Ihr Kind / Ihre Kinder an',
            'child.array' => 'Bitte geben Sie Ihr Kind / Ihre Kinder an',
            'child.*.firstname.required' => 'Bitte geben Sie die Namen Ihres Kindes / Ihre Kinder an',
            'child.*.lastname.required' => 'Bitte geben Sie die Namen Ihres Kindes / Ihre Kinder an',
            'child.*.group.not_in' => "Bitte eine Gruppe für alle Kinder auswählen"
        ]);

        $validator->sometimes('child.*.firstname', 'required|string', function ($input) {
            return $input->nokids != 1;
        });

        $validator->sometimes('child.*.lastname', 'required_with_all:child.*.firstname,child.*.group|string', function ($input) {
            return $input->nokids != 1;
        });

        $validator->sometimes('child.*.group', 'required_with_all:child.*.firstname,child.*.lastname|not_in:0', function ($input) {
            return $input->nokids != 1;
        });

        return $validator;
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        $user = User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);

        if (request('nokids') == 1) {
            $role = Role::where('name', 'nursery')->first();
        } else {
            $role = Role::where('name', 'parent')->first();
        }

        $user->attachRole($role);

        collect($data['child'])->each(function ($child_data) use ($user) {
            if (!empty($child_data['firstname'])) {
                Child::create([
                    'firstname' => $child_data['firstname'],
                    'lastname' => $child_data['lastname'],
                    'nursery_group_id' => $child_data['group'],
                    'user_id' => $user->id
                ]);
            }
        });

        return $user;
    }

    /**
     * Handle a registration request for the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        $this->validator($request->all())->validate();

        event(new Registered($user = $this->create($request->all())));

        $this->activationService->sendActivationMail($user);

        return $this->registered($request, $user)
            ?: redirect($this->redirectPath());
    }


    /**
     * @param $token
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function activateUser($token)
    {
        if ($user = $this->activationService->activateUser($token)) {
            return view('auth.confirmation_awaiting');
        }
        abort(404);
    }


    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showSuccessfulMessage()
    {
        return view('auth.register_successful');
    }
}
