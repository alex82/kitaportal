<?php

namespace App\Http\Controllers\Api;

use App\Models\ShiftlistEntry;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class ShiftlistController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    public function update(Request $request)
    {
        $this->validate($request, [
            'userid' => 'required|in:' . Auth::id(),
            'day' => 'required|date|after:today',
            'time' => 'required|in:0600,0630,0700,0730,0800,1700,1800'
        ]);

        Auth::user()->children()->each(function ($child) {
            /* @var $child \App\Models\Child */
            $child->getShiftlistEntryForDay(request('day'))->saveTime(request('time'));
        });
    }
}
