<?php

namespace App\Http\Controllers\Admin\UserManager;

use App\DataTables\UsersDataTable;
use App\Models\Permission;
use App\Models\Role;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PermissionController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:usermanager-access');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $permissions = Permission::with('roles')->get();

        return view('admin.usermanager.permission.index')->with(compact('permissions'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $permission = Permission::find($id);
        $roles = Role::all();

        return view('admin.usermanager.permission.edit')->with(compact('permission', 'roles'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $permission = Permission::find($id);
        $permission->name = request('name');
        $permission->display_name = request('display_name');
        $permission->description = request('description');
        $permission->save();

        $permission->roles()->detach();

        $role_ids = request('roles');
        if (is_array($role_ids) && count($role_ids) > 0) {
            Role::find($role_ids)->each(function ($role) use ($permission) {
                $role->attachPermission($permission);
            });
        }

        return redirect()->route('permission.index')->with('success', 'Berechtigung erfolgreich geändert');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Role::all();

        return view('admin.usermanager.permission.create')->with(compact('roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string|max:255',
            'display_name' => 'required|string|max:255',
            'description' => 'string|max:255',
        ]);

        $permission = Permission::create($request->all());

        $role_ids = request('roles');
        if (is_array($role_ids) && count($role_ids) > 0) {
            Role::find($role_ids)->each(function ($role) use ($permission) {
                $role->attachPermission($permission);
            });
        }

        return redirect()->route('permission.index')->with('success', 'Berechtigung erfolgreich angelegt');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $permission = Permission::find($id);
        $permission->delete();

        return redirect()->route('permission.index')->with('success', 'Berechtigung erfolgreich gelöscht');
    }
}
