<?php

namespace App\Http\Controllers\Admin\UserManager;

use App\Models\Role;
use App\Models\User;
use Illuminate\Http\Request;
use App\DataTables\ParentDataTable;
use App\Http\Controllers\Controller;

class ParentController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:parentmanager-access');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(ParentDataTable $dataTable)
    {
        return $dataTable->render('admin.usermanager.parent.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);
        $roles = Role::where('name', '!=', 'admin')->get();

        return view('admin.usermanager.parent.edit')->with(compact('user', 'roles'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::find($id);
        $user->name = request('name');
        $user->email = request('email');
        $user->confirmed = ($request->has('confirmed') && request('confirmed') == 1);
        $user->activated = ($request->has('activated') && request('activated') == 1);
        $user->save();

        $user->roles()->detach();

        $role_ids = request('roles');
        if (is_array($role_ids) && count($role_ids) > 0) {
            Role::find($role_ids)->each(function ($role) use ($user) {
                $user->attachRole($role);
            });
        }

        return redirect()->route('parent.index')->with('success', 'User erfolgreich geändert');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);
        $user->delete();

        return redirect()->route('user.index')->with('success', 'User erfolgreich gelöscht');
    }
}