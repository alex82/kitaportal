<?php

namespace App\Http\Controllers\Admin\Shiftlist;

use Auth;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Services\ShiftlistService;
use App\Http\Controllers\Controller;

class IndexController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:shiftlist-view|shiftlist-edit');
    }

    public function index($year = null, $month = null)
    {
        if (!Auth::user()->can('shiftlist-edit')) {
            return redirect()->route('news_index');
        }

        // use 2016 to check here, as the tool started 2016
        $year = (intval($year) >= 2016) ? intval($year) : date("Y");
        $month = (intval($month) >= 1 && intval($month) <= 12) ? intval($month) : date("m");
        $carbon = Carbon::create($year, $month, 1, 0, 0, 0);
        $user = Auth::user();

        return view('admin.shiftlist.index')->with(compact('year', 'month', 'carbon', 'user'));
    }

    public function view($day = null)
    {
        if (!Auth::user()->can('shiftlist-view')) {
            return redirect()->route('news_index');
        }

        return view('admin.shiftlist.view')->with(ShiftlistService::getViewDataForDay($day));
    }
}
