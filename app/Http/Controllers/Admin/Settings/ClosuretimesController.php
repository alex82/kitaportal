<?php

namespace App\Http\Controllers\Admin\Settings;

use App\Models\Child;
use App\Models\ClosureTime;
use App\Models\NurseryBuilding;
use Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;

class ClosuretimesController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:closuretimes-access');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $closuretimes = ClosureTime::whereYear('from', '>=', date("Y"))->get();

        return view('admin.settings.closuretimes.index')->with(compact('closuretimes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.settings.closuretimes.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'from' => 'required|date',
            'to' => 'date',
            'type' => 'required|in:'.ClosureTime::TYPE_FULL_DAY_CLOSED.','.ClosureTime::TYPE_MORNING_CLOSED,
        ], [
            'from.required' => 'Bitte geben Sie ein Startdatum ein',
            'from.date' => 'Bitte geben Sie ein gültiges Startdatum ein',
            'to.date' => 'Bitte geben Sie ein gültiges Enddatum ein',
            'type.required' => 'Bitte wählen Sie einen Typ aus',
            'type.in' => 'Bitte wählen Sie einen Typ aus'
        ]);

        ClosureTime::create([
            'from' => strtotime(request('from')),
            'to' => strtotime(request('to')),
            'type' => request('type')
        ]);

        return redirect()->route('closuretimes.index')->with('success', 'Schließzeit erfolgreich angelegt');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }


    /**
     * @param $id
     *
     * @return View
     */
    public function edit($id)
    {
        $closuretime = ClosureTime::find($id);

        return view('admin.settings.closuretimes.edit')->with(compact('closuretime'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'from' => 'required|date',
            'to' => 'date',
            'type' => 'required|in:'.ClosureTime::TYPE_FULL_DAY_CLOSED.','.ClosureTime::TYPE_MORNING_CLOSED,
        ], [
            'from.required' => 'Bitte geben Sie ein Startdatum ein',
            'from.date' => 'Bitte geben Sie ein gültiges Startdatum ein',
            'to.date' => 'Bitte geben Sie ein gültiges Enddatum ein',
            'type.required' => 'Bitte wählen Sie einen Typ aus',
            'type.in' => 'Bitte wählen Sie einen Typ aus'
        ]);

        $closuretime = ClosureTime::find($id);

        $closuretime->from = strtotime(request('from'));
        $closuretime->to = strtotime(request('to'));
        $closuretime->type = request('type');
        $closuretime->save();

        return redirect()->route('closuretimes.index')->with('success', 'Schließzeit erfolgreich geändert');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $entry = ClosureTime::find($id);
        $entry->delete();

        return redirect()->route('closuretimes.index')->with('success', 'Schließzeit erfolgreich gelöscht');
    }
}
