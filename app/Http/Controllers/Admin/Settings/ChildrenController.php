<?php

namespace App\Http\Controllers\Admin\Settings;

use App\Models\Child;
use App\Models\NurseryBuilding;
use Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ChildrenController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:parentsettings-access');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $children = Auth::user()->children;

        return view('admin.settings.children.index')->with(compact('children'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $buildings = NurseryBuilding::with('groups')->get();

        return view('admin.settings.children.create')->with(compact('buildings'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'firstname' => 'required|max:255',
            'lastname' => 'required|max:255',
            'group' => 'required|exists:nursery_groups,id',
        ], [
            'firstname.required' => 'Bitte geben Sie den Vorname Ihres Kindes ein',
            'firstname.max' => 'Bitte geben Sie den Vorname Ihres Kindes ein',
            'lastname.required' => 'Bitte geben Sie den Nachnamen Ihres Kindes ein',
            'lastname.max' => 'Bitte geben Sie den Nachnamen Ihres Kindes ein',
            'group.required' => 'Bitte wählen Sie eine Gruppe aus',
            'group.exists' => 'Bitte wählen Sie eine Gruppe aus'
        ]);

        Child::create([
            'firstname' => request('firstname'),
            'lastname' => request('lastname'),
            'nursery_group_id' => request('group'),
            'user_id' => Auth::user()->id
        ]);

        return redirect()->route('children.index')->with('success', 'Kind erfolgreich angelegt');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $buildings = NurseryBuilding::with('groups')->get();
        $child = Child::find($id);

        if ($child->user_id != Auth::user()->id) {
            return response('Sie können dieses Kind nicht bearbeiten!', 404);
        }

        return view('admin.settings.children.edit')->with(compact('buildings', 'child'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'firstname' => 'required|max:255',
            'lastname' => 'required|max:255',
            'group' => 'required|exists:nursery_groups,id',
        ], [
            'firstname.required' => 'Bitte geben Sie den Vorname Ihres Kindes ein',
            'firstname.max' => 'Bitte geben Sie den Vorname Ihres Kindes ein',
            'lastname.required' => 'Bitte geben Sie den Nachnamen Ihres Kindes ein',
            'lastname.max' => 'Bitte geben Sie den Nachnamen Ihres Kindes ein',
            'group.required' => 'Bitte wählen Sie eine Gruppe aus',
            'group.exists' => 'Bitte wählen Sie eine Gruppe aus'
        ]);

        $child = Child::find($id);

        if ($child->user_id != Auth::user()->id) {
            return response('Sie können dieses Kind nicht bearbeiten!', 404);
        }

        $child->firstname = request('firstname');
        $child->lastname = request('lastname');
        $child->nursery_group_id = request('group');
        $child->save();

        return redirect()->route('children.index')->with('success', 'Kind erfolgreich geändert');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $child = Child::find($id);

        if ($child->user_id != Auth::user()->id) {
            return response('Sie können dieses Kind nicht bearbeiten!', 404);
        }

        $child->delete();

        return redirect()->route('children.index')->with('success', 'Kind erfolgreich gelöscht');
    }
}
