<?php

namespace App\Http\Controllers\Admin\Settings;

use App\Http\Requests\SaveSettings;
use App\Models\Child;
use App\Models\NurseryBuilding;
use App\Models\Setting;
use Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SettingsController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:parentsettings-access');
    }

    public function index()
    {
        return view('admin.settings.settings.index');
    }

    public function save(SaveSettings $saveSettings)
    {
        $settings_to_set = array_only($saveSettings->all(), array_keys(Setting::DEFAULTS));

        foreach ($settings_to_set as $key => $value) {
            Auth::user()->settings()->updateOrCreate(
                ['key' => $key],
                ['value' => $value]
            );
        }

        return redirect()->route('settings_index')->with('success', 'Einstellungen gespeichert');
    }
}
