<?php

namespace App\Http\Controllers\Admin\News;

use Auth;
use App\Models\File;
use App\Models\Post;
use App\Models\Comment;
use Illuminate\Http\Request;
use App\Mail\NewUserConfirmed;
use Illuminate\Http\UploadedFile;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;

class PostController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Post::with('user')->orderBy('created_at', 'DESC')->paginate(5);

        $unapprovedComments = Comment::unApproved()->count();

        return view('admin/news/index')->with(compact('posts', 'unapprovedComments'));
    }

    public function details(Post $post)
    {
        return view('admin/news/details')->with(compact('post'));
    }

    public function delete(Post $post)
    {
        $post->delete();

        return redirect()->route('news_index')->with('success_message', 'Beitrag erfolgreich gelöscht');
    }

    public function create()
    {
        return view('admin.news.create');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required|max:255',
            'text' => 'required|not_in:<p><br></p>',
            'files.*' => 'file'
        ], [
            'title.required' => 'Bitte geben Sie einen Titel ein.',
            'title.max' => 'Der Titel darf nicht mehr als 255 Zeichen haben.',
            'text.required' => 'Bitte geben Sie einen Text ein.',
            'text.not_in' => 'Bitte geben Sie einen Text ein.'
        ]);

        $post = Post::create([
            'title' => request('title'),
            'text' => request('text'),
            'user_id' => Auth::id()
        ]);

        if ($post) {
            $post->visibleForGroups()->attach(request('visible_for_groups'));
            $post->visibleForBuildings()->attach(request('visible_for_buildings'));

            collect($request->file('files'))->each(function ($uploaded_file) use ($post) {
                /**
                 * @var UploadedFile $uploaded_file
                 */
                $file_path = $uploaded_file->store('uploads');

                File::create([
                    'file_name' => $uploaded_file->getClientOriginalName(),
                    'file_path' => $file_path,
                    'mime_type' => $uploaded_file->getMimeType(),
                    'size' => $uploaded_file->getSize(),
                    'related_id' => $post->id,
                    'related_type' => get_class($post)
                ]);
            });

            return redirect()->route('news_index')->with('success_message', 'Beitrag erfolgreich gespeichert');
        } else {
            return redirect()->route('news_create_post')->with('error_message', 'Ihr Beitrag konnte nicht gespeichert werden');
        }
    }

    public function edit(Post $post)
    {
        return view('admin.news.edit')->with(compact('post'));
    }

    public function update(Post $post, Request $request)
    {
        $this->validate($request, [
            'title' => 'required|max:255',
            'text' => 'required|not_in:<p><br></p>',
            'files.*' => 'file'
        ], [
            'title.required' => 'Bitte geben Sie einen Titel ein.',
            'title.max' => 'Der Titel darf nicht mehr als 255 Zeichen haben.',
            'text.required' => 'Bitte geben Sie einen Text ein.',
            'text.not_in' => 'Bitte geben Sie einen Text ein.'
        ]);

        $post->title = request('title');
        $post->text = request('text');
        $post->save();

        $post->visibleForGroups()->detach();
        $post->visibleForGroups()->attach(request('visible_for_groups'));

        $post->visibleForBuildings()->detach();
        $post->visibleForBuildings()->attach(request('visible_for_buildings'));

        collect($request->file('files'))->each(function ($uploaded_file) use ($post) {
            /**
             * @var UploadedFile $uploaded_file
             */
            $file_path = $uploaded_file->store('uploads');

            File::create([
                'file_name' => $uploaded_file->getClientOriginalName(),
                'file_path' => $file_path,
                'mime_type' => $uploaded_file->getMimeType(),
                'size' => $uploaded_file->getSize(),
                'related_id' => $post->id,
                'related_type' => get_class($post)
            ]);
        });

        return redirect()->route('news_index')->with('success_message', 'Beitrag erfolgreich gespeichert');

    }

    public function deleteFile(Post $post, File $file)
    {
        $file->delete();
        flash()->success('Datei gelöscht');
        return redirect()->route('news_edit_post', $post);
    }
}
