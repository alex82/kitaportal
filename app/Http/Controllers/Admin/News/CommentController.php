<?php

namespace App\Http\Controllers\Admin\News;

use Auth;
use App\Models\Post;
use App\Models\Comment;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CommentController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    public function delete(Comment $comment)
    {
        $comment->delete();

        return redirect()->route('news_show_comments', $comment->post)->with('success_message', 'Kommentar erfolgreich gelöscht');
    }

    public function store(Post $post, Request $request)
    {
        $this->validate($request, [
            'comment' => 'required'
        ], [
            'comment.required' => 'Bitte geben Sie einen Text ein.'
        ]);

        $comment = Comment::create([
            'text' => request('comment'),
            'user_id' => Auth::id(),
            'post_id' => $post->id,
            'approved' => config('app.comments_need_approval') ? 0 : 1
        ]);

        if ($comment) {
            return redirect()->route('news_show_comments', $post)->with('success_message', 'Vielen Dank für Ihren Kommentar, diese wird in Kürze freigeschaltet.');
        } else {
            return redirect()->route('news_show_comments', $post)->with('error_message', 'Ihr Kommentar konnte nicht gespeichert werden');
        }
    }

    public function showApprovals()
    {
        $comments = Comment::unApproved()->get();

        return view('admin.news.comment_approval')->with(compact('comments'));
    }

    public function approve(Comment $comment)
    {
        $comment->approved = 1;
        $comment->save();

        return redirect()->route('show_approvals')->with('success_message', 'Kommentar freigeschaltet.');
    }
}
