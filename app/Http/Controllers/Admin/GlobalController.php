<?php

namespace App\Http\Controllers\Admin;

use App\Models\File;
use App\Http\Controllers\Controller;

class GlobalController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    public function downloadFile(File $file)
    {
        return response()->download(storage_path('app/'.$file->file_path), $file->file_name);
    }
}
