<?php

namespace App\Models;

use Illuminate\Support\Facades\Session;
use Carbon\Carbon;
use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laratrust\Traits\LaratrustUserTrait;

class User extends Authenticatable
{
    use LaratrustUserTrait;
    use HasApiTokens, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'confirmed' => 'boolean',
        'activated' => 'boolean',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function activation()
    {
        return $this->hasOne(UserActivation::class);
    }

    public function children()
    {
        return $this->hasMany(Child::class)->with('shiftlist_entries');
    }

    public function settings()
    {
        return $this->hasMany(Setting::class);
    }

    public function getSetting($key, $useDefault = true)
    {
        if ($this->settings->contains('key', $key)) {
            return $this->settings->where('key', $key)->first()->value;
        } else {
            return ($useDefault) ? Setting::getDefault($key) : null;
        }
    }

    public function getShiftlistValue(Carbon $day, $type)
    {
        $shiftlist_entry = $this->children->first()->shiftlist_entries->where('day', $day->format('Y-m-d'))->first();

        return ($shiftlist_entry) ? $shiftlist_entry->{$type.'_time'} : '';
    }

    public function setImpersonating($id)
    {
        Session::put('impersonate', $id);
    }

    public function stopImpersonating()
    {
        Session::forget('impersonate');
    }

    public function isImpersonating()
    {
        return Session::has('impersonate');
    }
}
