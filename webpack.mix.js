const { mix } = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.scripts([
    './resources/assets/js/pages/register.js',
    './resources/assets/js/pages/shiftlist.js'
], 'public/js/app.js');

mix.copy('bower_components/bootstrap/dist/fonts', 'public/fonts');
mix.copy('resources/assets/fonts', 'public/fonts');
mix.copy('bower_components/fontawesome/fonts', 'public/fonts');
//mix.copy('bower_components/summernote/dist/font', 'public/css/font');
//mix.copy('bower_components/trumbowyg/dist/ui/icons.svg', 'public/js/ui/icons.svg');
mix.styles([
    //'bower_components/bootstrap/dist/css/bootstrap.css',
    'bower_components/fontawesome/css/font-awesome.css',
    //'bower_components/jquery-ui/themes/base/jquery-ui.css',
    //'bower_components/summernote/dist/summernote.css',
    'bower_components/jodit/jodit.min.css',
    'resources/assets/css/simple-line-icons.css',
    'resources/assets/css/style.css',
    'resources/assets/css/styles.css',
    'bower_components/toastr/toastr.min.css',
    'bower_components/bootstrap-multiselect/dist/css/bootstrap-multiselect.css'
], 'public/css/styles.css', './');

mix.scripts([
    'bower_components/jquery/dist/jquery.js',
    'bower_components/tether/dist/js/tether.js',
    'bower_components/bootstrap/dist/js/bootstrap.js',
    'bower_components/Chart.js/Chart.js',
    'bower_components/metisMenu/dist/metisMenu.js',
    'bower_components/jodit/jodit.min.js',
    'resources/assets/js/jodit_de.js',       // usw own version until pull request has been merged
    'bower_components/toastr/toastr.min.js',
    'resources/assets/js/coreui-app.js',
    'bower_components/bootstrap-multiselect/dist/js/bootstrap-multiselect.js',
], 'public/js/frontend.js', './');
