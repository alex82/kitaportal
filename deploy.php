<?php
namespace Deployer;
require 'recipe/common.php';

// Configuration
set('ssh_type', 'native');
set('ssh_multiplexing', true);
set('use_atomic_symlink', true);
set('repository', 'git@bitbucket.org:alex82/kitaportal.git');
set('default_stage', 'prod');
set('composer_options', '--ignore-platform-reqs');

// Servers
server('production', '5.189.158.130', '22')
    ->user('alex')
    ->identityFile('/home/vagrant/.ssh/deployment.pub', '/home/vagrant/.ssh/deployment', '')
    ->set('deploy_path', '/docker/kitaportal/data/docroot')
    ->stage('prod');


// Tasks
task('deploy', [
    'deploy:prepare',
    'deploy:lock',
    'deploy:release',
    'deploy:fix_release',
    'deploy:update_code',
    'deploy:docker_shared',
    'deploy:docker_vendors',
    'artisan:view:clear',
    'artisan:cache:clear',
    'artisan:config:cache',
    'artisan:optimize',
    'artisan:migrate',
    'deploy:docker_symlink',
    'deploy:unlock',
    'cleanup',
]);

task('deploy:fix_release', function () {
    $releasePath = str_replace('/docker/kitaportal/data/docroot', '/var/www/app', get('release_path'));
    run('docker exec kita_web rm /var/www/app/release');
    run('docker exec kita_web ln -s '.$releasePath.' /var/www/app/release');
});

task('artisan:migrate', function () {
    $releasePath = str_replace('/docker/kitaportal/data/docroot', '/var/www/app', get('release_path'));
    run("docker exec kita_web bash -c 'cd ".$releasePath." && php artisan migrate --force'");
});

task('artisan:optimize', function () {
    $releasePath = str_replace('/docker/kitaportal/data/docroot', '/var/www/app', get('release_path'));
    run("docker exec kita_web bash -c 'cd ".$releasePath." && php artisan optimize'");
});

task('artisan:config:cache', function () {
    $releasePath = str_replace('/docker/kitaportal/data/docroot', '/var/www/app', get('release_path'));
    run("docker exec kita_web bash -c 'cd ".$releasePath." && php artisan config:cache'");
});

task('artisan:cache:clear', function () {
    $releasePath = str_replace('/docker/kitaportal/data/docroot', '/var/www/app', get('release_path'));
    run("docker exec kita_web bash -c 'cd ".$releasePath." && php artisan cache:clear'");
});

task('artisan:view:clear', function () {
    $releasePath = str_replace('/docker/kitaportal/data/docroot', '/var/www/app', get('release_path'));
    run("docker exec kita_web bash -c 'cd ".$releasePath." && php artisan view:clear'");
});

task('deploy:docker_symlink', function () {
    $releasePath = str_replace('/docker/kitaportal/data/docroot', '/var/www/app', get('release_path'));
    run('docker exec kita_web rm /var/www/app/current');
    run('docker exec kita_web ln -s '.$releasePath.' /var/www/app/current');
});

task('deploy:docker_vendors', function () {
    $releasePath = str_replace('/docker/kitaportal/data/docroot', '/var/www/app', get('release_path'));
    run("docker exec kita_web bash -c 'cd ".$releasePath." && composer install --verbose --prefer-dist --no-progress --no-interaction --no-dev --optimize-autoloader'");
});

task('deploy:docker_shared', function () {
    $releasePath = str_replace('/docker/kitaportal/data/docroot', '/var/www/app', get('release_path'));
    run('docker exec kita_web ln -s /var/www/app/shared/storage '.$releasePath.'/storage');
    run('docker exec kita_web ln -s /var/www/app/shared/.env '.$releasePath.'/.env');
});

task('restart:server', function () {
    run('docker restart kita_web');
});

task('backup:run', function () {
    run('php /var/www/app/current/artisan backup:run');
});

//before('deploy', 'backup:run');
after('deploy', 'restart:server');
after('rollback', 'restart:server');
